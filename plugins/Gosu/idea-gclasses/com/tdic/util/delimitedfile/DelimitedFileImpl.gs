package com.tdic.util.delimitedfile

uses com.tdic.util.properties.PropertyUtil
uses gw.lang.reflect.TypeSystem

uses java.lang.RuntimeException
uses java.util.ArrayList

/**
 * This is the default implementation of the DelimitedFile abstract class.  This class can be further extended to
 * override certain functions, such as the definition of the header and trailer lines.
 */
class DelimitedFileImpl extends DelimitedFile {
  /**
   * Implementation of the abstract method.  This will handle the following prefixes after the "Fixed." prefix:
   * "Cache." - Retrieves the value from the Cache Manager Property table
   * "Reflect." - Uses Reflection to call a method that returns a String
   * "Data." - Retrieves the value from the Data column of the FieldSpec
   */
  @Param("fieldSpec", "The FieldSpec object containing the field name and other fields necessary to encode the non-XML field")
  @Returns("A String containing the value of the non-XML field")
  override function getNonXMLFieldValue(fieldSpec: FieldSpec): String {
    var fieldName = fieldSpec.Field
    // Strip off the "Fixed." prefix before checking the next prefix
    if (fieldName.startsWithIgnoreCase("Fixed.")) {
      fieldName = fieldName.substring(6)
    }

    // Check the next prefix to determine the action
    if (fieldName.startsWithIgnoreCase("Cache.")) {
      return getCacheValue(fieldName)
    }
    else if (fieldName.startsWithIgnoreCase("Reflect.")) {
      return getReflectionValue(fieldName)
    }
    else if (fieldName.startsWithIgnoreCase("Data.")) {
      return getSpreadsheetDataValue(fieldSpec)
    }
    else {
      // Default to getting the entire field from the Cache
      return getCacheValue(fieldSpec.Field)
    }
  }

  /**
   * Retrieves the value from the Cache Manager Property table.
   */
  @Param("fieldName", "A String containing the name of the field to retrieve from the Cache Manager Property table")
  @Returns("A String containing the value of the non-XML field")
  public function getCacheValue(fieldName: String): String {
    return PropertyUtil.getInstance().getProperty(fieldName)
  }

  /**
   * Uses Reflection to call a method that returns a String.
   */
  @Param("fieldName", "A String containing the name of the field to retrieve from the Reflection method")
  @Returns("A String containing the value of the non-XML field")
  public function getReflectionValue(fieldName: String): String {
    var myFullClassName = fieldName.substring(0, fieldName.lastIndexOf("."))
    if (myFullClassName.startsWithIgnoreCase("Reflect.")) {
      myFullClassName = myFullClassName.substring(8)
    }
    var methodName = fieldName.substring(fieldName.lastIndexOf(".") + 1, fieldName.size)
    var type = TypeSystem.getByFullName(myFullClassName)
    var instance = type.TypeInfo.getConstructor(null).Constructor.newInstance(null)
    var m = (typeof instance).TypeInfo.Methods.firstWhere(\i -> i.DisplayName == methodName)
    if (m == null) {
      throw new RuntimeException("No method named '" + methodName + "' found for class '" + myFullClassName + "'")
    }
    return m.CallHandler.handleCall(instance, {}) as String
  }

  /**
   * Retrieves the value from the Data column of the FieldSpec.
   */
  @Param("fieldSpec", "The FieldSpec containing the data to return from the Data column")
  @Returns("A String containing the value of the non-XML field")
  public function getSpreadsheetDataValue(fieldSpec: FieldSpec): String {
    return fieldSpec.Data == "'" ? "" : fieldSpec.Data
  }

  /**
   * Default record types of the header lines.
   */
  @Returns("A List<String> containing the record types for the header lines")
  override property get HeaderRecordTypes(): List<String> {
    var recordTypes = new ArrayList<String>()
    recordTypes.add("HEAD")
    return recordTypes
  }

  /**
   * Default record types of the trailer lines.
   */
  @Returns("A List<String> containing the record types for the trailer lines")
  override property get TrailerRecordTypes(): List<String> {
    var recordTypes = new ArrayList<String>()
    recordTypes.add("TRLR")
    return recordTypes
  }
}