package com.tdic.util.database

uses com.tdic.util.database.IWritableBeanInfo

uses java.sql.Connection
uses java.sql.PreparedStatement
uses java.sql.SQLException
uses java.lang.Exception
uses org.slf4j.LoggerFactory

/**
 * Base class for other Writable beans that will be persisted using the BeanInfo framework
 * 
 * @author Guidewire
 */
class WritableBeanBaseImpl {

  /**
   * class level logger
   */
  private static final var _logger = LoggerFactory.getLogger("TDIC_INTEGRATION")

  /**
   * Instance variable to hold the public id / key for external objects,  publicID is the name for all object keys with this framework for now
   */
  private var _publicID : String as PublicID

  /**
   * Instance of the BeanInfo object which describes the current bean
   */  
  private var _info: IWritableBeanInfo

  /**
   * Constructor for WritableBean requiring an BeanInfo object
   * 
   * @param type - BeanInfo object for this WritableBean
   */
  construct(type: IWritableBeanInfo) {
    _info = type
  }

  /**
   * Method which persists the current object (either creating or updating) into the external database
   * 
   * @param conn - Database connection to the external database
   */
  function executeCreate(conn:Connection) {
    _logger.debug("Creating new object...")
    var stmt:PreparedStatement
    stmt = _info.createAndBindCreateSQL(conn, this)

    executeDbCall(\ -> {
      stmt.execute()
    })
    
    closeStatement(stmt)
    _logger.debug("Done")
  }
  
  /**
   * Helper method used to handle database exceptions in a single spot uniformly
   * 
   * @param dbCall - Block of code that executes database related calls that should be wrapped in standard exception handling
   */
  function executeDbCall(dbCall()) {
   try {
     dbCall()
   } catch(e:SQLException) { // This will catch database errors,  calls against closed statements in the block
     _logger.error("Error executing database call", e)
     throw e
   } catch(e:Exception) { // This will catch everything else
     _logger.error("Unexpected error encountered executing database call", e)
     throw e
   }
  }
  
  /**
   * Delegating method that uses the BeanInfo object for this entity to load instance properties from a GW XML model object with properties of the same name
   * 
   * @param model - GW XML Model object for this entity type
   */
  function loadFromModel(model:Object) {
    _info.loadFromModel(this, model)
  }
  
  /**
   * Closes a Prepared Statement.
   * 
   * @param stmt the PreparedStatement object to close
   */
  protected function closeStatement(stmt:PreparedStatement) {
    try {
      if (stmt != null) {
        stmt.close()
      }
    } catch (sqle : SQLException) { // This will catch database errors,  calls against closed statements in the block
     _logger.error("Error closing statement", sqle)
     // Not re-throwing here, we are disposing of resources
    } catch (e:Exception) {
      _logger.error("Unexpected error encountered closing statement", e)
    }
    finally {
      stmt = null
    }
  }
  
  /**
   * Mark the current entity as written to the external system in the database
   *                                                     0
   * @param conn Database connection to use to execute database calls
   */
  function markAsProcessed(conn:Connection) {
    var stmt = _info.markProcessed(conn, this)
    executeDbCall(\ -> {
      stmt.execute()
    })
    closeStatement(stmt)
  }
}