package com.tdic.util.database

uses gw.pl.logging.LoggerCategory
uses java.lang.Class
uses java.lang.Exception
uses java.sql.CallableStatement
uses java.sql.Connection
uses java.sql.DriverManager
uses java.sql.ResultSet
uses java.sql.SQLException
uses org.slf4j.LoggerFactory

/**
 *
 */
class DatabaseManager {
  private static var _logger = LoggerFactory.getLogger("TDIC_INTEGRATION")

  private static final var SQL_SERVER   = "SqlServer"
  private static final var ORACLE       = "Oracle"
  private static final var USED_DB      = SQL_SERVER                    // Modify this per company usage
  private static final var S_DRIVER     = "com.microsoft.sqlserver.jdbc.SQLServerDriver"
  private static final var O_DRIVER     = "oracle.jdbc.driver.OracleDriver"

  /*
   *
   */
  static public function getConnection(url:String) : Connection {
    _logger.debug("DatabaseManager#getConnection(String) - Entering")

    var _con : Connection = null;

    try {
      if (USED_DB == SQL_SERVER) {
        Class.forName(S_DRIVER)
        _con = DriverManager.getConnection(url)

      } else if (USED_DB == ORACLE) {
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver())
        _con = DriverManager.getConnection(url)

      }
      if(_con != null)
        _logger.debug("DatabaseManager#getConnection(String) - Connection Successful!")

    } catch (sql:SQLException) {
      _logger.error("DatabaseManager#getConnection(String) - SQLException = " + sql)
      throw(sql)
    } catch (e:Exception) {
      _logger.error("DatabaseManager#getConnection(String) - Exception = " + e)
      throw(e)
    }

    _logger.debug("DatabaseManager#getConnection(String) - Exiting")

    //For the purposes of being able to rollback, setting auto-commit to false.  Explicit commits are required.
    _con.setAutoCommit(false)
    return _con;
  }

  /*
   *
   */
  static public function getConnection(host:String
                                       , instance:String
                                       , port:String
                                       , dbName:String
                                       , usr:String
                                       , pwd:String) : Connection {
    _logger.debug("DatabaseManager#getConnection(...) - Entering")

    var _con : Connection = null;

    try {
      if (USED_DB == SQL_SERVER) {
        var _dbURL = "jdbc:sqlserver://" + host + "\\" + instance + ":" + port + ";databaseName=" + dbName + ";User=" + usr + ";Password=" + pwd
        _con = getConnection(_dbURL)

      } else if (USED_DB ==ORACLE) {
        var _dbURL = "jdbc:oracle:thin:" + usr + "/" + pwd + "@" + host + ":" + port + ":" + dbName
        _con = getConnection(_dbURL)
      }

      if(_con != null)
        _logger.debug("DatabaseManager#getConnection(String) - Connection Successful!")

    } catch (sql:SQLException) {
      _logger.error("DatabaseManager#getConnection(...) - SQLException = " + sql)
      throw(sql)
    } catch (e:Exception) {
      _logger.error("DatabaseManager#executeQuery(...) - Exception = " + e)
      throw(e)
    }


    _logger.debug("DatabaseManager#closeConnection(...) - Exiting")
    return _con;
  }

  /*
   * Execute a select query that return a result set
   */
  static public function executeQuery(connection: Connection, sqlStmt: String) : ResultSet {
    _logger.debug("DatabaseManager#executeQuery(Connection, String) - Entering")
    var _resultSet: ResultSet
    try {
      var _stmt = connection.prepareStatement(sqlStmt)
      _resultSet = _stmt.executeQuery()
    } catch (sql:SQLException) {
      _logger.error("DatabaseManager#executeQuery(Connection, String) - SQLException = " + sql)
      throw(sql)
    } catch (e:Exception) {
      _logger.error("DatabaseManager#executeQuery(Connection, String) - Exception = " + e)
      throw(e)
    }

    _logger.debug("DatabaseManager#executeQuery(Connection, String) - Exiting")

    return _resultSet
  }

  /*
   * Execute an insert/update sql that returns the number of affected rows
   */
  static public function executeStmt(connection: Connection, sqlStmt: String): int {
    _logger.debug("DatabaseManager#executeInsert(Connection, String) - Entering")
    var _return: int

    try {
      var _stmt = connection.prepareStatement(sqlStmt)
      _return = _stmt.executeUpdate()
      connection.commit()

    } catch (sql:SQLException) {
      _logger.error("DatabaseManager#executeInsert(Connection, String) - SQLException = " + sql)
      throw(sql)
    } catch (e:Exception) {
      _logger.error("DatabaseManager#executeInsert(Connection, String) - Exception = " + e)
      throw(e)
    }

    _logger.debug("DatabaseManager#executeInsert(Connection, String) - Exiting")
    return _return
  }

  /*
   *
   */
  static public function closeConnection(connection:Connection) {
    _logger.debug("DatabaseManager#closeConnection(Connection) - Entering")
    try {
      connection?.close()
    } catch (sql:SQLException) {
      _logger.error("DatabaseManager#closeConnection(Connection) - SQLException = " + sql)
      throw(sql)
    }

    _logger.debug("DatabaseManager#closeConnection(Connection) - Exiting")
  }

  /**
   * closeStatement closes any open CallableStatements
   *
   * @param stmt
   */
  static public function closeCallableStatement(stmt: CallableStatement) {
    _logger.info(getClass().getName() + "Closing callable Statement")
    try {
      if (stmt != null && !stmt.isClosed()) {
        stmt.close()
      }
    } catch (e: SQLException) {
      _logger.error(getClass().getName() + "Error Occurred while Closing Callabale Statement")
    }
  }

}

  /* default SqlServer
  private static final var S_DB_USER      = "gwUser"             // DB username
  private static final var S_DB_PASSWORD  = "gwPassword0"        // DB password
  private static final var S_DB_HOST      = "localhost"
  private static final var S_DB_INSTANCE  = ""
  private static final var S_DB_PORT      = "1433"
  private static final var S_DB_NAME      = "GWINT"              // DB name

  static public function getDefaultSqlConnection() : Connection {
    return getConnection (S_DB_HOST, S_DB_INSTANCE, S_DB_PORT, S_DB_NAME, S_DB_USER, S_DB_PASSWORD)
  }

  */

  /* default Oracle
  private static final var O_DB_USER      = "imf"                // "aes"
  private static final var O_DB_PASSWORD  = "imf"                // "SBKZ23B"
  private static final var O_DB_HOST      = "localhost"
  private static final var O_DB_NAME      = "oper3"              // "oper1"
  private static final var O_DB_PORT      = "1514"               // "1510"
  private static final var O_DATA_SOURCE  = "oracle.jdbc.xa.client.OracleXADataSource"

  static public function getDefaultOracleConnection() : Connection {
    return getConnection (O_DB_HOST, "", O_DB_PORT, O_DB_NAME, O_DB_USER, O_DB_PASSWORD)
  }
  */

