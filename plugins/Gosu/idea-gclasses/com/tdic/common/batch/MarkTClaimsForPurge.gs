package com.tdic.common.batch

uses gw.api.database.Query
uses gw.api.util.DateUtil
uses gw.processes.BatchProcessBase
uses gw.webservice.cc.MaintenanceToolsAPI
uses org.apache.commons.lang.ClassUtils
uses org.slf4j.LoggerFactory

class MarkTClaimsForPurge extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("MARK_TClAIMS_PURGE")
  private final var className = ClassUtils.getShortClassName(getClass())

  construct() {
    super(BatchProcessType.TC_MARKTCLAIMSFORPURGE_EXT)
  }

  protected override function doWork() {
    _logger.info("Entering ${className}: doWork }")
    var methodName = "doWork"
     try{
       var purgeDate = DateUtil.currentDate().addDays(-ScriptParameters.TempClaimsPurgeDays)
       var claimnumberList = Query.make(Claim).compare(Claim#State, Equals, ClaimState.TC_DRAFT).compare(Claim#UpdateTime, LessThan, purgeDate).select()
       var claimnumberString = new java.lang.StringBuffer("")  /* This variable is used for logging the claim numbers*/
       var claimArray = new ArrayList<String>()
       for (claim in claimnumberList) {
         if (TerminateRequested) {
           _logger.info(className + " : " + methodName + ": Terminating Batch Process ")
           return
         }
         claimArray.add(claim.ClaimNumber)
         claimnumberString.append(claim.ClaimNumber).append("  ,")
       }
       var m = new MaintenanceToolsAPI()
       m.markPurgeReady(claimArray.toTypedArray(), true)
       _logger.info("Marking claims for purge :" + claimnumberString)
     } catch(e){
       _logger.error("Error occurred while marking claims for purge" + e.StackTraceAsString)
       incrementOperationsCompleted()
       incrementOperationsFailed()
     }
    incrementOperationsCompleted()
  }
}