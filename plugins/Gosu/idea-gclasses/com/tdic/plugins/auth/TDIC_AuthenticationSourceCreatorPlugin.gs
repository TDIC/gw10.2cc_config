package com.tdic.plugins.auth

uses gw.plugin.security.AuthenticationSourceCreatorPlugin
uses gw.plugin.security.AuthenticationSource
uses gw.api.util.StringUtil
uses gw.plugin.security.UserNamePasswordAuthenticationSource
uses javax.servlet.http.HttpServletRequest
uses org.slf4j.LoggerFactory

/**
 * US684
 * 01/15/2015 Rob Kelly
 *
 * An AuthenticationSourceCreatorPlugin implementation that creates credentials for both SPNEGO SSO and basic login form authentication.
 */
class TDIC_AuthenticationSourceCreatorPlugin implements AuthenticationSourceCreatorPlugin {

  /**
   * The logger for this Plugin.
   */
  private static final var LOGGER = LoggerFactory.getLogger("AUTHENTICATION")

  /**
   * The logger tag for this Plugin.
   */
  private static final var LOG_TAG = "TDIC_AuthenticationSourceCreatorPlugin#"


  construct() {

    // Nothing to do
  }

  /**
   * Creates a new AuthenticationSource from the data in the specified HttpServletRequest.
   */
  @Param("httpServletRequest", "a Http request")
  @Returns("an AuthenticationSource created from the data in httpServletRequest")
  override function createSourceFromHTTPRequest(httpServletRequest : HttpServletRequest) : AuthenticationSource {

    var logTag =  LOG_TAG + "createSourceFromHTTPRequest(HttpServletRequest) - "
    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Entering")

    var authSource : AuthenticationSource = null
    if (isSpnegoAuthentication(httpServletRequest)) {

      if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Creating SPNEGO AuthenticationSource")
      authSource = createSPNEGOAuthenticationSource(httpServletRequest)
    }
    else {

      if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Creating Basic AuthenticationSource")
      authSource = createBasicAuthenticationSource(httpServletRequest)
    }

    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Exiting")
    return authSource
  }

  /**
   * Returns true if the specified HttpServletRequest has SPNEGO authentication details; false otherwise.
   */
  private function isSpnegoAuthentication(aHttpServletRequest : HttpServletRequest) : boolean {

    return aHttpServletRequest.UserPrincipal != null
  }

  /**
   * Creates a new AuthenticationSource for SPNEGO SSO authentication from the specified HttpServletRequest.
   */
  private function createSPNEGOAuthenticationSource(aHttpServletRequest : HttpServletRequest) : AuthenticationSource {

    var logTag =  LOG_TAG + "createSPNEGOAuthenticationSource(HttpServletRequest) - "
    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Entering")

    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "UserPrincipal: " + aHttpServletRequest.UserPrincipal.Name)

    var userName = StringUtil.substring(aHttpServletRequest.UserPrincipal.Name, 0, aHttpServletRequest.UserPrincipal.Name.indexOf("@"))
    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Extracted UserName " + userName)

    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Exiting")
    return new TDIC_UserNameAuthenticationSource(userName)
  }

  /**
   * Creates a new AuthenticationSource for basic login form authentication from the specified HttpServletRequest.
   */
  private function createBasicAuthenticationSource(aHttpServletRequest : HttpServletRequest) : AuthenticationSource {

    var logTag =  LOG_TAG + "createBasicAuthenticationSource(HttpServletRequest) - "
    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Entering")

    var userName = aHttpServletRequest.getAttribute("username")
    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Extracted UserName " + userName)
    var password = aHttpServletRequest.getAttribute("password")

    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Exiting")
    return new UserNamePasswordAuthenticationSource(userName as String, password as String)
  }
}