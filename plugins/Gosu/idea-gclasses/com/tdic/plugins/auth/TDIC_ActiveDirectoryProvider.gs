package com.tdic.plugins.auth

uses com.tdic.util.properties.PropertyUtil

uses java.util.Map
uses java.util.HashMap
uses java.util.Hashtable

uses javax.naming.ldap.LdapContext
uses javax.naming.ldap.InitialLdapContext
uses javax.naming.Context
uses javax.naming.NamingException
uses javax.naming.ConfigurationException
uses javax.naming.directory.SearchControls;
uses javax.naming.directory.Attribute

/**
 * US1132
 * 02/04/2015 Rob Kelly
 *
 * A UserDataProvider implementation for ActiveDirectory.
 */
class TDIC_ActiveDirectoryProvider implements TDIC_UserDataProvider {

  /**
   * The name of the Active Directory URL property in cached properties.
   */
  private static final var AUTHENTICATION_AD_URL = "authentication.ad.url"

  /**
   * The name of the Active Directory username property in cached properties.
   */
  private static final var AUTHENTICATION_AD_USERNAME = "authentication.ad.username"

  /**
   * The name of the Active Directory password property in cached properties.
   */
  private static final var AUTHENTICATION_AD_PASSWORD = "authentication.ad.password"

  /**
   * The mapping between ActiveDirectory user attributes and TDIC_UserDataProvider fields.
   */
  private var activeDirectoryAttributeToUserDataKey : Map<String, String>

  construct() {

    this.activeDirectoryAttributeToUserDataKey = new HashMap<String, String>()
    this.activeDirectoryAttributeToUserDataKey.put("givenname", TDIC_UserDataProvider.FIRST_NAME)
    this.activeDirectoryAttributeToUserDataKey.put("initials", TDIC_UserDataProvider.MIDDLE_NAME)
    this.activeDirectoryAttributeToUserDataKey.put("sn", TDIC_UserDataProvider.LAST_NAME)
    this.activeDirectoryAttributeToUserDataKey.put("telephonenumber", TDIC_UserDataProvider.WORK_PHONE)
    this.activeDirectoryAttributeToUserDataKey.put("mail", TDIC_UserDataProvider.EMAIL_ADDRESS)
    this.activeDirectoryAttributeToUserDataKey.put("streetAddress", TDIC_UserDataProvider.ADDRESS_LINE1)
    this.activeDirectoryAttributeToUserDataKey.put("physicalDeliveryOfficeName", TDIC_UserDataProvider.ADDRESS_LINE2)
    this.activeDirectoryAttributeToUserDataKey.put("l", TDIC_UserDataProvider.ADDRESS_CITY)
    this.activeDirectoryAttributeToUserDataKey.put("st", TDIC_UserDataProvider.ADDRESS_STATE)
    this.activeDirectoryAttributeToUserDataKey.put("postalCode", TDIC_UserDataProvider.ADDRESS_POSTAL_CODE)
    this.activeDirectoryAttributeToUserDataKey.put("c", TDIC_UserDataProvider.ADDRESS_COUNTRY)
    this.activeDirectoryAttributeToUserDataKey.put("title", TDIC_UserDataProvider.JOB_TITLE)
    this.activeDirectoryAttributeToUserDataKey.put("department", TDIC_UserDataProvider.DEPARTMENT)
  }

  /**
   * Returns the user data for the specified username as a mapping between TDIC_UserDataProvider fields and their corresponding values.
   */
  @Param("username", "the username for which the user data is required")
  @Returns("the user data for username as a Map")
  @Throws(NamingException, "if the LDAP connection fails")
  override function getUserData(username: String): Map<String, String> {

    var ldapContext = getLdapContext()
    var controls = new SearchControls()
    controls.setSearchScope(SearchControls.SUBTREE_SCOPE)
    controls.setReturningAttributes(this.activeDirectoryAttributeToUserDataKey.Keys.toTypedArray())
    var searchResults = ldapContext.search("dc=cda,dc=corp", "(&(objectClass=user)(sAMAccountName=" + username + "))", controls)
    var attribute : Attribute
    var attributeValue : String
    var office : String
    var userData = new HashMap<String, String>()
    if (searchResults.hasMore()) {

      var attributes = searchResults.next().getAttributes()
      if (attributes != null) {

        for (anAttributeID in this.activeDirectoryAttributeToUserDataKey.Keys) {

          attribute = attributes.get(anAttributeID)
          attributeValue = attribute != null ? attribute.get() as String : null
          if (anAttributeID == "streetAddress" and attributeValue != null) {

            var addressElements = attributeValue.split("\r\n")
            userData.put(TDIC_UserDataProvider.ADDRESS_LINE1, addressElements[0])
            if (addressElements.length == 2) {
              userData.put(TDIC_UserDataProvider.ADDRESS_LINE2, addressElements[1])
            }
          }
          else if (anAttributeID == "physicalDeliveryOfficeName") {
            office = attributeValue
          }
          else if (anAttributeID.equalsIgnoreCase("initials")) {
            userData.put(this.activeDirectoryAttributeToUserDataKey.get(anAttributeID), "")
          }
          else {
            userData.put(this.activeDirectoryAttributeToUserDataKey.get(anAttributeID), attributeValue)
          }
        }

        if (userData.get(TDIC_UserDataProvider.ADDRESS_LINE2) == null and office != null) {
          userData.put(TDIC_UserDataProvider.ADDRESS_LINE2, office)
        }
      }
    }

    return userData
  }

  @Throws(ConfigurationException, "if a configuration property required for establishing the LDAP connection is not set")
  @Throws(NamingException, "if the LDAP connection fails")
  private function getLdapContext() : LdapContext  {

    var adURL = getLdapPropertyValue(AUTHENTICATION_AD_URL)
    var adUsername = getLdapPropertyValue(AUTHENTICATION_AD_USERNAME)
    var adPassword = getLdapPropertyValue(AUTHENTICATION_AD_PASSWORD)

    var env = new Hashtable<String, String>()
    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory")
    env.put(Context.SECURITY_AUTHENTICATION, "Simple")
    env.put(Context.SECURITY_PRINCIPAL, adUsername)
    env.put(Context.SECURITY_CREDENTIALS, adPassword)
    env.put(Context.PROVIDER_URL, adURL)

    return new InitialLdapContext(env, null)
  }

  @Throws(ConfigurationException, "if the LDAP connection property with name propertyName is not set")
  private function getLdapPropertyValue(propertyName : String) : String {

    var propertyValue = PropertyUtil.getInstance().getProperty(propertyName)
    if (propertyValue == null or propertyValue == "") {

      throw new ConfigurationException("Required property " + propertyName + " not available in PropertiesCache. Ensure property is set in Integration Database.")
    }

    return propertyValue
  }

  public static function isSsoEnabled(): boolean {
    return PropertyUtil.getInstance().getProperty("authentication.sso.enabled")?.toBoolean()
  }
}