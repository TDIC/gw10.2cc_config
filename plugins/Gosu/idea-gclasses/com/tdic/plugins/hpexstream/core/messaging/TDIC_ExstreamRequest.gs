/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P.
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package com.tdic.plugins.hpexstream.core.messaging

uses gw.api.system.PLLoggerCategory
uses gw.plugin.messaging.MessageRequest
uses com.tdic.plugins.hpexstream.core.bo.TDIC_FinalizationRequest
uses org.slf4j.LoggerFactory

class TDIC_ExstreamRequest implements MessageRequest {
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  /**
   * US555
   * 10/13/2014 shanem
   */
  override function resume() {
    _logger.debug("TDIC_ExstreamRequest#resume() - Entering")
    //Do nothing
    _logger.debug("TDIC_ExstreamRequest#resume() - Exiting")
  }

  override property set DestinationID(destinationID : int) {

  }

  /**
   * US555
   * 10/13/2014 shanem

  override function setDestinationID(aDestinationId: int) {
    _logger.debug("TDIC_ExstreamRequest#setDestinationID() - Entering")
    //Do nothing
    _logger.debug("TDIC_ExstreamRequest#setDestinationID() - Exiting")
  }  */

  /**
   * US555
   * 10/13/2014 shanem
   */
  override function shutdown() {
    _logger.debug("TDIC_ExstreamRequest#shutdown() - Entering")
    //Do nothing
    _logger.debug("TDIC_ExstreamRequest#shutdown() - Exiting")
  }

  /**
   * US555
   * 10/13/2014 shanem
   */
  override function suspend() {
    _logger.debug("TDIC_ExstreamRequest#suspend() - Entering")
    //Do nothing
    _logger.debug("TDIC_ExstreamRequest#suspend() - Exiting")
  }

  /**
   * US555
   * 10/13/2014 shanem
   */
  override function afterSend(aMessage: Message) {
    _logger.debug("TDIC_ExstreamRequest#afterSend() - Entering")
    //Do nothing
    _logger.debug("TDIC_ExstreamRequest#afterSend() - Exiting")
  }

  /**
   * US555
   * 02/27/2015 shanem
   *
   * For Finalization request, we use the SenderRefId to store the Document PublicID.
   * We use a Finalization Request object to create the XML to store this information.
   */
  override function beforeSend(aMessage: Message): String {
    _logger.trace("TDIC_ExstreamRequest#beforeSend() - Entering")
    if (aMessage.SenderRefID == null && aMessage.EventName == "FinalizeExstreamDocument") {
      aMessage.SenderRefID = (aMessage.MessageRoot as Document).DocUID
      var requestObject = new TDIC_FinalizationRequest(aMessage.SenderRefID, aMessage.Payload)
      var finalizationRequest = new com.tdic.plugins.hpexstream.core.model.tdic_finalizationrequestmodel.TDIC_FinalizationRequest(requestObject)
      aMessage.Payload = finalizationRequest.asUTFString()
      _logger.debug("Finalization Payload: \n" + finalizationRequest.asUTFString())
      _logger.trace("TDIC_ExstreamRequest#beforeSend() - Exiting")
      return finalizationRequest.asUTFString()
    }
    _logger.trace("TDIC_ExstreamRequest#beforeSend() - Exiting")
    return aMessage.Payload
  }
}
