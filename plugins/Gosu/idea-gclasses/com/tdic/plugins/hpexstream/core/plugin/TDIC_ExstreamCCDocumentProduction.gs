/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 */
package com.tdic.plugins.hpexstream.core.plugin

uses com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateIds
uses com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateidToDocPubIdMap
uses com.tdic.plugins.hpexstream.core.bo.commandcenter.TDIC_CommandCenterActionRequest
uses com.tdic.plugins.hpexstream.core.bo.commandcenter.TDIC_CommandCenterDCRequest
uses com.tdic.plugins.hpexstream.core.util.TDIC_HelperFactory
uses com.tdic.plugins.hpexstream.core.xml.TDIC_ExstreamXMLCreatorFactory
uses com.tdic.util.properties.PropertyUtil
uses gw.api.upgrade.Coercions
uses gw.api.util.DisplayableException
uses gw.document.DocumentContentsInfo
uses gw.document.SimpleSymbol
uses gw.plugin.document.IDocumentProduction
uses gw.plugin.document.IDocumentTemplateDescriptor
uses gw.util.Base64Util
uses org.slf4j.LoggerFactory

/**
 * This class is implementation of IDocumentProduction interface for HP Exstream Command Center integration.
 * It provides synchronous integration via Command Center's Data Channels interface, asynchronous document creation is performed via Command Center's Action API SOAP interface.
 * Required metadata are retrieved from provided IDocumentTemplateDescriptor.
 * <br>
 * <b>Note:</b><i>
 * Currently the synchronous integration supports composition of only one document, if more than one document is retrieved these documents are disregarded.
 * </i>
 *
 * <br>
 * Please modify or further extend this class to meet your needs.
 * <b>Note:</b><i>This class is not complete and is not meant for production use.</i>
 *
 * @author Miro Kubicek, HP Exstream
 * TODO: US669, shanem, 03/24/2015: 'CC' in the class name should be removed, easily confused with ClaimCenter
 */
class TDIC_ExstreamCCDocumentProduction implements IDocumentProduction {
  private var dataChannelAPIManager = new TDIC_CommandCenterDCManager ()
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  private static var _onDemandJobDefName: String
  construct() {
  }

  /**
   * US669
   * 03/24/2015 Shane Murphy
   *
   * Called in the preload.
   * Sets the HP Job Definition Id for current environment and application once using the IntDb.
   * No access to IDocumentProduction plugin parameters from this class.
   */
  static function init() {
    _onDemandJobDefName = PropertyUtil.getInstance().getProperty("ODJobDefName")+"-"+gw.api.system.server.ServerUtil.getEnv().toUpperCase()
  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   */
  @Param("aTemplateDescriptor", "The descriptor which should be used to create the new document")
  @Param("aParamMap", "A map of name-> object pairs, used to hold root entity for XML generation")
  override function createDocumentSynchronously(aTemplateDescriptor: IDocumentTemplateDescriptor, aParamMap: Map<Object, Object>): DocumentContentsInfo {
    _logger.debug("TDIC_ExstreamCCDocumentProduction#createDocumentSynchronously(IDocumentTemplateDescriptor, Map<Object,Object>)) - Entering")
    _logger.debug("TDIC_ExstreamCCDocumentProduction#createDocumentSynchronously(IDocumentTemplateDescriptor, Map<Object,Object>)) - Exiting")
    return createDocumentSynchronously(aTemplateDescriptor, aParamMap, null)
  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   *
   * Driver Class - The root object to use. Will later be set to the XCenter's default class if null, can be overridden in metadata.
   * Driver Schema - As above, but related to the GX Model.
   * Driver Name - The name of the driver file that wil be created on HP Exstream to store the payload XML
   * DC Name - The Data channel to use on the HP Exstream server.
   *
   * Get the XML Creator for this XCenter, create the xml for the template.
   * If the document template specifies this document is editable:
   * Create the request object and call the HP Exstream Command Center getDocument Method to produce the document.
   * HP Returns a list of DCDocumentResponse objects which contains the document name and content.
   * Else:
   * We treat the document as a batch document and store it's payload for sending when a batch job is run.
   */
  @Param("aTemplateDescriptor", "The descriptor which should be used to create the new document")
  @Param("aParamMap", "A map of name-> object pairs, used to hold root entity for XML generation")
  @Param("aDocument", "The document metadata set up so far")
  @Returns("DocumentContentsInfo object which contains the results of the document creation.")
  override function createDocumentSynchronously(aTemplateDescriptor: IDocumentTemplateDescriptor, aParamMap: Map<Object, Object>, aDocument: Document): DocumentContentsInfo {
    _logger.debug("TDIC_ExstreamCCDocumentProduction#createDocumentSynchronously(IDocumentTemplateDescriptor, Map<Object,Object>, Document)) - Entering")
    if(aTemplateDescriptor.TemplateId == null){
      throw new DisplayableException("The template Id can not be null.")
    }
    var immediateCreation = Coercions.makeBooleanFrom(aTemplateDescriptor.getMetadataPropertyValue("immediatecreation"))
    var editable = Coercions.makeBooleanFrom(aTemplateDescriptor.getMetadataPropertyValue("Editable_TDIC"))
    if (!immediateCreation) {
      throw new DisplayableException("The selected template is not available for immediate creation.")
    } else {
      if (aDocument.Editable_TDIC or editable) {
        if(_onDemandJobDefName != null){
          return handleEditableOnDemand(aTemplateDescriptor, aParamMap)
        } else {
          throw new DisplayableException("Cannot create document - Job Definition Name is not specified for the current Product or Environment")
        }
      } else {
        return handleOnDemandAsBatch(aTemplateDescriptor, aParamMap)
      }
    }
  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   *
   * Synchronous Creation is supported for on demand documents
   */
  override function synchronousCreationSupported(aTemplateDescriptor: IDocumentTemplateDescriptor): boolean {
    _logger.debug("TDIC_ExstreamCCDocumentProduction#synchronousCreationSupported(IDocumentTemplateDescriptor) - Entering")
    _logger.debug("TDIC_ExstreamCCDocumentProduction#synchronousCreationSupported(IDocumentTemplateDescriptor) - Exiting")
    return true
  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   */
  override function asynchronousCreationSupported(aTemplateDescriptor: IDocumentTemplateDescriptor): boolean {
    _logger.debug("TDIC_ExstreamCCDocumentProduction#asynchronousCreationSupported(IDocumentTemplateDescriptor) - Entering")
    _logger.debug("TDIC_ExstreamCCDocumentProduction#asynchronousCreationSupported(IDocumentTemplateDescriptor) - Exiting")
    return true
  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   */
  override function createDocumentAsynchronously(aTemplateDescriptor: IDocumentTemplateDescriptor, aParamMap: Map<Object, Object>): String {
    _logger.debug("TDIC_ExstreamCCDocumentProduction#createDocumentAsynchronously(IDocumentTemplateDescriptorm, Map<Object,Object>) - Entering")
    _logger.debug("TDIC_ExstreamCCDocumentProduction#createDocumentAsynchronously(IDocumentTemplateDescriptor, Map<Object,Object>) - Exiting")
    return createDocumentAsynchronously(aTemplateDescriptor, aParamMap, null)
  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   */
  @Param("aTemplateDescriptor", "The descriptor which should be used to create the new document")
  @Param("aParamMap", "Any objects which may be required for document creation (for context object values, for example)")
  @Param("aParamMap2", " A name->value Map of values which should be set in the new Document's metadata, once it is created")
  override function createDocumentAsynchronously(aTemplateDescriptor: IDocumentTemplateDescriptor, aParamMap: Map<Object, Object>, aParamMap2: Map<Object, Object>): String {
    _logger.debug("TDIC_ExstreamCCDocumentProduction#createDocumentAsynchronously(IDocumentTemplateDescriptorm, Map<Object,Object>, Map<Object,Object>) - Entering")
    _logger.debug("TDIC_ExstreamCCDocumentProduction#createDocumentAsynchronously(IDocumentTemplateDescriptorm, Map<Object,Object>, Map<Object,Object>) - Template used is: " + aTemplateDescriptor.TemplateId)
    var actionRequest = new TDIC_CommandCenterActionRequest((aTemplateDescriptor.getMetadataPropertyValue("hpxjobdefid") as java.lang.String),
        aTemplateDescriptor.getMetadataPropertyValue("hpxjobdefname") as java.lang.String,
        aTemplateDescriptor.getMetadataPropertyValue("hpxdrivername") as java.lang.String,
        aParamMap.get("driver") as java.lang.String)
    _logger.debug("TDIC_ExstreamCCDocumentProduction#createDocumentAsynchronously(IDocumentTemplateDescriptor, Map<Object,Object>, Map<Object,Object>) - Exiting")
    return dataChannelAPIManager.createCCJob(actionRequest)
  }

  /**
   * US669
   * 02/17/2015 Shane Murphy
   *
   * Sends a request to HP Exstream Command Center to generate the document
   */
  @Param("aTemplateDescriptor", "The descriptor which should be used to create the new document")
  @Param("aParamMap", "A map of name-> object pairs, used to hold root entity for XML generation")
  @Returns("DocumentContentInfo for the contents of the document to open in the UI to edit")
  function handleEditableOnDemand(aTemplateDescriptor: IDocumentTemplateDescriptor, aParamMap: Map<Object, Object>): DocumentContentsInfo {
    _logger.debug("TDIC_ExstreamCCDocumentProduction#handleEditableOnDemand(IDocumentTemplateDescriptor, Map<Object,Object>, Document)) - Entering")
    var dcRequest = getRequestObject(aTemplateDescriptor, aParamMap)
    var dcResponse = dataChannelAPIManager.generateDocument(dcRequest)
    //US555: 02/18/2015, shanem: Only a one document is expected in sync - handling of multiple documents is not supported
    var documentInputStream = dcResponse?.DocumentList?.get(0)?.DocumentInputStream
    _logger.debug("TDIC_ExstreamCCDocumentProduction#handleEditableOnDemand(IDocumentTemplateDescriptor, Map<Object,Object>, Document)) - Exiting")
    // TODO Commented out as part of upgrade, plugin implementation jar has been changed.
    return new DocumentContentsInfo(DocumentContentsInfo.ContentResponseType.DOCUMENT_CONTENTS, documentInputStream, aTemplateDescriptor.MimeType)
    //return new DocumentContentsInfo(null, documentInputStream, aTemplateDescriptor.MimeType)
  }

  /**
   * US669
   * 02/18/2015 Shane Murphy
   *
   * Creates a Request object
   */
  @Param("aTemplateDescriptor", "The descriptor which should be used to create the new document")
  @Param("aParamMap", "A map of name-> object pairs, used to hold root entity for XML generation")
  @Returns("A request object")
  function getRequestObject(aTemplateDescriptor: IDocumentTemplateDescriptor, aParamMap: Map<Object, Object>): TDIC_CommandCenterDCRequest {
    var templateId = {new TDIC_TemplateidToDocPubIdMap(aTemplateDescriptor.TemplateId, null)}
    var templateArray = new ArrayList<String>()
    templateArray.add(aTemplateDescriptor.TemplateId)
    var templateMap = new TDIC_TemplateIds(templateArray?.toTypedArray(), templateId?.toTypedArray())
    var driverClass = aTemplateDescriptor.getMetadataPropertyValue("hpxdriverclass") as java.lang.String
    var driverSchema = aTemplateDescriptor.getMetadataPropertyValue("hpxdriverschema") as java.lang.String
    var xmlCreator = TDIC_ExstreamXMLCreatorFactory.getExstreamXMLCreator(aParamMap)
    var dataXml = xmlCreator.generateTransactionXML(templateMap, null /*eventName*/, false/*systemGenerated*/, driverClass, driverSchema)
    var dcRequest = new TDIC_CommandCenterDCRequest()
    dcRequest.DataFiles.put(aTemplateDescriptor.getMetadataPropertyValue("hpxdrivername") as java.lang.String, Base64Util.encode(dataXml.getBytes()))
    dcRequest.ChannelName = aTemplateDescriptor.getMetadataPropertyValue("hpxdcname") as java.lang.String
    dcRequest.JobDefinitionName = _onDemandJobDefName
    _logger.debug("TDIC_ExstreamCCDocumentProduction#handleEditableOnDemand(IDocumentTemplateDescriptor, Map<Object,Object>, Document)) - Exiting")
    return dcRequest
  }

 /**
   * US669
   * 02/17/2015 Shane Murphy
   *
   * Non Editable Documents are handled as though they were batch documents.
   *
   * Create the document stub for the current user. This triggers the DocumentAdded event, which
   * is picked up by an event fired rule if it's an on-demand document, it creates the message
   * for the ExstreamTransport plugin which finally stores the payload for batch production.
   */
  @Param("aTemplateDescriptor", "The descriptor which should be used to create the new document")
  @Param("aParamMap", "A map of name-> object pairs, used to hold root entity for XML generation")
  @Returns("Null, because we don't want these documents to open in the UI as they will not be found")
  function handleOnDemandAsBatch(aTemplateDescriptor: IDocumentTemplateDescriptor, aParamMap: Map<Object, Object>): DocumentContentsInfo {
    _logger.debug("TDIC_ExstreamCCDocumentProduction#handleOnDemandAsBatch(IDocumentTemplateDescriptor, Map<Object,Object>, Document)) - Entering")
    var claim = (aParamMap.get(("Claim")) typeis SimpleSymbol)?(aParamMap.get("Claim") as SimpleSymbol)?.Value as Claim : aParamMap.get("Claim") as Claim
    var entity = claim
    var user = gw.plugin.util.CurrentUserUtil.getCurrentUser().User.DisplayName
    var xcHelper = TDIC_HelperFactory.getXCenterHelper(aParamMap)
    gw.transaction.Transaction.runWithNewBundle(\newBundle -> {
      if(Coercions.makePBooleanFrom(aTemplateDescriptor.getMetadataPropertyValue("SkipStubCreation"))) {
        xcHelper.skipDocCreation(aTemplateDescriptor.TemplateId, entity, newBundle)
        _logger.debug("TDIC_ExstreamCCDocumentProduction#handleOnDemandAsBatch(IDocumentTemplateDescriptor, Map<Object,Object>, Document)) - Document Stub creation skipped because of template descriptor meta data value")
      } else {
        xcHelper.createStub(aTemplateDescriptor.TemplateId, 1, entity, "OnDemand", user)
      }
    })
    _logger.debug("TDIC_ExstreamCCDocumentProduction#handleOnDemandAsBatch(IDocumentTemplateDescriptor, Map<Object,Object>, Document)) - ~Exiting")
    return null
  }
}
