/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 */
package com.tdic.plugins.hpexstream.core.plugin

uses gw.pl.logging.LoggerCategory
uses com.tdic.plugins.hpexstream.core.bo.commandcenter.TDIC_CommandCenterActionRequest
uses com.tdic.plugins.hpexstream.core.bo.commandcenter.TDIC_CommandCenterDCRequest
uses com.tdic.plugins.hpexstream.core.bo.commandcenter.TDIC_CommandCenterDCResponse
uses com.tdic.plugins.hpexstream.core.bo.commandcenter.TDIC_DCResponseDocument
uses java.lang.Exception
uses com.tdic.plugins.hpexstream.core.util.TDIC_ExstreamHelper
uses org.slf4j.LoggerFactory

/**
 * Service layer implementation of HP Exstream Command Center integration via Data Channels and Action API.
 * By default only synchronous and asynchronous CC job creation (via Data Channels and Action API) is implemented.
 * This class can be further extended to implement additional service calls to Command Center. 
 * Also separate Manager classes can be created for Data Channels and Action API respectively, so as these are decoupled and modular. 
 *
 * <br>
 * Please modify or further extend this class to meet your needs.
 * <b>Note:</b><i>This class is not complete and is not meant for production use.</i>
 *
 * @author Miro Kubicek, HP Exstream
 */
class TDIC_CommandCenterDCManager {
  private var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  construct() {
  }

  /**
   * US555
   * 10/13/2014 shanem
   *
   * Invokes a call to CommandCenter Data Channel API "getDocument" to generate a document.
   */
  @Param("aRequest", "A data transfer object containing information required by DC interface")
  @Returns("CommandCenterDCResponse that contains names and content of documents that were generated")
  function generateDocument(aRequest: TDIC_CommandCenterDCRequest): TDIC_CommandCenterDCResponse {
    _logger.debug("TDIC_CommandCenterDCManager#generateDocument(TDIC_CommandCenterDCRequest) - Entering")
    var requestStr = aRequest.parse()
    _logger.debug("TDIC_CommandCenterDCManager#generateDocument(TDIC_CommandCenterDCRequest) - Request XML: \n " + requestStr)
    var dcResponse: TDIC_CommandCenterDCResponse
    //var response = new com.tdic.plugins.hpexstream.core.webservice.commandcenter.soapgateway.SOAPGatewayInterfaceService().getDocument(requestStr)
    var response = TDIC_ExstreamHelper.getODDocProdService().getDocument(requestStr)
    if (response != null) {
      _logger.debug("TDIC_CommandCenterDCManager#generateDocument(TDIC_CommandCenterDCRequest) - Processing response: " + response.toString())
      _logger.debug("TDIC_CommandCenterDCManager#generateDocument(TDIC_CommandCenterDCRequest) - Response contains binary data of size: " + response.Document.Bytes.length)
      dcResponse = new TDIC_CommandCenterDCResponse()
      //multiple documents are not supported by Guidewire for synchronous composition, so grabbing first document is enough: 
      dcResponse.DocumentList.add(new TDIC_DCResponseDocument(response.DocumentName, response.Document.InputStream))
    }
    _logger.debug("TDIC_CommandCenterDCManager#generateDocument(TDIC_CommandCenterDCRequest) - Exiting")
    return dcResponse
  }

  /**
   * US555
   * 10/13/2014 shanem
   *
   * Invokes a call to CommandCenter Action API "CREATE_JOB" to start a new job, does not wait
   * for the job to finish hence it is suitable for asynchronous operations.
   */
  @Param("aRequest", "A transfer object containing information required by Command Center Action api")
  @Returns("ID of a Command Center job that was triggered")
  function createCCJob(request: TDIC_CommandCenterActionRequest): String {
    _logger.debug("TDIC_CommandCenterDCManager#createCCJob(TDIC_CommandCenterActionRequest) - Entering")
    var response = TDIC_ExstreamHelper.getDocProdService().action("CREATE_JOB", request.getXMLModelAttributes())
    _logger.debug("Processing Command Center Action response: " + response.asUTFString())
    var jobId: String
    var status: String
    if (response == null) {
      throw new Exception ("Error communication with Command Center Action API: response is: " + response.asUTFString())
    }
    for (attribute in response.Attribute) {
      if (attribute.Name == "job_id"){
        jobId = attribute.Value_Attribute
      }
      if (attribute.Name == "status_detail"){
        status = attribute.Value_Attribute
      }
    }
    _logger.debug("TDIC_CommandCenterDCManager#createCCJob(TDIC_CommandCenterActionRequest) - Command Center response status: " + status)
    _logger.debug("TDIC_CommandCenterDCManager#createCCJob(TDIC_CommandCenterActionRequest) - Command Center job ID: " + jobId)
    if (status != "OK") {
      throw new Exception ("Error communication with Command Center Action API: response is: " + response.asUTFString())
    }
    _logger.debug("TDIC_CommandCenterDCManager#createCCJob(TDIC_CommandCenterActionRequest) - Exiting")
    return jobId
  }
}
