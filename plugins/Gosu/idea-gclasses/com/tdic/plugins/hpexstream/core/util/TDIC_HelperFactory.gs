package com.tdic.plugins.hpexstream.core.util

uses gw.api.system.PLLoggerCategory
uses gw.lang.reflect.ReflectUtil
uses java.lang.Class
uses java.lang.ClassNotFoundException
uses java.lang.RuntimeException
uses java.util.Map
uses org.slf4j.LoggerFactory

class TDIC_HelperFactory {
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  static function getXCenterHelper(aParamMap: Map<Object, Object>): TDIC_Helper {
    final var PC_CREATOR = "tdic.plugin.hpexstream.pc.util.TDIC_PCExstreamHelper"
    final var BC_CREATOR = "tdic.plugin.hpexstream.bc.util.TDIC_BCExstreamHelper"
    final var CC_CREATOR = "tdic.cc.integ.plugins.hpexstream.util.TDIC_CCExstreamHelper"
    var helper: TDIC_Helper
    var polPeriod = aParamMap.get("policyPeriod")
    var anAccount = aParamMap.get("account")
    var claim = aParamMap.get("Claim")
    if (polPeriod != null) {
      _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - PolicyCenter")
      helper = ReflectUtil.construct(PC_CREATOR, new Object[]{})
    } else {
      if (anAccount != null) {
        try {
          // 08/27/2014, shanem, US656: Check if class exists, if it does, we are in billingcenter
          Class.forName(BC_CREATOR)
          _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - BillingCenter")
          helper = ReflectUtil.construct(BC_CREATOR, new Object[]{})
        } catch (var e: ClassNotFoundException) {
          // 08/27/2014, shanem, US656: else, we are in policycenter
          _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - PolicyCenter")
          helper = ReflectUtil.construct(PC_CREATOR, new Object[]{})
        }
      } else {
        if (claim != null) {
          _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - ClaimCenter")
          helper = ReflectUtil.construct(CC_CREATOR, new Object[]{})
        } else {
          throw new RuntimeException("Incorrect entity supplied...")
        }
      }
    }

    _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - Exiting")
    return helper
  }
}