/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P.
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package com.tdic.plugins.hpexstream.core.messaging

uses com.tdic.util.properties.PropertyUtil
uses gw.plugin.InitializablePlugin
uses gw.plugin.Plugins
uses gw.plugin.document.IDocumentMetadataSource
uses gw.plugin.document.IDocumentTemplateDescriptor
uses gw.plugin.document.IDocumentTemplateSource
uses gw.xml.ws.WebServiceException
uses com.tdic.plugins.hpexstream.core.bo.commandcenter.TDIC_CommandCenterActionRequest
uses com.tdic.plugins.hpexstream.core.util.TDIC_DocumentEventSender
uses java.lang.Exception
uses java.util.Map
uses tdic.cc.integ.plugins.message.TDIC_MessagePlugin
uses gw.xml.XmlElement
uses com.tdic.plugins.hpexstream.core.util.TDIC_ExstreamHelper
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory

/**
 * US555
 * 02/16/2015 Shane Murphy
 *
 * Message Transport class responsible for automated document production.
 * Uses <b>HP Exstream Command Center</b> on-demand and batch document creation.
 *
 * In the case of Batch creation, a call to Command Center API is not made directly, the composition payload data is
 * generated and stored in an intermediary staging area in the integration database ExstreamPayload table
 * The actual call to the command Center API is performed once an independent batch process is run and
 * doWork() method is triggered ( see {@link tdic.cc.integ.plugins.hpexstream.batch.TDIC_DocumentCreationBatch#doWork()}),
 * it extracts payload data from this staging area and sends them in a SOAP request to HP Exstream Command Center to
 * start a composition job.
 */
 class TDIC_ExstreamTransport extends TDIC_MessagePlugin implements InitializablePlugin {
  private static var _payloadFilePath: String as PayloadFilePath
  private static final var _ccJobDefName:String as CCJobDefName = PropertyUtil.getInstance().getProperty("ccJobDefName")+"-"+gw.api.system.server.ServerUtil.getEnv().toUpperCase()
  private static final var _finalizationJobDefName:String as FinalizationJobDefName = PropertyUtil.getInstance().getProperty("finalizationJobDefName")+"-"+gw.api.system.server.ServerUtil.getEnv().toUpperCase()
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  /**
   * US555
   * 10/13/2014 Shane Murphy
   *
   * Send message to HP Exstream containing the output XML which includes
   * all data required and list of documents to create for the event.
   */
  override function send(aMessage: Message, aPayload: String): void {
    _logger.trace("TDIC_ExstreamTransport#send(Message, String) - Entering.")
    _logger.trace("TDIC_ExstreamTransport#send(Message, String) - Event: " + aMessage.EventName)
    _logger.trace("TDIC_ExstreamTransport#send(Message, String) - aMessage.Payload: " + aMessage.Payload)

    /*
     To get the TemplateId, we can parse the XML payload and get the first templateId.
     To avoid using XCenter specific GX Model references here, we use XMLElement

     We can take the first template as all templates in the list will have the same meta data for the entry we're interested in.
    */
    var templateId: String
    if(aMessage.EventName == "FinalizeExstreamDocument") {
      templateId = (aMessage.MessageRoot as Document).TemplateId_TDIC
    } else {
      var parsedPayload = XmlElement.parse(aMessage.Payload)
      templateId = parsedPayload.$Children.where( \ elt -> elt.$QName.LocalPart == "TemplateIds").first()?.$Children?.first()?.$Children?.first()?.$SimpleValue?.StringValue
    }

    var templateDescriptor = getTemplateDescriptor(templateId)
    var _editable = false
    if (templateDescriptor != null) {
      _editable = Coercions.makeBooleanFrom(templateDescriptor.getMetadataPropertyValue("Editable_TDIC"))
    }
    _logger.debug("TDIC_ExstreamTransport#send(Message, String) - Editable document: " + _editable)
    if (_editable == true) {
      handleEditableDocument(aMessage)
    } else {
      if (_editable == false) {
        sendBatchDocument(aMessage)
      } else {
     //   if (_editable == null){
          _logger.debug("TDIC_ExstreamTransport#send(Message, String) - Immediate Creation metadata attribute in descriptor not specified for Exstream template.")
          throw new com.guidewire.pl.web.controller.UserDisplayableException ("Immediate Creation metadata attribute in descriptor not specified for Exstream template.")
     //   }
      }
    }
    try{
      if (aMessage.MessageRoot != null && aMessage.MessageRoot typeis Document) {
        updateDocumentMetadata(aMessage.MessageRoot)
      }
    } catch (var e: Exception) {
      _logger.error("TDIC_ExstreamTransport#sendBatchDocument(Message) -  Exception" + e.Message)
    }
    _logger.trace("TDIC_ExstreamTransport#send(Message, String) - Exiting.")
  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   */
  override function resume(): void {
    _logger.trace("TDIC_ExstreamTransport#resume() - Entering.")
    // Do Nothing
    _logger.trace("TDIC_ExstreamTransport#resume() - Exiting.")
  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   */
  override function shutdown(): void {
    _logger.trace("TDIC_ExstreamTransport#shutdown() - Entering.")
    // Do Nothing
    _logger.trace("TDIC_ExstreamTransport#shutdown() - Exiting.")
  }

  /**
   * US555
   * 10/13/2014 Shane Murphy
   *
   * Reading the Parameter from the Plugin Directory
   */
 /* override function setParameters(aParamMap: Map<Object, Object>): void {
    _ccJobDefName = aParamMap["ccJobDefName"] as String
    _logger.debug("TDIC_ExstreamTransport#setParameters(Map<Object, Object>) - value of parameter ccJobDefName: " + _ccJobDefName)
    _finalizationJobDefName = aParamMap["finalizationJobDefName"] as String
    _logger.debug("TDIC_ExstreamTransport#setParameters(Map<Object, Object>) - Param value finalizationJobDefName: " + _finalizationJobDefName)
  } */

  override property set Parameters(map : Map) {

  }

  override property set DestinationID(destinationID : int) {

  }

  /**
   * US555
   * 01/26/2015 Shane Murphy
   *
   * Writes payload data to the integration database.
   */
  @Param("aPayload", "Payload to store in the database")
  protected function sendBatchDocument(aMessage: Message): void {
    _logger.trace("TDIC_ExstreamTransport#sendBatchDocument(Message) - Entering.")
    try {
      if (TDIC_DocumentEventSender.sendMessage(aMessage.Payload) < 1) {
        _logger.error("TDIC_ExstreamTransport#sendBatchDocument(Message) - Could not send Payload to Exstream.")
        reportError(aMessage, "Could not send Payload to Exstream")
      } else {
        aMessage.reportAck()
      }
    } catch (var wse: WebServiceException) {
      _logger.error("TDIC_ExstreamTransport#sendBatchDocument(Message) -  WebServiceException" + wse.Message,wse)
      reportError(aMessage, wse.Message)

    } catch (var e: Exception) {
      _logger.error("TDIC_ExstreamTransport#sendBatchDocument(Message) -  Exception" + e.Message, e)
      reportError(aMessage, e.Message)

    }
    _logger.trace("TDIC_ExstreamTransport#sendBatchDocument(Message) - Exiting.")
  }

  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * If the document is editable we send the request to HP Exstream to generate the document immediately.
   */
  @Param("anEventName", "The data type is: String")
  @Param("aPayload", "The data type is: String")
  protected function handleEditableDocument(aMessage: Message) {
    _logger.trace("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Entering.")
    var requestToCmd = getRequestObject(aMessage.Payload, aMessage.EventName)
    _logger.trace("TDIC_ExstreamTransport#handleEditableDocument(String, String) - aMessage.Payload: " + aMessage.Payload)
    _logger.trace("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Request: " + requestToCmd.getXMLModelAttributes())
    try {
      var actionInterface = TDIC_ExstreamHelper.getDocProdService()
      _logger.debug("Request Payload: ")
      requestToCmd.getXMLModelAttributes().each(\elt -> print(elt.asUTFString()))
      var responseFromCMD = actionInterface.action("CREATE_JOB", requestToCmd.getXMLModelAttributes())
      _logger.debug("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Response from Command Center" + responseFromCMD.asUTFString())
      if (responseFromCMD != null and responseFromCMD.Attribute.where(\attrib -> (attrib.Name == "status_code" and attrib.Value_Attribute == "1001")).Empty == false) {
        aMessage.reportAck()
        _logger.debug("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Successfully consumed")
      } else {
        aMessage.reportError()
      }
    } catch (var responseException: Exception) {
      _logger.error("TDIC_DocumentCreationBatch#handleEditableDocument(String, String) - Exception in Response from Command Center " + responseException.toString(),responseException)
      aMessage.reportError()
    }
    _logger.trace("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Exiting")
  }

  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * Depending on the event, Create a finalization or batch request object for HP's Command Center
   */
  @Param("aPayload", "XML Payload")
  @Param("anEventName", "The Event Name")
  @Returns("request object for the event")
  function getRequestObject(aPayload: String, anEventName: String): TDIC_CommandCenterActionRequest {
    _logger.trace("TDIC_ExstreamTransport#getRequestObject(String, String) - Entering")
    var reqXMLinBase64 = gw.util.Base64Util.encode(aPayload.Bytes)
    _logger.debug("TDIC_ExstreamTransport#getRequestObject(String, String) - Payload for ${anEventName} request: " + aPayload)
    var requestToCmd: TDIC_CommandCenterActionRequest
    if (anEventName == "FinalizeExstreamDocument") {
      requestToCmd = new TDIC_CommandCenterActionRequest(null /*aJobDefId*/, FinalizationJobDefName /*aJobDefName*/, "DRIVERFILE" /*aDriverName*/, reqXMLinBase64 /*anEncodedDriverContent*/)
    } else {
      requestToCmd = new TDIC_CommandCenterActionRequest(null /*aJobDefId*/, CCJobDefName /*aJobDefName*/, "DRIVERFILE" /*aDriverName*/, reqXMLinBase64 /*anEncodedDriverContent*/)
    }
    _logger.debug("TDIC_ExstreamTransport#handleEditableDocument(String, String) - Request to Command Center")
    requestToCmd.getXMLModelAttributes().each(\elt -> _logger.debug(elt.asUTFString()))
    _logger.trace("TDIC_ExstreamTransport#getRequestObject(String, String) - Exiting")
    return requestToCmd
  }

  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * Update the document's metadata after sending to HP Exstream
   */
  @Param("aDocument", "The document to update")
  function updateDocumentMetadata(aDocument: Document) {
    _logger.trace("TDIC_ExstreamTransport#updateDocumentMetadata(Document) - Entering")
    aDocument.DeliveryStatus_TDIC = DocDlvryStatusType_TDIC.TC_SENT
    var dms = Plugins.get(IDocumentMetadataSource)
    dms.saveDocument(aDocument)
    _logger.trace("TDIC_ExstreamTransport#updateDocumentMetadata(Document) - Exiting")
  }

  function getTemplateDescriptor(aDocTemplateId: String): IDocumentTemplateDescriptor {
    if (aDocTemplateId == null) {
      return null
    } else {
      var dts = Plugins.get(IDocumentTemplateSource)
      var templateDescriptor = dts.getDocumentTemplate(aDocTemplateId, null)
      _logger.debug("TDIC_ExsreamTransport#getTemplateDescriptor(Message, String) - Template Descriptor " + templateDescriptor.TemplateId
          + " retrieved for document with Template Id: " + aDocTemplateId)
      return templateDescriptor
    }
  }

  /**
   * US556
   * 03/27/2015 Shane Murphy
   *
   * This method changes the wait time while retry
   * For each retry time varies.
   */
  function reportError(aMessage: Message, errorMessage: String) {
    try{
      aMessage.ErrorDescription = errorMessage
    }catch(e: Exception){
      aMessage.ErrorDescription = "Unable to Update ErrorMessage"
      _logger.error("Unable to update ErrorMessage: ${aMessage}",e)
    }
    aMessage.reportError()
  }
}