package com.tdic.plugins.hpexstream.core.util

uses com.tdic.util.properties.exception.LoadingPropertyFileException
uses com.tdic.util.properties.exception.PropertyNotFoundException
uses gw.api.upgrade.Coercions
uses gw.api.util.ConfigAccess
uses gw.pl.logging.LoggerCategory
uses org.apache.commons.collections.MultiHashMap
uses org.apache.commons.collections.MultiMap
uses org.apache.commons.lang.ClassUtils
uses org.slf4j.Logger

uses java.io.FileInputStream
uses java.io.IOException

class TDIC_ExstreamPropertyUtil {
  private final var className = ClassUtils.getShortClassName(getClass())
  private var logger: Logger
  private static var uniqueInstance: TDIC_ExstreamPropertyUtil
  private static var PROP_FILE_PATH_NAME: String = getExstreaPropertiesFilePath()
  private var exstreamEventProperties: Properties
  private static var exstreamEventTemplateMappings: MultiMap

  private construct() {
    var myMethodName = "TDIC_ExstreamPropertyUtil";
    logger = LoggerCategory.TDIC_INTEGRATION
    logger.info(className + ":" + myMethodName + ":Constructing a new PropertyUtil object")
    try {
      exstreamEventProperties = new Properties()
      exstreamEventProperties.load(new FileInputStream(getExstreaPropertiesFilePath()))
      loadExstreamMapCache()
    } catch (e: IOException) {
      logger.error(className + ":" + myMethodName + ":" + e.getMessage())
      throw new LoadingPropertyFileException(PROP_FILE_PATH_NAME)
    }
  }

  public static function getInstance(): TDIC_ExstreamPropertyUtil {
    if (uniqueInstance == null) {
      uniqueInstance = new TDIC_ExstreamPropertyUtil()
    }
    return uniqueInstance
  }

  public function getEventTemplateMappings(event: String):List<ExstreamMapParam>{
    return exstreamEventTemplateMappings.get(event) as List<ExstreamMapParam>
  }

  private static function getExstreaPropertiesFilePath(): String{
    var propFilePath: String
    var fileLocation = "gsrc/tdic/cc/integ/plugins/hpexstream/exstream-eventtemplates.properties"
    propFilePath = ConfigAccess.getConfigFile(fileLocation).Path
    return propFilePath
  }

  private function getExstreamEventProperty(key: String): String {
    var myMethodName = "getExstreamEventProperty"
    logger.debug(className + " : " + myMethodName + " : Getting ExstreamEventProperty value for key: " + key)
    var propertyValue: String = null
    try {
      propertyValue = new String(exstreamEventProperties.getProperty(key, null))
    }
    catch (e: Exception) {
      logger.error(className + ":" + myMethodName + ":KEY:" + key + ":" + e)
      throw new PropertyNotFoundException(key, e)
    }
    return propertyValue == null ? null : propertyValue.trim()
  }

  private function loadExstreamMapCache() {
    var myMethodName = "loadExstreamMapCache"

    exstreamEventTemplateMappings = new MultiHashMap()
    var exstreamEvents = getExstreamEventProperty("Exstream.Events")
    var templateIds : String
    for (event in exstreamEvents.split(",")) {
      templateIds = getExstreamEventProperty("${event}.TemplateIds")
      for (templateId in templateIds?.split(",")) {
        var _exstreamMapParam = new ExstreamMapParam()
        _exstreamMapParam.TemplateId = templateId
        _exstreamMapParam.EventName = event

        _exstreamMapParam.EffectiveDate = Coercions.makeDateFrom(getExstreamEventProperty("${templateId}.EffectiveDate"))
        _exstreamMapParam.ExpirationDate = Coercions.makeDateFrom(getExstreamEventProperty("${templateId}.ExpirationDate"))
        _exstreamMapParam.PrintOrder = Coercions.makeIntFrom(getExstreamEventProperty("${templateId}.PrintOrder"))
        _exstreamMapParam.DocType = getExstreamEventProperty("${templateId}.DocType")
        exstreamEventTemplateMappings.put(_exstreamMapParam.EventName, _exstreamMapParam)
      }
    }
  }

}