/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 */
package com.tdic.plugins.hpexstream.core.plugin

uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.util.DateUtil
uses gw.pl.logging.LoggerCategory
uses gw.transaction.Transaction
uses gw.xml.ws.annotation.WsiPermissions
uses gw.xml.ws.annotation.WsiWebService
uses com.tdic.plugins.hpexstream.core.bo.TDIC_ExstreamResponse
uses gw.plugin.Plugins
uses gw.plugin.document.IDocumentContentSource
uses org.slf4j.LoggerFactory

/**
 * This class exposes Guidewire DMS functionality as a web service endpoint for Command Center to store generated documents.
 * <br><br>
 * Please modify or further extend this class to meet your needs.
 * <b>Note:</b><i>This class is not complete and is not meant for production use.</i>
 */
@WsiPermissions({})
@WsiWebService
class TDIC_CommandCenterDocumentAPI {
  private var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  /**
   * US555
   * 10/13/2014 shanem
   *
   * Stores a document in a DMS using current IDocumentContentSource & IDocumentMetadataSource plugins
   */
  @Param("aCommandCenterResponse", "Response from the Command Center server")
  @Param("aPolicyNumber", "Related Policy Number")
  @Returns("Message indicating if document was stored successfully")
  public function sendDocument(response: TDIC_ExstreamResponse): String {
    _logger.debug("TDIC_CommandCenterDocumentAPI#sendDocument(String, String) - Entering")
    var responseString = "Document Successfully received"
    response.Documents.each(\elt -> {
      _logger.debug("TDIC_CommandCenterDocumentAPI#sendDocument(String, String) - Received URL '" + elt.DocumentUrl + "' for Document '" + elt.DocUID + "'")
      try {
        var document = Query.make(Document).compare(Document#DocUID, Equals, elt.DocUID).select().AtMostOneRow
        if (document == null) {
          _logger.warn("TDIC_CommandCenterDocumentAPI#sendDocument(String, String) - Document with DocUID ${elt.DocUID} not found.")
        } else {
          _logger.debug("TDIC_CommandCenterDocumentAPI#sendDocument(String, String) - Document found - ${document.Name}|satus=${document.Status}")
          Transaction.runWithNewBundle(\bundle -> {
            document = bundle.add(document)
            /**
             * US669
             * 28/05/2015 Shane Murphy
             *
             * DLF document is stored in the DMS by GW using OOTB plugins, when we finalize the request, HP
             * Exstream creates a PDF version of this file. GW needs to delete the original DLF as HP is unaware of it
             *
             * DLF Documents returned from HP Exstream have no Event associated with them, set to OnDemand.
             */
            if (document.Event_TDIC == null || document.Event_TDIC == "") {
              var dcs = Plugins.get(IDocumentContentSource)
              dcs.removeDocument(document)
              document.Event_TDIC = "OnDemand"
            }
            _logger.debug("TDIC_CommandCenterDocumentAPI#sendDocument(String, String) - Updating document meta data")
            /*
            US669
            04/21/2015 Shane Murphy

            N.B. The first 13 characters are stripped off because we must have a non-null value set for documents.path parameter in the IDocumentContentSource
            Using the OOTB plugins, this value must be non-null and must be able to generate an absolute path from the relative path.
             */
            document.DocUID = elt.DocumentUrl.substring(PropertyUtil.getInstance().getProperty("document.path").length())
            document.DMS = true
            document.Status = DocumentStatusType.TC_FINAL
            document.MimeType = "application/pdf"
            document.DateModified = DateUtil.currentDate()
            document.DeliveryStatus_TDIC = DocDlvryStatusType_TDIC.TC_RECEIVED
          }, "su")
          _logger.debug("TDIC_CommandCenterDocumentAPI#sendDocument(String, String) - DocUID ${document.DocUID} updated successfully")
          _logger.debug("TDIC_CommandCenterDocumentAPI#sendDocument(String, String) - document is sucessfully saved: " + responseString)
        }
      } catch (e) {
        _logger.error("TDIC_CommandCenterDocumentAPI#sendDocument(String, String) - error in either receiving or saving document " + e.Message)
        _logger.error("TDIC_CommandCenterDocumentAPI#sendDocument(String, String) - " + e.Cause)
        responseString = "error in either receiving or saving document " + e.Message + " " + e
      }
    })
    _logger.debug("TDIC_CommandCenterDocumentAPI#sendDocument(String, String) - Exiting")

    return responseString
  }
}
