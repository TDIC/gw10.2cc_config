package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/icdtransitionto10/exitpoints/ICDData.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ICDData extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (refUrl :  String) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ICDData, {refUrl}, 0)
  }
  
  static function drilldown (refUrl :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ICDData, {refUrl}, 0).drilldown()
  }
  
  static function printPage (refUrl :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ICDData, {refUrl}, 0).printPage()
  }
  
  static function push (refUrl :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ICDData, {refUrl}, 0).push()
  }
  
  
}