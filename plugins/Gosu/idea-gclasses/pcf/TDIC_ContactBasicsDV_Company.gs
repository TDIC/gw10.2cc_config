package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_ContactBasicsDV.Company.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_ContactBasicsDV_Company extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($contactHandle :  gw.api.contact.ContactHandle, $contactRole :  typekey.ContactRole, $showRoles :  boolean, $linkStatus :  gw.api.contact.ContactSystemLinkStatus, $claim :  Claim) : void {
    __widgetOf(this, pcf.TDIC_ContactBasicsDV_Company, SECTION_WIDGET_CLASS).setVariables(false, {$contactHandle, $contactRole, $showRoles, $linkStatus, $claim})
  }
  
  function refreshVariables ($contactHandle :  gw.api.contact.ContactHandle, $contactRole :  typekey.ContactRole, $showRoles :  boolean, $linkStatus :  gw.api.contact.ContactSystemLinkStatus, $claim :  Claim) : void {
    __widgetOf(this, pcf.TDIC_ContactBasicsDV_Company, SECTION_WIDGET_CLASS).setVariables(true, {$contactHandle, $contactRole, $showRoles, $linkStatus, $claim})
  }
  
  
}