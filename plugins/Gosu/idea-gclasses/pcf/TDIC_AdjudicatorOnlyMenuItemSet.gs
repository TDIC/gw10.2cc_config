package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_AdjudicatorOnlyMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_AdjudicatorOnlyMenuItemSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($claim :  Claim, $adjudicatorName :  String) : void {
    __widgetOf(this, pcf.TDIC_AdjudicatorOnlyMenuItemSet, SECTION_WIDGET_CLASS).setVariables(false, {$claim, $adjudicatorName})
  }
  
  function refreshVariables ($claim :  Claim, $adjudicatorName :  String) : void {
    __widgetOf(this, pcf.TDIC_AdjudicatorOnlyMenuItemSet, SECTION_WIDGET_CLASS).setVariables(true, {$claim, $adjudicatorName})
  }
  
  
}