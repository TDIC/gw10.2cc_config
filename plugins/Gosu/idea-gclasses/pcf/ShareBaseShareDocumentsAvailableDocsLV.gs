package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsAvailableDocsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseShareDocumentsAvailableDocsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($claim :  Claim, $share :  OutboundSharing_Ext) : void {
    __widgetOf(this, pcf.ShareBaseShareDocumentsAvailableDocsLV, SECTION_WIDGET_CLASS).setVariables(false, {$claim, $share})
  }
  
  function refreshVariables ($claim :  Claim, $share :  OutboundSharing_Ext) : void {
    __widgetOf(this, pcf.ShareBaseShareDocumentsAvailableDocsLV, SECTION_WIDGET_CLASS).setVariables(true, {$claim, $share})
  }
  
  
}