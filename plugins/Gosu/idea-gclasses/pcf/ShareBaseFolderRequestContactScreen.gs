package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderRequestContactScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseFolderRequestContactScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($claim :  Claim, $Request :  InboundRequest_Ext) : void {
    __widgetOf(this, pcf.ShareBaseFolderRequestContactScreen, SECTION_WIDGET_CLASS).setVariables(false, {$claim, $Request})
  }
  
  function refreshVariables ($claim :  Claim, $Request :  InboundRequest_Ext) : void {
    __widgetOf(this, pcf.ShareBaseFolderRequestContactScreen, SECTION_WIDGET_CLASS).setVariables(true, {$claim, $Request})
  }
  
  
}