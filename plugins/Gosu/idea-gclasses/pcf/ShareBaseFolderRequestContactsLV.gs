package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderRequestContactsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseFolderRequestContactsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($claim :  Claim, $Contacts :  List<Contact>, $hasCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.ShareBaseFolderRequestContactsLV, SECTION_WIDGET_CLASS).setVariables(false, {$claim, $Contacts, $hasCheckboxes})
  }
  
  function refreshVariables ($claim :  Claim, $Contacts :  List<Contact>, $hasCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.ShareBaseFolderRequestContactsLV, SECTION_WIDGET_CLASS).setVariables(true, {$claim, $Contacts, $hasCheckboxes})
  }
  
  
}