package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseShareDocumentsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($Claim :  Claim, $Folder :  ShareBase_Ext, $Request :  OutboundSharing_Ext) : void {
    __widgetOf(this, pcf.ShareBaseShareDocumentsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$Claim, $Folder, $Request})
  }
  
  function refreshVariables ($Claim :  Claim, $Folder :  ShareBase_Ext, $Request :  OutboundSharing_Ext) : void {
    __widgetOf(this, pcf.ShareBaseShareDocumentsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$Claim, $Folder, $Request})
  }
  
  
}