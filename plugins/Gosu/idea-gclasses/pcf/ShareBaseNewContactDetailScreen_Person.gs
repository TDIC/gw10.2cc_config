package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseNewContactDetailScreen.Person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseNewContactDetailScreen_Person extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($Contact :  Contact) : void {
    __widgetOf(this, pcf.ShareBaseNewContactDetailScreen_Person, SECTION_WIDGET_CLASS).setVariables(false, {$Contact})
  }
  
  function refreshVariables ($Contact :  Contact) : void {
    __widgetOf(this, pcf.ShareBaseNewContactDetailScreen_Person, SECTION_WIDGET_CLASS).setVariables(true, {$Contact})
  }
  
  
}