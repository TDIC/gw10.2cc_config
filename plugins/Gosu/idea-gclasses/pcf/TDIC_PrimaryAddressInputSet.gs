package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_PrimaryAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_PrimaryAddressInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($contactHandle :  gw.api.contact.ContactHandle, $contactRole :  typekey.ContactRole) : void {
    __widgetOf(this, pcf.TDIC_PrimaryAddressInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$contactHandle, $contactRole})
  }
  
  function refreshVariables ($contactHandle :  gw.api.contact.ContactHandle, $contactRole :  typekey.ContactRole) : void {
    __widgetOf(this, pcf.TDIC_PrimaryAddressInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$contactHandle, $contactRole})
  }
  
  
}