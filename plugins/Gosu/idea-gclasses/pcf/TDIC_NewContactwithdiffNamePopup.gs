package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_NewContactwithdiffNamePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_NewContactwithdiffNamePopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (contactSubtype :  typekey.Contact, parentContact :  Contact, claim :  Claim, titlevalue :  String) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_NewContactwithdiffNamePopup, {contactSubtype, parentContact, claim, titlevalue}, 0)
  }
  
  function pickValueAndCommit (value :  Contact) : void {
    __widgetOf(this, pcf.TDIC_NewContactwithdiffNamePopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (contactSubtype :  typekey.Contact, parentContact :  Contact, claim :  Claim, titlevalue :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_NewContactwithdiffNamePopup, {contactSubtype, parentContact, claim, titlevalue}, 0).push()
  }
  
  
}