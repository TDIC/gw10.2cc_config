package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_NewContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_NewContactPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (contactSubtype :  typekey.Contact, parentContact :  Contact, claim :  Claim, contactRole :  typekey.ContactRole) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_NewContactPopup, {contactSubtype, parentContact, claim, contactRole}, 0)
  }
  
  function pickValueAndCommit (value :  Contact) : void {
    __widgetOf(this, pcf.TDIC_NewContactPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (contactSubtype :  typekey.Contact, parentContact :  Contact, claim :  Claim, contactRole :  typekey.ContactRole) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_NewContactPopup, {contactSubtype, parentContact, claim, contactRole}, 0).push()
  }
  
  
}