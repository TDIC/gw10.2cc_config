package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_AttorneyOnlyMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_AttorneyOnlyMenuItemSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($claim :  Claim, $attorneyType :  String) : void {
    __widgetOf(this, pcf.TDIC_AttorneyOnlyMenuItemSet, SECTION_WIDGET_CLASS).setVariables(false, {$claim, $attorneyType})
  }
  
  function refreshVariables ($claim :  Claim, $attorneyType :  String) : void {
    __widgetOf(this, pcf.TDIC_AttorneyOnlyMenuItemSet, SECTION_WIDGET_CLASS).setVariables(true, {$claim, $attorneyType})
  }
  
  
}