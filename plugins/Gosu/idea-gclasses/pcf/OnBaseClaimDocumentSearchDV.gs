package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseClaimDocumentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class OnBaseClaimDocumentSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($DocumentSearchCriteria :  DocumentSearchCriteria, $onbaseThumbnailUtil :  acc.onbase.util.ThumbnailUtil) : void {
    __widgetOf(this, pcf.OnBaseClaimDocumentSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$DocumentSearchCriteria, $onbaseThumbnailUtil})
  }
  
  function refreshVariables ($DocumentSearchCriteria :  DocumentSearchCriteria, $onbaseThumbnailUtil :  acc.onbase.util.ThumbnailUtil) : void {
    __widgetOf(this, pcf.OnBaseClaimDocumentSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$DocumentSearchCriteria, $onbaseThumbnailUtil})
  }
  
  
}