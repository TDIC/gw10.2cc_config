package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderAvailableContactsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseFolderAvailableContactsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($claim :  Claim, $Contacts :  List<Contact>) : void {
    __widgetOf(this, pcf.ShareBaseFolderAvailableContactsLV, SECTION_WIDGET_CLASS).setVariables(false, {$claim, $Contacts})
  }
  
  function refreshVariables ($claim :  Claim, $Contacts :  List<Contact>) : void {
    __widgetOf(this, pcf.ShareBaseFolderAvailableContactsLV, SECTION_WIDGET_CLASS).setVariables(true, {$claim, $Contacts})
  }
  
  
}