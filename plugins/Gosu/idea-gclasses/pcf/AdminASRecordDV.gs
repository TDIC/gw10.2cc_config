package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/AdminASRecordDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AdminASRecordDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($ASRecord :  entity.AssignStrategy_TDIC) : void {
    __widgetOf(this, pcf.AdminASRecordDV, SECTION_WIDGET_CLASS).setVariables(false, {$ASRecord})
  }
  
  function refreshVariables ($ASRecord :  entity.AssignStrategy_TDIC) : void {
    __widgetOf(this, pcf.AdminASRecordDV, SECTION_WIDGET_CLASS).setVariables(true, {$ASRecord})
  }
  
  
}