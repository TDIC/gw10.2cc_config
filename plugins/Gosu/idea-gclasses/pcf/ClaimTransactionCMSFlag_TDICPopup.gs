package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/financials/transactions/ClaimTransactionCMSFlag_TDICPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ClaimTransactionCMSFlag_TDICPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (payment :  Payment) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ClaimTransactionCMSFlag_TDICPopup, {payment}, 0)
  }
  
  function pickValueAndCommit (value :  Payment) : void {
    __widgetOf(this, pcf.ClaimTransactionCMSFlag_TDICPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (payment :  Payment) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ClaimTransactionCMSFlag_TDICPopup, {payment}, 0).push()
  }
  
  
}