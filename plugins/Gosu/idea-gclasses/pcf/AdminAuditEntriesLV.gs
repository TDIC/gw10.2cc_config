package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/audit_ext/AdminAuditEntriesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AdminAuditEntriesLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($AuditEntries :  gw.api.database.IQueryBeanResult<AdminAudit_Ext>) : void {
    __widgetOf(this, pcf.AdminAuditEntriesLV, SECTION_WIDGET_CLASS).setVariables(false, {$AuditEntries})
  }
  
  function refreshVariables ($AuditEntries :  gw.api.database.IQueryBeanResult<AdminAudit_Ext>) : void {
    __widgetOf(this, pcf.AdminAuditEntriesLV, SECTION_WIDGET_CLASS).setVariables(true, {$AuditEntries})
  }
  
  
}