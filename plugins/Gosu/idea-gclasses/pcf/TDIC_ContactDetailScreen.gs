package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_ContactDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_ContactDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($contactHandle :  gw.api.contact.ContactHandle, $contactRole :  typekey.ContactRole, $canAccessLinkButtons :  boolean, $showRoles :  boolean, $linkStatus :  gw.api.contact.ContactSystemLinkStatus, $claim :  Claim, $canPick :  boolean, $showRelatedObjects :  boolean, $allowEditInAddressBook :  boolean) : void {
    __widgetOf(this, pcf.TDIC_ContactDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$contactHandle, $contactRole, $canAccessLinkButtons, $showRoles, $linkStatus, $claim, $canPick, $showRelatedObjects, $allowEditInAddressBook})
  }
  
  function refreshVariables ($contactHandle :  gw.api.contact.ContactHandle, $contactRole :  typekey.ContactRole, $canAccessLinkButtons :  boolean, $showRoles :  boolean, $linkStatus :  gw.api.contact.ContactSystemLinkStatus, $claim :  Claim, $canPick :  boolean, $showRelatedObjects :  boolean, $allowEditInAddressBook :  boolean) : void {
    __widgetOf(this, pcf.TDIC_ContactDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$contactHandle, $contactRole, $canAccessLinkButtons, $showRoles, $linkStatus, $claim, $canPick, $showRelatedObjects, $allowEditInAddressBook})
  }
  
  
}