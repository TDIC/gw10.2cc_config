package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/ASRecordDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ASRecordDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($ASRecord :  entity.AssignStrategy_TDIC) : void {
    __widgetOf(this, pcf.ASRecordDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$ASRecord})
  }
  
  function refreshVariables ($ASRecord :  entity.AssignStrategy_TDIC) : void {
    __widgetOf(this, pcf.ASRecordDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$ASRecord})
  }
  
  
}