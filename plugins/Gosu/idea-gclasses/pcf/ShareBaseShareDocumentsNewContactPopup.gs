package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsNewContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseShareDocumentsNewContactPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (claim :  Claim, contactSubtype :  typekey.Contact, share :  OutboundSharing_Ext) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ShareBaseShareDocumentsNewContactPopup, {claim, contactSubtype, share}, 0)
  }
  
  static function push (claim :  Claim, contactSubtype :  typekey.Contact, share :  OutboundSharing_Ext) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ShareBaseShareDocumentsNewContactPopup, {claim, contactSubtype, share}, 0).push()
  }
  
  
}