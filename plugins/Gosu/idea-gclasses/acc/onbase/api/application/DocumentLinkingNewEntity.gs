package acc.onbase.api.application

uses acc.onbase.configuration.DocumentLinkType
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 09/20/2017 - Daniel Q. Yu
 * * Initial implementation.
 */
class DocumentLinkingNewEntity extends DocumentLinking {
  /**
   * Max new entities in the _temporaryLinkedDocumentsMap
   */
  public static final var MAX_NEW_ENTITIES: int = 100
  /**
   * Prefix for generated public id
   */
  public static final var PUBLIC_ID_PREFIX: String = "acc.onbase:"
  /**
   * Logger
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)
  /**
   * linked documents for new entities
   */
  private static var _temporaryLinkedDocumentsMap = new SizeLimitedHashMap<String, List<OnBaseDocumentLink_Ext>>()

  /**
   * Helper class generate an unique public id.
   *
   * @return an unique public id string.
   */
  function getUniquePublicID(): String {
    var nextNum = gw.api.system.database.SequenceUtil.next(1, "ACC.OnBase.PublicID")
    return PUBLIC_ID_PREFIX + nextNum
  }

  /**
   * Link multiple documents to entity.
   *
   * @param entity    The entity which documents linked to.
   * @param documents The list of document ids to be linked to the entity.
   * @param linkType  The document link type.
   */
  public function linkDocumentsToEntity(entity: KeyableBean, documents: Object[], linkType: DocumentLinkType) {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentLinkingNewEntity.linkDocumentToEntity(" + entity + "," + documents + "," + linkType + ")")
    }
    var linkedDocuments: List<OnBaseDocumentLink_Ext> = null
    if (entity.PublicID == null) {
      var publicID = getUniquePublicID()
      entity.PublicID = publicID
      linkedDocuments = new ArrayList<OnBaseDocumentLink_Ext>()
      _temporaryLinkedDocumentsMap.put(entity.PublicID, linkedDocuments)
    } else {
      linkedDocuments = _temporaryLinkedDocumentsMap.get(entity.PublicID)
    }
    if (linkedDocuments == null) {
      throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentLinkingNotFoundException"))
    }
    foreach (doc in documents) {
      var link = new OnBaseDocumentLink_Ext()
      link.LinkType = linkType.toString()
      link.LinkedEntity = entity.PublicID
      link.Document = doc as Document
      linkedDocuments.add(link)
    }
  }

  /**
   * Unlink multiple documents from entity.
   *
   * @param documents The list of documents to be unlinked from the entity.
   * @param linkType  The document link type.
   * @param entityId  The entity id which document to be unlinked from.
   */
  public function unlinkDocumentsFromEntity(entity: KeyableBean, documents: Object[], linkType: DocumentLinkType) {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentLinkingNewEntity.unlinkDocumentsFromEntity(" + entity + "," + documents + "," + linkType + ")")
    }
    if (entity.PublicID != null) {
      var linkedDocuments = _temporaryLinkedDocumentsMap.get(entity.PublicID)
      if (linkedDocuments != null) {
        var bundle = gw.transaction.Transaction.getCurrent()
        foreach (doc in documents) {
          foreach (link in linkedDocuments) {
            if (link.LinkType == linkType.toString() && link.Document == doc as Document) {
              bundle.delete(link)
              linkedDocuments.remove(link)
              break
            }
          }
        }
      }
    }
  }

  /**
   * Get documents linked to entity.
   *
   * @param entityId The entity which documents linked to.
   * @param linkType The document link type.
   * @return A list of OnBase documents which linked to the entity.
   */
  public function getDocumentsLinkedToEntity(entity: KeyableBean, linkType: DocumentLinkType): List<Document> {
    if (_logger.isDebugEnabled()) {
      _logger.debug("Running method DocumentLinkingNewEntity.getDocumentsLinkedToEntity(" + entity + "," + linkType + ")")
    }
    var linkedDocs = new ArrayList<Document>()
    if (entity.PublicID != null) {
      var linkedDocuments = _temporaryLinkedDocumentsMap.get(entity.PublicID)
      if (linkedDocuments != null) {
        foreach (link in linkedDocuments) {
          if (link.LinkType == linkType.toString()) {
            linkedDocs.add(link.Document)
          }
        }
      }
    }

    return linkedDocs
  }

  /**
   * Get the UI Label for document linking.
   *
   * @param entity   The entity which documents linked to.
   * @param linkType The document link type.
   * @return The UI Label.
   */
  public static function getLinkingDocumentUILabel(entity: KeyableBean, linkType: DocumentLinkType): String {
    if (entity == null || linkType == null) {
      throw new java.lang.NullPointerException()
    }
    if (!OnBaseConfigurationFactory.Instance.EnableLinkedDocumentCount) {
      return DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentListPopup_Label_NoCount")
    }
    var docCount = new DocumentLinkingNewEntity().getDocumentsLinkedToEntity(entity, linkType).Count
    if (docCount > 0) {
      return DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentListPopup_Label_ViewDocuments", docCount)
    } else {
      return DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentListPopup_Label_NoDocument")
    }
  }

  /**
   * An inner class to set max size of the _temporaryLinkedDocumentsMap.
   * Gosu won't compile when this is written as anonymous class.
   */
  static class SizeLimitedHashMap<K, V> extends LinkedHashMap<K, V> {
    override protected function removeEldestEntry(eldest: Map.Entry<K, V>): boolean {
      if (size() > MAX_NEW_ENTITIES) {
        if (_logger.isDebugEnabled()) {
          _logger.debug("Removing eldest entity from document linking hashmap.")
        }
        return true
      }
      return false

    }
  }
}

