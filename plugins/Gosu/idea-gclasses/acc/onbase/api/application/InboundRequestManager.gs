package acc.onbase.api.application

uses acc.onbase.util.LoggerFactory
uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 */

class InboundRequestManager {

  /**
   * Logger
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

  /**
   * Utility function to perform actions like linking folder and contacts to the request,
   * creating folder in ShareBase and generating folder link, prior to committing the Request.
   *
   * @param folder
   * @param request
   */
  public function buildRequest(folder: ShareBase_Ext, request: InboundRequest_Ext) {

    request.linkShareBaseFolder(folder)
    request.createClaimContact()
    request.createInboundRequest()
    //to avoid duplicate file names, we place the folder created as a subfolder within a unique folder. Request ID is not available at this point.
    folder.ParentFolder = (UUID.randomUUID() as String)
    folder.RequestType = ShareBaseRequestType_Ext.TC_INBOUND
    folder.initiateFolderCreationAndShare()
  }

  /**
   * * This is currently not used, but contains function to get Inbound Requests related to Claim, if that information is needed
   *
   * @param claim
   * @return links List of all Request linked to the claim
   */
  public function getInboundRequestsForClaim(claim: Claim): List<InboundRequest_Ext> {
    if (_logger.isDebugEnabled()) {
      _logger.debug("Running method InboundRequestManager.getInboundRequestsForClaim(" + claim + ")")
    }
    if (claim == null) {
      throw new IllegalArgumentException("Calling InboundRequestManager.getInboundRequestsForClaim without any claim")
    }

    var query = Query.make(InboundRequest_Ext).compare(InboundRequest_Ext#Claim, Equals, claim)
    return query.select().toList()
  }

  /**
   * Find Inbound Request by PublicID
   * @param requestID the PublicID of the request
   * @return the Inbound Request with this PublicID
   */
  public function findInboundRequestByID(requestID: String): InboundRequest_Ext {
    var ibRequest: InboundRequest_Ext = null
    if (!requestID.HasContent) {
      _logger.error("Missing InBound Request ID")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingRequestID"))
    } else {
      ibRequest = Query.make(entity.InboundRequest_Ext).compare(entity.InboundRequest_Ext#PublicID, Equals, requestID).select().AtMostOneRow
      if (ibRequest == null) {
        _logger.error("Invalid Inbound Request ID " + requestID)
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidRequestID", requestID))
      }
    }
    return ibRequest
  }

  /**
   * Verify if the request and the folder are related
   * @param ibRequest Inbound Request
   * @param folder ShareBase Folder
   */
  public function validateFolderForRequest(ibRequest: InboundRequest_Ext, folder: ShareBase_Ext) {
    verifyFolderNotNull(folder)
    verifyRequestNotNull(ibRequest)
    //check to make sure an exception is returned if the folder and the request id's are not related
    if (ibRequest.ShareBaseFolder.PublicID != folder.PublicID) {
      _logger.error("ShareBase Folder " + folder.FolderName + " and Request " + ibRequest.PublicID + " are not related")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnrelatedFolderAndRequest", folder.FolderName, ibRequest.PublicID))
    }
  }

  /**
   * Verify the folderStatus passed is the expected status for an Inbound Request when calling the Activity Web Service
   * @param folderStatus current status of the folder
   * @param folder ShareBase Folder
   */
  public function validateActivityFolderStatusForRequest(folderStatus: ShareBaseRequestStatus, folder: ShareBase_Ext) {
    verifyFolderNotNull(folder)
    if (folderStatus == null) {
      _logger.error("Missing ShareBase Folder Status")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingFolderStatus"))
    }
    if ((folderStatus != ShareBaseRequestStatus.TC_MONITORED) && (folderStatus != ShareBaseRequestStatus.TC_ERROR)) {
      _logger.error("ShareBase Folder " + folder.FolderName + ": Monitored flag not set when ready for Activity creation")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InboundFolderNotMonitoredAtActivityCreation", folder.FolderName))
    }
  }

  /**
   * Verify the folder entity passed is not null
   * @param folder ShareBase Folder
   */
  private function verifyFolderNotNull(folder: ShareBase_Ext) {
    if (folder == null) {
      _logger.error("Missing ShareBase Folder")
      throw new IllegalArgumentException("Missing ShareBase Folder")
    }
  }

  /**
   * Verify the request is not null
   * @param request Inbound Request
   */
  private function verifyRequestNotNull(request: InboundRequest_Ext) {
    if (request == null) {
      _logger.error("Missing Inbound Request")
      throw new IllegalArgumentException("Missing Inbound Request")
    }
  }

}