package acc.onbase.api.application

uses acc.onbase.util.LoggerFactory
uses gw.api.database.Query
uses gw.api.locale.DisplayKey

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */

class OutboundSharingManager {

  /**
   * Logger
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)


  /**
   * Utility function to perform actions like linking folder and contacts to the outbound share,
   * creating folder in ShareBase and generating folder link, prior to committing the outbound share.
   *
   * @param folder
   * @param outboundShare
   */
  public function buildShare(folder: ShareBase_Ext, outboundShare: OutboundSharing_Ext) {
    if (_logger.isDebugEnabled()) {
      _logger.debug("Running method OutboundSharingManager.buildRequest(" + folder + " " + outboundShare + ")")
    }
    outboundShare.linkShareBaseFolder(folder)
    outboundShare.createClaimContact()
    outboundShare.createOutboundSharing()
    //to avoid duplicate file names, we place the folder created as a subfolder within a unique folder. Outbound Share ID is not available at this point.
    folder.ParentFolder = (UUID.randomUUID() as String)
    folder.RequestType = ShareBaseRequestType_Ext.TC_OUTBOUND
    folder.initiateFolderCreationAndShare()
  }


  public function getOutboundSharesForClaim(claim: Claim): List<OutboundSharing_Ext> {
    if (_logger.isDebugEnabled()) {
      _logger.debug("Running method OutboundSharingManager.getOutboundSharesForClaim(" + claim + ")")
    }
    if (claim == null) {
      throw new IllegalArgumentException("Calling OutboundSharingManager.getOutboundSharesForClaim without any claim")
    }
    var query = Query.make(OutboundSharing_Ext).compare(OutboundSharing_Ext#Claim, Equals, claim)
    return query.select().toList()
  }

  /**
   * Find Outbound Share by PublicID
   * @param shareID the PublicID of the share
   * @return the Outbound Share with this publicID
   */
  public function findOutboundShareByID(shareID: String): OutboundSharing_Ext {
    var obShare: OutboundSharing_Ext = null
    if (!shareID.HasContent) {
      _logger.error("Missing OutBound Sharing ID")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingShareID"))
    } else {
      obShare = Query.make(entity.OutboundSharing_Ext).compare(entity.OutboundSharing_Ext#PublicID, Equals, shareID).select().AtMostOneRow
      if (obShare == null) {
        _logger.error("Invalid Outbound Sharing ID " + shareID)
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidSharingID", shareID))
      }
    }

    return obShare
  }

  /**
   * Verify that the outbound share and the sharebase folder are related
   * @param obShare Outbound Share
   * @param folder ShareBase Folder
   */
  public function validateFolderForShare(obShare: OutboundSharing_Ext, folder: ShareBase_Ext) {
    verifyFolderNotNull(folder)
    verifyShareNotNull(obShare)
    //check to make sure an exception is returned if the folder and the sharing id's are not related
    if (obShare.ShareBaseFolder.PublicID != folder.PublicID) {
      _logger.error("ShareBase Folder " + folder.FolderName + " and Share " + obShare.PublicID + " are not related")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnrelatedFolderAndShare", folder.FolderName, obShare.PublicID))
    }
  }

  /**
   * Verify the folderStatus passed is the expected status for an Outbound Share when calling the Activity Web Service
   * @param folderStatus current status of the ShareBase Folder
   * @param folder ShareBase Folder
   */
  public function validateActivityFolderStatusForShare(folderStatus: ShareBaseRequestStatus, folder: ShareBase_Ext) {
    verifyFolderNotNull(folder)
    if (folderStatus == null) {
      _logger.error("Missing ShareBase Folder Status")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingFolderStatus"))
    }
    if ((folderStatus != ShareBaseRequestStatus.TC_DOCSSHARED) && (folderStatus != ShareBaseRequestStatus.TC_ERROR)) {
      _logger.error("Documents for ShareBase Folder " + folder.FolderName + ": Shared flag not set when ready for Activity creation")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_OutboundDocsNotSharedAtActivityCreation", folder.FolderName))
    }
  }

  /**
   * Verify the folder entity passed is not null
   * @param folder ShareBase Folder
   */
  private function verifyFolderNotNull(folder: ShareBase_Ext) {
    if (folder == null) {
      _logger.error("Missing ShareBase Folder")
      throw new IllegalArgumentException("Missing ShareBase Folder")
    }
  }

  /**
   * Verify the request is not null
   * @param share Outbound Share
   */
  private function verifyShareNotNull(share: OutboundSharing_Ext) {
    if (share == null) {
      _logger.error("Missing Outbound Share")
      throw new IllegalArgumentException("Missing Outbound Share")
    }
  }
}