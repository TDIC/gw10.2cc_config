package acc.onbase.api.security

uses acc.onbase.configuration.OnBaseConfiguration
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses org.apache.commons.codec.binary.Base64

uses javax.crypto.Cipher
uses javax.crypto.spec.SecretKeySpec
uses java.io.FileInputStream
uses java.io.FileNotFoundException

uses gw.api.util.ConfigAccess
uses acc.onbase.util.LoggerFactory

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 08/09/2016 - Daniel Q. Yu
 * * Initial implementation.
 */

/**
 * Loading secure properties from a file.
 */
class SecurePropertiesManager {
  public static final var ENCRYPTION_KEY: String = "encryption_key"
  public static final var WSP_USERNAME: String = "wsp_username"
  public static final var WSP_PASSWORD: String = "wsp_password"
  public static final var DECRYPTED_WSP_PASSWORD: String = "decrypted_wsp_password"
  public static final var SHAREBASE_API_TOKEN: String = "sharebase_api_token"
  public static final var DECRYPTED_SHAREBASE_API_TOKEN: String = "decrypted_sharebase_api_token"
  public static final var SHAREBASE_ENCRYPTION_KEY: String = "sharebase_encryption_key"

  /**
   * Logger
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

  /**
   * The secure properties
   */
  private static var _secureProperties = new Properties()
  private static var _encryptionProperties = new Properties()

  private static function loadSecureProperties() {
    // Load secure properties from file.
    var fileInput: FileInputStream
    var securePropertiesFileLocation = OnBaseConfigurationFactory.Instance.SecurePropertiesFile
    try {
      var securePropertiesFile = ConfigAccess.getConfigFile(securePropertiesFileLocation)
      fileInput = new FileInputStream(securePropertiesFile)
      _secureProperties.load(fileInput)
    } catch (ex: Exception) {
      _logger.error("No Configuration file found at " + securePropertiesFileLocation)
      throw new FileNotFoundException("No Configuration file found!")
    } finally {
      if (fileInput != null) {
        try {
          fileInput.close()
        } catch (ex2: Exception) {
        }
      }
    }

    // decrypt wsp password.
    var decryptedPassword = decrypt(_secureProperties.get(WSP_PASSWORD).toString(), _secureProperties.get(ENCRYPTION_KEY).toString().getBytes("UTF-8"))
    _secureProperties.setProperty(DECRYPTED_WSP_PASSWORD, decryptedPassword)
  }

  private static function loadEncryptionProperties(){
    // Load encryption key from file.
    var fileInput: FileInputStream
    var encryptionPropertiesFileLocation = OnBaseConfigurationFactory.Instance.EncryptionPropertiesFile
    try {
      var encryptionPropertiesFile = ConfigAccess.getConfigFile(encryptionPropertiesFileLocation)
      fileInput = new FileInputStream(encryptionPropertiesFile)
      _encryptionProperties.load(fileInput)
    } catch (ex: Exception) {
      _logger.error("No Configuration file found at " + encryptionPropertiesFileLocation)
      throw new FileNotFoundException("No Configuration file found!")
    } finally {
      if (fileInput != null) {
        try {
          fileInput.close()
        } catch (ex2: Exception) {
        }
      }
    }

    //Decrypt ShareBase API Token
    var decryptedToken = decrypt(_secureProperties.get(SHAREBASE_API_TOKEN).toString(), _encryptionProperties.get(SHAREBASE_ENCRYPTION_KEY).toString().getBytes("UTF-8"))
    _encryptionProperties.setProperty(DECRYPTED_SHAREBASE_API_TOKEN, decryptedToken)

  }

  /**
   * Get the password for WSP requests.
   *
   * @return The decrypted password.
   */
  public static function getUsernameForWSP(): String {
    if (_secureProperties.size() == 0) {
      loadSecureProperties()
    }
    return _secureProperties.getProperty(WSP_USERNAME)
  }

  /**
   * Get the password for WSP requests.
   *
   * @return The decrypted password.
   */
  public static function getPasswordForWSP(): String {
    if (_secureProperties.size() == 0) {
      loadSecureProperties()
    }
    return _secureProperties.getProperty(DECRYPTED_WSP_PASSWORD)
  }

  /**
   * Get the password for WSP requests.
   *
   * @return The decrypted password.
   */
  public static function getTokenForShareBase(): String {
    if (_secureProperties.size() == 0) {
      loadSecureProperties()
    }
    if (_encryptionProperties.size() == 0) {
      loadEncryptionProperties()
    }
    return _encryptionProperties.getProperty(DECRYPTED_SHAREBASE_API_TOKEN)
  }

  /**
   * Encrypt a string using AES.
   *
   * @return The encrypted string.
   */
  public static function encrypt(str: String, key: byte[]): String {
    var cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
    var secretKey = new SecretKeySpec(key, "AES")
    cipher.init(Cipher.ENCRYPT_MODE, secretKey);
    return new String(Base64.encodeBase64(cipher.doFinal(str.getBytes("UTF-8"))), "UTF-8")
  }

  /**
   * Decrypt an encrypted string using AES.
   *
   * @return The decrypted string.
   */
  private static function decrypt(str: String, key: byte[]): String {
    var cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
    var secretKey = new SecretKeySpec(key, "AES")
    cipher.init(Cipher.DECRYPT_MODE, secretKey);
    return new String(cipher.doFinal(Base64.decodeBase64(str.getBytes("UTF-8"))), "UTF-8")
  }

  /**
   * Clears hashtable
   */
  public static function clearSecureProperties(){
    _secureProperties.clear()
  }

  public static function clearEncryptionProperties(){
    _encryptionProperties.clear()
  }
}