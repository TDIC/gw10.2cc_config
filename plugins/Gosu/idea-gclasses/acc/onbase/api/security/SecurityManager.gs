package acc.onbase.api.security

uses acc.onbase.api.exception.InvalidContentException
uses acc.onbase.configuration.OnBaseConfigurationFactory

uses javax.crypto.Mac
uses javax.crypto.spec.SecretKeySpec

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 02/01/2016 - Daniel Q. Yu
 * * Initial implementation.
 * <p>
 * 03/17/2016 - Daniel Q. Yu
 * * Added max length to the joined strings.
 * <p>
 * 06/02/2106 - Daniel Q. Yu
 * * Added Exposure security level and Exposure ACL.
 * <p>
 * 01/27/2017 - Daniel Q. Yu
 * * Added Check Sum Method.
 * <p>
 * 08/14/2017 - Tori Brenneison
 * * Refactored/added getHashBytes function.
 * <p>
 * 02/20/2018 - Daniel Q. Yu
 * * Remove exposure security
 */

/**
 * Claim Center 7 & 8 security utilities.
 */
class SecurityManager {
  /**
   * The default max keyword value length
   */
  public static final var KEYWORD_MAX_LENGTH : int = 250

  /**
   * Get a claim's ACL roles in a list of comma separated strings with each string shorter than default KEYWORD_MAX_LENGTH.
   *
   * @param claim The claim.
   * @return The claim's ACL roles in a list of comma separated strings.
   */
  public static function getClaimSecurityACL(claim : Claim) : List<String> {
    return getClaimSecurityACL(claim, KEYWORD_MAX_LENGTH)
  }

  /**
   * Get a claim's ACL roles in a list of comma separated strings with each string shorter than maxLength.
   *
   * @param claim     The claim.
   * @param maxLength The max length for each returned string.
   * @return The claim's ACL roles in a list of comma separated strings.
   */
  public static function getClaimSecurityACL(claim : Claim, maxLength : int) : List<String> {
    var roleList = new ArrayList<String>()
    if (claim == null || claim.PermissionRequired == null) {
      roleList.add("Anyone")
    } else {
      var roles = new ArrayList<String>()
      foreach (acl in claim.Access) {
        if (acl.Permission == ClaimAccessType.TC_VIEW) {
          if (acl.SecurityZone != null) {
            roles.add(acl.SecurityZone as String)
          } else if (acl.Group != null) {
            roles.add(acl.Group as String)
          } else if (acl.User != null) {
            roles.add(acl.User.Credential.UserName)
          }
        }
      }

      var buf = new StringBuilder()
      foreach (role in roles) {
        if (role.length() > maxLength) {
          throw new InvalidContentException("Keyword value exceeds maximum length" + maxLength + " - " + role)
        } else if (role.length() == maxLength) {
          roleList.add(role)
        } else if (buf.length() == 0) {
          buf.append(role)
        } else {
          if (buf.length() + role.length() > maxLength) {
            roleList.add(buf as String)
            buf = new StringBuilder(role)
          } else {
            buf.append(",").append(role)
          }
        }
      }
      roleList.add(buf as String)
    }
    return roleList
  }

  /**
   * Compute hash bytes from key
   *
   * @param docId OnBase Document Id.
   * @param hashKey hash key.
   */
  public static function getHashBytes(docID : String, hashKey : String) : byte[] {

    var secretKey = new SecretKeySpec(hashKey.getBytes("ASCII"), "HmacSHA256")
    var mac = Mac.getInstance("HmacSHA256")
    mac.init(secretKey)
    var hashBytes = mac.doFinal(docID.getBytes("ASCII"))

    return hashBytes
  }

  /**
   * Compute docId hash for docpop URL checksum.
   *
   * @param docId OnBase Document Id.
   * @return The hash hex string for the document id.
   */
  public static function computeDocIdCheckSum(docId : String) : String {

    // compute hashHex hashBytes.
    var key = OnBaseConfigurationFactory.Instance.PopChecksumKey
    var hashBytes = getHashBytes(docId, key)

    // convert hashHex to hex string.
    var hashHex = new StringBuffer();
    foreach (b in hashBytes) {
      var hex = Integer.toHexString(0xFF & b);
      if (hex.length() == 1) {
        hashHex.append('0');
      }
      hashHex.append(hex);
    }

    return hashHex.toString()
  }

  /**
   * This method has been copied from Policy Center Onbase accelerator to implement WebCustomQuery and it would be
   * removed in future after updating accelerator.
   * @param checksumValue
   * @return hashed checksumValue.
   */
  public static function computeCheckSum(checksumValue: String): String {
    // compute hashHex hashBytes.
    var key = new SecretKeySpec(OnBaseConfigurationFactory.Instance.PopChecksumKey.getBytes("ASCII"), "HmacSHA256")
    var mac = Mac.getInstance("HmacSHA256")
    mac.init(key);
    var hashBytes = mac.doFinal(checksumValue.getBytes("ASCII"))

    // convert hashHex to hex string.
    var hashHex = new StringBuffer();
    foreach(b in hashBytes) {
      var hex = Integer.toHexString(0xFF & b);
      if (hex.length() == 1) {
        hashHex.append('0');
      }
      hashHex.append(hex);
    }

    return hashHex.toString()
  }

}
