<%@ params(document: Document, filename: String, source: String)
%>>>>>Self Configuring Tagged DIP<<<<
BEGIN:
>>DocTypeName:CS Archive Document
>>DocDate:${ new java.text.SimpleDateFormat("MM/dd/yyyy").format(new java.util.Date()) }
>>FileName:${filename}
mimetype:${document.MimeType}
Source: ${source}
Author: ${User.util.CurrentUser ?: ""}
Async Document ID: ${document.PendingDocUID ?: ""}
END:
