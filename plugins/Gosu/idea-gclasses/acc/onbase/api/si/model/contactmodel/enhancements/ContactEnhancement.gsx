package acc.onbase.api.si.model.contactmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ContactEnhancement : acc.onbase.api.si.model.contactmodel.Contact {
  public static function create(object : entity.Contact) : acc.onbase.api.si.model.contactmodel.Contact {
    return new acc.onbase.api.si.model.contactmodel.Contact(object)
  }

  public static function create(object : entity.Contact, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.contactmodel.Contact {
    return new acc.onbase.api.si.model.contactmodel.Contact(object, options)
  }

}