package acc.onbase.api.si.model.claimmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ClaimEnhancement : acc.onbase.api.si.model.claimmodel.Claim {
  public static function create(object : entity.Claim) : acc.onbase.api.si.model.claimmodel.Claim {
    return new acc.onbase.api.si.model.claimmodel.Claim(object)
  }

  public static function create(object : entity.Claim, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.claimmodel.Claim {
    return new acc.onbase.api.si.model.claimmodel.Claim(object, options)
  }

}