package acc.onbase.api.si.model.outboundsharing_extmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement OutboundSharing_ExtEnhancement : acc.onbase.api.si.model.outboundsharing_extmodel.OutboundSharing_Ext {
  public static function create(object : entity.OutboundSharing_Ext) : acc.onbase.api.si.model.outboundsharing_extmodel.OutboundSharing_Ext {
    return new acc.onbase.api.si.model.outboundsharing_extmodel.OutboundSharing_Ext(object)
  }

  public static function create(object : entity.OutboundSharing_Ext, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.outboundsharing_extmodel.OutboundSharing_Ext {
    return new acc.onbase.api.si.model.outboundsharing_extmodel.OutboundSharing_Ext(object, options)
  }

}