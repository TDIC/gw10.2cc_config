package acc.onbase.api.si.model.documentmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement DocumentEnhancement : acc.onbase.api.si.model.documentmodel.Document {
  public static function create(object : entity.Document, labels : java.util.Collection<gw.xml.gx.PropertyLabel>) : acc.onbase.api.si.model.documentmodel.Document {
    return new acc.onbase.api.si.model.documentmodel.Document(object, labels)
  }

  public static function create(object : entity.Document, options : gw.api.gx.GXOptions, labels : java.util.Collection<gw.xml.gx.PropertyLabel>) : acc.onbase.api.si.model.documentmodel.Document {
    return new acc.onbase.api.si.model.documentmodel.Document(object, options, labels)
  }

  public static function create(object : entity.Document) : acc.onbase.api.si.model.documentmodel.Document {
    return new acc.onbase.api.si.model.documentmodel.Document(object)
  }

  public static function create(object : entity.Document, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.documentmodel.Document {
    return new acc.onbase.api.si.model.documentmodel.Document(object, options)
  }

  public static function create(object : entity.Document, labels : java.util.Collection<gw.xml.gx.PropertyLabel>, throughLabels : java.util.Collection<java.lang.String>) : acc.onbase.api.si.model.documentmodel.Document {
    return new acc.onbase.api.si.model.documentmodel.Document(object, labels, throughLabels)
  }

  public static function create(object : entity.Document, options : gw.api.gx.GXOptions, labels : java.util.Collection<gw.xml.gx.PropertyLabel>, throughLabels : java.util.Collection<java.lang.String>) : acc.onbase.api.si.model.documentmodel.Document {
    return new acc.onbase.api.si.model.documentmodel.Document(object, options, labels, throughLabels)
  }

}