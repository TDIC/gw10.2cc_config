package acc.onbase.api.si.model.inboundrequest_extmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement InboundRequest_ExtEnhancement : acc.onbase.api.si.model.inboundrequest_extmodel.InboundRequest_Ext {
  public static function create(object : entity.InboundRequest_Ext) : acc.onbase.api.si.model.inboundrequest_extmodel.InboundRequest_Ext {
    return new acc.onbase.api.si.model.inboundrequest_extmodel.InboundRequest_Ext(object)
  }

  public static function create(object : entity.InboundRequest_Ext, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.inboundrequest_extmodel.InboundRequest_Ext {
    return new acc.onbase.api.si.model.inboundrequest_extmodel.InboundRequest_Ext(object, options)
  }

}