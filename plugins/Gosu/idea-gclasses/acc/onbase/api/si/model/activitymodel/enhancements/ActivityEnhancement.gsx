package acc.onbase.api.si.model.activitymodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ActivityEnhancement : acc.onbase.api.si.model.activitymodel.Activity {
  public static function create(object : entity.Activity) : acc.onbase.api.si.model.activitymodel.Activity {
    return new acc.onbase.api.si.model.activitymodel.Activity(object)
  }

  public static function create(object : entity.Activity, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.activitymodel.Activity {
    return new acc.onbase.api.si.model.activitymodel.Activity(object, options)
  }

}