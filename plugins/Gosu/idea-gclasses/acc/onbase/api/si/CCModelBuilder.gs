package acc.onbase.api.si

uses gw.api.gx.GXOptions

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 11/16/2017 - Tori Brenneison
 * * Initial implementation.
 *
 */


class CCModelBuilder {


  public static function buildClaimModel(myClaim : Claim) : acc.onbase.api.si.model.claimmodel.Claim {

    var options = new GXOptions() {:Verbose = true}
    var model = acc.onbase.api.si.model.claimmodel.Claim.create(myClaim,options)

      //remove entries unrelated to view permission
      model.Access.Entry.retainWhere(\elt -> elt.Permission.Code == "view")

      //set "anyone" to true and remove additional entries for unsecured claims
      if (myClaim == null || myClaim.PermissionRequired == null) {
        model.Access.Entry.clear()
        model.Access.Entry[0].Anyone = true
      }

    return model
  }

  /**
   * Build an OnBase Message which contains the Document and Claim Model data to be sent to Onbase during Document Archival
   *
   * @param document The document whose keywords have to be sent to OnBase
   * @param docid The Document Handle returned from OnBase for the document
   * @return builder which contains the Document and Claim Model added to the Message
   */
  public static function buildArchiveMessage(document : Document) : MessageBuilder {
    var defaultLabel = acc.onbase.api.si.model.documentmodel.Document.Label.default_label
    var model = acc.onbase.api.si.model.documentmodel.Document.create(document,{defaultLabel})
    var builder = new MessageBuilder("ArchiveDocumentCC")
    builder.addMessagePart('document', model)
    var claimModel = CCModelBuilder.buildClaimModel(document.Claim)
    builder.addMessagePart('claim', claimModel)
    if(document.Exposure != null){
      var exposureModel = acc.onbase.api.si.model.exposuremodel.Exposure.create(document.Exposure)
      builder.addMessagePart('exposure',exposureModel)
    }
    return builder
  }

}