package acc.onbase.api.exception

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */

class ShareBaseAPIException extends Exception {

  private var _statusCode: int as StatusCode
  private var _responseMessage: String as ResponseMessage

  construct(statusCode: int, responseMessage: String) {
    super(statusCode + ":" + responseMessage)
    this._statusCode = statusCode
    this._responseMessage = responseMessage
  }

}