package acc.onbase.api.sharebaseconfig

uses acc.onbase.api.exception.ShareBaseAPIException
uses acc.onbase.api.service.implementations.http.HTTPConfig
uses acc.onbase.api.service.implementations.http.HTTPURLConnectionHandlerFactory
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
uses org.json.simple.JSONObject
uses org.json.simple.parser.JSONParser

uses java.text.SimpleDateFormat

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */

/**
 * Implementation of HTTP Request to call ShareBase API - each function corresponds to different API calls
 */
class ShareBaseAPI implements ShareBaseInterface {

  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

  public function getFoldersForLibraryfromAPI() {
    try {
      var config = new HTTPConfig(OnBaseConfigurationFactory.Instance.ShareBaseURL, "/" + OnBaseConfigurationFactory.Instance.ShareBaseLibrary + "/folders")
      var connection = HTTPURLConnectionHandlerFactory.Instance.handleGet(config)
      connection.setRequestProperty("Authorization", OnBaseConfigurationFactory.Instance.ShareBaseAPIToken)
      connection.verifyStatusCode()
      var response = connection.readResponse()
      connection.disconnect()
    } catch (ex: Exception) {
      _logger.error("Failed to access library from ShareBase", ex)
      throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_GenericShareBaseExceptionMessage"))
    }

  }

  /**
   * Call ShareBase Rest API to create a folder with specified folder name
   */
  public function createFolderfromAPI(folder: ShareBase_Ext) {
    //POST /api/libraries/{libraryId}/folders
    try {
      var config = new HTTPConfig(OnBaseConfigurationFactory.Instance.ShareBaseURL, OnBaseConfigurationFactory.Instance.ShareBaseLibrary + "/folders")
      var createFolderParameters = new HashMap<String, Object>();
      // to avoid duplicate folders - a parent folder with request id(unique) is created and the actual folder is placed as a subfolder within it
      // inbound\{UUID}\foldername (or) outbound\{UUID}\foldername
      createFolderParameters.put("FolderPath", folder.RequestType + "\\" + folder.ParentFolder + "\\" + folder.FolderName)
      var connection = HTTPURLConnectionHandlerFactory.Instance.handlePost(config)
      connection.setRequestProperty("Authorization", OnBaseConfigurationFactory.Instance.ShareBaseAPIToken)
      connection.setPostBodyParameters(createFolderParameters)
      connection.verifyStatusCode()
      var response = connection.readResponse()
      //set sharebase folder id
      folder.ShareBaseFolderUID = response.get("FolderId") as String
      folder.Status = ShareBaseRequestStatus.TC_CREATEDINSHAREBASE
      connection.disconnect()
    } catch (ex: ShareBaseAPIException) {
      folder.Status = ShareBaseRequestStatus.TC_ERROR
      _logger.error("Failed to create folder: " + folder.FolderName + " in ShareBase", ex)
      var responseMessage = ex.ResponseMessage
      //throw the exception if the file name entered is invalid - contains invalid chars based on windows allowable chars
      if (responseMessage.contains("is an invalid file name")) {
        throw new DisplayableException(responseMessage)
      } else {
        throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_GenericShareBaseExceptionMessage"))
      }
    }catch(ex:Exception){
      throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_GenericShareBaseExceptionMessage"))
    }
  }

  /**
   * Call ShareBase Rest API to obtain the folder upload link
   */
  public function getShareBaseLink(folder: ShareBase_Ext) {
    // POST /api/folders/{folderId}/share
    if (folder.FolderLink != null) {
      throw new Exception("Folder already has a URL assigned")
    }
    try {
      var config = new HTTPConfig(OnBaseConfigurationFactory.Instance.ShareBaseURL, "/api/folders/" + folder.ShareBaseFolderUID + "/share")
      var shareParameters = new HashMap<String, Object>()
      var date = new SimpleDateFormat("MM/dd/yyyy").format(folder.Expiration)
      shareParameters.put("ExpiresOn", date)
      shareParameters.put("ExpireStyle", "date")
      shareParameters.put("Password", folder.LinkPassword)
      shareParameters.put("AllowView", true)
      shareParameters.put("AllowDownload", true)
      shareParameters.put("AllowUpload", true)
      var connection = HTTPURLConnectionHandlerFactory.Instance.handlePost(config)
      connection.setRequestProperty("Authorization", OnBaseConfigurationFactory.Instance.ShareBaseAPIToken)
      connection.setPostBodyParameters(shareParameters)
      connection.verifyStatusCode()
      var response = connection.readResponse()
      var links = response.get("Links") as JSONObject
      var shareURL = links.get("WebUri") as String
      folder.FolderLink = shareURL
      folder.ShareID = response.get("ShareId") as String
      folder.Status = ShareBaseRequestStatus.TC_LINKOBTAINED
      connection.disconnect()
    } catch (ex: ShareBaseAPIException) {
      //delete the folder from ShareBase if created
      if (folder.ShareBaseFolderUID != null) {
        deleteFolderInShareBase(folder)
      }
      _logger.error("Failed to get URL for folder: " + folder.FolderName + " from ShareBase", ex)
      var responseMessage = ex.ResponseMessage
      if ((responseMessage.equalsIgnoreCase("The expiration date needs to be specified"))
          || (responseMessage.equalsIgnoreCase("The expiration date needs to be in the future"))
          || (responseMessage.contains("User group policy limits maximum link duration to"))
          ) {
        //these are currently checked before the api call but could still be user displayable if necessary
        throw new DisplayableException(responseMessage)
      } else if (responseMessage.contains("InvalidPassword")) {
        /** parse json response to display the message
         *  response template:
         {
         "InvalidPassword": true,
         "Error": <Password criteria not being met>
         }
         **/
        var jsonResponse = new JSONParser().parse(responseMessage) as JSONObject
        throw new DisplayableException(jsonResponse.get("Error") as String)
      } else {
        throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_GenericShareBaseExceptionMessage"))
      }
    }catch(ex:Exception){
      throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_GenericShareBaseExceptionMessage"))
    }
  }

  /**
   * Call ShareBase Rest API to revoke access for this folder share
   */
  public function revokeShare(folder: ShareBase_Ext) {
    //DELETE /api/folders/{folderId}/share/{shareId}
    try {
      if ((folder.ShareID.isEmpty()) || (folder.ShareID == null)) {
        _logger.error("No share exists for this folder")
        throw new Exception("No share exists for this folder")
      }
      var config = new HTTPConfig(OnBaseConfigurationFactory.Instance.ShareBaseURL, "/api/folders/" + folder.ShareBaseFolderUID + "/share/" + folder.ShareID)
      var connection = HTTPURLConnectionHandlerFactory.Instance.handleDelete(config)
      connection.setRequestProperty("Authorization", OnBaseConfigurationFactory.Instance.ShareBaseAPIToken)
      connection.verifyStatusCode()
      folder.ShareID = null
      folder.FolderLink = null
      connection.disconnect()
    } catch (ex: Exception) {
      _logger.error("Failed to revoke access for folder:" + folder.FolderName + " in ShareBase", ex)
      throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_GenericShareBaseExceptionMessage"))
    }

  }

  /**
   * Call ShareBase Rest API to delete and purge this folder in ShareBase
   */
  public function deleteFolderInShareBase(folder: ShareBase_Ext) {
    //DELETE /api/folders/{folderId}?purge=true
    try {
      var config = new HTTPConfig(OnBaseConfigurationFactory.Instance.ShareBaseURL, "/api/folders/" + folder.ShareBaseFolderUID + "?purge=true")
      var connection = HTTPURLConnectionHandlerFactory.Instance.handleDelete(config)
      connection.setRequestProperty("Authorization", OnBaseConfigurationFactory.Instance.ShareBaseAPIToken)
      connection.verifyStatusCode()
      folder.ShareBaseFolderUID = null
      folder.ShareID = null
      folder.FolderLink = null
      folder.Status = ShareBaseRequestStatus.TC_DELETED
      connection.disconnect()
    } catch (ex: Exception) {
      folder.Status = ShareBaseRequestStatus.TC_ERROR
      _logger.error("Failed to delete folder :" + folder.FolderName + " from ShareBase", ex)
    }
  }
}