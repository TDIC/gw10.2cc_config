package acc.onbase.api.service.interfaces

uses java.io.InputStream

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 01/14/2015 - Daniel Q. Yu
 * * Initial implementation.
 */

/**
 * Interface to call OnBase Update GetDoc service.
 */
interface DocumentContentInterface {
  /**
   * Get document content from OnBase. The content can be retrieved from document.FileContent input stream after this call.
   *
   * @param document The OnBase document.
   */
  public function getDocumentContent(document: Document): InputStream
}
