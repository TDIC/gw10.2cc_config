package acc.onbase.api.service.interfaces

uses gw.xml.XmlElement

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 *
 * Interface to submit an outgoing message to OnBase
 */
interface SubmitMessageInterface {

  /**
   * Submit a message to OnBase
   *
   * @param payload string payload of message
   * @return submitted message id
   */
  function submitMessage(payload : String) : long

  /**
   * Submit a message to OnBase
   *
   * @param payload XML message
   * @return submitted message id
   */
  function submitMessage(payload : XmlElement) : long

}