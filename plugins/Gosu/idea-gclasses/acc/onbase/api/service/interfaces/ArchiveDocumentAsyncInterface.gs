package acc.onbase.api.service.interfaces

uses java.io.File

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 01/13/2015 - Daniel Q. Yu
 * * Initial implementation.
 */

/**
 * Interface to call OnBase Asynchronized Archive Document service.
 */
interface ArchiveDocumentAsyncInterface {
  /**
   * Archive document asynchronously.
   *
   * @param documentFile The document local file.
   * @param document The document entity instance.
   */
  public function archiveDocument(documentFile: File, document: Document)
}
