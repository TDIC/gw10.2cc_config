package acc.onbase.api.service.implementations.wsp

uses acc.onbase.api.service.implementations.wsp.util.WSPUtil
uses acc.onbase.api.service.interfaces.ArchiveDocumentSyncInterface
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.util.LoggerFactory
uses acc.onbase.wsc.onbasewspwsc.soapservice.anonymous.elements.KeywordsArchiveDocument_StandAlone
uses acc.onbase.wsc.onbasewspwsc.soapservice.elements.ArchiveDocument
uses org.apache.commons.codec.binary.Base64

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 01/30/2015 - dlittleton
 * * Initial implementation.
 * <p>
 * 01/27/2016 - Daniel Q. Yu
 * * Added Keyword claimsecurityrole.
 * <p>
 * 03/10/2016 - Daniel Q. Yu
 * * Use MIKG instand standalone so that no workflow action needed.
 * <p>
 * 04/26/2016 - Richard R. Kantimahanthi
 * * Reinstating broken functionality set in SCR#215219 (use GW Link MIKG when archiving a document related to an Exposure).
 * <p>
 * 05/20/2016 - Anirudh Mohan
 * * Added Document Handle to archiveKeywords
 * <p>
 * 06/08/2016 - Anirudh Mohan
 * * Replaced Document Handle with DocumentIdForRevision
 * * Updated GWLink as a MIKG and renamed GWLinkTypeBackup,GWLinkIDBackup to GWLinkType and GWLinkID respectively
 * <p>
 * 11/02/2016 - Daniel Q. Yu
 * * Uses WSPUtil.getWSPService() to get proper WSP service.
 * <p>
 * 01/09/2017 - Anirudh Mohan
 * * Moved Exposure from GWLink MIKG to Exposure MIKG
 * * Removed Job,Account Number and producer ID Keywords
 * <p>
 * 01/10/2017 - Daniel Q. Yu
 * * Added exposure security level and exposure security role keyword information.
 * * Added document security level keyword information.
 * <p>
 * 03/13/2017 - Anirudh Mohan
 * * Added Service Requests
 */

/**
 * Implementation of the ArchiveDocumentSync interface using WSP.
 */
class ArchiveDocumentSyncWSP implements ArchiveDocumentSyncInterface {
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  /**
   * Archive document synchronously.
   *
   * @param documentContents The document content in bytes.
   * @param document         The entity.document object.
   * @return The newly archived document id.
   */

  public override function archiveDocument(documentContents: byte[], document: Document): String {
    _logger.debug("Start executing archiveDocument() using WSP service.")

    // Build query (Soap 1.1)
    var service = WSPUtil.getWSPService()

    var base64Content = Base64.encodeBase64String(documentContents)

    var archiveDocument = new ArchiveDocument()

    // File information
    archiveDocument.DocumentArchiveData.FileInfo.MIMEType = document.MimeType
    archiveDocument.DocumentArchiveData.FileInfo.Base64FileStream = base64Content

    // Standalone Keywords
    var archiveKeywords = new KeywordsArchiveDocument_StandAlone()
    archiveKeywords.Source_Collection.String[0] = OnBaseConfigurationFactory.Instance.SourceCenterName
    archiveKeywords.Author_Collection.String[0] = document.Author

    archiveDocument.DocumentArchiveData.Keywords.StandAlone = archiveKeywords

    // Make request
    var response = service.ArchiveDocument(archiveDocument)
    if (_logger.isDebugEnabled()) {
      _logger.debug("Finished executing archiveDocument() using WSP service with document ID: ${response}")
    }

    return response.toString()
  }
}

