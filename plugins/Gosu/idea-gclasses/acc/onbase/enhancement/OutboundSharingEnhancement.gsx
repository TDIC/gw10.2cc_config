package acc.onbase.enhancement

uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
uses entity.Contact

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */
enhancement OutboundSharingEnhancement : OutboundSharing_Ext {

  public function addRecipient(contact: Contact) {
    if (contact == null) {
      throw new IllegalArgumentException("Calling OutboundSharingEnhancement.addRecipient without a contact")
    }
    addRecipients(new Contact[]{contact})
  }

  public function addRecipients(contacts: Contact[]) {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    if (_logger.DebugEnabled) {
      _logger.debug("Running method OutboundSharingEnhancement.addRecipients(" + contacts + ")")
    }
    if (contacts.Count == 0) {
      throw new IllegalArgumentException("Calling OutboundSharingEnhancement.addRecipients with no contacts")
    }

    foreach(contact in contacts) {
      var link = new OutbdShareContactLink_Ext(){
          :OutboundSharing = this,
          :Contact = contact
          }

      this.addToContacts(link)
    }
  }

  public function createClaimContact() {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    var claim = this.Claim
    if (_logger.DebugEnabled) {
      _logger.debug("Running method OutboundSharingEnhancement.createClaimContact(" + claim + ")")
    }
    if (claim == null) {
      throw new IllegalArgumentException("Outbound Share not associated with a Claim")
    }
    foreach(contact in this.Contacts) {
      if (!claim.getRelatedContacts().contains(contact.Contact)) {
        claim.createClaimContact(contact.Contact)
      }
    }
  }

  public function removeRecipient(contact: Contact) {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    if (_logger.DebugEnabled) {
      _logger.debug("Running method OutboundSharingEnhancement.removeReceipient(" + contact + ")")
    }
    if (contact == null) {
      throw new IllegalArgumentException("Calling OutboundSharingEnhancement.removeReceipient without a contact")
    }

    var link = this.Contacts.singleWhere(\contactLink -> contactLink.Contact == contact)
    if (link != null) {
      this.removeFromContacts(link)
      link.remove()
    }
  }


  public property get LinkedRecipients(): List<Contact> {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    if (_logger.DebugEnabled) {
      _logger.debug("Running property OutboundSharingEnhancement.LinkedRecipients()")
    }

    var contacts = this.Contacts.map(\contactLink -> contactLink.Contact)
    return contacts.toList()
  }

  public property get ContactsNotLinked(): List<Contact> {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    var claim = this.Claim
    if (_logger.DebugEnabled) {
      _logger.debug("Running property OutboundSharingEnhancement.ContactsNotLinked(" + claim + ")")
    }
    if (claim == null) {
      throw new IllegalArgumentException("No Claim assoicated with the share")
    }
    var notLinked = new ArrayList<Contact>()

    //Only select contacts with valid email address
    var contacts = claim.getRelatedContacts().where(\contact -> contact.EmailAddress1 != null)

    var linked = this.LinkedRecipients

    // if the user isn't in the outbound share's contacts list and it has an email address, move it to the available contacts list. Oh let's also only put it there if it's not already there.
    if (!(linked.contains(User.util.CurrentUser.Contact)) && User.util.CurrentUser.Contact.EmailAddress1 != null && !(notLinked.contains(User.util.CurrentUser.Contact))) {
      notLinked.add(User.util.CurrentUser.Contact)
    }

    foreach(contact in contacts) {
      // sometimes the currently logged in user gets duplicated in the notLinked list when we don't check this here
      if (!linked.contains(contact) && !notLinked.contains(contact)) {
        notLinked.add(contact)
      }
    }
    return notLinked
  }

  public function linkShareBaseFolder(folder: ShareBase_Ext) {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    if (_logger.isDebugEnabled()) {
      _logger.debug("Running method OutboundSharingEnhancement.linkShareBaseFolder(" + folder + ")")
    }

    if (folder == null) {
      throw new IllegalArgumentException("Calling OutboundSharingEnhancement.linkShareBaseFolder with null folder value")
    }

    folder.Status = ShareBaseRequestStatus.TC_NOTCREATED
    this.ShareBaseFolder = folder

  }

  public function createOutboundSharing() {

    if (this.Contacts.Count == 0) {
      throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.ShareBase.STR_GW_MissingContacts"))
    }
    if( this.DocumentLinks.Count == 0 )
    {
      throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.ShareBase.STR_GW_MissingDocuments"))
    }
    this.Owner = User.util.CurrentUser
    this.Created = gw.api.util.DateUtil.currentDate()
  }

  public function addDocument(document: Document) {
    if (document == null) {
      throw new IllegalArgumentException("Calling OutboundSharingEnhancement.addDocument without a document entity")
    }
    addDocuments(new Document[]{document})
  }

  public function addDocuments(documents: Document[]) {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    if (_logger.DebugEnabled) {
      _logger.debug("Running method OutboundSharingEnhancement.addDocuments(" + documents + ")")
    }
    if (documents.Count == 0) {
      throw new IllegalArgumentException("Calling OutboundSharingEnhancement.addDocuments without any documents")
    }

    foreach(document in documents) {
      var link = new OutbdShareDocLink_Ext(){
        :OutboundSharing = this,
        :Document = document
      }

      this.addToDocumentLinks(link)
    }
  }

  public function removeDocumentLink(docLink: OutbdShareDocLink_Ext) {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    if (_logger.DebugEnabled) {
      _logger.debug("Running method OutboundSharingEnhancement.removeDocument(" + docLink.Document + ")")
    }
    if (docLink == null) {
      throw new IllegalArgumentException("Calling OutboundSharingEnhancement.removeDocumentLink without a documentLink")
    }

    var link = this.DocumentLinks.singleWhere(\documentLink -> documentLink == docLink)
    if (link != null) {
      this.removeFromDocumentLinks(link)
      link.remove()
    }
  }

  public property get DocumentsNotLinked(): List<Document> {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    var claim = this.Claim
    if (_logger.DebugEnabled) {
      _logger.debug("Running property OutboundSharingEnhancement.DocumentsNotLinked(" + claim + ")")
    }
    if (claim == null) {
      throw new IllegalArgumentException("No Claim assoicated with the share")
    }
    var notLinked = new ArrayList<Document>() // the array list we're populating of docs associated with the claim that aren't already linked
    var shareDocs = new ArrayList<Document>() // docs already linked to the claim
    var claimDocs = claim.getDocuments()      // all docs associated with the claim

    foreach ( link in this.DocumentLinks )
    {
      shareDocs.add(link.Document)  // populate the docs associated with the share
    }

    foreach( doc in claimDocs )
    {
      if( !shareDocs.contains(doc) )
      {
        notLinked.add(doc)  // populate the docs asscoiated with the claim but not the share
      }
    }
    return notLinked
  }

}
