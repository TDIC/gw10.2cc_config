package acc.onbase.enhancement

uses acc.onbase.api.application.DocumentRetrieval
uses entity.ContactDocumentInfo
uses gw.api.locale.DisplayKey
uses gw.api.system.PLLoggerCategory
uses gw.api.util.DisplayableException
uses gw.document.ContentDisposition
uses gw.document.DocumentContentsInfo
uses gw.document.DocumentsUtilBase

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 *
 * 06/15/2017 - Chris Hoffman
 * * Initial implementation, modeled after OnBaseDocumentEnhancement & GWDocumentEnhancement
 */
enhancement OnBaseContactDocumentEnhancement: ContactDocumentInfo {

  public function viewOnBaseDocument() {

    var dci : DocumentContentsInfo
    try {
      var retrievalApp = new DocumentRetrieval()

      dci = retrievalApp.getDocumentContentsInfo(this.DocUID, true)
      if (dci == null) {
        throw new DisplayableException(DisplayKey.get("Document.ViewFailure"))
      }

      DocumentsUtilBase.renderDocumentContentsDirectly(this.Name, dci, ContentDisposition.INLINE)
    }
    catch (e : DisplayableException) {
      throw e
    }
    catch (e : Throwable) {
      PLLoggerCategory.DOCUMENT.info("DownloadContent for doc=" + this.getID() + " [" + this.PublicID + "] caught unexpected exception", e)
      throw new DisplayableException(DisplayKey.get("Document.ViewFailure"), e)
    }
  }

  /**
   * Gets the document content from this document from the server. This is intended as an action for UI widgets that
   * support downloads.  Note that the download attribute should be true, inline or attachment.
   */
  public function downloadContent() {
    var dci : DocumentContentsInfo
    try {
      var retrievalApp = new DocumentRetrieval()

      dci = retrievalApp.getDocumentContentsInfo(this.DocUID, true)
      if (dci == null) {
        throw new DisplayableException(DisplayKey.get("Document.ViewFailure"))
      }
      DocumentsUtilBase.renderDocumentContentsWithDownloadDisposition(this.Name, dci)
    }
    catch (e : DisplayableException) {
      throw e
    }
    catch (e : Throwable) {
      PLLoggerCategory.DOCUMENT.info("DownloadContent for doc=" + this.getID() + " [" + this.PublicID + "] caught unexpected exception", e)
      throw new DisplayableException(DisplayKey.get("Document.ViewFailure"), e)
    }
  }
}
