package acc.onbase.enhancement

uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.system.PLLoggerCategory
uses gw.api.util.DisplayableException
uses gw.document.ContentDisposition
uses gw.document.DocumentContentsInfo
uses gw.document.DocumentsUtilBase
uses gw.plugin.Plugins
uses gw.plugin.document.IDocumentContentSourceBase

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * 04/27/2017 - Tori Brenneison
 * * Initial Implementation
 * <p>
 * 04/16/2018 - Blake Dechant
 * * Added IsAsyncUpdate
 */

/**
 * Enhancement for resetting content disposition for Unity Client view document links
 */

enhancement OnBaseDocumentEnhancement : Document {

  public property get IsAsyncUpdate() : Boolean {

    // these fields and only these fields will be changed on a pending async update
    var fieldsChangedForAsync = {
        this#PendingDocUID.PropertyInfo.Name,
        this#DocUID.PropertyInfo.Name,
        this#BeanVersion.PropertyInfo.Name,
        this#UpdateTime.PropertyInfo.Name,
        this#DocumentIdentifier.PropertyInfo.Name
    }

    // check if our changed fields match the list above
    return this.ChangedFields.subtract(fieldsChangedForAsync).Empty
  }

  public function viewOnBaseDocument() {
    var _dmsPlugin = Plugins.get("IDocumentContentSource") as IDocumentContentSourceBase
    var dci : DocumentContentsInfo
    try {
      dci = _dmsPlugin.getDocumentContentsInfo(this, true)
      if (dci == null) {
        throw new DisplayableException(DisplayKey.get("Document.ViewFailure"))
      }

      DocumentsUtilBase.renderDocumentContentsDirectly(this.Name, dci, ContentDisposition.INLINE)
    } catch (e : DisplayableException) {
      throw e
    } catch (e : Throwable) {
      PLLoggerCategory.DOCUMENT.info("DownloadContent for doc=" + this.getID() + " [" + this.PublicID + "] caught unexpected exception", e)
      throw new DisplayableException(DisplayKey.get("Document.ViewFailure"), e)
    }
  }

  public static function findDocumentByOnBaseID(docuid : String) : Document {
    return Query.make(entity.Document).compare(entity.Document#DocUID, Equals, docuid).select().AtMostOneRow
  }

  public static function findDocumentByPendingDocUID(pendingdocuid : String) : Document {
    return Query.make(entity.Document).compare(entity.Document#PendingDocUID, Equals, pendingdocuid).select().AtMostOneRow
  }

}
