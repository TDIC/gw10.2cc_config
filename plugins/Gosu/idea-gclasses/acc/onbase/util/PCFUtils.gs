package acc.onbase.util

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 04/12/2018 - Daniel Q. Yu
 * * Initial implementation.
 */

/**
 * Utility class for PCFs
 */

class PCFUtils {

  /**
   * If there is only one subtype for a document type, automatically select the only value.
   *
   * @param docType The document type.
   * @return The only subtype or null if more than one.
   */
  public static function getOnlyDocumentSubType(docType : DocumentType) : OnBaseDocumentSubtype_Ext {
    if (docType == null) {
      return null
    }
    var onlySubType : OnBaseDocumentSubtype_Ext = null
    foreach (subtype in OnBaseDocumentSubtype_Ext.AllTypeKeys) {
      if (subtype.hasCategory(docType)) {
        if (onlySubType == null) {
          onlySubType = subtype
        } else {
          return null
        }
      }
    }
    return onlySubType
  }

  /**
   * Used for widgets doesn't update dependent dropdown correctly.
   *
   * @param docType The Document Type.
   * @return A list of subtypes for this document type.
   */
  public static function getDocumentSubTypeRange(docType : DocumentType) : OnBaseDocumentSubtype_Ext[] {
    if(docType == null) {
      return null
    }
    var range = new ArrayList<typekey.OnBaseDocumentSubtype_Ext>()
    foreach (subtype in OnBaseDocumentSubtype_Ext.AllTypeKeys) {
      if (subtype.hasCategory(docType)) {
        range.add(subtype)
      }
    }
    return range.toTypedArray()
  }
}