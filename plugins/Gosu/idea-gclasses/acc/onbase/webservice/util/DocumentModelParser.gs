package acc.onbase.webservice.util

uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Utility class to assist in deserializing document models received via web service calls.
 */
class DocumentModelParser {

  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  private final var _model : acc.onbase.api.si.model.documentmodel.Document

  /**
   * @param model document model
   */
  construct(model : acc.onbase.api.si.model.documentmodel.Document) {
    _model = model
  }

  /**
   * Retrieve DocumentType specified by OnBaseName
   * <p>
   * DocumentType is a required value.
   *
   * @return valid document type
   */
  public property get Type() : DocumentType {
    if (!_model.Type.OnBaseName.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocumentType"))
    }

    return DocumentType.findByOnBaseName(_model.Type.OnBaseName)
  }

  /**
   * Retrieve Subtype specified by OnBaseName
   * <p>
   * Subtype is optional, but must be a valid subtype for the provided document type if present.
   *
   * @return null or valid subtype
   */
  public property get Subtype() : OnBaseDocumentSubtype_Ext {
    if (!_model.Subtype.OnBaseName.HasContent) {
      // Subtype is optional
      return null
    }

    var subtype = OnBaseDocumentSubtype_Ext.findByOnBaseName(_model.Subtype.OnBaseName)
    var doctype = Type;
    if (!subtype.hasCategory(doctype)) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_SubTypeNotRelatedToDocumentType", subtype, doctype))
    }

    return subtype
  }

  /**
   * Retrieve Claim by ClaimNumber
   * <p>
   * Claim Number ID is required and must provide a valid Claim Number
   *
   * @return valid claim
   */
  public property get Claim() : Claim {
    if (_model.Claim == null || !_model.Claim.ClaimNumber.HasContent) {
      // no claim entered
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingClaimNumber"))
    }
    var claim = entity.Claim.finder.findClaimByClaimNumber(_model.Claim.ClaimNumber)
    if (claim == null) {
      //invalid claim entered
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidClaimNumber", _model.Claim.ClaimNumber))
    }
    return claim
  }

  /**
   * Retrieve Exposure by PublicID
   * <p>
   * Exposure is optional and must provide a valid exposure ID
   *
   * @return null or valid exposure
   */
  public property get Exposure() : Exposure {
    if (_model.Exposure == null || !_model.Exposure.PublicID.HasContent) {
      // no exposure related
      return null
    }
    var exposure = Claim.Exposures.firstWhere(\e -> e.PublicID.equalsIgnoreCase(_model.Exposure.PublicID))
    if (exposure == null) {
      //invalid exposure entered
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidExposureID", _model.Exposure.PublicID))
    }
    return exposure
  }


  /**
   * Validate that the Policy Number entered in the model matches the PolicyNumber of the Policy associated with the Claim
   * <p>
   * Policy Number is optional and must be a valid Policy Number if entered
   */
  public function validatePolicy() {
    if (_model.Claim != null && _model.Claim.Policy != null && _model.Claim.Policy.PolicyNumber != Claim.Policy.PolicyNumber) {
      //invalid policy number entered
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidPolicyNumber", _model.Claim.Policy.PolicyNumber, Claim.ClaimNumber))
    }
  }

  /**
   * Validate Status
   *
   * @return valid Status
   */
  public property get Status(): DocumentStatusType {
    if (_model.Status == null) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingStatus"))
    }

    return _model.Status
  }

  /**
   * Validate Mimetype
   *
   * @return valid mimetype
   */
  public property get MimeType(): String {
    if (!_model.MimeType.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingFileType"))
    }

    return _model.MimeType
  }

  /**
   * Validate Pending Document
   *
   * @return document found by the pending docuid
   */
  public function getPendingDocument(): Document {
    validateDocUID()
    var document = Document.findDocumentByOnBaseID(_model.DocUID)
    verifyDocumentIsNull(document)
    if (!_model.PendingDocUID.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingPendingID"))
    } else {
      document = Document.findDocumentByPendingDocUID(_model.PendingDocUID)
      if (document == null) {
        logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownPendingID", _model.PendingDocUID))
      }
    }
    return document
  }

  /**
   * Validate New Document
   *
   * @return new document instance
   */
  public function getNewDocument(): Document {
    validateDocUID()
    verifyDocumentIsNull(Document.findDocumentByOnBaseID(_model.DocUID))
    return new Document()
  }

  /**
   * Validate Existing Document
   *
   * @return document found by docuid
   */
  public function getExistingDocument(): Document {
    validateDocUID()
    var document = Document.findDocumentByOnBaseID(_model.DocUID)
    if (document == null) {
      //throw error if docuid doesn't exist for reindex
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownDocUID", _model.DocUID))
    }
    return document
  }

  /**
   * Verify if the document already exists
   *
   */
  private function verifyDocumentIsNull(document: Document) {
    if (document != null) {
      //throw error if docuid already exists for async/ new document
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_DuplicateDocUID", _model.DocUID))
    }
  }

  /**
   * Verify if the docuid is passed in the model
   *
   */
  private function validateDocUID() {
    if (!_model.DocUID.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocUID"))
    }
  }

  private static function logAndThrow(message : String) {
    _logger.error(message)
    throw new IllegalArgumentException(message)
  }

}