package acc.onbase.webservice

uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey
uses gw.xml.ws.annotation.WsiAvailability
uses gw.xml.ws.annotation.WsiPermissions
uses gw.xml.ws.annotation.WsiWebService

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */


/**
 * Guidewire web service returns claim and exposure metadata information.
 */
@WsiWebService("http://onbase/acc/claim/webservice/QueryClaimAPI")
@WsiPermissions({SystemPermissionType.TC_SOAPADMIN})
@WsiAvailability(MULTIUSER)
class QueryClaimAPI {
  /**
   * Logger for OnBaseDMS
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  /**
   * Web service method returns the claim GX model.
   *
   * @param claimNumber The claim number
   * @return the claim GX model
   */
  function getClaimQueryModel(claimNumber: String): acc.onbase.api.si.model.claimquerymodel.Claim {
    // Get claim for the document.
    var claim: entity.Claim = null
    if (claimNumber != null) {
      claim = entity.Claim.finder.findClaimByClaimNumber(claimNumber)
      if (claim == null) {
        //invalid claim entered
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidClaimNumber", claimNumber))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidClaimNumber", claimNumber))
      }
    } else {
      // no claim entered
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingClaimNumber"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingClaimNumber"))
    }

    return acc.onbase.api.si.model.claimquerymodel.Claim.create(claim)
  }
}
