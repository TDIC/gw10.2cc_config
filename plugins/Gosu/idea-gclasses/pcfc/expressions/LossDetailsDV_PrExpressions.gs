package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/lossdetails/LossDetailsDV.Pr.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LossDetailsDV_PrExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/lossdetails/LossDetailsDV.Pr.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LossDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_159 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.reporter), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_161 () : void {
      if (Claim.reporter != null) { ClaimContactDetailPopup.push(Claim.reporter, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_163 () : void {
      ClaimContactDetailPopup.push(Claim.reporter, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_188 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_190 () : void {
      if (Claim.maincontact != null) { ClaimContactDetailPopup.push(Claim.maincontact, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_192 () : void {
      ClaimContactDetailPopup.push(Claim.maincontact, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_160 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.reporter), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_164 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.reporter, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_189 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_193 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.maincontact, Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Pr.pcf: line 181, column 41
    function def_onEnter_130 (def :  pcf.EditableFixedPropertyIncidentsLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Pr.pcf: line 196, column 99
    function def_onEnter_134 (def :  pcf.EditableInjuryIncidentsLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on InputSetRef at LossDetailsDV.Pr.pcf: line 219, column 42
    function def_onEnter_139 (def :  pcf.CCAddressInputSet) : void {
      def.onEnter(Claim.AddressOwner)
    }
    
    // 'def' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_156 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Claim.reporter), null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_185 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Claim.maincontact), null, Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Pr.pcf: line 311, column 41
    function def_onEnter_220 (def :  pcf.EditableOfficialsLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on ListViewInput (id=WitnessLV) at LossDetailsDV.Pr.pcf: line 326, column 41
    function def_onEnter_223 (def :  pcf.EditableWitnessesLV) : void {
      def.onEnter(Claim.getClaimContactRolesByRole(ContactRole.TC_WITNESS), Claim, ContactRole.TC_WITNESS)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Pr.pcf: line 337, column 25
    function def_onEnter_225 (def :  pcf.EditableContributingFactorsLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Pr.pcf: line 349, column 25
    function def_onEnter_227 (def :  pcf.MetroReportsLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Pr.pcf: line 181, column 41
    function def_refreshVariables_131 (def :  pcf.EditableFixedPropertyIncidentsLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Pr.pcf: line 196, column 99
    function def_refreshVariables_135 (def :  pcf.EditableInjuryIncidentsLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on InputSetRef at LossDetailsDV.Pr.pcf: line 219, column 42
    function def_refreshVariables_140 (def :  pcf.CCAddressInputSet) : void {
      def.refreshVariables(Claim.AddressOwner)
    }
    
    // 'def' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_157 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Claim.reporter), null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_186 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Claim.maincontact), null, Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Pr.pcf: line 311, column 41
    function def_refreshVariables_221 (def :  pcf.EditableOfficialsLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on ListViewInput (id=WitnessLV) at LossDetailsDV.Pr.pcf: line 326, column 41
    function def_refreshVariables_224 (def :  pcf.EditableWitnessesLV) : void {
      def.refreshVariables(Claim.getClaimContactRolesByRole(ContactRole.TC_WITNESS), Claim, ContactRole.TC_WITNESS)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Pr.pcf: line 337, column 25
    function def_refreshVariables_226 (def :  pcf.EditableContributingFactorsLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Pr.pcf: line 349, column 25
    function def_refreshVariables_228 (def :  pcf.MetroReportsLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'value' attribute on DateInput (id=LossDate_Input) at LossDetailsDV.Pr.pcf: line 154, column 41
    function defaultSetter_110 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.LossDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=Claim_PermissionRequired_Input) at LossDetailsDV.Pr.pcf: line 166, column 42
    function defaultSetter_117 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.PermissionRequired = (__VALUE_TO_SET as typekey.ClaimSecurityType)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_ComputerSecurity_Input) at LossDetailsDV.Pr.pcf: line 172, column 26
    function defaultSetter_125 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ComputerSecurity = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Notification_FirstNoticeSuit_Input) at LossDetailsDV.Pr.pcf: line 232, column 42
    function defaultSetter_145 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.FirstNoticeSuit = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_167 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.reporter = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_ReportedByType_Input) at LossDetailsDV.Pr.pcf: line 257, column 41
    function defaultSetter_180 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ReportedByType = (__VALUE_TO_SET as typekey.PersonRelationType)
    }
    
    // 'value' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_196 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.maincontact = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_MainContactType_Input) at LossDetailsDV.Pr.pcf: line 284, column 41
    function defaultSetter_210 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.MainContactType = (__VALUE_TO_SET as typekey.PersonRelationType)
    }
    
    // 'value' attribute on DateInput (id=Notification_DateReportedToAgent_Input) at LossDetailsDV.Pr.pcf: line 302, column 26
    function defaultSetter_215 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.DateRptdToAgent = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextAreaInput (id=ClaimDescription_Input) at LossDetailsDV.Pr.pcf: line 25, column 43
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Pr.pcf: line 59, column 42
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.CallType_TDIC = (__VALUE_TO_SET as String)
    }
    
    // 'value' attribute on TextInput (id=Other_Input) at LossDetailsDV.Pr.pcf: line 69, column 43
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Other_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=DateOfNotice_Input) at LossDetailsDV.Pr.pcf: line 77, column 43
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ReportedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimNotification_HowReported_Input) at LossDetailsDV.Pr.pcf: line 84, column 43
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.HowReported = (__VALUE_TO_SET as typekey.HowReportedType)
    }
    
    // 'value' attribute on TypeKeyInput (id=LossCause_Input) at LossDetailsDV.Pr.pcf: line 100, column 41
    function defaultSetter_68 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.LossCause = (__VALUE_TO_SET as typekey.LossCause)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ArsonInvolved_Input) at LossDetailsDV.Pr.pcf: line 109, column 88
    function defaultSetter_74 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.PropertyFireDamage.Arson = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=LocationOfTheft_Input) at LossDetailsDV.Pr.pcf: line 116, column 77
    function defaultSetter_80 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.LocationOfTheft = (__VALUE_TO_SET as typekey.LocationOfTheft)
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_Fault_Input) at LossDetailsDV.Pr.pcf: line 123, column 41
    function defaultSetter_87 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.FaultRating = (__VALUE_TO_SET as typekey.FaultRating)
    }
    
    // 'value' attribute on TextInput (id=Notification_AtFaultPercentage_Input) at LossDetailsDV.Pr.pcf: line 135, column 73
    function defaultSetter_93 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Fault = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Pr.pcf: line 144, column 42
    function defaultSetter_99 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Catastrophe = (__VALUE_TO_SET as entity.Catastrophe)
    }
    
    // 'initialValue' attribute on Variable at LossDetailsDV.Pr.pcf: line 14, column 53
    function initialValue_0 () : gw.api.claim.FnolWizardPropertyHelper {
      return new gw.api.claim.FnolWizardPropertyHelper(Claim)
    }
    
    // 'onChange' attribute on PostOnChange at LossDetailsDV.Pr.pcf: line 156, column 74
    function onChange_106 () : void {
      gw.pcf.ClaimLossDetailsUtil.changedLossDate(Claim)
    }
    
    // 'onChange' attribute on PostOnChange at LossDetailsDV.Pr.pcf: line 102, column 70
    function onChange_65 () : void {
      PropertyHelper.createDamageTypesPerLossCause()
    }
    
    // 'onChange' attribute on PostOnChange at LossDetailsDV.Pr.pcf: line 126, column 32
    function onChange_84 () : void {
      Claim.createSubrogationActivity()
    }
    
    // 'onPick' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_165 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.reporter); var result = eval("Claim.reporter = Claim.resolveContact(Claim.reporter) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_194 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.maincontact); var result = eval("Claim.maincontact = Claim.resolveContact(Claim.maincontact) as " + contactType.Name + ";return null;"); ;
    }
    
    // Reflect at LossDetailsDV.Pr.pcf: line 259, column 42
    function reflectionValue_177 (TRIGGER_INDEX :  int, VALUE :  entity.Contact) : java.lang.Object {
      return (VALUE==Claim.Insured) ? ("self") : (true) ? ("") : "<NOCHANGE>"
    }
    
    // Reflect at LossDetailsDV.Pr.pcf: line 286, column 43
    function reflectionValue_207 (TRIGGER_INDEX :  int, VALUE :  entity.Person) : java.lang.Object {
      return (VALUE==Claim.Insured) ? ("self") : (true) ? ("") : "<NOCHANGE>"
    }
    
    // 'required' attribute on TextInput (id=Other_Input) at LossDetailsDV.Pr.pcf: line 69, column 43
    function required_39 () : java.lang.Boolean {
      return Claim.CallType_TDIC == "Other"
    }
    
    // 'validationExpression' attribute on DateInput (id=DateOfNotice_Input) at LossDetailsDV.Pr.pcf: line 77, column 43
    function validationExpression_45 () : java.lang.Object {
      return Claim.ReportedDate != null and Claim.ReportedDate > gw.api.util.DateUtil.currentDate() ? DisplayKey.get("Java.Validation.Date.ForbidFuture") : null
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_LossDate_Input) at LossDetailsDV.Pr.pcf: line 33, column 43
    function validationExpression_7 () : java.lang.Object {
      return Claim.LossDate == null || Claim.LossDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Pr.pcf: line 144, column 42
    function valueRange_101 () : java.lang.Object {
      return gw.api.admin.CatastropheUtil.getCatastrophes()
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at LossDetailsDV.Pr.pcf: line 166, column 42
    function valueRange_119 () : java.lang.Object {
      return gw.api.claim.ClaimUtil.getAvailableTypes()
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_169 () : java.lang.Object {
      return Claim.RelatedPersonArray
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Pr.pcf: line 59, column 42
    function valueRange_33 () : java.lang.Object {
      return Claim.getCallType(Claim)
    }
    
    // 'value' attribute on TextAreaInput (id=ClaimDescription_Input) at LossDetailsDV.Pr.pcf: line 25, column 43
    function valueRoot_4 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on BooleanRadioInput (id=ArsonInvolved_Input) at LossDetailsDV.Pr.pcf: line 109, column 88
    function valueRoot_75 () : java.lang.Object {
      return Claim.PropertyFireDamage
    }
    
    // 'value' attribute on RangeInput (id=Claim_PermissionRequired_Input) at LossDetailsDV.Pr.pcf: line 166, column 42
    function value_116 () : typekey.ClaimSecurityType {
      return Claim.PermissionRequired
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_ComputerSecurity_Input) at LossDetailsDV.Pr.pcf: line 172, column 26
    function value_124 () : java.lang.Boolean {
      return Claim.ComputerSecurity
    }
    
    // 'value' attribute on BooleanRadioInput (id=Notification_FirstNoticeSuit_Input) at LossDetailsDV.Pr.pcf: line 232, column 42
    function value_144 () : java.lang.Boolean {
      return Claim.FirstNoticeSuit
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossType_Input) at LossDetailsDV.Pr.pcf: line 40, column 43
    function value_15 () : typekey.LossType {
      return Claim.LossType
    }
    
    // 'value' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_166 () : entity.Contact {
      return Claim.reporter
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_ReportedByType_Input) at LossDetailsDV.Pr.pcf: line 257, column 41
    function value_179 () : typekey.PersonRelationType {
      return Claim.ReportedByType
    }
    
    // 'value' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_195 () : entity.Person {
      return Claim.maincontact
    }
    
    // 'value' attribute on TextAreaInput (id=ClaimDescription_Input) at LossDetailsDV.Pr.pcf: line 25, column 43
    function value_2 () : java.lang.String {
      return Claim.Description
    }
    
    // 'value' attribute on BooleanRadioInput (id=RiskManagementIncident_Input) at LossDetailsDV.Pr.pcf: line 45, column 43
    function value_20 () : java.lang.Boolean {
      return Claim.RiskMgmtIncident_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_MainContactType_Input) at LossDetailsDV.Pr.pcf: line 284, column 41
    function value_209 () : typekey.PersonRelationType {
      return Claim.MainContactType
    }
    
    // 'value' attribute on DateInput (id=Notification_DateReportedToAgent_Input) at LossDetailsDV.Pr.pcf: line 302, column 26
    function value_214 () : java.util.Date {
      return Claim.DateRptdToAgent
    }
    
    // 'value' attribute on CheckBoxInput (id=Status_IncidentReport_Input) at LossDetailsDV.Pr.pcf: line 50, column 43
    function value_25 () : java.lang.Boolean {
      return Claim.IncidentReport
    }
    
    // 'value' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Pr.pcf: line 59, column 42
    function value_30 () : String {
      return Claim.CallType_TDIC
    }
    
    // 'value' attribute on TextInput (id=Other_Input) at LossDetailsDV.Pr.pcf: line 69, column 43
    function value_40 () : java.lang.String {
      return Claim.Other_TDIC
    }
    
    // 'value' attribute on DateInput (id=DateOfNotice_Input) at LossDetailsDV.Pr.pcf: line 77, column 43
    function value_47 () : java.util.Date {
      return Claim.ReportedDate
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimNotification_HowReported_Input) at LossDetailsDV.Pr.pcf: line 84, column 43
    function value_54 () : typekey.HowReportedType {
      return Claim.HowReported
    }
    
    // 'value' attribute on TypeKeyInput (id=LossCause_Input) at LossDetailsDV.Pr.pcf: line 100, column 41
    function value_67 () : typekey.LossCause {
      return Claim.LossCause
    }
    
    // 'value' attribute on BooleanRadioInput (id=ArsonInvolved_Input) at LossDetailsDV.Pr.pcf: line 109, column 88
    function value_73 () : java.lang.Boolean {
      return Claim.PropertyFireDamage.Arson
    }
    
    // 'value' attribute on TypeKeyInput (id=LocationOfTheft_Input) at LossDetailsDV.Pr.pcf: line 116, column 77
    function value_79 () : typekey.LocationOfTheft {
      return Claim.LocationOfTheft
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_Fault_Input) at LossDetailsDV.Pr.pcf: line 123, column 41
    function value_86 () : typekey.FaultRating {
      return Claim.FaultRating
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at LossDetailsDV.Pr.pcf: line 33, column 43
    function value_9 () : java.util.Date {
      return Claim.LossDate
    }
    
    // 'value' attribute on TextInput (id=Notification_AtFaultPercentage_Input) at LossDetailsDV.Pr.pcf: line 135, column 73
    function value_92 () : java.math.BigDecimal {
      return Claim.Fault
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Pr.pcf: line 144, column 42
    function value_98 () : entity.Catastrophe {
      return Claim.Catastrophe
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Pr.pcf: line 144, column 42
    function verifyValueRangeIsAllowedType_102 ($$arg :  entity.Catastrophe[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Pr.pcf: line 144, column 42
    function verifyValueRangeIsAllowedType_102 ($$arg :  gw.api.database.IQueryBeanResult<entity.Catastrophe>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Pr.pcf: line 144, column 42
    function verifyValueRangeIsAllowedType_102 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at LossDetailsDV.Pr.pcf: line 166, column 42
    function verifyValueRangeIsAllowedType_120 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at LossDetailsDV.Pr.pcf: line 166, column 42
    function verifyValueRangeIsAllowedType_120 ($$arg :  typekey.ClaimSecurityType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_170 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_170 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_170 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_199 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_199 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_199 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Pr.pcf: line 59, column 42
    function verifyValueRangeIsAllowedType_34 ($$arg :  String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Pr.pcf: line 59, column 42
    function verifyValueRangeIsAllowedType_34 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Pr.pcf: line 144, column 42
    function verifyValueRange_103 () : void {
      var __valueRangeArg = gw.api.admin.CatastropheUtil.getCatastrophes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_102(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at LossDetailsDV.Pr.pcf: line 166, column 42
    function verifyValueRange_121 () : void {
      var __valueRangeArg = gw.api.claim.ClaimUtil.getAvailableTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_120(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_171 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_170(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_200 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_199(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Pr.pcf: line 59, column 42
    function verifyValueRange_35 () : void {
      var __valueRangeArg = Claim.getCallType(Claim)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_34(__valueRangeArg)
    }
    
    // 'valueType' attribute on ClaimContactInput (id=MainContact_Picker_Input) at LossDetailsDV.Pr.pcf: line 277, column 42
    function verifyValueType_206 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'visible' attribute on TextAreaInput (id=ClaimDescription_Input) at LossDetailsDV.Pr.pcf: line 25, column 43
    function visible_1 () : java.lang.Boolean {
      return !Claim.Policy.Verified
    }
    
    // 'visible' attribute on InputDivider at LossDetailsDV.Pr.pcf: line 189, column 100
    function visible_132 () : java.lang.Boolean {
      return Claim.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY and Claim.Policy.Verified
    }
    
    // 'visible' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_155 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_158 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.reporter), Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_187 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on TextAreaInput (id=Description_Input) at LossDetailsDV.Pr.pcf: line 92, column 42
    function visible_59 () : java.lang.Boolean {
      return Claim.Policy.Verified
    }
    
    // 'visible' attribute on BooleanRadioInput (id=ArsonInvolved_Input) at LossDetailsDV.Pr.pcf: line 109, column 88
    function visible_72 () : java.lang.Boolean {
      return PropertyHelper.PropertyFireDamage.Present and Claim.Policy.Verified
    }
    
    // 'visible' attribute on TypeKeyInput (id=LocationOfTheft_Input) at LossDetailsDV.Pr.pcf: line 116, column 77
    function visible_78 () : java.lang.Boolean {
      return Claim.LossCause == TC_BURGLARY and Claim.Policy.Verified
    }
    
    // 'visible' attribute on TextInput (id=Notification_AtFaultPercentage_Input) at LossDetailsDV.Pr.pcf: line 135, column 73
    function visible_91 () : java.lang.Boolean {
      return  Claim.FaultRating == TC_1 and Claim.Policy.Verified
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get PropertyHelper () : gw.api.claim.FnolWizardPropertyHelper {
      return getVariableValue("PropertyHelper", 0) as gw.api.claim.FnolWizardPropertyHelper
    }
    
    property set PropertyHelper ($arg :  gw.api.claim.FnolWizardPropertyHelper) {
      setVariableValue("PropertyHelper", 0, $arg)
    }
    
    function createPropertyFireDamageIfNecessary() {
      var claimProperty = entity.Claim.Type.TypeInfo.getProperty("PropertyFireDamage")
      
      if(Claim.LossCause == TC_FIRE) {
        var constructor = claimProperty.FeatureType.TypeInfo.getConstructor({})
        claimProperty.Accessor.setValue(Claim, constructor.Constructor.newInstance({}))
      } else {
        var bean = claimProperty.Accessor.getValue(Claim) as KeyableBean
        if(bean!=null) {
          bean.remove()
          claimProperty.Accessor.setValue(Claim, null)
        }
      }
    }
    
    
  }
  
  
}