package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.financials.CurrencyAmount
uses java.math.BigDecimal
@javax.annotation.Generated("config/web/pcf/claim/litigation/MatterDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class MatterDetailsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/litigation/MatterDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MatterDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_101 () : void {
      ClaimContactDetailPopup.push(Matter.plaintiffatt, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_120 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.plaintifffirm), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_122 () : void {
      if (Matter.plaintifffirm != null) { ClaimContactDetailPopup.push(Matter.plaintifffirm, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_124 () : void {
      ClaimContactDetailPopup.push(Matter.plaintifffirm, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_143 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.defenseattorney), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_145 () : void {
      if (Matter.defenseattorney != null) { ClaimContactDetailPopup.push(Matter.defenseattorney, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_147 () : void {
      ClaimContactDetailPopup.push(Matter.defenseattorney, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_166 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.defensefirm), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_168 () : void {
      if (Matter.defensefirm != null) { ClaimContactDetailPopup.push(Matter.defensefirm, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_170 () : void {
      ClaimContactDetailPopup.push(Matter.defensefirm, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_219 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.judge), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_22 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.plaintiff), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_221 () : void {
      if (Matter.judge != null) { ClaimContactDetailPopup.push(Matter.judge, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_223 () : void {
      ClaimContactDetailPopup.push(Matter.judge, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_24 () : void {
      if (Matter.plaintiff != null) { ClaimContactDetailPopup.push(Matter.plaintiff, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_25 () : void {
      ClaimContactDetailPopup.push(Matter.plaintiff, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_293 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.hearingvenue), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_295 () : void {
      if (Matter.hearingvenue != null) { ClaimContactDetailPopup.push(Matter.hearingvenue, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_297 () : void {
      ClaimContactDetailPopup.push(Matter.hearingvenue, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_322 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.hearingjudge), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_324 () : void {
      if (Matter.hearingjudge != null) { ClaimContactDetailPopup.push(Matter.hearingjudge, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_326 () : void {
      ClaimContactDetailPopup.push(Matter.hearingjudge, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_354 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.mediationvenue), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_356 () : void {
      if (Matter.mediationvenue != null) { ClaimContactDetailPopup.push(Matter.mediationvenue, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_358 () : void {
      ClaimContactDetailPopup.push(Matter.mediationvenue, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_383 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.mediator), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_385 () : void {
      if (Matter.mediator != null) { ClaimContactDetailPopup.push(Matter.mediator, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_387 () : void {
      ClaimContactDetailPopup.push(Matter.mediator, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_41 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.defendant), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_420 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.filedby), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_422 () : void {
      if (Matter.filedby != null) { ClaimContactDetailPopup.push(Matter.filedby, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_424 () : void {
      ClaimContactDetailPopup.push(Matter.filedby, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_43 () : void {
      if (Matter.defendant != null) { ClaimContactDetailPopup.push(Matter.defendant, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_44 () : void {
      ClaimContactDetailPopup.push(Matter.defendant, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_97 () : void {
      AddressBookPickerPopup.push(statictypeof (Matter.plaintiffatt), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_99 () : void {
      if (Matter.plaintiffatt != null) { ClaimContactDetailPopup.push(Matter.plaintiffatt, Matter.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_102 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.plaintiffatt, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_121 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.plaintifffirm), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_125 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.plaintifffirm, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_144 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.defenseattorney), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_148 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.defenseattorney, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_167 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.defensefirm), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_171 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.defensefirm, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_220 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.judge), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_224 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.judge, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_23 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.plaintiff), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_26 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.plaintiff, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_294 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.hearingvenue), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_298 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.hearingvenue, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_323 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.hearingjudge), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_327 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.hearingjudge, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_355 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.mediationvenue), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_359 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.mediationvenue, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_384 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.mediator), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_388 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.mediator, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_42 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.defendant), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_421 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.filedby), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_425 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.filedby, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_45 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Matter.defendant, Matter.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_98 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Matter.plaintiffatt), Matter.Claim, null as List<SpecialistService>)
    }
    
    // 'def' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_117 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Matter.plaintifffirm), null, Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_140 (def :  pcf.TDIC_AttorneyOnlyMenuItemSet) : void {
      def.onEnter(Matter.Claim , "Defense Attorney")
    }
    
    // 'def' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_163 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Matter.defensefirm), null, Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_19 (def :  pcf.TDIC_NegotiationNewContactMenuItemSet) : void {
      def.onEnter(Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_216 (def :  pcf.TDIC_AdjudicatorOnlyMenuItemSet) : void {
      def.onEnter(Matter.Claim,"Mediator")
    }
    
    // 'def' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_290 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Matter.hearingvenue), null, Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_319 (def :  pcf.TDIC_AdjudicatorOnlyMenuItemSet) : void {
      def.onEnter(Matter.Claim,"Mediator")
    }
    
    // 'def' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_351 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Matter.mediationvenue), null, Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_38 (def :  pcf.TDIC_NegotiationNewContactMenuItemSet) : void {
      def.onEnter(Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_380 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Matter.mediator), null, Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_417 (def :  pcf.TDIC_NegotiationNewContactMenuItemSet) : void {
      def.onEnter(Matter.Claim)
    }
    
    // 'def' attribute on ListViewInput (id=MatterGeneral_Status) at MatterDetailsDV.pcf: line 646, column 27
    function def_onEnter_510 (def :  pcf.EditableMatterStatusLinesLV) : void {
      def.onEnter(Matter)
    }
    
    // 'def' attribute on ListViewInput (id=Counsel_SecAttorney) at MatterDetailsDV.pcf: line 658, column 27
    function def_onEnter_512 (def :  pcf.EditableSecondaryAttorneyLV) : void {
      def.onEnter(Matter)
    }
    
    // 'def' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_94 (def :  pcf.TDIC_AttorneyOnlyMenuItemSet) : void {
      def.onEnter(Matter.Claim , "Plaintiff Attorney")
    }
    
    // 'def' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_118 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Matter.plaintifffirm), null, Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_141 (def :  pcf.TDIC_AttorneyOnlyMenuItemSet) : void {
      def.refreshVariables(Matter.Claim , "Defense Attorney")
    }
    
    // 'def' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_164 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Matter.defensefirm), null, Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_20 (def :  pcf.TDIC_NegotiationNewContactMenuItemSet) : void {
      def.refreshVariables(Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_217 (def :  pcf.TDIC_AdjudicatorOnlyMenuItemSet) : void {
      def.refreshVariables(Matter.Claim,"Mediator")
    }
    
    // 'def' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_291 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Matter.hearingvenue), null, Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_320 (def :  pcf.TDIC_AdjudicatorOnlyMenuItemSet) : void {
      def.refreshVariables(Matter.Claim,"Mediator")
    }
    
    // 'def' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_352 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Matter.mediationvenue), null, Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_381 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Matter.mediator), null, Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_39 (def :  pcf.TDIC_NegotiationNewContactMenuItemSet) : void {
      def.refreshVariables(Matter.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_418 (def :  pcf.TDIC_NegotiationNewContactMenuItemSet) : void {
      def.refreshVariables(Matter.Claim)
    }
    
    // 'def' attribute on ListViewInput (id=MatterGeneral_Status) at MatterDetailsDV.pcf: line 646, column 27
    function def_refreshVariables_511 (def :  pcf.EditableMatterStatusLinesLV) : void {
      def.refreshVariables(Matter)
    }
    
    // 'def' attribute on ListViewInput (id=Counsel_SecAttorney) at MatterDetailsDV.pcf: line 658, column 27
    function def_refreshVariables_513 (def :  pcf.EditableSecondaryAttorneyLV) : void {
      def.refreshVariables(Matter)
    }
    
    // 'def' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_95 (def :  pcf.TDIC_AttorneyOnlyMenuItemSet) : void {
      def.refreshVariables(Matter.Claim , "Plaintiff Attorney")
    }
    
    // 'value' attribute on TextInput (id=Matter_Name_Input) at MatterDetailsDV.pcf: line 19, column 30
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_105 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.plaintiffatt = (__VALUE_TO_SET as entity.Attorney)
    }
    
    // 'value' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_128 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.plaintifffirm = (__VALUE_TO_SET as entity.LawFirm)
    }
    
    // 'value' attribute on TypeKeyInput (id=TrialDetails_MatterType_Input) at MatterDetailsDV.pcf: line 40, column 40
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.MatterType = (__VALUE_TO_SET as typekey.MatterType)
    }
    
    // 'value' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_151 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.defenseattorney = (__VALUE_TO_SET as entity.Attorney)
    }
    
    // 'value' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_174 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.defensefirm = (__VALUE_TO_SET as entity.LawFirm)
    }
    
    // 'value' attribute on DateInput (id=Counsel_DefenseApptDate_Input) at MatterDetailsDV.pcf: line 169, column 216
    function defaultSetter_188 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.DefenseApptDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=Counsel_SentToDefenseDate_Input) at MatterDetailsDV.pcf: line 176, column 216
    function defaultSetter_194 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.SentToDefenseDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=TrialDetails_TrialDate_Input) at MatterDetailsDV.pcf: line 193, column 142
    function defaultSetter_202 (__VALUE_TO_SET :  java.lang.Object) : void {
      TrialDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=Matter_Venue_TDIC_Input) at MatterDetailsDV.pcf: line 210, column 36
    function defaultSetter_206 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.Venue_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=TrialDetails_Room_Input) at MatterDetailsDV.pcf: line 217, column 142
    function defaultSetter_211 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.Room = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_227 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.judge = (__VALUE_TO_SET as entity.Adjudicator)
    }
    
    // 'value' attribute on DateInput (id=ArbitrationDetails_TrialDate_Input) at MatterDetailsDV.pcf: line 244, column 153
    function defaultSetter_243 (__VALUE_TO_SET :  java.lang.Object) : void {
      ArbitrationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=ArbitrationDetails_Venue_Input) at MatterDetailsDV.pcf: line 263, column 9
    function defaultSetter_249 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.Mediationvenue_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=ArbitrationDetails_Arbitrator_Input) at MatterDetailsDV.pcf: line 291, column 9
    function defaultSetter_255 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.Mediator_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=HearingDetails_HearingDate_Input) at MatterDetailsDV.pcf: line 368, column 142
    function defaultSetter_285 (__VALUE_TO_SET :  java.lang.Object) : void {
      HearingDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_29 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.plaintiff = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_301 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.hearingvenue = (__VALUE_TO_SET as entity.LegalVenue)
    }
    
    // 'value' attribute on TextInput (id=HearingDetails_Room_Input) at MatterDetailsDV.pcf: line 385, column 142
    function defaultSetter_314 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.HearingRoom = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_330 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.hearingjudge = (__VALUE_TO_SET as entity.Adjudicator)
    }
    
    // 'value' attribute on DateInput (id=MediationDetails_MediationDate_Input) at MatterDetailsDV.pcf: line 411, column 54
    function defaultSetter_346 (__VALUE_TO_SET :  java.lang.Object) : void {
      MediationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_362 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.mediationvenue = (__VALUE_TO_SET as entity.LegalVenue)
    }
    
    // 'value' attribute on TextInput (id=MediationDetails_Room_Input) at MatterDetailsDV.pcf: line 428, column 54
    function defaultSetter_375 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.MediationRoom = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_391 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.mediator = (__VALUE_TO_SET as entity.Adjudicator)
    }
    
    // 'value' attribute on TextInput (id=DocketNumber_Input) at MatterDetailsDV.pcf: line 515, column 216
    function defaultSetter_406 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.DocketNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=TrialDetails_FilingDate_Input) at MatterDetailsDV.pcf: line 524, column 216
    function defaultSetter_412 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.FileDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_428 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.filedby = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on DateInput (id=ServiceDate_Input) at MatterDetailsDV.pcf: line 541, column 216
    function defaultSetter_440 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.ServiceDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=MethodServed_Input) at MatterDetailsDV.pcf: line 549, column 216
    function defaultSetter_446 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.MethodServed = (__VALUE_TO_SET as typekey.MatterMethodServed)
    }
    
    // 'value' attribute on DateInput (id=ResponseDue_Input) at MatterDetailsDV.pcf: line 556, column 216
    function defaultSetter_452 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.ResponseDue = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ResponseFiled_Input) at MatterDetailsDV.pcf: line 563, column 161
    function defaultSetter_458 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.ResponseFiled = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AdDamnumSpecified_Input) at MatterDetailsDV.pcf: line 571, column 320
    function defaultSetter_465 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.AdDamnumSpecified = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CurrencyInput (id=AdDamnumAmount_Input) at MatterDetailsDV.pcf: line 581, column 118
    function defaultSetter_472 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.AdDamnumAmount = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on BooleanRadioInput (id=PunitiveDamages_Input) at MatterDetailsDV.pcf: line 589, column 318
    function defaultSetter_479 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.PunitiveDamages = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.defendant = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on CurrencyInput (id=PunitiveAmount_Input) at MatterDetailsDV.pcf: line 599, column 116
    function defaultSetter_486 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.PunitiveAmount = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on TypeKeyInput (id=Matter_Resolution_Input) at MatterDetailsDV.pcf: line 610, column 45
    function defaultSetter_492 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.Resolution = (__VALUE_TO_SET as typekey.ResolutionType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Matter_SettlementType_Input) at MatterDetailsDV.pcf: line 617, column 50
    function defaultSetter_496 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.SettlementType_TDIC = (__VALUE_TO_SET as typekey.SettlementType_TDIC)
    }
    
    // 'value' attribute on TextInput (id=Matter_CaseNumber_Input) at MatterDetailsDV.pcf: line 25, column 36
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.CaseNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CurrencyInput (id=Matter_FinalLegalCost_Input) at MatterDetailsDV.pcf: line 624, column 40
    function defaultSetter_500 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.FinalLegalCost = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on CurrencyInput (id=Matter_FinalSettleCost_Input) at MatterDetailsDV.pcf: line 631, column 35
    function defaultSetter_504 (__VALUE_TO_SET :  java.lang.Object) : void {
      SettlementAmount = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on DateInput (id=Matter_FinalSettleDate_Input) at MatterDetailsDV.pcf: line 637, column 41
    function defaultSetter_507 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.FinalSettleDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=TrialDetails_SubroRelated_Input) at MatterDetailsDV.pcf: line 66, column 38
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.SubroRelated = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=CourtType_Input) at MatterDetailsDV.pcf: line 87, column 140
    function defaultSetter_69 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.CourtType = (__VALUE_TO_SET as typekey.MatterCourtType)
    }
    
    // 'value' attribute on TypeKeyInput (id=CourtDistrict_Input) at MatterDetailsDV.pcf: line 97, column 142
    function defaultSetter_75 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.CourtDistrict = (__VALUE_TO_SET as typekey.MatterCourtDistrict)
    }
    
    // 'value' attribute on TypeKeyInput (id=LegalSpecialty_Input) at MatterDetailsDV.pcf: line 105, column 142
    function defaultSetter_81 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.LegalSpecialty = (__VALUE_TO_SET as typekey.LegalSpecialty)
    }
    
    // 'value' attribute on TypeKeyInput (id=TrialDetails_PrimaryCause2_Input) at MatterDetailsDV.pcf: line 113, column 142
    function defaultSetter_87 (__VALUE_TO_SET :  java.lang.Object) : void {
      Matter.PrimaryCause = (__VALUE_TO_SET as typekey.PrimaryCauseType)
    }
    
    // 'editable' attribute on DateInput (id=ArbitrationDetails_TrialDate_Input) at MatterDetailsDV.pcf: line 244, column 153
    function editable_240 () : java.lang.Boolean {
      return Matter.ArbitrationDateActivity == null || Matter.ArbitrationDateActivity.canEdit()
    }
    
    // 'editable' attribute on DateInput (id=HearingDetails_HearingDate_Input) at MatterDetailsDV.pcf: line 368, column 142
    function editable_282 () : java.lang.Boolean {
      return Matter.HearingDateActivity == null || Matter.HearingDateActivity.canEdit()
    }
    
    // 'editable' attribute on DateInput (id=MediationDetails_MediationDate_Input) at MatterDetailsDV.pcf: line 411, column 54
    function editable_343 () : java.lang.Boolean {
      return Matter.MediationDateActivity == null || Matter.MediationDateActivity.canEdit()
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_103 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.plaintiffatt); var result = eval("Matter.plaintiffatt = Matter.Claim.resolveContact(Matter.plaintiffatt) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_126 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.plaintifffirm); var result = eval("Matter.plaintifffirm = Matter.Claim.resolveContact(Matter.plaintifffirm) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_149 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.defenseattorney); var result = eval("Matter.defenseattorney = Matter.Claim.resolveContact(Matter.defenseattorney) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_172 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.defensefirm); var result = eval("Matter.defensefirm = Matter.Claim.resolveContact(Matter.defensefirm) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_225 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.judge); var result = eval("Matter.judge = Matter.Claim.resolveContact(Matter.judge) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_27 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.plaintiff); var result = eval("Matter.plaintiff = Matter.Claim.resolveContact(Matter.plaintiff) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_299 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.hearingvenue); var result = eval("Matter.hearingvenue = Matter.Claim.resolveContact(Matter.hearingvenue) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_328 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.hearingjudge); var result = eval("Matter.hearingjudge = Matter.Claim.resolveContact(Matter.hearingjudge) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_360 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.mediationvenue); var result = eval("Matter.mediationvenue = Matter.Claim.resolveContact(Matter.mediationvenue) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_389 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.mediator); var result = eval("Matter.mediator = Matter.Claim.resolveContact(Matter.mediator) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_426 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.filedby); var result = eval("Matter.filedby = Matter.Claim.resolveContact(Matter.filedby) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_46 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Matter.defendant); var result = eval("Matter.defendant = Matter.Claim.resolveContact(Matter.defendant) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'validationExpression' attribute on BooleanRadioInput (id=AdDamnumSpecified_Input) at MatterDetailsDV.pcf: line 571, column 320
    function validationExpression_462 () : java.lang.Object {
      return (!Matter.AdDamnumSpecified and !(Matter.AdDamnumAmount == null || Matter.AdDamnumAmount.Amount == 0)? DisplayKey.get("Matter.AdDamnumSpecified.Validation"): null)
    }
    
    // 'validationExpression' attribute on BooleanRadioInput (id=PunitiveDamages_Input) at MatterDetailsDV.pcf: line 589, column 318
    function validationExpression_476 () : java.lang.Object {
      return (!Matter.PunitiveDamages and !(Matter.PunitiveAmount == null || Matter.PunitiveAmount.Amount == 0)? DisplayKey.get("Matter.PunitiveDamagesSpecified.Validation"): null)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_107 () : java.lang.Object {
      return Matter.Claim.RelatedAttorneyArray
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_130 () : java.lang.Object {
      return Matter.Claim.RelatedLawFirmArray
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_229 () : java.lang.Object {
      return Matter.Claim.RelatedAdjudicatorArray
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_303 () : java.lang.Object {
      return Matter.Claim.RelatedLegalVenueArray
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_31 () : java.lang.Object {
      return Matter.Claim.RelatedContacts
    }
    
    // 'value' attribute on TextInput (id=Matter_Name_Input) at MatterDetailsDV.pcf: line 19, column 30
    function valueRoot_2 () : java.lang.Object {
      return Matter
    }
    
    // 'value' attribute on TextInput (id=Matter_Name_Input) at MatterDetailsDV.pcf: line 19, column 30
    function value_0 () : java.lang.String {
      return Matter.Name
    }
    
    // 'value' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_104 () : entity.Attorney {
      return Matter.plaintiffatt
    }
    
    // 'value' attribute on TextInput (id=AssignedGroup_Name_Input) at MatterDetailsDV.pcf: line 33, column 56
    function value_11 () : java.lang.String {
      return Matter.AssigneeGroupOnlyDisplayString
    }
    
    // 'value' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_127 () : entity.LawFirm {
      return Matter.plaintifffirm
    }
    
    // 'value' attribute on TypeKeyInput (id=TrialDetails_MatterType_Input) at MatterDetailsDV.pcf: line 40, column 40
    function value_14 () : typekey.MatterType {
      return Matter.MatterType
    }
    
    // 'value' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_150 () : entity.Attorney {
      return Matter.defenseattorney
    }
    
    // 'value' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_173 () : entity.LawFirm {
      return Matter.defensefirm
    }
    
    // 'value' attribute on DateInput (id=Counsel_DefenseApptDate_Input) at MatterDetailsDV.pcf: line 169, column 216
    function value_187 () : java.util.Date {
      return Matter.DefenseApptDate
    }
    
    // 'value' attribute on DateInput (id=Counsel_SentToDefenseDate_Input) at MatterDetailsDV.pcf: line 176, column 216
    function value_193 () : java.util.Date {
      return Matter.SentToDefenseDate
    }
    
    // 'value' attribute on DateInput (id=TrialDetails_TrialDate_Input) at MatterDetailsDV.pcf: line 193, column 142
    function value_201 () : java.util.Date {
      return TrialDate
    }
    
    // 'value' attribute on TextInput (id=Matter_Venue_TDIC_Input) at MatterDetailsDV.pcf: line 210, column 36
    function value_205 () : java.lang.String {
      return Matter.Venue_TDIC
    }
    
    // 'value' attribute on TextInput (id=TrialDetails_Room_Input) at MatterDetailsDV.pcf: line 217, column 142
    function value_210 () : java.lang.String {
      return Matter.Room
    }
    
    // 'value' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_226 () : entity.Adjudicator {
      return Matter.judge
    }
    
    // 'value' attribute on DateInput (id=ArbitrationDetails_TrialDate_Input) at MatterDetailsDV.pcf: line 244, column 153
    function value_242 () : java.util.Date {
      return ArbitrationDate
    }
    
    // 'value' attribute on TextInput (id=ArbitrationDetails_Venue_Input) at MatterDetailsDV.pcf: line 263, column 9
    function value_248 () : java.lang.String {
      return Matter.Mediationvenue_TDIC
    }
    
    // 'value' attribute on TextInput (id=ArbitrationDetails_Arbitrator_Input) at MatterDetailsDV.pcf: line 291, column 9
    function value_254 () : java.lang.String {
      return Matter.Mediator_TDIC
    }
    
    // 'value' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_28 () : entity.Contact {
      return Matter.plaintiff
    }
    
    // 'value' attribute on DateInput (id=HearingDetails_HearingDate_Input) at MatterDetailsDV.pcf: line 368, column 142
    function value_284 () : java.util.Date {
      return HearingDate
    }
    
    // 'value' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_300 () : entity.LegalVenue {
      return Matter.hearingvenue
    }
    
    // 'value' attribute on TextInput (id=HearingDetails_Room_Input) at MatterDetailsDV.pcf: line 385, column 142
    function value_313 () : java.lang.String {
      return Matter.HearingRoom
    }
    
    // 'value' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_329 () : entity.Adjudicator {
      return Matter.hearingjudge
    }
    
    // 'value' attribute on DateInput (id=MediationDetails_MediationDate_Input) at MatterDetailsDV.pcf: line 411, column 54
    function value_345 () : java.util.Date {
      return MediationDate
    }
    
    // 'value' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_361 () : entity.LegalVenue {
      return Matter.mediationvenue
    }
    
    // 'value' attribute on TextInput (id=MediationDetails_Room_Input) at MatterDetailsDV.pcf: line 428, column 54
    function value_374 () : java.lang.String {
      return Matter.MediationRoom
    }
    
    // 'value' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_390 () : entity.Adjudicator {
      return Matter.mediator
    }
    
    // 'value' attribute on TextInput (id=Matter_CaseNumber_Input) at MatterDetailsDV.pcf: line 25, column 36
    function value_4 () : java.lang.String {
      return Matter.CaseNumber
    }
    
    // 'value' attribute on TextInput (id=DocketNumber_Input) at MatterDetailsDV.pcf: line 515, column 216
    function value_405 () : java.lang.String {
      return Matter.DocketNumber
    }
    
    // 'value' attribute on DateInput (id=TrialDetails_FilingDate_Input) at MatterDetailsDV.pcf: line 524, column 216
    function value_411 () : java.util.Date {
      return Matter.FileDate
    }
    
    // 'value' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_427 () : entity.Contact {
      return Matter.filedby
    }
    
    // 'value' attribute on DateInput (id=ServiceDate_Input) at MatterDetailsDV.pcf: line 541, column 216
    function value_439 () : java.util.Date {
      return Matter.ServiceDate
    }
    
    // 'value' attribute on TypeKeyInput (id=MethodServed_Input) at MatterDetailsDV.pcf: line 549, column 216
    function value_445 () : typekey.MatterMethodServed {
      return Matter.MethodServed
    }
    
    // 'value' attribute on DateInput (id=ResponseDue_Input) at MatterDetailsDV.pcf: line 556, column 216
    function value_451 () : java.util.Date {
      return Matter.ResponseDue
    }
    
    // 'value' attribute on DateInput (id=ResponseFiled_Input) at MatterDetailsDV.pcf: line 563, column 161
    function value_457 () : java.util.Date {
      return Matter.ResponseFiled
    }
    
    // 'value' attribute on BooleanRadioInput (id=AdDamnumSpecified_Input) at MatterDetailsDV.pcf: line 571, column 320
    function value_464 () : java.lang.Boolean {
      return Matter.AdDamnumSpecified
    }
    
    // 'value' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_47 () : entity.Contact {
      return Matter.defendant
    }
    
    // 'value' attribute on CurrencyInput (id=AdDamnumAmount_Input) at MatterDetailsDV.pcf: line 581, column 118
    function value_471 () : gw.api.financials.CurrencyAmount {
      return Matter.AdDamnumAmount
    }
    
    // 'value' attribute on BooleanRadioInput (id=PunitiveDamages_Input) at MatterDetailsDV.pcf: line 589, column 318
    function value_478 () : java.lang.Boolean {
      return Matter.PunitiveDamages
    }
    
    // 'value' attribute on CurrencyInput (id=PunitiveAmount_Input) at MatterDetailsDV.pcf: line 599, column 116
    function value_485 () : gw.api.financials.CurrencyAmount {
      return Matter.PunitiveAmount
    }
    
    // 'value' attribute on TypeKeyInput (id=Matter_Resolution_Input) at MatterDetailsDV.pcf: line 610, column 45
    function value_491 () : typekey.ResolutionType {
      return Matter.Resolution
    }
    
    // 'value' attribute on TypeKeyInput (id=Matter_SettlementType_Input) at MatterDetailsDV.pcf: line 617, column 50
    function value_495 () : typekey.SettlementType_TDIC {
      return Matter.SettlementType_TDIC
    }
    
    // 'value' attribute on CurrencyInput (id=Matter_FinalLegalCost_Input) at MatterDetailsDV.pcf: line 624, column 40
    function value_499 () : gw.api.financials.CurrencyAmount {
      return Matter.FinalLegalCost
    }
    
    // 'value' attribute on CurrencyInput (id=Matter_FinalSettleCost_Input) at MatterDetailsDV.pcf: line 631, column 35
    function value_503 () : gw.api.financials.CurrencyAmount {
      return SettlementAmount
    }
    
    // 'value' attribute on DateInput (id=Matter_FinalSettleDate_Input) at MatterDetailsDV.pcf: line 637, column 41
    function value_506 () : java.util.Date {
      return Matter.FinalSettleDate
    }
    
    // 'value' attribute on BooleanRadioInput (id=TrialDetails_SubroRelated_Input) at MatterDetailsDV.pcf: line 66, column 38
    function value_56 () : java.lang.Boolean {
      return Matter.SubroRelated
    }
    
    // 'value' attribute on DateInput (id=Matter_CloseDate_Input) at MatterDetailsDV.pcf: line 70, column 35
    function value_60 () : java.util.Date {
      return Matter.CloseDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Matter_ReopenedReason_Input) at MatterDetailsDV.pcf: line 75, column 51
    function value_63 () : typekey.MatterReopenedReason {
      return Matter.ReopenedReason
    }
    
    // 'value' attribute on TypeKeyInput (id=CourtType_Input) at MatterDetailsDV.pcf: line 87, column 140
    function value_68 () : typekey.MatterCourtType {
      return Matter.CourtType
    }
    
    // 'value' attribute on TypeKeyInput (id=CourtDistrict_Input) at MatterDetailsDV.pcf: line 97, column 142
    function value_74 () : typekey.MatterCourtDistrict {
      return Matter.CourtDistrict
    }
    
    // 'value' attribute on TextInput (id=AssignedUser_Name_Input) at MatterDetailsDV.pcf: line 29, column 47
    function value_8 () : java.lang.String {
      return Matter.AssigneeDisplayString
    }
    
    // 'value' attribute on TypeKeyInput (id=LegalSpecialty_Input) at MatterDetailsDV.pcf: line 105, column 142
    function value_80 () : typekey.LegalSpecialty {
      return Matter.LegalSpecialty
    }
    
    // 'value' attribute on TypeKeyInput (id=TrialDetails_PrimaryCause2_Input) at MatterDetailsDV.pcf: line 113, column 142
    function value_86 () : typekey.PrimaryCauseType {
      return Matter.PrimaryCause
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_108 ($$arg :  entity.Attorney[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_108 ($$arg :  gw.api.database.IQueryBeanResult<entity.Attorney>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_108 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_131 ($$arg :  entity.LawFirm[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_131 ($$arg :  gw.api.database.IQueryBeanResult<entity.LawFirm>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_131 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_154 ($$arg :  entity.Attorney[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_154 ($$arg :  gw.api.database.IQueryBeanResult<entity.Attorney>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_154 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_177 ($$arg :  entity.LawFirm[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_177 ($$arg :  gw.api.database.IQueryBeanResult<entity.LawFirm>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_177 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_230 ($$arg :  entity.Adjudicator[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_230 ($$arg :  gw.api.database.IQueryBeanResult<entity.Adjudicator>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_230 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_304 ($$arg :  entity.LegalVenue[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_304 ($$arg :  gw.api.database.IQueryBeanResult<entity.LegalVenue>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_304 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_32 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_32 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_32 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_333 ($$arg :  entity.Adjudicator[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_333 ($$arg :  gw.api.database.IQueryBeanResult<entity.Adjudicator>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_333 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_365 ($$arg :  entity.LegalVenue[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_365 ($$arg :  gw.api.database.IQueryBeanResult<entity.LegalVenue>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_365 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_394 ($$arg :  entity.Adjudicator[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_394 ($$arg :  gw.api.database.IQueryBeanResult<entity.Adjudicator>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_394 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_431 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_431 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_431 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_51 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_51 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_51 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_109 () : void {
      var __valueRangeArg = Matter.Claim.RelatedAttorneyArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_108(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_132 () : void {
      var __valueRangeArg = Matter.Claim.RelatedLawFirmArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_131(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_155 () : void {
      var __valueRangeArg = Matter.Claim.RelatedAttorneyArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_154(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_178 () : void {
      var __valueRangeArg = Matter.Claim.RelatedLawFirmArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_177(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_231 () : void {
      var __valueRangeArg = Matter.Claim.RelatedAdjudicatorArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_230(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_305 () : void {
      var __valueRangeArg = Matter.Claim.RelatedLegalVenueArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_304(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_33 () : void {
      var __valueRangeArg = Matter.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_32(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_334 () : void {
      var __valueRangeArg = Matter.Claim.RelatedAdjudicatorArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_333(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_366 () : void {
      var __valueRangeArg = Matter.Claim.RelatedLegalVenueArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_365(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_395 () : void {
      var __valueRangeArg = Matter.Claim.RelatedAdjudicatorArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_394(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_432 () : void {
      var __valueRangeArg = Matter.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_431(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_52 () : void {
      var __valueRangeArg = Matter.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_51(__valueRangeArg)
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at MatterDetailsDV.pcf: line 129, column 216
    function verifyValueType_115 () : void {
      var __valueTypeArg : entity.Attorney
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at MatterDetailsDV.pcf: line 139, column 216
    function verifyValueType_138 () : void {
      var __valueTypeArg : entity.LawFirm
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at MatterDetailsDV.pcf: line 150, column 216
    function verifyValueType_161 () : void {
      var __valueTypeArg : entity.Attorney
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at MatterDetailsDV.pcf: line 160, column 216
    function verifyValueType_184 () : void {
      var __valueTypeArg : entity.LawFirm
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at MatterDetailsDV.pcf: line 228, column 142
    function verifyValueType_237 () : void {
      var __valueTypeArg : entity.Adjudicator
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at MatterDetailsDV.pcf: line 378, column 142
    function verifyValueType_311 () : void {
      var __valueTypeArg : entity.LegalVenue
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at MatterDetailsDV.pcf: line 396, column 142
    function verifyValueType_340 () : void {
      var __valueTypeArg : entity.Adjudicator
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at MatterDetailsDV.pcf: line 421, column 54
    function verifyValueType_372 () : void {
      var __valueTypeArg : entity.LegalVenue
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at MatterDetailsDV.pcf: line 438, column 54
    function verifyValueType_401 () : void {
      var __valueTypeArg : entity.Adjudicator
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'visible' attribute on ClaimContactInput (id=Counsel_PlaintiffLawFirm_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_119 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.plaintifffirm), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=Counsel_DefenseAttorney_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_142 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.defenseattorney), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=Counsel_DefenseLawFirm_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_165 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.defensefirm), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_18 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=Plaintiff_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_21 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.plaintiff), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=TrialDetails_Judge_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_218 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.judge), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on Label at MatterDetailsDV.pcf: line 235, column 153
    function visible_239 () : java.lang.Boolean {
      return Matter.MatterType == TC_GENERAL || Matter.MatterType == MatterType.TC_SMALLCLAIMS_TDIC || Matter.MatterType == MatterType.TC_LAWSUIT
    }
    
    // 'visible' attribute on InputDivider at MatterDetailsDV.pcf: line 293, column 107
    function visible_259 () : java.lang.Boolean {
      return Matter.MatterType == TC_GENERAL || Matter.MatterType == MatterType.TC_SMALLCLAIMS_TDIC
    }
    
    // 'visible' attribute on Label at MatterDetailsDV.pcf: line 298, column 56
    function visible_260 () : java.lang.Boolean {
      return Matter.MatterType == TC_ARBITRATION
    }
    
    // 'visible' attribute on Label at MatterDetailsDV.pcf: line 359, column 142
    function visible_281 () : java.lang.Boolean {
      return Matter.MatterType == TC_HEARING || Matter.MatterType == TC_GENERAL || Matter.MatterType == MatterType.TC_SMALLCLAIMS_TDIC
    }
    
    // 'visible' attribute on ClaimContactInput (id=HearingDetails_Venue_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_292 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.hearingvenue), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=HearingDetails_Arbitrator_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_321 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.hearingjudge), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on Label at MatterDetailsDV.pcf: line 402, column 54
    function visible_342 () : java.lang.Boolean {
      return Matter.MatterType == TC_MEDIATION
    }
    
    // 'visible' attribute on ClaimContactInput (id=MediationDetails_Venue_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_353 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.mediationvenue), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=MediationDetails_Mediator_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_382 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.mediator), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=Defendant_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_40 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.defendant), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=TrialDetails_FiledBy_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_419 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.filedby), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on DateInput (id=ResponseFiled_Input) at MatterDetailsDV.pcf: line 563, column 161
    function visible_456 () : java.lang.Boolean {
      return Matter.MatterType == TC_LAWSUIT || Matter.MatterType == TC_ARBITRATION || Matter.MatterType == TC_HEARING || Matter.MatterType == TC_GENERAL
    }
    
    // 'visible' attribute on BooleanRadioInput (id=AdDamnumSpecified_Input) at MatterDetailsDV.pcf: line 571, column 320
    function visible_463 () : java.lang.Boolean {
      return (Matter.AdDamnumSpecified || !(Matter.AdDamnumAmount == null || Matter.AdDamnumAmount.Amount == 0)) || (Matter.MatterType == TC_LAWSUIT || Matter.MatterType == TC_ARBITRATION || Matter.MatterType == TC_HEARING || Matter.MatterType == TC_GENERAL || Matter.MatterType == MatterType.TC_SMALLCLAIMS_TDIC)
    }
    
    // 'visible' attribute on CurrencyInput (id=AdDamnumAmount_Input) at MatterDetailsDV.pcf: line 581, column 118
    function visible_470 () : java.lang.Boolean {
      return Matter.AdDamnumSpecified || !(Matter.AdDamnumAmount == null || Matter.AdDamnumAmount.Amount == 0)
    }
    
    // 'visible' attribute on BooleanRadioInput (id=PunitiveDamages_Input) at MatterDetailsDV.pcf: line 589, column 318
    function visible_477 () : java.lang.Boolean {
      return (Matter.PunitiveDamages || !(Matter.PunitiveAmount == null || Matter.PunitiveAmount.Amount == 0)) || (Matter.MatterType == TC_LAWSUIT || Matter.MatterType == TC_ARBITRATION || Matter.MatterType == TC_HEARING || Matter.MatterType == TC_GENERAL || Matter.MatterType == MatterType.TC_SMALLCLAIMS_TDIC)
    }
    
    // 'visible' attribute on CurrencyInput (id=PunitiveAmount_Input) at MatterDetailsDV.pcf: line 599, column 116
    function visible_484 () : java.lang.Boolean {
      return Matter.PunitiveDamages || !(Matter.PunitiveAmount == null || Matter.PunitiveAmount.Amount == 0)
    }
    
    // 'visible' attribute on Label at MatterDetailsDV.pcf: line 79, column 141
    function visible_66 () : java.lang.Boolean {
      return Matter.MatterType == TC_LAWSUIT|| Matter.MatterType == TC_GENERAL || Matter.MatterType == MatterType.TC_SMALLCLAIMS_TDIC
    }
    
    // 'visible' attribute on TypeKeyInput (id=CourtDistrict_Input) at MatterDetailsDV.pcf: line 97, column 142
    function visible_73 () : java.lang.Boolean {
      return Matter.MatterType == TC_LAWSUIT || Matter.MatterType == TC_GENERAL || Matter.MatterType == MatterType.TC_SMALLCLAIMS_TDIC
    }
    
    // 'visible' attribute on Label at MatterDetailsDV.pcf: line 118, column 216
    function visible_92 () : java.lang.Boolean {
      return Matter.MatterType == TC_LAWSUIT || Matter.MatterType == TC_ARBITRATION || Matter.MatterType == TC_HEARING || Matter.MatterType == TC_GENERAL || Matter.MatterType == MatterType.TC_SMALLCLAIMS_TDIC
    }
    
    // 'visible' attribute on ClaimContactInput (id=Counsel_PlaintiffAttorney_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_96 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Matter.plaintiffatt), Matter.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    property get Matter () : Matter {
      return getRequireValue("Matter", 0) as Matter
    }
    
    property set Matter ($arg :  Matter) {
      setRequireValue("Matter", 0, $arg)
    }
    
    
    
        property get TrialDate() : java.util.Date {
          return Matter.TrialDateActivity.TargetDate
        }
    
        property set TrialDate(date : java.util.Date) {
          Matter.updateTrialDate(date)
        }
    
        property get ArbitrationDate() : java.util.Date {
          return Matter.ArbitrationDateActivity.TargetDate
        }
    
        property set ArbitrationDate(date : java.util.Date) {
          Matter.updateArbitrationDate(date)
        }
    
    
        property get HearingDate() : java.util.Date {
          return Matter.HearingDateActivity.TargetDate
        }
    
        property set HearingDate(date : java.util.Date) {
          Matter.updateHearingDate(date)
        }
    
        property get MediationDate() : java.util.Date {
          return Matter.MediationDateActivity.TargetDate
        }
    
        property set MediationDate(date : java.util.Date) {
          Matter.updateMediationDate(date)
        }
    
        property get SettlementAmount() : CurrencyAmount {
    
          var isfinalAvailable = Matter.Claim.Transactions.hasMatch(\elt ->
            elt typeis Payment and (elt as Payment).CostType == CostType.TC_CLAIMCOST and (elt as Payment).PaymentType == PaymentType.TC_FINAL
          )
    
          if (Matter.FinalSettleCost == null and isfinalAvailable) {
            Matter.Claim.Checks.each(\check ->
              {
                check.Payments.each(\payment ->
                  {
                    if (payment.CostType == CostType.TC_CLAIMCOST) {
                      Matter.FinalSettleCost = Matter.FinalSettleCost == null ? payment.ClaimAmount : Matter.FinalSettleCost + payment.ClaimAmount
                    }
                  })
              })
    
          }
          return Matter.FinalSettleCost
        }
    
        property set SettlementAmount(settlementAmount : CurrencyAmount) {
          Matter.FinalSettleCost = settlementAmount
        }
        
    
    
  }
  
  
}