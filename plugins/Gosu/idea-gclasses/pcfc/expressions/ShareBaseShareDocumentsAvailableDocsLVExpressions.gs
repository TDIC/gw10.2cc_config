package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsAvailableDocsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseShareDocumentsAvailableDocsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsAvailableDocsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ShareBaseShareDocumentsAvailableDocsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 59, column 147
    function action_12 () : void {
      document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 68, column 145
    function action_17 () : void {
      document.downloadContent()
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 59, column 147
    function available_10 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document) 
    }
    
    // 'available' attribute on Link (id=NameLinkWeb) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 68, column 145
    function available_15 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document)
    }
    
    // 'condition' attribute on ToolbarFlag at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 33, column 34
    function condition_7 () : java.lang.Boolean {
      return perm.Document.edit(document)
    }
    
    // 'condition' attribute on ToolbarFlag at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 36, column 24
    function condition_8 () : java.lang.Boolean {
      return document.Obsolete
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 43, column 32
    function icon_9 () : java.lang.String {
      return document.Icon
    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 59, column 147
    function label_13 () : java.lang.Object {
      return document.Name
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 59, column 147
    function tooltip_14 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(document)
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 75, column 45
    function valueRoot_21 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 75, column 45
    function value_20 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 81, column 58
    function value_23 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 86, column 51
    function value_26 () : typekey.DocumentStatusType {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 91, column 36
    function value_29 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 100, column 24
    function value_32 () : java.util.Date {
      return document.DateModified
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 59, column 147
    function visible_11 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 68, column 145
    function visible_16 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    property get document () : entity.Document {
      return getIteratedValue(1) as entity.Document
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsAvailableDocsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseShareDocumentsAvailableDocsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 21, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'sortBy' attribute on LinkCell (id=Name) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 50, column 23
    function sortValue_1 (document :  entity.Document) : java.lang.Object {
      return document.Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 75, column 45
    function sortValue_2 (document :  entity.Document) : java.lang.Object {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 81, column 58
    function sortValue_3 (document :  entity.Document) : java.lang.Object {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 86, column 51
    function sortValue_4 (document :  entity.Document) : java.lang.Object {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 91, column 36
    function sortValue_5 (document :  entity.Document) : java.lang.Object {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 100, column 24
    function sortValue_6 (document :  entity.Document) : java.lang.Object {
      return document.DateModified
    }
    
    // 'value' attribute on RowIterator at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 28, column 40
    function value_35 () : List<Document> {
      return share.DocumentsNotLinked
    }
    
    // 'valueType' attribute on RowIterator at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 28, column 40
    function verifyValueTypeIsAllowedType_36 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 28, column 40
    function verifyValueTypeIsAllowedType_36 ($$arg :  gw.api.iterator.IteratorBackingData) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 28, column 40
    function verifyValueTypeIsAllowedType_36 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at ShareBaseShareDocumentsAvailableDocsLV.pcf: line 28, column 40
    function verifyValueType_37 () : void {
      var __valueTypeArg : List<Document>
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the valueType is not a valid type for use with an iterator
      // The valueType for an iterator must be an array or extend from List or IQueryBeanResult
      verifyValueTypeIsAllowedType_36(__valueTypeArg)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get share () : OutboundSharing_Ext {
      return getRequireValue("share", 0) as OutboundSharing_Ext
    }
    
    property set share ($arg :  OutboundSharing_Ext) {
      setRequireValue("share", 0, $arg)
    }
    
    
  }
  
  
}