package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/newtransaction/recovery/EditableRecoveryLineItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditableRecoveryLineItemsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/newtransaction/recovery/EditableRecoveryLineItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditableRecoveryLineItemsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeCell (id=LineCategory_Cell) at EditableRecoveryLineItemsLV.pcf: line 28, column 118
    function sortValue_0 (TransactionLineItem :  entity.TransactionLineItem) : java.lang.Object {
      return TransactionLineItem.LineCategory
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at EditableRecoveryLineItemsLV.pcf: line 33, column 49
    function sortValue_1 (TransactionLineItem :  entity.TransactionLineItem) : java.lang.Object {
      return TransactionLineItem.Comments
    }
    
    // 'value' attribute on CurrencyCell (id=Amount_Cell) at EditableRecoveryLineItemsLV.pcf: line 42, column 55
    function sortValue_2 (TransactionLineItem :  entity.TransactionLineItem) : java.lang.Object {
      return TransactionLineItem.TransactionAmountReservingAmountPair
    }
    
    // '$$sumValue' attribute on RowIterator at EditableRecoveryLineItemsLV.pcf: line 42, column 55
    function sumValueRoot_4 (TransactionLineItem :  entity.TransactionLineItem) : java.lang.Object {
      return TransactionLineItem
    }
    
    // 'footerSumValue' attribute on RowIterator at EditableRecoveryLineItemsLV.pcf: line 42, column 55
    function sumValue_3 (TransactionLineItem :  entity.TransactionLineItem) : java.lang.Object {
      return TransactionLineItem.TransactionAmountReservingAmountPair
    }
    
    // 'toAdd' attribute on RowIterator at EditableRecoveryLineItemsLV.pcf: line 20, column 48
    function toAdd_19 (TransactionLineItem :  entity.TransactionLineItem) : void {
      Transaction.addToLineItems(TransactionLineItem)
    }
    
    // 'toRemove' attribute on RowIterator at EditableRecoveryLineItemsLV.pcf: line 20, column 48
    function toRemove_20 (TransactionLineItem :  entity.TransactionLineItem) : void {
      Transaction.removeFromLineItemsIfEditable(TransactionLineItem)
    }
    
    // 'value' attribute on RowIterator at EditableRecoveryLineItemsLV.pcf: line 20, column 48
    function value_21 () : entity.TransactionLineItem[] {
      return Transaction.LineItems
    }
    
    property get Transaction () : Transaction {
      return getRequireValue("Transaction", 0) as Transaction
    }
    
    property set Transaction ($arg :  Transaction) {
      setRequireValue("Transaction", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/newtransaction/recovery/EditableRecoveryLineItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends EditableRecoveryLineItemsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at EditableRecoveryLineItemsLV.pcf: line 33, column 49
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      TransactionLineItem.Comments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeCell (id=LineCategory_Cell) at EditableRecoveryLineItemsLV.pcf: line 28, column 118
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      TransactionLineItem.LineCategory = (__VALUE_TO_SET as typekey.LineCategory)
    }
    
    // 'valueRange' attribute on RangeCell (id=LineCategory_Cell) at EditableRecoveryLineItemsLV.pcf: line 28, column 118
    function valueRange_8 () : java.lang.Object {
      return TransactionLineItem.ValidLineCategoriesforRecovery
    }
    
    // 'value' attribute on RangeCell (id=LineCategory_Cell) at EditableRecoveryLineItemsLV.pcf: line 28, column 118
    function valueRoot_7 () : java.lang.Object {
      return TransactionLineItem
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at EditableRecoveryLineItemsLV.pcf: line 33, column 49
    function value_12 () : java.lang.String {
      return TransactionLineItem.Comments
    }
    
    // 'value' attribute on CurrencyCell (id=Amount_Cell) at EditableRecoveryLineItemsLV.pcf: line 42, column 55
    function value_16 () : gw.api.financials.IPairedMoney {
      return TransactionLineItem.TransactionAmountReservingAmountPair
    }
    
    // 'value' attribute on RangeCell (id=LineCategory_Cell) at EditableRecoveryLineItemsLV.pcf: line 28, column 118
    function value_5 () : typekey.LineCategory {
      return TransactionLineItem.LineCategory
    }
    
    // 'valueRange' attribute on RangeCell (id=LineCategory_Cell) at EditableRecoveryLineItemsLV.pcf: line 28, column 118
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=LineCategory_Cell) at EditableRecoveryLineItemsLV.pcf: line 28, column 118
    function verifyValueRangeIsAllowedType_9 ($$arg :  typekey.LineCategory[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=LineCategory_Cell) at EditableRecoveryLineItemsLV.pcf: line 28, column 118
    function verifyValueRange_10 () : void {
      var __valueRangeArg = TransactionLineItem.ValidLineCategoriesforRecovery
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_9(__valueRangeArg)
    }
    
    property get TransactionLineItem () : entity.TransactionLineItem {
      return getIteratedValue(1) as entity.TransactionLineItem
    }
    
    
  }
  
  
}