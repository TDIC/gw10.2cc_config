package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/icdtransitionto10/exitpoints/ICDData.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ICDDataExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/icdtransitionto10/exitpoints/ICDData.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ICDDataExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (refUrl :  String) : int {
      return 0
    }
    
    override property get CurrentLocation () : pcf.ICDData {
      return super.CurrentLocation as pcf.ICDData
    }
    
    property get refUrl () : String {
      return getVariableValue("refUrl", 0) as String
    }
    
    property set refUrl ($arg :  String) {
      setVariableValue("refUrl", 0, $arg)
    }
    
    
  }
  
  
}