package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsSelectDocsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseShareDocumentsSelectDocsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsSelectDocsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseShareDocumentsSelectDocsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=UnlinkDocuments) at ShareBaseShareDocumentsSelectDocsScreen.pcf: line 34, column 99
    function checkedRowAction_0 (element :  entity.OutbdShareDocLink_Ext, CheckedValue :  entity.OutbdShareDocLink_Ext) : void {
      share.removeDocumentLink(CheckedValue)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=selectDocumentsButton) at ShareBaseShareDocumentsSelectDocsScreen.pcf: line 48, column 88
    function checkedRowAction_3 (element :  entity.Document, CheckedValue :  entity.Document) : void {
      share.addDocument(CheckedValue as Document)
    }
    
    // 'def' attribute on PanelRef (id=selectedDocs) at ShareBaseShareDocumentsSelectDocsScreen.pcf: line 26, column 25
    function def_onEnter_1 (def :  pcf.ShareBaseShareDocumentsSelectedDocsLV) : void {
      def.onEnter(claim, share.DocumentLinks, true)
    }
    
    // 'def' attribute on PanelRef (id=availableDocuments) at ShareBaseShareDocumentsSelectDocsScreen.pcf: line 40, column 31
    function def_onEnter_4 (def :  pcf.ShareBaseShareDocumentsAvailableDocsLV) : void {
      def.onEnter(claim, share)
    }
    
    // 'def' attribute on PanelRef (id=selectedDocs) at ShareBaseShareDocumentsSelectDocsScreen.pcf: line 26, column 25
    function def_refreshVariables_2 (def :  pcf.ShareBaseShareDocumentsSelectedDocsLV) : void {
      def.refreshVariables(claim, share.DocumentLinks, true)
    }
    
    // 'def' attribute on PanelRef (id=availableDocuments) at ShareBaseShareDocumentsSelectDocsScreen.pcf: line 40, column 31
    function def_refreshVariables_5 (def :  pcf.ShareBaseShareDocumentsAvailableDocsLV) : void {
      def.refreshVariables(claim, share)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get share () : OutboundSharing_Ext {
      return getRequireValue("share", 0) as OutboundSharing_Ext
    }
    
    property set share ($arg :  OutboundSharing_Ext) {
      setRequireValue("share", 0, $arg)
    }
    
    
  }
  
  
}