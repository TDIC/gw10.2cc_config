package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/contacts/ClaimNewWitnessContactPickerMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClaimNewWitnessContactPickerMenuItemSetExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/contacts/ClaimNewWitnessContactPickerMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimNewWitnessContactPickerMenuItemSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=NewContactPickerMenuItemSet_NewPerson) at ClaimNewWitnessContactPickerMenuItemSet.pcf: line 21, column 96
    function action_1 () : void {
      NewContactPopup.push(typekey.Contact.TC_PERSON, parentContact, claim)
    }
    
    // 'action' attribute on MenuItem (id=NewAdjudicator) at ClaimNewWitnessContactPickerMenuItemSet.pcf: line 59, column 88
    function action_3 () : void {
      NewPartyInvolvedPopup.push(claim, typekey.Contact.TC_ATTORNEY, DisplayKey.get("Web.NewContactMenu.PlaintiffAttorney_TDIC"))
    }
    
    // 'action' attribute on MenuItem (id=NewAttorney) at ClaimNewWitnessContactPickerMenuItemSet.pcf: line 63, column 86
    function action_5 () : void {
      NewPartyInvolvedPopup.push(claim, typekey.Contact.TC_ATTORNEY, DisplayKey.get("Web.NewContactMenu.DefenseAttorney_TDIC"))
    }
    
    // 'action' attribute on MenuItem (id=NewLawFirm) at ClaimNewWitnessContactPickerMenuItemSet.pcf: line 67, column 73
    function action_7 () : void {
      NewPartyInvolvedPopup.push(claim, typekey.Contact.TC_LAWFIRM, null)
    }
    
    // 'action' attribute on MenuItem (id=NewContactPickerMenuItemSet_NewPerson) at ClaimNewWitnessContactPickerMenuItemSet.pcf: line 21, column 96
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_PERSON, parentContact, claim)
    }
    
    // 'action' attribute on MenuItem (id=NewAdjudicator) at ClaimNewWitnessContactPickerMenuItemSet.pcf: line 59, column 88
    function action_dest_4 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(claim, typekey.Contact.TC_ATTORNEY, DisplayKey.get("Web.NewContactMenu.PlaintiffAttorney_TDIC"))
    }
    
    // 'action' attribute on MenuItem (id=NewAttorney) at ClaimNewWitnessContactPickerMenuItemSet.pcf: line 63, column 86
    function action_dest_6 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(claim, typekey.Contact.TC_ATTORNEY, DisplayKey.get("Web.NewContactMenu.DefenseAttorney_TDIC"))
    }
    
    // 'action' attribute on MenuItem (id=NewLawFirm) at ClaimNewWitnessContactPickerMenuItemSet.pcf: line 67, column 73
    function action_dest_8 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(claim, typekey.Contact.TC_LAWFIRM, null)
    }
    
    // 'visible' attribute on MenuItem (id=NewContactPickerMenuItemSet_NewPerson) at ClaimNewWitnessContactPickerMenuItemSet.pcf: line 21, column 96
    function visible_0 () : java.lang.Boolean {
      return requiredContactType == entity.Contact or requiredContactType == entity.Person
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get parentContact () : Contact {
      return getRequireValue("parentContact", 0) as Contact
    }
    
    property set parentContact ($arg :  Contact) {
      setRequireValue("parentContact", 0, $arg)
    }
    
    property get requiredContactType () : Type {
      return getRequireValue("requiredContactType", 0) as Type
    }
    
    property set requiredContactType ($arg :  Type) {
      setRequireValue("requiredContactType", 0, $arg)
    }
    
    property get ShowNewVendor() : boolean {
      return {entity.Contact,
              entity.Company,
              entity.Person,
              entity.PersonVendor,
              entity.CompanyVendor,
              entity.AutoRepairShop,
              entity.AutoTowingAgcy,
              entity.Doctor,
              entity.MedicalCareOrg}.contains(requiredContactType)
    }
    
    property get ShowNewLegal() : boolean {
      return {entity.Contact,
              entity.Attorney,
              entity.LawFirm,
              entity.Company,
              entity.LegalVenue,
              entity.Person,
              entity.Adjudicator}.contains(requiredContactType)
    }
    
    
  }
  
  
}