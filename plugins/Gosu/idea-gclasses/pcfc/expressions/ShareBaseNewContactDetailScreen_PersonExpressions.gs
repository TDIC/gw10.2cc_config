package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseNewContactDetailScreen.Person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseNewContactDetailScreen_PersonExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseNewContactDetailScreen.Person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseNewContactDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=FirstName_Input) at ShareBaseNewContactDetailScreen.Person.pcf: line 22, column 50
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      (Contact as Person).FirstName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=LastName_Input) at ShareBaseNewContactDetailScreen.Person.pcf: line 28, column 49
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      (Contact as Person).LastName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=EmailAddress_Input) at ShareBaseNewContactDetailScreen.Person.pcf: line 34, column 54
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      (Contact as Person).EmailAddress1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=FirstName_Input) at ShareBaseNewContactDetailScreen.Person.pcf: line 22, column 50
    function valueRoot_2 () : java.lang.Object {
      return (Contact as Person)
    }
    
    // 'value' attribute on TextInput (id=FirstName_Input) at ShareBaseNewContactDetailScreen.Person.pcf: line 22, column 50
    function value_0 () : java.lang.String {
      return (Contact as Person).FirstName
    }
    
    // 'value' attribute on TextInput (id=LastName_Input) at ShareBaseNewContactDetailScreen.Person.pcf: line 28, column 49
    function value_4 () : java.lang.String {
      return (Contact as Person).LastName
    }
    
    // 'value' attribute on TextInput (id=EmailAddress_Input) at ShareBaseNewContactDetailScreen.Person.pcf: line 34, column 54
    function value_8 () : java.lang.String {
      return (Contact as Person).EmailAddress1
    }
    
    property get Contact () : Contact {
      return getRequireValue("Contact", 0) as Contact
    }
    
    property set Contact ($arg :  Contact) {
      setRequireValue("Contact", 0, $arg)
    }
    
    
  }
  
  
}