package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/icd/ICD.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ICDExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/icd/ICD.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ICDExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=Search_Button) at ICD.pcf: line 63, column 69
    function action_3 () : void {
      codeSearchResults = libraries.ICDCodeUtil.queryICD(findCode, findBodySystem,findDesc,find9, find10, true ) 
    }
    
    // 'action' attribute on ToolbarButton (id=Add_ICD_Code_Button) at ICD.pcf: line 69, column 27
    function action_4 () : void {
      NewICDPopup.push()
    }
    
    // 'action' attribute on ToolbarButton (id=Add_ICD_Code_Button) at ICD.pcf: line 69, column 27
    function action_dest_5 () : pcf.api.Destination {
      return pcf.NewICDPopup.createDestination()
    }
    
    // 'canEdit' attribute on Page (id=ICD) at ICD.pcf: line 10, column 59
    function canEdit_26 () : java.lang.Boolean {
      return perm.System.editrefdata
    }
    
    // 'canVisit' attribute on Page (id=ICD) at ICD.pcf: line 10, column 59
    static function canVisit_27 () : java.lang.Boolean {
      return perm.System.viewrefdata
    }
    
    // 'def' attribute on PanelRef (id=ICDPanel) at ICD.pcf: line 122, column 23
    function def_onEnter_24 (def :  pcf.ICDLV) : void {
      def.onEnter(codeSearchResults)
    }
    
    // 'def' attribute on PanelRef (id=ICDPanel) at ICD.pcf: line 122, column 23
    function def_refreshVariables_25 (def :  pcf.ICDLV) : void {
      def.refreshVariables(codeSearchResults)
    }
    
    // 'value' attribute on TextInput (id=description_Input) at ICD.pcf: line 89, column 33
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      findDesc = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=findBodySystem_Input) at ICD.pcf: line 98, column 50
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      findBodySystem = (__VALUE_TO_SET as typekey.ICDBodySystem)
    }
    
    // 'value' attribute on BooleanRadioInput (id=find10Ele_Input) at ICD.pcf: line 109, column 31
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      find10 = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=find9Ele_Input) at ICD.pcf: line 115, column 30
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      find9 = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=idcCode_Input) at ICD.pcf: line 82, column 33
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      findCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at ICD.pcf: line 20, column 22
    function initialValue_0 () : String {
      return null
    }
    
    // 'initialValue' attribute on Variable at ICD.pcf: line 24, column 29
    function initialValue_1 () : ICDBodySystem {
      return null
    }
    
    // Page (id=ICD) at ICD.pcf: line 10, column 59
    static function parent_28 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'valueRange' attribute on RangeInput (id=findBodySystem_Input) at ICD.pcf: line 98, column 50
    function valueRange_14 () : java.lang.Object {
      return ICDBodySystem.getTypeKeys( false )
    }
    
    // 'value' attribute on RangeInput (id=findBodySystem_Input) at ICD.pcf: line 98, column 50
    function value_12 () : typekey.ICDBodySystem {
      return findBodySystem
    }
    
    // 'value' attribute on BooleanRadioInput (id=find10Ele_Input) at ICD.pcf: line 109, column 31
    function value_18 () : java.lang.Boolean {
      return find10
    }
    
    // 'value' attribute on BooleanRadioInput (id=find9Ele_Input) at ICD.pcf: line 115, column 30
    function value_21 () : java.lang.Boolean {
      return find9
    }
    
    // 'value' attribute on TextInput (id=idcCode_Input) at ICD.pcf: line 82, column 33
    function value_6 () : java.lang.String {
      return findCode
    }
    
    // 'value' attribute on TextInput (id=description_Input) at ICD.pcf: line 89, column 33
    function value_9 () : java.lang.String {
      return findDesc
    }
    
    // 'valueRange' attribute on RangeInput (id=findBodySystem_Input) at ICD.pcf: line 98, column 50
    function verifyValueRangeIsAllowedType_15 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=findBodySystem_Input) at ICD.pcf: line 98, column 50
    function verifyValueRangeIsAllowedType_15 ($$arg :  typekey.ICDBodySystem[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=findBodySystem_Input) at ICD.pcf: line 98, column 50
    function verifyValueRange_16 () : void {
      var __valueRangeArg = ICDBodySystem.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_15(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.ICD {
      return super.CurrentLocation as pcf.ICD
    }
    
    property get ICDs () : gw.api.database.IQueryBeanResult<ICDCode> {
      return getVariableValue("ICDs", 0) as gw.api.database.IQueryBeanResult<ICDCode>
    }
    
    property set ICDs ($arg :  gw.api.database.IQueryBeanResult<ICDCode>) {
      setVariableValue("ICDs", 0, $arg)
    }
    
    property get codeSearchResults () : gw.api.database.IQueryBeanResult<ICDCode> {
      return getVariableValue("codeSearchResults", 0) as gw.api.database.IQueryBeanResult<ICDCode>
    }
    
    property set codeSearchResults ($arg :  gw.api.database.IQueryBeanResult<ICDCode>) {
      setVariableValue("codeSearchResults", 0, $arg)
    }
    
    property get find10 () : Boolean {
      return getVariableValue("find10", 0) as Boolean
    }
    
    property set find10 ($arg :  Boolean) {
      setVariableValue("find10", 0, $arg)
    }
    
    property get find9 () : Boolean {
      return getVariableValue("find9", 0) as Boolean
    }
    
    property set find9 ($arg :  Boolean) {
      setVariableValue("find9", 0, $arg)
    }
    
    property get findBodySystem () : ICDBodySystem {
      return getVariableValue("findBodySystem", 0) as ICDBodySystem
    }
    
    property set findBodySystem ($arg :  ICDBodySystem) {
      setVariableValue("findBodySystem", 0, $arg)
    }
    
    property get findCode () : String {
      return getVariableValue("findCode", 0) as String
    }
    
    property set findCode ($arg :  String) {
      setVariableValue("findCode", 0, $arg)
    }
    
    property get findDesc () : String {
      return getVariableValue("findDesc", 0) as String
    }
    
    property set findDesc ($arg :  String) {
      setVariableValue("findDesc", 0, $arg)
    }
    
    
  }
  
  
}