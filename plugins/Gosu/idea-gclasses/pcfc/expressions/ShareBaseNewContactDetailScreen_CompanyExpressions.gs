package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseNewContactDetailScreen.Company.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseNewContactDetailScreen_CompanyExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseNewContactDetailScreen.Company.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseNewContactDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at ShareBaseNewContactDetailScreen.Company.pcf: line 22, column 46
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      (Contact as Company).Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=EmailAddress_Input) at ShareBaseNewContactDetailScreen.Company.pcf: line 28, column 55
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      (Contact as Company).EmailAddress1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at ShareBaseNewContactDetailScreen.Company.pcf: line 22, column 46
    function valueRoot_2 () : java.lang.Object {
      return (Contact as Company)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at ShareBaseNewContactDetailScreen.Company.pcf: line 22, column 46
    function value_0 () : java.lang.String {
      return (Contact as Company).Name
    }
    
    // 'value' attribute on TextInput (id=EmailAddress_Input) at ShareBaseNewContactDetailScreen.Company.pcf: line 28, column 55
    function value_4 () : java.lang.String {
      return (Contact as Company).EmailAddress1
    }
    
    property get Contact () : Contact {
      return getRequireValue("Contact", 0) as Contact
    }
    
    property set Contact ($arg :  Contact) {
      setRequireValue("Contact", 0, $arg)
    }
    
    
  }
  
  
}