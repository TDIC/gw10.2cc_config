package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/activity/ActivityDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityDocumentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/activity/ActivityDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityDocumentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at ActivityDocumentsLV.pcf: line 13, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ActivityDocumentsLV.pcf: line 96, column 29
    function sortValue_1 (document :  entity.Document) : java.lang.Object {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at ActivityDocumentsLV.pcf: line 103, column 29
    function sortValue_2 (document :  entity.Document) : java.lang.Object {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ActivityDocumentsLV.pcf: line 108, column 51
    function sortValue_3 (document :  entity.Document) : java.lang.Object {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at ActivityDocumentsLV.pcf: line 113, column 36
    function sortValue_4 (document :  entity.Document) : java.lang.Object {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at ActivityDocumentsLV.pcf: line 122, column 29
    function sortValue_5 (document :  entity.Document) : java.lang.Object {
      return document.DateModified
    }
    
    // 'value' attribute on RowIterator at ActivityDocumentsLV.pcf: line 19, column 37
    function value_50 () : entity.Document[] {
      return Activity.LinkedDocuments
    }
    
    property get Activity () : Activity {
      return getRequireValue("Activity", 0) as Activity
    }
    
    property set Activity ($arg :  Activity) {
      setRequireValue("Activity", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/activity/ActivityDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ActivityDocumentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at ActivityDocumentsLV.pcf: line 50, column 145
    function action_14 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at ActivityDocumentsLV.pcf: line 61, column 77
    function action_18 () : void {
      DocumentDetailsPopup.push(document, !CurrentLocation.InEditMode)
    }
    
    // 'action' attribute on Link (id=DownloadLink) at ActivityDocumentsLV.pcf: line 70, column 90
    function action_23 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=UploadLink) at ActivityDocumentsLV.pcf: line 79, column 88
    function action_28 () : void {
      UploadDocumentContentPopup.push(document)
    }
    
    // 'action' attribute on Link (id=DocumentsLV_RemoveLink) at ActivityDocumentsLV.pcf: line 88, column 82
    function action_32 () : void {
      documentsActionsHelper.removeLinkedDocument(document, Activity, CurrentLocation.InEditMode)
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at ActivityDocumentsLV.pcf: line 41, column 147
    function action_9 () : void {
      document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at ActivityDocumentsLV.pcf: line 61, column 77
    function action_dest_19 () : pcf.api.Destination {
      return pcf.DocumentDetailsPopup.createDestination(document, !CurrentLocation.InEditMode)
    }
    
    // 'action' attribute on Link (id=UploadLink) at ActivityDocumentsLV.pcf: line 79, column 88
    function action_dest_29 () : pcf.api.Destination {
      return pcf.UploadDocumentContentPopup.createDestination(document)
    }
    
    // 'available' attribute on Link (id=NameLinkWeb) at ActivityDocumentsLV.pcf: line 50, column 145
    function available_12 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=ViewPropertiesLink) at ActivityDocumentsLV.pcf: line 61, column 77
    function available_17 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'available' attribute on Link (id=DownloadLink) at ActivityDocumentsLV.pcf: line 70, column 90
    function available_21 () : java.lang.Boolean {
      return documentsActionsHelper.isDownloadDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=UploadLink) at ActivityDocumentsLV.pcf: line 79, column 88
    function available_26 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at ActivityDocumentsLV.pcf: line 41, column 147
    function available_7 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document) 
    }
    
    // 'confirmMessage' attribute on Link (id=DocumentsLV_RemoveLink) at ActivityDocumentsLV.pcf: line 88, column 82
    function confirmMessage_33 () : java.lang.String {
      return DisplayKey.get("Web.DocumentsLV.Button.Remove.Confirm", "activity")
    }
    
    // 'icon' attribute on Link (id=DownloadLink) at ActivityDocumentsLV.pcf: line 70, column 90
    function icon_25 () : java.lang.String {
      return "document_download" 
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at ActivityDocumentsLV.pcf: line 27, column 32
    function icon_6 () : java.lang.String {
      return document.Icon
    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at ActivityDocumentsLV.pcf: line 41, column 147
    function label_10 () : java.lang.Object {
      return document.Name
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at ActivityDocumentsLV.pcf: line 41, column 147
    function tooltip_11 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(document)
    }
    
    // 'tooltip' attribute on Link (id=ViewPropertiesLink) at ActivityDocumentsLV.pcf: line 61, column 77
    function tooltip_20 () : java.lang.String {
      return documentsActionsHelper.ViewDocumentPropertiesTooltip
    }
    
    // 'tooltip' attribute on Link (id=DownloadLink) at ActivityDocumentsLV.pcf: line 70, column 90
    function tooltip_24 () : java.lang.String {
      return documentsActionsHelper.DownloadDocumentContentTooltip
    }
    
    // 'tooltip' attribute on Link (id=UploadLink) at ActivityDocumentsLV.pcf: line 79, column 88
    function tooltip_30 () : java.lang.String {
      return documentsActionsHelper.UploadDocumentContentTooltip
    }
    
    // 'tooltip' attribute on Link (id=DocumentsLV_RemoveLink) at ActivityDocumentsLV.pcf: line 88, column 82
    function tooltip_34 () : java.lang.String {
      return documentsActionsHelper.RemoveDocumentReferenceLinkTooltip
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ActivityDocumentsLV.pcf: line 96, column 29
    function valueRoot_36 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ActivityDocumentsLV.pcf: line 96, column 29
    function value_35 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at ActivityDocumentsLV.pcf: line 103, column 29
    function value_38 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ActivityDocumentsLV.pcf: line 108, column 51
    function value_41 () : typekey.DocumentStatusType {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at ActivityDocumentsLV.pcf: line 113, column 36
    function value_44 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at ActivityDocumentsLV.pcf: line 122, column 29
    function value_47 () : java.util.Date {
      return document.DateModified
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at ActivityDocumentsLV.pcf: line 50, column 145
    function visible_13 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on Link (id=DownloadLink) at ActivityDocumentsLV.pcf: line 70, column 90
    function visible_22 () : java.lang.Boolean {
      return documentsActionsHelper.isDownloadDocumentContentVisible(document)
    }
    
    // 'visible' attribute on Link (id=UploadLink) at ActivityDocumentsLV.pcf: line 79, column 88
    function visible_27 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentVisible(document)
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at ActivityDocumentsLV.pcf: line 41, column 147
    function visible_8 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    property get document () : entity.Document {
      return getIteratedValue(1) as entity.Document
    }
    
    
  }
  
  
}