package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.database.IQueryBeanResult
@javax.annotation.Generated("config/web/pcf/claim/documents/ClaimDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClaimDocumentsExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/documents/ClaimDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimDocumentsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Claim :  Claim) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=webCustomQueryButton) at ClaimDocuments.pcf: line 31, column 49
    function action_2 () : void {
      OnBaseUrl.push(tdic.cc.integ.plugins.onbase.util.OnBaseWebUtils_TDIC.renderPopUrl((ScriptParameters.getParameterValue("acc.onbase.WebCustomQuery") as String)?.trim() , {Claim.ClaimNumber, ((acc.onbase.configuration.OnBaseConfigurationFactory.Instance.WebClientType) as String)?.trim()}))
    }
    
    // 'action' attribute on ToolbarButton (id=webCustomQueryButton) at ClaimDocuments.pcf: line 31, column 49
    function action_dest_3 () : pcf.api.Destination {
      return pcf.OnBaseUrl.createDestination(tdic.cc.integ.plugins.onbase.util.OnBaseWebUtils_TDIC.renderPopUrl((ScriptParameters.getParameterValue("acc.onbase.WebCustomQuery") as String)?.trim() , {Claim.ClaimNumber, ((acc.onbase.configuration.OnBaseConfigurationFactory.Instance.WebClientType) as String)?.trim()}))
    }
    
    // 'canVisit' attribute on Page (id=ClaimDocuments) at ClaimDocuments.pcf: line 11, column 79
    static function canVisit_59 (Claim :  Claim) : java.lang.Boolean {
      return perm.Claim.view(Claim) and perm.System.viewdocs and (Claim.State != ClaimState.TC_DRAFT)
    }
    
    // 'initialValue' attribute on Variable at ClaimDocuments.pcf: line 20, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'initialValue' attribute on Variable at ClaimDocuments.pcf: line 24, column 45
    function initialValue_1 () : acc.onbase.util.ThumbnailUtil {
      return new acc.onbase.util.ThumbnailUtil()
    }
    
    // 'onResume' attribute on Page (id=ClaimDocuments) at ClaimDocuments.pcf: line 11, column 79
    function onResume_60 () : void {
      onbaseThumbnailUtil.resetThumbnailVars()
    }
    
    // Page (id=ClaimDocuments) at ClaimDocuments.pcf: line 11, column 79
    static function parent_61 (Claim :  Claim) : pcf.api.Destination {
      return pcf.Claim.createDestination(Claim)
    }
    
    // 'visible' attribute on AlertBar (id=IDCSDisabledAlert) at ClaimDocuments.pcf: line 36, column 68
    function visible_4 () : java.lang.Boolean {
      return not documentsActionsHelper.ContentSourceEnabled
    }
    
    // 'visible' attribute on AlertBar (id=IDCSUnavailableAlert) at ClaimDocuments.pcf: line 40, column 72
    function visible_5 () : java.lang.Boolean {
      return documentsActionsHelper.ShowContentServerDownWarning
    }
    
    // 'visible' attribute on AlertBar (id=IDMSUnavailableAlert) at ClaimDocuments.pcf: line 44, column 73
    function visible_6 () : java.lang.Boolean {
      return documentsActionsHelper.ShowMetadataServerDownWarning
    }
    
    property get Claim () : Claim {
      return getVariableValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setVariableValue("Claim", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.ClaimDocuments {
      return super.CurrentLocation as pcf.ClaimDocuments
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get onbaseThumbnailUtil () : acc.onbase.util.ThumbnailUtil {
      return getVariableValue("onbaseThumbnailUtil", 0) as acc.onbase.util.ThumbnailUtil
    }
    
    property set onbaseThumbnailUtil ($arg :  acc.onbase.util.ThumbnailUtil) {
      setVariableValue("onbaseThumbnailUtil", 0, $arg)
    }
    
    
        function createSearchCriteria() : DocumentSearchCriteria {
          var searchCriteria = new DocumentSearchCriteria();
          searchCriteria.Claim = Claim;
          searchCriteria.IncludeObsoletes = false;
          return searchCriteria;
        }
    
    
        function searchAndResetPageNum(docSearch : DocumentSearchCriteria) : IQueryBeanResult<Document> {
          var result = docSearch.performSearch() as gw.api.database.IQueryBeanResult<Document>
          onbaseThumbnailUtil.ThumbnailPageNumber = 1
          return result
        }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/documents/ClaimDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends ClaimDocumentsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=RequestDocuments) at ClaimDocuments.pcf: line 78, column 58
    function action_13 () : void {
      ShareBaseFolderRequestWizard.push(Claim)
    }
    
    // 'action' attribute on ToolbarButton (id=ShareDocuments) at ClaimDocuments.pcf: line 83, column 59
    function action_16 () : void {
      ShareBaseShareDocumentsWizard.push(Claim)
    }
    
    // 'action' attribute on ToolbarButton (id=RefreshAsyncContent) at ClaimDocuments.pcf: line 90, column 110
    function action_20 () : void {
      
    }
    
    // 'action' attribute on ButtonInput (id=previousbutton_Input) at ClaimDocuments.pcf: line 106, column 107
    function action_25 () : void {
      onbaseThumbnailUtil.decrementPageNumber()
    }
    
    // 'action' attribute on ButtonInput (id=nextbutton_Input) at ClaimDocuments.pcf: line 122, column 103
    function action_30 () : void {
      onbaseThumbnailUtil.incrementPageNumber(DocumentList.Count)
    }
    
    // 'action' attribute on ToolbarButton (id=RequestDocuments) at ClaimDocuments.pcf: line 299, column 59
    function action_38 () : void {
      ShareBaseFolderRequestWizard.push(Claim)
    }
    
    // 'action' attribute on ToolbarButton (id=ShareDocuments) at ClaimDocuments.pcf: line 304, column 59
    function action_41 () : void {
      ShareBaseShareDocumentsWizard.push(Claim)
    }
    
    // 'action' attribute on ToolbarButton (id=RefreshAsyncContent) at ClaimDocuments.pcf: line 352, column 110
    function action_53 () : void {
      
    }
    
    // 'action' attribute on ToolbarButton (id=RequestDocuments) at ClaimDocuments.pcf: line 78, column 58
    function action_dest_14 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestWizard.createDestination(Claim)
    }
    
    // 'action' attribute on ToolbarButton (id=ShareDocuments) at ClaimDocuments.pcf: line 83, column 59
    function action_dest_17 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsWizard.createDestination(Claim)
    }
    
    // 'action' attribute on ToolbarButton (id=RequestDocuments) at ClaimDocuments.pcf: line 299, column 59
    function action_dest_39 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestWizard.createDestination(Claim)
    }
    
    // 'action' attribute on ToolbarButton (id=ShareDocuments) at ClaimDocuments.pcf: line 304, column 59
    function action_dest_42 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsWizard.createDestination(Claim)
    }
    
    // 'available' attribute on ToolbarButton (id=RefreshAsyncContent) at ClaimDocuments.pcf: line 90, column 110
    function available_18 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentContentServerAvailable
    }
    
    // 'available' attribute on ButtonInput (id=previousbutton_Input) at ClaimDocuments.pcf: line 106, column 107
    function available_24 () : java.lang.Boolean {
      return onbaseThumbnailUtil.ThumbnailPageNumber > 1
    }
    
    // 'available' attribute on ButtonInput (id=nextbutton_Input) at ClaimDocuments.pcf: line 122, column 103
    function available_29 () : java.lang.Boolean {
      return onbaseThumbnailUtil.ThumbnailPageNumber < onbaseThumbnailUtil.pageCount(DocumentList.Count)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=ClaimDocuments_DeobsolesceButton) at ClaimDocuments.pcf: line 329, column 66
    function available_45 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=ClaimDocuments_DeobsolesceButton) at ClaimDocuments.pcf: line 329, column 66
    function checkedRowAction_47 (element :  entity.Document, CheckedValue :  entity.Document) : void {
       CheckedValue.Obsolete = false
    }
    
    // 'def' attribute on PanelRef (id=thumbnailPanel) at ClaimDocuments.pcf: line 63, column 63
    function def_onEnter_22 (def :  pcf.BlankLV) : void {
      def.onEnter()
    }
    
    // 'def' attribute on MenuItemSetRef at ClaimDocuments.pcf: line 293, column 59
    function def_onEnter_34 (def :  pcf.ClaimNewDocumentMenuItemSet) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at ClaimDocuments.pcf: line 285, column 27
    function def_onEnter_43 (def :  pcf.BlankLV) : void {
      def.onEnter()
    }
    
    // 'def' attribute on MenuItemSetRef at ClaimDocuments.pcf: line 335, column 59
    function def_onEnter_48 (def :  pcf.ClaimNewDocumentMenuItemSet) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at ClaimDocuments.pcf: line 309, column 64
    function def_onEnter_55 (def :  pcf.DocumentsLV) : void {
      def.onEnter(DocumentList,DocumentSearchCriteria)
    }
    
    // 'def' attribute on PanelRef (id=searchPanel1) at ClaimDocuments.pcf: line 56, column 29
    function def_onEnter_7 (def :  pcf.OnBaseClaimDocumentSearchDV) : void {
      def.onEnter(DocumentSearchCriteria, onbaseThumbnailUtil)
    }
    
    // 'def' attribute on MenuItemSetRef at ClaimDocuments.pcf: line 72, column 59
    function def_onEnter_9 (def :  pcf.ClaimNewDocumentMenuItemSet) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on MenuItemSetRef at ClaimDocuments.pcf: line 72, column 59
    function def_refreshVariables_10 (def :  pcf.ClaimNewDocumentMenuItemSet) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef (id=thumbnailPanel) at ClaimDocuments.pcf: line 63, column 63
    function def_refreshVariables_23 (def :  pcf.BlankLV) : void {
      def.refreshVariables()
    }
    
    // 'def' attribute on MenuItemSetRef at ClaimDocuments.pcf: line 293, column 59
    function def_refreshVariables_35 (def :  pcf.ClaimNewDocumentMenuItemSet) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at ClaimDocuments.pcf: line 285, column 27
    function def_refreshVariables_44 (def :  pcf.BlankLV) : void {
      def.refreshVariables()
    }
    
    // 'def' attribute on MenuItemSetRef at ClaimDocuments.pcf: line 335, column 59
    function def_refreshVariables_49 (def :  pcf.ClaimNewDocumentMenuItemSet) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at ClaimDocuments.pcf: line 309, column 64
    function def_refreshVariables_56 (def :  pcf.DocumentsLV) : void {
      def.refreshVariables(DocumentList,DocumentSearchCriteria)
    }
    
    // 'def' attribute on PanelRef (id=searchPanel1) at ClaimDocuments.pcf: line 56, column 29
    function def_refreshVariables_8 (def :  pcf.OnBaseClaimDocumentSearchDV) : void {
      def.refreshVariables(DocumentSearchCriteria, onbaseThumbnailUtil)
    }
    
    // TemplatePanel at ClaimDocuments.pcf: line 125, column 24
    function renderCall_33 (__writer :  java.io.Writer, __escaper :  gw.lang.parser.template.StringEscaper, __helper :  gw.api.web.template.TemplatePanelHelper) : void {
      pcfc.expressions.ClaimDocuments_TemplatePanel1.render(__writer, __escaper, DocumentList, onbaseThumbnailUtil)
    }
    
    // 'searchCriteria' attribute on SearchPanel at ClaimDocuments.pcf: line 53, column 78
    function searchCriteria_58 () : entity.DocumentSearchCriteria {
      return createSearchCriteria();
    }
    
    // 'search' attribute on SearchPanel at ClaimDocuments.pcf: line 53, column 78
    function search_57 () : java.lang.Object {
      return searchAndResetPageNum(DocumentSearchCriteria)
    }
    
    // 'value' attribute on TextInput (id=pageNumber_Input) at ClaimDocuments.pcf: line 113, column 34
    function value_27 () : String {
      return DisplayKey.get("Accelerator.OnBase.ThumbnailViewer.STR_GW_OnPageCountofCount", onbaseThumbnailUtil.ThumbnailPageNumber, (onbaseThumbnailUtil.pageCount(DocumentList.Count) as String))
    }
    
    // 'visible' attribute on ToolbarButton (id=AddDocuments) at ClaimDocuments.pcf: line 70, column 58
    function visible_11 () : java.lang.Boolean {
      return perm.Claim.createdocument(Claim)
    }
    
    // 'visible' attribute on ToolbarButton (id=RefreshAsyncContent) at ClaimDocuments.pcf: line 90, column 110
    function visible_19 () : java.lang.Boolean {
      return documentsActionsHelper.isShowAsynchronousRefreshAction(DocumentList.toTypedArray())
    }
    
    // 'visible' attribute on PanelRef (id=thumbnailPanel) at ClaimDocuments.pcf: line 63, column 63
    function visible_21 () : java.lang.Boolean {
      return onbaseThumbnailUtil.IsThumbnailViewActive
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=ClaimDocuments_DeobsolesceButton) at ClaimDocuments.pcf: line 329, column 66
    function visible_46 () : java.lang.Boolean {
      return DocumentSearchCriteria.IncludeObsoletes
    }
    
    // 'visible' attribute on PanelRef at ClaimDocuments.pcf: line 309, column 64
    function visible_54 () : java.lang.Boolean {
      return !onbaseThumbnailUtil.IsThumbnailViewActive
    }
    
    property get DocumentList () : gw.api.database.IQueryBeanResult<Document> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Document>
    }
    
    property get DocumentSearchCriteria () : entity.DocumentSearchCriteria {
      return getCriteriaValue(1) as entity.DocumentSearchCriteria
    }
    
    property set DocumentSearchCriteria ($arg :  entity.DocumentSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}