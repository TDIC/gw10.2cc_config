package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_NewLossDetailsScreen.WC7.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FNOLWizard_NewLossDetailsScreen_WC7Expressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_NewLossDetailsScreen.WC7.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FNOLWizard_NewLossDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_163 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.supervisor), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_165 () : void {
      if (Claim.supervisor != null) { ClaimContactDetailPopup.push(Claim.supervisor, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_166 () : void {
      ClaimContactDetailPopup.push(Claim.supervisor, Claim)
    }
    
    // 'action' attribute on HiddenLink (id=employmentDataClaim) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 432, column 41
    function action_284 () : void {
      Claim.EmploymentData.Claim = Claim
    }
    
    // 'action' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_295 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.FirstIntakeDoctor), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_297 () : void {
      if (Claim.FirstIntakeDoctor != null) { ClaimContactDetailPopup.push(Claim.FirstIntakeDoctor, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_298 () : void {
      ClaimContactDetailPopup.push(Claim.FirstIntakeDoctor, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_319 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.hospital), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_321 () : void {
      if (Claim.hospital != null) { ClaimContactDetailPopup.push(Claim.hospital, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_322 () : void {
      ClaimContactDetailPopup.push(Claim.hospital, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_65 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.claimant), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_67 () : void {
      if (Claim.claimant != null) { ClaimContactDetailPopup.push(Claim.claimant, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_69 () : void {
      ClaimContactDetailPopup.push(Claim.claimant, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_164 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.supervisor), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_167 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.supervisor, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_296 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.FirstIntakeDoctor), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_299 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.FirstIntakeDoctor, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_320 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.hospital), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_323 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.hospital, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_66 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.claimant), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_70 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.claimant, Claim)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=MakeFirstButton) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 429, column 113
    function checkedRowAction_283 (element :  entity.BodyPartDetails, CheckedValue :  entity.BodyPartDetails) : void {
      Claim.ClaimInjuryIncident.FirstBodyPart = CheckedValue
    }
    
    // 'def' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_160 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Claim.supervisor), null, Claim)
    }
    
    // 'def' attribute on InputSetRef at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 306, column 109
    function def_onEnter_221 (def :  pcf.TDIC_CCAddressInputSet) : void {
      def.onEnter(tdic.cc.config.fnol.TDIC_WC7FNOLHelper.getClaimAddressOwner(Claim))
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 417, column 29
    function def_onEnter_285 (def :  pcf.EditableBodyPartDetailsLV) : void {
      def.onEnter(Claim.ClaimInjuryIncident, false)
    }
    
    // 'def' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_292 (def :  pcf.ClaimNewDoctorOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Claim.FirstIntakeDoctor), null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_316 (def :  pcf.TDIC_ClaimNewMedCareOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Claim.hospital), null, Claim)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 624, column 33
    function def_onEnter_406 (def :  pcf.EditableConcurrentEmploymentLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_62 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(Person,null,Claim,typekey.ContactRole.TC_CLAIMANT)
    }
    
    // 'def' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_161 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Claim.supervisor), null, Claim)
    }
    
    // 'def' attribute on InputSetRef at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 306, column 109
    function def_refreshVariables_222 (def :  pcf.TDIC_CCAddressInputSet) : void {
      def.refreshVariables(tdic.cc.config.fnol.TDIC_WC7FNOLHelper.getClaimAddressOwner(Claim))
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 417, column 29
    function def_refreshVariables_286 (def :  pcf.EditableBodyPartDetailsLV) : void {
      def.refreshVariables(Claim.ClaimInjuryIncident, false)
    }
    
    // 'def' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_293 (def :  pcf.ClaimNewDoctorOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Claim.FirstIntakeDoctor), null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_317 (def :  pcf.TDIC_ClaimNewMedCareOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Claim.hospital), null, Claim)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 624, column 33
    function def_refreshVariables_407 (def :  pcf.EditableConcurrentEmploymentLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_63 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(Person,null,Claim,typekey.ContactRole.TC_CLAIMANT)
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 25, column 37
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_ClassCodeByLocation_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 188, column 27
    function defaultSetter_130 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimWorkComp.ClassCodeByLocation = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 198, column 44
    function defaultSetter_134 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.ClassCode = (__VALUE_TO_SET as entity.ClassCode)
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_Occupation_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 203, column 45
    function defaultSetter_141 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.claimant.Occupation = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EmploymentData_HireDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 216, column 50
    function defaultSetter_146 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.HireDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_HireState_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 223, column 38
    function defaultSetter_151 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.HireState = (__VALUE_TO_SET as typekey.State)
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_EmploymentStatus_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 229, column 53
    function defaultSetter_156 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.EmploymentStatus = (__VALUE_TO_SET as typekey.EmploymentStatusType)
    }
    
    // 'value' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_170 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.supervisor = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossCause_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 50, column 43
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.LossCause = (__VALUE_TO_SET as typekey.LossCause)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_IncidentReport_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 249, column 41
    function defaultSetter_180 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.IncidentReport = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_EmploymentInjury_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 268, column 43
    function defaultSetter_193 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentInjury = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=EmploymentData_InjuryStartDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 275, column 56
    function defaultSetter_200 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.InjuryStartTime = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=DateReportedtoEmployer_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 295, column 45
    function defaultSetter_212 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.DateRptdToEmployer = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_InsuredPremises_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 311, column 42
    function defaultSetter_224 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.InsuredPremises = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_JurisdictionState_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 318, column 45
    function defaultSetter_228 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.JurisdictionState = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on TextInput (id=Claim_EquipmentUsed_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 323, column 40
    function defaultSetter_233 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EquipmentUsed = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Claim_ActivityPerformed_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 328, column 44
    function defaultSetter_237 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ActivityPerformed = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 351, column 72
    function defaultSetter_241 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Catastrophe = (__VALUE_TO_SET as entity.Catastrophe)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_Severity_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 361, column 45
    function defaultSetter_248 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimInjuryIncident.Severity = (__VALUE_TO_SET as typekey.SeverityType)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_DeathReport_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 367, column 37
    function defaultSetter_252 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.DeathReport = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_PrimaryInjury_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 377, column 43
    function defaultSetter_256 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimInjuryIncident.GeneralInjuryType = (__VALUE_TO_SET as typekey.InjuryType)
    }
    
    // 'value' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 65, column 30
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.CallType_TDIC = (__VALUE_TO_SET as String)
    }
    
    // 'value' attribute on DateInput (id=Claim_DeathDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 385, column 48
    function defaultSetter_262 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.DeathDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_AccidentType_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 399, column 45
    function defaultSetter_272 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.AccidentType = (__VALUE_TO_SET as typekey.AccidentType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_DetailedInjury_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 406, column 51
    function defaultSetter_276 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimInjuryIncident.DetailedInjuryType = (__VALUE_TO_SET as typekey.DetailedInjuryType)
    }
    
    // 'value' attribute on TextInput (id=Claim_InjuryDescription_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 411, column 58
    function defaultSetter_280 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimInjuryIncident.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_MedicalReport_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 444, column 39
    function defaultSetter_288 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.MedicalReport = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_302 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.FirstIntakeDoctor = (__VALUE_TO_SET as entity.Doctor)
    }
    
    // 'value' attribute on TypeKeyInput (id=MedCase_MedicalTreatment_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 477, column 55
    function defaultSetter_312 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimInjuryIncident.MedicalTreatmentType = (__VALUE_TO_SET as typekey.MedicalTreatmentType)
    }
    
    // 'value' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_326 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.hospital = (__VALUE_TO_SET as entity.MedicalCareOrg)
    }
    
    // 'value' attribute on DateInput (id=MedCase_HospitalDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 500, column 41
    function defaultSetter_336 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.HospitalDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=Other_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 74, column 37
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Other_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=MedCase_HospitalDays_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 506, column 44
    function defaultSetter_340 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.HospitalDays = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=MedCase_TreatedInEmergencyRoom_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 511, column 56
    function defaultSetter_344 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.TreatedInEmergencyRoom_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=MedCase_HospitalizedOvernight_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 516, column 55
    function defaultSetter_348 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.HospitalizedOvernight_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_TimeLossReport_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 527, column 40
    function defaultSetter_353 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.TimeLossReport = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=Claim_DateLastWorked_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 550, column 68
    function defaultSetter_358 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimInjuryIncident.DateLastWorked_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=Claim_DateReturnedToWork_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 557, column 65
    function defaultSetter_364 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimInjuryIncident.ReturnToWorkDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_NumDaysWorkedPerWeek_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 563, column 47
    function defaultSetter_369 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.NumDaysWorked = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_NumHoursWorkedPerDay_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 569, column 47
    function defaultSetter_373 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.NumHoursWorked = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on CurrencyInput (id=EmploymentData_GrossWages_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 580, column 59
    function defaultSetter_377 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.GrossWages_TDIC = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_PayScheme_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 586, column 53
    function defaultSetter_381 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.PayScheme_TDIC = (__VALUE_TO_SET as typekey.PaySchemeType_TDIC)
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_PaidFullWages_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 591, column 52
    function defaultSetter_385 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.PaidFull = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_WagePaymentContinued_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 596, column 59
    function defaultSetter_389 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.WagePaymentCont = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Status_CoverageQuestion_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 81, column 44
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.CoverageInQuestion = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_ModifiedDutyAvailable_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 602, column 51
    function defaultSetter_394 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ModifiedDutyAvail = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_InjuredOnRegularJob_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 607, column 46
    function defaultSetter_399 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.InjuredRegularJob = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_ConcurrentEmployment_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 613, column 39
    function defaultSetter_403 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ConcurrentEmp = (__VALUE_TO_SET as typekey.YesNo)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Notification_FirstNoticeSuit_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 87, column 44
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.FirstNoticeSuit = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=DateOfNotice_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 94, column 39
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ReportedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_HowReported_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 100, column 48
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.HowReported = (__VALUE_TO_SET as typekey.HowReportedType)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claimant_ContactProhibited_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 127, column 44
    function defaultSetter_85 (__VALUE_TO_SET :  java.lang.Object) : void {
      ContactProhibited = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=Claimant_TaxNumber_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 156, column 43
    function encryptionExpression_112 (VALUE :  java.lang.String) : java.lang.String {
      return Claim.claimant.maskTaxId(VALUE)
    }
    
    // 'filter' attribute on TypeKeyInput (id=EmploymentData_HireState_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 223, column 38
    function filter_153 (VALUE :  typekey.State, VALUES :  typekey.State[]) : java.lang.Boolean {
      return VALUE.hasCategory(Country.TC_US)
    }
    
    // 'filter' attribute on TypeKeyInput (id=Claim_JurisdictionState_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 318, column 45
    function filter_230 (VALUE :  typekey.Jurisdiction, VALUES :  typekey.Jurisdiction[]) : java.lang.Boolean {
      return VALUE.hasCategory(Country.TC_US)
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 52, column 87
    function onChange_15 () : void {
      Wizard.GeneralLiabilityHelper.setDamageAccordingToLossCause()
    }
    
    // 'onPick' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_168 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.supervisor); var result = eval("Claim.supervisor = Claim.resolveContact(Claim.supervisor) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_300 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.FirstIntakeDoctor); var result = eval("Claim.FirstIntakeDoctor = Claim.resolveContact(Claim.FirstIntakeDoctor) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_324 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.hospital); var result = eval("Claim.hospital = Claim.resolveContact(Claim.hospital) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_71 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.claimant); var result = eval("Claim.claimant = Claim.resolveContact(Claim.claimant) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'value' attribute on Reflect at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 159, column 34
    function reflectionValue_107 (TRIGGER_INDEX :  int, VALUE :  entity.Person) : java.lang.Object {
      return VALUE.TaxID
    }
    
    // 'value' attribute on Reflect at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 278, column 28
    function reflectionValue_196 (TRIGGER_INDEX :  int, VALUE :  Object) : java.lang.Object {
      return VALUE
    }
    
    // 'value' attribute on Reflect at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 135, column 46
    function reflectionValue_88 (TRIGGER_INDEX :  int, VALUE :  entity.Person) : java.lang.Object {
      return VALUE.PrimaryPhoneValue
    }
    
    // 'value' attribute on Reflect at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 144, column 55
    function reflectionValue_95 (TRIGGER_INDEX :  int, VALUE :  entity.Person) : java.lang.Object {
      return VALUE.PrimaryAddressDisplayValue
    }
    
    // 'required' attribute on TextInput (id=Other_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 74, column 37
    function required_32 () : java.lang.Boolean {
      return Claim.CallType_TDIC == "Other"
    }
    
    // 'validationExpression' attribute on DateInput (id=EmploymentData_HireDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 216, column 50
    function validationExpression_144 () : java.lang.Object {
      return Claim.EmploymentData.HireDate == null || Claim.EmploymentData.HireDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'validationExpression' attribute on DateInput (id=EmploymentData_InjuryStartDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 275, column 56
    function validationExpression_198 () : java.lang.Object {
      return Claim.EmploymentData.InjuryStartTime == null || Claim.EmploymentData.InjuryStartTime < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'validationExpression' attribute on DateInput (id=DateReportedtoEmployer_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 295, column 45
    function validationExpression_210 () : java.lang.Object {
      return Claim.DateRptdToEmployer != null and Claim.DateRptdToEmployer > gw.api.util.DateUtil.currentDate() ? DisplayKey.get("Java.Validation.Date.ForbidFuture") : null
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_ReportedDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 304, column 39
    function validationExpression_216 () : java.lang.Object {
      return Claim.ReportedDate == null || Claim.ReportedDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_DeathDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 385, column 48
    function validationExpression_259 () : java.lang.Object {
      return Claim.DeathDate == null || Claim.DeathDate <= gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("TDIC.Validation.Date.NotFuture")
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_DateLastWorked_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 550, column 68
    function validationExpression_356 () : java.lang.Object {
      return Claim.ClaimInjuryIncident.DateLastWorked_TDIC == null || Claim.ClaimInjuryIncident.DateLastWorked_TDIC <= gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("TDIC.Validation.Date.NotFuture")
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_DateReturnedToWork_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 557, column 65
    function validationExpression_362 () : java.lang.Object {
      return Claim.ClaimInjuryIncident.DateLastWorked_TDIC == null or Claim.ClaimInjuryIncident.ReturnToWorkDate == null || Claim.ClaimInjuryIncident.ReturnToWorkDate >= Claim.ClaimInjuryIncident.DateLastWorked_TDIC ? null : DisplayKey.get("TDIC.Validation.ReturnToWorkDate.NotBeforeDateLastWorked")
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 32, column 35
    function validationExpression_4 () : java.lang.Object {
      return Claim.LossDate == null || Claim.LossDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'validationExpression' attribute on DateInput (id=DateOfNotice_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 94, column 39
    function validationExpression_49 () : java.lang.Object {
      return Claim.ReportedDate != null and Claim.ReportedDate > gw.api.util.DateUtil.currentDate() ? DisplayKey.get("Java.Validation.Date.ForbidFuture") : null
    }
    
    // 'valueRange' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 198, column 44
    function valueRange_136 () : java.lang.Object {
      return FilteredClassCodes
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 351, column 72
    function valueRange_243 () : java.lang.Object {
      return gw.api.admin.CatastropheUtil.getCatastrophes()
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 65, column 30
    function valueRange_28 () : java.lang.Object {
      return Claim.getCallType(Claim)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_304 () : java.lang.Object {
      return Claim.getRelatedContacts(Claim.getContactTypes(ContactRole.TC_FIRSTINTAKEDOCTOR)) as Doctor[]
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_328 () : java.lang.Object {
      return Claim.RelatedMedicalCareOrgArray
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_74 () : java.lang.Object {
      return Claim.RelatedPersonArray
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_ClassCodeByLocation_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 188, column 27
    function valueRoot_131 () : java.lang.Object {
      return Claim.ClaimWorkComp
    }
    
    // 'value' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 198, column 44
    function valueRoot_135 () : java.lang.Object {
      return Claim.EmploymentData
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 25, column 37
    function valueRoot_2 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_Severity_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 361, column 45
    function valueRoot_249 () : java.lang.Object {
      return Claim.ClaimInjuryIncident
    }
    
    // 'value' attribute on TextInput (id=Claimant_Phone_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 132, column 43
    function valueRoot_92 () : java.lang.Object {
      return Claim.claimant
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 25, column 37
    function value_0 () : java.lang.String {
      return Claim.Description
    }
    
    // 'value' attribute on DateInput (id=Claimant_DateOfBirth_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 150, column 43
    function value_103 () : java.util.Date {
      return Claim.claimant.DateOfBirth
    }
    
    // 'value' attribute on PrivacyInput (id=Claimant_TaxNumber_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 156, column 43
    function value_110 () : java.lang.String {
      return Claim.claimant.TaxID
    }
    
    // 'value' attribute on TypeKeyInput (id=Claimant_Gender_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 166, column 43
    function value_116 () : typekey.GenderType {
      return Claim.claimant.Gender
    }
    
    // 'value' attribute on BooleanRadioInput (id=RiskManagementIncident_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 42, column 48
    function value_12 () : java.lang.Boolean {
      return Claim.RiskMgmtIncident_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=Claimant_MaritalStatus_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 172, column 43
    function value_121 () : typekey.MaritalStatus {
      return Claim.claimant.MaritalStatus
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_EmploymentLocation_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 182, column 46
    function value_126 () : entity.PolicyLocation {
      return Claim.LocationCode
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_ClassCodeByLocation_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 188, column 27
    function value_129 () : java.lang.Boolean {
      return Claim.ClaimWorkComp.ClassCodeByLocation
    }
    
    // 'value' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 198, column 44
    function value_133 () : entity.ClassCode {
      return Claim.EmploymentData.ClassCode
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_Occupation_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 203, column 45
    function value_140 () : java.lang.String {
      return Claim.claimant.Occupation
    }
    
    // 'value' attribute on DateInput (id=EmploymentData_HireDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 216, column 50
    function value_145 () : java.util.Date {
      return Claim.EmploymentData.HireDate
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_HireState_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 223, column 38
    function value_150 () : typekey.State {
      return Claim.EmploymentData.HireState
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_EmploymentStatus_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 229, column 53
    function value_155 () : typekey.EmploymentStatusType {
      return Claim.EmploymentData.EmploymentStatus
    }
    
    // 'value' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_169 () : entity.Person {
      return Claim.supervisor
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossCause_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 50, column 43
    function value_17 () : typekey.LossCause {
      return Claim.LossCause
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_EmploymentInjury_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 268, column 43
    function value_192 () : java.lang.Boolean {
      return Claim.EmploymentInjury
    }
    
    // 'value' attribute on DateInput (id=EmploymentData_InjuryStartDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 275, column 56
    function value_199 () : java.util.Date {
      return Claim.EmploymentData.InjuryStartTime
    }
    
    // 'value' attribute on DateInput (id=DateReportedtoEmployer_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 295, column 45
    function value_211 () : java.util.Date {
      return Claim.DateRptdToEmployer
    }
    
    // 'value' attribute on CheckBoxInput (id=Status_IncidentReport_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 57, column 41
    function value_22 () : java.lang.Boolean {
      return Claim.IncidentReport
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_InsuredPremises_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 311, column 42
    function value_223 () : java.lang.Boolean {
      return Claim.InsuredPremises
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_JurisdictionState_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 318, column 45
    function value_227 () : typekey.Jurisdiction {
      return Claim.JurisdictionState
    }
    
    // 'value' attribute on TextInput (id=Claim_EquipmentUsed_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 323, column 40
    function value_232 () : java.lang.String {
      return Claim.EquipmentUsed
    }
    
    // 'value' attribute on TextInput (id=Claim_ActivityPerformed_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 328, column 44
    function value_236 () : java.lang.String {
      return Claim.ActivityPerformed
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 351, column 72
    function value_240 () : entity.Catastrophe {
      return Claim.Catastrophe
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_Severity_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 361, column 45
    function value_247 () : typekey.SeverityType {
      return Claim.ClaimInjuryIncident.Severity
    }
    
    // 'value' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 65, column 30
    function value_25 () : String {
      return Claim.CallType_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_DeathReport_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 367, column 37
    function value_251 () : java.lang.Boolean {
      return Claim.DeathReport
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_PrimaryInjury_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 377, column 43
    function value_255 () : typekey.InjuryType {
      return Claim.ClaimInjuryIncident.GeneralInjuryType
    }
    
    // 'value' attribute on DateInput (id=Claim_DeathDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 385, column 48
    function value_261 () : java.util.Date {
      return Claim.DeathDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_AccidentType_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 399, column 45
    function value_271 () : typekey.AccidentType {
      return Claim.AccidentType
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_DetailedInjury_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 406, column 51
    function value_275 () : typekey.DetailedInjuryType {
      return Claim.ClaimInjuryIncident.DetailedInjuryType
    }
    
    // 'value' attribute on TextInput (id=Claim_InjuryDescription_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 411, column 58
    function value_279 () : java.lang.String {
      return Claim.ClaimInjuryIncident.Description
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_MedicalReport_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 444, column 39
    function value_287 () : java.lang.Boolean {
      return Claim.MedicalReport
    }
    
    // 'value' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_301 () : entity.Doctor {
      return Claim.FirstIntakeDoctor
    }
    
    // 'value' attribute on TypeKeyInput (id=MedCase_MedicalTreatment_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 477, column 55
    function value_311 () : typekey.MedicalTreatmentType {
      return Claim.ClaimInjuryIncident.MedicalTreatmentType
    }
    
    // 'value' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_325 () : entity.MedicalCareOrg {
      return Claim.hospital
    }
    
    // 'value' attribute on TextInput (id=Other_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 74, column 37
    function value_33 () : java.lang.String {
      return Claim.Other_TDIC
    }
    
    // 'value' attribute on DateInput (id=MedCase_HospitalDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 500, column 41
    function value_335 () : java.util.Date {
      return Claim.HospitalDate
    }
    
    // 'value' attribute on TextInput (id=MedCase_HospitalDays_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 506, column 44
    function value_339 () : java.lang.Integer {
      return Claim.HospitalDays
    }
    
    // 'value' attribute on BooleanRadioInput (id=MedCase_TreatedInEmergencyRoom_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 511, column 56
    function value_343 () : java.lang.Boolean {
      return Claim.TreatedInEmergencyRoom_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=MedCase_HospitalizedOvernight_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 516, column 55
    function value_347 () : java.lang.Boolean {
      return Claim.HospitalizedOvernight_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_TimeLossReport_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 527, column 40
    function value_352 () : java.lang.Boolean {
      return Claim.TimeLossReport
    }
    
    // 'value' attribute on DateInput (id=Claim_DateLastWorked_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 550, column 68
    function value_357 () : java.util.Date {
      return Claim.ClaimInjuryIncident.DateLastWorked_TDIC
    }
    
    // 'value' attribute on DateInput (id=Claim_DateReturnedToWork_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 557, column 65
    function value_363 () : java.util.Date {
      return Claim.ClaimInjuryIncident.ReturnToWorkDate
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_NumDaysWorkedPerWeek_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 563, column 47
    function value_368 () : java.math.BigDecimal {
      return Claim.EmploymentData.NumDaysWorked
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_NumHoursWorkedPerDay_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 569, column 47
    function value_372 () : java.math.BigDecimal {
      return Claim.EmploymentData.NumHoursWorked
    }
    
    // 'value' attribute on CurrencyInput (id=EmploymentData_GrossWages_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 580, column 59
    function value_376 () : gw.api.financials.CurrencyAmount {
      return Claim.EmploymentData.GrossWages_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=Status_CoverageQuestion_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 81, column 44
    function value_38 () : java.lang.Boolean {
      return Claim.CoverageInQuestion
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_PayScheme_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 586, column 53
    function value_380 () : typekey.PaySchemeType_TDIC {
      return Claim.EmploymentData.PayScheme_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_PaidFullWages_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 591, column 52
    function value_384 () : java.lang.Boolean {
      return Claim.EmploymentData.PaidFull
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_WagePaymentContinued_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 596, column 59
    function value_388 () : java.lang.Boolean {
      return Claim.EmploymentData.WagePaymentCont
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_ModifiedDutyAvailable_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 602, column 51
    function value_393 () : java.lang.Boolean {
      return Claim.ModifiedDutyAvail
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_InjuredOnRegularJob_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 607, column 46
    function value_398 () : java.lang.Boolean {
      return Claim.InjuredRegularJob
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_ConcurrentEmployment_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 613, column 39
    function value_402 () : typekey.YesNo {
      return Claim.ConcurrentEmp
    }
    
    // 'value' attribute on BooleanRadioInput (id=Notification_FirstNoticeSuit_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 87, column 44
    function value_44 () : java.lang.Boolean {
      return Claim.FirstNoticeSuit
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 32, column 35
    function value_5 () : java.util.Date {
      return Claim.LossDate
    }
    
    // 'value' attribute on DateInput (id=DateOfNotice_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 94, column 39
    function value_50 () : java.util.Date {
      return Claim.ReportedDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_HowReported_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 100, column 48
    function value_55 () : typekey.HowReportedType {
      return Claim.HowReported
    }
    
    // 'value' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_72 () : entity.Person {
      return Claim.claimant
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claimant_ContactProhibited_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 127, column 44
    function value_84 () : java.lang.Boolean {
      return ContactProhibited
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossType_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 38, column 41
    function value_9 () : typekey.LossType {
      return Claim.LossType
    }
    
    // 'value' attribute on TextInput (id=Claimant_Phone_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 132, column 43
    function value_91 () : java.lang.String {
      return Claim.claimant.PrimaryPhoneValue
    }
    
    // 'value' attribute on TextInput (id=Claimant_Address_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 141, column 43
    function value_98 () : java.lang.String {
      return Claim.claimant.PrimaryAddressDisplayValue
    }
    
    // 'valueRange' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 198, column 44
    function verifyValueRangeIsAllowedType_137 ($$arg :  entity.ClassCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 198, column 44
    function verifyValueRangeIsAllowedType_137 ($$arg :  gw.api.database.IQueryBeanResult<entity.ClassCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 198, column 44
    function verifyValueRangeIsAllowedType_137 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_173 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_173 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_173 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 351, column 72
    function verifyValueRangeIsAllowedType_244 ($$arg :  entity.Catastrophe[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 351, column 72
    function verifyValueRangeIsAllowedType_244 ($$arg :  gw.api.database.IQueryBeanResult<entity.Catastrophe>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 351, column 72
    function verifyValueRangeIsAllowedType_244 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 65, column 30
    function verifyValueRangeIsAllowedType_29 ($$arg :  String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 65, column 30
    function verifyValueRangeIsAllowedType_29 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_305 ($$arg :  entity.Doctor[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_305 ($$arg :  gw.api.database.IQueryBeanResult<entity.Doctor>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_305 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_329 ($$arg :  entity.MedicalCareOrg[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_329 ($$arg :  gw.api.database.IQueryBeanResult<entity.MedicalCareOrg>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_329 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_75 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_75 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_75 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 198, column 44
    function verifyValueRange_138 () : void {
      var __valueRangeArg = FilteredClassCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_137(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_174 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_173(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 351, column 72
    function verifyValueRange_245 () : void {
      var __valueRangeArg = gw.api.admin.CatastropheUtil.getCatastrophes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_244(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 65, column 30
    function verifyValueRange_30 () : void {
      var __valueRangeArg = Claim.getCallType(Claim)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_29(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_306 () : void {
      var __valueRangeArg = Claim.getRelatedContacts(Claim.getContactTypes(ContactRole.TC_FIRSTINTAKEDOCTOR)) as Doctor[]
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_305(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_330 () : void {
      var __valueRangeArg = Claim.RelatedMedicalCareOrgArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_329(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_76 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_75(__valueRangeArg)
    }
    
    // 'valueType' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 239, column 38
    function verifyValueType_178 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 460, column 40
    function verifyValueType_310 () : void {
      var __valueTypeArg : entity.Doctor
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 494, column 48
    function verifyValueType_334 () : void {
      var __valueTypeArg : entity.MedicalCareOrg
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Claimant_Picker_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 121, column 44
    function verifyValueType_82 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'visible' attribute on TypeKeyInput (id=Claim_LossCause_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 50, column 43
    function visible_16 () : java.lang.Boolean {
      return Claim.Policy.Verified
    }
    
    // 'visible' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_162 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.supervisor), Claim, null as List<SpecialistService>)" != "" && false
    }
    
    // 'visible' attribute on DateInput (id=Claim_DeathDate_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 385, column 48
    function visible_260 () : java.lang.Boolean {
      return Claim.DeathReport == true
    }
    
    // 'visible' attribute on ClaimContactInput (id=MedCase_FirstIntakeDoctor_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_294 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.FirstIntakeDoctor), Claim, null as List<SpecialistService>)" != "" && false
    }
    
    // 'visible' attribute on ClaimContactInput (id=MedCase_Hospital_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_318 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.hospital), Claim, null as List<SpecialistService>)" != "" && false
    }
    
    // 'visible' attribute on InputSet (id=MedicalReport_InputSet) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 450, column 47
    function visible_351 () : java.lang.Boolean {
      return Claim.MedicalReport==true
    }
    
    // 'visible' attribute on BooleanRadioInput (id=Claim_ModifiedDutyAvailable_Input) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 602, column 51
    function visible_392 () : java.lang.Boolean {
      return Claim.TimeLossReport==true
    }
    
    // 'visible' attribute on InputSet (id=JobDetails) at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 619, column 51
    function visible_408 () : java.lang.Boolean {
      return Claim.ConcurrentEmp==TC_YES
    }
    
    // 'visible' attribute on DetailViewPanel at FNOLWizard_NewLossDetailsScreen.WC7.pcf: line 18, column 40
    function visible_59 () : java.lang.Boolean {
      return !Claim.Policy.Verified
    }
    
    // 'visible' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_61 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_64 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.claimant), Claim, null as List<SpecialistService>)" != "" && false
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get Wizard () : gw.api.claim.NewClaimWizardInfo {
      return getRequireValue("Wizard", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set Wizard ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setRequireValue("Wizard", 0, $arg)
    }
    
    property get ContactProhibited() : boolean {
          return Claim.getClaimContact(Claim.claimant).ContactProhibited
        }
        property set ContactProhibited(prohibited : boolean) {
          var claimContact = Claim.getClaimContact(Claim.claimant)
          if (claimContact != null) claimContact.ContactProhibited = prohibited
        }
    
        property get FilteredClassCodes() : ClassCode[] {
          if(Claim.ClaimWorkComp.ClassCodeByLocation) {
            return Claim.Policy.ClassCodes.where( \ code -> Claim.LocationCode.LocationBasedRisks*.ClassCode.contains(code) )
          }
          else {
            return Claim.Policy.ClassCodes
          }
        }
    
    
  }
  
  
}