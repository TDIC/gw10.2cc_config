package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/newtransaction/shared/NewPaymentDetailDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPaymentDetailDV_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/newtransaction/shared/NewPaymentDetailDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPaymentDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=ApplyDeductibleButton) at NewPaymentDetailDV.default.pcf: line 93, column 52
    function action_38 () : void {
      Payment.addDeductibleLineItem()
    }
    
    // 'action' attribute on ButtonInput (id=ApplyDeductible_Input) at NewPaymentDetailDV.default.pcf: line 149, column 103
    function action_81 () : void {
      Payment.calculateDeductible_TDIC()
    }
    
    // 'available' attribute on ToolbarButton (id=ApplyDeductibleButton) at NewPaymentDetailDV.default.pcf: line 93, column 52
    function available_36 () : java.lang.Boolean {
      return Payment.canAddDeductibleLineItem()
    }
    
    // 'available' attribute on ListViewInput at NewPaymentDetailDV.default.pcf: line 78, column 84
    function available_41 () : java.lang.Boolean {
      return Payment.ReserveLine != null
    }
    
    // 'available' attribute on ButtonInput (id=ApplyDeductible_Input) at NewPaymentDetailDV.default.pcf: line 149, column 103
    function available_80 () : java.lang.Boolean {
      return (Payment.Check.CheckFixedAmt_TDIC != null or Payment.Check.CheckPercentage_TDIC != null) and (Payment.TransactionAmountReservingAmountPair != null and Payment.TransactionAmountReservingAmountPair.Amount > 0)
    }
    
    // 'def' attribute on InputSetRef at NewPaymentDetailDV.default.pcf: line 31, column 72
    function def_onEnter_1 (def :  pcf.ReserveLineInputSet) : void {
      def.onEnter(Payment, reserveLineInputSetHelper)
    }
    
    // 'def' attribute on InputSetRef at NewPaymentDetailDV.default.pcf: line 71, column 62
    function def_onEnter_32 (def :  pcf.CheckExchangeRateInputSet) : void {
      def.onEnter(Payment.Check,null)
    }
    
    // 'def' attribute on ListViewInput at NewPaymentDetailDV.default.pcf: line 78, column 84
    function def_onEnter_43 (def :  pcf.EditablePaymentLineItemsLV_default) : void {
      def.onEnter(Payment)
    }
    
    // 'def' attribute on InputSetRef at NewPaymentDetailDV.default.pcf: line 153, column 57
    function def_onEnter_84 (def :  pcf.ServiceRequestInvoiceInputSet_multiple) : void {
      def.onEnter(Wizard, true)
    }
    
    // 'def' attribute on InputSetRef at NewPaymentDetailDV.default.pcf: line 153, column 57
    function def_onEnter_86 (def :  pcf.ServiceRequestInvoiceInputSet_single) : void {
      def.onEnter(Wizard, true)
    }
    
    // 'def' attribute on InputSetRef at NewPaymentDetailDV.default.pcf: line 31, column 72
    function def_refreshVariables_2 (def :  pcf.ReserveLineInputSet) : void {
      def.refreshVariables(Payment, reserveLineInputSetHelper)
    }
    
    // 'def' attribute on InputSetRef at NewPaymentDetailDV.default.pcf: line 71, column 62
    function def_refreshVariables_33 (def :  pcf.CheckExchangeRateInputSet) : void {
      def.refreshVariables(Payment.Check,null)
    }
    
    // 'def' attribute on ListViewInput at NewPaymentDetailDV.default.pcf: line 78, column 84
    function def_refreshVariables_44 (def :  pcf.EditablePaymentLineItemsLV_default) : void {
      def.refreshVariables(Payment)
    }
    
    // 'def' attribute on InputSetRef at NewPaymentDetailDV.default.pcf: line 153, column 57
    function def_refreshVariables_85 (def :  pcf.ServiceRequestInvoiceInputSet_multiple) : void {
      def.refreshVariables(Wizard, true)
    }
    
    // 'def' attribute on InputSetRef at NewPaymentDetailDV.default.pcf: line 153, column 57
    function def_refreshVariables_87 (def :  pcf.ServiceRequestInvoiceInputSet_single) : void {
      def.refreshVariables(Wizard, true)
    }
    
    // 'value' attribute on TextInput (id=Transaction_Comments_Input) at NewPaymentDetailDV.default.pcf: line 58, column 35
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      Payment.Comments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Payment_Currency_Input) at NewPaymentDetailDV.default.pcf: line 66, column 66
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      Wizard.CheckCurrency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on BooleanRadioInput (id=deductible_Input) at NewPaymentDetailDV.default.pcf: line 103, column 49
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      Payment.Check.Deductible_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=Payment_PaymentType_Input) at NewPaymentDetailDV.default.pcf: line 40, column 42
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      Payment.PaymentType = (__VALUE_TO_SET as typekey.PaymentType)
    }
    
    // 'value' attribute on TextInput (id=Percentage_Input) at NewPaymentDetailDV.default.pcf: line 123, column 51
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      Payment.Check.CheckPercentage_TDIC = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on Choice (id=CheckPortionPercentage) at NewPaymentDetailDV.default.pcf: line 114, column 39
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      Payment.Check.Percentage_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CurrencyInput (id=FixedAmount_Input) at NewPaymentDetailDV.default.pcf: line 140, column 51
    function defaultSetter_71 (__VALUE_TO_SET :  java.lang.Object) : void {
      Payment.Check.CheckFixedAmt_TDIC = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on Choice (id=CheckPortionFixedAmount) at NewPaymentDetailDV.default.pcf: line 132, column 39
    function defaultSetter_77 (__VALUE_TO_SET :  java.lang.Object) : void {
      Payment.Check.FixedAmont_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on TypeKeyInput (id=Payment_Currency_Input) at NewPaymentDetailDV.default.pcf: line 66, column 66
    function editable_24 () : java.lang.Boolean {
      return not Wizard.Check.LinkedToServiceRequests
    }
    
    // 'filter' attribute on TypeKeyInput (id=Payment_PaymentType_Input) at NewPaymentDetailDV.default.pcf: line 40, column 42
    function filter_7 (VALUE :  typekey.PaymentType, VALUES :  typekey.PaymentType[]) : java.lang.Boolean {
      return Wizard.isAllowedPaymentType(Payment, VALUE)
    }
    
    // 'initialValue' attribute on Variable at NewPaymentDetailDV.default.pcf: line 28, column 26
    function initialValue_0 () : Deductible {
      return Payment.SharedDeductible
    }
    
    // 'label' attribute on ToolbarButton (id=ApplyDeductibleButton) at NewPaymentDetailDV.default.pcf: line 93, column 52
    function label_39 () : java.lang.Object {
      return Payment.LabelForAddDeductibleButton
    }
    
    // 'mode' attribute on ListViewInput at NewPaymentDetailDV.default.pcf: line 78, column 84
    function mode_45 () : java.lang.Object {
      return getMode()
    }
    
    // 'mode' attribute on InputSetRef at NewPaymentDetailDV.default.pcf: line 153, column 57
    function mode_88 () : java.lang.Object {
      return Wizard.Check.ServiceRequestInvoices.Count == 1 ? "single" : "multiple"
    }
    
    // 'onChange' attribute on PostOnChange at NewPaymentDetailDV.default.pcf: line 68, column 57
    function onChange_23 () : void {
      Wizard.Check.unapplyDeductibles()
    }
    
    // 'onChange' attribute on PostOnChange at NewPaymentDetailDV.default.pcf: line 105, column 182
    function onChange_46 () : void {
      Payment.Check.Percentage_TDIC = null; Payment.Check.FixedAmont_TDIC = null; Payment.Check.CheckFixedAmt_TDIC = null; Payment.Check.CheckPercentage_TDIC = null
    }
    
    // 'onChange' attribute on PostOnChange at NewPaymentDetailDV.default.pcf: line 125, column 103
    function onChange_54 () : void {
      Payment.Check.FixedAmont_TDIC = null; Payment.Check.CheckFixedAmt_TDIC = null
    }
    
    // 'onChange' attribute on PostOnChange at NewPaymentDetailDV.default.pcf: line 142, column 105
    function onChange_67 () : void {
      Payment.Check.Percentage_TDIC = null; Payment.Check.CheckPercentage_TDIC = null
    }
    
    // 'option' attribute on Choice (id=CheckPortionPercentage) at NewPaymentDetailDV.default.pcf: line 114, column 39
    function option_62 () : java.lang.Object {
      return Payment.Check.Deductible_TDIC
    }
    
    // 'option' attribute on Choice (id=CheckPortionFixedAmount) at NewPaymentDetailDV.default.pcf: line 132, column 39
    function option_75 () : java.lang.Object {
      return Payment.Check.Deductible_TDIC 
    }
    
    // 'requestValidationExpression' attribute on TypeKeyInput (id=Payment_PaymentType_Input) at NewPaymentDetailDV.default.pcf: line 40, column 42
    function requestValidationExpression_3 (VALUE :  typekey.PaymentType) : java.lang.Object {
      return Wizard.validatePaymentType(Payment, VALUE)
    }
    
    // 'required' attribute on TextInput (id=Percentage_Input) at NewPaymentDetailDV.default.pcf: line 123, column 51
    function required_56 () : java.lang.Boolean {
      return Payment.Check.Percentage_TDIC and !Payment.Check.FixedAmont_TDIC
    }
    
    // 'required' attribute on CurrencyInput (id=FixedAmount_Input) at NewPaymentDetailDV.default.pcf: line 140, column 51
    function required_69 () : java.lang.Boolean {
      return Payment.Check.FixedAmont_TDIC and !Payment.Check.Percentage_TDIC
    }
    
    // 'tooltip' attribute on ToolbarButton (id=ApplyDeductibleButton) at NewPaymentDetailDV.default.pcf: line 93, column 52
    function tooltip_40 () : java.lang.String {
      return new gw.api.financials.PaymentUIHelper(Payment).DeductibleButtonToolTip
    }
    
    // 'validationExpression' attribute on ListViewInput at NewPaymentDetailDV.default.pcf: line 78, column 84
    function validationExpression_42 () : java.lang.Object {
      return Payment.getCheckWizardPaymentValidationExpression()
    }
    
    // 'value' attribute on CurrencyInput (id=Transaction_AvailableReserves_Input) at NewPaymentDetailDV.default.pcf: line 53, column 80
    function valueRoot_16 () : java.lang.Object {
      return gw.api.financials.FinancialsCalculationsForEditedTransaction.getAvailableReserves(Payment)
    }
    
    // 'value' attribute on TypeKeyInput (id=Payment_Currency_Input) at NewPaymentDetailDV.default.pcf: line 66, column 66
    function valueRoot_28 () : java.lang.Object {
      return Wizard
    }
    
    // 'value' attribute on BooleanRadioInput (id=deductible_Input) at NewPaymentDetailDV.default.pcf: line 103, column 49
    function valueRoot_50 () : java.lang.Object {
      return Payment.Check
    }
    
    // 'value' attribute on TypeKeyInput (id=Payment_PaymentType_Input) at NewPaymentDetailDV.default.pcf: line 40, column 42
    function valueRoot_6 () : java.lang.Object {
      return Payment
    }
    
    // 'value' attribute on BooleanRadioInput (id=Payment_Eroding_Input) at NewPaymentDetailDV.default.pcf: line 46, column 98
    function value_10 () : java.lang.Boolean {
      return Payment.ErodesReserves
    }
    
    // 'value' attribute on CurrencyInput (id=Transaction_AvailableReserves_Input) at NewPaymentDetailDV.default.pcf: line 53, column 80
    function value_15 () : gw.api.financials.IPairedMoney {
      return gw.api.financials.FinancialsCalculationsForEditedTransaction.getAvailableReserves(Payment).ReservingAmountTransactionAmountPair
    }
    
    // 'value' attribute on TextInput (id=Transaction_Comments_Input) at NewPaymentDetailDV.default.pcf: line 58, column 35
    function value_19 () : java.lang.String {
      return Payment.Comments
    }
    
    // 'value' attribute on TypeKeyInput (id=Payment_Currency_Input) at NewPaymentDetailDV.default.pcf: line 66, column 66
    function value_26 () : typekey.Currency {
      return Wizard.CheckCurrency
    }
    
    // 'value' attribute on TypeKeyInput (id=Payment_PaymentType_Input) at NewPaymentDetailDV.default.pcf: line 40, column 42
    function value_4 () : typekey.PaymentType {
      return Payment.PaymentType
    }
    
    // 'value' attribute on BooleanRadioInput (id=deductible_Input) at NewPaymentDetailDV.default.pcf: line 103, column 49
    function value_48 () : java.lang.Boolean {
      return Payment.Check.Deductible_TDIC
    }
    
    // 'value' attribute on TextInput (id=Percentage_Input) at NewPaymentDetailDV.default.pcf: line 123, column 51
    function value_57 () : java.math.BigDecimal {
      return Payment.Check.CheckPercentage_TDIC
    }
    
    // 'value' attribute on Choice (id=CheckPortionPercentage) at NewPaymentDetailDV.default.pcf: line 114, column 39
    function value_63 () : java.lang.Boolean {
      return Payment.Check.Percentage_TDIC
    }
    
    // 'value' attribute on CurrencyInput (id=FixedAmount_Input) at NewPaymentDetailDV.default.pcf: line 140, column 51
    function value_70 () : gw.api.financials.CurrencyAmount {
      return Payment.Check.CheckFixedAmt_TDIC
    }
    
    // 'value' attribute on Choice (id=CheckPortionFixedAmount) at NewPaymentDetailDV.default.pcf: line 132, column 39
    function value_76 () : java.lang.Boolean {
      return Payment.Check.FixedAmont_TDIC
    }
    
    // 'visible' attribute on CurrencyInput (id=Transaction_AvailableReserves_Input) at NewPaymentDetailDV.default.pcf: line 53, column 80
    function visible_14 () : java.lang.Boolean {
      return Payment.ReserveLine != null and not Payment.ReserveLine.New
    }
    
    // 'visible' attribute on TypeKeyInput (id=Payment_Currency_Input) at NewPaymentDetailDV.default.pcf: line 66, column 66
    function visible_25 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'addVisible' attribute on IteratorButtons at NewPaymentDetailDV.default.pcf: line 85, column 90
    function visible_34 () : java.lang.Boolean {
      return gw.api.financials.FinancialsUtil.isAllowMultipleLineItems()
    }
    
    // 'visible' attribute on ToolbarButton (id=ApplyDeductibleButton) at NewPaymentDetailDV.default.pcf: line 93, column 52
    function visible_37 () : java.lang.Boolean {
      return Payment.DeductibleAvailable
    }
    
    // 'visible' attribute on BooleanRadioInput (id=deductible_Input) at NewPaymentDetailDV.default.pcf: line 103, column 49
    function visible_47 () : java.lang.Boolean {
      return serviceRequestInvoice == null
    }
    
    // 'visible' attribute on InputSetRef at NewPaymentDetailDV.default.pcf: line 153, column 57
    function visible_83 () : java.lang.Boolean {
      return Wizard.Check.LinkedToServiceRequests
    }
    
    // 'visible' attribute on BooleanRadioInput (id=Payment_Eroding_Input) at NewPaymentDetailDV.default.pcf: line 46, column 98
    function visible_9 () : java.lang.Boolean {
      return !Payment.Exposure.Closed and Payment.PaymentType != PaymentType.TC_SUPPLEMENT
    }
    
    property get Payment () : Payment {
      return getRequireValue("Payment", 0) as Payment
    }
    
    property set Payment ($arg :  Payment) {
      setRequireValue("Payment", 0, $arg)
    }
    
    property get Wizard () : gw.api.financials.CheckWizardInfo {
      return getRequireValue("Wizard", 0) as gw.api.financials.CheckWizardInfo
    }
    
    property set Wizard ($arg :  gw.api.financials.CheckWizardInfo) {
      setRequireValue("Wizard", 0, $arg)
    }
    
    property get deductible () : Deductible {
      return getVariableValue("deductible", 0) as Deductible
    }
    
    property set deductible ($arg :  Deductible) {
      setVariableValue("deductible", 0, $arg)
    }
    
    property get reserveLineInputSetHelper () : gw.api.financials.ReserveLineInputSetHelper {
      return getRequireValue("reserveLineInputSetHelper", 0) as gw.api.financials.ReserveLineInputSetHelper
    }
    
    property set reserveLineInputSetHelper ($arg :  gw.api.financials.ReserveLineInputSetHelper) {
      setRequireValue("reserveLineInputSetHelper", 0, $arg)
    }
    
    property get serviceRequestInvoice () : ServiceRequestInvoice {
      return getRequireValue("serviceRequestInvoice", 0) as ServiceRequestInvoice
    }
    
    property set serviceRequestInvoice ($arg :  ServiceRequestInvoice) {
      setRequireValue("serviceRequestInvoice", 0, $arg)
    }
    
    function getMode() : String {
      if (Payment.LineItems.countWhere(\ t -> t.LineCategory==TC_DEDUCTIBLE) == 1) {
        return "deductible"
      } else {
        return "default"
      }
    }
    
    
  }
  
  
}