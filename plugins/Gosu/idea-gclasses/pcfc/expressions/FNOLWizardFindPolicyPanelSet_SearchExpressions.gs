package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses java.util.Date
@javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizardFindPolicyPanelSet.Search.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FNOLWizardFindPolicyPanelSet_SearchExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizardFindPolicyPanelSet.Search.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FNOLWizardFindPolicyPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber2_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 282, column 58
    function defaultSetter_143 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 292, column 43
    function defaultSetter_151 (__VALUE_TO_SET :  java.lang.Object) : void {
      claimLossDate.LossDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=Claim_lossTime_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 298, column 43
    function defaultSetter_156 (__VALUE_TO_SET :  java.lang.Object) : void {
      claimLossDate.LossTime = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeRadioInput (id=ClaimMode_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 310, column 49
    function defaultSetter_162 (__VALUE_TO_SET :  java.lang.Object) : void {
      Wizard.ClaimMode = (__VALUE_TO_SET as gw.api.claim.NewClaimMode)
    }
    
    // 'initialValue' attribute on Variable at FNOLWizardFindPolicyPanelSet.Search.pcf: line 17, column 42
    function initialValue_0 () : gw.pcf.FNOLFindPolicyUtils {
      Wizard.FindPolicyUtils = new gw.pcf.FNOLFindPolicyUtils(Wizard, Claim); return Wizard.FindPolicyUtils
    }
    
    // 'initialValue' attribute on Variable at FNOLWizardFindPolicyPanelSet.Search.pcf: line 21, column 47
    function initialValue_1 () : gw.api.claim.ClaimLossDateProxy {
      return new gw.api.claim.ClaimLossDateProxy(Claim)
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizardFindPolicyPanelSet.Search.pcf: line 312, column 68
    function onChange_159 () : void {
      FindPolicyUtils.onClaimModeSelectionMade()
    }
    
    // 'showConfirmMessage' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 292, column 43
    function showConfirmMessage_149 () : java.lang.Boolean {
      return not Wizard.AtHighWaterMark
    }
    
    // 'showConfirmMessage' attribute on RangeRadioInput (id=ClaimMode_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 310, column 49
    function showConfirmMessage_160 () : java.lang.Boolean {
      return !Wizard.AtHighWaterMark
    }
    
    // 'sortBy' attribute on IteratorSort at FNOLWizardFindPolicyPanelSet.Search.pcf: line 333, column 32
    function sortBy_169 (otherClaim :  entity.PriorClaimView) : java.lang.Object {
      return otherClaim.LossDate
    }
    
    // 'value' attribute on TypeKeyCell (id=otherStatus_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 340, column 51
    function sortValue_170 (otherClaim :  entity.PriorClaimView) : java.lang.Object {
      return otherClaim.State
    }
    
    // 'value' attribute on TextCell (id=otherID_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 346, column 33
    function sortValue_171 (otherClaim :  entity.PriorClaimView) : java.lang.Object {
      return otherClaim.ClaimNumber
    }
    
    // 'value' attribute on DateCell (id=otherLossDate_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 351, column 48
    function sortValue_172 (otherClaim :  entity.PriorClaimView) : java.lang.Object {
      return otherClaim.LossDate
    }
    
    // 'value' attribute on TextCell (id=otherInsured_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 355, column 51
    function sortValue_173 (otherClaim :  entity.PriorClaimView) : java.lang.Object {
      return otherClaim.Description
    }
    
    // 'validationExpression' attribute on TextInput (id=ClaimNumber2_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 282, column 58
    function validationExpression_140 () : java.lang.Object {
      return !Claim.DuplicateClaimNumber ? null : DisplayKey.get("Java.NewClaimWizard.Error.ClaimNumberNotUnique", Claim.ClaimNumber)
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 292, column 43
    function validationExpression_148 () : java.lang.Object {
      return Claim.validateLossDate()
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=ClaimMode_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 310, column 49
    function valueRange_164 () : java.lang.Object {
      return FindPolicyUtils.ClaimModeArray
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber2_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 282, column 58
    function valueRoot_144 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 292, column 43
    function valueRoot_152 () : java.lang.Object {
      return claimLossDate
    }
    
    // 'value' attribute on RangeRadioInput (id=ClaimMode_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 310, column 49
    function valueRoot_163 () : java.lang.Object {
      return Wizard
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber2_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 282, column 58
    function value_142 () : java.lang.String {
      return Claim.ClaimNumber
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 292, column 43
    function value_150 () : java.util.Date {
      return claimLossDate.LossDate
    }
    
    // 'value' attribute on DateInput (id=Claim_lossTime_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 298, column 43
    function value_155 () : java.util.Date {
      return claimLossDate.LossTime
    }
    
    // 'value' attribute on RangeRadioInput (id=ClaimMode_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 310, column 49
    function value_161 () : gw.api.claim.NewClaimMode {
      return Wizard.ClaimMode
    }
    
    // 'value' attribute on RowIterator at FNOLWizardFindPolicyPanelSet.Search.pcf: line 329, column 89
    function value_196 () : gw.api.database.IQueryBeanResult<entity.PriorClaimView> {
      return Claim.findPriorClaimsByPolicyNumber(FindPolicyUtils.SelectedPolicySummary.PolicyNumber, {}) as gw.api.database.IQueryBeanResult<PriorClaimView>
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=ClaimMode_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 310, column 49
    function verifyValueRangeIsAllowedType_165 ($$arg :  gw.api.claim.NewClaimMode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=ClaimMode_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 310, column 49
    function verifyValueRangeIsAllowedType_165 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=ClaimMode_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 310, column 49
    function verifyValueRange_166 () : void {
      var __valueRangeArg = FindPolicyUtils.ClaimModeArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_165(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=ClaimNumber2_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 282, column 58
    function visible_141 () : java.lang.Boolean {
      return !Claim.ClaimNumberGenerationEnabled
    }
    
    // 'visible' attribute on Label at FNOLWizardFindPolicyPanelSet.Search.pcf: line 318, column 62
    function visible_168 () : java.lang.Boolean {
      return FindPolicyUtils.isClaimHistoryVisible()
    }
    
    // 'visible' attribute on DetailViewPanel at FNOLWizardFindPolicyPanelSet.Search.pcf: line 271, column 63
    function visible_198 () : java.lang.Boolean {
      return FindPolicyUtils.SelectedPolicySummary != null
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get FindPolicyUtils () : gw.pcf.FNOLFindPolicyUtils {
      return getVariableValue("FindPolicyUtils", 0) as gw.pcf.FNOLFindPolicyUtils
    }
    
    property set FindPolicyUtils ($arg :  gw.pcf.FNOLFindPolicyUtils) {
      setVariableValue("FindPolicyUtils", 0, $arg)
    }
    
    property get PolicySum () : PolicySummary {
      return getVariableValue("PolicySum", 0) as PolicySummary
    }
    
    property set PolicySum ($arg :  PolicySummary) {
      setVariableValue("PolicySum", 0, $arg)
    }
    
    property get Wizard () : gw.api.claim.NewClaimWizardInfo {
      return getRequireValue("Wizard", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set Wizard ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setRequireValue("Wizard", 0, $arg)
    }
    
    property get claimLossDate () : gw.api.claim.ClaimLossDateProxy {
      return getVariableValue("claimLossDate", 0) as gw.api.claim.ClaimLossDateProxy
    }
    
    property set claimLossDate ($arg :  gw.api.claim.ClaimLossDateProxy) {
      setVariableValue("claimLossDate", 0, $arg)
    }
    
    
        function paidThruDate(ps : PolicySummary) : Date{
          if(ps.PaidThruDate_TDIC == null){
            return ps.CancellationDate_TDIC
          }else{
            return ps.PaidThruDate_TDIC
          }
        }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizardFindPolicyPanelSet.Search.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends FNOLWizardFindPolicyPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=otherID_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 346, column 33
    function action_177 () : void {
      FindPolicyUtils.gotoClaimFile(CurrentLocation as pcf.api.Wizard, otherClaim.Claim)
    }
    
    // 'action' attribute on TextCell (id=otherClaimant_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 366, column 63
    function action_190 () : void {
      UserContactDetailPopup.push(otherClaim.Claim.AssignedUser)
    }
    
    // 'action' attribute on TextCell (id=otherClaimant_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 366, column 63
    function action_dest_191 () : pcf.api.Destination {
      return pcf.UserContactDetailPopup.createDestination(otherClaim.Claim.AssignedUser)
    }
    
    // 'highlighted' attribute on Row at FNOLWizardFindPolicyPanelSet.Search.pcf: line 335, column 61
    function highlighted_195 () : java.lang.Boolean {
      return otherClaim.State != TC_CLOSED
    }
    
    // 'value' attribute on TypeKeyCell (id=otherStatus_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 340, column 51
    function valueRoot_175 () : java.lang.Object {
      return otherClaim
    }
    
    // 'value' attribute on TypeKeyCell (id=otherStatus_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 340, column 51
    function value_174 () : typekey.ClaimState {
      return otherClaim.State
    }
    
    // 'value' attribute on TextCell (id=otherID_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 346, column 33
    function value_178 () : java.lang.String {
      return otherClaim.ClaimNumber
    }
    
    // 'value' attribute on DateCell (id=otherLossDate_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 351, column 48
    function value_181 () : java.util.Date {
      return otherClaim.LossDate
    }
    
    // 'value' attribute on TextCell (id=otherInsured_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 355, column 51
    function value_184 () : java.lang.String {
      return otherClaim.Description
    }
    
    // 'value' attribute on DateCell (id=otherPolicyID_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 360, column 52
    function value_187 () : java.util.Date {
      return otherClaim.ReportedDate
    }
    
    // 'value' attribute on TextCell (id=otherClaimant_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 366, column 63
    function value_192 () : java.lang.String {
      return otherClaim.AssignedUserDisplayName
    }
    
    property get otherClaim () : entity.PriorClaimView {
      return getIteratedValue(1) as entity.PriorClaimView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizardFindPolicyPanelSet.Search.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=selectButton) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 179, column 76
    function action_83 () : void {
      FindPolicyUtils.selectPolicyRow(PolicySummary, CurrentLocation as pcf.api.Wizard)
    }
    
    // 'action' attribute on Link (id=unselectButton) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 188, column 76
    function action_86 () : void {
      FindPolicyUtils.resetVariables(); Wizard.resetSteps(); gw.api.util.SearchUtil.search();
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 195, column 31
    function action_88 () : void {
      PolicySummaryInfoPopup.push(FindPolicyUtils.SelectedPolicySummary == null ? PolicySummary : FindPolicyUtils.SelectedPolicySummary)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 195, column 31
    function action_dest_89 () : pcf.api.Destination {
      return pcf.PolicySummaryInfoPopup.createDestination(FindPolicyUtils.SelectedPolicySummary == null ? PolicySummary : FindPolicyUtils.SelectedPolicySummary)
    }
    
    // 'highlighted' attribute on Row at FNOLWizardFindPolicyPanelSet.Search.pcf: line 167, column 103
    function highlighted_135 () : java.lang.Boolean {
      return PolicySummary.isSamePolicySummary( FindPolicyUtils.SelectedPolicySummary)
    }
    
    // 'showConfirmMessage' attribute on Link (id=selectButton) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 179, column 76
    function showConfirmMessage_84 () : java.lang.Boolean {
      return Claim.Policy != null and Claim.Policy.PolicyType != PolicySummary.PolicyType
    }
    
    // 'showConfirmMessage' attribute on Link (id=unselectButton) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 188, column 76
    function showConfirmMessage_87 () : java.lang.Boolean {
      return Claim.Policy != null
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 195, column 31
    function valueRoot_91 () : java.lang.Object {
      return PolicySummary
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 218, column 31
    function value_102 () : typekey.State {
      return PolicySummary.State
    }
    
    // 'value' attribute on TextCell (id=Zip_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 222, column 51
    function value_105 () : java.lang.String {
      return PolicySummary.PostalCode
    }
    
    // 'value' attribute on DateCell (id=Effective_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 226, column 54
    function value_108 () : java.util.Date {
      return PolicySummary.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=Expiration_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 230, column 55
    function value_111 () : java.util.Date {
      return PolicySummary.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=OFFERING_TDIC_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 234, column 54
    function value_114 () : java.lang.String {
      return PolicySummary.OFFERING_TDIC
    }
    
    // 'value' attribute on DateCell (id=PaidThruDate_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 238, column 54
    function value_117 () : java.util.Date {
      return paidThruDate(PolicySummary)
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 244, column 31
    function value_119 () : typekey.PolicyType {
      return PolicySummary.PolicyType
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 250, column 73
    function value_122 () : typekey.PolicyStatus {
      return PolicySummary.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=FirstInvoice_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 255, column 44
    function value_126 () : typekey.YesNo {
      return PolicySummary.FirstInvoicePaidFull_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=UnpaidPolicy_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 260, column 44
    function value_129 () : typekey.YesNo {
      return PolicySummary.UnpaidPolicyChangeInvoice_TDIC
    }
    
    // 'value' attribute on TextCell (id=ADANumber_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 264, column 55
    function value_132 () : java.lang.String {
      return PolicySummary.ADANumber_TDIC
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 195, column 31
    function value_90 () : java.lang.String {
      return PolicySummary.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 200, column 31
    function value_93 () : java.lang.String {
      return PolicySummary.InsuredName
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 206, column 55
    function value_96 () : java.lang.String {
      return PolicySummary.DisplayAddress
    }
    
    // 'value' attribute on TextCell (id=City_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 212, column 31
    function value_99 () : java.lang.String {
      return PolicySummary.DisplayCity
    }
    
    // 'visible' attribute on TypeKeyCell (id=Status_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 250, column 73
    function visible_124 () : java.lang.Boolean {
      return policySearchCriteria.IncludeArchived == true
    }
    
    // 'visible' attribute on Link (id=selectButton) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 179, column 76
    function visible_82 () : java.lang.Boolean {
      return FindPolicyUtils.SelectedPolicySummary == null
    }
    
    // 'visible' attribute on Link (id=unselectButton) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 188, column 76
    function visible_85 () : java.lang.Boolean {
      return FindPolicyUtils.SelectedPolicySummary != null
    }
    
    property get PolicySummary () : entity.PolicySummary {
      return getIteratedValue(2) as entity.PolicySummary
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizardFindPolicyPanelSet.Search.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends FNOLWizardFindPolicyPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=Search) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 143, column 33
    function action_63 () : void {
      FindPolicyUtils.resetVariables(); gw.api.util.SearchUtil.search(); if (PolicySummaryList.getCount() == 1) {FindPolicyUtils.selectPolicyRow(PolicySummaryList.getFirstResult(), CurrentLocation as pcf.api.Wizard);}
    }
    
    // 'action' attribute on Link (id=Reset) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 151, column 33
    function action_65 () : void {
      gw.api.util.SearchUtil.reset(); FindPolicyUtils.resetVariables(); FindPolicyUtils.PolicySearchCriteria.resetPolicySearchCriteria(); 
    }
    
    // 'available' attribute on RangeInput (id=CL_CoverageType_TDIC_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 89, column 95
    function available_31 () : java.lang.Boolean {
      return policySearchCriteria.ClaimType_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY
    }
    
    // 'available' attribute on Link (id=Search) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 143, column 33
    function available_62 () : java.lang.Boolean {
      return FindPolicyUtils.SelectedPolicySummary == null
    }
    
    // 'def' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 49, column 60
    function def_onEnter_11 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(policySearchCriteria))
    }
    
    // 'def' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 49, column 60
    function def_onEnter_13 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(policySearchCriteria))
    }
    
    // 'def' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 111, column 200
    function def_onEnter_55 (def :  pcf.FNOLWizard_PolicySearchInputSet_default) : void {
      def.onEnter(Claim, FindPolicyUtils.PolicySearchCriteria)
    }
    
    // 'def' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 46, column 60
    function def_onEnter_6 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(policySearchCriteria))
    }
    
    // 'def' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 46, column 60
    function def_onEnter_8 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(policySearchCriteria))
    }
    
    // 'def' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 49, column 60
    function def_refreshVariables_12 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(policySearchCriteria))
    }
    
    // 'def' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 49, column 60
    function def_refreshVariables_14 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(policySearchCriteria))
    }
    
    // 'def' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 111, column 200
    function def_refreshVariables_56 (def :  pcf.FNOLWizard_PolicySearchInputSet_default) : void {
      def.refreshVariables(Claim, FindPolicyUtils.PolicySearchCriteria)
    }
    
    // 'def' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 46, column 60
    function def_refreshVariables_7 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(policySearchCriteria))
    }
    
    // 'def' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 46, column 60
    function def_refreshVariables_9 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(policySearchCriteria))
    }
    
    // 'value' attribute on TypeKeyInput (id=PolicyType_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 57, column 45
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      policySearchCriteria.PolicyType = (__VALUE_TO_SET as typekey.PolicyType)
    }
    
    // 'value' attribute on DateInput (id=date_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 64, column 52
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      policySearchCriteria.LossDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimLossType_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 73, column 48
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      policySearchCriteria.ClaimType_TDIC = (__VALUE_TO_SET as typekey.ClaimType_TDIC)
    }
    
    // 'value' attribute on TextInput (id=policyNumber_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 43, column 56
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      policySearchCriteria.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=CL_CoverageType_TDIC_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 89, column 95
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      policySearchCriteria.CL_CoverageType_TDIC = (__VALUE_TO_SET as typekey.CL_CoverageType_TDIC)
    }
    
    // 'value' attribute on DateInput (id=reportdate_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 97, column 60
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      policySearchCriteria.ReportedDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on CheckBoxInput (id=IncludeArchivedPolicies_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 106, column 92
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      policySearchCriteria.IncludeArchived = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=DBAOrganizationName_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 128, column 64
    function defaultSetter_59 (__VALUE_TO_SET :  java.lang.Object) : void {
      policySearchCriteria.DBAOrganization_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'filter' attribute on TypeKeyInput (id=PolicyType_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 57, column 45
    function filter_19 (VALUE :  typekey.PolicyType, VALUES :  typekey.PolicyType[]) : java.lang.Object {
      return FindPolicyUtils.getPolicyType_TDIC()
    }
    
    // 'mode' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 46, column 60
    function mode_10 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'mode' attribute on InputSetRef at FNOLWizardFindPolicyPanelSet.Search.pcf: line 111, column 200
    function mode_57 () : java.lang.Object {
      return FindPolicyUtils.PolicySearchCriteria.InsuredAddress.Country != null ? FindPolicyUtils.PolicySearchCriteria.InsuredAddress.Country : gw.api.admin.BaseAdminUtil.getDefaultCountry()
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizardFindPolicyPanelSet.Search.pcf: line 76, column 36
    function onChange_25 () : void {
      Claim.Type_TDIC = policySearchCriteria.ClaimType_TDIC; Claim.LossCause = null; Claim.ResultType_TDIC = null
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizardFindPolicyPanelSet.Search.pcf: line 99, column 159
    function onChange_43 () : void {
      if(Claim.Policy.Offerings_TDIC == Offering_TDIC.TC_PROFLIABCLAIMSMADE){Claim.ReportedDate = policySearchCriteria.ReportedDate_TDIC}
    }
    
    // 'required' attribute on DateInput (id=reportdate_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 97, column 60
    function required_44 () : java.lang.Boolean {
      return Claim.setReportDate(policySearchCriteria)
    }
    
    // 'searchCriteria' attribute on SearchPanel at FNOLWizardFindPolicyPanelSet.Search.pcf: line 34, column 81
    function searchCriteria_138 () : entity.PolicySearchCriteria {
      return FindPolicyUtils.PolicySearchCriteria
    }
    
    // 'searchOnEnter' attribute on SearchPanel at FNOLWizardFindPolicyPanelSet.Search.pcf: line 34, column 81
    function searchOnEnter_139 () : java.lang.Boolean {
      return Claim.LossType != null
    }
    
    // 'search' attribute on SearchPanel at FNOLWizardFindPolicyPanelSet.Search.pcf: line 34, column 81
    function search_137 () : java.lang.Object {
      return FindPolicyUtils.performSearch()
    }
    
    // 'showConfirmMessage' attribute on TypeKeyInput (id=ClaimLossType_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 73, column 48
    function showConfirmMessage_26 () : java.lang.Boolean {
      return Claim.Policy.PolicyType != null and Claim.LossType != Wizard.LossType 
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 195, column 31
    function sortValue_66 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 200, column 31
    function sortValue_67 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.InsuredName
    }
    
    // 'sortBy' attribute on TextCell (id=Address_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 206, column 55
    function sortValue_68 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.AddressLine1
    }
    
    // 'sortBy' attribute on TextCell (id=City_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 212, column 31
    function sortValue_69 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.City
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 218, column 31
    function sortValue_70 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.State
    }
    
    // 'value' attribute on TextCell (id=Zip_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 222, column 51
    function sortValue_71 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.PostalCode
    }
    
    // 'value' attribute on DateCell (id=Effective_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 226, column 54
    function sortValue_72 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=Expiration_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 230, column 55
    function sortValue_73 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=OFFERING_TDIC_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 234, column 54
    function sortValue_74 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.OFFERING_TDIC
    }
    
    // 'value' attribute on DateCell (id=PaidThruDate_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 238, column 54
    function sortValue_75 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return paidThruDate(PolicySummary)
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 244, column 31
    function sortValue_76 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.PolicyType
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 250, column 73
    function sortValue_77 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=FirstInvoice_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 255, column 44
    function sortValue_79 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.FirstInvoicePaidFull_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=UnpaidPolicy_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 260, column 44
    function sortValue_80 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.UnpaidPolicyChangeInvoice_TDIC
    }
    
    // 'value' attribute on TextCell (id=ADANumber_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 264, column 55
    function sortValue_81 (PolicySummary :  entity.PolicySummary) : java.lang.Object {
      return PolicySummary.ADANumber_TDIC
    }
    
    // 'valueRange' attribute on RangeInput (id=CL_CoverageType_TDIC_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 89, column 95
    function valueRange_37 () : java.lang.Object {
      return CL_CoverageType_TDIC.getTypeKeys()
    }
    
    // 'value' attribute on TextInput (id=policyNumber_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 43, column 56
    function valueRoot_4 () : java.lang.Object {
      return policySearchCriteria
    }
    
    // 'value' attribute on RowIterator at FNOLWizardFindPolicyPanelSet.Search.pcf: line 165, column 86
    function value_136 () : gw.api.database.IQueryBeanResult<entity.PolicySummary> {
      FindPolicyUtils.saveSearch(policySearchCriteria, PolicySummaryList); return Wizard.getPolicySummariesOrSelected(PolicySummaryList, FindPolicyUtils.SelectedPolicySummary)
    }
    
    // 'value' attribute on TypeKeyInput (id=PolicyType_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 57, column 45
    function value_16 () : typekey.PolicyType {
      return policySearchCriteria.PolicyType
    }
    
    // 'value' attribute on TextInput (id=policyNumber_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 43, column 56
    function value_2 () : java.lang.String {
      return policySearchCriteria.PolicyNumber
    }
    
    // 'value' attribute on DateInput (id=date_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 64, column 52
    function value_21 () : java.util.Date {
      return policySearchCriteria.LossDate
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimLossType_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 73, column 48
    function value_27 () : typekey.ClaimType_TDIC {
      return policySearchCriteria.ClaimType_TDIC
    }
    
    // 'value' attribute on RangeInput (id=CL_CoverageType_TDIC_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 89, column 95
    function value_34 () : typekey.CL_CoverageType_TDIC {
      return policySearchCriteria.CL_CoverageType_TDIC
    }
    
    // 'value' attribute on DateInput (id=reportdate_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 97, column 60
    function value_45 () : java.util.Date {
      return policySearchCriteria.ReportedDate_TDIC
    }
    
    // 'value' attribute on CheckBoxInput (id=IncludeArchivedPolicies_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 106, column 92
    function value_50 () : java.lang.Boolean {
      return policySearchCriteria.IncludeArchived
    }
    
    // 'value' attribute on TextInput (id=DBAOrganizationName_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 128, column 64
    function value_58 () : java.lang.String {
      return policySearchCriteria.DBAOrganization_TDIC
    }
    
    // 'valueRange' attribute on RangeInput (id=CL_CoverageType_TDIC_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 89, column 95
    function verifyValueRangeIsAllowedType_38 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CL_CoverageType_TDIC_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 89, column 95
    function verifyValueRangeIsAllowedType_38 ($$arg :  typekey.CL_CoverageType_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CL_CoverageType_TDIC_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 89, column 95
    function verifyValueRange_39 () : void {
      var __valueRangeArg = CL_CoverageType_TDIC.getTypeKeys()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_38(__valueRangeArg)
    }
    
    // 'visible' attribute on CheckBoxInput (id=IncludeArchivedPolicies_Input) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 106, column 92
    function visible_49 () : java.lang.Boolean {
      return gw.api.system.CCConfigParameters.PolicySystemArchivingEnabled.Value
    }
    
    // 'visible' attribute on TypeKeyCell (id=Status_Cell) at FNOLWizardFindPolicyPanelSet.Search.pcf: line 250, column 73
    function visible_78 () : java.lang.Boolean {
      return policySearchCriteria.IncludeArchived == true
    }
    
    property get PolicySummaryList () : gw.api.database.IQueryBeanResult<PolicySummary> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<PolicySummary>
    }
    
    property get policySearchCriteria () : entity.PolicySearchCriteria {
      return getCriteriaValue(1) as entity.PolicySearchCriteria
    }
    
    property set policySearchCriteria ($arg :  entity.PolicySearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}