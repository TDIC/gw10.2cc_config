package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses tdic.cc.config.contacts.address.TDIC_ClaimAddressOwner
@javax.annotation.Generated("config/web/pcf/addressbook/shared/CCAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CCAddressInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/addressbook/shared/CCAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CCAddressInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCAddressInputSet.pcf: line 44, column 109
    function available_22 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.CCAddressOwnerFieldId.ADDRESSTYPE)
    }
    
    // 'available' attribute on DateInput (id=Address_ValidUntil_Input) at CCAddressInputSet.pcf: line 61, column 26
    function available_33 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.CCAddressOwnerFieldId.VALIDUNTIL)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCAddressInputSet.pcf: line 35, column 43
    function def_onEnter_15 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCAddressInputSet.pcf: line 35, column 43
    function def_onEnter_17 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCAddressInputSet.pcf: line 35, column 43
    function def_onEnter_19 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCAddressInputSet.pcf: line 35, column 43
    function def_refreshVariables_16 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCAddressInputSet.pcf: line 35, column 43
    function def_refreshVariables_18 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCAddressInputSet.pcf: line 35, column 43
    function def_refreshVariables_20 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'value' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCAddressInputSet.pcf: line 44, column 109
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.Address.AddressType = (__VALUE_TO_SET as typekey.AddressType)
    }
    
    // 'value' attribute on DateInput (id=Address_ValidUntil_Input) at CCAddressInputSet.pcf: line 61, column 26
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.Address.ValidUntil = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=Claim_LocationCode_Input) at CCAddressInputSet.pcf: line 67, column 141
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.Claim.LossLocationCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.Claim.LocationCode = (__VALUE_TO_SET as entity.PolicyLocation)
    }
    
    // 'value' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCAddressInputSet.pcf: line 76, column 25
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'editable' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCAddressInputSet.pcf: line 44, column 109
    function editable_23 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.CCAddressOwnerFieldId.ADDRESSTYPE)
    }
    
    // 'editable' attribute on DateInput (id=Address_ValidUntil_Input) at CCAddressInputSet.pcf: line 61, column 26
    function editable_34 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.CCAddressOwnerFieldId.VALIDUNTIL)
    }
    
    // 'editable' attribute on TextInput (id=Claim_LocationCode_Input) at CCAddressInputSet.pcf: line 67, column 141
    function editable_42 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.CCAddressOwnerFieldId.LOCATIONCODE)
    }
    
    // 'editable' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCAddressInputSet.pcf: line 76, column 25
    function editable_50 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.CCAddressOwnerFieldId.JURISDICTIONSTATE)
    }
    
    // 'filter' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCAddressInputSet.pcf: line 76, column 25
    function filter_54 (VALUE :  typekey.Jurisdiction, VALUES :  typekey.Jurisdiction[]) : java.lang.Boolean {
      return VALUE.hasCategory(Country.TC_US)
    }
    
    // 'label' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function label_2 () : java.lang.Object {
      return addressOwner.AddressNameLabel != null ? addressOwner.AddressNameLabel : DisplayKey.get("Web.Address.Default.Name")
    }
    
    // 'mode' attribute on InputSetRef (id=globalAddress) at CCAddressInputSet.pcf: line 35, column 43
    function mode_21 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PCFMode
    }
    
    // 'newValue' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function newValue_7 () : java.lang.Object {
      return addressOwner.getOrCreateNewAddress()
    }
    
    // 'onChange' attribute on PostOnChange at CCAddressInputSet.pcf: line 29, column 32
    function onChange_0 () : void {
      addressOwner.Address = addressOwner.Claim.LocationCode.Address
    }
    
    // 'optionLabel' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function optionLabel_8 (VALUE :  entity.PolicyLocation) : java.lang.String {
      return gw.util.RangeInputUtil.formatLabel(addressOwner.getOrCreateNewAddress(), VALUE)
    }
    
    // 'required' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCAddressInputSet.pcf: line 44, column 109
    function required_25 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.CCAddressOwnerFieldId.ADDRESSTYPE)
    }
    
    // 'required' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function required_3 () : java.lang.Boolean {
      return addressOwner.RequiredFields.contains(gw.api.address.CCAddressOwnerFieldId.ADDRESS_NAME)
    }
    
    // 'required' attribute on DateInput (id=Address_ValidUntil_Input) at CCAddressInputSet.pcf: line 61, column 26
    function required_35 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.CCAddressOwnerFieldId.VALIDUNTIL)
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCAddressInputSet.pcf: line 76, column 25
    function valueRange_55 () : java.lang.Object {
      return addressOwner.Jurisdictions
    }
    
    // 'valueRange' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function valueRange_9 () : java.lang.Object {
      return addressOwner.Claim.Policy.PolicyLocations
    }
    
    // 'value' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCAddressInputSet.pcf: line 44, column 109
    function valueRoot_28 () : java.lang.Object {
      return addressOwner.Address
    }
    
    // 'value' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCAddressInputSet.pcf: line 76, column 25
    function valueRoot_53 () : java.lang.Object {
      return addressOwner
    }
    
    // 'value' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function valueRoot_6 () : java.lang.Object {
      return addressOwner.Claim
    }
    
    // 'value' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCAddressInputSet.pcf: line 44, column 109
    function value_26 () : typekey.AddressType {
      return addressOwner.Address.AddressType
    }
    
    // 'value' attribute on DateInput (id=Address_ValidUntil_Input) at CCAddressInputSet.pcf: line 61, column 26
    function value_36 () : java.util.Date {
      return addressOwner.Address.ValidUntil
    }
    
    // 'value' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function value_4 () : entity.PolicyLocation {
      return addressOwner.Claim.LocationCode
    }
    
    // 'value' attribute on TextInput (id=Claim_LocationCode_Input) at CCAddressInputSet.pcf: line 67, column 141
    function value_44 () : java.lang.String {
      return addressOwner.Claim.LossLocationCode
    }
    
    // 'value' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCAddressInputSet.pcf: line 76, column 25
    function value_51 () : typekey.Jurisdiction {
      return addressOwner.Jurisdiction
    }
    
    // 'valueRange' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function verifyValueRangeIsAllowedType_10 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function verifyValueRangeIsAllowedType_10 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function verifyValueRangeIsAllowedType_10 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCAddressInputSet.pcf: line 76, column 25
    function verifyValueRangeIsAllowedType_56 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCAddressInputSet.pcf: line 76, column 25
    function verifyValueRangeIsAllowedType_56 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function verifyValueRange_11 () : void {
      var __valueRangeArg = addressOwner.Claim.Policy.PolicyLocations
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_10(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCAddressInputSet.pcf: line 76, column 25
    function verifyValueRange_57 () : void {
      var __valueRangeArg = addressOwner.Jurisdictions
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_56(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=Address_Picker_Input) at CCAddressInputSet.pcf: line 26, column 198
    function visible_1 () : java.lang.Boolean {
      return (addressOwner.Claim.isCreatedFromLegacyPolInd_TDIC ? true : ((addressOwner.Claim.Contacts != null and addressOwner.Claim.Contacts.hasMatch(\contact -> !contact.Contact.New))))
    }
    
    // 'visible' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCAddressInputSet.pcf: line 44, column 109
    function visible_24 () : java.lang.Boolean {
      return not addressOwner.HiddenFields.contains(gw.api.address.CCAddressOwnerFieldId.ADDRESSTYPE)
    }
    
    // 'visible' attribute on TextInput (id=Claim_LocationCode_Input) at CCAddressInputSet.pcf: line 67, column 141
    function visible_43 () : java.lang.Boolean {
      return addressOwner.Claim != null and not addressOwner.HiddenFields.contains(gw.api.address.CCAddressOwnerFieldId.LOCATIONCODE)
    }
    
    property get addressOwner () : gw.api.address.CCAddressOwner {
      return getRequireValue("addressOwner", 0) as gw.api.address.CCAddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.CCAddressOwner) {
      setRequireValue("addressOwner", 0, $arg)
    }
    
    
        function IsJurisdictionVisible() : boolean {
          return addressOwner.Claim != null
            and not addressOwner.HiddenFields.contains(gw.api.address.CCAddressOwnerFieldId.JURISDICTIONSTATE)
            and addressOwner.Jurisdictions.HasElements
        }
    
        /**
         * US529, robk 
         */
        property get LocationDescriptionLabel_TDIC() : String {
          if (addressOwner typeis TDIC_ClaimAddressOwner) {
            return DisplayKey.get("TDIC.LossLocation.Description")
          }
          return DisplayKey.get("Web.Address.Default.Description")
        }
    
    
  }
  
  
}