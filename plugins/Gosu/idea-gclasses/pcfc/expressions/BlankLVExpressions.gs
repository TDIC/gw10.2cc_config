package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/documents/BlankLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BlankLVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/documents/BlankLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BlankLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  
}