package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/NewClaimPolicyGeneralPanelSet.Pr.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewClaimPolicyGeneralPanelSet_PrExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/NewClaimPolicyGeneralPanelSet.Pr.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewClaimPolicyGeneralPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_42 () : void {
      AddressBookPickerPopup.push(statictypeof (Policy.insured), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_44 () : void {
      if (Policy.insured != null) { ClaimContactDetailPopup.push(Policy.insured, Policy.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_46 () : void {
      ClaimContactDetailPopup.push(Policy.insured, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_43 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Policy.insured), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_47 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Policy.insured, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_39 (def :  pcf.ClaimNewInsuredContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Policy.insured), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_40 (def :  pcf.ClaimNewInsuredContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Policy.insured), null, Policy.Claim)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 42, column 28
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=CancellationDate_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 48, column 28
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.CancellationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=OrigEffectiveDate_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 54, column 28
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.OrigEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 61, column 28
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.Status = (__VALUE_TO_SET as typekey.PolicyStatus)
    }
    
    // 'value' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.insured = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 106, column 27
    function defaultSetter_70 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 35, column 28
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on DateInput (id=EffectiveDate_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 35, column 28
    function editable_6 () : java.lang.Boolean {
      return !Policy.Verified
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_48 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Policy.insured); var result = eval("Policy.insured = Policy.Claim.resolveContact(Policy.insured) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'value' attribute on Reflect at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 92, column 55
    function reflectionValue_60 (TRIGGER_INDEX :  int, VALUE :  entity.Contact) : java.lang.Object {
      return VALUE.PrimaryAddressDisplayValue
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 42, column 28
    function validationExpression_13 () : java.lang.Object {
      return Policy.EffectiveDate != null and Policy.ExpirationDate != null and Policy.ExpirationDate < Policy.EffectiveDate ? DisplayKey.get("Java.Validation.AdminCatastrophe.Date.ForbidValidReverse") : null
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_52 () : java.lang.Object {
      return Policy.Claim.RelatedContacts
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 22, column 28
    function valueRoot_1 () : java.lang.Object {
      return Policy
    }
    
    // 'value' attribute on TextInput (id=Insured_Address_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 89, column 61
    function valueRoot_63 () : java.lang.Object {
      return Policy.insured
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 99, column 28
    function valueRoot_66 () : java.lang.Object {
      return Policy.PolicyAccount.AccountHolder
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 22, column 28
    function value_0 () : java.lang.String {
      return Policy.PolicyNumber
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 42, column 28
    function value_14 () : java.util.Date {
      return Policy.ExpirationDate
    }
    
    // 'value' attribute on DateInput (id=CancellationDate_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 48, column 28
    function value_21 () : java.util.Date {
      return Policy.CancellationDate
    }
    
    // 'value' attribute on DateInput (id=OrigEffectiveDate_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 54, column 28
    function value_27 () : java.util.Date {
      return Policy.OrigEffectiveDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 29, column 28
    function value_3 () : typekey.PolicyType {
      return Policy.PolicyType
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 61, column 28
    function value_33 () : typekey.PolicyStatus {
      return Policy.Status
    }
    
    // 'value' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_49 () : entity.Contact {
      return Policy.insured
    }
    
    // 'value' attribute on TextInput (id=Insured_Address_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 89, column 61
    function value_62 () : java.lang.String {
      return Policy.insured.PrimaryAddressDisplayValue
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 99, column 28
    function value_65 () : java.lang.String {
      return Policy.PolicyAccount.AccountHolder.DisplayName
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 106, column 27
    function value_69 () : java.lang.String {
      return Policy.AccountNumber
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at NewClaimPolicyGeneralPanelSet.Pr.pcf: line 35, column 28
    function value_7 () : java.util.Date {
      return Policy.EffectiveDate
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_53 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_53 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_53 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_54 () : void {
      var __valueRangeArg = Policy.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_53(__valueRangeArg)
    }
    
    // 'visible' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_38 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_41 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Policy.insured), Policy.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    property get Policy () : Policy {
      return getRequireValue("Policy", 0) as Policy
    }
    
    property set Policy ($arg :  Policy) {
      setRequireValue("Policy", 0, $arg)
    }
    
    
  }
  
  
}