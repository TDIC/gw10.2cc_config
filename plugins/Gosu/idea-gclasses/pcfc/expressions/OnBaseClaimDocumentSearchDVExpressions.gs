package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseClaimDocumentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBaseClaimDocumentSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseClaimDocumentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseClaimDocumentSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on CheckBoxInput (id=ThumbnailDefaultPrefCheckBox_Input) at OnBaseClaimDocumentSearchDV.pcf: line 90, column 57
    function available_46 () : java.lang.Boolean {
      return !onbaseThumbnailUtil.ActiveViewIsDefault
    }
    
    // 'def' attribute on InputSetRef at OnBaseClaimDocumentSearchDV.pcf: line 96, column 41
    function def_onEnter_52 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at OnBaseClaimDocumentSearchDV.pcf: line 96, column 41
    function def_refreshVariables_53 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on RangeInput (id=RelatedTo_Input) at OnBaseClaimDocumentSearchDV.pcf: line 24, column 50
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      DocumentSearchCriteria.RelatedTo = (__VALUE_TO_SET as gw.pl.persistence.core.Bean)
    }
    
    // 'value' attribute on TypeKeyInput (id=DocumentType_Input) at OnBaseClaimDocumentSearchDV.pcf: line 31, column 43
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      DocumentSearchCriteria.Type = (__VALUE_TO_SET as typekey.DocumentType)
    }
    
    // 'value' attribute on TextInput (id=NameOrID_Input) at OnBaseClaimDocumentSearchDV.pcf: line 36, column 50
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      DocumentSearchCriteria.NameOrID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at OnBaseClaimDocumentSearchDV.pcf: line 44, column 67
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      DocumentSearchCriteria.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at OnBaseClaimDocumentSearchDV.pcf: line 53, column 49
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      DocumentSearchCriteria.Status = (__VALUE_TO_SET as typekey.DocumentStatusType)
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at OnBaseClaimDocumentSearchDV.pcf: line 58, column 48
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      DocumentSearchCriteria.Author = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IncludeObsoletes_Input) at OnBaseClaimDocumentSearchDV.pcf: line 63, column 58
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      DocumentSearchCriteria.IncludeObsoletes = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=viewerType_Input) at OnBaseClaimDocumentSearchDV.pcf: line 72, column 51
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      onbaseThumbnailUtil.ActiveViewerType = (__VALUE_TO_SET as OnBaseDocListViewerType_Ext)
    }
    
    // 'value' attribute on BooleanRadioInput (id=hideNoThumbDocsInput_Input) at OnBaseClaimDocumentSearchDV.pcf: line 82, column 61
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      onbaseThumbnailUtil.DisplayNoThumbDocs = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=ThumbnailDefaultPrefCheckBox_Input) at OnBaseClaimDocumentSearchDV.pcf: line 90, column 57
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      onbaseThumbnailUtil.ActiveViewIsDefault = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'optionGroupLabel' attribute on RangeInput (id=RelatedTo_Input) at OnBaseClaimDocumentSearchDV.pcf: line 24, column 50
    function optionGroupLabel_3 (VALUE :  gw.pl.persistence.core.Bean) : java.lang.String {
      return gw.pcf.RelatedToUtil.getOptionGroupLabel(VALUE as KeyableBean)
    }
    
    // 'optionLabel' attribute on RangeInput (id=RelatedTo_Input) at OnBaseClaimDocumentSearchDV.pcf: line 24, column 50
    function optionLabel_4 (VALUE :  gw.pl.persistence.core.Bean) : java.lang.String {
      return gw.pcf.RelatedToUtil.getOptionLabel(VALUE as KeyableBean)
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at OnBaseClaimDocumentSearchDV.pcf: line 24, column 50
    function valueRange_5 () : java.lang.Object {
      return DocumentSearchCriteria.Claim.RelatedToCandidates
    }
    
    // 'value' attribute on RangeInput (id=RelatedTo_Input) at OnBaseClaimDocumentSearchDV.pcf: line 24, column 50
    function valueRoot_2 () : java.lang.Object {
      return DocumentSearchCriteria
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=viewerType_Input) at OnBaseClaimDocumentSearchDV.pcf: line 72, column 51
    function valueRoot_37 () : java.lang.Object {
      return onbaseThumbnailUtil
    }
    
    // 'value' attribute on RangeInput (id=RelatedTo_Input) at OnBaseClaimDocumentSearchDV.pcf: line 24, column 50
    function value_0 () : gw.pl.persistence.core.Bean {
      return DocumentSearchCriteria.RelatedTo
    }
    
    // 'value' attribute on TextInput (id=NameOrID_Input) at OnBaseClaimDocumentSearchDV.pcf: line 36, column 50
    function value_13 () : java.lang.String {
      return DocumentSearchCriteria.NameOrID
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at OnBaseClaimDocumentSearchDV.pcf: line 44, column 67
    function value_18 () : typekey.LanguageType {
      return DocumentSearchCriteria.Language
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at OnBaseClaimDocumentSearchDV.pcf: line 53, column 49
    function value_23 () : typekey.DocumentStatusType {
      return DocumentSearchCriteria.Status
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at OnBaseClaimDocumentSearchDV.pcf: line 58, column 48
    function value_27 () : java.lang.String {
      return DocumentSearchCriteria.Author
    }
    
    // 'value' attribute on BooleanRadioInput (id=IncludeObsoletes_Input) at OnBaseClaimDocumentSearchDV.pcf: line 63, column 58
    function value_31 () : java.lang.Boolean {
      return DocumentSearchCriteria.IncludeObsoletes
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=viewerType_Input) at OnBaseClaimDocumentSearchDV.pcf: line 72, column 51
    function value_35 () : OnBaseDocListViewerType_Ext {
      return onbaseThumbnailUtil.ActiveViewerType
    }
    
    // 'value' attribute on BooleanRadioInput (id=hideNoThumbDocsInput_Input) at OnBaseClaimDocumentSearchDV.pcf: line 82, column 61
    function value_41 () : java.lang.Boolean {
      return onbaseThumbnailUtil.DisplayNoThumbDocs
    }
    
    // 'value' attribute on CheckBoxInput (id=ThumbnailDefaultPrefCheckBox_Input) at OnBaseClaimDocumentSearchDV.pcf: line 90, column 57
    function value_47 () : java.lang.Boolean {
      return onbaseThumbnailUtil.ActiveViewIsDefault
    }
    
    // 'value' attribute on TypeKeyInput (id=DocumentType_Input) at OnBaseClaimDocumentSearchDV.pcf: line 31, column 43
    function value_9 () : typekey.DocumentType {
      return DocumentSearchCriteria.Type
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at OnBaseClaimDocumentSearchDV.pcf: line 24, column 50
    function verifyValueRangeIsAllowedType_6 ($$arg :  gw.pl.persistence.core.Bean[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at OnBaseClaimDocumentSearchDV.pcf: line 24, column 50
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at OnBaseClaimDocumentSearchDV.pcf: line 24, column 50
    function verifyValueRange_7 () : void {
      var __valueRangeArg = DocumentSearchCriteria.Claim.RelatedToCandidates
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    // 'valueType' attribute on TypeKeyRadioInput (id=viewerType_Input) at OnBaseClaimDocumentSearchDV.pcf: line 72, column 51
    function verifyValueType_39 () : void {
      var __valueTypeArg : OnBaseDocListViewerType_Ext
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    // 'visible' attribute on TypeKeyInput (id=Language_Input) at OnBaseClaimDocumentSearchDV.pcf: line 44, column 67
    function visible_17 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on BooleanRadioInput (id=hideNoThumbDocsInput_Input) at OnBaseClaimDocumentSearchDV.pcf: line 82, column 61
    function visible_40 () : java.lang.Boolean {
      return onbaseThumbnailUtil.IsThumbnailViewActive
    }
    
    property get DocumentSearchCriteria () : DocumentSearchCriteria {
      return getRequireValue("DocumentSearchCriteria", 0) as DocumentSearchCriteria
    }
    
    property set DocumentSearchCriteria ($arg :  DocumentSearchCriteria) {
      setRequireValue("DocumentSearchCriteria", 0, $arg)
    }
    
    property get onbaseThumbnailUtil () : acc.onbase.util.ThumbnailUtil {
      return getRequireValue("onbaseThumbnailUtil", 0) as acc.onbase.util.ThumbnailUtil
    }
    
    property set onbaseThumbnailUtil ($arg :  acc.onbase.util.ThumbnailUtil) {
      setRequireValue("onbaseThumbnailUtil", 0, $arg)
    }
    
    
  }
  
  
}