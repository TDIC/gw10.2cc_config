package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseShareDocumentsWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseShareDocumentsWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (claim :  Claim) : int {
      return 0
    }
    
    // 'allowNext' attribute on WizardStep (id=RequestContacts) at ShareBaseShareDocumentsWizard.pcf: line 54, column 195
    function allowNext_11 () : java.lang.Boolean {
      return !share.Contacts.IsEmpty
    }
    
    // 'allowNext' attribute on WizardStep (id=SelectDocs) at ShareBaseShareDocumentsWizard.pcf: line 42, column 189
    function allowNext_4 () : java.lang.Boolean {
      return !share.DocumentLinks.IsEmpty
    }
    
    // 'beforeCancel' attribute on Wizard (id=ShareBaseShareDocumentsWizard) at ShareBaseShareDocumentsWizard.pcf: line 10, column 73
    function beforeCancel_18 () : void {
      folder.deleteFolderIfExists()
    }
    
    // 'beforeCommit' attribute on Wizard (id=ShareBaseShareDocumentsWizard) at ShareBaseShareDocumentsWizard.pcf: line 10, column 73
    function beforeCommit_19 (pickedValue :  java.lang.Object) : void {
      gw.api.system.bundle.BundleCommitCondition.verify(claim); OBShareManager.buildShare(folder,share)
    }
    
    // 'initialValue' attribute on Variable at ShareBaseShareDocumentsWizard.pcf: line 16, column 44
    function initialValue_0 () : gw.api.web.wizard.WizardInfo {
      return new gw.api.web.wizard.WizardInfo(CurrentLocation)
    }
    
    // 'initialValue' attribute on Variable at ShareBaseShareDocumentsWizard.pcf: line 20, column 65
    function initialValue_1 () : acc.onbase.api.application.OutboundSharingManager {
      return new acc.onbase.api.application.OutboundSharingManager()
    }
    
    // 'initialValue' attribute on Variable at ShareBaseShareDocumentsWizard.pcf: line 27, column 29
    function initialValue_2 () : ShareBase_Ext {
      return new ShareBase_Ext()
    }
    
    // 'initialValue' attribute on Variable at ShareBaseShareDocumentsWizard.pcf: line 31, column 35
    function initialValue_3 () : OutboundSharing_Ext {
      return createShare()
    }
    
    // 'onResume' attribute on Wizard (id=ShareBaseShareDocumentsWizard) at ShareBaseShareDocumentsWizard.pcf: line 10, column 73
    function onResume_20 () : void {
      gw.api.system.bundle.BundleCommitCondition.verify(claim)
    }
    
    // 'screen' attribute on WizardStep (id=RequestContacts) at ShareBaseShareDocumentsWizard.pcf: line 54, column 195
    function screen_onEnter_12 (def :  pcf.ShareBaseShareDocumentsContactScreen) : void {
      def.onEnter(claim, share)
    }
    
    // 'screen' attribute on WizardStep (id=ReviewShare) at ShareBaseShareDocumentsWizard.pcf: line 59, column 193
    function screen_onEnter_15 (def :  pcf.ShareBaseShareDocumentsReviewScreen) : void {
      def.onEnter(claim, folder, share)
    }
    
    // 'screen' attribute on WizardStep (id=SelectDocs) at ShareBaseShareDocumentsWizard.pcf: line 42, column 189
    function screen_onEnter_5 (def :  pcf.ShareBaseShareDocumentsSelectDocsScreen) : void {
      def.onEnter(claim, share)
    }
    
    // 'screen' attribute on WizardStep (id=ShareBaseNewFolderDetails) at ShareBaseShareDocumentsWizard.pcf: line 48, column 188
    function screen_onEnter_8 (def :  pcf.ShareBaseNewFolderDetailsScreen) : void {
      def.onEnter(claim, folder)
    }
    
    // 'screen' attribute on WizardStep (id=RequestContacts) at ShareBaseShareDocumentsWizard.pcf: line 54, column 195
    function screen_refreshVariables_13 (def :  pcf.ShareBaseShareDocumentsContactScreen) : void {
      def.refreshVariables(claim, share)
    }
    
    // 'screen' attribute on WizardStep (id=ReviewShare) at ShareBaseShareDocumentsWizard.pcf: line 59, column 193
    function screen_refreshVariables_16 (def :  pcf.ShareBaseShareDocumentsReviewScreen) : void {
      def.refreshVariables(claim, folder, share)
    }
    
    // 'screen' attribute on WizardStep (id=SelectDocs) at ShareBaseShareDocumentsWizard.pcf: line 42, column 189
    function screen_refreshVariables_6 (def :  pcf.ShareBaseShareDocumentsSelectDocsScreen) : void {
      def.refreshVariables(claim, share)
    }
    
    // 'screen' attribute on WizardStep (id=ShareBaseNewFolderDetails) at ShareBaseShareDocumentsWizard.pcf: line 48, column 188
    function screen_refreshVariables_9 (def :  pcf.ShareBaseNewFolderDetailsScreen) : void {
      def.refreshVariables(claim, folder)
    }
    
    // 'tabBar' attribute on Wizard (id=ShareBaseShareDocumentsWizard) at ShareBaseShareDocumentsWizard.pcf: line 10, column 73
    function tabBar_onEnter_21 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=ShareBaseShareDocumentsWizard) at ShareBaseShareDocumentsWizard.pcf: line 10, column 73
    function tabBar_refreshVariables_22 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    // 'title' attribute on WizardStep (id=ShareBaseNewFolderDetails) at ShareBaseShareDocumentsWizard.pcf: line 48, column 188
    function title_10 () : java.lang.String {
      return DisplayKey.get("Accelerator.OnBase.Wizard.NewShareBaseFolderWizard.STR_GW_FolderDetails_withStepCount",  wizard.CurrentStepNumber ,  wizard.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=RequestContacts) at ShareBaseShareDocumentsWizard.pcf: line 54, column 195
    function title_14 () : java.lang.String {
      return DisplayKey.get("Accelerator.OnBase.Wizard.NewShareBaseFolderWizard.Share.STR_GW_SelectContacts_withStepCount",  wizard.CurrentStepNumber ,  wizard.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=ReviewShare) at ShareBaseShareDocumentsWizard.pcf: line 59, column 193
    function title_17 () : java.lang.String {
      return DisplayKey.get("Accelerator.OnBase.Wizard.NewShareBaseFolderWizard.Share.STR_GW_ReviewAndSubmit_withStepCount", wizard.CurrentStepNumber, wizard.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=SelectDocs) at ShareBaseShareDocumentsWizard.pcf: line 42, column 189
    function title_7 () : java.lang.String {
      return DisplayKey.get("Accelerator.OnBase.Wizard.NewShareBaseFolderWizard.Share.STR_GW_SelectDocs_withStepCount", wizard.CurrentStepNumber , wizard.TotalNumberOfSteps)
    }
    
    override property get CurrentLocation () : pcf.ShareBaseShareDocumentsWizard {
      return super.CurrentLocation as pcf.ShareBaseShareDocumentsWizard
    }
    
    property get OBShareManager () : acc.onbase.api.application.OutboundSharingManager {
      return getVariableValue("OBShareManager", 0) as acc.onbase.api.application.OutboundSharingManager
    }
    
    property set OBShareManager ($arg :  acc.onbase.api.application.OutboundSharingManager) {
      setVariableValue("OBShareManager", 0, $arg)
    }
    
    property get claim () : Claim {
      return getVariableValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setVariableValue("claim", 0, $arg)
    }
    
    property get folder () : ShareBase_Ext {
      return getVariableValue("folder", 0) as ShareBase_Ext
    }
    
    property set folder ($arg :  ShareBase_Ext) {
      setVariableValue("folder", 0, $arg)
    }
    
    property get selectedDocuments () : Document[] {
      return getVariableValue("selectedDocuments", 0) as Document[]
    }
    
    property set selectedDocuments ($arg :  Document[]) {
      setVariableValue("selectedDocuments", 0, $arg)
    }
    
    property get share () : OutboundSharing_Ext {
      return getVariableValue("share", 0) as OutboundSharing_Ext
    }
    
    property set share ($arg :  OutboundSharing_Ext) {
      setVariableValue("share", 0, $arg)
    }
    
    property get wizard () : gw.api.web.wizard.WizardInfo {
      return getVariableValue("wizard", 0) as gw.api.web.wizard.WizardInfo
    }
    
    property set wizard ($arg :  gw.api.web.wizard.WizardInfo) {
      setVariableValue("wizard", 0, $arg)
    }
    
    function createShare() : OutboundSharing_Ext {
      var newShare = new OutboundSharing_Ext() {
          :Claim = claim
      }
    
      // if current user has an email, add them to the outbound share
      if( User.util.CurrentUser.Contact.EmailAddress1 != null ){
        newShare.addRecipient(User.util.CurrentUser.Contact)
      }
    
      return newShare
    }
    
    
  }
  
  
}