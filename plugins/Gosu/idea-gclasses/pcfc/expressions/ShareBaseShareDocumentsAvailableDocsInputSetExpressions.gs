package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsAvailableDocsInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseShareDocumentsAvailableDocsInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsAvailableDocsInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseShareDocumentsAvailableDocsInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at ShareBaseShareDocumentsAvailableDocsInputSet.pcf: line 16, column 38
    function def_onEnter_1 (def :  pcf.ShareBaseShareDocumentsAvailableDocsLV) : void {
      def.onEnter(claim,outboundSharing)
    }
    
    // 'def' attribute on ListViewInput at ShareBaseShareDocumentsAvailableDocsInputSet.pcf: line 16, column 38
    function def_refreshVariables_2 (def :  pcf.ShareBaseShareDocumentsAvailableDocsLV) : void {
      def.refreshVariables(claim,outboundSharing)
    }
    
    // 'visible' attribute on ListViewInput at ShareBaseShareDocumentsAvailableDocsInputSet.pcf: line 16, column 38
    function visible_0 () : java.lang.Boolean {
      return perm.System.viewdocs
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get outboundSharing () : OutboundSharing_Ext {
      return getRequireValue("outboundSharing", 0) as OutboundSharing_Ext
    }
    
    property set outboundSharing ($arg :  OutboundSharing_Ext) {
      setRequireValue("outboundSharing", 0, $arg)
    }
    
    
  }
  
  
}