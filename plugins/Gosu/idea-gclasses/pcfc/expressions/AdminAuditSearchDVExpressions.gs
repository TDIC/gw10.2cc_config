package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/audit_ext/AdminAuditSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminAuditSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/audit_ext/AdminAuditSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminAuditSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateInput (id=SearchStartDate_Input) at AdminAuditSearchDV.pcf: line 18, column 43
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.StartDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=SearchModifiedBy_Input) at AdminAuditSearchDV.pcf: line 41, column 29
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ModifiedBy = (__VALUE_TO_SET as String)
    }
    
    // 'value' attribute on DateInput (id=SearchEndDate_Input) at AdminAuditSearchDV.pcf: line 23, column 41
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EndDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=SearchEntityName_Input) at AdminAuditSearchDV.pcf: line 34, column 29
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EntityName = (__VALUE_TO_SET as String)
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchEntityName_Input) at AdminAuditSearchDV.pcf: line 34, column 29
    function valueRange_11 () : java.lang.Object {
      return gw.acc.adminaudit.util.AdminAuditSearchUtil.modifiedEntities()
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchModifiedBy_Input) at AdminAuditSearchDV.pcf: line 41, column 29
    function valueRange_18 () : java.lang.Object {
      return gw.acc.adminaudit.util.AdminAuditSearchUtil.modifiedBy()
    }
    
    // 'value' attribute on DateInput (id=SearchStartDate_Input) at AdminAuditSearchDV.pcf: line 18, column 43
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on DateInput (id=SearchStartDate_Input) at AdminAuditSearchDV.pcf: line 18, column 43
    function value_0 () : java.util.Date {
      return searchCriteria.StartDate
    }
    
    // 'value' attribute on RangeInput (id=SearchModifiedBy_Input) at AdminAuditSearchDV.pcf: line 41, column 29
    function value_15 () : String {
      return searchCriteria.ModifiedBy
    }
    
    // 'value' attribute on DateInput (id=SearchEndDate_Input) at AdminAuditSearchDV.pcf: line 23, column 41
    function value_4 () : java.util.Date {
      return searchCriteria.EndDate
    }
    
    // 'value' attribute on RangeInput (id=SearchEntityName_Input) at AdminAuditSearchDV.pcf: line 34, column 29
    function value_8 () : String {
      return searchCriteria.EntityName
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchEntityName_Input) at AdminAuditSearchDV.pcf: line 34, column 29
    function verifyValueRangeIsAllowedType_12 ($$arg :  String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchEntityName_Input) at AdminAuditSearchDV.pcf: line 34, column 29
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchModifiedBy_Input) at AdminAuditSearchDV.pcf: line 41, column 29
    function verifyValueRangeIsAllowedType_19 ($$arg :  String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchModifiedBy_Input) at AdminAuditSearchDV.pcf: line 41, column 29
    function verifyValueRangeIsAllowedType_19 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchEntityName_Input) at AdminAuditSearchDV.pcf: line 34, column 29
    function verifyValueRange_13 () : void {
      var __valueRangeArg = gw.acc.adminaudit.util.AdminAuditSearchUtil.modifiedEntities()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchModifiedBy_Input) at AdminAuditSearchDV.pcf: line 41, column 29
    function verifyValueRange_20 () : void {
      var __valueRangeArg = gw.acc.adminaudit.util.AdminAuditSearchUtil.modifiedBy()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_19(__valueRangeArg)
    }
    
    property get searchCriteria () : gw.acc.adminaudit.util.AdminAuditSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.acc.adminaudit.util.AdminAuditSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.acc.adminaudit.util.AdminAuditSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}