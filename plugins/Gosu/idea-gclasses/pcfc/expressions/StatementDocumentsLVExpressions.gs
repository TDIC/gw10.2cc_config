package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/servicerequests/StatementDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class StatementDocumentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/servicerequests/StatementDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends StatementDocumentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at StatementDocumentsLV.pcf: line 57, column 145
    function action_14 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at StatementDocumentsLV.pcf: line 69, column 55
    function action_19 () : void {
      DocumentDetailsPopup.push(document, not currentStatement.ServiceRequest.AlreadyPromoted)
    }
    
    // 'action' attribute on Link (id=DownloadLink) at StatementDocumentsLV.pcf: line 78, column 181
    function action_24 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=UploadLink) at StatementDocumentsLV.pcf: line 86, column 179
    function action_29 () : void {
      UploadDocumentContentPopup.push(document)
    }
    
    // 'action' attribute on Link (id=Unlink) at StatementDocumentsLV.pcf: line 94, column 43
    function action_34 () : void {
      currentStatement.unlinkDocumentForUI(document, currentStatement.New ? false : true) 
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at StatementDocumentsLV.pcf: line 48, column 147
    function action_9 () : void {
      document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at StatementDocumentsLV.pcf: line 69, column 55
    function action_dest_20 () : pcf.api.Destination {
      return pcf.DocumentDetailsPopup.createDestination(document, not currentStatement.ServiceRequest.AlreadyPromoted)
    }
    
    // 'action' attribute on Link (id=UploadLink) at StatementDocumentsLV.pcf: line 86, column 179
    function action_dest_30 () : pcf.api.Destination {
      return pcf.UploadDocumentContentPopup.createDestination(document)
    }
    
    // 'available' attribute on Link (id=ViewPropertiesLink) at StatementDocumentsLV.pcf: line 69, column 55
    function available_17 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'available' attribute on Link (id=DownloadLink) at StatementDocumentsLV.pcf: line 78, column 181
    function available_22 () : java.lang.Boolean {
      return documentsActionsHelper.isDownloadDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=UploadLink) at StatementDocumentsLV.pcf: line 86, column 179
    function available_27 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at StatementDocumentsLV.pcf: line 48, column 147
    function available_7 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document) and not CurrentLocation.InEditMode
    }
    
    // 'icon' attribute on Link (id=DownloadLink) at StatementDocumentsLV.pcf: line 78, column 181
    function icon_26 () : java.lang.String {
      return "document_download" 
    }
    
    // 'icon' attribute on Link (id=Unlink) at StatementDocumentsLV.pcf: line 94, column 43
    function icon_36 () : java.lang.String {
      return "document_remove"  
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at StatementDocumentsLV.pcf: line 35, column 32
    function icon_6 () : java.lang.String {
      return document.Icon
    }
    
    // 'initialValue' attribute on Variable at StatementDocumentsLV.pcf: line 27, column 26
    function initialValue_5 () : Document {
      return statementDocumentLinkPair.Second
    }
    
    // RowIterator (id=ActiveStatementDocuments) at StatementDocumentsLV.pcf: line 23, column 121
    function initializeVariables_51 () : void {
        document = statementDocumentLinkPair.Second;

    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at StatementDocumentsLV.pcf: line 48, column 147
    function label_10 () : java.lang.Object {
      return document.Name
    }
    
    // 'label' attribute on Link (id=DocumentsLV_ActionsDisabled) at StatementDocumentsLV.pcf: line 99, column 75
    function label_38 () : java.lang.Object {
      return documentsActionsHelper.AsynchronousActionsMessage
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at StatementDocumentsLV.pcf: line 48, column 147
    function tooltip_11 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(document)
    }
    
    // 'tooltip' attribute on Link (id=ViewPropertiesLink) at StatementDocumentsLV.pcf: line 69, column 55
    function tooltip_21 () : java.lang.String {
      return documentsActionsHelper.ViewDocumentPropertiesTooltip
    }
    
    // 'tooltip' attribute on Link (id=DownloadLink) at StatementDocumentsLV.pcf: line 78, column 181
    function tooltip_25 () : java.lang.String {
      return documentsActionsHelper.DownloadDocumentContentTooltip
    }
    
    // 'tooltip' attribute on Link (id=UploadLink) at StatementDocumentsLV.pcf: line 86, column 179
    function tooltip_31 () : java.lang.String {
      return documentsActionsHelper.UploadDocumentContentTooltip
    }
    
    // 'tooltip' attribute on Link (id=Unlink) at StatementDocumentsLV.pcf: line 94, column 43
    function tooltip_35 () : java.lang.String {
      return documentsActionsHelper.RemoveDocumentLinkTooltip()
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at StatementDocumentsLV.pcf: line 105, column 45
    function valueRoot_40 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at StatementDocumentsLV.pcf: line 105, column 45
    function value_39 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at StatementDocumentsLV.pcf: line 110, column 58
    function value_42 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at StatementDocumentsLV.pcf: line 114, column 36
    function value_45 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at StatementDocumentsLV.pcf: line 122, column 42
    function value_48 () : java.util.Date {
      return document.DateModified
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at StatementDocumentsLV.pcf: line 57, column 145
    function visible_13 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on Link (id=ViewPropertiesLink) at StatementDocumentsLV.pcf: line 69, column 55
    function visible_18 () : java.lang.Boolean {
      return not CurrentLocation.InEditMode
    }
    
    // 'visible' attribute on Link (id=DownloadLink) at StatementDocumentsLV.pcf: line 78, column 181
    function visible_23 () : java.lang.Boolean {
      return documentsActionsHelper.isDownloadDocumentContentVisible(document) and not CurrentLocation.InEditMode and not currentStatement.ServiceRequest.AlreadyPromoted
    }
    
    // 'visible' attribute on Link (id=UploadLink) at StatementDocumentsLV.pcf: line 86, column 179
    function visible_28 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentVisible(document)and not CurrentLocation.InEditMode and not currentStatement.ServiceRequest.AlreadyPromoted 
    }
    
    // 'visible' attribute on Link (id=Unlink) at StatementDocumentsLV.pcf: line 94, column 43
    function visible_33 () : java.lang.Boolean {
      return documentsRemovable
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_ActionsDisabled) at StatementDocumentsLV.pcf: line 99, column 75
    function visible_37 () : java.lang.Boolean {
      return documentsActionsHelper.isDocumentPending(document)
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at StatementDocumentsLV.pcf: line 48, column 147
    function visible_8 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    property get document () : Document {
      return getVariableValue("document", 1) as Document
    }
    
    property set document ($arg :  Document) {
      setVariableValue("document", 1, $arg)
    }
    
    property get statementDocumentLinkPair () : gw.util.Pair<entity.ServiceRequestStatementDocumentLink, entity.Document> {
      return getIteratedValue(1) as gw.util.Pair<entity.ServiceRequestStatementDocumentLink, entity.Document>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/servicerequests/StatementDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class StatementDocumentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at StatementDocumentsLV.pcf: line 17, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at StatementDocumentsLV.pcf: line 105, column 45
    function sortValue_1 (statementDocumentLinkPair :  gw.util.Pair<entity.ServiceRequestStatementDocumentLink, entity.Document>) : java.lang.Object {
      var document : Document = (statementDocumentLinkPair.Second)
return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at StatementDocumentsLV.pcf: line 110, column 58
    function sortValue_2 (statementDocumentLinkPair :  gw.util.Pair<entity.ServiceRequestStatementDocumentLink, entity.Document>) : java.lang.Object {
      var document : Document = (statementDocumentLinkPair.Second)
return document.Subtype
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at StatementDocumentsLV.pcf: line 114, column 36
    function sortValue_3 (statementDocumentLinkPair :  gw.util.Pair<entity.ServiceRequestStatementDocumentLink, entity.Document>) : java.lang.Object {
      var document : Document = (statementDocumentLinkPair.Second)
return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at StatementDocumentsLV.pcf: line 122, column 42
    function sortValue_4 (statementDocumentLinkPair :  gw.util.Pair<entity.ServiceRequestStatementDocumentLink, entity.Document>) : java.lang.Object {
      var document : Document = (statementDocumentLinkPair.Second)
return document.DateModified
    }
    
    // 'value' attribute on RowIterator (id=ActiveStatementDocuments) at StatementDocumentsLV.pcf: line 23, column 121
    function value_52 () : java.util.List<gw.util.Pair<entity.ServiceRequestStatementDocumentLink, entity.Document>> {
      return currentStatement.ViewableDocumentLinksAndDocuments
    }
    
    property get currentStatement () : ServiceRequestStatement {
      return getRequireValue("currentStatement", 0) as ServiceRequestStatement
    }
    
    property set currentStatement ($arg :  ServiceRequestStatement) {
      setRequireValue("currentStatement", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get documentsRemovable () : boolean {
      return getRequireValue("documentsRemovable", 0) as java.lang.Boolean
    }
    
    property set documentsRemovable ($arg :  boolean) {
      setRequireValue("documentsRemovable", 0, $arg)
    }
    
    
  }
  
  
}