package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBaseDocumentInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends OnBaseDocumentLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at OnBaseDocumentInputSet.pcf: line 80, column 151
    function action_19 () : void {
      Document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at OnBaseDocumentInputSet.pcf: line 89, column 149
    function action_24 () : void {
      Document.downloadContent()
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at OnBaseDocumentInputSet.pcf: line 80, column 151
    function available_17 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(Document)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at OnBaseDocumentInputSet.pcf: line 59, column 36
    function icon_13 () : java.lang.String {
      return (Document as Document).Icon
    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at OnBaseDocumentInputSet.pcf: line 80, column 151
    function label_20 () : java.lang.Object {
      return Document.Name
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at OnBaseDocumentInputSet.pcf: line 80, column 151
    function tooltip_21 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(Document as Document)
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBaseDocumentInputSet.pcf: line 64, column 40
    function valueRoot_15 () : java.lang.Object {
      return Document
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBaseDocumentInputSet.pcf: line 64, column 40
    function value_14 () : java.lang.String {
      return Document.DocUID
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at OnBaseDocumentInputSet.pcf: line 96, column 49
    function value_27 () : typekey.DocumentType {
      return Document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=Subtype_Cell) at OnBaseDocumentInputSet.pcf: line 102, column 54
    function value_30 () : OnBaseDocumentSubtype_Ext {
      return Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at OnBaseDocumentInputSet.pcf: line 107, column 55
    function value_34 () : typekey.DocumentStatusType {
      return Document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at OnBaseDocumentInputSet.pcf: line 112, column 40
    function value_37 () : java.lang.String {
      return Document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at OnBaseDocumentInputSet.pcf: line 120, column 46
    function value_40 () : java.util.Date {
      return Document.DateModified
    }
    
    // 'valueType' attribute on TypeKeyCell (id=Subtype_Cell) at OnBaseDocumentInputSet.pcf: line 102, column 54
    function verifyValueType_33 () : void {
      var __valueTypeArg : OnBaseDocumentSubtype_Ext
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at OnBaseDocumentInputSet.pcf: line 80, column 151
    function visible_18 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at OnBaseDocumentInputSet.pcf: line 89, column 149
    function visible_23 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    property get Document () : Document {
      return getIteratedValue(2) as Document
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseDocumentInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=OnBaseDocumentListPopup_LinkDocumentButton) at OnBaseDocumentInputSet.pcf: line 33, column 106
    function action_2 () : void {
      OnBasePickExistingDocumentPopup.push(LinkedEntity, LinkType, Beans)
    }
    
    // 'action' attribute on ToolbarButton (id=OnBaseDocumentListPopup_LinkDocumentButton) at OnBaseDocumentInputSet.pcf: line 33, column 106
    function action_dest_3 () : pcf.api.Destination {
      return pcf.OnBasePickExistingDocumentPopup.createDestination(LinkedEntity, LinkType, Beans)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=OnBaseDocumentListPopup_UnlinkDocumentButton) at OnBaseDocumentInputSet.pcf: line 38, column 108
    function allCheckedRowsAction_4 (CheckedValues :  Document[], CheckedValuesErrors :  java.util.Map) : void {
      DocumentLinking.unlinkDocumentsFromEntity(LinkedEntity, CheckedValues , LinkType);
    }
    
    // 'initialValue' attribute on Variable at OnBaseDocumentInputSet.pcf: line 19, column 58
    function initialValue_0 () : acc.onbase.api.application.DocumentLinking {
      if(LinkedEntity.isNew()){return new acc.onbase.api.application.DocumentLinkingNewEntity()} else{ return new acc.onbase.api.application.DocumentLinking()}
    }
    
    // 'initialValue' attribute on Variable at OnBaseDocumentInputSet.pcf: line 24, column 43
    function initialValue_1 () : List<entity.Document> {
      return DocumentLinking.getDocumentsLinkedToEntity(LinkedEntity, LinkType)
    }
    
    property get Beans () : KeyableBean[] {
      return getRequireValue("Beans", 0) as KeyableBean[]
    }
    
    property set Beans ($arg :  KeyableBean[]) {
      setRequireValue("Beans", 0, $arg)
    }
    
    property get DocumentLinking () : acc.onbase.api.application.DocumentLinking {
      return getVariableValue("DocumentLinking", 0) as acc.onbase.api.application.DocumentLinking
    }
    
    property set DocumentLinking ($arg :  acc.onbase.api.application.DocumentLinking) {
      setVariableValue("DocumentLinking", 0, $arg)
    }
    
    property get LinkType () : acc.onbase.configuration.DocumentLinkType {
      return getRequireValue("LinkType", 0) as acc.onbase.configuration.DocumentLinkType
    }
    
    property set LinkType ($arg :  acc.onbase.configuration.DocumentLinkType) {
      setRequireValue("LinkType", 0, $arg)
    }
    
    property get LinkedDocuments () : List<entity.Document> {
      return getVariableValue("LinkedDocuments", 0) as List<entity.Document>
    }
    
    property set LinkedDocuments ($arg :  List<entity.Document>) {
      setVariableValue("LinkedDocuments", 0, $arg)
    }
    
    property get LinkedEntity () : KeyableBean {
      return getRequireValue("LinkedEntity", 0) as KeyableBean
    }
    
    property set LinkedEntity ($arg :  KeyableBean) {
      setRequireValue("LinkedEntity", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseDocumentLVExpressionsImpl extends OnBaseDocumentInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at OnBaseDocumentInputSet.pcf: line 45, column 56
    function initialValue_5 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at OnBaseDocumentInputSet.pcf: line 107, column 55
    function sortValue_10 (Document :  Document) : java.lang.Object {
      return Document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at OnBaseDocumentInputSet.pcf: line 112, column 40
    function sortValue_11 (Document :  Document) : java.lang.Object {
      return Document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at OnBaseDocumentInputSet.pcf: line 120, column 46
    function sortValue_12 (Document :  Document) : java.lang.Object {
      return Document.DateModified
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBaseDocumentInputSet.pcf: line 64, column 40
    function sortValue_6 (Document :  Document) : java.lang.Object {
      return Document.DocUID
    }
    
    // 'sortBy' attribute on LinkCell (id=Name) at OnBaseDocumentInputSet.pcf: line 71, column 27
    function sortValue_7 (Document :  Document) : java.lang.Object {
      return (Document as Document).Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at OnBaseDocumentInputSet.pcf: line 96, column 49
    function sortValue_8 (Document :  Document) : java.lang.Object {
      return Document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=Subtype_Cell) at OnBaseDocumentInputSet.pcf: line 102, column 54
    function sortValue_9 (Document :  Document) : java.lang.Object {
      return Document.Subtype
    }
    
    // 'value' attribute on RowIterator at OnBaseDocumentInputSet.pcf: line 52, column 51
    function value_43 () : List<entity.Document> {
      return LinkedDocuments
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentInputSet.pcf: line 52, column 51
    function verifyValueTypeIsAllowedType_44 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentInputSet.pcf: line 52, column 51
    function verifyValueTypeIsAllowedType_44 ($$arg :  gw.api.iterator.IteratorBackingData) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentInputSet.pcf: line 52, column 51
    function verifyValueTypeIsAllowedType_44 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentInputSet.pcf: line 52, column 51
    function verifyValueType_45 () : void {
      var __valueTypeArg : List<entity.Document>
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the valueType is not a valid type for use with an iterator
      // The valueType for an iterator must be an array or extend from List or IQueryBeanResult
      verifyValueTypeIsAllowedType_44(__valueTypeArg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 1) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 1, $arg)
    }
    
    
  }
  
  
}