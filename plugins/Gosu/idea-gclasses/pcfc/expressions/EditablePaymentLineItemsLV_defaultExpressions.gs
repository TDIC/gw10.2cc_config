package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.system.CCLoggerCategory
uses java.math.BigDecimal
@javax.annotation.Generated("config/web/pcf/claim/newtransaction/shared/EditablePaymentLineItemsLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditablePaymentLineItemsLV_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/newtransaction/shared/EditablePaymentLineItemsLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditablePaymentLineItemsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'autoAdd' attribute on RowIterator at EditablePaymentLineItemsLV.default.pcf: line 22, column 48
    function autoAdd_4 () : java.lang.Boolean {
      return Transaction.LineItems.length == 0
    }
    
    // 'value' attribute on RangeCell (id=LineCategory_Cell) at EditablePaymentLineItemsLV.default.pcf: line 32, column 45
    function sortValue_0 (TransactionLineItem :  entity.TransactionLineItem) : java.lang.Object {
      return TransactionLineItem.LineCategory
    }
    
    // 'value' attribute on CurrencyCell (id=Amount_Cell) at EditablePaymentLineItemsLV.default.pcf: line 41, column 54
    function sortValue_1 (TransactionLineItem :  entity.TransactionLineItem) : java.lang.Object {
      return TransactionLineItem.TransactionAmountReservingAmountPair
    }
    
    // '$$sumValue' attribute on RowIterator at EditablePaymentLineItemsLV.default.pcf: line 41, column 54
    function sumValueRoot_3 (TransactionLineItem :  entity.TransactionLineItem) : java.lang.Object {
      return TransactionLineItem
    }
    
    // 'footerSumValue' attribute on RowIterator at EditablePaymentLineItemsLV.default.pcf: line 41, column 54
    function sumValue_2 (TransactionLineItem :  entity.TransactionLineItem) : java.lang.Object {
      return TransactionLineItem.TransactionAmountReservingAmountPair
    }
    
    // 'toAdd' attribute on RowIterator at EditablePaymentLineItemsLV.default.pcf: line 22, column 48
    function toAdd_16 (TransactionLineItem :  entity.TransactionLineItem) : void {
      Transaction.addToLineItems(TransactionLineItem)
    }
    
    // 'toRemove' attribute on RowIterator at EditablePaymentLineItemsLV.default.pcf: line 22, column 48
    function toRemove_17 (TransactionLineItem :  entity.TransactionLineItem) : void {
      remove(TransactionLineItem)
    }
    
    // 'value' attribute on RowIterator at EditablePaymentLineItemsLV.default.pcf: line 22, column 48
    function value_18 () : entity.TransactionLineItem[] {
      return Transaction.LineItems
    }
    
    property get Transaction () : Transaction {
      return getRequireValue("Transaction", 0) as Transaction
    }
    
    property set Transaction ($arg :  Transaction) {
      setRequireValue("Transaction", 0, $arg)
    }
    
    
    
    function remove(tli : TransactionLineItem) {
      if (tli.LineCategory == TC_DEDUCTIBLE) {
        (Transaction as Payment).removeDeductibleLineItem(tli)
      } else {
        Transaction.removeFromLineItemsIfEditable(tli)
      }
    }
    
    function fetchLineCategory(tLI:entity.TransactionLineItem):List {
      try {
        if (tLI.Transaction.Claim.Type_TDIC.Code.equalsIgnoreCase(ClaimType_TDIC.TC_CYBERLIABILITY.Code)) {
          if (tLI.Transaction.Exposure.ExposureType == null and
              (tLI.Transaction.CostType.Code.equalsIgnoreCase(CostType.TC_DCCEXPENSE.Code) or
                  tLI.Transaction.CostType.Code.equalsIgnoreCase(CostType.TC_EXPENSES_TDIC.Code) or
                  tLI.Transaction.CostType.Code.equalsIgnoreCase(CostType.TC_AOEXPENSE.Code)) and
              !(tLI.Transaction.CostCategory.Code.equalsIgnoreCase(CostCategory.TC_UNSPECIFIED.Code))) {
            return (LineCategory.Type as gw.entity.ITypeList).getTypeKeysByCategories({tLI.Transaction.Claim.Type_TDIC, tLI.
                Transaction.CostType}).removeMatches(\elt -> elt.Code == "CyberPhotocopy_TDIC").cast(LineCategory)
          } else if (tLI.Transaction.Exposure.ExposureType != null) {
            return (LineCategory.Type as gw.entity.ITypeList).getTypeKeysByCategories({tLI.Transaction.Claim.Type_TDIC, tLI.Transaction.CostType}).cast(LineCategory)
          } else if (tLI.Transaction.Exposure.ExposureType == null and tLI.Transaction.CostType.Code == null) {
            return (LineCategory.Type as gw.entity.ITypeList).getTypeKeysByCategories({tLI.Transaction.Claim.Type_TDIC, tLI.Transaction.CostType}).cast(LineCategory)
          }
        }
        return (LineCategory.Type as gw.entity.ITypeList).getTypeKeysByCategories({tLI.Transaction.Claim.Type_TDIC, tLI.Transaction.CostType}).cast(LineCategory)
      }catch (e:Exception){
        CCLoggerCategory.APPLICATION.info("Exception in FetchingLineCategory: " + e + "\n")
      }
      return (LineCategory.Type as gw.entity.ITypeList).getTypeKeysByCategories({tLI.Transaction.Claim.Type_TDIC, tLI.Transaction.CostType}).cast(LineCategory)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/newtransaction/shared/EditablePaymentLineItemsLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends EditablePaymentLineItemsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeCell (id=LineCategory_Cell) at EditablePaymentLineItemsLV.default.pcf: line 32, column 45
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      TransactionLineItem.LineCategory = (__VALUE_TO_SET as typekey.LineCategory)
    }
    
    // 'editable' attribute on Row at EditablePaymentLineItemsLV.default.pcf: line 24, column 156
    function editable_15 () : java.lang.Boolean {
      return TransactionLineItem.LineCategory != LineCategory.TC_DEDUCTIBLE and TransactionLineItem.LineCategory != LineCategory.TC_FORMERDEDUCTIBLE
    }
    
    // 'valueRange' attribute on RangeCell (id=LineCategory_Cell) at EditablePaymentLineItemsLV.default.pcf: line 32, column 45
    function valueRange_8 () : java.lang.Object {
      return fetchLineCategory(TransactionLineItem)
    }
    
    // 'value' attribute on RangeCell (id=LineCategory_Cell) at EditablePaymentLineItemsLV.default.pcf: line 32, column 45
    function valueRoot_7 () : java.lang.Object {
      return TransactionLineItem
    }
    
    // 'value' attribute on CurrencyCell (id=Amount_Cell) at EditablePaymentLineItemsLV.default.pcf: line 41, column 54
    function value_12 () : gw.api.financials.IPairedMoney {
      return TransactionLineItem.TransactionAmountReservingAmountPair
    }
    
    // 'value' attribute on RangeCell (id=LineCategory_Cell) at EditablePaymentLineItemsLV.default.pcf: line 32, column 45
    function value_5 () : typekey.LineCategory {
      return TransactionLineItem.LineCategory
    }
    
    // 'valueRange' attribute on RangeCell (id=LineCategory_Cell) at EditablePaymentLineItemsLV.default.pcf: line 32, column 45
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=LineCategory_Cell) at EditablePaymentLineItemsLV.default.pcf: line 32, column 45
    function verifyValueRangeIsAllowedType_9 ($$arg :  typekey.LineCategory[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=LineCategory_Cell) at EditablePaymentLineItemsLV.default.pcf: line 32, column 45
    function verifyValueRange_10 () : void {
      var __valueRangeArg = fetchLineCategory(TransactionLineItem)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_9(__valueRangeArg)
    }
    
    property get TransactionLineItem () : entity.TransactionLineItem {
      return getIteratedValue(1) as entity.TransactionLineItem
    }
    
    
  }
  
  
}