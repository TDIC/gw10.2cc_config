package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/exposures/TowOnlyDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TowOnlyDVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/exposures/TowOnlyDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TowOnlyDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=TowOnlyDV_NewIncidentMenuItem) at TowOnlyDV.pcf: line 172, column 98
    function action_106 () : void {
      NewVehicleIncidentPopup.push(Exposure.Claim)
    }
    
    // 'action' attribute on MenuItem (id=TowOnlyDV_EditIncidentMenuItem) at TowOnlyDV.pcf: line 177, column 99
    function action_108 () : void {
      EditVehicleIncidentPopup.push(Exposure.VehicleIncident, true)
    }
    
    // 'action' attribute on MenuItem (id=TowOnlyDV_ViewIncidentMenuItem) at TowOnlyDV.pcf: line 182, column 99
    function action_110 () : void {
      EditVehicleIncidentPopup.push(Exposure.VehicleIncident, false)
    }
    
    // 'action' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_161 () : void {
      AddressBookPickerPopup.push(statictypeof (asVehicleIncident().recoveryagent), Exposure.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_163 () : void {
      if (asVehicleIncident().recoveryagent != null) { ClaimContactDetailPopup.push(asVehicleIncident().recoveryagent, Exposure.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_165 () : void {
      ClaimContactDetailPopup.push(asVehicleIncident().recoveryagent, Exposure.Claim)
    }
    
    // 'action' attribute on MenuItem (id=Exposure_StatLine_PickerButton) at PolicyStatCodePickerWidget.xml: line 5, column 25
    function action_33 () : void {
      ClaimPolicyStatCodePickerPopup.push(Exposure.Claim)
    }
    
    // 'action' attribute on PolicyStatCodePickerInput (id=Exposure_StatLine_Input) at PolicyStatCodePickerWidget.xml: line 5, column 25
    function action_35 () : void {
      ClaimPolicyStatCodePickerPopup.push(Exposure.Claim)
    }
    
    // 'action' attribute on PolicyStatCodePickerInput (id=Exposure_StatLine_Input) at TowOnlyDV.pcf: line 68, column 63
    function action_43 () : void {
      ClaimPolicyStatCodePickerPopup.push(Exposure.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_70 () : void {
      AddressBookPickerPopup.push(statictypeof (Exposure.Claimant), Exposure.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_72 () : void {
      if (Exposure.Claimant != null) { ClaimContactDetailPopup.push(Exposure.Claimant, Exposure.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_75 () : void {
      ClaimContactDetailPopup.push(Exposure.Claimant, Exposure.Claim)
    }
    
    // 'action' attribute on MenuItem (id=TowOnlyDV_NewIncidentMenuItem) at TowOnlyDV.pcf: line 172, column 98
    function action_dest_107 () : pcf.api.Destination {
      return pcf.NewVehicleIncidentPopup.createDestination(Exposure.Claim)
    }
    
    // 'action' attribute on MenuItem (id=TowOnlyDV_EditIncidentMenuItem) at TowOnlyDV.pcf: line 177, column 99
    function action_dest_109 () : pcf.api.Destination {
      return pcf.EditVehicleIncidentPopup.createDestination(Exposure.VehicleIncident, true)
    }
    
    // 'action' attribute on MenuItem (id=TowOnlyDV_ViewIncidentMenuItem) at TowOnlyDV.pcf: line 182, column 99
    function action_dest_111 () : pcf.api.Destination {
      return pcf.EditVehicleIncidentPopup.createDestination(Exposure.VehicleIncident, false)
    }
    
    // 'action' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_162 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (asVehicleIncident().recoveryagent), Exposure.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_166 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(asVehicleIncident().recoveryagent, Exposure.Claim)
    }
    
    // 'action' attribute on MenuItem (id=Exposure_StatLine_PickerButton) at PolicyStatCodePickerWidget.xml: line 5, column 25
    function action_dest_34 () : pcf.api.Destination {
      return pcf.ClaimPolicyStatCodePickerPopup.createDestination(Exposure.Claim)
    }
    
    // 'action' attribute on PolicyStatCodePickerInput (id=Exposure_StatLine_Input) at PolicyStatCodePickerWidget.xml: line 5, column 25
    function action_dest_36 () : pcf.api.Destination {
      return pcf.ClaimPolicyStatCodePickerPopup.createDestination(Exposure.Claim)
    }
    
    // 'action' attribute on PolicyStatCodePickerInput (id=Exposure_StatLine_Input) at TowOnlyDV.pcf: line 68, column 63
    function action_dest_44 () : pcf.api.Destination {
      return pcf.ClaimPolicyStatCodePickerPopup.createDestination(Exposure.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_71 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Exposure.Claimant), Exposure.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_76 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Exposure.Claimant, Exposure.Claim)
    }
    
    // 'def' attribute on InputSetRef at TowOnlyDV.pcf: line 218, column 51
    function def_onEnter_139 (def :  pcf.ExposureWorkloadInputSet) : void {
      def.onEnter(Exposure)
    }
    
    // 'def' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_158 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (asVehicleIncident().recoveryagent), null, Exposure.Claim)
    }
    
    // 'def' attribute on InputSetRef (id=RecoveryAddress) at TowOnlyDV.pcf: line 264, column 167
    function def_onEnter_186 (def :  pcf.CCAddressInputSet) : void {
      def.onEnter(asVehicleIncident().RecoveryAddressOwner)
    }
    
    // 'def' attribute on ListViewInput at TowOnlyDV.pcf: line 305, column 27
    function def_onEnter_209 (def :  pcf.EditableOtherCoverageDetailsLV) : void {
      def.onEnter(Exposure)
    }
    
    // 'def' attribute on InputSetRef at TowOnlyDV.pcf: line 338, column 49
    function def_onEnter_226 (def :  pcf.DeductibleInfoInputSet) : void {
      def.onEnter(Exposure)
    }
    
    // 'def' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_67 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Exposure.Claimant), null, Exposure.Claim)
    }
    
    // 'def' attribute on InputSetRef at TowOnlyDV.pcf: line 218, column 51
    function def_refreshVariables_140 (def :  pcf.ExposureWorkloadInputSet) : void {
      def.refreshVariables(Exposure)
    }
    
    // 'def' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_159 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (asVehicleIncident().recoveryagent), null, Exposure.Claim)
    }
    
    // 'def' attribute on InputSetRef (id=RecoveryAddress) at TowOnlyDV.pcf: line 264, column 167
    function def_refreshVariables_187 (def :  pcf.CCAddressInputSet) : void {
      def.refreshVariables(asVehicleIncident().RecoveryAddressOwner)
    }
    
    // 'def' attribute on ListViewInput at TowOnlyDV.pcf: line 305, column 27
    function def_refreshVariables_210 (def :  pcf.EditableOtherCoverageDetailsLV) : void {
      def.refreshVariables(Exposure)
    }
    
    // 'def' attribute on InputSetRef at TowOnlyDV.pcf: line 338, column 49
    function def_refreshVariables_227 (def :  pcf.DeductibleInfoInputSet) : void {
      def.refreshVariables(Exposure)
    }
    
    // 'def' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_68 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Exposure.Claimant), null, Exposure.Claim)
    }
    
    // 'value' attribute on TypeKeyInput (id=Exposure_LossParty_Input) at TowOnlyDV.pcf: line 19, column 44
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      Exposure.LossParty = (__VALUE_TO_SET as typekey.LossPartyType)
    }
    
    // 'value' attribute on RangeInput (id=Exposure_Coverage_Input) at TowOnlyDV.pcf: line 37, column 38
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      Exposure.Coverage = (__VALUE_TO_SET as entity.Coverage)
    }
    
    // 'value' attribute on RangeInput (id=Vehicle_Incident_Input) at TowOnlyDV.pcf: line 164, column 44
    function defaultSetter_113 (__VALUE_TO_SET :  java.lang.Object) : void {
      Exposure.VehicleIncident = (__VALUE_TO_SET as entity.VehicleIncident)
    }
    
    // 'value' attribute on TypeKeyInput (id=Recovery_RecovInd_Input) at TowOnlyDV.pcf: line 228, column 35
    function defaultSetter_142 (__VALUE_TO_SET :  java.lang.Object) : void {
      asVehicleIncident().RecovInd = (__VALUE_TO_SET as typekey.YesNo)
    }
    
    // 'value' attribute on DateInput (id=Recovery_RecovDate_Input) at TowOnlyDV.pcf: line 236, column 61
    function defaultSetter_147 (__VALUE_TO_SET :  java.lang.Object) : void {
      asVehicleIncident().RecovDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Recovery_RecovState_Input) at TowOnlyDV.pcf: line 243, column 61
    function defaultSetter_153 (__VALUE_TO_SET :  java.lang.Object) : void {
      asVehicleIncident().RecovState = (__VALUE_TO_SET as typekey.State)
    }
    
    // 'value' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_169 (__VALUE_TO_SET :  java.lang.Object) : void {
      asVehicleIncident().recoveryagent = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TypeKeyInput (id=Recovery_RecovCondType_Input) at TowOnlyDV.pcf: line 258, column 60
    function defaultSetter_181 (__VALUE_TO_SET :  java.lang.Object) : void {
      asVehicleIncident().RecovCondType = (__VALUE_TO_SET as typekey.RecovCondType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Recovery_RecovClassType_Input) at TowOnlyDV.pcf: line 271, column 60
    function defaultSetter_190 (__VALUE_TO_SET :  java.lang.Object) : void {
      asVehicleIncident().RecovClassType = (__VALUE_TO_SET as typekey.RecovClassType)
    }
    
    // 'value' attribute on TypeKeyInput (id=JurisdictionState_Input) at TowOnlyDV.pcf: line 291, column 43
    function defaultSetter_201 (__VALUE_TO_SET :  java.lang.Object) : void {
      Exposure.JurisdictionState = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claimant_OtherCoverage_Input) at TowOnlyDV.pcf: line 300, column 41
    function defaultSetter_206 (__VALUE_TO_SET :  java.lang.Object) : void {
      Exposure.OtherCoverage = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on PolicyStatCodePickerInput (id=Exposure_StatLine_Input) at PolicyStatCodePickerWidget.xml: line 5, column 25
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      Exposure.StatLine = (__VALUE_TO_SET as entity.StatCode)
    }
    
    // 'value' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_79 (__VALUE_TO_SET :  java.lang.Object) : void {
      Exposure.Claimant = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claimant_Type_Input) at TowOnlyDV.pcf: line 121, column 42
    function defaultSetter_89 (__VALUE_TO_SET :  java.lang.Object) : void {
      Exposure.ClaimantType = (__VALUE_TO_SET as typekey.ClaimantType)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claimant_ContactPermitted_Input) at TowOnlyDV.pcf: line 136, column 36
    function defaultSetter_93 (__VALUE_TO_SET :  java.lang.Object) : void {
      ContactProhibited = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'filter' attribute on TypeKeyInput (id=JurisdictionState_Input) at TowOnlyDV.pcf: line 291, column 43
    function filter_203 (VALUE :  typekey.Jurisdiction, VALUES :  typekey.Jurisdiction[]) : java.lang.Boolean {
      return VALUE.hasCategory(JurisdictionType.TC_INSURANCE)
    }
    
    // 'onPick' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_167 (PickedValue :  Contact) : void {
      var contactType = statictypeof (asVehicleIncident().recoveryagent); var result = eval("asVehicleIncident().recoveryagent = Exposure.Claim.resolveContact(asVehicleIncident().recoveryagent) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_77 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Exposure.Claimant); var result = eval("Exposure.Claimant = Exposure.Claim.resolveContact(Exposure.Claimant) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'value' attribute on Reflect at TowOnlyDV.pcf: line 151, column 53
    function reflectionValue_100 (TRIGGER_INDEX :  int, VALUE :  entity.Contact) : java.lang.Object {
      return VALUE.PrimaryAddressDisplayValue
    }
    
    // 'value' attribute on Reflect at TowOnlyDV.pcf: line 191, column 33
    function reflectionValue_119 (TRIGGER_INDEX :  int, VALUE :  entity.VehicleIncident) : java.lang.Object {
      return VALUE.driver
    }
    
    // 'value' attribute on Reflect at TowOnlyDV.pcf: line 199, column 38
    function reflectionValue_124 (TRIGGER_INDEX :  int, VALUE :  entity.VehicleIncident) : java.lang.Object {
      return VALUE.Description
    }
    
    // 'value' attribute on Reflect at TowOnlyDV.pcf: line 207, column 42
    function reflectionValue_129 (TRIGGER_INDEX :  int, VALUE :  entity.VehicleIncident) : java.lang.Object {
      return VALUE.VehicleOperable
    }
    
    // 'value' attribute on Reflect at TowOnlyDV.pcf: line 215, column 39
    function reflectionValue_134 (TRIGGER_INDEX :  int, VALUE :  entity.VehicleIncident) : java.lang.Object {
      return VALUE.LossEstimate
    }
    
    // ClaimContactInput (id=Claimant_Picker_Input) at TowOnlyDV.pcf: line 106, column 43
    function reflectionValue_74 (TRIGGER_INDEX :  int, VALUE :  typekey.LossPartyType) : java.lang.Object {
      return (VALUE == TC_INSURED) ? (Exposure.Claim.Insured) : (true) ? ("") : "<NOCHANGE>"
    }
    
    // Reflect at TowOnlyDV.pcf: line 123, column 40
    function reflectionValue_87 (TRIGGER_INDEX :  int, VALUE :  entity.Contact) : java.lang.Object {
      return (Exposure.Claim.Insured==VALUE) ? ("insured") : (true) ? ("") : "<NOCHANGE>"
    }
    
    // 'value' attribute on Reflect at TowOnlyDV.pcf: line 143, column 44
    function reflectionValue_95 (TRIGGER_INDEX :  int, VALUE :  entity.Contact) : java.lang.Object {
      return VALUE.PrimaryPhoneValue
    }
    
    // 'valueRange' attribute on Reflect at TowOnlyDV.pcf: line 167, column 82
    function valueRange_105 (TRIGGER_INDEX :  int, VALUE :  typekey.LossPartyType) : java.lang.Object {
      return Exposure.getVehicleIncidentsWithMatchingLossParty(VALUE)
    }
    
    // 'valueRange' attribute on RangeInput (id=Vehicle_Incident_Input) at TowOnlyDV.pcf: line 164, column 44
    function valueRange_115 () : java.lang.Object {
      return Exposure.getVehicleIncidentsWithMatchingLossParty(Exposure.LossParty)
    }
    
    // 'valueRange' attribute on RangeInput (id=Exposure_Coverage_Input) at TowOnlyDV.pcf: line 37, column 38
    function valueRange_13 () : java.lang.Object {
      return Exposure.Claim.Policy.getCoveragesByCoverageType(Exposure.PrimaryCoverage)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_171 () : java.lang.Object {
      return Exposure.Claim.RelatedContacts
    }
    
    // 'valueRange' attribute on PolicyStatCodePickerInput (id=Exposure_StatLine_Input) at PolicyStatCodePickerWidget.xml: line 5, column 25
    function valueRange_40 () : java.lang.Object {
      return Exposure.Claim.Policy.CappedStatCodes
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_81 () : java.lang.Object {
      return Exposure.Claim.getRelatedContacts(Exposure.getContactTypes(TC_CLAIMANT))
    }
    
    // 'value' attribute on TextInput (id=Incident_Driver_Input) at TowOnlyDV.pcf: line 188, column 35
    function valueRoot_122 () : java.lang.Object {
      return Exposure.VehicleIncident
    }
    
    // 'value' attribute on TypeKeyInput (id=Recovery_RecovInd_Input) at TowOnlyDV.pcf: line 228, column 35
    function valueRoot_143 () : java.lang.Object {
      return asVehicleIncident()
    }
    
    // 'value' attribute on TypeKeyInput (id=Exposure_LossParty_Input) at TowOnlyDV.pcf: line 19, column 44
    function valueRoot_2 () : java.lang.Object {
      return Exposure
    }
    
    // 'value' attribute on CurrencyInput (id=Exposure_RemainingReserves_Input) at TowOnlyDV.pcf: line 318, column 57
    function valueRoot_212 () : java.lang.Object {
      return Exposure.ExposureRpt
    }
    
    // 'value' attribute on TextInput (id=Claimant_PrimaryPhone_Input) at TowOnlyDV.pcf: line 140, column 53
    function valueRoot_98 () : java.lang.Object {
      return Exposure.Claimant
    }
    
    // 'value' attribute on TypeKeyInput (id=Exposure_LossParty_Input) at TowOnlyDV.pcf: line 19, column 44
    function value_0 () : typekey.LossPartyType {
      return Exposure.LossParty
    }
    
    // 'value' attribute on RangeInput (id=Exposure_Coverage_Input) at TowOnlyDV.pcf: line 37, column 38
    function value_10 () : entity.Coverage {
      return Exposure.Coverage
    }
    
    // 'value' attribute on TextInput (id=Claimant_Address_Input) at TowOnlyDV.pcf: line 148, column 62
    function value_102 () : java.lang.String {
      return Exposure.Claimant.PrimaryAddressDisplayValue
    }
    
    // 'value' attribute on RangeInput (id=Vehicle_Incident_Input) at TowOnlyDV.pcf: line 164, column 44
    function value_112 () : entity.VehicleIncident {
      return Exposure.VehicleIncident
    }
    
    // 'value' attribute on TextInput (id=Incident_Driver_Input) at TowOnlyDV.pcf: line 188, column 35
    function value_121 () : entity.Person {
      return Exposure.VehicleIncident.driver
    }
    
    // 'value' attribute on TextInput (id=Incident_Description_Input) at TowOnlyDV.pcf: line 196, column 54
    function value_126 () : java.lang.String {
      return Exposure.VehicleIncident.Description
    }
    
    // 'value' attribute on BooleanRadioInput (id=Incident_VehicleOperable_Input) at TowOnlyDV.pcf: line 204, column 58
    function value_131 () : java.lang.Boolean {
      return Exposure.VehicleIncident.VehicleOperable
    }
    
    // 'value' attribute on CurrencyInput (id=Incident_LossEstimate_Input) at TowOnlyDV.pcf: line 212, column 55
    function value_136 () : gw.api.financials.CurrencyAmount {
      return Exposure.VehicleIncident.LossEstimate
    }
    
    // 'value' attribute on TypeKeyInput (id=Recovery_RecovInd_Input) at TowOnlyDV.pcf: line 228, column 35
    function value_141 () : typekey.YesNo {
      return asVehicleIncident().RecovInd
    }
    
    // 'value' attribute on DateInput (id=Recovery_RecovDate_Input) at TowOnlyDV.pcf: line 236, column 61
    function value_146 () : java.util.Date {
      return asVehicleIncident().RecovDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Recovery_RecovState_Input) at TowOnlyDV.pcf: line 243, column 61
    function value_152 () : typekey.State {
      return asVehicleIncident().RecovState
    }
    
    // 'value' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_168 () : entity.Contact {
      return asVehicleIncident().recoveryagent
    }
    
    // 'value' attribute on TextInput (id=AssignedUser_Name_Input) at TowOnlyDV.pcf: line 41, column 49
    function value_17 () : java.lang.String {
      return Exposure.AssigneeDisplayString
    }
    
    // 'value' attribute on TypeKeyInput (id=Recovery_RecovCondType_Input) at TowOnlyDV.pcf: line 258, column 60
    function value_180 () : typekey.RecovCondType {
      return asVehicleIncident().RecovCondType
    }
    
    // 'value' attribute on TypeKeyInput (id=Recovery_RecovClassType_Input) at TowOnlyDV.pcf: line 271, column 60
    function value_189 () : typekey.RecovClassType {
      return asVehicleIncident().RecovClassType
    }
    
    // 'value' attribute on TypeKeyInput (id=Exposure_Segment_Input) at TowOnlyDV.pcf: line 279, column 43
    function value_194 () : typekey.ClaimSegment {
      return Exposure.Segment
    }
    
    // 'value' attribute on TypeKeyInput (id=Exposure_Strategy_Input) at TowOnlyDV.pcf: line 284, column 44
    function value_197 () : typekey.ClaimStrategy {
      return Exposure.Strategy
    }
    
    // 'value' attribute on TextInput (id=AssignedGroup_Name_Input) at TowOnlyDV.pcf: line 45, column 58
    function value_20 () : java.lang.String {
      return Exposure.AssigneeGroupOnlyDisplayString
    }
    
    // 'value' attribute on TypeKeyInput (id=JurisdictionState_Input) at TowOnlyDV.pcf: line 291, column 43
    function value_200 () : typekey.Jurisdiction {
      return Exposure.JurisdictionState
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claimant_OtherCoverage_Input) at TowOnlyDV.pcf: line 300, column 41
    function value_205 () : java.lang.Boolean {
      return Exposure.OtherCoverage
    }
    
    // 'value' attribute on CurrencyInput (id=Exposure_RemainingReserves_Input) at TowOnlyDV.pcf: line 318, column 57
    function value_211 () : gw.api.financials.CurrencyAmount {
      return Exposure.ExposureRpt.RemainingReserves
    }
    
    // 'value' attribute on CurrencyInput (id=Exposure_FuturePayments_Input) at TowOnlyDV.pcf: line 322, column 54
    function value_214 () : gw.api.financials.CurrencyAmount {
      return Exposure.ExposureRpt.FuturePayments
    }
    
    // 'value' attribute on CurrencyInput (id=Exposure_TotalPayments_Input) at TowOnlyDV.pcf: line 326, column 53
    function value_217 () : gw.api.financials.CurrencyAmount {
      return Exposure.ExposureRpt.TotalPayments
    }
    
    // 'value' attribute on CurrencyInput (id=Exposure_TotalRecoveries_Input) at TowOnlyDV.pcf: line 330, column 55
    function value_220 () : gw.api.financials.CurrencyAmount {
      return Exposure.ExposureRpt.TotalRecoveries
    }
    
    // 'value' attribute on CurrencyInput (id=Exposure_TotalIncurredNet_Input) at TowOnlyDV.pcf: line 335, column 56
    function value_223 () : gw.api.financials.CurrencyAmount {
      return Exposure.ExposureRpt.TotalIncurredNet
    }
    
    // 'value' attribute on TypeKeyInput (id=State_Input) at TowOnlyDV.pcf: line 50, column 44
    function value_23 () : typekey.ExposureState {
      return Exposure.State
    }
    
    // 'value' attribute on TypeKeyInput (id=CreatedVia_Input) at TowOnlyDV.pcf: line 55, column 33
    function value_26 () : CreatedVia {
      return Exposure.CreatedVia
    }
    
    // 'value' attribute on DateInput (id=CreateTime_Input) at TowOnlyDV.pcf: line 59, column 38
    function value_30 () : java.util.Date {
      return Exposure.CreateTime
    }
    
    // 'value' attribute on PolicyStatCodePickerInput (id=Exposure_StatLine_Input) at PolicyStatCodePickerWidget.xml: line 5, column 25
    function value_37 () : entity.StatCode {
      return Exposure.StatLine
    }
    
    // 'value' attribute on TypeKeyInput (id=Exposure_PrimaryCoverage_Input) at TowOnlyDV.pcf: line 24, column 43
    function value_4 () : typekey.CoverageType {
      return Exposure.PrimaryCoverage
    }
    
    // 'value' attribute on TextInput (id=StatLine_Warning_Input) at TowOnlyDV.pcf: line 75, column 65
    function value_49 () : java.lang.String {
      return DisplayKey.get("Java.StatCodes.StatCodeListCappedWarning", Exposure.Claim.Policy.StatCodeListCap, Exposure.Claim.Policy.StatCodes.length)
    }
    
    // 'value' attribute on DateInput (id=ClosedDate_Input) at TowOnlyDV.pcf: line 82, column 48
    function value_54 () : java.util.Date {
      return Exposure.CloseDate
    }
    
    // 'value' attribute on TypeKeyInput (id=ClosedOutcome_Input) at TowOnlyDV.pcf: line 88, column 49
    function value_59 () : typekey.ExposureClosedOutcomeType {
      return Exposure.ClosedOutcome
    }
    
    // 'value' attribute on TypeKeyInput (id=ValidationLevel_Input) at TowOnlyDV.pcf: line 93, column 46
    function value_63 () : typekey.ValidationLevel {
      return Exposure.ValidationLevel
    }
    
    // 'value' attribute on TypeKeyInput (id=Exposure_CoverageSubType_Input) at TowOnlyDV.pcf: line 29, column 46
    function value_7 () : typekey.CoverageSubtype {
      return Exposure.CoverageSubType
    }
    
    // 'value' attribute on ClaimContactInput (id=Claimant_Picker_Input) at TowOnlyDV.pcf: line 109, column 45
    function value_73 (TRIGGER_INDEX :  int, VALUE :  typekey.LossPartyType) : java.lang.Object {
      return Exposure.Claim.Insured
    }
    
    // 'value' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_78 () : entity.Contact {
      return Exposure.Claimant
    }
    
    // 'value' attribute on TypeKeyInput (id=Claimant_Type_Input) at TowOnlyDV.pcf: line 121, column 42
    function value_88 () : typekey.ClaimantType {
      return Exposure.ClaimantType
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claimant_ContactPermitted_Input) at TowOnlyDV.pcf: line 136, column 36
    function value_92 () : java.lang.Boolean {
      return ContactProhibited
    }
    
    // 'value' attribute on TextInput (id=Claimant_PrimaryPhone_Input) at TowOnlyDV.pcf: line 140, column 53
    function value_97 () : java.lang.String {
      return Exposure.Claimant.PrimaryPhoneValue
    }
    
    // 'valueRange' attribute on RangeInput (id=Vehicle_Incident_Input) at TowOnlyDV.pcf: line 164, column 44
    function verifyValueRangeIsAllowedType_116 ($$arg :  entity.VehicleIncident[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Vehicle_Incident_Input) at TowOnlyDV.pcf: line 164, column 44
    function verifyValueRangeIsAllowedType_116 ($$arg :  gw.api.database.IQueryBeanResult<entity.VehicleIncident>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Vehicle_Incident_Input) at TowOnlyDV.pcf: line 164, column 44
    function verifyValueRangeIsAllowedType_116 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Exposure_Coverage_Input) at TowOnlyDV.pcf: line 37, column 38
    function verifyValueRangeIsAllowedType_14 ($$arg :  entity.Coverage[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Exposure_Coverage_Input) at TowOnlyDV.pcf: line 37, column 38
    function verifyValueRangeIsAllowedType_14 ($$arg :  gw.api.database.IQueryBeanResult<entity.Coverage>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Exposure_Coverage_Input) at TowOnlyDV.pcf: line 37, column 38
    function verifyValueRangeIsAllowedType_14 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_172 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_172 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_172 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on PolicyStatCodePickerInput (id=Exposure_StatLine_Input) at PolicyStatCodePickerWidget.xml: line 5, column 25
    function verifyValueRangeIsAllowedType_41 ($$arg :  entity.StatCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on PolicyStatCodePickerInput (id=Exposure_StatLine_Input) at PolicyStatCodePickerWidget.xml: line 5, column 25
    function verifyValueRangeIsAllowedType_41 ($$arg :  gw.api.database.IQueryBeanResult<entity.StatCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on PolicyStatCodePickerInput (id=Exposure_StatLine_Input) at PolicyStatCodePickerWidget.xml: line 5, column 25
    function verifyValueRangeIsAllowedType_41 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_82 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_82 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_82 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Vehicle_Incident_Input) at TowOnlyDV.pcf: line 164, column 44
    function verifyValueRange_117 () : void {
      var __valueRangeArg = Exposure.getVehicleIncidentsWithMatchingLossParty(Exposure.LossParty)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_116(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Exposure_Coverage_Input) at TowOnlyDV.pcf: line 37, column 38
    function verifyValueRange_15 () : void {
      var __valueRangeArg = Exposure.Claim.Policy.getCoveragesByCoverageType(Exposure.PrimaryCoverage)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_14(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_173 () : void {
      var __valueRangeArg = Exposure.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_172(__valueRangeArg)
    }
    
    // 'valueRange' attribute on PolicyStatCodePickerInput (id=Exposure_StatLine_Input) at PolicyStatCodePickerWidget.xml: line 5, column 25
    function verifyValueRange_42 () : void {
      var __valueRangeArg = Exposure.Claim.Policy.CappedStatCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_41(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_83 () : void {
      var __valueRangeArg = Exposure.Claim.getRelatedContacts(Exposure.getContactTypes(TC_CLAIMANT))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_82(__valueRangeArg)
    }
    
    // 'valueType' attribute on TypeKeyInput (id=CreatedVia_Input) at TowOnlyDV.pcf: line 55, column 33
    function verifyValueType_29 () : void {
      var __valueTypeArg : CreatedVia
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    // 'visible' attribute on DateInput (id=Recovery_RecovDate_Input) at TowOnlyDV.pcf: line 236, column 61
    function visible_145 () : java.lang.Boolean {
      return  asVehicleIncident().RecovInd == TC_YES 
    }
    
    // 'visible' attribute on ClaimContactInput (id=RecovAgcy_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_160 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (asVehicleIncident().recoveryagent), Exposure.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on InputSetRef (id=RecoveryAddress) at TowOnlyDV.pcf: line 264, column 167
    function visible_185 () : java.lang.Boolean {
      return asVehicleIncident().RecovInd == YesNo.TC_YES and (asVehicleIncident().RecovCondType != null and asVehicleIncident().RecovCondType != TC_NOT_RECOV)
    }
    
    // 'visible' attribute on TypeKeyInput (id=Recovery_RecovClassType_Input) at TowOnlyDV.pcf: line 271, column 60
    function visible_188 () : java.lang.Boolean {
      return asVehicleIncident().RecovInd == TC_YES 
    }
    
    // 'visible' attribute on TextInput (id=StatLine_Warning_Input) at TowOnlyDV.pcf: line 75, column 65
    function visible_48 () : java.lang.Boolean {
      return Exposure.Claim.Policy.StatCodeListCapped
    }
    
    // 'visible' attribute on InputSet at TowOnlyDV.pcf: line 61, column 59
    function visible_52 () : java.lang.Boolean {
      return Exposure.Claim.Policy.StatCodingEnabled
    }
    
    // 'visible' attribute on DateInput (id=ClosedDate_Input) at TowOnlyDV.pcf: line 82, column 48
    function visible_53 () : java.lang.Boolean {
      return Exposure.State == TC_CLOSED
    }
    
    // 'visible' attribute on TypeKeyInput (id=ClosedOutcome_Input) at TowOnlyDV.pcf: line 88, column 49
    function visible_58 () : java.lang.Boolean {
      return Exposure.State == TC_CLOSED 
    }
    
    // 'visible' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_66 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_69 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Exposure.Claimant), Exposure.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    property get Exposure () : Exposure {
      return getRequireValue("Exposure", 0) as Exposure
    }
    
    property set Exposure ($arg :  Exposure) {
      setRequireValue("Exposure", 0, $arg)
    }
    
    
    function asVehicleIncident() : VehicleIncident {
            return Exposure.Incident as VehicleIncident;
          }
          
    property get ContactProhibited() : boolean {
        return Exposure.Claim.getClaimContact(Exposure.Claimant).ContactProhibited
    }
      
    property set ContactProhibited(prohibited : boolean) {
      var claimContact = Exposure.Claim.getClaimContact(Exposure.Claimant) 
      if (claimContact != null) claimContact.ContactProhibited = prohibited 
    }
          
        
    
    
  }
  
  
}