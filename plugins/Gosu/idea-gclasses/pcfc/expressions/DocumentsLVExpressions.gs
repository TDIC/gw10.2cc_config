package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/documents/DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/documents/DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at DocumentsLV.pcf: line 22, column 43
    function initialValue_0 () : java.util.Set<String> {
      return documentSearchCriteria.Claim.UndeletableDocumentPublicIds
    }
    
    // 'initialValue' attribute on Variable at DocumentsLV.pcf: line 26, column 52
    function initialValue_1 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'sortBy' attribute on LinkCell (id=HiddenDocument) at DocumentsLV.pcf: line 175, column 61
    function sortValue_10 (document :  entity.Document) : java.lang.Object {
      return document.Obsolete
    }
    
    // 'sortBy' attribute on LinkCell (id=Name) at DocumentsLV.pcf: line 59, column 23
    function sortValue_2 (document :  entity.Document) : java.lang.Object {
      return document.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentsLV.pcf: line 133, column 41
    function sortValue_3 (document :  entity.Document) : java.lang.Object {
      return document.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at DocumentsLV.pcf: line 139, column 45
    function sortValue_4 (document :  entity.Document) : java.lang.Object {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at DocumentsLV.pcf: line 145, column 58
    function sortValue_5 (document :  entity.Document) : java.lang.Object {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentsLV.pcf: line 150, column 51
    function sortValue_6 (document :  entity.Document) : java.lang.Object {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentsLV.pcf: line 155, column 36
    function sortValue_7 (document :  entity.Document) : java.lang.Object {
      return document.Author
    }
    
    // 'value' attribute on TextCell (id=Event_Cell) at DocumentsLV.pcf: line 160, column 40
    function sortValue_8 (document :  entity.Document) : java.lang.Object {
      return document.Event_TDIC
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at DocumentsLV.pcf: line 168, column 42
    function sortValue_9 (document :  entity.Document) : java.lang.Object {
      return document.DateModified
    }
    
    // 'value' attribute on RowIterator at DocumentsLV.pcf: line 33, column 87
    function value_67 () : gw.api.database.IQueryBeanResult<gw.pl.persistence.core.Bean> {
      return DocumentList
    }
    
    // 'visible' attribute on LinkCell (id=HiddenDocument) at DocumentsLV.pcf: line 175, column 61
    function visible_11 () : java.lang.Boolean {
      return documentSearchCriteria.IncludeObsoletes
    }
    
    property get DocumentList () : gw.api.database.IQueryBeanResult<gw.pl.persistence.core.Bean> {
      return getRequireValue("DocumentList", 0) as gw.api.database.IQueryBeanResult<gw.pl.persistence.core.Bean>
    }
    
    property set DocumentList ($arg :  gw.api.database.IQueryBeanResult<gw.pl.persistence.core.Bean>) {
      setRequireValue("DocumentList", 0, $arg)
    }
    
    property get documentSearchCriteria () : DocumentSearchCriteria {
      return getRequireValue("documentSearchCriteria", 0) as DocumentSearchCriteria
    }
    
    property set documentSearchCriteria ($arg :  DocumentSearchCriteria) {
      setRequireValue("documentSearchCriteria", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get undeletableDocumentPublicIds () : java.util.Set<String> {
      return getVariableValue("undeletableDocumentPublicIds", 0) as java.util.Set<String>
    }
    
    property set undeletableDocumentPublicIds ($arg :  java.util.Set<String>) {
      setVariableValue("undeletableDocumentPublicIds", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/documents/DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DocumentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at DocumentsLV.pcf: line 68, column 147
    function action_18 () : void {
      document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at DocumentsLV.pcf: line 77, column 145
    function action_23 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=DocumentsLV_FinalizeLink) at DocumentsLV.pcf: line 88, column 124
    function action_27 () : void {
      new tdic.cc.integ.plugins.hpexstream.util.TDIC_CCExstreamHelper().finalizeLiveDocument(document)
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at DocumentsLV.pcf: line 95, column 77
    function action_29 () : void {
      DocumentDetailsPopup.push(document)
    }
    
    // 'action' attribute on Link (id=UploadLink) at DocumentsLV.pcf: line 113, column 88
    function action_34 () : void {
      UploadDocumentContentPopup.push(document)
    }
    
    // 'action' attribute on Link (id=DocumentsLV_DeleteLink) at DocumentsLV.pcf: line 122, column 85
    function action_39 () : void {
      gw.document.DocumentsUtil.deleteDocument(document)
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at DocumentsLV.pcf: line 95, column 77
    function action_dest_30 () : pcf.api.Destination {
      return pcf.DocumentDetailsPopup.createDestination(document)
    }
    
    // 'action' attribute on Link (id=UploadLink) at DocumentsLV.pcf: line 113, column 88
    function action_dest_35 () : pcf.api.Destination {
      return pcf.UploadDocumentContentPopup.createDestination(document)
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at DocumentsLV.pcf: line 68, column 147
    function available_16 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document) 
    }
    
    // 'available' attribute on Link (id=NameLinkWeb) at DocumentsLV.pcf: line 77, column 145
    function available_21 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=ViewPropertiesLink) at DocumentsLV.pcf: line 95, column 77
    function available_28 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'available' attribute on Link (id=UploadLink) at DocumentsLV.pcf: line 113, column 88
    function available_32 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=DocumentsLV_DeleteLink) at DocumentsLV.pcf: line 122, column 85
    function available_37 () : java.lang.Boolean {
      return documentsActionsHelper.isDeleteDocumentLinkAvailable(document, undeletableDocumentPublicIds)
    }
    
    // 'condition' attribute on ToolbarFlag at DocumentsLV.pcf: line 38, column 34
    function condition_12 () : java.lang.Boolean {
      return perm.Document.edit(document)
    }
    
    // 'condition' attribute on ToolbarFlag at DocumentsLV.pcf: line 41, column 24
    function condition_13 () : java.lang.Boolean {
      return document.Obsolete
    }
    
    // 'condition' attribute on ToolbarFlag at DocumentsLV.pcf: line 44, column 35
    function condition_14 () : java.lang.Boolean {
      return documentsActionsHelper.isDeleteDocumentLinkVisible(document) and documentsActionsHelper.isDeleteDocumentLinkAvailable(document, undeletableDocumentPublicIds)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at DocumentsLV.pcf: line 52, column 32
    function icon_15 () : java.lang.String {
      return document.Icon
    }
    
    // 'icon' attribute on Link (id=DocumentsLV_DeleteLink) at DocumentsLV.pcf: line 122, column 85
    function icon_41 () : java.lang.String {
      return "delete" 
    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at DocumentsLV.pcf: line 68, column 147
    function label_19 () : java.lang.Object {
      return document.Name
    }
    
    // 'label' attribute on Link (id=DocumentsLV_ActionsDisabled) at DocumentsLV.pcf: line 127, column 75
    function label_43 () : java.lang.Object {
      return documentsActionsHelper.AsynchronousActionsMessage
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at DocumentsLV.pcf: line 68, column 147
    function tooltip_20 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(document)
    }
    
    // 'tooltip' attribute on Link (id=ViewPropertiesLink) at DocumentsLV.pcf: line 95, column 77
    function tooltip_31 () : java.lang.String {
      return documentsActionsHelper.ViewDocumentPropertiesTooltip
    }
    
    // 'tooltip' attribute on Link (id=UploadLink) at DocumentsLV.pcf: line 113, column 88
    function tooltip_36 () : java.lang.String {
      return documentsActionsHelper.UploadDocumentContentTooltip
    }
    
    // 'tooltip' attribute on Link (id=DocumentsLV_DeleteLink) at DocumentsLV.pcf: line 122, column 85
    function tooltip_40 () : java.lang.String {
      return documentsActionsHelper.DeleteDocumentTooltip(document, undeletableDocumentPublicIds)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentsLV.pcf: line 133, column 41
    function valueRoot_45 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentsLV.pcf: line 133, column 41
    function value_44 () : java.lang.String {
      return document.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at DocumentsLV.pcf: line 139, column 45
    function value_47 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at DocumentsLV.pcf: line 145, column 58
    function value_50 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentsLV.pcf: line 150, column 51
    function value_53 () : typekey.DocumentStatusType {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentsLV.pcf: line 155, column 36
    function value_56 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on TextCell (id=Event_Cell) at DocumentsLV.pcf: line 160, column 40
    function value_59 () : java.lang.String {
      return document.Event_TDIC
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at DocumentsLV.pcf: line 168, column 42
    function value_62 () : java.util.Date {
      return document.DateModified
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at DocumentsLV.pcf: line 68, column 147
    function visible_17 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at DocumentsLV.pcf: line 77, column 145
    function visible_22 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_FinalizeLink) at DocumentsLV.pcf: line 88, column 124
    function visible_26 () : java.lang.Boolean {
      return document.MimeType == "application/dlf" and document.Status == DocumentStatusType.TC_DRAFT
    }
    
    // 'visible' attribute on Link (id=UploadLink) at DocumentsLV.pcf: line 113, column 88
    function visible_33 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentVisible(document)
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_DeleteLink) at DocumentsLV.pcf: line 122, column 85
    function visible_38 () : java.lang.Boolean {
      return documentsActionsHelper.isDeleteDocumentLinkVisible(document)
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_ActionsDisabled) at DocumentsLV.pcf: line 127, column 75
    function visible_42 () : java.lang.Boolean {
      return documentsActionsHelper.isDocumentPending(document)
    }
    
    // 'visible' attribute on LinkCell (id=HiddenDocument) at DocumentsLV.pcf: line 175, column 61
    function visible_66 () : java.lang.Boolean {
      return documentSearchCriteria.IncludeObsoletes
    }
    
    property get document () : entity.Document {
      return getIteratedValue(1) as entity.Document
    }
    
    
  }
  
  
}