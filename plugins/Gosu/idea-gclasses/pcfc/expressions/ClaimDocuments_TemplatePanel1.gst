<% uses pcf.* %>
<% uses entity.* %>
<% uses typekey.* %>
<% uses gw.api.locale.DisplayKey %>
<%@ params(final DocumentList : gw.api.database.IQueryBeanResult<Document>, final onbaseThumbnailUtil : acc.onbase.util.ThumbnailUtil) %>
<%@ extends acc.onbase.util.ThumbnailUtil %>


<% if (onbaseThumbnailUtil.IsThumbnailViewActive) { %>

    <style>
    <% var thumbnailSize = acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ThumbnailSize%>

    #templatePanelContainer {
        width: 100%;
    }

    a:link {
        color: #000000;
        text-decoration: none;
    }

    a:visited {
        color: #000000;
        text-decoration: none;
    }

    a:hover {
        color: #0086ac;
        text-decoration: underline;
    }

    a:active {
        color: #000000;
        text-decoration: none;
    }

    .card {
        display: inline-block;
        text-align: center;
        margin: 10px;
        margin-bottom: 0px;
        border-radius: 5px;
        background-color: #e3e3e3;
        box-shadow: 3px 3px 5px #cccccc;
        height: ${thumbnailSize}px;
        width: ${thumbnailSize}px;
        overflow: hidden;
        border: 1px solid #cccccc;
    }

    .cardcontainer {
        display: inline-block;
        text-align: center;
        margin-bottom: 5px;
        min-width: 150px;
    }

    .thumbnailBox {
        display: flex;
        width: ${thumbnailSize}px;
        height: ${thumbnailSize}px;
        border-radius: 5px;
        margin: auto;
        text-align: center;
    }

    .docNameText {
        margin: auto;
        padding: 2%;
        padding-top: 0px;
    }

    .pendingInfoContainer {
        display: block;
        margin: auto;
    }

    p {
    margin: auto;
    padding: 0px;
    }

</style>

    <% var docSublist = getDocSublist(onbaseThumbnailUtil.ThumbnailPageNumber, DocumentList) %>

    <div id="templatePanelContainer">

        <% foreach (doc in docSublist) {%>

            <% var docName = doc.Name %>

            <%if (doc.DocUID != null) { %>

                <% var docPopURL = popWebClient(doc.DocUID) %>
                <% var thumbnailBytes = getThumbnailBytes(doc.DocUID) %>

                <% if (thumbnailBytes.length > 0 || onbaseThumbnailUtil.DisplayNoThumbDocs) { %>
                    <div class="cardcontainer">
                        <div class="card">

                            <% if(thumbnailBytes.length > 0) { %>
                                 <a href="${docPopURL}" target="_blank">
                                    <div class="thumbnailBox" title="${docName}"
                                    style="background-image: url(data:image/png;base64,${thumbnailBytes}); background-repeat: no-repeat; background-size: cover;">
                                    </div>
                                 </a>
                            <% } else { %>
                                <div class="thumbnailBox" title="${docName}">
                                     <p>
                                     ${DisplayKey.get("Accelerator.OnBase.ThumbnailViewer.STR_GW_NoThumbnailImageAvailable")}
                                    </p>
                                    <br>
                                 </div>
                            <% } %>
                        </div>

                        <a href="${docPopURL}" target="_blank">
                            <p class="docNameText" title="${docName}">
                                <% if (docName.length > 15 ) { %>
                                ${docName.safeSubstring(0,15)}...
                                <% } else { %>
                                ${docName}
                                <% } %>
                            </p>
                        </a>
                    </div>

                <% } %>
            <% } %>

            <% if (doc.DocUID == null && onbaseThumbnailUtil.DisplayNoThumbDocs) { %>
                <div class="cardcontainer">
                        <div class="card">
                            <div class="thumbnailBox" title="${docName}">
                                <div class="pendingInfoContainer">
                                    <p>
                                        ${DisplayKey.get("Accelerator.OnBase.ThumbnailViewer.STR_GW_DocContentPending")}
                                    </p>
                                    <p>
                                        ${DisplayKey.get("Accelerator.OnBase.ThumbnailViewer.STR_GW_NoThumbnailImageAvailable")}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <p class="docNameText" title="${docName}">
                            <% if (docName.length > 15 ) { %>
                            ${docName.safeSubstring(0,15)}...
                            <% } else { %>
                            ${docName}
                            <% } %>
                        </p>
                </div>
            <% } %>

        <% } %>

    </div>

<% } %>