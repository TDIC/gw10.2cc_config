package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_NegotiationCoverageInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_NegotiationCoverageInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_NegotiationCoverageInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_NegotiationCoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at TDIC_NegotiationCoverageInputSet.default.pcf: line 26, column 45
    function valueRoot_2 () : java.lang.Object {
      return cov.Type
    }
    
    // 'value' attribute on TextCell (id=limit_Cell) at TDIC_NegotiationCoverageInputSet.default.pcf: line 30, column 42
    function valueRoot_5 () : java.lang.Object {
      return cov
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at TDIC_NegotiationCoverageInputSet.default.pcf: line 26, column 45
    function value_1 () : java.lang.String {
      return cov.Type.DisplayName
    }
    
    // 'value' attribute on TextCell (id=limit_Cell) at TDIC_NegotiationCoverageInputSet.default.pcf: line 30, column 42
    function value_4 () : gw.api.financials.CurrencyAmount {
      return cov.ExposureLimit
    }
    
    property get cov () : PolicyCoverage {
      return getIteratedValue(1) as PolicyCoverage
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_NegotiationCoverageInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_NegotiationCoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label at TDIC_NegotiationCoverageInputSet.default.pcf: line 12, column 44
    function label_0 () : java.lang.String {
      return claim.Type_TDIC.DisplayName
    }
    
    // 'value' attribute on RowIterator at TDIC_NegotiationCoverageInputSet.default.pcf: line 21, column 40
    function value_7 () : PolicyCoverage[] {
      return claim.Policy.Coverages
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    
  }
  
  
}