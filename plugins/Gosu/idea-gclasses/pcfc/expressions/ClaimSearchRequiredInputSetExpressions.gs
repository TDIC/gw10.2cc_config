package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/claims/ClaimSearchRequiredInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClaimSearchRequiredInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/search/claims/ClaimSearchRequiredInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimSearchRequiredInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 10, column 49
    function action_41 () : void {
      pcf.GroupSearchPopup.push()
    }
    
    // 'action' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 13, column 49
    function action_43 () : void {
      pcf.OrganizationGroupTreePopup.push()
    }
    
    // 'action' attribute on UserInput (id=AssignedToUser_Input) at UserWidget.xml: line 9, column 49
    function action_63 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // 'action' attribute on UserInput (id=AssignedToUser_Input) at UserWidget.xml: line 12, column 49
    function action_65 () : void {
      pcf.UserSelectPopup.push()
    }
    
    // 'action' attribute on UserInput (id=CreatedBy_Input) at UserWidget.xml: line 9, column 49
    function action_79 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // 'action' attribute on UserInput (id=CreatedBy_Input) at UserWidget.xml: line 12, column 49
    function action_81 () : void {
      pcf.UserSelectPopup.push()
    }
    
    // 'action' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 10, column 49
    function action_dest_42 () : pcf.api.Destination {
      return pcf.GroupSearchPopup.createDestination()
    }
    
    // 'action' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 13, column 49
    function action_dest_44 () : pcf.api.Destination {
      return pcf.OrganizationGroupTreePopup.createDestination()
    }
    
    // 'action' attribute on UserInput (id=AssignedToUser_Input) at UserWidget.xml: line 9, column 49
    function action_dest_64 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'action' attribute on UserInput (id=AssignedToUser_Input) at UserWidget.xml: line 12, column 49
    function action_dest_66 () : pcf.api.Destination {
      return pcf.UserSelectPopup.createDestination()
    }
    
    // 'action' attribute on UserInput (id=CreatedBy_Input) at UserWidget.xml: line 9, column 49
    function action_dest_80 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'action' attribute on UserInput (id=CreatedBy_Input) at UserWidget.xml: line 12, column 49
    function action_dest_82 () : pcf.api.Destination {
      return pcf.UserSelectPopup.createDestination()
    }
    
    // 'available' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 7, column 52
    function available_45 () : java.lang.Boolean {
      return liveClaimFlag
    }
    
    // 'def' attribute on InputSetRef at ClaimSearchRequiredInputSet.pcf: line 64, column 54
    function def_onEnter_27 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(ClaimSearchCriteria.NameCriteria))
    }
    
    // 'def' attribute on InputSetRef at ClaimSearchRequiredInputSet.pcf: line 64, column 54
    function def_onEnter_29 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(ClaimSearchCriteria.NameCriteria))
    }
    
    // 'def' attribute on InputSetRef at ClaimSearchRequiredInputSet.pcf: line 67, column 54
    function def_onEnter_32 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(ClaimSearchCriteria.NameCriteria))
    }
    
    // 'def' attribute on InputSetRef at ClaimSearchRequiredInputSet.pcf: line 67, column 54
    function def_onEnter_34 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(ClaimSearchCriteria.NameCriteria))
    }
    
    // 'def' attribute on InputSetRef at ClaimSearchRequiredInputSet.pcf: line 64, column 54
    function def_refreshVariables_28 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(ClaimSearchCriteria.NameCriteria))
    }
    
    // 'def' attribute on InputSetRef at ClaimSearchRequiredInputSet.pcf: line 64, column 54
    function def_refreshVariables_30 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(ClaimSearchCriteria.NameCriteria))
    }
    
    // 'def' attribute on InputSetRef at ClaimSearchRequiredInputSet.pcf: line 67, column 54
    function def_refreshVariables_33 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(ClaimSearchCriteria.NameCriteria))
    }
    
    // 'def' attribute on InputSetRef at ClaimSearchRequiredInputSet.pcf: line 67, column 54
    function def_refreshVariables_35 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(ClaimSearchCriteria.NameCriteria))
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 41, column 49
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=IncidentType_Input) at ClaimSearchRequiredInputSet.pcf: line 47, column 43
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.IncidentType_TDIC = (__VALUE_TO_SET as typekey.ClaimType_TDIC)
    }
    
    // 'value' attribute on BooleanRadioInput (id=RiskManagementIncident_Input) at ClaimSearchRequiredInputSet.pcf: line 52, column 64
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.RiskManagementIncident_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=SearchFor_Input) at ClaimSearchRequiredInputSet.pcf: line 61, column 54
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.NameSearchType = (__VALUE_TO_SET as typekey.ClaimSearchNameSearchType)
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 30, column 48
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.ClaimNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=TaxID_Input) at ClaimSearchRequiredInputSet.pcf: line 73, column 55
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.NameCriteria.TaxId = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 7, column 52
    function defaultSetter_47 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.AssignedToGroup.AssignedToGroup = (__VALUE_TO_SET as entity.Group)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IncludeSubGroups_Input) at ClaimSearchRequiredInputSet.pcf: line 87, column 69
    function defaultSetter_59 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.AssignedToGroup.IncludeSubGroups = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on UserInput (id=AssignedToUser_Input) at UserWidget.xml: line 6, column 85
    function defaultSetter_69 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.AssignedToUser = (__VALUE_TO_SET as entity.User)
    }
    
    // 'value' attribute on TextInput (id=LegacyClaimNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 35, column 59
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.LegacyClaimNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on UserInput (id=CreatedBy_Input) at UserWidget.xml: line 6, column 85
    function defaultSetter_85 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.CreatedByUser = (__VALUE_TO_SET as entity.User)
    }
    
    // 'value' attribute on RangeInput (id=CatNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 114, column 39
    function defaultSetter_97 (__VALUE_TO_SET :  java.lang.Object) : void {
      ClaimSearchCriteria.Catastrophe = (__VALUE_TO_SET as entity.Catastrophe)
    }
    
    // 'filter' attribute on TypeKeyInput (id=SearchFor_Input) at ClaimSearchRequiredInputSet.pcf: line 61, column 54
    function filter_25 (VALUE :  typekey.ClaimSearchNameSearchType, VALUES :  typekey.ClaimSearchNameSearchType[]) : java.lang.Boolean {
      return liveClaimFlag or gw.api.system.PLConfigParameters.PersonalDataDestructionEnabled.Value or VALUE.hasCategory(ClaimSearchType.TC_ARCHIVED)
    }
    
    // 'initialValue' attribute on Variable at ClaimSearchRequiredInputSet.pcf: line 18, column 23
    function initialValue_0 () : boolean {
      return ClaimSearchCriteria.ClaimSearchType == ClaimSearchType.TC_ACTIVE
    }
    
    // 'initialValue' attribute on Variable at ClaimSearchRequiredInputSet.pcf: line 22, column 41
    function initialValue_1 () : ClaimSearchNameSearchType {
      return setNameSearchType()
    }
    
    // 'mode' attribute on InputSetRef at ClaimSearchRequiredInputSet.pcf: line 64, column 54
    function mode_31 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'valueRange' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 7, column 52
    function valueRange_49 () : java.lang.Object {
      return gw.api.admin.BaseAdminUtil.getGroupsForCurrentUser()
    }
    
    // 'valueRange' attribute on UserInput (id=AssignedToUser_Input) at UserWidget.xml: line 6, column 85
    function valueRange_71 () : java.lang.Object {
      return entity.User.util.getUsersInCurrentUsersGroup()
    }
    
    // 'valueRange' attribute on RangeInput (id=CatNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 114, column 39
    function valueRange_99 () : java.lang.Object {
      return gw.api.admin.CatastropheUtil.getCatastrophes()
    }
    
    // 'value' attribute on TextInput (id=TaxID_Input) at ClaimSearchRequiredInputSet.pcf: line 73, column 55
    function valueRoot_39 () : java.lang.Object {
      return ClaimSearchCriteria.NameCriteria
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 30, column 48
    function valueRoot_4 () : java.lang.Object {
      return ClaimSearchCriteria
    }
    
    // 'value' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 7, column 52
    function valueRoot_48 () : java.lang.Object {
      return ClaimSearchCriteria.AssignedToGroup
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 41, column 49
    function value_10 () : java.lang.String {
      return ClaimSearchCriteria.PolicyNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=IncidentType_Input) at ClaimSearchRequiredInputSet.pcf: line 47, column 43
    function value_14 () : typekey.ClaimType_TDIC {
      return ClaimSearchCriteria.IncidentType_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=RiskManagementIncident_Input) at ClaimSearchRequiredInputSet.pcf: line 52, column 64
    function value_18 () : java.lang.Boolean {
      return ClaimSearchCriteria.RiskManagementIncident_TDIC
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 30, column 48
    function value_2 () : java.lang.String {
      return ClaimSearchCriteria.ClaimNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=SearchFor_Input) at ClaimSearchRequiredInputSet.pcf: line 61, column 54
    function value_22 () : typekey.ClaimSearchNameSearchType {
      return ClaimSearchCriteria.NameSearchType
    }
    
    // 'value' attribute on TextInput (id=TaxID_Input) at ClaimSearchRequiredInputSet.pcf: line 73, column 55
    function value_37 () : java.lang.String {
      return ClaimSearchCriteria.NameCriteria.TaxId
    }
    
    // 'value' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 7, column 52
    function value_46 () : entity.Group {
      return ClaimSearchCriteria.AssignedToGroup.AssignedToGroup
    }
    
    // 'value' attribute on BooleanRadioInput (id=IncludeSubGroups_Input) at ClaimSearchRequiredInputSet.pcf: line 87, column 69
    function value_58 () : java.lang.Boolean {
      return ClaimSearchCriteria.AssignedToGroup.IncludeSubGroups
    }
    
    // 'value' attribute on TextInput (id=LegacyClaimNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 35, column 59
    function value_6 () : java.lang.String {
      return ClaimSearchCriteria.LegacyClaimNumber_TDIC
    }
    
    // 'value' attribute on UserInput (id=AssignedToUser_Input) at UserWidget.xml: line 6, column 85
    function value_68 () : entity.User {
      return ClaimSearchCriteria.AssignedToUser
    }
    
    // 'value' attribute on UserInput (id=CreatedBy_Input) at UserWidget.xml: line 6, column 85
    function value_84 () : entity.User {
      return ClaimSearchCriteria.CreatedByUser
    }
    
    // 'value' attribute on RangeInput (id=CatNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 114, column 39
    function value_96 () : entity.Catastrophe {
      return ClaimSearchCriteria.Catastrophe
    }
    
    // 'valueRange' attribute on RangeInput (id=CatNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 114, column 39
    function verifyValueRangeIsAllowedType_100 ($$arg :  entity.Catastrophe[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CatNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 114, column 39
    function verifyValueRangeIsAllowedType_100 ($$arg :  gw.api.database.IQueryBeanResult<entity.Catastrophe>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CatNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 114, column 39
    function verifyValueRangeIsAllowedType_100 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 7, column 52
    function verifyValueRangeIsAllowedType_50 ($$arg :  entity.Group[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 7, column 52
    function verifyValueRangeIsAllowedType_50 ($$arg :  gw.api.database.IQueryBeanResult<entity.Group>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 7, column 52
    function verifyValueRangeIsAllowedType_50 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=AssignedToUser_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_72 ($$arg :  entity.User[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=AssignedToUser_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_72 ($$arg :  gw.api.database.IQueryBeanResult<entity.User>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=AssignedToUser_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_72 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=CreatedBy_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_88 ($$arg :  entity.User[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=CreatedBy_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_88 ($$arg :  gw.api.database.IQueryBeanResult<entity.User>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=CreatedBy_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_88 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CatNumber_Input) at ClaimSearchRequiredInputSet.pcf: line 114, column 39
    function verifyValueRange_101 () : void {
      var __valueRangeArg = gw.api.admin.CatastropheUtil.getCatastrophes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_100(__valueRangeArg)
    }
    
    // 'valueRange' attribute on GroupInput (id=AssignedToGroup_Input) at GroupWidget.xml: line 7, column 52
    function verifyValueRange_51 () : void {
      var __valueRangeArg = gw.api.admin.BaseAdminUtil.getGroupsForCurrentUser()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_50(__valueRangeArg)
    }
    
    // 'valueRange' attribute on UserInput (id=AssignedToUser_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRange_73 () : void {
      var __valueRangeArg = entity.User.util.getUsersInCurrentUsersGroup()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_72(__valueRangeArg)
    }
    
    // 'valueRange' attribute on UserInput (id=CreatedBy_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRange_89 () : void {
      var __valueRangeArg = entity.User.util.getUsersInCurrentUsersGroup()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_88(__valueRangeArg)
    }
    
    property get ClaimSearchCriteria () : ClaimSearchCriteria {
      return getRequireValue("ClaimSearchCriteria", 0) as ClaimSearchCriteria
    }
    
    property set ClaimSearchCriteria ($arg :  ClaimSearchCriteria) {
      setRequireValue("ClaimSearchCriteria", 0, $arg)
    }
    
    property get liveClaimFlag () : boolean {
      return getVariableValue("liveClaimFlag", 0) as java.lang.Boolean
    }
    
    property set liveClaimFlag ($arg :  boolean) {
      setVariableValue("liveClaimFlag", 0, $arg)
    }
    
    property get nameSearchType () : ClaimSearchNameSearchType {
      return getVariableValue("nameSearchType", 0) as ClaimSearchNameSearchType
    }
    
    property set nameSearchType ($arg :  ClaimSearchNameSearchType) {
      setVariableValue("nameSearchType", 0, $arg)
    }
    
    function setNameSearchType(): ClaimSearchNameSearchType{
      return ClaimSearchNameSearchType.TC_ANY
    }
    
    
  }
  
  
}