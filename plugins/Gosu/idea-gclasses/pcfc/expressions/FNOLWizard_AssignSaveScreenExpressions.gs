package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.cc.claim.NewClaimAssignmentOption
@javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_AssignSaveScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FNOLWizard_AssignSaveScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_AssignSaveScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FNOLWizard_AssignSaveScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=CommonAssign_PickerButton) at CCAssigneeWidget.xml: line 8, column 25
    function action_2 () : void {
      AssigneePickerPopup.push(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (Claim))), Claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimAssign_PickerButton) at CCAssigneeWidget.xml: line 8, column 25
    function action_21 () : void {
      AssigneePickerPopup.push(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (Claim))), Claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimAssign_PickerButton) at CCAssigneeWidget.xml: line 8, column 25
    function action_dest_22 () : pcf.api.Destination {
      return pcf.AssigneePickerPopup.createDestination(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (Claim))), Claim)
    }
    
    // 'action' attribute on MenuItem (id=CommonAssign_PickerButton) at CCAssigneeWidget.xml: line 8, column 25
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AssigneePickerPopup.createDestination(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (Claim))), Claim)
    }
    
    // 'value' attribute on Choice (id=CommonAssignChoice) at FNOLWizard_AssignSaveScreen.pcf: line 24, column 41
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      Wizard.CommonAssignment = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CCAssigneeInput (id=ClaimAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      AssignableInstanceForClaim.Assignee = (__VALUE_TO_SET as gw.api.assignment.Assignee)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SuppressFNOLDocsEmail_Input) at FNOLWizard_AssignSaveScreen.pcf: line 68, column 113
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.SuppressFNOLDocsEmail_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=lateReportedReason_TDIC_Input) at FNOLWizard_AssignSaveScreen.pcf: line 76, column 113
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.LateReportedReason_TDIC = (__VALUE_TO_SET as typekey.LateReportedReason_TDIC)
    }
    
    // 'required' attribute on CCAssigneeInput (id=ClaimAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function required_25 () : java.lang.Boolean {
      return !Wizard.CommonAssignment
    }
    
    // 'required' attribute on CCAssigneeInput (id=CommonAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function required_5 () : java.lang.Boolean {
      return Wizard.CommonAssignment
    }
    
    // 'valueRange' attribute on CCAssigneeInput (id=CommonAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function valueRange_8 () : java.lang.Object {
      return Claim.SuggestedAssignees
    }
    
    // 'value' attribute on CCAssigneeInput (id=ClaimAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function valueRoot_28 () : java.lang.Object {
      return AssignableInstanceForClaim
    }
    
    // 'value' attribute on BooleanRadioInput (id=SuppressFNOLDocsEmail_Input) at FNOLWizard_AssignSaveScreen.pcf: line 68, column 113
    function valueRoot_46 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on CCAssigneeInput (id=CommonAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function valueRoot_7 () : java.lang.Object {
      return Wizard
    }
    
    // 'value' attribute on CCAssigneeInput (id=ClaimAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function value_26 () : gw.api.assignment.Assignee {
      return AssignableInstanceForClaim.Assignee
    }
    
    // 'value' attribute on BooleanRadioInput (id=SuppressFNOLDocsEmail_Input) at FNOLWizard_AssignSaveScreen.pcf: line 68, column 113
    function value_44 () : java.lang.Boolean {
      return Claim.SuppressFNOLDocsEmail_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=lateReportedReason_TDIC_Input) at FNOLWizard_AssignSaveScreen.pcf: line 76, column 113
    function value_50 () : typekey.LateReportedReason_TDIC {
      return Claim.LateReportedReason_TDIC
    }
    
    // 'value' attribute on CCAssigneeInput (id=CommonAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function value_6 () : gw.api.assignment.Assignee {
      return Wizard.CommonAssignee
    }
    
    // 'valueRange' attribute on CCAssigneeInput (id=ClaimAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function verifyValueRangeIsAllowedType_30 ($$arg :  gw.api.assignment.Assignee[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on CCAssigneeInput (id=ClaimAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function verifyValueRangeIsAllowedType_30 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on CCAssigneeInput (id=CommonAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function verifyValueRangeIsAllowedType_9 ($$arg :  gw.api.assignment.Assignee[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on CCAssigneeInput (id=CommonAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on CCAssigneeInput (id=CommonAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function verifyValueRange_10 () : void {
      var __valueRangeArg = Claim.SuggestedAssignees
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_9(__valueRangeArg)
    }
    
    // 'valueRange' attribute on CCAssigneeInput (id=ClaimAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function verifyValueRange_31 () : void {
      var __valueRangeArg = Claim.SuggestedAssignees
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_30(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=ClaimAssignment_Input) at FNOLWizard_AssignSaveScreen.pcf: line 28, column 56
    function visible_0 () : java.lang.Boolean {
      return Claim.Policy.Verified == false 
    }
    
    // 'visible' attribute on Label at FNOLWizard_AssignSaveScreen.pcf: line 49, column 95
    function visible_20 () : java.lang.Boolean {
      return ((Claim.Exposures != null) && (Claim.Exposures.length > 0))
    }
    
    // 'visible' attribute on CCAssigneeInput (id=CommonAssign_Input) at CCAssigneeWidget.xml: line 8, column 25
    function visible_4 () : java.lang.Boolean {
      return Claim.Policy.Verified == true 
    }
    
    // 'visible' attribute on BooleanRadioInput (id=SuppressFNOLDocsEmail_Input) at FNOLWizard_AssignSaveScreen.pcf: line 68, column 113
    function visible_43 () : java.lang.Boolean {
      return Claim.Type_TDIC == ClaimType_TDIC.TC_WORKERSCOMPENSATION and Claim.Policy.Verified == true
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get Wizard () : gw.api.claim.NewClaimWizardInfo {
      return getRequireValue("Wizard", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set Wizard ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setRequireValue("Wizard", 0, $arg)
    }
    
    
    property get AssignableInstanceForClaim() : NewClaimAssignmentOption {
      var allAssignments = Wizard.IndividualAssignments
      for (var instanceAssignment in allAssignments){
        if (instanceAssignment.Target == Claim) {
           return instanceAssignment
        }
      }
      return null   
    }
          
    
    function getAssignableInstanceForExposure(exposureToFind : Exposure) : NewClaimAssignmentOption {  
      var allAssignments = Wizard.IndividualAssignments
      for (var instanceAssignment in allAssignments){
        if (instanceAssignment.Target == exposureToFind) {
           return instanceAssignment
        }
      }  
      return null
    }
    
    
  }
  
  
}