package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/lossdetails/TDIC_InjuryNewPersonOnlyPickerMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_InjuryNewPersonOnlyPickerMenuItemSetExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/lossdetails/TDIC_InjuryNewPersonOnlyPickerMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_InjuryNewPersonOnlyPickerMenuItemSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ClaimNewPersonOnlyPickerMenuItemSet_NewPersonMenuItem) at TDIC_InjuryNewPersonOnlyPickerMenuItemSet.pcf: line 27, column 68
    function action_1 () : void {
      TDIC_NewContactPopup.push(typekey.Contact.TC_PERSON, parentContact, claim, contactRole)
    }
    
    // 'action' attribute on PickerMenuItem (id=ContactSearch) at TDIC_InjuryNewPersonOnlyPickerMenuItemSet.pcf: line 32, column 62
    function action_3 () : void {
      AddressBookPickerPopup.push(requiredContactType,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimNewPersonOnlyPickerMenuItemSet_NewPersonMenuItem) at TDIC_InjuryNewPersonOnlyPickerMenuItemSet.pcf: line 27, column 68
    function action_dest_2 () : pcf.api.Destination {
      return pcf.TDIC_NewContactPopup.createDestination(typekey.Contact.TC_PERSON, parentContact, claim, contactRole)
    }
    
    // 'action' attribute on PickerMenuItem (id=ContactSearch) at TDIC_InjuryNewPersonOnlyPickerMenuItemSet.pcf: line 32, column 62
    function action_dest_4 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(requiredContactType,claim)
    }
    
    // 'onPick' attribute on PickerMenuItem (id=ContactSearch) at TDIC_InjuryNewPersonOnlyPickerMenuItemSet.pcf: line 32, column 62
    function onPick_5 (PickedValue :  Contact) : void {
      injuryIncident.injured = PickedValue as Person
    }
    
    // 'visible' attribute on MenuItem (id=ClaimNewPersonOnlyPickerMenuItemSet_NewPersonMenuItem) at TDIC_InjuryNewPersonOnlyPickerMenuItemSet.pcf: line 27, column 68
    function visible_0 () : java.lang.Boolean {
      return requiredContactType.isAssignableFrom(entity.Person)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get contactRole () : typekey.ContactRole {
      return getRequireValue("contactRole", 0) as typekey.ContactRole
    }
    
    property set contactRole ($arg :  typekey.ContactRole) {
      setRequireValue("contactRole", 0, $arg)
    }
    
    property get injuryIncident () : InjuryIncident {
      return getRequireValue("injuryIncident", 0) as InjuryIncident
    }
    
    property set injuryIncident ($arg :  InjuryIncident) {
      setRequireValue("injuryIncident", 0, $arg)
    }
    
    property get parentContact () : Contact {
      return getRequireValue("parentContact", 0) as Contact
    }
    
    property set parentContact ($arg :  Contact) {
      setRequireValue("parentContact", 0, $arg)
    }
    
    property get requiredContactType () : Type {
      return getRequireValue("requiredContactType", 0) as Type
    }
    
    property set requiredContactType ($arg :  Type) {
      setRequireValue("requiredContactType", 0, $arg)
    }
    
    
  }
  
  
}