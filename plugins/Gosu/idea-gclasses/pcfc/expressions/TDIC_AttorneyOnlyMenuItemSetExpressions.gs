package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_AttorneyOnlyMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_AttorneyOnlyMenuItemSetExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_AttorneyOnlyMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_AttorneyOnlyMenuItemSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Adjudicator) at TDIC_AttorneyOnlyMenuItemSet.pcf: line 16, column 29
    function action_0 () : void {
      TDIC_NewContactwithdiffNamePopup.push(typekey.Contact.TC_ATTORNEY, null ,claim,attorneyType)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Adjudicator) at TDIC_AttorneyOnlyMenuItemSet.pcf: line 16, column 29
    function action_dest_1 () : pcf.api.Destination {
      return pcf.TDIC_NewContactwithdiffNamePopup.createDestination(typekey.Contact.TC_ATTORNEY, null ,claim,attorneyType)
    }
    
    // 'label' attribute on MenuItem (id=ClaimContacts_Adjudicator) at TDIC_AttorneyOnlyMenuItemSet.pcf: line 16, column 29
    function label_2 () : java.lang.Object {
      return attorneyType
    }
    
    property get attorneyType () : String {
      return getRequireValue("attorneyType", 0) as String
    }
    
    property set attorneyType ($arg :  String) {
      setRequireValue("attorneyType", 0, $arg)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    
  }
  
  
}