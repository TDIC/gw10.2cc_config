package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderRequestReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseFolderRequestReviewScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderRequestReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseFolderRequestReviewScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at ShareBaseFolderRequestReviewScreen.pcf: line 41, column 24
    function def_onEnter_6 (def :  pcf.ShareBaseFolderRequestContactsLV) : void {
      def.onEnter(claim, Request.LinkedRecipients, false)
    }
    
    // 'def' attribute on PanelRef at ShareBaseFolderRequestReviewScreen.pcf: line 41, column 24
    function def_refreshVariables_7 (def :  pcf.ShareBaseFolderRequestContactsLV) : void {
      def.refreshVariables(claim, Request.LinkedRecipients, false)
    }
    
    // 'value' attribute on TextInput (id=FolderName_Input) at ShareBaseFolderRequestReviewScreen.pcf: line 24, column 38
    function valueRoot_1 () : java.lang.Object {
      return folder
    }
    
    // 'value' attribute on TextInput (id=FolderName_Input) at ShareBaseFolderRequestReviewScreen.pcf: line 24, column 38
    function value_0 () : java.lang.String {
      return folder.FolderName
    }
    
    // 'value' attribute on DateInput (id=FolderExpiration_Input) at ShareBaseFolderRequestReviewScreen.pcf: line 28, column 38
    function value_3 () : java.util.Date {
      return folder.Expiration
    }
    
    property get Request () : InboundRequest_Ext {
      return getRequireValue("Request", 0) as InboundRequest_Ext
    }
    
    property set Request ($arg :  InboundRequest_Ext) {
      setRequireValue("Request", 0, $arg)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get folder () : ShareBase_Ext {
      return getRequireValue("folder", 0) as ShareBase_Ext
    }
    
    property set folder ($arg :  ShareBase_Ext) {
      setRequireValue("folder", 0, $arg)
    }
    
    
  }
  
  
}