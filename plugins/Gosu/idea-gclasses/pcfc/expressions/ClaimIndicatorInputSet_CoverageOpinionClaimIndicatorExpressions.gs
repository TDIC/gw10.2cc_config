package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/summary/indicator/ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClaimIndicatorInputSet_CoverageOpinionClaimIndicatorExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/summary/indicator/ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimIndicatorInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on BooleanRadioInput (id=CoverageOpinion_Input) at ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf: line 37, column 44
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      indicator.Claim.CoverageOpinion_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'iconColor' attribute on Link (id=CoverageOpinionIcon) at ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf: line 25, column 35
    function iconColor_3 () : gw.api.web.color.GWColor {
      return indicator.IconColor
    }
    
    // 'icon' attribute on Link (id=CoverageOpinionIcon) at ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf: line 25, column 35
    function icon_2 () : java.lang.String {
      return indicator.Icon
    }
    
    // 'initialValue' attribute on Variable at ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf: line 15, column 23
    function initialValue_0 () : boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'label' attribute on Link (id=CoverageOpinionText) at ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf: line 28, column 33
    function label_4 () : java.lang.Object {
      return indicator.Text
    }
    
    // 'onChange' attribute on PostOnChange at ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf: line 39, column 45
    function onChange_6 () : void {
      setCoverageInQuestion()
    }
    
    // 'value' attribute on BooleanRadioInput (id=CoverageOpinion_Input) at ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf: line 37, column 44
    function valueRoot_10 () : java.lang.Object {
      return indicator.Claim
    }
    
    // 'value' attribute on BooleanRadioInput (id=CoverageOpinion_Input) at ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf: line 37, column 44
    function value_8 () : java.lang.Boolean {
      return indicator.Claim.CoverageOpinion_TDIC
    }
    
    // 'visible' attribute on Link (id=CoverageOpinionIcon) at ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf: line 25, column 35
    function visible_1 () : java.lang.Boolean {
      return indicator.IsOn
    }
    
    // 'visible' attribute on ContentInput (id=CoverageOpinionOnLabel) at ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf: line 19, column 29
    function visible_5 () : java.lang.Boolean {
      return !inEditMode
    }
    
    // 'visible' attribute on BooleanRadioInput (id=CoverageOpinion_Input) at ClaimIndicatorInputSet.CoverageOpinionClaimIndicator.pcf: line 37, column 44
    function visible_7 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get inEditMode () : boolean {
      return getVariableValue("inEditMode", 0) as java.lang.Boolean
    }
    
    property set inEditMode ($arg :  boolean) {
      setVariableValue("inEditMode", 0, $arg)
    }
    
    property get indicator () : ClaimIndicator {
      return getRequireValue("indicator", 0) as ClaimIndicator
    }
    
    property set indicator ($arg :  ClaimIndicator) {
      setRequireValue("indicator", 0, $arg)
    }
    
    function setCoverageInQuestion()
    {
      if(indicator.Claim.CoverageOpinion_TDIC)
      indicator.Claim.CoverageInQuestion = true  
    }
    
    
  }
  
  
}