package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/util/Login.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LoginExpressions {
  @javax.annotation.Generated("config/web/pcf/util/Login.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LoginExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (target :  pcf.api.Destination, entryException :  java.lang.Exception) : int {
      return 0
    }
    
    // 'afterEnter' attribute on LoginPage (id=Login) at Login.pcf: line 12, column 26
    function afterEnter_10 () : void {
      loginForm.checkDirectHTTPLogin()
    }
    
    // 'def' attribute on PanelRef at Login.pcf: line 50, column 26
    function def_onEnter_5 (def :  pcf.LoginDV) : void {
      def.onEnter(loginForm)
    }
    
    // 'def' attribute on PanelRef at Login.pcf: line 57, column 102
    function def_onEnter_8 (def :  pcf.TDIC_SSOLoginPanelSet) : void {
      def.onEnter(url_Tdic_Ext, entryException)
    }
    
    // 'def' attribute on PanelRef at Login.pcf: line 50, column 26
    function def_refreshVariables_6 (def :  pcf.LoginDV) : void {
      def.refreshVariables(loginForm)
    }
    
    // 'def' attribute on PanelRef at Login.pcf: line 57, column 102
    function def_refreshVariables_9 (def :  pcf.TDIC_SSOLoginPanelSet) : void {
      def.refreshVariables(url_Tdic_Ext, entryException)
    }
    
    // 'initialValue' attribute on Variable at Login.pcf: line 24, column 37
    function initialValue_0 () : gw.api.util.LoginForm {
      return new gw.api.util.LoginForm(target, entryException)
    }
    
    // 'initialValue' attribute on Variable at Login.pcf: line 28, column 19
    function initialValue_1 () : int {
      return com.guidewire.pl.system.dependency.PLDependencies.getWebController().getRequestInfo().getRequestWrapper().getRequest().getServerPort()
    }
    
    // 'initialValue' attribute on Variable at Login.pcf: line 32, column 22
    function initialValue_2 () : String {
      return com.guidewire.pl.system.dependency.PLDependencies.getWebController().getRequestInfo().getRequestWrapper().getRequest().getServerName()
    }
    
    // 'initialValue' attribute on Variable at Login.pcf: line 36, column 22
    function initialValue_3 () : String {
      return "https://"+serverid_Tdic_Ext+":"+port_Tdic_Ext+"/cc/service/SSOLogin"
    }
    
    // 'label' attribute on Verbatim (id=loginFormMsg) at Login.pcf: line 53, column 40
    function label_4 () : java.lang.String {
      return loginForm.Message
    }
    
    // 'visible' attribute on PanelRef at Login.pcf: line 57, column 102
    function visible_7 () : java.lang.Boolean {
      return com.tdic.util.properties.PropertyUtil.isSsoEnabled() and entryException == null
    }
    
    override property get CurrentLocation () : pcf.Login {
      return super.CurrentLocation as pcf.Login
    }
    
    property get entryException () : java.lang.Exception {
      return getVariableValue("entryException", 0) as java.lang.Exception
    }
    
    property set entryException ($arg :  java.lang.Exception) {
      setVariableValue("entryException", 0, $arg)
    }
    
    property get loginForm () : gw.api.util.LoginForm {
      return getVariableValue("loginForm", 0) as gw.api.util.LoginForm
    }
    
    property set loginForm ($arg :  gw.api.util.LoginForm) {
      setVariableValue("loginForm", 0, $arg)
    }
    
    property get port_Tdic_Ext () : int {
      return getVariableValue("port_Tdic_Ext", 0) as java.lang.Integer
    }
    
    property set port_Tdic_Ext ($arg :  int) {
      setVariableValue("port_Tdic_Ext", 0, $arg)
    }
    
    property get serverid_Tdic_Ext () : String {
      return getVariableValue("serverid_Tdic_Ext", 0) as String
    }
    
    property set serverid_Tdic_Ext ($arg :  String) {
      setVariableValue("serverid_Tdic_Ext", 0, $arg)
    }
    
    property get target () : pcf.api.Destination {
      return getVariableValue("target", 0) as pcf.api.Destination
    }
    
    property set target ($arg :  pcf.api.Destination) {
      setVariableValue("target", 0, $arg)
    }
    
    property get url_Tdic_Ext () : String {
      return getVariableValue("url_Tdic_Ext", 0) as String
    }
    
    property set url_Tdic_Ext ($arg :  String) {
      setVariableValue("url_Tdic_Ext", 0, $arg)
    }
    
    
  }
  
  
}