package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsSelectedDocsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseShareDocumentsSelectedDocsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsSelectedDocsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ShareBaseShareDocumentsSelectedDocsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 64, column 147
    function action_14 () : void {
      documentLink.Document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 73, column 145
    function action_19 () : void {
      documentLink.Document.downloadContent()
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 64, column 147
    function available_12 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(documentLink.Document) 
    }
    
    // 'available' attribute on Link (id=NameLinkWeb) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 73, column 145
    function available_17 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(documentLink.Document)
    }
    
    // 'condition' attribute on ToolbarFlag at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 37, column 34
    function condition_8 () : java.lang.Boolean {
      return perm.Document.edit(documentLink.Document)
    }
    
    // 'condition' attribute on ToolbarFlag at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 40, column 24
    function condition_9 () : java.lang.Boolean {
      return documentLink.Document.Obsolete
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 48, column 36
    function icon_10 () : java.lang.String {
      return documentLink.Document.Icon
    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 64, column 147
    function label_15 () : java.lang.Object {
      return documentLink.Document.Name
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 64, column 147
    function tooltip_16 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(documentLink.Document)
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 80, column 45
    function valueRoot_23 () : java.lang.Object {
      return documentLink.Document
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 80, column 45
    function value_22 () : typekey.DocumentType {
      return documentLink.Document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 86, column 58
    function value_25 () : typekey.OnBaseDocumentSubtype_Ext {
      return documentLink.Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 91, column 51
    function value_28 () : typekey.DocumentStatusType {
      return documentLink.Document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 96, column 49
    function value_31 () : java.lang.String {
      return documentLink.Document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 105, column 24
    function value_34 () : java.util.Date {
      return documentLink.Document.DateModified
    }
    
    // 'visible' attribute on BooleanRadioCell (id=Icon_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 48, column 36
    function visible_11 () : java.lang.Boolean {
      return hasCheckboxes
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 64, column 147
    function visible_13 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 73, column 145
    function visible_18 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    property get documentLink () : entity.OutbdShareDocLink_Ext {
      return getIteratedValue(1) as entity.OutbdShareDocLink_Ext
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsSelectedDocsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseShareDocumentsSelectedDocsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 24, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'sortBy' attribute on LinkCell (id=Name) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 55, column 23
    function sortValue_2 (documentLink :  entity.OutbdShareDocLink_Ext) : java.lang.Object {
      return documentLink.Document.Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 80, column 45
    function sortValue_3 (documentLink :  entity.OutbdShareDocLink_Ext) : java.lang.Object {
      return documentLink.Document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 86, column 58
    function sortValue_4 (documentLink :  entity.OutbdShareDocLink_Ext) : java.lang.Object {
      return documentLink.Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 91, column 51
    function sortValue_5 (documentLink :  entity.OutbdShareDocLink_Ext) : java.lang.Object {
      return documentLink.Document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 96, column 49
    function sortValue_6 (documentLink :  entity.OutbdShareDocLink_Ext) : java.lang.Object {
      return documentLink.Document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 105, column 24
    function sortValue_7 (documentLink :  entity.OutbdShareDocLink_Ext) : java.lang.Object {
      return documentLink.Document.DateModified
    }
    
    // 'value' attribute on RowIterator at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 32, column 50
    function value_38 () : entity.OutbdShareDocLink_Ext[] {
      return sharedDocsLinks
    }
    
    // 'visible' attribute on BooleanRadioCell (id=Icon_Cell) at ShareBaseShareDocumentsSelectedDocsLV.pcf: line 48, column 36
    function visible_1 () : java.lang.Boolean {
      return hasCheckboxes
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get hasCheckboxes () : Boolean {
      return getRequireValue("hasCheckboxes", 0) as Boolean
    }
    
    property set hasCheckboxes ($arg :  Boolean) {
      setRequireValue("hasCheckboxes", 0, $arg)
    }
    
    property get sharedDocsLinks () : OutbdShareDocLink_Ext[] {
      return getRequireValue("sharedDocsLinks", 0) as OutbdShareDocLink_Ext[]
    }
    
    property set sharedDocsLinks ($arg :  OutbdShareDocLink_Ext[]) {
      setRequireValue("sharedDocsLinks", 0, $arg)
    }
    
    
  }
  
  
}