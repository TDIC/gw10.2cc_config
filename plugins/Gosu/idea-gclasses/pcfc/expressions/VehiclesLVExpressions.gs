package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/policy/VehiclesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class VehiclesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/policy/VehiclesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends VehiclesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=LinkedDocuments) at VehiclesLV.pcf: line 81, column 161
    function action_35 () : void {
      OnBaseDocumentListPopup.push(VehicleRU, acc.onbase.configuration.DocumentLinkType.VehicleCoverage, VehicleRU.DisplayName, new KeyableBean[]{VehicleRU.Policy.Claim})
    }
    
    // 'action' attribute on Link (id=LinkedDocuments) at VehiclesLV.pcf: line 81, column 161
    function action_dest_36 () : pcf.api.Destination {
      return pcf.OnBaseDocumentListPopup.createDestination(VehicleRU, acc.onbase.configuration.DocumentLinkType.VehicleCoverage, VehicleRU.DisplayName, new KeyableBean[]{VehicleRU.Policy.Claim})
    }
    
    // 'label' attribute on Link (id=LinkedDocuments) at VehiclesLV.pcf: line 81, column 161
    function label_37 () : java.lang.Object {
      return acc.onbase.api.application.DocumentLinking.getLinkingDocumentUILabel(VehicleRU, acc.onbase.configuration.DocumentLinkType.VehicleCoverage)
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at VehiclesLV.pcf: line 33, column 42
    function valueRoot_10 () : java.lang.Object {
      return VehicleRU
    }
    
    // 'value' attribute on TextCell (id=Make_Cell) at VehiclesLV.pcf: line 38, column 43
    function valueRoot_13 () : java.lang.Object {
      return VehicleRU.Vehicle
    }
    
    // 'value' attribute on TextCell (id=Make_Cell) at VehiclesLV.pcf: line 38, column 43
    function value_12 () : java.lang.String {
      return VehicleRU.Vehicle.Make
    }
    
    // 'value' attribute on TextCell (id=Model_Cell) at VehiclesLV.pcf: line 43, column 44
    function value_15 () : java.lang.String {
      return VehicleRU.Vehicle.Model
    }
    
    // 'value' attribute on TextCell (id=Year_Cell) at VehiclesLV.pcf: line 49, column 42
    function value_18 () : java.lang.Integer {
      return VehicleRU.Vehicle.Year
    }
    
    // 'value' attribute on TextCell (id=Color_Cell) at VehiclesLV.pcf: line 54, column 44
    function value_21 () : java.lang.String {
      return VehicleRU.Vehicle.Color
    }
    
    // 'value' attribute on TextCell (id=VIN_Cell) at VehiclesLV.pcf: line 59, column 42
    function value_24 () : java.lang.String {
      return VehicleRU.Vehicle.Vin
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at VehiclesLV.pcf: line 65, column 45
    function value_27 () : typekey.Jurisdiction {
      return VehicleRU.Vehicle.State
    }
    
    // 'value' attribute on TextCell (id=LicensePlate_Cell) at VehiclesLV.pcf: line 70, column 51
    function value_30 () : java.lang.String {
      return VehicleRU.Vehicle.LicensePlate
    }
    
    // 'value' attribute on TextCell (id=Lienholders_Cell) at VehiclesLV.pcf: line 75, column 114
    function value_33 () : java.lang.String {
      return VehicleRU.Vehicle != null ? VehicleRU.Vehicle.Lienholders.join(", ") : ""
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at VehiclesLV.pcf: line 33, column 42
    function value_9 () : java.lang.Integer {
      return VehicleRU.RUNumber
    }
    
    property get VehicleRU () : entity.VehicleRU {
      return getIteratedValue(1) as entity.VehicleRU
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/policy/VehiclesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class VehiclesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at VehiclesLV.pcf: line 33, column 42
    function sortValue_0 (VehicleRU :  entity.VehicleRU) : java.lang.Object {
      return VehicleRU.RUNumber
    }
    
    // 'value' attribute on TextCell (id=Make_Cell) at VehiclesLV.pcf: line 38, column 43
    function sortValue_1 (VehicleRU :  entity.VehicleRU) : java.lang.Object {
      return VehicleRU.Vehicle.Make
    }
    
    // 'value' attribute on TextCell (id=Model_Cell) at VehiclesLV.pcf: line 43, column 44
    function sortValue_2 (VehicleRU :  entity.VehicleRU) : java.lang.Object {
      return VehicleRU.Vehicle.Model
    }
    
    // 'value' attribute on TextCell (id=Year_Cell) at VehiclesLV.pcf: line 49, column 42
    function sortValue_3 (VehicleRU :  entity.VehicleRU) : java.lang.Object {
      return VehicleRU.Vehicle.Year
    }
    
    // 'value' attribute on TextCell (id=Color_Cell) at VehiclesLV.pcf: line 54, column 44
    function sortValue_4 (VehicleRU :  entity.VehicleRU) : java.lang.Object {
      return VehicleRU.Vehicle.Color
    }
    
    // 'value' attribute on TextCell (id=VIN_Cell) at VehiclesLV.pcf: line 59, column 42
    function sortValue_5 (VehicleRU :  entity.VehicleRU) : java.lang.Object {
      return VehicleRU.Vehicle.Vin
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at VehiclesLV.pcf: line 65, column 45
    function sortValue_6 (VehicleRU :  entity.VehicleRU) : java.lang.Object {
      return VehicleRU.Vehicle.State
    }
    
    // 'value' attribute on TextCell (id=LicensePlate_Cell) at VehiclesLV.pcf: line 70, column 51
    function sortValue_7 (VehicleRU :  entity.VehicleRU) : java.lang.Object {
      return VehicleRU.Vehicle.LicensePlate
    }
    
    // 'value' attribute on TextCell (id=Lienholders_Cell) at VehiclesLV.pcf: line 75, column 114
    function sortValue_8 (VehicleRU :  entity.VehicleRU) : java.lang.Object {
      return VehicleRU.Vehicle != null ? VehicleRU.Vehicle.Lienholders.join(", ") : ""
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at VehiclesLV.pcf: line 25, column 38
    function toCreateAndAdd_38 () : entity.VehicleRU {
      return Claim.Policy.createVehicleRU()
    }
    
    // 'toRemove' attribute on RowIterator at VehiclesLV.pcf: line 25, column 38
    function toRemove_39 (VehicleRU :  entity.VehicleRU) : void {
      Claim.Policy.removeVehicleRU(VehicleRU)
    }
    
    // 'value' attribute on RowIterator at VehiclesLV.pcf: line 25, column 38
    function value_40 () : entity.VehicleRU[] {
      return PolicyVehicleList
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get PolicyVehicleList () : VehicleRU[] {
      return getRequireValue("PolicyVehicleList", 0) as VehicleRU[]
    }
    
    property set PolicyVehicleList ($arg :  VehicleRU[]) {
      setRequireValue("PolicyVehicleList", 0, $arg)
    }
    
    
  }
  
  
}