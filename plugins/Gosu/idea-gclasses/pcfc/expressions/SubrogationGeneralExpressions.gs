package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/subrogation/General/SubrogationGeneral.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SubrogationGeneralExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/subrogation/General/SubrogationGeneral.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SubrogationGeneralExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Claim :  Claim) : int {
      return 0
    }
    
    static function __constructorIndex (Claim :  Claim, selectSubrogationFinancialsCard :  boolean) : int {
      return 1
    }
    
    // 'action' attribute on ToolbarButton (id=ToolbarButton) at SubrogationGeneral.pcf: line 28, column 171
    function action_1 () : void {
      OnBaseDocumentListPopup.push(Claim.SubrogationSummary, acc.onbase.configuration.DocumentLinkType.Subrogations, "Subrogations", new KeyableBean[]{Claim})
    }
    
    // 'action' attribute on ToolbarButton (id=ToolbarButton) at SubrogationGeneral.pcf: line 28, column 171
    function action_dest_2 () : pcf.api.Destination {
      return pcf.OnBaseDocumentListPopup.createDestination(Claim.SubrogationSummary, acc.onbase.configuration.DocumentLinkType.Subrogations, "Subrogations", new KeyableBean[]{Claim})
    }
    
    // 'canEdit' attribute on Page (id=SubrogationGeneral) at SubrogationGeneral.pcf: line 10, column 33
    function canEdit_6 () : java.lang.Boolean {
      return perm.Claim.edit(Claim)and perm.System.editsubrodetails
    }
    
    // 'canVisit' attribute on Page (id=SubrogationGeneral) at SubrogationGeneral.pcf: line 10, column 33
    static function canVisit_7 (Claim :  Claim, selectSubrogationFinancialsCard :  boolean) : java.lang.Boolean {
      return perm.Claim.view(Claim) and perm.System.viewsubrodetails
    }
    
    // 'def' attribute on PanelRef at SubrogationGeneral.pcf: line 31, column 80
    function def_onEnter_4 (def :  pcf.SubrogationMainPanelSet) : void {
      def.onEnter(Claim, selectSubrogationFinancialsCard)
    }
    
    // 'def' attribute on PanelRef at SubrogationGeneral.pcf: line 31, column 80
    function def_refreshVariables_5 (def :  pcf.SubrogationMainPanelSet) : void {
      def.refreshVariables(Claim, selectSubrogationFinancialsCard)
    }
    
    // EditButtons at SubrogationGeneral.pcf: line 24, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on ToolbarButton (id=ToolbarButton) at SubrogationGeneral.pcf: line 28, column 171
    function label_3 () : java.lang.Object {
      return acc.onbase.api.application.DocumentLinking.getLinkingDocumentUILabel(Claim.SubrogationSummary, acc.onbase.configuration.DocumentLinkType.Subrogations)
    }
    
    // Page (id=SubrogationGeneral) at SubrogationGeneral.pcf: line 10, column 33
    static function parent_8 (Claim :  Claim, selectSubrogationFinancialsCard :  boolean) : pcf.api.Destination {
      return pcf.ClaimSubrogationGroup.createDestination(Claim)
    }
    
    property get Claim () : Claim {
      return getVariableValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setVariableValue("Claim", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.SubrogationGeneral {
      return super.CurrentLocation as pcf.SubrogationGeneral
    }
    
    property get selectSubrogationFinancialsCard () : boolean {
      return getVariableValue("selectSubrogationFinancialsCard", 0) as java.lang.Boolean
    }
    
    property set selectSubrogationFinancialsCard ($arg :  boolean) {
      setVariableValue("selectSubrogationFinancialsCard", 0, $arg)
    }
    
    
  }
  
  
}