package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/contacts/AdditionalInfoInputSet.Person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdditionalInfoInputSet_PersonExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/contacts/AdditionalInfoInputSet.Person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdditionalInfoInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_100 () : void {
      if (Person.Guardian != null) { ClaimContactDetailPopup.push(Person.Guardian, claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_102 () : void {
      ClaimContactDetailPopup.push(Person.Guardian, claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_132 () : void {
      AddressBookPickerPopup.push(statictypeof (Person.Employer), claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_134 () : void {
      if (Person.Employer != null) { ClaimContactDetailPopup.push(Person.Employer, claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_135 () : void {
      ClaimContactDetailPopup.push(Person.Employer, claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_98 () : void {
      AddressBookPickerPopup.push(entity.Person.Type, claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_103 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Person.Guardian, claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_133 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Person.Employer), claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_136 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Person.Employer, claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_99 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(entity.Person.Type, claim, null as List<SpecialistService>)
    }
    
    // 'def' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_129 (def :  pcf.ClaimNewCompanyOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Person.Employer), Person, claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_95 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(entity.Person.Type, Person, claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_130 (def :  pcf.ClaimNewCompanyOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Person.Employer), Person, claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_96 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(entity.Person.Type, Person, claim)
    }
    
    // 'value' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_106 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.Guardian = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at AdditionalInfoInputSet.Person.pcf: line 160, column 150
    function defaultSetter_119 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.PreferredCurrency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on TextInput (id=Occupation_Input) at AdditionalInfoInputSet.Person.pcf: line 169, column 34
    function defaultSetter_125 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.Occupation = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_139 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.Employer = (__VALUE_TO_SET as entity.Company)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SSNReleaseAuthorized_Input) at AdditionalInfoInputSet.Person.pcf: line 54, column 118
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.SSNReleaseAuthorized = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=IDType_Input) at AdditionalInfoInputSet.Person.pcf: line 36, column 62
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.PersonClaimantIDType = (__VALUE_TO_SET as typekey.ClaimantIDType)
    }
    
    // 'value' attribute on TextInput (id=VisaNumber_Input) at AdditionalInfoInputSet.Person.pcf: line 61, column 118
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.VisaNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=GreenCardNumber_Input) at AdditionalInfoInputSet.Person.pcf: line 68, column 118
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.GreenCardNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=PassportNumber_Input) at AdditionalInfoInputSet.Person.pcf: line 75, column 118
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.PassportNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=JurisdictionAssignedID_Input) at AdditionalInfoInputSet.Person.pcf: line 82, column 118
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.JurisdictionAssignedID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=TaxExemptionsEntitled_Input) at AdditionalInfoInputSet.Person.pcf: line 97, column 63
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.TaxExemptionsEntitled = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TypeKeyInput (id=TaxFilingStatus_Input) at AdditionalInfoInputSet.Person.pcf: line 104, column 42
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.TaxFilingStatus = (__VALUE_TO_SET as typekey.TaxFilingStatusType)
    }
    
    // 'value' attribute on DateInput (id=DateOfBirth_Input) at AdditionalInfoInputSet.Person.pcf: line 117, column 42
    function defaultSetter_70 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.DateOfBirth = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Gender_Input) at AdditionalInfoInputSet.Person.pcf: line 125, column 42
    function defaultSetter_78 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.Gender = (__VALUE_TO_SET as typekey.GenderType)
    }
    
    // 'value' attribute on TypeKeyInput (id=MaritalStatus_Input) at AdditionalInfoInputSet.Person.pcf: line 132, column 97
    function defaultSetter_84 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.MaritalStatus = (__VALUE_TO_SET as typekey.MaritalStatus)
    }
    
    // 'value' attribute on PrivacyInput (id=TaxID_Input) at AdditionalInfoInputSet.Person.pcf: line 47, column 42
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.TaxID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=EducationLevel_Input) at AdditionalInfoInputSet.Person.pcf: line 140, column 63
    function defaultSetter_90 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.EducationLevel = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=TaxID_Input) at AdditionalInfoInputSet.Person.pcf: line 47, column 42
    function encryptionExpression_11 (VALUE :  java.lang.String) : java.lang.String {
      return tdic.cc.config.fnol.TDIC_WC7FNOLHelper.maskTaxID(claimContact,Person,VALUE)
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_104 (PickedValue :  Contact) : void {
      var contactType = entity.Person.Type; var result = eval("Person.Guardian = claim.resolveContact(Person.Guardian) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_137 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Person.Employer); var result = eval("Person.Employer = claim.resolveContact(Person.Employer) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'required' attribute on TextInput (id=Occupation_Input) at AdditionalInfoInputSet.Person.pcf: line 169, column 34
    function required_123 () : java.lang.Boolean {
      return tdic.cc.config.fnol.TDIC_WC7FNOLHelper.isOccupationRequired(claimContact)
    }
    
    // 'required' attribute on DateInput (id=DateOfBirth_Input) at AdditionalInfoInputSet.Person.pcf: line 117, column 42
    function required_68 () : java.lang.Boolean {
      return tdic.cc.config.fnol.TDIC_WC7FNOLHelper.isDOBRequired(claimContact)
    }
    
    // 'required' attribute on PrivacyInput (id=TaxID_Input) at AdditionalInfoInputSet.Person.pcf: line 47, column 42
    function required_7 () : java.lang.Boolean {
      return tdic.cc.config.fnol.TDIC_WC7FNOLHelper.isTaxIdRequired(claimContact)
    }
    
    // 'required' attribute on TypeKeyInput (id=Gender_Input) at AdditionalInfoInputSet.Person.pcf: line 125, column 42
    function required_76 () : java.lang.Boolean {
      return tdic.cc.config.fnol.TDIC_WC7FNOLHelper.isGenderRequired(claimContact)
    }
    
    // 'validationExpression' attribute on DateInput (id=DateOfBirth_Input) at AdditionalInfoInputSet.Person.pcf: line 117, column 42
    function validationExpression_66 () : java.lang.Object {
      return (Person.DateOfBirth == null or Person.DateOfBirth <= gw.api.upgrade.Coercions.makeDateFrom("today")) ? null : DisplayKey.get("Web.ContactDetail.AdditionalInfo.DateOfBirth.FutureError")
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_108 () : java.lang.Object {
      return claim.RelatedPersonArray
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_141 () : java.lang.Object {
      return claim.RelatedCompanyArray
    }
    
    // 'value' attribute on TypeKeyInput (id=IDType_Input) at AdditionalInfoInputSet.Person.pcf: line 36, column 62
    function valueRoot_3 () : java.lang.Object {
      return Person
    }
    
    // 'value' attribute on TypeKeyInput (id=IDType_Input) at AdditionalInfoInputSet.Person.pcf: line 36, column 62
    function value_1 () : typekey.ClaimantIDType {
      return Person.PersonClaimantIDType
    }
    
    // 'value' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_105 () : entity.Person {
      return Person.Guardian
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at AdditionalInfoInputSet.Person.pcf: line 160, column 150
    function value_118 () : typekey.Currency {
      return Person.PreferredCurrency
    }
    
    // 'value' attribute on TextInput (id=Occupation_Input) at AdditionalInfoInputSet.Person.pcf: line 169, column 34
    function value_124 () : java.lang.String {
      return Person.Occupation
    }
    
    // 'value' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_138 () : entity.Company {
      return Person.Employer
    }
    
    // 'value' attribute on BooleanRadioInput (id=SSNReleaseAuthorized_Input) at AdditionalInfoInputSet.Person.pcf: line 54, column 118
    function value_15 () : java.lang.Boolean {
      return Person.SSNReleaseAuthorized
    }
    
    // 'value' attribute on TextInput (id=VisaNumber_Input) at AdditionalInfoInputSet.Person.pcf: line 61, column 118
    function value_21 () : java.lang.String {
      return Person.VisaNumber
    }
    
    // 'value' attribute on TextInput (id=GreenCardNumber_Input) at AdditionalInfoInputSet.Person.pcf: line 68, column 118
    function value_27 () : java.lang.String {
      return Person.GreenCardNumber
    }
    
    // 'value' attribute on TextInput (id=PassportNumber_Input) at AdditionalInfoInputSet.Person.pcf: line 75, column 118
    function value_33 () : java.lang.String {
      return Person.PassportNumber
    }
    
    // 'value' attribute on TextInput (id=JurisdictionAssignedID_Input) at AdditionalInfoInputSet.Person.pcf: line 82, column 118
    function value_39 () : java.lang.String {
      return Person.JurisdictionAssignedID
    }
    
    // 'value' attribute on TextInput (id=EmployeeSecurityID_Input) at AdditionalInfoInputSet.Person.pcf: line 88, column 63
    function value_45 () : java.lang.String {
      return Person.EmployeeSecurityID
    }
    
    // 'value' attribute on TextInput (id=TaxExemptionsEntitled_Input) at AdditionalInfoInputSet.Person.pcf: line 97, column 63
    function value_50 () : java.math.BigDecimal {
      return Person.TaxExemptionsEntitled
    }
    
    // 'value' attribute on TypeKeyInput (id=TaxFilingStatus_Input) at AdditionalInfoInputSet.Person.pcf: line 104, column 42
    function value_56 () : typekey.TaxFilingStatusType {
      return Person.TaxFilingStatus
    }
    
    // 'value' attribute on TextInput (id=VendorNumber_Input) at AdditionalInfoInputSet.Person.pcf: line 109, column 42
    function value_62 () : java.lang.String {
      return Person.VendorNumber
    }
    
    // 'value' attribute on DateInput (id=DateOfBirth_Input) at AdditionalInfoInputSet.Person.pcf: line 117, column 42
    function value_69 () : java.util.Date {
      return Person.DateOfBirth
    }
    
    // 'value' attribute on TypeKeyInput (id=Gender_Input) at AdditionalInfoInputSet.Person.pcf: line 125, column 42
    function value_77 () : typekey.GenderType {
      return Person.Gender
    }
    
    // 'value' attribute on PrivacyInput (id=TaxID_Input) at AdditionalInfoInputSet.Person.pcf: line 47, column 42
    function value_8 () : java.lang.String {
      return Person.TaxID
    }
    
    // 'value' attribute on TypeKeyInput (id=MaritalStatus_Input) at AdditionalInfoInputSet.Person.pcf: line 132, column 97
    function value_83 () : typekey.MaritalStatus {
      return Person.MaritalStatus
    }
    
    // 'value' attribute on TextInput (id=EducationLevel_Input) at AdditionalInfoInputSet.Person.pcf: line 140, column 63
    function value_89 () : java.lang.String {
      return Person.EducationLevel
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_109 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_109 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_109 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_142 ($$arg :  entity.Company[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_142 ($$arg :  gw.api.database.IQueryBeanResult<entity.Company>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_142 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_110 () : void {
      var __valueRangeArg = claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_109(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_143 () : void {
      var __valueRangeArg = claim.RelatedCompanyArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_142(__valueRangeArg)
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Guardian_Input) at AdditionalInfoInputSet.Person.pcf: line 153, column 97
    function verifyValueType_116 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Organization_Input) at AdditionalInfoInputSet.Person.pcf: line 180, column 35
    function verifyValueType_147 () : void {
      var __valueTypeArg : entity.Company
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'visible' attribute on TypeKeyInput (id=IDType_Input) at AdditionalInfoInputSet.Person.pcf: line 36, column 62
    function visible_0 () : java.lang.Boolean {
      return claim.showClaimantInfo(Person, claimContact)
    }
    
    // 'visible' attribute on TypeKeyInput (id=Currency_Input) at AdditionalInfoInputSet.Person.pcf: line 160, column 150
    function visible_117 () : java.lang.Boolean {
      return claimContact.Claim.LossType == LossType.TC_WC7? (gw.api.util.CurrencyUtil.isMultiCurrencyMode() and isAdditionalInfoVisible): false
    }
    
    // 'visible' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_131 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Person.Employer), claim, null as List<SpecialistService>)" != "" && false
    }
    
    // 'visible' attribute on BooleanRadioInput (id=SSNReleaseAuthorized_Input) at AdditionalInfoInputSet.Person.pcf: line 54, column 118
    function visible_14 () : java.lang.Boolean {
      return claim.showClaimantInfo(Person, claimContact) and Person.PersonClaimantIDType == ClaimantIDType.TC_S
    }
    
    // 'visible' attribute on TextInput (id=VisaNumber_Input) at AdditionalInfoInputSet.Person.pcf: line 61, column 118
    function visible_20 () : java.lang.Boolean {
      return claim.showClaimantInfo(Person, claimContact) and Person.PersonClaimantIDType == ClaimantIDType.TC_E
    }
    
    // 'visible' attribute on TextInput (id=GreenCardNumber_Input) at AdditionalInfoInputSet.Person.pcf: line 68, column 118
    function visible_26 () : java.lang.Boolean {
      return claim.showClaimantInfo(Person, claimContact) and Person.PersonClaimantIDType == ClaimantIDType.TC_G
    }
    
    // 'visible' attribute on TextInput (id=PassportNumber_Input) at AdditionalInfoInputSet.Person.pcf: line 75, column 118
    function visible_32 () : java.lang.Boolean {
      return claim.showClaimantInfo(Person, claimContact) and Person.PersonClaimantIDType == ClaimantIDType.TC_P
    }
    
    // 'visible' attribute on TextInput (id=JurisdictionAssignedID_Input) at AdditionalInfoInputSet.Person.pcf: line 82, column 118
    function visible_38 () : java.lang.Boolean {
      return claim.showClaimantInfo(Person, claimContact) and Person.PersonClaimantIDType == ClaimantIDType.TC_A
    }
    
    // 'visible' attribute on PrivacyInput (id=TaxID_Input) at AdditionalInfoInputSet.Person.pcf: line 47, column 42
    function visible_6 () : java.lang.Boolean {
      return isAdditionalInfoVisible
    }
    
    // 'visible' attribute on TypeKeyInput (id=MaritalStatus_Input) at AdditionalInfoInputSet.Person.pcf: line 132, column 97
    function visible_82 () : java.lang.Boolean {
      return claimContact.Claim.LossType == LossType.TC_WC7? isAdditionalInfoVisible: false
    }
    
    // 'visible' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_94 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=Guardian_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_97 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(entity.Person.Type, claim, null as List<SpecialistService>)" != "" && false
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get claimContact () : ClaimContact {
      return getRequireValue("claimContact", 0) as ClaimContact
    }
    
    property set claimContact ($arg :  ClaimContact) {
      setRequireValue("claimContact", 0, $arg)
    }
    
    property get contactHandle () : gw.api.contact.ContactHandle {
      return getRequireValue("contactHandle", 0) as gw.api.contact.ContactHandle
    }
    
    property set contactHandle ($arg :  gw.api.contact.ContactHandle) {
      setRequireValue("contactHandle", 0, $arg)
    }
    
    property get isAdditionalInfoVisible () : boolean {
      return getRequireValue("isAdditionalInfoVisible", 0) as java.lang.Boolean
    }
    
    property set isAdditionalInfoVisible ($arg :  boolean) {
      setRequireValue("isAdditionalInfoVisible", 0, $arg)
    }
    
    
    property get Person() : Person { return contactHandle.Contact as Person; }
        
    
    
  }
  
  
}