package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/ASRecordDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ASRecordDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/ASRecordDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ASRecordDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (ASRecord :  AssignStrategy_TDIC) : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at ASRecordDetail.pcf: line 15, column 45
    function def_onEnter_0 (def :  pcf.ASRecordDetailScreen) : void {
      def.onEnter(ASRecord)
    }
    
    // 'def' attribute on ScreenRef at ASRecordDetail.pcf: line 15, column 45
    function def_refreshVariables_1 (def :  pcf.ASRecordDetailScreen) : void {
      def.refreshVariables(ASRecord)
    }
    
    // 'parent' attribute on Page (id=ASRecordDetail) at ASRecordDetail.pcf: line 8, column 124
    static function parent_2 (ASRecord :  AssignStrategy_TDIC) : pcf.api.Destination {
      return pcf.AssignmentStrategy_TDIC.createDestination()
    }
    
    // 'title' attribute on Page (id=ASRecordDetail) at ASRecordDetail.pcf: line 8, column 124
    static function title_3 (ASRecord :  AssignStrategy_TDIC) : java.lang.Object {
      return DisplayKey.get("TDIC.Web.Admin.ASRecordDetail", ASRecord.AssignTo+", "+ASRecord.GroupName)
    }
    
    property get ASRecord () : entity.AssignStrategy_TDIC {
      return getVariableValue("ASRecord", 0) as entity.AssignStrategy_TDIC
    }
    
    property set ASRecord ($arg :  entity.AssignStrategy_TDIC) {
      setVariableValue("ASRecord", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.ASRecordDetail {
      return super.CurrentLocation as pcf.ASRecordDetail
    }
    
    
  }
  
  
}