package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_ContactBasicsDV.Person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_ContactBasicsDV_PersonExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_ContactBasicsDV.Person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_ContactBasicsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 36, column 96
    function def_onEnter_0 (def :  pcf.ContactBasicsHeaderInputSet) : void {
      def.onEnter(contactHandle, ClaimContact, showRoles, linkStatus)
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 55, column 32
    function def_onEnter_11 (def :  pcf.TDIC_AdditionalInfoInputSet_Adjudicator) : void {
      def.onEnter(contactHandle, ClaimContact, claim, contactRole, isAdditionalInfoVisible_TDIC())
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 55, column 32
    function def_onEnter_13 (def :  pcf.TDIC_AdditionalInfoInputSet_Person) : void {
      def.onEnter(contactHandle, ClaimContact, claim, contactRole, isAdditionalInfoVisible_TDIC())
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 55, column 32
    function def_onEnter_15 (def :  pcf.TDIC_AdditionalInfoInputSet_PersonVendor) : void {
      def.onEnter(contactHandle, ClaimContact, claim, contactRole, isAdditionalInfoVisible_TDIC())
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 55, column 32
    function def_onEnter_17 (def :  pcf.TDIC_AdditionalInfoInputSet_UserContact) : void {
      def.onEnter(contactHandle, ClaimContact, claim, contactRole, isAdditionalInfoVisible_TDIC())
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 40, column 50
    function def_onEnter_2 (def :  pcf.PersonNameInputSet) : void {
      def.onEnter(contactHandle)
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 58, column 85
    function def_onEnter_21 (def :  pcf.DriversLicenseInfoInputSet) : void {
      def.onEnter(contactHandle)
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 43, column 50
    function def_onEnter_5 (def :  pcf.TDIC_PersonContactInfoInputSet) : void {
      def.onEnter(contactHandle, ClaimContact, contactRole)
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 47, column 55
    function def_onEnter_8 (def :  pcf.TDIC_PrimaryAddressInputSet) : void {
      def.onEnter(contactHandle, contactRole)
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 36, column 96
    function def_refreshVariables_1 (def :  pcf.ContactBasicsHeaderInputSet) : void {
      def.refreshVariables(contactHandle, ClaimContact, showRoles, linkStatus)
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 55, column 32
    function def_refreshVariables_12 (def :  pcf.TDIC_AdditionalInfoInputSet_Adjudicator) : void {
      def.refreshVariables(contactHandle, ClaimContact, claim, contactRole, isAdditionalInfoVisible_TDIC())
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 55, column 32
    function def_refreshVariables_14 (def :  pcf.TDIC_AdditionalInfoInputSet_Person) : void {
      def.refreshVariables(contactHandle, ClaimContact, claim, contactRole, isAdditionalInfoVisible_TDIC())
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 55, column 32
    function def_refreshVariables_16 (def :  pcf.TDIC_AdditionalInfoInputSet_PersonVendor) : void {
      def.refreshVariables(contactHandle, ClaimContact, claim, contactRole, isAdditionalInfoVisible_TDIC())
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 55, column 32
    function def_refreshVariables_18 (def :  pcf.TDIC_AdditionalInfoInputSet_UserContact) : void {
      def.refreshVariables(contactHandle, ClaimContact, claim, contactRole, isAdditionalInfoVisible_TDIC())
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 58, column 85
    function def_refreshVariables_22 (def :  pcf.DriversLicenseInfoInputSet) : void {
      def.refreshVariables(contactHandle)
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 40, column 50
    function def_refreshVariables_3 (def :  pcf.PersonNameInputSet) : void {
      def.refreshVariables(contactHandle)
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 43, column 50
    function def_refreshVariables_6 (def :  pcf.TDIC_PersonContactInfoInputSet) : void {
      def.refreshVariables(contactHandle, ClaimContact, contactRole)
    }
    
    // 'def' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 47, column 55
    function def_refreshVariables_9 (def :  pcf.TDIC_PrimaryAddressInputSet) : void {
      def.refreshVariables(contactHandle, contactRole)
    }
    
    // 'value' attribute on TextAreaInput (id=Notes_Input) at TDIC_ContactBasicsDV.Person.pcf: line 66, column 31
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.Notes = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'mode' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 55, column 32
    function mode_19 () : java.lang.Object {
      return Person.Subtype
    }
    
    // 'value' attribute on TextAreaInput (id=Notes_Input) at TDIC_ContactBasicsDV.Person.pcf: line 66, column 31
    function valueRoot_25 () : java.lang.Object {
      return Person
    }
    
    // 'value' attribute on TextAreaInput (id=Notes_Input) at TDIC_ContactBasicsDV.Person.pcf: line 66, column 31
    function value_23 () : java.lang.String {
      return Person.Notes
    }
    
    // 'visible' attribute on Label at TDIC_ContactBasicsDV.Person.pcf: line 52, column 51
    function visible_10 () : java.lang.Boolean {
      return isAdditionalInfoVisible_TDIC()
    }
    
    // 'visible' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 58, column 85
    function visible_20 () : java.lang.Boolean {
      return Person.Subtype==TC_PERSON and isDriversLicenseInfoVisible_TDIC()
    }
    
    // 'visible' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 43, column 50
    function visible_4 () : java.lang.Boolean {
      return !(Person typeis PersonVendor)
    }
    
    // 'visible' attribute on InputSetRef at TDIC_ContactBasicsDV.Person.pcf: line 47, column 55
    function visible_7 () : java.lang.Boolean {
      return isPrimaryAddressInfoVisible_TDIC()
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get contactHandle () : gw.api.contact.ContactHandle {
      return getRequireValue("contactHandle", 0) as gw.api.contact.ContactHandle
    }
    
    property set contactHandle ($arg :  gw.api.contact.ContactHandle) {
      setRequireValue("contactHandle", 0, $arg)
    }
    
    property get contactRole () : typekey.ContactRole {
      return getRequireValue("contactRole", 0) as typekey.ContactRole
    }
    
    property set contactRole ($arg :  typekey.ContactRole) {
      setRequireValue("contactRole", 0, $arg)
    }
    
    property get linkStatus () : gw.api.contact.ContactSystemLinkStatus {
      return getRequireValue("linkStatus", 0) as gw.api.contact.ContactSystemLinkStatus
    }
    
    property set linkStatus ($arg :  gw.api.contact.ContactSystemLinkStatus) {
      setRequireValue("linkStatus", 0, $arg)
    }
    
    property get showRoles () : boolean {
      return getRequireValue("showRoles", 0) as java.lang.Boolean
    }
    
    property set showRoles ($arg :  boolean) {
      setRequireValue("showRoles", 0, $arg)
    }
    
    property get Person() : Person { return contactHandle.Contact as Person; }
          
    property get ClaimContact() : ClaimContact { return contactHandle typeis ClaimContact ? (contactHandle) : null; }
    
    /**
     * US538
     * 09/10/2014 robk
     */
    function isAdditionalInfoVisible_TDIC() : boolean {
      return contactRole != typekey.ContactRole.TC_MAINCONTACT and contactRole != typekey.ContactRole.TC_REPORTER and contactHandle.Contact typeis Person
    }
    
    /**
     * US538
     * 09/10/2014 robk
     */
    function isDriversLicenseInfoVisible_TDIC() : boolean {
      return contactRole != typekey.ContactRole.TC_REPORTER and contactRole != typekey.ContactRole.TC_MAINCONTACT and contactRole != typekey.ContactRole.TC_INJURED and contactRole != typekey.ContactRole.TC_CLAIMANT
    }
    
    /**
     * US538
     * 09/10/2014 robk
     */
    function isPrimaryAddressInfoVisible_TDIC(): boolean {
      return contactRole != typekey.ContactRole.TC_MAINCONTACT and contactRole != typekey.ContactRole.TC_REPORTER
    }
    
    
  }
  
  
}