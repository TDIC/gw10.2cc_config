package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsContactScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseShareDocumentsContactScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsContactScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseShareDocumentsContactScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_MedicalCareOrg) at ShareBaseShareDocumentsContactScreen.pcf: line 65, column 86
    function action_11 () : void {
      ShareBaseShareDocumentsNewContactPopup.push(claim, typekey.Contact.TC_MEDICALCAREORG, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_CompanyVendor) at ShareBaseShareDocumentsContactScreen.pcf: line 69, column 85
    function action_13 () : void {
      ShareBaseShareDocumentsNewContactPopup.push(claim, typekey.Contact.TC_COMPANYVENDOR, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Company) at ShareBaseShareDocumentsContactScreen.pcf: line 74, column 77
    function action_15 () : void {
      ShareBaseShareDocumentsNewContactPopup.push(claim, typekey.Contact.TC_COMPANY, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Adjudicator) at ShareBaseShareDocumentsContactScreen.pcf: line 81, column 83
    function action_17 () : void {
      ShareBaseShareDocumentsNewContactPopup.push(claim, typekey.Contact.TC_ADJUDICATOR, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Attorney) at ShareBaseShareDocumentsContactScreen.pcf: line 85, column 80
    function action_19 () : void {
      ShareBaseShareDocumentsNewContactPopup.push(claim, typekey.Contact.TC_ATTORNEY, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LawFirm) at ShareBaseShareDocumentsContactScreen.pcf: line 89, column 79
    function action_21 () : void {
      ShareBaseShareDocumentsNewContactPopup.push(claim, typekey.Contact.TC_LAWFIRM, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LegalVenue) at ShareBaseShareDocumentsContactScreen.pcf: line 93, column 82
    function action_23 () : void {
      ShareBaseShareDocumentsNewContactPopup.push(claim, typekey.Contact.TC_LEGALVENUE, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_NewPerson) at ShareBaseShareDocumentsContactScreen.pcf: line 46, column 76
    function action_3 () : void {
      ShareBaseShareDocumentsNewContactPopup.push(claim, typekey.Contact.TC_PERSON, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_AutoRepairShop) at ShareBaseShareDocumentsContactScreen.pcf: line 53, column 86
    function action_5 () : void {
      ShareBaseShareDocumentsNewContactPopup.push(claim, typekey.Contact.TC_AUTOREPAIRSHOP, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_AutoTowingAgcy) at ShareBaseShareDocumentsContactScreen.pcf: line 57, column 86
    function action_7 () : void {
      ShareBaseShareDocumentsNewContactPopup.push(claim, typekey.Contact.TC_AUTOTOWINGAGCY, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Doctor) at ShareBaseShareDocumentsContactScreen.pcf: line 61, column 78
    function action_9 () : void {
      ShareBaseShareDocumentsNewContactPopup.push(claim, typekey.Contact.TC_DOCTOR, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Doctor) at ShareBaseShareDocumentsContactScreen.pcf: line 61, column 78
    function action_dest_10 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsNewContactPopup.createDestination(claim, typekey.Contact.TC_DOCTOR, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_MedicalCareOrg) at ShareBaseShareDocumentsContactScreen.pcf: line 65, column 86
    function action_dest_12 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsNewContactPopup.createDestination(claim, typekey.Contact.TC_MEDICALCAREORG, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_CompanyVendor) at ShareBaseShareDocumentsContactScreen.pcf: line 69, column 85
    function action_dest_14 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsNewContactPopup.createDestination(claim, typekey.Contact.TC_COMPANYVENDOR, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Company) at ShareBaseShareDocumentsContactScreen.pcf: line 74, column 77
    function action_dest_16 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsNewContactPopup.createDestination(claim, typekey.Contact.TC_COMPANY, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Adjudicator) at ShareBaseShareDocumentsContactScreen.pcf: line 81, column 83
    function action_dest_18 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsNewContactPopup.createDestination(claim, typekey.Contact.TC_ADJUDICATOR, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Attorney) at ShareBaseShareDocumentsContactScreen.pcf: line 85, column 80
    function action_dest_20 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsNewContactPopup.createDestination(claim, typekey.Contact.TC_ATTORNEY, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LawFirm) at ShareBaseShareDocumentsContactScreen.pcf: line 89, column 79
    function action_dest_22 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsNewContactPopup.createDestination(claim, typekey.Contact.TC_LAWFIRM, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LegalVenue) at ShareBaseShareDocumentsContactScreen.pcf: line 93, column 82
    function action_dest_24 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsNewContactPopup.createDestination(claim, typekey.Contact.TC_LEGALVENUE, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_NewPerson) at ShareBaseShareDocumentsContactScreen.pcf: line 46, column 76
    function action_dest_4 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsNewContactPopup.createDestination(claim, typekey.Contact.TC_PERSON, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_AutoRepairShop) at ShareBaseShareDocumentsContactScreen.pcf: line 53, column 86
    function action_dest_6 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsNewContactPopup.createDestination(claim, typekey.Contact.TC_AUTOREPAIRSHOP, share)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_AutoTowingAgcy) at ShareBaseShareDocumentsContactScreen.pcf: line 57, column 86
    function action_dest_8 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsNewContactPopup.createDestination(claim, typekey.Contact.TC_AUTOTOWINGAGCY, share)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=UnlinkContacts) at ShareBaseShareDocumentsContactScreen.pcf: line 38, column 110
    function checkedRowAction_2 (element :  entity.Contact, CheckedValue :  entity.Contact) : void {
      share.removeRecipient((CheckedValue as Contact))
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=linkContactsOG) at ShareBaseShareDocumentsContactScreen.pcf: line 109, column 107
    function checkedRowAction_27 (element :  entity.Contact, CheckedValue :  entity.Contact) : void {
      share.addRecipient(CheckedValue as Contact)
    }
    
    // 'def' attribute on PanelRef (id=selectedContacts) at ShareBaseShareDocumentsContactScreen.pcf: line 30, column 29
    function def_onEnter_25 (def :  pcf.ShareBaseFolderRequestContactsLV) : void {
      def.onEnter(claim, share.LinkedRecipients,true)
    }
    
    // 'def' attribute on PanelRef (id=availableContacts) at ShareBaseShareDocumentsContactScreen.pcf: line 101, column 30
    function def_onEnter_28 (def :  pcf.ShareBaseFolderAvailableContactsLV) : void {
      def.onEnter(claim,share.ContactsNotLinked)
    }
    
    // 'def' attribute on PanelRef (id=selectedContacts) at ShareBaseShareDocumentsContactScreen.pcf: line 30, column 29
    function def_refreshVariables_26 (def :  pcf.ShareBaseFolderRequestContactsLV) : void {
      def.refreshVariables(claim, share.LinkedRecipients,true)
    }
    
    // 'def' attribute on PanelRef (id=availableContacts) at ShareBaseShareDocumentsContactScreen.pcf: line 101, column 30
    function def_refreshVariables_29 (def :  pcf.ShareBaseFolderAvailableContactsLV) : void {
      def.refreshVariables(claim,share.ContactsNotLinked)
    }
    
    // 'label' attribute on AlertBar (id=NoUserEmail) at ShareBaseShareDocumentsContactScreen.pcf: line 27, column 70
    function label_1 () : java.lang.Object {
      return DisplayKey.get("Accelerator.OnBase.ShareBase.STR_GW_WarningNoEmailForUser", User.util.CurrentUser.Contact.DisplayName)
    }
    
    // 'visible' attribute on AlertBar (id=NoUserEmail) at ShareBaseShareDocumentsContactScreen.pcf: line 27, column 70
    function visible_0 () : java.lang.Boolean {
      return User.util.CurrentUser.Contact.EmailAddress1 == null
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get share () : OutboundSharing_Ext {
      return getRequireValue("share", 0) as OutboundSharing_Ext
    }
    
    property set share ($arg :  OutboundSharing_Ext) {
      setRequireValue("share", 0, $arg)
    }
    
    
  }
  
  
}