package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/newclaim/NewClaimSavedDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewClaimSavedDVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/newclaim/NewClaimSavedDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewClaimSavedDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=ServiceRequestNumber_Cell) at NewClaimSavedDV.pcf: line 62, column 62
    function action_21 () : void {
      pcf.ClaimServiceRequests.go(claim, serviceRequest)
    }
    
    // 'action' attribute on TextCell (id=ServiceRequestNumber_Cell) at NewClaimSavedDV.pcf: line 62, column 62
    function action_dest_22 () : pcf.api.Destination {
      return pcf.ClaimServiceRequests.createDestination(claim, serviceRequest)
    }
    
    // 'iconLabel' attribute on BooleanRadioCell (id=Kind_Cell) at NewClaimSavedDV.pcf: line 56, column 38
    function iconLabel_19 () : java.lang.String {
      return serviceRequest.Kind.Label
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Kind_Cell) at NewClaimSavedDV.pcf: line 56, column 38
    function icon_20 () : java.lang.String {
      return serviceRequest.Kind.Icon
    }
    
    // 'value' attribute on TextCell (id=ServiceRequestNumber_Cell) at NewClaimSavedDV.pcf: line 62, column 62
    function valueRoot_24 () : java.lang.Object {
      return serviceRequest
    }
    
    // 'value' attribute on TextCell (id=ServiceRequestNumber_Cell) at NewClaimSavedDV.pcf: line 62, column 62
    function value_23 () : java.lang.String {
      return serviceRequest.ServiceRequestNumber
    }
    
    // 'value' attribute on TextCell (id=RelatedTo_Cell) at NewClaimSavedDV.pcf: line 67, column 55
    function value_26 () : java.lang.String {
      return serviceRequest.RelatedToName
    }
    
    // 'value' attribute on TextCell (id=Services_Cell) at NewClaimSavedDV.pcf: line 71, column 56
    function value_29 () : java.lang.String {
      return serviceRequest.ServicesString
    }
    
    // 'value' attribute on TextCell (id=Specialist_Cell) at NewClaimSavedDV.pcf: line 76, column 56
    function value_32 () : java.lang.String {
      return serviceRequest.SpecialistName
    }
    
    // 'value' attribute on DateCell (id=RequestedCompletionDate_Cell) at NewClaimSavedDV.pcf: line 80, column 150
    function value_35 () : java.util.Date {
      return serviceRequest.quoteAllowed() ? serviceRequest.RequestedQuoteCompletionDate : serviceRequest.RequestedServiceCompletionDate
    }
    
    property get serviceRequest () : entity.ServiceRequest {
      return getIteratedValue(1) as entity.ServiceRequest
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/newclaim/NewClaimSavedDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewClaimSavedDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on BulletPointTextInput (id=GoToClaim_Input) at NewClaimSavedDV.pcf: line 94, column 213
    function action_39 () : void {
      pcf.Claim.go(claim)
    }
    
    // 'action' attribute on BulletPointTextInput (id=CreateNewClaim_Input) at NewClaimSavedDV.pcf: line 101, column 42
    function action_44 () : void {
      FNOLWizard.go()
    }
    
    // 'action' attribute on BulletPointTextInput (id=CreateReserve_Input) at NewClaimSavedDV.pcf: line 108, column 42
    function action_48 () : void {
      NewReserveSet.go(claim)
    }
    
    // 'action' attribute on BulletPointTextInput (id=CreatePayment_Input) at NewClaimSavedDV.pcf: line 115, column 42
    function action_52 () : void {
      NormalCreateCheckWizardForward.go(claim)
    }
    
    // 'action' attribute on BulletPointTextInput (id=GoToClaim_Input) at NewClaimSavedDV.pcf: line 94, column 213
    function action_dest_40 () : pcf.api.Destination {
      return pcf.Claim.createDestination(claim)
    }
    
    // 'action' attribute on BulletPointTextInput (id=CreateNewClaim_Input) at NewClaimSavedDV.pcf: line 101, column 42
    function action_dest_45 () : pcf.api.Destination {
      return pcf.FNOLWizard.createDestination()
    }
    
    // 'action' attribute on BulletPointTextInput (id=CreateReserve_Input) at NewClaimSavedDV.pcf: line 108, column 42
    function action_dest_49 () : pcf.api.Destination {
      return pcf.NewReserveSet.createDestination(claim)
    }
    
    // 'action' attribute on BulletPointTextInput (id=CreatePayment_Input) at NewClaimSavedDV.pcf: line 115, column 42
    function action_dest_53 () : pcf.api.Destination {
      return pcf.NormalCreateCheckWizardForward.createDestination(claim)
    }
    
    // 'label' attribute on Label (id=Header) at NewClaimSavedDV.pcf: line 13, column 98
    function label_0 () : java.lang.String {
      return DisplayKey.get("Web.NewClaimWizard.Saved.Header",  claim.ClaimNumber)
    }
    
    // 'value' attribute on BooleanRadioCell (id=Kind_Cell) at NewClaimSavedDV.pcf: line 56, column 38
    function sortValue_13 (serviceRequest :  entity.ServiceRequest) : java.lang.Object {
      return true
    }
    
    // 'value' attribute on TextCell (id=ServiceRequestNumber_Cell) at NewClaimSavedDV.pcf: line 62, column 62
    function sortValue_14 (serviceRequest :  entity.ServiceRequest) : java.lang.Object {
      return serviceRequest.ServiceRequestNumber
    }
    
    // 'value' attribute on TextCell (id=RelatedTo_Cell) at NewClaimSavedDV.pcf: line 67, column 55
    function sortValue_15 (serviceRequest :  entity.ServiceRequest) : java.lang.Object {
      return serviceRequest.RelatedToName
    }
    
    // 'value' attribute on TextCell (id=Services_Cell) at NewClaimSavedDV.pcf: line 71, column 56
    function sortValue_16 (serviceRequest :  entity.ServiceRequest) : java.lang.Object {
      return serviceRequest.ServicesString
    }
    
    // 'value' attribute on TextCell (id=Specialist_Cell) at NewClaimSavedDV.pcf: line 76, column 56
    function sortValue_17 (serviceRequest :  entity.ServiceRequest) : java.lang.Object {
      return serviceRequest.SpecialistName
    }
    
    // 'value' attribute on DateCell (id=RequestedCompletionDate_Cell) at NewClaimSavedDV.pcf: line 80, column 150
    function sortValue_18 (serviceRequest :  entity.ServiceRequest) : java.lang.Object {
      return serviceRequest.quoteAllowed() ? serviceRequest.RequestedQuoteCompletionDate : serviceRequest.RequestedServiceCompletionDate
    }
    
    // 'value' attribute on TextInput (id=AssignedGroup_Input) at NewClaimSavedDV.pcf: line 17, column 107
    function value_1 () : java.lang.String {
      return DisplayKey.get("Web.NewClaimWizard.Saved.AssignedGroup",  claim.AssignedGroup)
    }
    
    // 'value' attribute on RowIterator at NewClaimSavedDV.pcf: line 46, column 49
    function value_37 () : entity.ServiceRequest[] {
      return claim.ServiceRequests
    }
    
    // 'value' attribute on TextInput (id=AssignedUser_Input) at NewClaimSavedDV.pcf: line 22, column 87
    function value_4 () : java.lang.String {
      return DisplayKey.get("Web.NewClaimWizard.Saved.AssignedUser",  claim.AssignedUser)
    }
    
    // 'value' attribute on BulletPointTextInput (id=GoToClaim_Input) at NewClaimSavedDV.pcf: line 94, column 213
    function value_41 () : java.lang.String {
      return claim.Policy.Verified ? DisplayKey.get("Web.NewClaimWizard.Saved.GoToClaim", claim.ClaimNumber) : DisplayKey.get("TDIC.Web.NewClaimWizard.Saved.GoToClaim", claim.ClaimNumber)
    }
    
    // 'value' attribute on TextInput (id=AssignedQueue_TDIC_Input) at NewClaimSavedDV.pcf: line 27, column 88
    function value_8 () : java.lang.String {
      return DisplayKey.get("TDIC.Web.NewClaimWizard.Saved.AssignedQueue",  claim.AssignedQueue)
    }
    
    // 'visible' attribute on TextInput (id=PendingAssignment_Input) at NewClaimSavedDV.pcf: line 32, column 58
    function visible_11 () : java.lang.Boolean {
      return claim.AssignmentStatus != TC_ASSIGNED
    }
    
    // 'visible' attribute on TextInput (id=AssignedUser_Input) at NewClaimSavedDV.pcf: line 22, column 87
    function visible_3 () : java.lang.Boolean {
      return claim.AssignmentStatus == TC_ASSIGNED and claim.AssignedUser!=null
    }
    
    // 'visible' attribute on ListViewInput at NewClaimSavedDV.pcf: line 39, column 53
    function visible_38 () : java.lang.Boolean {
      return not claim.ServiceRequests.IsEmpty
    }
    
    // 'visible' attribute on BulletPointTextInput (id=CreateNewClaim_Input) at NewClaimSavedDV.pcf: line 101, column 42
    function visible_43 () : java.lang.Boolean {
      return claim.Policy.Verified
    }
    
    // 'visible' attribute on TextInput (id=AssignedQueue_TDIC_Input) at NewClaimSavedDV.pcf: line 27, column 88
    function visible_7 () : java.lang.Boolean {
      return claim.AssignmentStatus == TC_ASSIGNED and claim.AssignedQueue!=null
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    
  }
  
  
}