package gw.plugin.pcintegration.pc1000.mapping

uses java.math.BigDecimal

/**
 * * GINTEG-1178 : This DTO will hold Legacy Cov data from Legacy Data Mart
 */
class BOPLegacyCovDataMapper_TDIC {

  var covpatterncode : String as COVPATTERNCODE
  var publicid : String as PUBLICID
  var locationid : String as LOCATIONID
  var lob : String as LOB
  var polnbr : String as POLNBR
  var prpnbr : int as PRPNBR
  var source_system : String as SOURCE_SYSTEM
  var effectivedate : 	Date as EFFECTIVEDATE
  var expirationdate : Date as EXPIRATIONDATE
  var covTerms : List<BOPLegacyCovTermDataMap_TDIC> as COVTERMS

}