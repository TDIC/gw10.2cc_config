package gw.plugin.pcintegration.pc1000.mapping

/**
 *
 */
class BOPLegacyCovTermDataMap_TDIC {

  var covTermPatterncode : String as COVTERMPATTERNCODE
  var covTermValue : String as COVTERMVALUE
  var covTermDateValue : String as COVTERMDATEVALUE
  var covTermStringValue : String as COVTERMSTRINGVALUE

}