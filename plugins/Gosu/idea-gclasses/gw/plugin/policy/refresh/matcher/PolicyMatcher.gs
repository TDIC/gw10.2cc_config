package gw.plugin.policy.refresh.matcher


/**
 * EntityMatcher for Policy entities.
 */
@Export
class PolicyMatcher extends PolicyGraphMatcherBase<Policy>  {

  /**
   * Will always return true, because you can select a different policy (i.e., different policy number)
   * that can be compared.
   */
  override function doEntitiesMatch(sourcePolicy : Policy, comparePolicy : Policy) : boolean {
    if (sourcePolicy.Offerings_TDIC != null) {
      comparePolicy.Offerings_TDIC = sourcePolicy.Offerings_TDIC
    }
    if (not sourcePolicy.PolicyNumber.Numeric) {
      if (sourcePolicy.EffectiveDate != null) {
        comparePolicy.EffectiveDate = sourcePolicy.EffectiveDate
      }
      if (sourcePolicy.ExpirationDate != null) {
        comparePolicy.ExpirationDate = sourcePolicy.ExpirationDate
      }
      if (sourcePolicy.CancellationDate != null) {
        comparePolicy.CancellationDate = sourcePolicy.CancellationDate
      }
      if (sourcePolicy.OrigEffectiveDate != null) {
        comparePolicy.OrigEffectiveDate = sourcePolicy.OrigEffectiveDate
      }
      if (sourcePolicy.ERE_TDIC != null) {
        comparePolicy.ERE_TDIC = sourcePolicy.ERE_TDIC
      }
      if (sourcePolicy.PriorActs_TDIC != null) {
        comparePolicy.PriorActs_TDIC = sourcePolicy.PriorActs_TDIC
      }
    }
    return true
  }

}
