package gw.claim.indicator

uses gw.api.claim.indicator.ClaimIndicatorMethodsImpl
uses gw.api.locale.DisplayKey

/**
 * GCC 15
 * create by: NattuduraiK
 * @description: A class for Coverage Opinion Claim Indicator
 * @create time: 09:09 PM 10/25/2019
 */

class CoverageOpinionClaimIndicatorMethodsImpl extends ClaimIndicatorMethodsImpl {

  construct(inIndicator : CoverageOpinionClaimIndicator) {
    super(inIndicator, "coverage_in_question")
  }

  /**
   * Update, sets indicator on if the claim's CoverageInOpinion field is true
   */
  override function update() {
    setOn(Indicator.Claim.CoverageOpinion_TDIC)
  }

  /**
   * Text label, just has two values depending on whether indicator is on or off
   */
  override property get Text() : String {
    return Indicator.IsOn ? DisplayKey.get("TDIC_Web.Claim.CoverageOpinionClaimIndicator.CoverageOpinion.OnText")
        : null
  }

  /**
   * This indicator does not have any hover text
   */
  override property get HoverText() : String {
    return Indicator.IsOn ?
        DisplayKey.get("TDIC_Web.Claim.CoverageOpinionClaimIndicator.CoverageOpinion.OnText") : null
  }
}