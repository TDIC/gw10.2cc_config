package gw.claim

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.financials.CurrencyAmount
uses gw.api.util.CurrencyUtil
uses gw.financials.CoverageLimitRulesUtil

enhancement ClaimEnhancement : Claim {
  /**
   * Find exposure limits
   * @param covType
   * @param costType
   * @param exposure
   * @return
   */
  function calculateCombinedLimit(costType:CostType,exposure:Exposure,costcategory:CostCategory):CurrencyAmount{

    var clmsQry = Query.make(Claim).compare(Claim#State, Relop.Equals,ClaimState.TC_OPEN)
                    .join("Policy").compare(Policy#PolicyNumber, Relop.Equals,this.Policy.PolicyNumber)
                    .compare(Policy#EffectiveDate, Relop.Equals, this.Policy.EffectiveDate)
                    .compare(Policy#ExpirationDate, Relop.Equals, this.Policy.ExpirationDate).select()
    var exposureSet = new java.util.HashSet<Exposure>()
    for (clm in clmsQry){
      var exp = clm.Exposures.firstWhere(\elt -> elt.ExposureType == exposure.ExposureType)
      if(exp != null ){
        exposureSet.add(exp)
      }
    }
    var totalCombined : CurrencyAmount = null
    for (exps in exposureSet) {
      if (exps.HasCoverageLimit) {
        var exposureLimit = exps.Coverage.ExposureLimit

        var compareInLimitCurrency = CoverageLimitRulesUtil.shouldCompareInReservingCurrency({exps}, costType, exposureLimit.Currency)
        if (not compareInLimitCurrency) {
          exposureLimit = exposureLimit.convert(this.Currency, java.math.RoundingMode.UP)
        }
        var combinedCalc = CoverageLimitRulesUtil.getCommittedAndFutureAndPendingApprovalPaymentsAndReserves()
            .withExposure(exps)
            .withCostType(costType)
            .withCostCategory(costcategory)
        var combined = CoverageLimitRulesUtil.getCalcValueForCompare(
            combinedCalc,
            exposureLimit.Currency, compareInLimitCurrency)
        if(totalCombined != null )
         totalCombined = combined+totalCombined
        else
          totalCombined=combined
      }
    }
   return totalCombined
  }

  /**
   * Find exposure limits
   * @param covType
   * @param exposure
   * @return
   */
  function calculateCombinedLimit(costType:CostType, exposure:Exposure):CurrencyAmount{
    var clmsQry = Query.make(Claim).compare(Claim#State, Relop.NotEquals,ClaimState.TC_DRAFT)
        .join("Policy").compare(Policy#PolicyNumber, Relop.Equals,this.Policy.PolicyNumber)
        .compare(Policy#EffectiveDate, Relop.Equals, this.Policy.EffectiveDate)
        .compare(Policy#ExpirationDate, Relop.Equals, this.Policy.ExpirationDate).select()
    var exposureSet = new java.util.HashSet<Exposure>()
    for (clm in clmsQry){
      var exp = clm.Exposures.firstWhere(\elt -> elt.ExposureType == exposure.ExposureType)
      if(exp != null ){
        exposureSet.add(exp)
      }
    }
    var totalCombined : CurrencyAmount = null
    for (exps in exposureSet) {
      if (exps.HasCoverageLimit) {
        var exposureLimit = exps.Coverage.ExposureLimit

        var compareInLimitCurrency = CoverageLimitRulesUtil.shouldCompareInReservingCurrency({exps}, costType, exposureLimit.Currency)
        if (not compareInLimitCurrency) {
          exposureLimit = exposureLimit.convert(this.Currency, java.math.RoundingMode.UP)
        }
        var combinedCalc = CoverageLimitRulesUtil.getCommittedAndFutureAndPendingApprovalPaymentsAndReserves()
            .withExposure(exps)
            .withCostType(costType)
        var combined = CoverageLimitRulesUtil.getCalcValueForCompare(
            combinedCalc,
            exposureLimit.Currency, compareInLimitCurrency)
        if(totalCombined != null )
          totalCombined = combined+totalCombined
        else
          totalCombined=combined
      }
    }
    return totalCombined
  }
}
