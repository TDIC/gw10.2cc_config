package gw.webservice.cc.cc1000.pcintegration.pcentities

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.xml.ws.annotation.WsiExportable

uses java.math.BigDecimal

uses typekey.Currency

@WsiExportable("http://guidewire.com/cc/ws/gw/webservice/cc/cc1000/pcintegration/pcentities/PCClaim")
@Export
final class PCClaim {

  var _claimNumber : String as ClaimNumber
  var _claimPublicId : String as ClaimPublicID
  var _lossDate : Date as LossDate
  var _policyNumber : String as PolicyNumber
  var _policyTypeName : String as PolicyTypeName
  var _totalIncurred : BigDecimal as TotalIncurred
  var _totalIncurredCurrency : Currency as TotalIncurredCurrency
  var _remainingReserves : BigDecimal as RemainingReserves
  var _remainingReservesCurrency : Currency as RemainingReservesCurrency
  var _claimSecurityType : String as ClaimSecurityType
  var _litigationIndicator : Boolean as LitigationIndicator
  var _fraudIndicator : Boolean as FraudIndicator
  var _largeLossIndicator : Boolean as LargeLossIndicator
  var _status : String as Status
  var _lossType : String as LossType
  var _adjuster : PCAdjuster as Adjuster

  // need to be able to access adjuster's and claim rep's name from PC for TDIC.
  var _adjusterName : String as AdjusterName
  // US977, robk
  var _insuredName : String as InsuredName

  // Need Claim closed date for Risk Analysis
  var _closedDate : Date as ClosedDate

  //WCSTAT fields

  var _classificationCode : String as ClassificationCode
  var _weeklyWageAmount :  long as WeeklyWageAmount
  var _injuryCode : String as InjuryCode
  var _catastropheNumber : int as CatastropheNumber
  var _incurredIndemnityAmount : long as IncurredIndemnityAmount
  var _incurredMedicalAmount : long as IncurredMedicalAmount
  var _typeOfLossCode : String as TypeOfLossCode
  var _typeOfSettlementCode : String as TypeOfSettlementCode
  var _totalIncurredVocRehabAmnt : long as TotalIncurredVocRehabAmnt
  var _partOfBodyCode : String as PartOfBodyCode
  var _natureOfInjuryCode : String as NatureOfInjuryCode
  var _causeOfInjuryCode : String as CauseOfInjuryCode
  var _vocRehabInd : String as VocationalRehabInd
  var _paidIndemnityAmount : long as PaidIndemnityAmount
  var _paidMedicalAmount : long as PaidMedicalAmount
  var _totalGrossIncurredAmnt : long as TotalGrossIncurredAmount
  var _paidAllocatedLossAdjExpenseAmnt : long as PaidAllocatedLossAdjExpenseAmnt
  var _scheduledIndemnityPercentageOfDisability : long as ScheduledIndemnityPercentageOfDisability
  var _fraudulentClaimCode : String as FraudulentClaimCode

  //GINTEG-595
  var _reportedDate : Date as ReportedDate_TDIC

  //GINTEG-1062
  var _claimType : String as claimType

  construct() {
  }

  construct(ccClaim: Claim) {
    _claimNumber = ccClaim.ClaimNumber
    _claimPublicId = ccClaim.PublicID
    _policyNumber = ccClaim.Policy.PolicyNumber
    _lossDate = ccClaim.LossDate
    // added Closed date for PC-CC Claim Loss integration
    _closedDate = ccClaim.CloseDate
    _status = ccClaim.State.Description
    _policyTypeName = ccClaim.Policy.PolicyType.DisplayName
    _claimSecurityType = ccClaim.PermissionRequired.Code
    _litigationIndicator = Query.make(Matter).compare(Matter#Claim, Relop.Equals, ccClaim).select().Count > 0
    var largeLossStatus = ccClaim.LargeLossNotificationStatus
    _largeLossIndicator = largeLossStatus == LargeLossNotificationStatus.TC_INQUEUE
                          or largeLossStatus == LargeLossNotificationStatus.TC_SENT
    _fraudIndicator = ccClaim.FraudIndicator
    var tempIncurred = ccClaim.ClaimRpt.TotalIncurredNet
    if (tempIncurred != null) {
      _totalIncurred = tempIncurred.Amount
      _totalIncurredCurrency = tempIncurred.Currency
    }
    var tempReserves = ccClaim.ClaimRpt.RemainingReserves
    if (tempReserves != null) {
      _remainingReserves = tempReserves.Amount
      _remainingReservesCurrency = tempReserves.Currency
    }
    _lossType = ccClaim.LossType.DisplayName
    _adjuster = new PCAdjuster(ccClaim.AssignedUser.Contact)

    // map adjuster's name from PC for TDIC.
    if(ccClaim.ClaimInfo.Adjuster != null){
      _adjusterName = ccClaim.ClaimInfo.Adjuster.DisplayName
    }
    else{
      /* OOTB CC uses AssigneeDisplayString to display Adjuster's name in UI
         and ccClaim.ClaimInfo.Adjuster is null. */
      _adjusterName = ccClaim.AssigneeDisplayString
    }

    // US977, robk
    _insuredName = ccClaim.Policy.insured.DisplayName

    //GINTEG-595
    //ToDO : Need to map below fields once we receive requirements from ClaimCenter Team
    if(ccClaim.LossType == typekey.LossType.TC_GL or ccClaim.LossType == typekey.LossType.TC_PR) {
      _paidIndemnityAmount = 0//ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)?.ExposureRpt?.TotalPayments.Amount?.longValue()
      _reportedDate = ccClaim.ReportedDate
      _paidAllocatedLossAdjExpenseAmnt = 0//getExposureExpense(ccClaim).longValue()
    }
    if(ccClaim.LossType == typekey.LossType.TC_WC7) {

      // WCSTAT fields mapping
      _weeklyWageAmount = (ccClaim?.EmploymentData == null ? 0 : ccClaim?.EmploymentData?.WageAmount as long)
      _injuryCode = ccClaim.getIncidentWithSeverityCodes().Severity.Code
      _catastropheNumber = (ccClaim?.Catastrophe == null ? 0 :  ccClaim?.Catastrophe?.CatastropheNumber.toInt())
      _classificationCode =  ccClaim.EmploymentData.ClassCode != null ? ccClaim.EmploymentData.ClassCode as String : '8839'
      _incurredIndemnityAmount = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)?.ExposureRpt?.TotalIncurredGross as long
      _incurredMedicalAmount = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_WCINJURYDAMAGE)?.ExposureRpt?.TotalIncurredGross as long
      _typeOfLossCode = ccClaim?.ensureClaimInjuryIncident()?.GeneralInjuryType as String
      _typeOfSettlementCode = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)?.Settlements?.first()?.SettleMethod as String
      _totalIncurredVocRehabAmnt = getAmount(ccClaim) as long
      _partOfBodyCode = ccClaim?.ensureClaimInjuryIncident()?.BodyParts?.first()?.DetailedBodyPart as String
      _natureOfInjuryCode = ccClaim?.ensureClaimInjuryIncident()?.DetailedInjuryType as String
      _causeOfInjuryCode = ccClaim?.AccidentType as String
      _vocRehabInd = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)?.VocationalRehab_TDIC?.toString()
      _fraudulentClaimCode = ccClaim?.SIEscalateSIU as String//ccClaim.SIUStatus as String
      _paidIndemnityAmount = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)?.ExposureRpt?.TotalPayments.Amount?.longValue()
      _paidMedicalAmount = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_WCINJURYDAMAGE)?.ExposureRpt?.TotalPayments.Amount?.longValue()
      _totalGrossIncurredAmnt = ccClaim?.ClaimRpt?.TotalIncurredGross as long
      _paidAllocatedLossAdjExpenseAmnt = getExposureExpense(ccClaim).longValue()
      _scheduledIndemnityPercentageOfDisability = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)?.InjuryIncident?.Impairment  as long
    }
    _claimType=ccClaim?.Type_TDIC?.Description
  }

  function getExposureExpense(clm:Claim): BigDecimal {
    var amount:BigDecimal = 0
    clm?.Exposures?.each( \ elt -> {
	  //GINTEG-595 : corrected the cost types and removed 'Expense' cost type
      var exps = elt?.Transactions?.where( \ ee -> (ee.CostType == CostType.TC_DCCEXPENSE or ee.CostType == CostType.TC_AOEXPENSE)) //TODO Change depricated function use
      exps.each( \ exp ->{amount += exp.Amount })
    })
    return amount
  }

  function getAmount(clm:Claim): BigDecimal {
    var exposure = clm?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)
    var amount = exposure?.Transactions?.firstWhere( \ elt -> elt.CostCategory==CostCategory.TC_VOCREHAB_TDIC).Amount
    return amount
  }

  construct(ccClaimInfo : ClaimInfo) {
    _claimNumber = ccClaimInfo.ClaimNumber
    _policyNumber = ccClaimInfo.PolicyNumber
    _lossDate = ccClaimInfo.LossDate
    if (ccClaimInfo.ArchiveState != null) {
      _status = ArchiveState.TC_ARCHIVED.DisplayName
    }
  }
}
