package gw.webservice.cc.cc1000.pcintegration.pcentities

uses gw.xml.ws.annotation.WsiExportable

@WsiExportable ("http://guidewire.com/cc/ws/gw/webservice/cc/cc1000/pcintegration/pcentities/PCClaimSearchCriteria")
@Export
final class PCClaimSearchCriteria
{
  var _policyNumbers : String[] as PolicyNumbers
  var _beginDate : Date as BeginDate
  var _endDate : Date as EndDate
  var _lob : String as Lob
  //GINTEG-595 : added 'legacy pol number' and 'offering' to search criteria
  var _legacyPolicyNumbers : String[] as LegacyPolicyNumbers_TDIC
  var _offering : String as Offering_TDIC
  
  // null matches claims in any state; use "open", "draft", "closed", "archived" to match claims in a specific state.
  var _status : String as Status

  
  construct()
  {
  }
  
}
