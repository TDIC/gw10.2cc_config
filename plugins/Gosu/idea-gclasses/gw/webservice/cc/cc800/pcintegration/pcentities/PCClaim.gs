package gw.webservice.cc.cc800.pcintegration.pcentities

uses java.util.Date
uses java.math.BigDecimal
uses gw.xml.ws.annotation.WsiExportable
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.upgrade.Coercions

@WsiExportable(("http://guidewire.com/cc/ws/gw/webservice/cc/cc800/pcintegration/pcentities/PCClaim"))
@Export
final class PCClaim {
  
  var _claimNumber : String as ClaimNumber
  var _lossDate : Date as LossDate
  var _policyNumber : String as PolicyNumber
  var _policyTypeName : String as PolicyTypeName
  var _totalIncurred : BigDecimal as TotalIncurred
  var _totalIncurredCurrency : Currency as TotalIncurredCurrency
  var _remainingReserves : BigDecimal as RemainingReserves
  var _remainingReservesCurrency : Currency as RemainingReservesCurrency
  var _claimSecurityType : String as ClaimSecurityType
  var _litigationIndicator : Boolean as LitigationIndicator
  var _fraudIndicator : Boolean as FraudIndicator
  var _largeLossIndicator : Boolean as LargeLossIndicator
  // need to be able to access adjuster's and claim rep's name from PC for TDIC.
  var _adjusterName : String as AdjusterName
  // US977, robk
  var _insuredName : String as InsuredName

  // Need Claim closed date for Risk Analysis
  var _closedDate : Date as ClosedDate

  // typelists
  var _status : String as Status

  //WCSTAT fields

  var _classificationCode : String as ClassificationCode
  var _weeklyWageAmount :  long as WeeklyWageAmount
  var _injuryCode : String as InjuryCode
  var _catastropheNumber : int as CatastropheNumber
  var _incurredIndemnityAmount : long as IncurredIndemnityAmount
  var _incurredMedicalAmount : long as IncurredMedicalAmount
  var _typeOfLossCode : String as TypeOfLossCode
  var _typeOfSettlementCode : String as TypeOfSettlementCode
  var _totalIncurredVocRehabAmnt : long as TotalIncurredVocRehabAmnt
  var _partOfBodyCode : String as PartOfBodyCode
  var _natureOfInjuryCode : String as NatureOfInjuryCode
  var _causeOfInjuryCode : String as CauseOfInjuryCode
  var _vocRehabInd : String as VocationalRehabInd
  var _paidIndemnityAmount : long as PaidIndemnityAmount
  var _paidMedicalAmount : long as PaidMedicalAmount
  var _totalGrossIncurredAmnt : long as TotalGrossIncurredAmount
  var _paidAllocatedLossAdjExpenseAmnt : long as PaidAllocatedLossAdjExpenseAmnt
  var _scheduledIndemnityPercentageOfDisability : long as ScheduledIndemnityPercentageOfDisability
  var _fraudulentClaimCode : String as FraudulentClaimCode

  construct()
  {
  }

  construct( ccClaim : Claim )
  {
    _claimNumber = ccClaim.ClaimNumber
    _policyNumber = ccClaim.Policy.PolicyNumber
    _lossDate = ccClaim.LossDate
    // added Closed date for PC-CC Claim Loss integration
    _closedDate = ccClaim.CloseDate
    _status = ccClaim.State.Description
    _policyTypeName = ccClaim.Policy.PolicyType.DisplayName
    _claimSecurityType = ccClaim.PermissionRequired.Code
    _litigationIndicator = Query.make(Matter).compare(Matter#Claim, Relop.Equals, ccClaim).select().Count > 0
    var largeLossStatus = ccClaim.LargeLossNotificationStatus
    _largeLossIndicator = largeLossStatus == LargeLossNotificationStatus.TC_INQUEUE
                          or largeLossStatus == LargeLossNotificationStatus.TC_SENT
    _fraudIndicator = ccClaim.FraudIndicator
    var tempIncurred =  ccClaim.ClaimRpt.TotalIncurredNet
    if (tempIncurred != null) {
      _totalIncurred = tempIncurred.Amount
      _totalIncurredCurrency = tempIncurred.Currency
    }
    var tempReserves = ccClaim.ClaimRpt.RemainingReserves
    if (tempReserves != null) {
      _remainingReserves = tempReserves.Amount
      _remainingReservesCurrency = tempReserves.Currency
    }
    // map adjuster's name from PC for TDIC.
    if(ccClaim.ClaimInfo.Adjuster != null){
      _adjusterName = ccClaim.ClaimInfo.Adjuster.DisplayName
    }
    else{
      /* OOTB CC uses AssigneeDisplayString to display Adjuster's name in UI
         and ccClaim.ClaimInfo.Adjuster is null. */
      _adjusterName = ccClaim.AssigneeDisplayString
    }

    // US977, robk
    _insuredName = ccClaim.Policy.insured.DisplayName

    // WCSTAT fields mapping
    _weeklyWageAmount = ccClaim?.EmploymentData?.WageAmount as long
    _injuryCode = ccClaim.getIncidentWithSeverityCodes().Severity.Code
    _catastropheNumber = Coercions.makePIntFrom(ccClaim?.Catastrophe?.CatastropheNumber)
    _classificationCode =  ccClaim.EmploymentData.ClassCode != null ? ccClaim.EmploymentData.ClassCode as String : '8839'
    _incurredIndemnityAmount = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)?.ExposureRpt?.TotalIncurredGross as long
    _incurredMedicalAmount = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_WCINJURYDAMAGE)?.ExposureRpt?.TotalIncurredGross as long
    _typeOfLossCode = ccClaim?.ensureClaimInjuryIncident()?.GeneralInjuryType as String
    _typeOfSettlementCode = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)?.Settlements?.first()?.SettleMethod as String
    _totalIncurredVocRehabAmnt = getAmount(ccClaim) as long
    _partOfBodyCode = ccClaim?.ensureClaimInjuryIncident()?.BodyParts?.first()?.DetailedBodyPart as String
    _natureOfInjuryCode = ccClaim?.ensureClaimInjuryIncident()?.DetailedInjuryType as String
    _causeOfInjuryCode = ccClaim?.AccidentType as String
    _vocRehabInd = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)?.VocationalRehab_TDIC as String
    _fraudulentClaimCode = ccClaim?.SIEscalateSIU as String//ccClaim.SIUStatus as String
    _paidIndemnityAmount = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)?.ExposureRpt?.TotalPayments as long
    _paidMedicalAmount = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_WCINJURYDAMAGE)?.ExposureRpt?.TotalPayments as long
    _totalGrossIncurredAmnt = ccClaim?.ClaimRpt?.TotalIncurredGross as long
    _paidAllocatedLossAdjExpenseAmnt = getExposureExpense(ccClaim) as long
    _scheduledIndemnityPercentageOfDisability = ccClaim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)?.InjuryIncident?.Impairment  as long

  }

  function getExposureExpense(clm:Claim): BigDecimal {
    var amount=0.0
    clm?.Exposures?.each( \ elt -> {
      var exps = elt?.Transactions?.where( \ ee -> ee.CostType == CostType.TC_EXPENSES_TDIC)
      exps.each( \ exp ->{amount += (exp.Amount) as double })
    })
    return amount
  }

  function getAmount(clm:Claim): BigDecimal {
    var exposure = clm?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_LOSTWAGES)
    var amount = exposure?.Transactions?.firstWhere( \ elt -> elt.CostCategory==CostCategory.TC_VOCREHAB_TDIC).Amount
    return amount
  }

  construct(ccClaimInfo : ClaimInfo) {
    _claimNumber = ccClaimInfo.ClaimNumber
    _policyNumber = ccClaimInfo.PolicyNumber
    _lossDate = ccClaimInfo.LossDate

    if (ccClaimInfo.ArchiveState != null) {
      _status = ArchiveState.TC_ARCHIVED.DisplayName
    }
  }
}
