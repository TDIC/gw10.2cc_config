package util;
uses java.util.ArrayList
uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths
uses gw.config.LOBAbstraction

@Export
class QuestionUtils{

  public static function getAppropriateQuestionSet(claimInput : Claim) : QuestionSet[] {
    var questionSetTypes = new ArrayList<QuestionSetType>()
    questionSetTypes.add(TC_SIUGEN);
    if(claimInput.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY)
      questionSetTypes.add(QuestionSetType.TC_SIUPL_TDIC);
    else if(claimInput.Type_TDIC == ClaimType_TDIC.TC_BUSINESSLIABILITY or claimInput.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY)
      questionSetTypes.add(QuestionSetType.TC_SIUGL_TDIC);
    else if(claimInput.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY or claimInput.Type_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY)
      questionSetTypes.add(QuestionSetType.TC_SIUIDR_TDIC);
    else if(claimInput.Type_TDIC == ClaimType_TDIC.TC_PROPERTY) {
      questionSetTypes.add(QuestionSetType.TC_SIUPROERTY_ARSON_TDIC)
      questionSetTypes.add(QuestionSetType.TC_SIUPROERTY_BUILDIN_TDIC)
      questionSetTypes.add(QuestionSetType.TC_SIUPROERTY_THEFT_TDIC)
    }
    var query = Query.make(entity.QuestionSet)
        .compareIn("QuestionSetType", (questionSetTypes?.toTypedArray()))
           .select().orderBy(QuerySelectColumns.path(Paths.make(QuestionSet#QuestionSetType)))
                    .thenBy(QuerySelectColumns.path(Paths.make(QuestionSet#Priority)))
    
    var questionSets = new ArrayList<QuestionSet>()
    for (questionSet in query) {
      questionSets.add(questionSet)
    }
    return questionSets.toTypedArray()
  }

  public static function getQuestionSetTotalScore(answerContainer : gw.api.question.AnswerContainer, questionSetsTemp : QuestionSet[]) : int {
    var result = 0;
    for (questionSetTemp in questionSetsTemp){
       result = result + questionSetTemp.getPointTotal(answerContainer);
    }
    return result;
  }


  /**
   * create by: NattuduraiK
   * @description: method to get the siu score from single question set
   * @create time: 4:52 PM 12/9/2019
   * @param : answerContainer : gw.api.question.AnswerContainer, questionSetsTemp : QuestionSet
   * @return: int
   */

  public static function getQuestionSetTotalScore(answerContainer : gw.api.question.AnswerContainer, questionSetsTemp : QuestionSet) : int {
    var result = 0;
    if(questionSetsTemp != null)
      result = result + questionSetsTemp.getPointTotal(answerContainer);
    return result;
  }

  /**
   * create by: NattuduraiK
   * @description: method to clear the all selected question choices as null
   * @create time: 4:52 PM 12/9/2019
   * @param : questionSet : QuestionSet[] , AnswerSetContainer : gw.api.question.AnswerSetContainer
   * @return:
   */

  public  static function clearSelected(questionSet : QuestionSet[] , AnswerSetContainer : gw.api.question.AnswerSetContainer)
  {
    for(aquestionSet in questionSet)
    {
      for (question in aquestionSet.Questions)
      {
        AnswerSetContainer.getOrCreateAnswer(question).ChoiceAnswer = null
      }
    }
  }


}
