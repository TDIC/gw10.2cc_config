package tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ClaimContactEnhancement : tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactmodel.ClaimContact {
  public static function create(object : entity.ClaimContact) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactmodel.ClaimContact {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactmodel.ClaimContact(object)
  }

  public static function create(object : entity.ClaimContact, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactmodel.ClaimContact {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactmodel.ClaimContact(object, options)
  }

}