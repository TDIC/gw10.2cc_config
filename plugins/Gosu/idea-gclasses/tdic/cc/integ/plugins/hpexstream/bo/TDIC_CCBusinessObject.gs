/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package tdic.cc.integ.plugins.hpexstream.bo

uses com.tdic.plugins.hpexstream.core.bo.TDIC_BaseBusinessObject
uses gw.api.util.DateUtil

class TDIC_CCBusinessObject extends TDIC_BaseBusinessObject {

  construct() {

  }

  property get Claim() : Claim{
    return RootEntities[0] as Claim
  }

  property get MailingDate() : Date {
    // The document mailing date is the next business day.
    var mailingDate = DateUtil.addBusinessDays(Date.Today, 1);
    return mailingDate;
  }
    
} 
