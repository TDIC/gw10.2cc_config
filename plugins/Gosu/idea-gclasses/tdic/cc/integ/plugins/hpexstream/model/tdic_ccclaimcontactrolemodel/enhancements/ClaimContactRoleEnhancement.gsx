package tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactrolemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ClaimContactRoleEnhancement : tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactrolemodel.ClaimContactRole {
  public static function create(object : entity.ClaimContactRole) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactrolemodel.ClaimContactRole {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactrolemodel.ClaimContactRole(object)
  }

  public static function create(object : entity.ClaimContactRole, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactrolemodel.ClaimContactRole {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimcontactrolemodel.ClaimContactRole(object, options)
  }

}