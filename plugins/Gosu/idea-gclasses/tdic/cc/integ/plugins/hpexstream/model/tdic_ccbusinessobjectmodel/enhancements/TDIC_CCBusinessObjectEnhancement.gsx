package tdic.cc.integ.plugins.hpexstream.model.tdic_ccbusinessobjectmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_CCBusinessObjectEnhancement : tdic.cc.integ.plugins.hpexstream.model.tdic_ccbusinessobjectmodel.TDIC_CCBusinessObject {
  public static function create(object : tdic.cc.integ.plugins.hpexstream.bo.TDIC_CCBusinessObject) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccbusinessobjectmodel.TDIC_CCBusinessObject {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccbusinessobjectmodel.TDIC_CCBusinessObject(object)
  }

  public static function create(object : tdic.cc.integ.plugins.hpexstream.bo.TDIC_CCBusinessObject, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccbusinessobjectmodel.TDIC_CCBusinessObject {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccbusinessobjectmodel.TDIC_CCBusinessObject(object, options)
  }

}