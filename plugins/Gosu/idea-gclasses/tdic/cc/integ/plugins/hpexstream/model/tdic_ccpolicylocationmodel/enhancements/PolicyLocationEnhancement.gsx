package tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicylocationmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyLocationEnhancement : tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicylocationmodel.PolicyLocation {
  public static function create(object : entity.PolicyLocation) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicylocationmodel.PolicyLocation {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicylocationmodel.PolicyLocation(object)
  }

  public static function create(object : entity.PolicyLocation, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicylocationmodel.PolicyLocation {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicylocationmodel.PolicyLocation(object, options)
  }

}