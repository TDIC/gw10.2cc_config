package tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicymodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyEnhancement : tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicymodel.Policy {
  public static function create(object : entity.Policy) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicymodel.Policy {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicymodel.Policy(object)
  }

  public static function create(object : entity.Policy, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicymodel.Policy {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccpolicymodel.Policy(object, options)
  }

}