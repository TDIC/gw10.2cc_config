package tdic.cc.integ.plugins.hpexstream.model.tdic_ccaddressmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AddressEnhancement : tdic.cc.integ.plugins.hpexstream.model.tdic_ccaddressmodel.Address {
  public static function create(object : entity.Address) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccaddressmodel.Address {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccaddressmodel.Address(object)
  }

  public static function create(object : entity.Address, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccaddressmodel.Address {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccaddressmodel.Address(object, options)
  }

}