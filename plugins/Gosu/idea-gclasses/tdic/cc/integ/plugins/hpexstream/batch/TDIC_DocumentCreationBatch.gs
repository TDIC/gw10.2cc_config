/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 */
package tdic.cc.integ.plugins.hpexstream.batch

uses com.tdic.util.properties.PropertyUtil
uses gw.api.system.server.ServerUtil
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses com.tdic.plugins.hpexstream.core.bo.commandcenter.TDIC_CommandCenterActionRequest
uses com.tdic.plugins.hpexstream.core.messaging.TDIC_ExstreamTransport
uses com.tdic.plugins.hpexstream.core.util.TDIC_ExstreamHelper

/**
 * US555
 * 01/23/2015 Shane Murphy
 *
 *  HP Exstream Composition process batch implementation for high-volume batch runs of asynchronous document creation that are to be scheduled on periodic or manual basis.
 *
 *  Throughout a day, composition payload data are being generated and stored in an intermediary staging area in the integration database
 *  by the Exstream Transport plugin (see {@link com.tdic.plugins.hpexstream.core.messaging.TDIC_ExstreamTransport}).
 *
 *  Once a doWork() method is triggered, it extracts payload data from this staging area and sends them in a SOAP request to HP Exstream Command Center to start a composition job.
 */
class TDIC_DocumentCreationBatch extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  construct() {
    super(BatchProcessType.TC_DOCUMENTCREATIONBATCH)
  }

  /**
   * US555
   * 01/23/2015 Shane Murphy
   *
   * Extracts payload data from this staging area and sends them in a SOAP request to HP Exstream Command Center to start a composition job.
   */
  override function doWork(): void {
    _logger.debug("TDIC_DocumentCreationBatch#doWork() - Entering")
    var requestXML = getAggregateXML()
    if (requestXML != null) {
      var requestToCmd = createRequestObject(requestXML)
      sendRequest(requestToCmd)
    }
    _logger.debug("TDIC_DocumentCreationBatch#doWork() - Exiting")
  }

  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * Retrieves all payloads stored in IntDB staging area for ClaimCenter batch document creation and
   * concatenates all XML payload data together under a single <ClaimArray> parent tag.
   */
  @Returns("Batch XML containing payloads from multiple events.")
  function getAggregateXML(): String {
    _logger.trace("TDIC_DocumentCreationBatch#getAggregateXML() - Entering")
    var eventPayloads = {PropertyUtil.getInstance().getProperty(ServerUtil.getProduct().ProductName)}
    if (eventPayloads == null) {
      _logger.debug("TDIC_DocumentCreationBatch#getAggregateXML() - No payloads stored for batch")
      return null
    } else {
      var aggregateXmlBuilder = new StringBuilder()
      eventPayloads.each(\payload -> {
        aggregateXmlBuilder.append(payload.remove("<?xml version=\"1.0\"?>"))
      })
      var requestXML = "<ClaimArray>" + aggregateXmlBuilder.toString() + "</ClaimArray>"
      _logger.debug("TDIC_DocumentCreationBatch#getAggregateXML() - Request XML: ${requestXML}")
      _logger.trace("TDIC_DocumentCreationBatch#getAggregateXML() - Exiting")
      return requestXML
    }
  }

  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * Creates a request object for the batch payload.
   */
  @Param("aBatchXMLPayload", "Aggregate XML payload of all events to be sent.")
  @Returns("Request Object")
  function createRequestObject(aBatchXMLPayload: String): TDIC_CommandCenterActionRequest {
    _logger.trace("TDIC_DocumentCreationBatch#createRequestObject(String) - Entering")
    var reqXMLinBase64 = gw.util.Base64Util.encode(aBatchXMLPayload.Bytes)
    var requestToCmd = new TDIC_CommandCenterActionRequest(null, getCCJobDefName(), "DRIVERFILE", reqXMLinBase64)
    _logger.debug("TDIC_DocumentCreationBatch#createRequestObject(String) - XML Attributes:")
    requestToCmd.getXMLModelAttributes().each(\attribute -> _logger.debug(attribute.asUTFString()))
    _logger.trace("TDIC_DocumentCreationBatch#createRequestObject(String) - Exiting")
    return requestToCmd
  }

  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * Send the request object to HP Exstream and delete sent entries from staging area if successful
   */
  @Param("requestToCmd", "Request object to send")
  function sendRequest(requestToCmd: TDIC_CommandCenterActionRequest) {
    _logger.trace("TDIC_DocumentCreationBatch#sendRequest(TDIC_CommandCenterActionRequest) - Entering")
    try {
      var actionInterface = TDIC_ExstreamHelper.getDocProdService()
      var responseFromCMD = actionInterface.action("CREATE_JOB", requestToCmd.getXMLModelAttributes())
      _logger.debug("TDIC_DocumentCreationBatch#sendRequest(TDIC_CommandCenterActionRequest) - Response from Command Center" + responseFromCMD.asUTFString())
      if (responseFromCMD != null and responseFromCMD.Attribute.where(\a -> a.Name == "status_code" and a.Value_Attribute == "1001").Empty == false) {
        _logger.trace("TDIC_DocumentCreationBatch#sendRequest(TDIC_CommandCenterActionRequest) - Successfully consumed")
      }
    } catch (var responseException: Exception) {
      _logger.error("TDIC_DocumentCreationBatch#sendRequest(TDIC_CommandCenterActionRequest) - Exception in Response from Command Center " + responseException.toString())
    }
    _logger.trace("TDIC_DocumentCreationBatch#sendRequest(TDIC_CommandCenterActionRequest) - Exiting")
  }

  /**
   *  US555
   *  10/13/2014 shanem
   *
   *  A helper method to retrieve CCJobDefName attribute of the current TDIC_ExstreamTransport plugin.
   *  Currently this attribute is configured as the plugin's parameter and during instantiation of the ExstreamTransport class its static variable <b>CCJobDefName</b> is updated.
   *
   *  <b>Note:</b> instead of using static variable (which limits number of instances of such Transport class to 1.
   *  These parameters should be retrieved via {@link gw.plugin.Plugins#get(java.lang.String pluginName)}.
   *  This is not possible as the Plugin retrieved this way cannot be cast to TDIC_ExstreamTransport class in v8.0.1.
   */
  @Returns("CCJobDefName attribute of current TDIC_ExstreamTransport plugin")
  function getCCJobDefName(): String {
    return TDIC_ExstreamTransport.CCJobDefName
  }
}