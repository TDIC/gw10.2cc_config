package tdic.cc.integ.plugins.checkprinting.dto

uses java.math.BigDecimal

/**
 * CheckPaymentDTO contains check payment info felds for check printing outbound.
 * Created by RambabuN on 11/20/2019.
 */
class CheckPaymentDTO {

  var _checkNumber : String as CheckNumber
  var _checkPublicID : String as CheckPublicID
  var _vendorNumber : String as VendorNumber
  var _invoiceNumber : String as InvoiceNumber
  var _costCategory : String as CostCategory
  var _netAmount : BigDecimal as NetAmount
  var _payeeName : String as PayeeName
  var _lineItems : List<PaymentLineItemDTO> as LineItems
  var _incomeCode : String as IncomeCode
  var _vendorName : String as VendorName
  var _addr1 : String as Address1
  var _addr2 : String as Address2
  var _city : String as City
  var _state : String as State
  var _postalCode : String as PostalCode
  var _remitCode : String as RemitCode
  var _voucherNumber : String as VoucherNumber

}