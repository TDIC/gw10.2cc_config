package tdic.cc.integ.plugins.checkprinting.dto

uses java.util.ArrayList
uses gw.api.util.DateUtil
uses java.text.SimpleDateFormat
uses java.util.List

/**
 * A Gosu class to contain the fields needed for the GX model when generating the flat files.
 */
class CheckPrintingFile {

  static final var DATE_FORMAT_WITH_SLASH = "MM/dd/yyyy"

  static final var DATE_FORMAT_WITH_NO_SLASH = "MMddyyyy"

  /**
   * The List of check payments to write to the flat files.
   */
  var _checkPayments : List<CheckPaymentDTO> as CheckPayments

  /**
   * The List of check payment line to write to the flat files.
   */
  var _paymentLines : List<PaymentLineItemDTO> as PaymentLines

  construct() {
    _checkPayments = new ArrayList<CheckPaymentDTO>()
    _paymentLines = new ArrayList<PaymentLineItemDTO>()

  }

  /**
   * Formats the current date in its full format for the flat files.
   */
  @Returns("A String containing the formatted current date in its full format for the flat files")
  static function formatCurrentDateFull() : String {
    var currentDateTime = DateUtil.currentDate()
    var dateFormat = new SimpleDateFormat(DATE_FORMAT_WITH_SLASH)
    return dateFormat.format(currentDateTime)
  }

  /**
   * Formats the current date without any separators for the flat files.
   */
  @Returns("A String containing the formatted current date without any separators for the flat files")
  static function formatCurrentDateNoSeparators() : String {
    var currentDateTime = DateUtil.currentDate()
    var dateFormat = new SimpleDateFormat(DATE_FORMAT_WITH_NO_SLASH)
    return dateFormat.format(currentDateTime)
  }

}