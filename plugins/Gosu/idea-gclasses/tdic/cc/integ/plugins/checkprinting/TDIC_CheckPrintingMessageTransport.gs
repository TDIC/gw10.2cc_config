package tdic.cc.integ.plugins.checkprinting

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.plugin.messaging.MessageTransport
uses org.slf4j.LoggerFactory
uses tdic.cc.common.batch.checkprinting.util.TDIC_CheckPrintingHelper
uses tdic.cc.integ.plugins.checkprinting.exception.CheckPrintingException
uses tdic.cc.integ.plugins.checkprinting.model.checkpaymentdtomodel.CheckPaymentDTO

/**
 * The Transport plugin is responsible to read message from the Check Printing Destination Queue
 * and store the check payment info into table CheckPaymentLines of GWINT DB.
 * Created by RambabuN on 11/20/2019.
 */
class TDIC_CheckPrintingMessageTransport implements MessageTransport {

  static final var _logger = LoggerFactory.getLogger("TDIC_CheckPrinting")

  /**
   * Notification email's subject for Check printing message transport plugin failure.
   */
  public static final var EMAIL_SUBJECT_FAILURE : String =
      "Check printing message transport Failure on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Key for looking up the failure notification email addresses.
   */
  static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "CCInfraIssueNotificationEmail"

  static final var NO_VENDOR_NUM_ERROR_DESC = "No vendor number for contact"


  override function send(message : Message, transformedPayload : String) {
    _logger.debug("Enter TDIC_CheckPrintingMessageTransport.send() ")

    _logger.debug(this.Class.getName() + "Check Payment Payload XML : " + transformedPayload)

    var helper = new TDIC_CheckPrintingHelper()

    try {

      var checkPaymentDTO = CheckPaymentDTO.parse(transformedPayload)

      // Validate Vendor Number prior to writing to integration database. send email and report error if null
      if(!isVendorNumberAvailable(checkPaymentDTO, message))
        return

      //Invoke helper to insert check line items to database
      helper.insertPaymentLineItem(checkPaymentDTO)

      //Acknowledge message
      message.reportAck()

      //Acknowledge for Check requesting Status
      if(message.MessageRoot typeis Check)
      helper.acknowledgeCheck(message.MessageRoot)
    } catch (exception : CheckPrintingException) {
      _logger.error(this.Class.getName() + " Exception Occurred while inserting Transaction LineItem Data" +
          "into the DB", exception)
      message.ErrorDescription = exception.getMessage()
      // Throwing an exception triggers automatic retries
      message.reportError()
    }
    _logger.debug("Exit TDIC_CheckPrintingMessageTransport.send")
  }

  /**
   * Function to validate check payee VendorNumber.
   * @param checkPaymentDTO
   * @param message
   */
  function isVendorNumberAvailable(checkPaymentDTO : CheckPaymentDTO, message : Message) : boolean {
    // Validate Vendor Number prior to writing to integration database; send email and report error if null
    if (checkPaymentDTO.VendorNumber == null || checkPaymentDTO.VendorNumber.Empty) {
      _logger.warn("TDIC_CheckPrintingMessageTransport#send - Vendor Number not found for check with Public ID "
          + checkPaymentDTO.CheckPublicID)
      var failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
      if (failureNotificationEmail != null) {
        EmailUtil.sendEmail(failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "TDIC_CheckPrintingMessageTransport#send - Check Payment messaging has found payee contact " +
                "has no vendor number.  Check number : " + checkPaymentDTO.CheckNumber +
                ". Check Payee Name: " + checkPaymentDTO.PayeeName +
                ". Amount of Check Payment: $" + checkPaymentDTO.NetAmount +
                ". Check Payment Public ID: " + checkPaymentDTO.CheckPublicID +
                ". The Contact entity must be updated with the Vendor Number before retrying this message.")
        _logger.info("TDIC_CheckPrintingMessageTransport#send - Failure email notification sent. Reporting error.")
        message.ErrorDescription = NO_VENDOR_NUM_ERROR_DESC
        message.reportError(ErrorCategory.TC_VENDERNUMBERERROR_TDIC)
        return false
      }
    }
    return true
  }

  override function shutdown() {

  }

  override function suspend() {

  }

  override function resume() {

  }

  override property set DestinationID(destinationID : int) {

  }
}