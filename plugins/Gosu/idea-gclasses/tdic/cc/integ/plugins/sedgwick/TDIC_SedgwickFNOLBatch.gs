package tdic.cc.integ.plugins.sedgwick

uses com.tdic.util.database.DatabaseUtil
uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.misc.FtpUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.util.ConfigAccess
uses gw.api.util.StringUtil
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses gw.xml.XmlElement
uses org.apache.commons.io.FileUtils
uses org.slf4j.LoggerFactory
uses tdic.cc.common.batch.sedgwick.helper.TDIC_FlatFileUtilityHelper
uses tdic.cc.integ.plugins.sedgwick.helper.TDIC_SedgwickDateUtil

uses java.io.File
uses java.io.FileNotFoundException
uses java.sql.Connection
uses java.sql.PreparedStatement
uses java.sql.Statement
uses java.text.SimpleDateFormat

/**
 * Created with IntelliJ IDEA.
 * User: Praneethk
 * Date: 5/23/15
 * Time: 9:19 AM
 * Sedgwick Batch Process
 */
class TDIC_SedgwickFNOLBatch extends BatchProcessBase {
  private var _logger = LoggerFactory.getLogger("TDIC_FEEDTOSEDGWICK")             //Standard Logger
  private static var CLASS_NAME : String = "TDIC_SedgwickFNOLBatch"
  var headerPath : String
  var excelPath : String
  var destDir : String
  private var _emailTo : String as readonly EmailTo
  private var _emailRecipient : String as readonly EmailRecipient
  public static final var SEDGWICK_EMAIL_TO_PROP_FIELD : String = "sedgwick.error.email"
  public static final var SEDGWICK_EMAIL_RECIPIENT : String = "sedgwick.notification.email"
  public static final var EMAIL_SUBJECT_COMPLETED: String = "Feed To Sedgwick Batch Job Completed on server " + gw.api.system.server.ServerUtil.ServerId
  public static final var EMAIL_SUBJECT_FAILURE: String = "Feed To Sedgwick Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId
  construct() {
      super(BatchProcessType.TC_SEDGWICKFNOL)
      headerPath = PropertyUtil.getInstance().getProperty("headerPath")
      excelPath = PropertyUtil.getInstance().getProperty("sedgwick.excelPath")
      destDir =  PropertyUtil.getInstance().getProperty("sedgwick.outputDir")
  }

  override function checkInitialConditions() : boolean {
    var logPrefix = "${CLASS_NAME}#checkInitialConditions()"
    _logger.debug("${logPrefix} - Entering")
    _emailTo = PropertyUtil.getInstance().getProperty(SEDGWICK_EMAIL_TO_PROP_FIELD)
    if (_emailTo == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"+SEDGWICK_EMAIL_TO_PROP_FIELD+"' from integration database.")
    }
    _emailRecipient = PropertyUtil.getInstance().getProperty(SEDGWICK_EMAIL_RECIPIENT)
    if (_emailRecipient == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"+SEDGWICK_EMAIL_RECIPIENT+"' from integration database.")
    }
    _logger.debug("${logPrefix} - Exiting")
    return Boolean.TRUE
  }

  override function doWork() {
    _logger.debug("Entering #dowork() - TDIC_SedgwickFNOLBatch")

    if(!new File(destDir).exists()){
      EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The Feed To Sedgwick batch process failed. Cannot Access destDir. The Directory May Not Exist or There is an Error in network Connection")
      _logger.error("Cannot Access destDir. The Directory May Not Exist or There is an Error in network Connection")
      throw new FileNotFoundException ("The Directory May Not Exist or There is an Error in network Connection. Sedgwick FlatFile does not exist:  " + destDir) //return null
    }

    var payloads = getFNOLPayloads()

    for(claimNumber in payloads.keySet()){
      var payload = payloads.get(claimNumber)
      var encryptedFile = generateEncryptedFlatFile(payload)
      var file = new File(encryptedFile)

      if (file.exists()and file.length() != 0) {
        _logger.trace("Sending File From Shared Access to FTP"+payload)
        var fileUploaded = FtpUtil.uploadFile("sedgwick", file)
        if(fileUploaded){
          markClaimAsProcessed(claimNumber)
          EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_COMPLETED, "The Feed To Sedgwick batch process Completed Successfully, File generated is:" + file.Name + " for Claim Number:-" + claimNumber)
        }
        else{
          EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The Feed To Sedgwick batch process failed. File not uploaded to FTP from shared Drive. Check FTP Site and crossverify Shared Directory for file named." + file.Name)
          _logger.error("File not uploaded to FTP")
        }
      }
      else {
        EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The Feed To Sedgwick batch process failed. Either Sedgwick FlatFile :" + file.Name + " does not exist or File Exists but is Empty.")
        throw new FileNotFoundException ("Sedgwick FlatFile does not exist: " + file)
      }
    }
    _logger.debug("Exiting #dowork() - TDIC_SedgwickFNOLBatch")
  }

  override function requestTermination() : boolean {
    _logger.debug("TDIC_SedgwickFNOLBatch#requestTermination() - Terminate requested for batch process.")
    super.requestTermination()// Set TerminateRequested to true by calling super method
    return true
  }

  function markClaimAsProcessed(claimNumber:String){
    var _con: Connection = null
    var _prepStatement : PreparedStatement = null
    try{
      _con = DatabaseUtil.getIntegDBConnection()
      var _prepSqlStmt = "UPDATE FNOLSedgwick SET Processed=1 WHERE ClaimNumber = ?"
      _prepStatement = _con.prepareStatement(_prepSqlStmt)
      _prepStatement.setString(1,claimNumber)
      var _return =_prepStatement.executeUpdate()
      _con.commit()
    }

    finally{
      _con.close()
    }
  }

  function getFNOLPayloads():HashMap<String,String>{
    var _con: Connection = null
    var _statement : Statement = null
    var payloadMap = new HashMap<String,String>()
    try{
      _con = DatabaseUtil.getIntegDBConnection()
      var _sqlStmt = "SELECT ClaimNumber,Payload FROM FNOLSedgwick Where Processed = 0"
      _statement = _con.createStatement()
      var _return =_statement.executeQuery(_sqlStmt)
      while(_return.next()){
        payloadMap.put(_return.getString("ClaimNumber"),_return.getString("Payload"))
      }
    }
    finally{
      _statement.close()
      _con.close()
    }
    return payloadMap
  }
  /*
   * Get Payload to Encrypted Flatfile before sending it over FTP
   */
  function generateEncryptedFlatFile(payload:String):String{
    var flatFileName = TDIC_SedgwickDateUtil.getSedgwickCounter()
    var sedgwickCounter = flatFileName.substring(8)
    var sedgwickMonth = StringUtil.formatNumber(flatFileName.substring(5,7).toDouble(),"00")
    var encryptedFile = destDir+flatFileName
    var destFile = encryptedFile+"-TempFile"

   /*
    * Store the variables required by flatfileutilityhelper.gs to db
    */
    var date = new Date()
    var dateFormat = new SimpleDateFormat("MMddyyyy")
    var timeFormat = new SimpleDateFormat("HHmmss")
    var _sqlStmt = "INSERT INTO dbo.SedgwickHistory (FileName, DateProcessed , TimeProcessed, SedgwickCounter, SedgwickMonth)"
        + "VALUES ('" + flatFileName +"','" +  dateFormat.format(date).toString()+"','" +timeFormat.format(date).toString()+"','" +
        sedgwickCounter+ "','"+ sedgwickMonth+"')"
    DatabaseUtil.dbInsertProperty(_sqlStmt)
    /*
    * Encode XML to FlatFile
    */
    var flatFileUtility = new TDIC_FlatFileUtilityHelper ()
    _logger.info("#### Header Path is ::" + headerPath)
    _logger.info("#### Excel path is :: " + excelPath)
    var flatFileGenerated = flatFileUtility.encode(excelPath,headerPath,XmlElement.parse(payload),destFile)
    if(!flatFileGenerated){         //Error in flat file generation
      _logger.error("Flat File Cannot be generated. Error in Flat File Generation")
      return null
    }
    /*
    * Encrypt File Before FTP'ing
    */
	  var publicKeyFile = PropertyUtil.getInstance().getProperty("sedgwick.publickey")
    _logger.info("publicKeyFile before:" + publicKeyFile)
    publicKeyFile =  ConfigAccess.getConfigFile(publicKeyFile).Path
    _logger.info("publicKeyFile after:" +publicKeyFile)
    UtilPGP.PGPUtility.encrypt(publicKeyFile,destFile,encryptedFile)
    if(ScriptParameters.DeleteSedgwickTempFiles){
      FileUtils.forceDelete(new File(destFile))
    }
    return encryptedFile
  }
}