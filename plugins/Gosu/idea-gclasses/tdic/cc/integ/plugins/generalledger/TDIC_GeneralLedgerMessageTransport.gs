package tdic.cc.integ.plugins.generalledger

uses gw.plugin.messaging.MessageTransport
uses org.slf4j.LoggerFactory
uses tdic.cc.common.batch.generalledger.util.TDIC_GeneralLedgerHelper
uses tdic.cc.integ.plugins.generalledger.exception.TDIC_GeneralLedgerException
uses tdic.cc.integ.plugins.generalledger.model.gltransactiondtomodel.GLTransactionDTO

class TDIC_GeneralLedgerMessageTransport implements MessageTransport {

  static final var _logger = LoggerFactory.getLogger("TDIC_GeneralLedger")

  override function send(message : Message, transformedPayload : String) {
    _logger.debug("Enter TDIC_GeneralLedgerMessageTransport.send() ")

    _logger.debug(this.Class.getName() + " Transaction Payload XML : " + transformedPayload)

    try {
      var glTransactionDTO = GLTransactionDTO.parse(transformedPayload)

      //Invoke helper to insert CC transaction to database
      TDIC_GeneralLedgerHelper.insertGLTransaction(glTransactionDTO)

      //Acknowledge message
      message.reportAck()
    } catch (exception : TDIC_GeneralLedgerException) {
      _logger.error(this.Class.getName() + " Exception Occurred while inserting Transaction Data" +
          "into the DB", exception)
      message.ErrorDescription = exception.getMessage()
      // Throwing an exception triggers automatic retries
      message.reportError()
    }
    _logger.debug("Exit TDIC_GeneralLedgerMessageTransport.send()")
  }

  override function shutdown() {

  }

  override function suspend() {

  }

  override function resume() {

  }

  override property set DestinationID(destinationID : int) {

  }
}