package tdic.cc.integ.plugins.generalledger.exception

uses com.tdic.util.exception.TDICException

/**
 * TDIC_GeneralLedgerException to be Thrown for errors in GL Integration Transactions.
 * Created by RambabuN on 12/17/2019.
 */
class TDIC_GeneralLedgerException extends TDICException {

  public construct() {
    super()
  }

  public construct(errorMessage : String) {
    super(errorMessage)
  }

  public construct(errorMessage : String, exception : Exception) {
    super(errorMessage, exception)
  }

}