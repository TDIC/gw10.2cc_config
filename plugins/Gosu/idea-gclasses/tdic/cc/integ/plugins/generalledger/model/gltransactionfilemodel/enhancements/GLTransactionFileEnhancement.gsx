package tdic.cc.integ.plugins.generalledger.model.gltransactionfilemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLTransactionFileEnhancement : tdic.cc.integ.plugins.generalledger.model.gltransactionfilemodel.GLTransactionFile {
  public static function create(object : tdic.cc.integ.plugins.generalledger.dto.GLTransactionFile) : tdic.cc.integ.plugins.generalledger.model.gltransactionfilemodel.GLTransactionFile {
    return new tdic.cc.integ.plugins.generalledger.model.gltransactionfilemodel.GLTransactionFile(object)
  }

  public static function create(object : tdic.cc.integ.plugins.generalledger.dto.GLTransactionFile, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.generalledger.model.gltransactionfilemodel.GLTransactionFile {
    return new tdic.cc.integ.plugins.generalledger.model.gltransactionfilemodel.GLTransactionFile(object, options)
  }

}