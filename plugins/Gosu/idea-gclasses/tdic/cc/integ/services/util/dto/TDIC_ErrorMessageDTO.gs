package tdic.cc.integ.services.util.dto

uses gw.xml.ws.annotation.WsiExportable

/**
 *This class is used to capture check status validation error messages for updateCheckStatus Webservice.
 * Created by RambabuN on 11/22/2019.
 */
@WsiExportable("http://tdic.com/tdic/cc/integ/services/util/dto/TDIC_ErrorMessageDTO")
final class TDIC_ErrorMessageDTO {
  var _errorCode : int as ErrorCode

  override function toString() : String {
    return "${ErrorCode}"
  }

}