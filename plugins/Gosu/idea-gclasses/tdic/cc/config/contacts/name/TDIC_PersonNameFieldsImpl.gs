package tdic.cc.config.contacts.name

uses tdic.cc.config.contacts.name.TDIC_PersonNameFields

/**
 * US1031, DE66
 * 01/06/2015 Rob Kelly
 *
 * Name fields used for Persons at TDIC.
 */
class TDIC_PersonNameFieldsImpl implements TDIC_PersonNameFields {

  var _firstName : String as FirstName
  var _lastName : String as LastName
  var _firstNameKanji : String as FirstNameKanji
  var _lastNameKanji : String as LastNameKanji
  var _middleName : String as MiddleName
  var _particle : String as Particle
  var _prefix : NamePrefix as Prefix
  var _suffix : NameSuffix as Suffix
  var _credential : Credential_TDIC as Credential_TDIC

  // not used for Person entities:
  var _name : String as Name
  var _nameKanji : String as NameKanji
}