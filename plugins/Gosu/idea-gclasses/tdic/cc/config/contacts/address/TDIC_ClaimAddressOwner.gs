package tdic.cc.config.contacts.address

uses gw.api.address.ClaimAddressOwner
uses gw.api.address.AddressOwnerFieldId
uses gw.api.address.CCAddressOwnerFieldId
uses java.util.Set
uses java.util.HashSet
uses gw.api.locale.DisplayKey

/**
 * US529, US839
 * 10/10/2014 Rob Kelly
 *
 * AddressOwner for TDIC Claims.
 */
class TDIC_ClaimAddressOwner extends ClaimAddressOwner {

  private var _claim : Claim as readonly RelatedClaim

  construct(inClaim : Claim) {
    super(inClaim)
    _claim = inClaim
  }

  override property get AddressNameLabel() : String {
    return DisplayKey.get("TDIC.Claim.LossLocation")
  }

  override property get HiddenFields() : Set<AddressOwnerFieldId> {
    return AddressFields.getClaimFileHiddenFields(_claim).union({CCAddressOwnerFieldId.LOCATIONCODE, CCAddressOwnerFieldId.COUNTRY})
  }

  override property get NonEditableAddresses() : Set<Address> {
    return new HashSet<Address>()
  }
}