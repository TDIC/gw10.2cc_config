package tdic.cc.config.recoveryreserve

class TDIC_RecoveryReservesHelper {
  public static function getValidCostCategories(aTransaction : Transaction ,  WizardHelper : gw.api.financials.RecoveryReserveWizardHelper) : List<typekey.CostCategory> {

    if (aTransaction.Claim.isWorkCompClaim())
      return WizardHelper.getValidCostCategories(aTransaction)


    var validCostCategories = CostCategory.AllTypeKeys.where(\elt ->
        elt.Categories.contains(aTransaction.Claim.Type_TDIC)
            and elt.Categories.contains(aTransaction.CostType)
            and ((aTransaction.Exposure == null) or aTransaction.Exposure != null and elt.Categories.contains(aTransaction.CostType))
            and ((aTransaction.Exposure == null or aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY) or aTransaction.Exposure != null and elt.Categories.contains(aTransaction.Exposure.PrimaryCoverage))).toList()
    if ((aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_BUSINESSLIABILITY or aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE) and aTransaction.Exposure == null)
      validCostCategories = validCostCategories.where(\elt -> elt.Code != "unspecified" and elt.Code != "other")
    return validCostCategories
  }

  public static function getValidCostTypes(aTransaction : Transaction, WizardHelper : gw.api.financials.RecoveryReserveWizardHelper) : List<CostType> {

    if (aTransaction.Claim.isWorkCompClaim())
      return WizardHelper.getValidCostTypes(aTransaction)

    var costTypes = CostType.getTypeKeys(false).toList()

    if ((aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_PROPERTY
        or aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES
        or aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY
        or aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY
        or aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_BUSINESSLIABILITY
        or aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY)
        and aTransaction.Exposure != null)
      return costTypes.where(\elt -> elt.Code.equalsIgnoreCase("claimcost"))

    if ((aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY) and aTransaction.Exposure == null)
      return costTypes.where(\elt -> elt.Code == "")
    if (aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE and aTransaction.Exposure == null)
      return costTypes.where(\elt -> elt.Code.equalsIgnoreCase("dccexpense"))
    if (aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE and aTransaction.Exposure != null)
      return costTypes.where(\elt -> elt.Code == "")
    if (aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY and aTransaction.Exposure != null and aTransaction.Exposure.PrimaryCoverage == CoverageType.TC_GLCYBCOV1_TDIC)
      return costTypes.where(\elt -> elt.Code.equalsIgnoreCase("dccexpense") or elt.Code.equalsIgnoreCase("aoexpense"))
    var validCostTypes = costTypes.where(\elt -> elt.Code != "expenses_TDIC")
    return validCostTypes
  }

  public static function getValidExposures(aTransaction : Transaction, WizardHelper : gw.api.financials.RecoveryReserveWizardHelper) : List<Exposure> {


    if(aTransaction.Claim.isWorkCompClaim())
    return WizardHelper.getValidExposures(aTransaction)
    if(aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE)
      return new ArrayList<Exposure>()
       
    return aTransaction.Claim.Exposures.toList()
  }

}