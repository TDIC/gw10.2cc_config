package tdic.cc.config.enhancement

enhancement TDIC_ClaimContactEnhancement : entity.ClaimContact {
  /**
   * US669
   * 02/04/2015 Shane Murphy
   *
   * Number of roles this contact has. Required for Exstream processing of contact roles.
  */
  property get NumRoles(): int{
    return this.Roles.Count
  }

}
