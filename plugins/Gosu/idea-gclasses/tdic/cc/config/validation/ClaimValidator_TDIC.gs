package tdic.cc.config.validation

uses gw.api.locale.DisplayKey
uses gw.api.validation.FieldValidationResult
uses gw.api.validation.FieldValidatorBase
uses gw.api.validation.IFieldValidationResult

class ClaimValidator_TDIC extends FieldValidatorBase {

  construct() {
  }

  override function validate(claimNumber : String, p1 : Object, parameters : Map<Object, Object>) : IFieldValidationResult {
    var result = new FieldValidationResult();
    if (!(claimNumber?.matches("(CP-[0-9]{4})?([a-zA-Z0-9]{3}-[0-9]{2}-[0-9]{6})?") == true or
        claimNumber?.matches("([0-9]{5}-[0-9]{3})?") == true )) {
      result.addError(DisplayKey.get("Validator.ClaimNumber", ""));
    }
    return result;
  }

}
