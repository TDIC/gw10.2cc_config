package tdic.cc.common.admindata

uses org.w3c.dom.Element
uses tdic.cc.common.admindata.CleanseAdminData

uses java.util.List

class CleansePCAdminData extends CleanseAdminData {

  /**
   * Get the list of admin data files.
   */
  override protected property get Filenames() : List<String> {
    var retval = super.Filenames;
    retval.add ("policy forms.xml");
    retval.add ("policy holds.xml");
    return retval;
  }

  /**
   * Cleanse the admin data file.
   */
  override protected function cleanseAdminDataFile (filename : String, import : Element) : void {
    super.cleanseAdminDataFile (filename, import);

    if (filename == "admin.xml") {
      removeElements (import, {"UWIssueType"});

      removeSubElements (import, "Address",     {"LastUpdateTime"});
      removeSubElements (import, "UserContact", {"LastUpdateTime"});
    }
  }
}