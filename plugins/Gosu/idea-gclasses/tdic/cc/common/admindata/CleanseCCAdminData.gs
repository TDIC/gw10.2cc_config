package tdic.cc.common.admindata
/**
 * Created with IntelliJ IDEA.
 * User: TimT
 * Date: 6/21/18
 * Time: 10:44 AM
 * To change this template use File | Settings | File Templates.
 */

uses java.util.HashMap
uses org.w3c.dom.Element
uses tdic.cc.common.admindata.CleanseAdminData

uses java.util.ArrayList
uses java.util.List

class CleanseCCAdminData extends CleanseAdminData {
  /**
   * Get the list of admin data files.
   */
  override protected property get Filenames() : List<String> {
    var retval = super.Filenames;
    retval.add ("holidays.xml");
    return retval;
  }

  /**
   * Cleanse the admin data file.
   */
  override protected function cleanseAdminDataFile (filename : String, import : Element) : void {
    super.cleanseAdminDataFile (filename, import);

    if (filename == "admin.xml") {
      // The admin xml file doesn't contain all the Holiday data.  It is missing the holiday types.
      removeElements (import, {"Holiday"});

//      removeAuthorityLimitProfilesWithMissingCustomUser (import);
    }
  }

  /**
   * Remove AuthorityLimitProfile entities that reference a missing custom user.
   */
  protected function removeAuthorityLimitProfilesWithMissingCustomUser (import : Element) : void {
    var customUserElement : Element;
    var removeList = new ArrayList<Element>();
    var userPublicID : String;

    // Get Users
    var users = new HashMap<String,Element>();
    for (userElement in getChildElements (import, "User")) {
      users.put (userElement.getAttribute("public-id"), userElement);
    }

    // Process AuthorityLimitProfiles
    for (authorityLimitProfileElement in getChildElements (import, "AuthorityLimitProfile")) {
      customUserElement = getSingleElement (authorityLimitProfileElement, "CustomUser");
      userPublicID = customUserElement.getAttribute("public-id");

      if (userPublicID != "" and users.containsKey(userPublicID) == false) {
        print ("Removing AuthorityLimitProfile " + authorityLimitProfileElement.getAttribute("public-id")
                  + " because CustomUser " + userPublicID + " does not exist in admin data.");
        removeList.add (authorityLimitProfileElement);
      }
    }

    for (element in removeList) {
      import.removeChild (element);
    }
  }
}