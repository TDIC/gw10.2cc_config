package tdic.cc.common.batch.generalledger

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses tdic.cc.common.batch.generalledger.util.TDIC_GeneralLedgerHelper
uses tdic.cc.integ.plugins.generalledger.exception.TDIC_GeneralLedgerException

class TDIC_GeneralLedgerOutboundBatch extends BatchProcessBase {

  static final var _logger = LoggerFactory.getLogger("TDIC_GeneralLedger")

  /**
   * Key for looking up the failure notification email addresses from the integration database
   * (value: InfraIssueNotificationEmail).
   */
  static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "CCInfraIssueNotificationEmail"
  /**
   * Failure notification email addresses in case of any errors in batch process.
   */
  var _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
  /**
   * Notification email's subject for General Ledger Outbound batch failures.
   */
  static final var TERMINATE_SUB =
      "Terminate::${gw.api.system.server.ServerUtil.ServerId}- General Ledger Outbound batch Terminated"
  /**
   * Notification email's subject for General Ledger Outbound batch failures.
   */
  static final var EMAIL_SUBJECT_FAILURE =
      "General Ledger Outbound Batch Job Failure on server ${gw.api.system.server.ServerUtil.ServerId}"

  construct() {
    super(BatchProcessType.TC_GENERALLEDGEROUTBOUNDBATCH)
  }

  /**
   * Function dowork() is responsible to fetech and unprocessed CC transaction records and write to General Ledger
   * outbound file.
   */
  protected override function doWork() {
    _logger.debug("TDIC_GeneralLedgerOutboundBatch#doWork() - Entering")
    try {
      if (TerminateRequested) {
        _logger.debug("TDIC_GeneralLedgerOutboundBatch#doWork() - " +
            "Terminate requested during doWork() method before writing outbound file().")
        EmailUtil.sendEmail(_failureNotificationEmail, TERMINATE_SUB, "General Ledger outbound batch is terminated.")
        return
      }

      //Write CC GL transactions data into General Ledger outbound file.
      TDIC_GeneralLedgerHelper.writeGeneralLedgerOutboundFiles()
      incrementOperationsCompleted()

    } catch (e : TDIC_GeneralLedgerException) {
      _logger.error("Exception While writing CC GL transaction rcords to General Ledger outbound files.", e.toString())
      incrementOperationsFailed()
      EmailUtil.sendEmail(_failureNotificationEmail,
          EMAIL_SUBJECT_FAILURE, "Error writting oubound file to General Ledger: " + e.Message)
      throw e
    }

    _logger.debug("TDIC_GeneralLedgerOutboundBatch#doWork() - Exiting")

  }

  /**
   * To terminate batch process.
   * @return boolean
   */
  override function requestTermination() : boolean {
    // This will set the Termination request flag
    super.requestTermination()
    _logger.info("TDIC_GeneralLedgerOutboundBatch#requestTermination(): requestTermination Terminated.")
    // It will return true to signal that the attempt is made to stop the process in doWork method.
    return true
  }
}