package tdic.cc.common.batch.checkprinting

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses tdic.cc.common.batch.checkprinting.util.TDIC_CheckPrintingHelper
uses tdic.cc.integ.plugins.checkprinting.exception.CheckPrintingException

/**
 * TDIC_CheckPrintingOutboundBatch - File based integration to send check payment info to Lawson system.
 * Fetch unprocessed check payment records from table: CheckPaymentLines and write to check printing outbound
 * flat files(invoice, distribution, attachment).
 * Updates CheckPaymentLines processed to true once the check payment info written to outbound files.
 * Created by RambabuN on 11/20/2019.
 */
class TDIC_CheckPrintingOutboundBatch extends BatchProcessBase {

  static final var _logger = LoggerFactory.getLogger("TDIC_CheckPrinting")

  /**
   * Key for looking up the failure notification email addresses from the integration database
   * (value: InfraIssueNotificationEmail).
   */
  static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "CCInfraIssueNotificationEmail"
  /**
   * Failure notification email addresses in case of any errors in batch process.
   */
  var _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
  /**
   * Notification email's subject for CheckPrinting Outbound batch failures.
   */
  static final var TERMINATE_SUB =
      "Terminate::${gw.api.system.server.ServerUtil.ServerId}- CheckPrinting Outbound batch Terminated"
  /**
   * Notification email's subject for CheckPrinting Outbound batch failures.
   */
  static final var EMAIL_SUBJECT_FAILURE =
      "CheckPrinting Outbound Batch Job Failure on server ${gw.api.system.server.ServerUtil.ServerId}"

  construct() {
    super(BatchProcessType.TC_CHECKPRINTINGOUTBOUNDBATCH)
  }

  /**
   * Function dowork() is responsible to fetech and unprocessed check payment line records and write to check printing
   * outbound files for Lawson System.
   */
  protected override function doWork() {
 _logger.debug("TDIC_CheckPrintingOutboundBatch#doWork() - Entering")
    try {
      if (TerminateRequested) {
        _logger.debug("TDIC_CheckPrintingOutboundBatch#doWork() - " +
            "Terminate requested during doWork() method before writing outbound file().")
        EmailUtil.sendEmail(_failureNotificationEmail, TERMINATE_SUB, "CheckPrinting outbound batch is terminated.")
        return
      }

      //Write check payment details into invoice, distribution, attachment check printing outbound files.
      TDIC_CheckPrintingHelper.writeCheckPrintingOutboundFiles()
      incrementOperationsCompleted()

    } catch (e : CheckPrintingException) {
      _logger.error("Exception While writing Check payment records to CheckPrinting outbound files.", e.toString())
      incrementOperationsFailed()
      EmailUtil.sendEmail(_failureNotificationEmail,
          EMAIL_SUBJECT_FAILURE, "Error writting oubound file to CheckPrinting: " + e.Message)
      throw e
    }

    _logger.debug("TDIC_CheckPrintingOutboundBatch#doWork() - Exiting")

  }

  /**
   * To terminate batch process.
   * @return boolean
   */
  override function requestTermination() : boolean {
    // This will set the Termination request flag
    super.requestTermination()
    _logger.info("TDIC_CheckPrintingOutboundBatch#requestTermination(): requestTermination Terminated.")
    // It will return true to signal that the attempt is made to stop the process in doWork method.
    return true
  }

}