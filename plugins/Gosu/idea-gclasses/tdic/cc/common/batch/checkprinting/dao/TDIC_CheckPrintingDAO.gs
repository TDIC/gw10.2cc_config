package tdic.cc.common.batch.checkprinting.dao

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses org.slf4j.LoggerFactory
uses tdic.cc.integ.plugins.checkprinting.dto.CheckPrintingFile
uses tdic.cc.integ.plugins.checkprinting.dto.PaymentLineItemDTO
uses tdic.cc.integ.plugins.checkprinting.exception.CheckPrintingException
uses tdic.cc.integ.plugins.checkprinting.model.checkpaymentdtomodel.CheckPaymentDTO

uses java.sql.CallableStatement
uses java.sql.Connection
uses java.sql.ResultSet
uses java.sql.SQLException
uses java.sql.Statement
uses java.text.DecimalFormat

/**
 * TDIC_CheckPrintingDAO is responsible to
 * Insert check payment lines info to table: CheckPaymentLines of GWINT DB.
 * Update processed check payment lines records to true in table: CheckPaymentLines.
 * Fetch unprocessed records from table: CheckPaymentLines.
 * Created by RambabuN on 11/20/2019.
 */
class TDIC_CheckPrintingDAO {

  static final var _logger = LoggerFactory.getLogger("TDIC_CheckPrinting")

  //Insert SP Check Payment lines
  static final var INSERT_CC_PAYMENT_TRANSACTION = "dbo.SP_Insert_CheckPaymentLines ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"

  //Fectch SP Check Payment lines
  static final var SELECT_CHECKPAYMENTLINES_SP : String = "{ call dbo.SP_Select_CheckPaymentLines() }"

  //SQL query to update processed check payment lines to true.
  static final var UPDATE_CHECK_PAYMENT_LINES : String = "UPDATE CHECKPAYMENTLINES SET PROCESSED = 1 WHERE ID IN ("

  static final var CLOSE_PERENTHESIS = ")"
  static final var COMMA = ","
  static final var REMIT_CODE = "AH"
  static final var DECIMAL_FORMAT = "00"
  static final var EMPTY_STRING = ""

  var _conn : Connection
  //ID's for processed check payment lines.
  var _checkPaymentLinesRowIDs : String


  /**
   * Function to initialize the DB Connection Object.
   */
  function initialize() {
    _logger.debug("Initalizing DB connections - TDIC_CheckPrintingDAO.initialize()")

    _conn = DatabaseManager.getConnection(PropertyUtil.getInstance().getProperty("IntDBURL"))
    _conn.setAutoCommit(true)

    _logger.debug("Exit TDIC_CheckPrintingDAO.initialize()")
  }

  /**
   * Cleans up the database connection.
   */
  function cleanConnections() {
    DatabaseManager.closeConnection(_conn)
  }

  /**
   * Function to Insert Check Payment Line items to Integration GWINT DB table :CheckPaymentLines.
   * @param checkPayment
   */
  function insertPaymentLineItem(checkPayment : CheckPaymentDTO) {

    _logger.debug("Enter TDIC_CheckPrintingDAO.insertPaymentLineItem()")
    var _inCheckPaymentLinesCalStmt : CallableStatement

    try {
      if (checkPayment.LineItems?.Entry.HasElements) {
        _inCheckPaymentLinesCalStmt = _conn.prepareCall(INSERT_CC_PAYMENT_TRANSACTION)

        //For Every PaymentLineItem
        checkPayment.LineItems.Entry.each(\lineItem -> {

          _logger.debug("Inserting check payment line item [ CheckPublicID: ${checkPayment.CheckPublicID}, " +
          "LinePublicID ${lineItem.LinePublicID}, Line Amount ${lineItem.LineAmount} ]")
          _inCheckPaymentLinesCalStmt.setString(1, checkPayment.CheckNumber)
          _inCheckPaymentLinesCalStmt.setString(2, checkPayment.CheckPublicID)
          _inCheckPaymentLinesCalStmt.setString(3, checkPayment.VendorNumber)
          _inCheckPaymentLinesCalStmt.setString(4, checkPayment.InvoiceNumber)
          _inCheckPaymentLinesCalStmt.setString(5, checkPayment.CostCategory)
          _inCheckPaymentLinesCalStmt.setBigDecimal(6, checkPayment.NetAmount)
          _inCheckPaymentLinesCalStmt.setString(7, checkPayment.PayeeName)
          _inCheckPaymentLinesCalStmt.setString(8, lineItem.ClaimNumber)
          _inCheckPaymentLinesCalStmt.setString(9, lineItem.ClaimantName)
          _inCheckPaymentLinesCalStmt.setString(10, lineItem.InsuredName)
          _inCheckPaymentLinesCalStmt.setString(11, lineItem.VendorInvoiceNumber)
          _inCheckPaymentLinesCalStmt.setString(12, lineItem.LinePublicID)
          _inCheckPaymentLinesCalStmt.setInt(13, lineItem.LineSquenceNumber)
          _inCheckPaymentLinesCalStmt.setBigDecimal(14, lineItem.LineAmount)
          _inCheckPaymentLinesCalStmt.setString(15,checkPayment.IncomeCode)
          _inCheckPaymentLinesCalStmt.setString(16, checkPayment.VendorName)
          _inCheckPaymentLinesCalStmt.setString(17, checkPayment.Address1)
          _inCheckPaymentLinesCalStmt.setString(18, checkPayment.Address2)
          _inCheckPaymentLinesCalStmt.setString(19, checkPayment.City)
          _inCheckPaymentLinesCalStmt.setString(20, checkPayment.State)
          _inCheckPaymentLinesCalStmt.setString(21, checkPayment.PostalCode)
          _inCheckPaymentLinesCalStmt.setString(22, checkPayment.VoucherNumber)
          _inCheckPaymentLinesCalStmt.addBatch()
          _logger.info("Record for LinePublicID [" + lineItem.LinePublicID + "] " +
              " and line amount: ${lineItem.LineAmount} added to batch")
        })
      } else {
        _logger.error("Check or Payment is Null or Empty to insert data into GWINT database")
      }
      //Execute insert query
      _inCheckPaymentLinesCalStmt.executeBatch()
   } catch (ex : SQLException) {
      _logger.error(this.Class.Name + "Error Occurred while inserting Check payment " +
      "[ CheckPublicID: ${checkPayment.CheckPublicID}, Check Amount: ${checkPayment.NetAmount} ] ")

      //Throw exception
      throw new CheckPrintingException("Error Occurred while inserting Check payment " +
          "[ CheckPublicID: ${checkPayment.CheckPublicID}, Check Amount: ${checkPayment.NetAmount} ] ", ex)
    } finally {
      //Release connections.
      _inCheckPaymentLinesCalStmt.clearBatch()
      DatabaseManager.closeCallableStatement(_inCheckPaymentLinesCalStmt)
    }
    _logger.debug("Exit TDIC_CheckPrintingDAO.insertPaymentLineItem()")
  }

  /**
   * Function to fetch unprocessed CheckPaymentLines from table:CheckPaymentLines  integration DB GWINT.
   * @return checkPrintingFile
   */
  function fetchCheckPaymentLines() : CheckPrintingFile {
    _logger.debug("Enter TDIC_CheckPrintingDAO.fetchCheckPaymentLines()")
    var _selCheckPaymentLinesCalStmt : CallableStatement
    var  checkPrintingFile : CheckPrintingFile

    try {
      var _rowIds = new ArrayList<Integer>()
      var _checkNums = new ArrayList<String>()
      var previousCheckID : String
      var remitLocations = new HashMap<String, Integer>()

      _selCheckPaymentLinesCalStmt = _conn.prepareCall(SELECT_CHECKPAYMENTLINES_SP)
      var _resultSet = _selCheckPaymentLinesCalStmt.executeQuery()
      checkPrintingFile = new CheckPrintingFile()

      while (_resultSet.next()) {
        var checkPaymentDTO : tdic.cc.integ.plugins.checkprinting.dto.CheckPaymentDTO
        var lineItemDTO : PaymentLineItemDTO
        _rowIds.add(_resultSet.getInt(1))
        _checkNums.add(_resultSet.getString(2))
        if(previousCheckID != _resultSet.getString(5)) {
          //Capture check payment details into CheckPaymentDTO.
          populateCheckPaymentDTO(_resultSet, checkPrintingFile,remitLocations)
          //Capture check line details into PaymentLineItemDTO.
          populatePaymentLineItemDTO(_resultSet, checkPrintingFile)
          previousCheckID = _resultSet.getString(5)
        } else {
          //Capture check payment line details into PaymentLineItemDTO.
          populatePaymentLineItemDTO(_resultSet, checkPrintingFile)
          previousCheckID = _resultSet.getString(5)
        }
        //Join all the processed check payment line record ID's as string.
        _checkPaymentLinesRowIDs = _rowIds.join(COMMA)
        _logger.debug("${_rowIds.Count} CheckPaymentLine records are fetched successfylly.Check numbers: ${_checkNums}")
      }
    } catch (ex : SQLException) {
      _logger.error("Failed to fetch check payment lines form table GEINT:CheckPaymentLines.", ex)
      throw new CheckPrintingException(ex.Message, ex)
    } finally {
      //Release Connections
      DatabaseManager.closeCallableStatement(_selCheckPaymentLinesCalStmt)
    }
    _logger.debug("Exit TDIC_CheckPrintingDAO.fetchCheckPaymentLines()")
    return checkPrintingFile
  }

  /**
   * Function to capture check paymet details into CheckPaymentDTO from ResultSet.
   * @param resultSet
   * @param checkPrintingFile
   */
  private function populateCheckPaymentDTO(resultSet : ResultSet, checkPrintingFile : CheckPrintingFile, remitLocations : HashMap<String,Integer>) {

    _logger.debug("Enter TDIC_CheckPrintingDAO.populateCheckPaymentDTO()")
    var checkPaymentDTO = new tdic.cc.integ.plugins.checkprinting.dto.CheckPaymentDTO()
    checkPaymentDTO.CheckNumber = resultSet.getString(2)
    checkPaymentDTO.CheckPublicID = resultSet.getString(3)
    checkPaymentDTO.VendorNumber = resultSet.getString(4)
    checkPaymentDTO.InvoiceNumber = resultSet.getString(5)
    checkPaymentDTO.CostCategory = resultSet.getString(6)
    checkPaymentDTO.NetAmount = resultSet.getBigDecimal(7)
    checkPaymentDTO.PayeeName = resultSet.getString(8)
    checkPaymentDTO.IncomeCode = resultSet.getString(16)
    checkPaymentDTO.VendorName = resultSet.getString(17)
    checkPaymentDTO.Address1 = resultSet.getString(18)
    checkPaymentDTO.Address2= resultSet.getString(19)
    checkPaymentDTO.City = resultSet.getString(20)
    checkPaymentDTO.State = resultSet.getString(21)
    checkPaymentDTO.PostalCode = resultSet.getString(22)
    if(checkPaymentDTO.VendorName.NotBlank){
      checkPaymentDTO.RemitCode = setRemitCode(checkPaymentDTO,remitLocations)
    }
    else{
      checkPaymentDTO.RemitCode = EMPTY_STRING
    }
    checkPaymentDTO.VoucherNumber = resultSet.getString(23)
    checkPrintingFile.CheckPayments.add(checkPaymentDTO)
    _logger.debug("Exit TDIC_CheckPrintingDAO.populateCheckPaymentDTO()")

  }

  /**
   * Function to capture check payment line details into PaymentLineItemDTO from ResultSet.
   * @param _resultSet
   * @param checkPrintingFile
   */
  private function populatePaymentLineItemDTO(_resultSet : ResultSet, checkPrintingFile : CheckPrintingFile) {
    _logger.debug("Enter TDIC_CheckPrintingDAO.populatePaymentLineItemDTO")

    var lineItemDTO = new PaymentLineItemDTO()
    lineItemDTO.VendorNumber = _resultSet.getString(4)
    lineItemDTO.InvoiceNumber = _resultSet.getString(5)
    lineItemDTO.ClaimNumber = _resultSet.getString(9)
    lineItemDTO.ClaimantName = _resultSet.getString(10)
    lineItemDTO.InsuredName = _resultSet.getString(11)
    lineItemDTO.VendorInvoiceNumber = _resultSet.getString(12)
    lineItemDTO.LinePublicID = _resultSet.getString(13)
    lineItemDTO.LineSquenceNumber = _resultSet.getInt(14)
    lineItemDTO.LineAmount = _resultSet.getBigDecimal(15)
    lineItemDTO.VoucherNumber = _resultSet.getString(23)

    checkPrintingFile.PaymentLines.add(lineItemDTO)
    _logger.debug("Exit TDIC_CheckPrintingDAO.populatePaymentLineItemDTO")
  }

  /**
   * Function to update processed check payment line processed records status to true after writing check payment info
   * to outbound files.
   */
  function updateProcessedCheckPaymentLines() {
    _logger.debug("Enter TDIC_CheckPrintingDAO.updateProcessedCheckPaymentLines()")
    var _sqlStatement : Statement

    try {
      var sqlString = new StringBuilder().append(UPDATE_CHECK_PAYMENT_LINES).append(_checkPaymentLinesRowIDs)
          .append(CLOSE_PERENTHESIS)
      _sqlStatement = _conn.createStatement()
      _sqlStatement.executeUpdate(sqlString.toString())
      _conn.commit()
    } catch (ex : SQLException) {
      _logger.error("Failed to update check payment lines records status to processed.")
      throw new CheckPrintingException(ex.Message, ex)
    } finally {
      //Release Connections
      if (_sqlStatement != null) _sqlStatement.close()
    }
    _logger.debug("Exit TDIC_CheckPrintingDAO.updateProcessedCheckPaymentLines()")
  }

/*
*GWPS - 651 : Function to populate remit code when check mailing address or Check.PayTo is changed.
* Remit code is in AH01,AH02,AH03.... format. The remit code increases by 1 if the same vendor occurs more than once in the invoice file.
* @param : checkPaymentDTO,remitLocations
* @return : Remit Code
 */

  private function setRemitCode(checkPaymentDTO : tdic.cc.integ.plugins.checkprinting.dto.CheckPaymentDTO, remitLocations : HashMap<String,Integer> ) : String{

    if(remitLocations.containsKey(checkPaymentDTO.VendorNumber)){
      var count = remitLocations.get(checkPaymentDTO.VendorNumber)
      count++ ;
      remitLocations.replace(checkPaymentDTO.VendorNumber,count)
    }
    else{
      var count = 1
      remitLocations.put(checkPaymentDTO.VendorNumber,count)
    }
    var formatter = new DecimalFormat(DECIMAL_FORMAT);
    var number = formatter.format(remitLocations.get(checkPaymentDTO.VendorNumber));
    var remitCode = REMIT_CODE + number
    return remitCode
  }

}