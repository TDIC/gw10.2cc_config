package tdic.cc.common.batch.checkprinting.util

uses com.tdic.util.delimitedfile.DelimitedFileImpl
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.gx.GXOptions
uses gw.api.system.database.SequenceUtil
uses gw.api.util.StringUtil
uses gw.xml.XmlElement
uses org.apache.commons.io.FileUtils
uses org.slf4j.LoggerFactory
uses tdic.cc.common.batch.checkprinting.dao.TDIC_CheckPrintingDAO
uses tdic.cc.integ.plugins.checkprinting.dto.CheckPrintingFile
uses tdic.cc.integ.plugins.checkprinting.dto.PaymentLineItemDTO
uses tdic.cc.integ.plugins.checkprinting.exception.CheckPrintingException
uses tdic.cc.integ.plugins.checkprinting.model.checkpaymentdtomodel.CheckPaymentDTO

uses java.io.File
uses java.io.IOException
uses java.text.SimpleDateFormat

/**
 * TDIC_CheckPrintingHelper is responsible to
 * Insert check payment lines to table: CheckPaymentLines of GWINT DB.
 * Fetch unprocessed records from CheckPaymentLines and write to check printing outbound files.
 * Write XML formated CheckPayment and PaymentLine DTO's to Flat file.
 * Generate check Invoice number as like “CLMYYYYMMDD-00000X” where the X is the check sequence number.
 * Acknowledging check after insering CheckPaymentLines records., the check status from "Requesting" will be changed to "Requested"
 * Generating payload for CheckPaymentDTO from Check entity.
 * Created by RambabuN on 11/20/2019.
 */
class TDIC_CheckPrintingHelper {

  static final var _logger = LoggerFactory.getLogger("TDIC_CheckPrinting")

  static final var DATE_FORMAT_YYYY_MM_dd : String = "yyyyMMdd"

  static final var DATE_FORMAT_MM_dd_yyyy_HHmmss : String = "MM-dd-yyyy-HHmmss"

  static final var DATE_FORMAT_MM_dd_yy : String = "MMddyy"

  static final var CHECK_PRINTING_FILESDIRECTORY_NAME_PROPERTY = "checkprinting.outbound.filesdirectory.name"

  static final var CHECK_PRINTING_FILE_NAME_PROPERTY = "checkprinting.outbound.file.name"

  //File extensions for invoice, Distribution, Attachment check printing outbound files.
  static final var INVOICE_FILE_EXTENSION = ".apc"
  static final var DISTRIBUTION_FILE_EXTENSION = ".apc.dis"
  static final var ATTACHMENT_FILE_EXTENSION = ".apc.att"

  //Vendor specification files for invoice, Distribution, Attachment check printing outbound files.
  static final var INVOICE_VENDOR_SPEC_FILE_NAME = "CheckPrintingInvoiceSpec.xlsx"
  static final var DISTRIBUTION_VENDOR_SPEC_FILE_NAME = "CheckPrintingDistributionSpec.xlsx"
  static final var ATTACHMENT_VENDOR_SPEC_FILE_NAME = "CheckPrintingAttachmentSpec.xlsx"

  static final var NON_REPORTABLE = "NR"
  static final var ATTORNEY_PAY = "INDV"
  static final var OTHER_PAY = "MC7"
  static final var COST_TYPE_INDEMNITY = "INDEMNITY"
  static final var COST_TYPE_EXPENSE = "EXPENSE"

  static final var INVOICE_NUM_SEQUENCE_KEY = "invoice_num"
  static final var INVOICE_NUM_CLM = "CL"
  static final var INVOICE_SEQUENCE_NUM_FORMAT = "0000"
  static final var CHAR_UNDER_SCORE = "_"
  static final var CHAR_HYPHEN = "-"
  static final var EMPTY_STRING = ""

  /**
   * Function to Generate Invoice number “[Claim Number]_[voucherNumber]”.
   * @return
   */
  static function generateInvoiceNumber(claimNum : String, voucherNumber : String) : String {
    return new StringBuilder()
        .append(claimNum)
        .append(CHAR_UNDER_SCORE)
        .append(voucherNumber)
        .toString()
  }

  /**
   * Function to acknowledge Checks
   * <p>
   * Once the LineItems are successfully inserted into integration GWINT DB,
   * as a acknowledgement, the check status from "Requesting" will be changed to "Requested"
   * <p>
   * This function will all fire OOTB CheckStatusChanged and PaymentStatusChanged event because
   * the below method is invoked from TDIC_CheckPrintingMessgeTransportPlugin Class.
   * GW will not fire any events for entity data changes in EFR / Message Plugins
   *
   * @param check
   */
  static function acknowledgeCheck(check : Check) {

    _logger.debug("Check Status: ${check.Status.DisplayName}")

    // for Regular checks, If Check status is Requesting - change to Requested
    if (check.Status == TransactionStatus.TC_REQUESTING) {
      //Acknowledge Submission
      check.acknowledgeSubmission()
      if(check.Bulked){
        /*try{
          _logger.debug("Check belongs to Bulk Invoice with Invoice Number ${check.BulkInvoiceItem.BulkInvoice.InvoiceNumber}")
          var bulkInvoice = check.BulkInvoiceItem.BulkInvoice
          var countInRequesting = bulkInvoice.InvoiceItems.countWhere(\item -> item.BulkInvoiceItemInfo.Check.Status == TransactionStatus.TC_REQUESTING)
          if(countInRequesting==0){
            bulkInvoice.acknowledgeSubmission()
          } else {
            _logger.debug("Bulk Invoice with Invoice Number ${check.BulkInvoiceItem.BulkInvoice.InvoiceNumber} has ${countInRequesting} checks in Requesting status")
          }
        }catch(exception: Exception){
          _logger.error("Check belongs to Bulk Invoice with Invoice Number ${check.BulkInvoiceItem.BulkInvoice.InvoiceNumber} status update failed." , exception)
        }*/
      }
      //If check, trigger corresponding CheckStatusChanged Event
      check.addEvent(entity.Check.CHECKSTATUSCHANGED_EVENT)

      //trigger corresponding payment status changed Event
      check.Payments.each(\payment -> payment.addEvent(Payment.PAYMENTSTATUSCHANGED_EVENT))

    } else {
      _logger.debug("Check is not acknowledge check status is not Requesting.")
    }
  }

  /**
   * For every check transaction, insertPaymentLineItem function will invoke 'insertPaymentLineItem' function to insert
   * check line item level data into the database.
   *
   * @param checkObject
   */
  static function insertPaymentLineItem(checkPayment : CheckPaymentDTO) {

    var checkPrintingDAO = new TDIC_CheckPrintingDAO()
    try {
      //Initialize DAO
      checkPrintingDAO.initialize()
      checkPrintingDAO.insertPaymentLineItem(checkPayment)

      _logger.debug("insertPaymentLineItem(): Check Payment info : ${checkPayment.CheckPublicID} inserted successfully")
    }  catch (ex : CheckPrintingException) {
      _logger.error("Failed to insert Check Payment info for CheckPublicID# ${checkPayment.CheckPublicID}")
      throw ex
    }finally {
      //close any open connections
      checkPrintingDAO.cleanConnections()
    }
  }


  /**
   * Function to generate enrole inbound or outbound file based on file directry and name properties.
   * @param fileExtension
   * @return fileName
   */
  private static function getCheckPaymentFileName(fileExtension : String) : String {
    var propUtil = PropertyUtil.getInstance()
    var fileDir = propUtil.getProperty(CHECK_PRINTING_FILESDIRECTORY_NAME_PROPERTY)
    var fileName = propUtil.getProperty(CHECK_PRINTING_FILE_NAME_PROPERTY)
    return new StringBuilder().append(fileDir).append(fileName).append(CHAR_HYPHEN)
        .append(new SimpleDateFormat(DATE_FORMAT_MM_dd_yyyy_HHmmss).format(Date.CurrentDate))
        .append(fileExtension).toString()
  }


  /**
   * Function to generate check payment payload string from check entity.
   * @param check
   * @return check payment payload String.
   */
  static function getCheckPaymentDTOPayload(check : entity.Check) : String {
    _logger.debug("Enter TDIC_CheckPrintingHelper.getCheckPaymentDTOPayload()")

    var checkPaymentDTO : tdic.cc.integ.plugins.checkprinting.dto.CheckPaymentDTO
    if (check != null and check.Payments?.HasElements) {

      checkPaymentDTO = new tdic.cc.integ.plugins.checkprinting.dto.CheckPaymentDTO()
      checkPaymentDTO.CheckNumber = check.CheckNumber
      checkPaymentDTO.CheckPublicID = check.PublicID
      checkPaymentDTO.VendorNumber = check.FirstPayee.ClaimContact.Contact.VendorNumber
      checkPaymentDTO.InvoiceNumber = check.InvoiceNumber
      checkPaymentDTO.VoucherNumber = check.VoucherNumber_TDIC
      checkPaymentDTO.CostCategory =
          check.Payments.first().CostType == CostType.TC_CLAIMCOST ? COST_TYPE_INDEMNITY : COST_TYPE_EXPENSE
      //GINTEG-1483:Can't find Claims Payment in Lawson test.
      //As per the above defect GrossAmout is expected as part of invoice file.
      checkPaymentDTO.NetAmount = check.GrossAmount
      checkPaymentDTO.PayeeName = check.FirstPayee.ClaimContact.DisplayName
      checkPaymentDTO.IncomeCode =getIncomeCode(check)
      populateAdHocAddress(checkPaymentDTO, check)
      checkPaymentDTO.RemitCode = EMPTY_STRING
      checkPaymentDTO.LineItems = new ArrayList<PaymentLineItemDTO>()
      var lineItemSeqNum = 0

      //calculate the Number of LineItems on a check
      check.Payments.each(\pmt -> pmt.LineItems.each(\lineItem -> {
        lineItemSeqNum++

        var lineItemDTO = new PaymentLineItemDTO()
        lineItemDTO.LinePublicID = lineItem.PublicID
        lineItemDTO.LineSquenceNumber = lineItemSeqNum
        lineItemDTO.VendorNumber = checkPaymentDTO.VendorNumber
        lineItemDTO.InvoiceNumber = checkPaymentDTO.InvoiceNumber
        lineItemDTO.ClaimNumber = check.Claim.ClaimNumber
        lineItemDTO.ClaimantName = check.Claim.claimant.DisplayName
        lineItemDTO.InsuredName = check.Claim.Insured.DisplayName
        lineItemDTO.VendorInvoiceNumber = new SimpleDateFormat(DATE_FORMAT_MM_dd_yy).format(Date.CurrentDate)
        lineItemDTO.LineAmount = lineItem.Amount
        lineItemDTO.VoucherNumber = checkPaymentDTO.VoucherNumber
        checkPaymentDTO.LineItems.add(lineItemDTO)
      }))
    } else {
      _logger.error("Check or Payment is Null or Empty.")
    }

    var options = new GXOptions() { : Incremental = true}

    //Generate check payload xml
    var checkPaymentDTOModel = new tdic.cc.integ.plugins.checkprinting.model.checkpaymentdtomodel.CheckPaymentDTO()
        .create(checkPaymentDTO, options)

    _logger.debug("Exit TDIC_CheckPrintingHelper.getCheckPaymentDTOPayload()")

    return checkPaymentDTOModel.asUTFString()
  }

  /**
   * Function to get Income code.
   * @param check
   * @return
   */
  private static function getIncomeCode(check : Check) : String{
    var incomeCode : String
    var costType = check.Payments.first().CostType
    var payeeType = check.FirstPayee.PayeeType
    if(check.Reportability == ReportabilityType.TC_NOTREPORTABLE)
      incomeCode = NON_REPORTABLE
    else if(check.Reportability == ReportabilityType.TC_REPORTABLE){
      incomeCode = costType == CostType.TC_CLAIMCOST && payeeType == ContactRole.TC_ATTORNEY ? ATTORNEY_PAY : OTHER_PAY
    }
    return incomeCode
  }

  /**
   * Functuion to wtite check printing outbound files to outbound directory.
   * Invoice : TDIC-LA-CLM-MM-DD-YYYY-HHmmss.apc
   * Distribution : TDIC-LA-CLM-MM-DD-YYYY-HHmmss.apc.dis
   * Attachment : TDIC-LA-CLM-MM-DD-YYYY-HHmmss.apc.att
   */
  static function writeCheckPrintingOutboundFiles() {

    _logger.debug("Enter TDIC_CheckPrintingHelper.writeCheckPrintingOutboundFiles()")
    var checkPrintingDAO : TDIC_CheckPrintingDAO

    try {
      checkPrintingDAO = new TDIC_CheckPrintingDAO()
      //Initialize DAO
      checkPrintingDAO.initialize()

      //Fectch unprocessed check payment line items.
      var checkPrintingFile = checkPrintingDAO.fetchCheckPaymentLines()

      if(checkPrintingFile.CheckPayments.HasElements) {
        var apcInvoiceFile = new File(getCheckPaymentFileName(INVOICE_FILE_EXTENSION))
        var apcDistributionFile = new File(getCheckPaymentFileName(DISTRIBUTION_FILE_EXTENSION))
        var apcAttachmentFile = new File(getCheckPaymentFileName(ATTACHMENT_FILE_EXTENSION))

        var apcInvoiceLines = convertDataToFlatFileLines(INVOICE_VENDOR_SPEC_FILE_NAME, checkPrintingFile)
        var apcDistributionLines = convertDataToFlatFileLines(DISTRIBUTION_VENDOR_SPEC_FILE_NAME, checkPrintingFile)
        var apcAttachmentLines = convertDataToFlatFileLines(ATTACHMENT_VENDOR_SPEC_FILE_NAME, checkPrintingFile)

        FileUtils.writeLines(apcInvoiceFile, apcInvoiceLines)
        _logger.debug("Check printing outbound invoice file ${apcInvoiceFile} written successfully.")

        FileUtils.writeLines(apcDistributionFile, apcDistributionLines)
        _logger.debug("Check printing outbound distribution file ${apcDistributionFile} written successfully.")

        FileUtils.writeLines(apcAttachmentFile, apcAttachmentLines)
        _logger.debug("Check printing outbound attachment file ${apcAttachmentFile} written successfully.")

        /**On sucessfull writing check payment lines to file, Update processed flag to true in
         * table GEINT:CheckPaymentLines.*/
        checkPrintingDAO.updateProcessedCheckPaymentLines()
        _logger.debug("Updated processed check payment lines record status to true.")

        // Reset Check Invoice Sequence Number to 0 after writing check printing outbound files successfully.
        resetInvoiceSequenceNumber()
      }else {
        _logger.debug("There are no check payment lines to wtite into check printing Outbound file")
      }
    } catch (ioe : IOException) {
      _logger.error("IOException caught in batch process: ", ioe)
      throw new CheckPrintingException(ioe.Message, ioe)
    } catch (ex : Exception) {
      _logger.error("Exception while writing check payment lines to check printing outbound files", ex)
      throw new CheckPrintingException(ex.Message, ex)
    } finally {
      //close any open connections
      checkPrintingDAO.cleanConnections()
    }
    _logger.debug("Exit TDIC_CheckPrintingHelper.writeCheckPrintingOutboundFiles()")
  }

  /**
   * Converts data to the lines that can be written to the destination flat file.
   * @param vendorSpecName
   * @param flatFileData
   * @return List of processed rows.
   */
  @Param("vendorSpecName", "A String containing the name of the vendor spec spreadsheet to use when encoding the file")
  @Param("flatFileData", "The object containing the data to write to the destination flat file")
  @Returns("A List<String> containing the liens to be written to the destination flat file")
  @Throws(Exception, "If there are any unexpected errors encoding the data")
  static function convertDataToFlatFileLines(vendorSpecName : String, flatFileData : CheckPrintingFile) : List<String> {
    var gxModel = new tdic.cc.integ.plugins.checkprinting.model.checkprintingfilemodel.CheckPrintingFile()
        .create(flatFileData)
    _logger.debug("TDIC_CheckPrintingHelper#convertDataToFlatFileLines - Check Printing GX Model Generated: ")
    _logger.debug(gxModel.asUTFString())
    var xml = XmlElement.parse(gxModel.asUTFString())
    var df = new DelimitedFileImpl()
    return df.encode(vendorSpecName, xml)
  }

  /**
   * Function to reset Check Invoice Sequence Number to 0.
   */
  private static function resetInvoiceSequenceNumber() {

    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      var seqResult = Query.make(Sequence).compare(Sequence#SequenceKey, Equals, INVOICE_NUM_SEQUENCE_KEY).select()
      if(seqResult.Count > 0) {
        var invoiceSequece = seqResult.AtMostOneRow
        bundle.add(invoiceSequece)
        invoiceSequece.SequenceNumber = 0
      }
    })
  }

  /**
   * GWPS - 651 : Function to populate address and vendor name fileds in checkPaymentDTO when check mailing address or Check.PayTo is changed.
   * @param : checkPaymentDTO
   * @param : check
   */
  private static function populateAdHocAddress(checkPaymentDTO : tdic.cc.integ.plugins.checkprinting.dto.CheckPaymentDTO,check : Check){
    var vendor = check.Payees.firstWhere(\payee -> payee.ClaimContact.Contact.VendorNumber == checkPaymentDTO.VendorNumber)
    var payeeAddress = vendor.ClaimContact.Contact.PrimaryAddress
    var mailingAddress = check.MailingAddress
    var isMailingAddChanged =
        mailingAddress.AddressLine1 != payeeAddress.AddressLine1 or
        mailingAddress.AddressLine2 != payeeAddress.AddressLine2 or
        mailingAddress.City != payeeAddress.City or
        mailingAddress.State != payeeAddress.State or
        mailingAddress.PostalCode != payeeAddress.PostalCode
    var isNameChanged = check.PayTo.compareToIgnoreCase(vendor.ClaimContact.Contact.DisplayName) != 0
    if(isMailingAddChanged){
      checkPaymentDTO.VendorName = check.PayTo
      checkPaymentDTO.Address1 = mailingAddress.AddressLine1 != null ?  mailingAddress.AddressLine1 : EMPTY_STRING
      checkPaymentDTO.Address2 = mailingAddress.AddressLine2 != null ?  mailingAddress.AddressLine2 : EMPTY_STRING
      checkPaymentDTO.City = mailingAddress.City != null ? mailingAddress.City : EMPTY_STRING
      checkPaymentDTO.State = mailingAddress.State.Code != null ? mailingAddress.State.Code : EMPTY_STRING
      checkPaymentDTO.PostalCode = mailingAddress.PostalCode != null ?  mailingAddress.PostalCode : EMPTY_STRING
    }
    else {
      if(isNameChanged){
        checkPaymentDTO.VendorName = check.PayTo
      }
      else{
        checkPaymentDTO.VendorName = EMPTY_STRING
      }
      checkPaymentDTO.Address1 = EMPTY_STRING
      checkPaymentDTO.Address2 = EMPTY_STRING
      checkPaymentDTO.City = EMPTY_STRING
      checkPaymentDTO.State = EMPTY_STRING
      checkPaymentDTO.PostalCode = EMPTY_STRING
    }
    //GWPS- 2323 : String values having commas should be surrounded by double quotes.
    //  In condition checkPaymentDTO.VendorName.length() > 28 is verified because the total length of VendorName with double quotes will be 30 characters,
    // other conditions are verified similarly

    if (checkPaymentDTO.PayeeName.NotBlank && checkPaymentDTO.PayeeName.contains(",")){
      checkPaymentDTO.PayeeName = checkPaymentDTO.PayeeName.length() > 18 ? checkPaymentDTO.PayeeName.substring(0, 18) : checkPaymentDTO.PayeeName;
      checkPaymentDTO.PayeeName = "\"" + checkPaymentDTO.PayeeName + "\""
    }
    if (checkPaymentDTO.VendorName.NotBlank && checkPaymentDTO.VendorName.contains(",")){
      checkPaymentDTO.VendorName = checkPaymentDTO.VendorName.length() > 28 ? checkPaymentDTO.VendorName.substring(0, 28) : checkPaymentDTO.VendorName;
      checkPaymentDTO.VendorName = "\"" + checkPaymentDTO.VendorName + "\""
    }
    if (checkPaymentDTO.Address1.NotBlank && checkPaymentDTO.Address1.contains(",")){
      checkPaymentDTO.Address1 = checkPaymentDTO.Address1.length() > 28 ? checkPaymentDTO.Address1.substring(0, 28) : checkPaymentDTO.Address1;
      checkPaymentDTO.Address1 = "\"" + checkPaymentDTO.Address1 + "\""
    }
    if (checkPaymentDTO.Address2.NotBlank && checkPaymentDTO.Address2.contains(",")){
      checkPaymentDTO.Address2 = checkPaymentDTO.Address2.length() > 28 ? checkPaymentDTO.Address2.substring(0, 28) : checkPaymentDTO.Address2;
      checkPaymentDTO.Address2 = "\"" + checkPaymentDTO.Address2 + "\""
    }
    if (checkPaymentDTO.City.NotBlank && checkPaymentDTO.City.contains(",")){
      checkPaymentDTO.City = checkPaymentDTO.City.length() > 16 ? checkPaymentDTO.City.substring(0, 16) : checkPaymentDTO.City;
      checkPaymentDTO.City = "\"" + checkPaymentDTO.City + "\""
    }

  }

  /**
   * This method calls generateInvoiceNumber method to generate the Invoice Number using the Claim and Voucher Number.
   * It also checks if the Length of the generated Invoice Number is greated than 22. If so it trims the Voucher Number from
   * the beginning to generate an Invoice Number of 22 Characters.															  
   *    
   * @param claimNumber : String
   * @param voucherNumber : String
   * @return Invoice Number (String)
   */
  static function getInvoiceNumber_TDIC(claimNumber : String, voucherNumber : String)  : String{
    var trimmedClaimNumber = claimNumber.remove("-")
    var trimmedVoucherNumber = voucherNumber.remove(" ")
    var invoiceNumber = generateInvoiceNumber(trimmedClaimNumber, trimmedVoucherNumber)
    if (invoiceNumber.length > 22) {
      var extraLength = invoiceNumber.length - 22
      invoiceNumber = generateInvoiceNumber(trimmedClaimNumber, trimmedVoucherNumber.substring(extraLength, trimmedVoucherNumber.length))
		}
    return invoiceNumber
  }

}