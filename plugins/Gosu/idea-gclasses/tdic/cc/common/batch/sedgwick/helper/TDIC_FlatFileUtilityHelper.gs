package tdic.cc.common.batch.sedgwick.helper

uses java.util.ArrayList
uses java.util.HashMap
uses java.util.Properties
uses java.lang.Exception

uses com.tdic.util.properties.PropertyUtil
uses gw.api.util.ConfigAccess
uses tdic.util.flatfile.excel.ExcelDefinitionReader
uses java.io.FileInputStream
uses java.io.PrintWriter
uses gw.xml.XmlElement
uses org.apache.commons.lang.StringUtils

uses org.apache.commons.io.IOUtils
uses tdic.util.flatfile.model.classes.Record
uses tdic.cc.common.batch.sedgwick.dto.FlatFileContents
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: Praneethk
 * Date: 9/17/14
 * Time: 6:39 PM
 * To change this template use File | Settings | File Templates.
 */

class TDIC_FlatFileUtilityHelper {
  var properties:Properties                                           //Variable to hold Properties File
  private var _logger = LoggerFactory.getLogger("TDIC_SEDGWICK")                  //Standard Logger

  /**
   * Default Constructor
   * Loads Cache
   * This Cache Consist of Default Values Required in Flat File that are not provided in XML
   */
  construct(){
  }
  /**
   *This Method takes XML and TagName as arguments and returns the value corresponding to TagName in XML
   *This is Done by Splitting the tagName at "." and going through the hierarchy
   *At the end of the hierarchy the value is returned
   */
  public function getValueFromXML(xml:XmlElement, tagName:String):String{
    var tagHierarchy = tagName.split("\\.")     //Split the tagName
    for(level in tagHierarchy){                //Pass through Hierarchy
      try{
        xml= xml.$Children.singleWhere( \ elt -> elt.$QName.LocalPart==level)
      }
          catch(var e:Exception){
            return null
          }
    }
    return xml.$Text    //Return value
  }

  /**
   * This Method takes a String as input along with various vendor specifications for the particular string
   * The String is formatted according to the Vendor Specifications and returned.
   * Vendor Specifications include String (Length,Truncate,Strip,Fill,Justify,Format)
   */
  public function formatString(value:String, destLength:int, truncate:String, justify:String, fill:String, format:String, strip:String):String{
    //First strip unnecessary characters
    if(!strip.equals("'")){   //Default value is "'". So, remove chars if it is not the default value
      value=StringUtils.remove(value,strip) //Strip the values
    }
    if(fill=="'"){        //Default value of fill is "'" which is equal to a white space
      fill = " "
    }
    if(value==null){     //Handle null values
      value=fill
    }
    if(format=="S9(9)v99"){// Financial fields has a special requirement. Last two digits are considered as decimals. $ is replaced by +
      var currencyArray = value.split("\\.")                                   //Split at dot.
      if(currencyArray.length==2){                                             //Valid currency
        value = currencyArray[0]+StringUtils.rightPad(currencyArray[1],Coercions.makePIntFrom("0"),2 as char)  //Right pad decimals with 0 and append currency value to it
        value = StringUtils.leftPad(value,destLength-1,"0")                    //Left pad value to destLength-1 with zeros
        value = "+"+value                                                      //Append + sign
      }
    }
    else {
      if(format[0]+""=="9"){              //If value is an integer, discard decimals
        value = value.split("\\.")[0]
      }}
    var newValue = value
    if(value.length<destLength ){        //String length is less than required length. Do padding
      if(justify=="Right"){              //Right pad with fill values
        newValue = StringUtils.leftPad(value,destLength,fill)
      }
      else if(justify=="Left"){          //Left pad with fill values
        newValue = StringUtils.rightPad(value,destLength,fill)
      }
    }
    else if(value.length>destLength ) {  //String length is greater than required length. Truncate
      if(truncate == "Right"){           //Truncate right
        newValue = value.substring(0,destLength)
      }
      else if(truncate == "Left"){       //Truncate Left
        newValue = value.substring(value.length-destLength,value.length)
      }
    }
    return newValue
  }

  /**
   *This Method Takes Vendor specifications excel file path,xml destination path where flat file should be written
   *The Vendor Specification Excel Sheet is parsed to get Vendor specifications.
   *For each entry in vendor specification file the corresponding value is extracted from xml and is written to flat file in the specified format.
   *at specified location.
   */
  public function encode(excelFilePath:String,excelFileHeaderPath:String, xml:XmlElement,  flatFilePath:String):boolean{
    excelFilePath =  ConfigAccess.getConfigFile(excelFilePath).Path
    excelFileHeaderPath = ConfigAccess.getConfigFile(excelFileHeaderPath).Path
    var flatFile:PrintWriter
    var excel:ExcelDefinitionReader
    var vendorStructure:ArrayList<Record>
    try{
      flatFile = new PrintWriter(flatFilePath)     //Open flat file for writing
    }
        catch(var e:Exception){
          _logger.error("Unable To Create Flat File", e)
          return false
        }
    try{
      excel = new ExcelDefinitionReader(excelFileHeaderPath)
    }
        catch(var e:Exception){
          _logger.error("Unable To Read Excel File Header" + excel?.toString(), e )
          return false
        }
    try{

      vendorStructure = excel.read(excelFilePath)  //Read Excel Definition
    }
        catch(var e:Exception){
          _logger.error("Unable To Read Excel Defination File", e)
          return false
        }
    try{
      for(record in vendorStructure){                  //For each record in vendor file
        _logger.debug("/"+record.RecordType)
        var flatFileLine=""                              //Create a new flat file line
        for(field in record.Fields){                     //For each field in the record
          var value = getValueFromXML(xml, field.Name)   //Get value from Xml
          if(value == null){                             //If there is no value in XML, check in properties file for defaults
            value = PropertyUtil.getInstance().getProperty(field.Name)
          }
          flatFileLine += formatString(value, field.Length as int, field.Truncate, field.Justify, field.Fill, field.Format, field.Strip) //Format string and add it to flat file line
        }
        _logger.debug(flatFileLine)
        flatFile.println(flatFileLine)                   //Write to flat file
      }
      flatFile.close()                                 //Close flat file
    }
        catch(var e:Exception){
          _logger.error("Error While Generating Flat File", e)
          return false
        }
    return true
  }

  public function decode(excelFilePath:String,excelFileHeaderPath:String,flatFilePath:String): FlatFileContents{
    excelFilePath =  ConfigAccess.getConfigFile(excelFilePath).Path
    excelFileHeaderPath = ConfigAccess.getConfigFile(excelFileHeaderPath).Path
    var flatFile:PrintWriter
    var excel:ExcelDefinitionReader
    var vendorStructure:ArrayList<Record>

    try{
      excel = new ExcelDefinitionReader(excelFileHeaderPath)
    }
        catch(var e:Exception){
          e.printStackTrace()
          _logger.error("Unable To Read Excel File Header "+e.Message + " "+excelFileHeaderPath, e)
          return null
        }

    try{
      vendorStructure = excel.read(excelFilePath)  //Read Excel Definition
    }
        catch(var e:Exception){
          e.printStackTrace()
          _logger.error("Unable To Read Excel Defination File", e)
          return null
        }
    //var flatFileContents = IOUtils.toString(new FileInputStream(flatFilePath))
    var inputStream = new FileInputStream(flatFilePath)
    var flatFileContents = IOUtils.toString(inputStream,"UTF-8")
    inputStream.close()
    var flatFileContentArray = flatFileContents.split("\n")


    var multipleFlatFileContentsMap = new HashMap<String,HashMap<String,ArrayList<HashMap<String, String>>>>()
    var multipleFlatFileLineMap = new HashMap<String,HashMap<String,ArrayList<String>>>()

    for(var line in flatFileContentArray){
      var flag = false
      for(record in vendorStructure){
        var recordType = record.RecordType.split(" ")
        if(line.startsWith(recordType[0])){
          flag = true
          var claimNumber = line.substring(21,39).trim()
          if(!multipleFlatFileContentsMap.containsKey(claimNumber)){
            multipleFlatFileContentsMap.put(claimNumber,new HashMap<String,ArrayList<HashMap<String, String>>>())
            multipleFlatFileLineMap.put(claimNumber,new HashMap<String,ArrayList<String>>())
          }
          var flatFileContentsMap = multipleFlatFileContentsMap.get(claimNumber)
          var flatFileLineMap = multipleFlatFileLineMap.get(claimNumber)

          var innerMap =  new HashMap<String, String>()

          for(field in record.Fields){
            if((field.Name!=null) and (!field.Name.equals("N/A"))){
              innerMap.put(field.Name, line.substring(field.StartPosition-1,field.StartPosition+field.Length-1).trim())
            }
          }
          if(flatFileContentsMap.containsKey(recordType[0])){
            var innerMapArrayList = flatFileContentsMap.get(recordType[0])
            innerMapArrayList.add(innerMap)
            flatFileContentsMap.put(recordType[0],innerMapArrayList)
            flatFileLineMap.get(recordType[0]).add(line)
          }
          else{
            var innerMapArrayList = new ArrayList<HashMap<String, String>>();
            innerMapArrayList.add(innerMap)
            flatFileContentsMap.put(recordType[0],innerMapArrayList)
            flatFileLineMap.put(recordType[0],new ArrayList<String>())
            flatFileLineMap.get(recordType[0]).add(line)
          }
          break
        }

      }
      if(!flag){
        _logger.error("Encountered a line that doesn't start with header")
        return null
      }
    }
    return new FlatFileContents(multipleFlatFileContentsMap,multipleFlatFileLineMap)
  }
}