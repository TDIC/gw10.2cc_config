package tdic.cc.common.batch.sedgwick

uses com.tdic.util.properties.PropertyUtil
uses gw.api.util.ConfigAccess
uses gw.processes.BatchProcessBase

uses org.apache.commons.io.FileUtils
uses java.io.File
uses java.util.Arrays
uses java.util.ArrayList
uses com.tdic.util.misc.EmailUtil
uses gw.pl.exception.GWConfigurationException
uses java.lang.System
uses com.tdic.util.misc.FtpUtil
uses gw.pl.util.FileUtil
uses java.sql.Connection
uses java.sql.PreparedStatement
uses com.tdic.util.database.DatabaseManager
uses java.util.HashSet
uses java.sql.Statement
uses java.lang.Exception
uses java.lang.NullPointerException

uses org.slf4j.LoggerFactory
uses com.tdic.util.properties.PropertyUtil
uses com.tdic.util.database.DatabaseUtil
uses tdic.cc.common.batch.sedgwick.helper.TDIC_UpdateFeed

/**
 * Created with IntelliJ IDEA.
 * User: Praneethk
 * Date: 3/30/15
 * Time: 9:07 PM
 * US674 Claim updates from Sedgwick
 * This class overrides dowork() method of batch process and initiates batch process
 * for processed claim feed which was sent by sedgwick via SFTP.
 */
class TDIC_UpdateFeedBatchProcess extends BatchProcessBase {

   private static var _logger = LoggerFactory.getLogger("TDIC_SEDGWICK")
   private static var CLASS_NAME : String = "TDIC_UpdateFeedBatchProcess"	
   private var _emailTo : String as readonly EmailTo
   private var _emailRecipient : String as readonly EmailRecipient	
   public static final var SEDGWICK_EMAIL_TO_PROP_FIELD : String = "sedgwick.error.email"
   public static final var SEDGWICK_EMAIL_RECIPIENT : String = "sedgwick.notification.email"
   public static final var EMAIL_SUBJECT_COMPLETED: String = "FeedFromSedgwick Batch Job Completed on server " + gw.api.system.server.ServerUtil.ServerId
   public static final var EMAIL_SUBJECT_FAILURE: String = "FeedFromSedgwick Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId	
   private var headerPath : String
   private var excelPath : String
   private var correctedPath : String
   private var encryptedPath : String
   private var errorPath : String
   private var privateKeyFile : String
   private var passphrase : String

   construct() {
	  super(BatchProcessType.TC_FEEDFROMSEDGWICK)
      	headerPath = PropertyUtil.getInstance().getProperty("headerPath")
      	excelPath = PropertyUtil.getInstance().getProperty("sedgwick.updateFromSedgwickExcelPath")
      	correctedPath = PropertyUtil.getInstance().getProperty("sedgwick.inputDir.corrected")
      	encryptedPath = PropertyUtil.getInstance().getProperty("sedgwick.inputDir.encrypted")
      	errorPath = PropertyUtil.getInstance().getProperty("sedgwick.errorDir")
      	privateKeyFile =  PropertyUtil.getInstance().getProperty("sedgwick.privatekey")
      	passphrase =  PropertyUtil.getInstance().getProperty("sedgwick.passphrase")
   }

/**
 * US674 Claim updates from Sedgwick
 * praneethk
 * Determines if the process should run the doWork() method, based on successful retrieval of configured properties from int database.
 */

  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  
  override function checkInitialConditions() : boolean {
    var logPrefix = "${CLASS_NAME}#checkInitialConditions()"
    _logger.debug("${logPrefix} - Entering")
    _emailTo = PropertyUtil.getInstance().getProperty(SEDGWICK_EMAIL_TO_PROP_FIELD)
    if (_emailTo == null) {
       throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"+SEDGWICK_EMAIL_TO_PROP_FIELD+"' from integration database.")
    }
    _emailRecipient = PropertyUtil.getInstance().getProperty(SEDGWICK_EMAIL_RECIPIENT)
    if (_emailRecipient == null) {
       throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"+SEDGWICK_EMAIL_RECIPIENT+"' from integration database.")
    }
    _logger.debug("${logPrefix} - Exiting")
    return Boolean.TRUE
  }

/*
 * US674 Claim updates from Sedgwick
 * Runs the actual batch process
 */
   
  override function doWork() {
    _logger.debug("TDIC_UpdateFeedBatchProcess#doWork() - Entering")

    //Check For any Processing Failed Files in Error Directory. If Found Batch Will Terminate From #doWork()
    if (!isEmpty(errorPath)){   // No further processing if there are files in error dir
       _logger.error("TDIC_UpdateFeedBatchProcess - #doWork() - There are files in error directory. Handle them before running new jobs")
       EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE,
           "The FeedFromSedgwick batch process file cannot Update the incoming file.There are files in error directory. Handle them prior to running a new job")
       return
    }

    if (TerminateRequested){
       EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch process file was terminated by request on file")
       return
    }

/*
 * Check For any Files to be processed in Corrected Directory. These Files are in decrypted format as they are moved
 * from error directory after rectifying the error, which stopped processing previously.
 * If File is found in this folder, Batch Process will first process this file before processing the file in encrypted folder.
 * There can be one file atmost in this directory. If Error occurs in processing the file, the same is reflected in Error Directory
 */
	 
    var correctedFolder = new File(correctedPath)
    var correctedFiles = correctedFolder.listFiles()  //Get files from corrected folder. There will be one file at most
    var processedFiles = processFiles(correctedFiles,correctedPath) //Update files from corrected dir

    if(processedFiles.size()==1){  //If there is a file in corrected Dir
      var processedFile = processedFiles[0]
      if(processedFile.length==0){
        EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch process file cannot be process due to blank file")
        return
      }
      else{
      _logger.debug("TDIC_UpdateFeedBatchProcess - #doWork() - Processed File" + processedFile)
      FileUtils.forceDelete(new File(processedFile))
      }
    }
	
    if(!isEmpty(errorPath)){  //An error occured while updating files from corrected dir.
      _logger.error("TDIC_UpdateFeedBatchProcess - #doWork() - There is an error in processing corrected files")
      EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE,
          "The FeedFromSedgwick batch process file cannot Update the incoming file.There is an error in processing corrected files")
      return
    }

    if(processedFiles.size()!=correctedFiles.length){
      _logger.error("TDIC_UpdateFeedBatchProcess - #doWork() - Didn't process corrected files successfully")
      EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch did not process the file in the corrected folder successfully.")
      return
    }

    if (TerminateRequested){
       _logger.error("TDIC_UpdateFeedBatchProcess - #doWork() - File was Terminated Before Processing while running from Corrected Folder")
       EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch process file was terminated by request.")
       return
    }

/*
 * Check For any Files to be processed in Encrypted Directory. This method will get the files from encrypted folder
 * in the order it was recieved and will be processed accordingly by decrypting the file using PGPUtility and store
 * the file in a temporary directory and process from temp directory. After processing successfully/even incase of error
 * the file will be deleted recursively/ moved to error directory.
 */
	 
    getFilesFromFTP()
    var encryptedFolder = new File(encryptedPath)
    var encryptedFiles =encryptedFolder.listFiles()   //Get all files from the encrypted folder
    Arrays.sort(encryptedFiles)  //Sort the files. Sorting makes sure that the flat files are processed in the order they are created

    if (encryptedFiles.length==0){
       EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch process found No Files To Process Today.")
       return
    }

    var decryptedFiles = new File[encryptedFiles.length]
    var tempDirPath = System.getProperty("java.io.tmpdir") //Get temp dir

    for (var i in 0..encryptedFiles.length-1) {
      if (TerminateRequested){
         _logger.error("TDIC_UpdateFeedBatchProcess - #doWork() - File was Terminated Before Processing Before Decrypting")
         break
      }

      var encFile = encryptedFiles[i]
      var encFileName = encFile.Name
	  
      try {
         UtilPGP.PGPUtility.decrypt(ConfigAccess.getConfigFile(privateKeyFile).Path,passphrase,encFile.toString(),tempDirPath+encFileName) // Decrypt the encrypted file to temp dir
         decryptedFiles[i]=new File(tempDirPath+encFileName)  											                      //Add temp file to an array
      }
      catch (e:NullPointerException){
        EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch process failed. Unable to Decrypt File/Received Decrypted File.")
        _logger.error("TDIC_UpdateFeedBatchProcess# PGP - Exception = " + e)
        FileUtils.moveFile(encFile,new File(errorPath+encFileName))
        throw(e)
      }
      catch (e:Exception) {
        _logger.error("TDIC_UpdateFeedBatchProcess - #doWork() - Empty File Cannot Be Processed")
        EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch process failed. File is Empty/Blank and cannot be decrypted.")
        FileUtils.moveFile(encFile,new File(errorPath+encFileName))
        throw(e)
      }
    }
    
    if (TerminateRequested){
      _logger.error("TDIC_UpdateFeedBatchProcess - #doWork() - File was Terminated Before Processing While Running From Encrypted Folder")
       EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch process file was terminated by request on file")
       return
    }
	
    processedFiles = processFiles(decryptedFiles,System.getProperty("java.io.tmpdir")) 	//Process decrypted files

    for (file in decryptedFiles){   													//Delete all decrypted files
		    if (file!=null){   														    //All files might not be decrypted when the process is terminated
		       FileUtil.deleteDirectoryRecursively(file)
        }
    }

    for (lastProcessedFile in processedFiles){   										//Delete all encrypted files that are processed
        FileUtil.deleteDirectoryRecursively(new File(encryptedPath+lastProcessedFile.replace(tempDirPath,"")))
    }

    if (!isEmpty(errorPath)){  //There is a file in error dir
       _logger.error("TDIC_UpdateFeedBatchProcess - #doWork() - There is an error in processing encrypted files.")
       EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE,
           "The FeedFromSedgwick batch process file cannot Update the incoming file.There is an error in processing encrypted files.")
       return
    }
	
    _logger.debug("TDIC_UpdateFeedBatchProcess#doWork() - Exiting without errors")
    EmailUtil.sendEmail(_emailRecipient, EMAIL_SUBJECT_COMPLETED,
        "The FeedFromSedgwick batch process file Update Complete. Incoming File is Processed Without Errors.")
  }

/*
 * This Method will get the file to be processed each day by acquiring the non downloaded files from FTP.
 */

   function getFilesFromFTP(){

      var fileList = FtpUtil.getFileList("sedgwick") //Get all files in ftp
      var downloadedFiles = getDownloadedFiles() //Get files that are already downloaded
      var filesToDownload = fileList.subtract(downloadedFiles) //Get files that are not yet downloaded from ftp

      if (filesToDownload.size() == 0){
         _logger.error("TDIC_UpdateFeedBatchProcess - #doWork() - There are No Files To Download and Process Today.")
      }

      for (var fileName in filesToDownload){
          FtpUtil.downloadFile("sedgwick", new File(encryptedPath + fileName))
          updateFileName(fileName)
      }
   }

/*
 * This Method will Update The downloaded File information from ftp in the integration Database, to prevent downloading and processing duplicate files.
 */
   
   function updateFileName(fileName:String){

      var _con: Connection = null
      var _prepStatement : PreparedStatement = null
      try {
          _con = DatabaseUtil.getIntegDBConnection()
          var _prepSqlStmt = "INSERT INTO SedgwickFiles(FileName) VALUES(?)"
          _prepStatement = _con.prepareStatement(_prepSqlStmt)
          _prepStatement.setString(1,fileName)
          var _return =_prepStatement.executeUpdate()
          _con.commit()
     }
     finally {
        _con.close()
     }
   }

/*
 * This Method will get the downloaded file which is stored in the integration database.
 */
   
   function getDownloadedFiles():HashSet<String>{

      var _con: Connection = null
      var _statement : Statement = null
      var fileSet = new HashSet<String>()
	  
      try {
          _con = DatabaseUtil.getIntegDBConnection()
          var _sqlStmt = "SELECT FileName FROM SedgwickFiles"
          _statement = _con.createStatement()
          var _return =_statement.executeQuery(_sqlStmt)
          while (_return.next()){
                fileSet.add(_return.getString("FileName"))
          }
      }
      finally {
		      _statement.close()
		      _con.close()
      }
    return fileSet
   }

/*
 * This Method will check if the provided path for the directory is empty.
 * If Empty Return True Else False
 */
   
   function isEmpty (dirPath:String):boolean{
     if(!new File(dirPath).exists()){
       EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch process failed. Cannot Access destDir. The Directory May Not Exist or There is an Error in network Connection")
       throw new Exception("Cannot Access ${dirPath}. The Directory May Not Exist or There is an Error in network Connection")
     }
      var folder = new File(dirPath)
      var files = folder.listFiles()
      return files.length==0?true:false
   }

/*
 * The processFiles Method is the main method will which call the base helper class to process the decrypted data.
 * This Method will return the names of the processedFiles.
 */
  
   function processFiles (decryptedFileArray:File[],filePath:String): ArrayList<String> {

	 var processedFiles = new ArrayList<String>()
      	for (var decFile in decryptedFileArray){
		  if (TerminateRequested){
            _logger.error("TDIC_UpdateFeedBatchProcess - #processFiles() - File was Terminated During Processing")
            return processedFiles
          }
		  
		  /* "GW:752 BlankFile Error"*/
		  if (decFile.length()==0){
            _logger.info("Blank File"+"*************************************")
            EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch process failed. Unable to process a blank file")
            FileUtils.moveFile(decFile,new File(errorPath+decFile.getName()))
            _logger.error("TDIC_UpdateFeedBatchProcess - #processFiles() - Unable to process blank file")
			return processedFiles
		  }

          incrementOperationsCompleted()
          
          var decFileName=decFile.Name //Get only the file name by replacing the directory path with blank string
          _logger.debug("decFileName"+decFileName)
		  var updateFeed = new TDIC_UpdateFeed(decFile.toString())

          if(updateFeed.flatFileContents == null){
            EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch process failed due to an error in reading the flat file. The flat file has at least one line that does not start with header.")
            _logger.error("TDIC_UpdateFeedBatchJob#doWork() - Error in reading flat file. Flat file has atleast one line that do not start with header.")
            FileUtils.moveFile(decFile,new File(errorPath+decFile.getName()))
            return processedFiles
          }

          var errorString = updateFeed.updateAll()
		  
          processedFiles.add(decFile.toString())
		  
          if (errorString.length>0){        //If there is an error in processing the file
			       
			incrementOperationsFailed()
            _logger.error("TDIC_UpdateFeedBatchJob#doWork() - Processing cannot be finished with errors. There is some error. Writing to "+errorPath+decFileName)
            FileUtil.write(new File(errorPath+decFileName),errorString) //Write corresponding lines to error dir
            _logger.error("TDIC_UpdateFeedBatchProcess - #processFiles() - Processing cannot be finished with errors. There is some error")
            EmailUtil.sendEmail(_emailTo, EMAIL_SUBJECT_FAILURE, "The FeedFromSedgwick batch process failed due to errors. Processing cannot be finished with errors. There is some error.")
            return processedFiles
          }
        }
     return processedFiles
    }

/**
 * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.   *
 * @return A boolean to indicate that the process can be stopped.
 */
   override function requestTermination() : boolean {
     _logger.info("TDIC_UpdateFeedBatchProcess#requestTermination() - Terminate requested for batch process.")
     super.requestTermination()// Set TerminateRequested to true by calling super method
     return true
   }
}