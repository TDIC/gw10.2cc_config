package tdic.util.flatfile.utility.encoder

uses gw.pl.logging.LoggerCategory
uses tdic.util.flatfile.model.gx.flatfilemodel.FlatFile
uses tdic.util.flatfile.model.gx.flatfilemodel.anonymous.elements.FlatFile_Records_Entry
uses tdic.util.flatfile.model.gx.recordmodel.anonymous.elements.Record_Fields_Entry
uses java.io.BufferedWriter
uses java.io.File
uses java.io.FileWriter
uses java.lang.IllegalArgumentException
uses java.lang.StringBuilder
uses java.text.ParseException
uses java.text.SimpleDateFormat
uses java.util.ArrayList
uses org.slf4j.LoggerFactory

/**
 * US688
 * 09/03/2014 Shane Murphy
 *
 * Handles the encoding of a FlatFile GX Model
 */
class FlatFileEncoder extends FileEncoder {
  private var _encodedFile: String[] as EncodedFile
  private var _xml: FlatFile as XML
  private var _logger = LoggerFactory.getLogger("FLAT_FILE_UTILITY")
  /**
   * US688
   * 09/03/2014 shanem
   *
   * Constructs a FlatFileEncoder by parsing the xml file passed in
   */
  @Param("aFile", "an XML adhering to the Flat File XSD")
  @Param("aVendorDef", "The vendor definition to encode with")
  construct(aFile: FlatFile, aVendorDef: tdic.util.flatfile.model.gx.flatfilemodel.FlatFile) {
    EncodedFile = encode(aFile, aVendorDef)
    XML = aFile
  }

  /**
   * US688
   * 09/11/2014 shanem
   *
   * Encodes an XML document to a flat file using the vendor definition provided.
   */
  @Param("aFile", "The XML File to be encoded")
  @Param("aVendorDef", "The vendor definition to encode with.")
  final override function encode(aFile: FlatFile, aVendorDef: tdic.util.flatfile.model.gx.flatfilemodel.FlatFile): String[] {
    _logger.trace("FlatFileEncode#encode(FlatFile, FlatFile) - Entering")
    var array = new ArrayList<String>()
    var trailer: String
    var header: String
    for (var record in aFile.Records.Entry) {
      var vendorDefRecordInfo = aVendorDef.Records.Entry.firstWhere(\recordEntry -> recordEntry.Identifier == record.Identifier)
      var encodedRecord = encodeFields(record, vendorDefRecordInfo)
      // US688, 09/10/2014, shanem: Separate the header and trailer to place them at the start and end of the array list
      if (record.RecordType == "FileHeader") {
        header = encodedRecord
      } else if (record.RecordType == "FileTrailer") {
        trailer = encodedRecord
      } else {
        array.add(encodedRecord)
      }
    }
    array.add(0, header)
    array.add(array.size(), trailer)
    _logger.trace("FlatFileEncode#encode(FlatFile, FlatFile) - Exiting")
    return array?.toTypedArray()
  }

  /**
   * US688
   * 09/11/2014 shanem
   *
   * Encodes a record by iterating over and encoding each field, inserting it at the Start Position defined in the vendorDefinition
   */
  @Param("aRecord", "The XML File to be encoded")
  @Param("aVendorDef", "The vendor definition to encode with.")
  internal function encodeFields(aRecord: FlatFile_Records_Entry, aVendorDef: FlatFile_Records_Entry): String {
    _logger.trace("FlatFileEncode#encodeFields(FlatFile, FlatFile) - Entering")
    var sb = new StringBuilder()
    sb.append(aRecord.Identifier)
    for (field in aRecord.Fields.Entry) {
      var fieldDef = aVendorDef.Fields.Entry.firstWhere(\fieldEntry -> fieldEntry.Name == field.Name)
      var fieldData = field.Data

      if (fieldData.length < fieldDef.Length) {
        fieldData = fillFieldData(fieldData, fieldDef)
      } else if (fieldData.length > fieldDef.Length){
        fieldData = truncateFieldData(fieldData, fieldDef)
      }
      checkFieldDataCorrectlyFormatted(fieldData, fieldDef)

      sb.insert(fieldDef.StartPosition, fieldData)
    }
    _logger.trace("FlatFileEncode#encodeFields(FlatFile, FlatFile) - Exiting")
    return sb.toString()
  }

  /**
   * US688
   * 09/05/2014 shanem
   *
   * Writes the generated encoded records to a specified file path
   */
  @Param("aWritePath", "Path to where to write the encoded output.")
  override function writeToFile(aWritePath: String): void {
    _logger.trace("FlatFileEncode#writeToFile(String) - Entering")
    var file = new File(aWritePath);
    if (!file.exists()) {
      file.createNewFile();
    }
    using (var writer = new BufferedWriter(new FileWriter(file))) {
      _logger.debug("FlatFileEncode#writeToFile(String) - Writing file to ${aWritePath}")
      for (line in EncodedFile) {
        writer.write(line)
        writer.newLine()
      }
    }
    _logger.trace("FlatFileEncode#writeToFile(String) - Exiting")
  }

  /**
   * US688
   * 09/11/2014 shanem
   *
   * Fill field data for strings that are not as long as defined in their associated record definition
   */
  @Param("aFieldData", "The field data to be filled")
  @Param("aFieldDefinition", "The field definition related to the field being encoded")
  @Returns("Filled field data")
  internal function fillFieldData(fieldData: String, fieldDefinition: Record_Fields_Entry): String {
    _logger.trace("FlatFileEncode#fillFieldData(String, Record_Fields_Entry) - Entering")
    if ((fieldDefinition.Fill == null || fieldDefinition.Fill == "") || (fieldDefinition.Length == null || fieldDefinition.Length == 0)){
      throw new IllegalArgumentException("Can't fill data when fieldDefinition has no defined fill or length")
    }
    if (fieldData.length == fieldDefinition.Length){
      return fieldData
    }
    var dataString = new StringBuilder()
    // US688, 09/11/2014, shanem: If fieldData is empty, and it has a fill associated with it, fill it for the length of this field
    if (fieldData == null || fieldData == "") {
      _logger.debug("FlatFileEncode#fillFieldData(String, Record_Fields_Entry) - Filling empty field data")
      for (0..fieldDefinition.Length - 1) {
        dataString.append(fieldDefinition.Fill)
      }
    }
    switch (fieldDefinition.Justify) {
      case 'Left':
          _logger.debug("FlatFileEncode#fillFieldData(String, Record_Fields_Entry) - Filling data to the right")
          dataString.append(fieldData)
          for (fieldData.length..fieldDefinition.Length - 1) {
            dataString.append(fieldDefinition.Fill)
          }
          break
      case 'Right':
          _logger.debug("FlatFileEncode#fillFieldData(String, Record_Fields_Entry) - Filling data to the left")
          for (fieldData.length..fieldDefinition.Length - 1) {
            dataString.append(fieldDefinition.Fill)
          }
          dataString.append(fieldData)
          break
    }
    _logger.trace("FlatFileEncode#fillFieldData(String, Record_Fields_Entry) - Exiting")
    return dataString.toString()
  }

  /**
   * US688
   * 09/11/2014 shanem
   *
   * Truncate field data for strings that are longer than the length defined in their associated record definition
   */
  @Param("aFieldData", "The field data to be truncated")
  @Param("aFieldDefinition", "The field definition related to the field being encoded")
  @Returns("Truncated field data")
  internal function truncateFieldData(aFieldData: String, aFieldDefinition: Record_Fields_Entry): String {
    _logger.trace("FlatFileEncode#truncateFieldData(String, Record_Fields_Entry) - Entering")
    if (aFieldDefinition.Truncate == null || aFieldDefinition.Truncate == "" || aFieldDefinition.Length == null || aFieldDefinition.Length == 0){
      throw new IllegalArgumentException("Can't fill data when fieldDefinition has no defined fill or length")
    }
    if (aFieldData.length == aFieldDefinition.Length){
      return aFieldData
    }
    var dataString = new StringBuilder()
    switch (aFieldDefinition.Truncate) {
      case 'Left':
          _logger.debug("FlatFileEncode#truncateFieldData(String, Record_Fields_Entry) - Truncating Field Data to the left")
          dataString.append(aFieldData)
          for (0..(aFieldData.length - aFieldDefinition.Length - 1)) {
            dataString.deleteCharAt(0)
          }
          break
      case'Right':
          _logger.debug("FlatFileEncode#truncateFieldData(String, Record_Fields_Entry) - Truncating Field Data to the right")
          dataString.append(aFieldData)
          for (aFieldDefinition.Length..aFieldData.length - 1) {
            dataString.deleteCharAt(dataString.length() - 1)
          }
          break
        default:
        _logger.debug("FlatFileEncode#truncateFieldData(String, Record_Fields_Entry) - No Truncate value found")
    }
    _logger.trace("FlatFileEncode#truncateFieldData(String, Record_Fields_Entry) - Exiting")
    return dataString.toString()
  }

  /**
   * US688
   * 09/12/2014 shanem
   *
   * Check that the format of the field conforms to the specified field format in the definition
   */
  @Param("aFieldData", "The field data to be truncated")
  @Param("aFieldDefinition", "The field definition related to the field being encoded")
  @Returns("Formatted field data")
  @Throws(IllegalArgumentException, "If field data incorrectly formatted")
  internal function checkFieldDataCorrectlyFormatted(aFieldData: String, aFieldDefinition: Record_Fields_Entry) {
    _logger.trace("FlatFileEncoder#checkFieldDataFormat(String, Record_Fields_Entry) - Entering")
    if (aFieldDefinition.Format == null || aFieldDefinition.Format.length == 0) {
      throw new IllegalArgumentException("Cannot parse if no data to format to parse against.")
    } else if (aFieldData == null || aFieldData.length != aFieldDefinition.Length) {
      throw new IllegalArgumentException("Data provided not acceptable")
    } else if (aFieldDefinition.Format == "None") {
      //Do nothing
    } else {
      try {
        new SimpleDateFormat(aFieldDefinition.Format).parse(aFieldData)
      } catch (var e: ParseException) {
        _logger.error("FlatFileEncoder#checkFieldDataFormat(String, Record_Fields_Entry) - Parse Exception, The provided data does not match the format for output")
        throw new IllegalArgumentException()
      } catch (var e: IllegalArgumentException) {
        _logger.error("FlatFileEncoder#checkFieldDataFormat(String, Record_Fields_Entry) - Unexpected Exception thrown: Illegal pattern character")
        throw e
      }
    }
    _logger.debug("FlatFileEncoder#checkFieldDataFormat(String, Record_Fields_Entry) - Exiting")
  }
}