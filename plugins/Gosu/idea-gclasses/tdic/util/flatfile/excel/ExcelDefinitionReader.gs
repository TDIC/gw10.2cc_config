package tdic.util.flatfile.excel

uses gw.pl.logging.LoggerCategory
uses org.apache.poi.ss.usermodel.Cell
uses org.apache.poi.ss.usermodel.CellType
uses org.apache.poi.ss.usermodel.DateUtil
uses org.apache.poi.xssf.usermodel.XSSFCell
uses org.apache.poi.xssf.usermodel.XSSFRow
uses org.apache.poi.xssf.usermodel.XSSFSheet
uses org.apache.poi.xssf.usermodel.XSSFWorkbook
uses tdic.util.flatfile.model.classes.Field
uses tdic.util.flatfile.model.classes.FlatFile
uses tdic.util.flatfile.model.classes.Record
uses java.io.File
uses java.io.FileInputStream
uses java.io.FileNotFoundException
uses java.lang.IllegalArgumentException
uses java.util.ArrayList
uses java.util.Arrays
uses java.util.HashSet
uses java.util.Properties
uses org.apache.poi.openxml4j.opc.OPCPackage
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory

class ExcelDefinitionReader {
  final var START_POS: String
  final var LENGTH: String
  final var FIELD_NAME: String
  final var PARAMETER_1: String
  final var FORMAT: String
  final var JUSTIFY: String
  final var FILL: String
  final var TRUNCATE: String
  final var STRIP_TYPE: String
  final var STRIP: String
  final var DATA: String
  final var PARAMETER_2: String
  final var RECORD_TYPE: String
  final var RECORD_SECTION: String
  final var RECORD_DELIMITER: String
  final var RECORD_FORMAT: String
  final var EXECUTE: String
  final var TRANSACTION_SOURCE: String
  final var EXTENSIONS = {"xlsx", "xls"}
  final var FIELD_HEADERS: ArrayList<String>
  final var RECORD_HEADERS: ArrayList<String>
  private var _logger = LoggerFactory.getLogger("EXCEL_VENDOR_DEFINITION_READER")
  /**
   * US688
   * 09/18/2014 shanem
   *
   * Loads excel column headers from a properties file
   */
  @Param("aFileHeadersDefPropFile", "Location of the properties file defining the header names")
  construct(aFileHeadersDefPropFile: String) {
    var headers = loadHeadersProperties(aFileHeadersDefPropFile)
    START_POS = headers.getProperty("StartPosition")
    LENGTH = headers.getProperty("Length")
    FIELD_NAME = headers.getProperty("Name")
    PARAMETER_1 = headers.getProperty("Parameter1")
    FORMAT = headers.getProperty("Format")
    JUSTIFY = headers.getProperty("Justify")
    FILL = headers.getProperty("Fill")
    TRUNCATE = headers.getProperty("Truncate")
    STRIP_TYPE = headers.getProperty("StripType")
    STRIP = headers.getProperty("Strip")
    DATA = headers.getProperty("Data")
    PARAMETER_2 = headers.getProperty("Parameter2")
    RECORD_TYPE = headers.getProperty("RecordType")
    RECORD_SECTION = headers.getProperty("RecordSection")
    RECORD_DELIMITER = headers.getProperty("RecordDelimiter")
    RECORD_FORMAT = headers.getProperty("RecordFormat")
    EXECUTE = headers.getProperty("Execute")
    TRANSACTION_SOURCE = headers.getProperty("Source")
    FIELD_HEADERS = {RECORD_TYPE, START_POS, LENGTH, FIELD_NAME, PARAMETER_1, FORMAT, JUSTIFY, FILL, TRUNCATE, STRIP_TYPE, STRIP, DATA, PARAMETER_2}
    RECORD_HEADERS = {RECORD_TYPE, RECORD_SECTION, RECORD_DELIMITER, RECORD_FORMAT, EXECUTE, TRANSACTION_SOURCE}
  }

  /**
   * US688
   * 09/16/2014 shanem
   *
   * Reads an XLSX file and parses to a FlatFile object for use as a vendor definition
   */
  @Param("aWorkbookPath", "The path to the .xlsx file to be parsed")
  @Returns("The .xlsx file as a FlatFile")
  /**
   * US672/673 -Changed the FlatFile to ArrayList ---Praneethk
   */ 
  function read(aWorkbookPath: String): ArrayList<Record> {
   // _logger.trace("ExcelDefinitionReader#read(String) - Entering.")
    var file = new File(aWorkbookPath);
    if (!file.exists() || file.isDirectory()) {
      throw new FileNotFoundException()
    }
    if (!EXTENSIONS.contains(file.Extension)){
      throw new IllegalArgumentException()
    }
    var pkg = OPCPackage.open(aWorkbookPath)
    var workBook = new XSSFWorkbook(pkg)
    var valueArray = new ArrayList<Record>()
    try {
      var sheet = workBook.getSheetAt(0)
      var row: XSSFRow
      var cell: XSSFCell
      var rows = sheet.getPhysicalNumberOfRows();
      var headerRow = getHeaders(sheet)
      var cols = headerRow.length
      for (1..rows index r) {
        var record = new Record()
        row = sheet.getRow(r + 1);
        if (row != null) {
          for (0..cols index c) {
            cell = row.getCell(c);
            if (cell != null){
              switch (headerRow[c]) {
                case RECORD_TYPE:
                    record.RecordType = getCellContent(cell)
                    break
                case RECORD_SECTION:
                    record.RecordSection = getCellContent(cell)
                    break
                case RECORD_DELIMITER:
                    record.RecordDelimiter = getCellContent(cell)
                    break
                case RECORD_FORMAT:
                    record.RecordFormat = getCellContent(cell)
                    break
                case EXECUTE:
                    record.Execute = getCellContent(cell)
                    break
                case TRANSACTION_SOURCE:
                    record.Source = getCellContent(cell)
                    break
                  default:
              }
            }
          }
          // US688, 09/16/2014, shanem: create record from each row, add to array
          var recordFields = getRecordFields(record.RecordType, workBook)
          record.Identifier = recordFields.firstWhere(\elt -> elt.StartPosition == 1 && elt.Length == 1).Data
          record.Fields = recordFields
          valueArray.add(record)
        }
      }
    } catch (var e: CsvFormatException) {
      throw e
    }
    //_logger.trace("ExcelDefinitionReader#read(String) - Exiting.")
//    return new FlatFile(valueArray as Record[])
    pkg.close()
    return valueArray
  }

  /**
   * US688
   * 09/16/2014 shanem
   *
   * For each entry in the Fields worksheet of referencing the recordType provided, create a field object from the column values
   */
  @Param("aRecordType", "The record type to find the associated fields for")
  @Param("aWorkBook", "A Workbook object containing the field definitions")
  @Returns("An Array of Field objects associated with the Record Type")
  internal function getRecordFields(aRecordType: String, aWorkBook: XSSFWorkbook): Field[] {
    //_logger.trace("ExcelDefinitionReader#getRecordFields(String, XSSFWorkbook) - Entering.")
    if (aRecordType == null || aWorkBook == null){
      throw new IllegalArgumentException()
    }
    var sheet = aWorkBook.getSheetAt(1)
    var headerRow: String[]
    headerRow = getHeaders(sheet)
    var row: XSSFRow
    var cell: XSSFCell
    var rows = sheet.getPhysicalNumberOfRows();
    var cols = headerRow.length
    var recordFields = new ArrayList<Field>()
    for (1..rows index r) {
      var field = new Field()
      row = sheet.getRow(r + 1);
      if (row != null && row.Count>0 && row.getCell(0).toString() == aRecordType) {
        for (0..cols index c) {
          cell = row.getCell(c);
          if (cell != null){
            switch (headerRow[c]) {
              case RECORD_TYPE:
                  //Ignore
                  break
              case START_POS:
                  field.StartPosition = Coercions.makePIntFrom(getCellContent(cell))
                  break
              case LENGTH:
                  field.Length = Coercions.makePIntFrom(getCellContent(cell))
                  break
              case FIELD_NAME:
                  field.Name = getCellContent(cell)
                  break
              case PARAMETER_1:
                  field.Parameter1 = getCellContent(cell) == null ? '' : getCellContent(cell)
                  break
              case FORMAT:
                  field.Format = getCellContent(cell)
                  break
              case JUSTIFY:
                  field.Justify = getCellContent(cell)
                  break
              case FILL:
                  field.Fill = getCellContent(cell)
                  break
              case TRUNCATE:
                  field.Truncate = getCellContent(cell)
                  break
              case STRIP_TYPE:
                  field.StripType = getCellContent(cell)
                  break
              case STRIP:
                  field.Strip = getCellContent(cell) == null ? '' : getCellContent(cell)
                  break
              case DATA:
                  field.Data = getCellContent(cell) == null ? '' : getCellContent(cell)
                  break
              case PARAMETER_2:
                  field.Parameter2 = getCellContent(cell) == null ? '' : getCellContent(cell)
                  break
                default:
            }
          }
        }
        recordFields.add(field)
      }
    }
    //_logger.trace("ExcelDefinitionReader#getRecordFields(String, XSSFWorkbook) - Exiting.")
    return recordFields.Empty ? null : recordFields?.toTypedArray()
  }

  /**
   * US688
   * 09/16/2014 shanem
   *
   * Reads the first row in the sheet as a header row
   */
  @Param("aSheet", "The WorkSheet to return a header row for")
  @Param("aColumnCount", "The number of columns across the row spans")
  @Returns("The headers for each cell as an array of Strings")
  internal function getHeaders(aSheet: XSSFSheet): String[] {
    //_logger.trace("ExcelDefinitionReader#getHeaders(XSSFSheet, int) - Entering")
    var headerRow = aSheet.getRow(0)
    if (headerRow == null){
      throw new CsvFormatException()
    }
    var cols = headerRow.getPhysicalNumberOfCells()
    var cell: XSSFCell
    var headerArray = new ArrayList<String>()
    for (0..cols index c) {
      cell = headerRow.getCell(c);
      if (cell != null) {
        headerArray.add(cell.StringCellValue)
      }
    }
    // US688, 09/17/2014, shanem: the headers must match the expected headers, otherwise we cannot continue
    var set1 = new HashSet<String>(Arrays.asList(headerArray?.toTypedArray()))
    var set2 = new HashSet<String>(Arrays.asList(RECORD_HEADERS?.toTypedArray()))
    var set3 = new HashSet<String>(Arrays.asList(FIELD_HEADERS?.toTypedArray()))
    if ((set1 != set2 && set1 != set3) || headerArray.Empty){
      _logger.error("ExcelDefinitionReader#getHeaders(XSSFSheet, int) - Column Headers are incorrect")
      throw new IllegalArgumentException()
    }
    //_logger.trace("ExcelDefinitionReader#getHeaders(XSSFSheet, int) - Exiting")
    return headerArray?.toTypedArray()
  }

  internal final function loadHeadersProperties(fileHeadersDefPath: String): Properties {
    // _logger.trace("ExcelDefinitionReader#loadHeadersProperties(String) - Entering")

    if (fileHeadersDefPath == null){
      throw new IllegalArgumentException()
    }
    var fileHeadersFile = new File(fileHeadersDefPath)
    if (!(fileHeadersFile.exists()) || fileHeadersFile.isDirectory()) {
      //_logger.error("ExcelDefinitionReader#loadHeadersProperties(String) - No File located at: " + fileHeadersFile.Path)
      throw new FileNotFoundException("File headers property file not found at " + fileHeadersFile.Path);
    }
    var properties = new Properties()
    properties.load(new FileInputStream(fileHeadersFile))
    //_logger.trace("ExcelDefinitionReader#loadHeadersProperties(String) - Exiting")
    return properties
  }

  /**
   * US688
   * 09/16/2014 shanem
   *
   * Gets the contents of a cell and returns it as a string
   */
  @Param("aCell", "The Cell object to return the content of")
  @Returns("The cell contents as a String")
  internal function getCellContent(cell: XSSFCell): String {
   // _logger.trace("ExcelDefinitionReader#getCellContent(XSSFSheet) - Entering")
    var returnString: String
    switch (cell.getCellType()) {
      case CellType.STRING:
          returnString = cell.getRichStringCellValue().getString()
          break;
      case CellType.NUMERIC:
          if (DateUtil.isCellDateFormatted(cell)) {
            returnString = cell.getDateCellValue().toString()
          } else {
            returnString = cell.getNumericCellValue() as String
          }
          break;
      case CellType.BOOLEAN:
          returnString = cell.getBooleanCellValue() as String
          break;
      case CellType.FORMULA:
          returnString = cell.getCellFormula()
          break;
        default:
    }
   // _logger.trace("ExcelDefinitionReader#getCellContent(XSSFSheet) - Exiting")
    return returnString
  }
}