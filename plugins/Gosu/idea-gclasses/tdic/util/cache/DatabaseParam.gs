package tdic.util.cache
/**
 * User: skiriaki
 * Date: 8/27/14
 */
class DatabaseParam {
  var _host : String as Host
  var _instance : String as Instance
  var _port : String as Port
  var _dbName : String as DbName
  var _userName : String as UserName
  var _password : String as Password

}