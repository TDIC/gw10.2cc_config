package tdic.util.cache

uses gw.processes.BatchProcessBase
uses gw.pl.logging.LoggerCategory
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: skiriaki
 * Date: 10/15/14
 * Time: 3:04 PM
 * To change this template use File | Settings | File Templates.
 */
class CacheManagerBatchProcess extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("TDIC_INTEGRATION")

  construct() {
    super(BatchProcessType.TC_CACHEMANAGER)
  }

  override function doWork() {
    _logger.debug("CacheManagerBatchProcess#doWork() - Entering")
    CacheManager.loadCache()
    _logger.debug("CacheManagerBatchProcess#doWork() - Exiting")
  }
}