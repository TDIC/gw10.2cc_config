package tdic.util.cache
/**
 * User: skiriaki
 * Date: 8/27/14
 */
class ExternalCodeParam {
  var _sourceSystem : String as SourceSystem
  var _displayMessage : String as DisplayMessage
  var _activityPattern : String as ActivityPattern
  var _isWarning : boolean as IsWarning
  var _isError : boolean as IsError
  var _isRetryable : boolean as IsRetryable
}