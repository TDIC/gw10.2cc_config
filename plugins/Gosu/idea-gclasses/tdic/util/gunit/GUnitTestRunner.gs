package tdic.util.gunit

uses tdic.util.gunit.TextTestRunner
uses gw.lang.reflect.IType

abstract class GUnitTestRunner {

  /**
   * Prevent others from constructing instances.
   */
  private construct() {
  }

  static function runAllTests() {
    var runner = new TextTestRunner()
    runner.initalize()
    runner.runAllTests()
    runner.finish()
  }

  static function runAllTestsExceptSlowTests() {
    var runner = new TextTestRunner()
    runner.TestActions = new TestActionSkipSlowTests(runner.TestActions)
    runner.initalize()
    runner.runAllTests()
    runner.finish()
  }

  static function runTestsInPackage(packageName : String) {
    print("Running tests in package " + packageName)
    var runner = new TextTestRunner()
    runner.initalize()
    runner.runTestsInPackage(packageName)
    runner.finish()
  }

  static function runTestsInPackageExceptSlowTests(packageName : String) {
    print("Running tests in package " + packageName)
    var runner = new TextTestRunner()
    runner.TestActions = new TestActionSkipSlowTests(runner.TestActions)
    runner.initalize()
    runner.runTestsInPackage(packageName)
    runner.finish()
  }

  static function runTestsInClass(clazz : IType) {
    print("Running tests in class " + clazz.Name)
    var runner = new TextTestRunner()
    runner.initalize()
    runner.runTestsInClass(clazz)
    runner.finish()
  }

  static function runTestMethod(clazz : IType, methodName : String) {
    print("Running test method " + clazz.Name + "." + methodName)
    var runner = new TextTestRunner()
    var method = clazz.TypeInfo.getCallableMethod(methodName, {})
    if (method == null) {
      print("Method '" + methodName + "' NOT found in test class " + clazz.DisplayName)
      runner.TestReporter.printFailLine()
    } else {
      runner.initalize()
      runner.runTestMethod(method)
      runner.finish()
    }
  }

}
