package tdic.plugin.encryption

uses gw.plugin.InitializablePlugin
uses gw.plugin.util.IEncryption
uses gw.util.Base64Util
uses gw.pl.logging.LoggerCategory
uses gw.util.GosuStringUtil
uses javax.crypto.Cipher
uses javax.crypto.spec.IvParameterSpec
uses javax.crypto.spec.SecretKeySpec
uses java.lang.Exception
uses java.util.Map
uses org.slf4j.LoggerFactory

/**
 * US552 Encryption
 * 10/24/2014 Vicente
 *
 * Implementation of the use AES (Advanced Encryption Standard) algorithm with a 128 byte key.
 */
class EncryptionByAESPlugin implements IEncryption, InitializablePlugin {
  private static var _logger = LoggerFactory.getLogger("ENCRYPTION_PLUGIN")

  //Initialization Vector and Encryption Key usd by the AES algorithm
  private static var encryptionKey : String
  private static var initializationVecor : String

  construct() {}

  property set Parameters( params: Map ) : void  {
    encryptionKey = params.get("encryptionKey") as String
    initializationVecor = params.get("initializationVecor") as String
  }

  /**
   * Returns the value encrypted using the AES (Advanced Encryption Standard) algorithm with a 128 byte key.
   */
  @Param("value", "value to encrypt")
  @Returns("The value encrypted using the AES (Advanced Encryption Standard) algorithm with a 128 byte key")
  override function encrypt(value:String) : String {
    try {
      _logger.debug("EncryptionByAESPlugin#encrypt() - Encrypting value")
      var cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
      var key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES")
      cipher.init(Cipher.ENCRYPT_MODE, key ,new IvParameterSpec(initializationVecor.getBytes("UTF-8")))
      return Base64Util.encode(cipher.doFinal(value.getBytes()))
    } catch( e : Exception ) {
      _logger.error("EncryptionByAESPlugin#encrypt() - Error while encrypting " + value)
      throw new Exception( "Error while encrypting :", e )
    }
  }

  /**
   * Returns the value decrypted using the AES (Advanced Encryption Standard) algorithm with a 128 byte key.
   */
  @Param("value", "value to decrypt")
  @Returns("The value decrypted using the AES (Advanced Encryption Standard) algorithm with a 128 byte key")
  override function decrypt(value:String) : String {
    try {
      _logger.debug("EncryptionByAESPlugin#decrypt() - Decrypting value")
      var cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE")
      var key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES")
      cipher.init(Cipher.DECRYPT_MODE, key,new IvParameterSpec(initializationVecor.getBytes("UTF-8")))
      return new String(cipher.doFinal(Base64Util.decode(value)), "UTF8").trim()
    } catch( e : Exception ) {
      _logger.error("EncryptionByAESPlugin#encrypt() - Error while decrypting " + value)
      throw new Exception( "Error while encrypting :", e )
    }
  }

  /**
   * Returns the output AES encrypted size assuming PKCS#5 padding if the input data has the input length and it is
   * applied the Base64Util encrypted.
   */
  @Param("value", "the input value size")
  @Returns("The output AES encrypted size assuming PKCS#5 padding if the input data has the input length")
  override function getEncryptedLength(size:int) : int {
    _logger.debug("EncryptionByAESPlugin#getEncryptedLength() - Getting Encrypted Length")
    return encrypt(GosuStringUtil.leftPad("*",size)).size
  }

  /**
   * Returns the AES Encryption Identifier
   */
  @Returns("The AES Encryption Identifier")
  override property get EncryptionIdentifier() : String {
    return this.IntrinsicType.Name
  }
}