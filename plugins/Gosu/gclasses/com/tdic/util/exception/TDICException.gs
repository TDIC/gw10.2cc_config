package com.tdic.util.exception

class TDICException extends RuntimeException {

  public construct() {
    super()
  }

  public construct(message: String) {
    super(message)
  }

  public construct(message: String, exception : Exception) {
    super(message, exception)
  }

}