package com.tdic.util.database

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.system.server.ServerUtil
uses gw.pl.logging.LoggerCategory
uses java.sql.Connection
uses java.sql.SQLException

/**
 * Created with IntelliJ IDEA.
 * User: VipulK
 * Date: 6/18/19
 * Time: 2:14 AM
 * Util class to get connection for integration database with  methods to insert , update and delete values in database
 */

class DatabaseUtil {
  private static var Email_Sub = "Failure: Caching Integration properties failed for ${ServerUtil.getProduct().ProductName} on ${ServerUtil.getEnv()}-${ServerUtil.getServerId()}"
  private static var _logger = LoggerCategory.TDIC_INTEGRATION

  static public function dbInsertProperty(_sqlStmt: String): boolean {
    return executeQueryInDb( _sqlStmt)
  }

  static public function dbUpdateProperty( _sqlStmt: String): boolean {
    return executeQueryInDb( _sqlStmt)
  }

  static public function dbDeleteProperty(_sqlStmt: String): boolean {
    return executeQueryInDb(_sqlStmt)
  }


  static function executeQueryInDb(sqlStmt: String): boolean {
    _logger.trace("DatabaseUtil#executeQueryInDb() - Entering")
    var _con = getIntegDBConnection()
    var _return = DatabaseManager.executeStmt(_con, sqlStmt)
    _con.close()
    _logger.trace("DatabaseUtil#executeQueryInDb() - Exiting")
    if (_return >= 1) {
      return true
    } else {
      return false
    }
  }

  @Returns ( "Connection" )
  static public function getIntegDBConnection() : Connection{
    _logger.trace("DatabaseUtil#getIntegDBConnection() - Entering")
    var _intDBURL: String
    var _con: Connection
    try {
      //  retrieve the GWINT DB URL from tdicintegrations.properties file
        _intDBURL =  PropertyUtil.getInstance().getProperty("IntDBURL")
      // Get a connection from DatabaseManager using the GWINT DB URL
      _con = DatabaseManager.getConnection(_intDBURL)
    } catch (sql: SQLException) {
      _logger.error("DatabaseUtil#getIntegDBConnection() - SQLException= " + sql)
      sendEmail(sql.StackTraceAsString)
      throw(sql)
    } catch (e: Exception) {
      _logger.error("DatabaseUtil#getIntegDBConnection() - Exception= " + e)
      sendEmail(e.StackTraceAsString)
      throw(e)
    }
    _logger.trace("DatabaseUtil#getIntegDBConnection() - Exiting")
    return _con
  }


  private static function sendEmail(info: String){
    var _DefaultToEmailIDs = PropertyUtil.getInstance().getProperty("DefaultToEmailIDs")
    EmailUtil.sendEmail(_DefaultToEmailIDs, Email_Sub, "Cache Manager is unable to load integration properties. Please fix the error and restart the server. \n ${info}")
  }

}