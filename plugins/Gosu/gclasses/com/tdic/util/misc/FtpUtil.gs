package com.tdic.util.misc

uses com.tdic.util.properties.PropertyUtil
uses gw.pl.logging.LoggerCategory
uses java.io.File
uses org.apache.commons.vfs2.FileSystemOptions
uses org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder
uses org.apache.commons.vfs2.impl.StandardFileSystemManager
uses org.apache.commons.vfs2.Selectors
uses java.lang.Exception
uses java.util.HashSet
uses java.util.Set
uses org.slf4j.LoggerFactory

/**
 * Utility class to upload and download files via FTP.
 * Each vendor/connection will have 5 entries in the cache Properties HashMap to hold information for its FTP connection.
 * Example:
 *   Sedgwick.Usr  =
 *   Sedgwick.Pwd  =
 *   Sedgwick.Host =
 *   Sedgwick.Port =
 *   Sedgwick.Dir  =
 */
class FtpUtil {
  private static var _logger = LoggerFactory.getLogger("TDIC_INTEGRATION")

  /**
   * Uploads a file through an SFTP connection.
   */
  @Param("vendorName", "A String containing the name of the vendor, corresponding to definitions in Cache Properties")
  @Param("file", "A java.io.File object representing the file to upload")
  @Returns("A boolean indicating if the upload was successful")
  static public function uploadFile(vendorName: String, file: File): boolean {
    _logger.debug("Ftp#uploadFile() - Entering")

    if (PropertyUtil.getInstance().getProperty(vendorName + ".Usr") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".Pwd") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".Host") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".Port") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".OutDir") == null) {
      _logger.error("Ftp#uploadFile() - Integration Database missing vendor FTP values for: " + vendorName)
      return false
    }

    var manager : StandardFileSystemManager

    try {
      var connectionString =  "sftp://" + PropertyUtil.getInstance().getProperty(vendorName + ".Usr") +
                            ":" + PropertyUtil.getInstance().getProperty(vendorName + ".Pwd") +
                            "@" + PropertyUtil.getInstance().getProperty(vendorName + ".Host")  +
                            ":" + PropertyUtil.getInstance().getProperty(vendorName + ".Port")  +
                            "/" + PropertyUtil.getInstance().getProperty(vendorName + ".OutDir")

      var opts = new FileSystemOptions()
      SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no")
      SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, false)
      SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000)

      manager = new StandardFileSystemManager()
      manager.init()
      var localFile = manager.resolveFile(file.getAbsolutePath())
      _logger.debug("Ftp#uploadFile() - Resolved source file: " + localFile)
      var remoteFile = manager.resolveFile(connectionString + "/" + localFile.Name.BaseName, opts)
      _logger.debug("Ftp#uploadFile() - Resolved target file: " + remoteFile.Name.Path)
      remoteFile.copyFrom(localFile, Selectors.SELECT_SELF)
      _logger.debug("Ftp#uploadFile() - Successfully uploaded file")
      return true
    } catch (e:Exception) {
      _logger.error("Ftp#uploadFile() - Exception occurred while uploading file: " + e.LocalizedMessage, e)
      return false
    } finally {
      if (manager != null) {
        manager.close()
      }
      _logger.debug("Ftp#uploadFile() - Exiting")
    }
  }

  /**
   * Downloads a file through an SFTP connection.
   */
  @Param("vendorName", "A String containing the name of the vendor, corresponding to definitions in Cache Properties")
  @Param("desiredLocalFile", "A java.io.File object representing the desired local file")
  @Returns("A boolean indicating if the download was successful")
  static public function downloadFile(vendorName: String, desiredLocalFile: File): boolean {
    _logger.debug("Ftp#downloadFile() - Entering")

    if (PropertyUtil.getInstance().getProperty(vendorName + ".Usr") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".Pwd") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".Host") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".Port") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".InDir") == null) {
      _logger.error("Ftp#downloadFile() - Integration Database missing vendor FTP values for: " + vendorName)
      return false
    }

    var manager : StandardFileSystemManager

    try {
      var connectionString =  "sftp://" + PropertyUtil.getInstance().getProperty(vendorName + ".Usr") +
          ":" + PropertyUtil.getInstance().getProperty(vendorName + ".Pwd") +
          "@" + PropertyUtil.getInstance().getProperty(vendorName + ".Host")  +
          ":" +PropertyUtil.getInstance().getProperty(vendorName + ".Port")  +
          "/" + PropertyUtil.getInstance().getProperty(vendorName + ".InDir")

      var opts = new FileSystemOptions()
      SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no")
      SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, false)
      SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000)

      manager = new StandardFileSystemManager()
      manager.init()

      var remoteFile = manager.resolveFile(connectionString + "/" + desiredLocalFile.Name, opts)
      _logger.debug("Ftp#downloadFile() - Resolved source file: " + remoteFile.Name.Path)
      if (!remoteFile.exists()) {
        _logger.warn("Ftp#downloadFile() - Source file does not exist.  Cannot download." )
        return false
      }

      var localFile = manager.resolveFile(desiredLocalFile.AbsolutePath)
      _logger.debug("Ftp#downloadFile() - Resolved target file: " + localFile)
      localFile.copyFrom(remoteFile, Selectors.SELECT_SELF);
      _logger.debug("Ftp#downloadFile() - Successfully downloaded file")

      _logger.debug("Ftp#downloadFile() - Exiting")
      return true

    } catch (e:Exception) {
      _logger.error("Ftp#downloadFile() - Exception occurred while downloading file: " + e.LocalizedMessage, e)
      return false
    } finally {
      if (manager != null) {
        manager.close()
      }
    }
  }

  /**
   * Gets a file list of files in a remote directory through an SFTP connection.
   */
  @Param("vendorName", "A String containing the name of the vendor, corresponding to definitions in Cache Properties")
  @Returns("A Set<String> of the file names inside the remote directory; otherwise null if there is an issue retrieving list")
  static public function getFileList(vendorName: String): Set<String> {
    _logger.debug("Ftp#getFileList() - Entering")

    if (PropertyUtil.getInstance().getProperty(vendorName + ".Usr") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".Pwd") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".Host") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".Port") == null
        || PropertyUtil.getInstance().getProperty(vendorName + ".InDir") == null) {
      _logger.error("Ftp#getFileList() - Integration Database missing vendor FTP values for: " + vendorName)
      return null
    }

    var manager : StandardFileSystemManager

    try {
      var connectionString =  "sftp://" + PropertyUtil.getInstance().getProperty(vendorName+".Usr") +
          ":" + PropertyUtil.getInstance().getProperty(vendorName + ".Pwd") +
          "@" + PropertyUtil.getInstance().getProperty(vendorName + ".Host")  +
          ":" + PropertyUtil.getInstance().getProperty(vendorName + ".Port")  +
          "/" + PropertyUtil.getInstance().getProperty(vendorName + ".InDir")

      var opts = new FileSystemOptions()
      SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no")
      SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, false)
      SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000)

      manager = new StandardFileSystemManager()
      manager.init()

      var remoteFile = manager.resolveFile(connectionString)
      _logger.debug("Ftp#getFileList() - Resolved source folder path: " + remoteFile.Name.Path)
      var childrenFiles = new HashSet<String>()
      if (remoteFile.Children != null) {
        _logger.debug("Ftp#getFileList() - Number of files in source folder: " + remoteFile.Children.Count)
        for (file in remoteFile.Children) {
          _logger.debug("Ftp#getFileList() - File name: " + file.Name.BaseName)
          childrenFiles.add(file.Name.BaseName)
        }
      }
      _logger.debug("Ftp#getFileList() - Exiting")
      return childrenFiles
    } catch (e:Exception) {
      _logger.error("Ftp#getFileList() - Exception occurred while getting file list: " + e.LocalizedMessage, e)
      return null
    } finally {
      if (manager != null) {
        manager.close()
      }
    }
  }

}
