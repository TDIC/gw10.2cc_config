package com.tdic.plugins.auth

uses gw.plugin.security.AuthenticationServicePlugin
uses gw.plugin.security.AuthenticationSource
uses gw.plugin.security.AuthenticationServicePluginCallbackHandler
uses gw.plugin.security.UserNamePasswordAuthenticationSource
uses gw.plugin.security.UserValueNVP
uses gw.plugin.security.CredentialVerificationResult
uses gw.plugin.security.LockedCredentialException
uses gw.plugin.security.MustWaitToRetryException
uses javax.security.auth.login.LoginException
uses javax.security.auth.login.FailedLoginException
uses java.lang.IllegalArgumentException
uses java.lang.Exception
uses org.slf4j.LoggerFactory

/**
 * US684, US1132
 * 05/18/2015 Rob Kelly
 *
 * An AuthenticationServicePlugin implementation that authenticates both SPNEGO SSO and basic login form credentials.
 * If SPNEGO SSO authentication is successful, the user data for the authenticated user is synched using an Active Directory user data provider.
 *
 * @see TDIC_UserDataProvider
 * @see com.tdic.plugins.auth.TDIC_ActiveDirectoryProvider
 */
class TDIC_AuthenticationServicePlugin implements AuthenticationServicePlugin {

  /**
   * The logger for this Plugin.
   */
  private static final var LOGGER = LoggerFactory.getLogger("AUTHENTICATION")

  /**
   * The logger tag for this Plugin.
   */
  private static final var LOG_TAG = "TDIC_AuthenticationServicePlugin#"

  /**
   * The CallbackHandler for this AuthenticationServicePlugin.
   * A CallbackHandler is used to query and update Users in the application database.
   */
  private var handler : AuthenticationServicePluginCallbackHandler

  /**
   * The User data provider for this AuthenticationServicePlugin.
   * A User data provider is used to retrieve User data following successful SSO authentication.
   */
  private var userDataProvider : TDIC_UserDataProvider

  construct() {

    this.userDataProvider = new TDIC_ActiveDirectoryProvider()
  }

  /*
  * Authenticates the user using the authentication credentials contained in the specified AuthenticationSource.
  * Two types of AuthenticationSource are supported: TDIC_UserNameAuthenticationSource and UserNamePasswordAuthenticationSource.
  */
  @Param("anAuthenticationSource", "an AuthenticationSource containing the authentication credentials")
  @Returns("the PublicID of the authenticated user")
  @Throws(IllegalArgumentException, "if the AuthenticationSource is not supported")
  @Throws(LoginException, "if authentication fails")
  override function authenticate(anAuthenticationSource : AuthenticationSource) : String {

    var logTag =  LOG_TAG + "authenticate(AuthenticationSource) - "
    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Entering")

    var userPublicID : String = null
    if (anAuthenticationSource typeis TDIC_UserNameAuthenticationSource) {

      userPublicID = doUserNameAuthentication(anAuthenticationSource)
    }
    else if (anAuthenticationSource typeis UserNamePasswordAuthenticationSource) {

      userPublicID = doDefaultAuthentication(anAuthenticationSource)
    }
    else {

      LOGGER.error(logTag + "AuthenticationSource not supported")
      throw new IllegalArgumentException("AuthenticationSource not supported")
    }

    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Exiting")
    return userPublicID
  }

  @Param("callbackHandler", "the callback handler for this AuthenticationServicePlugin")
  override property set Callback(callbackHandler : AuthenticationServicePluginCallbackHandler) {
    this.handler = callbackHandler
  }

  /*
     * Sets the CallbackHandler for this AuthenticationServicePlugin.
     */
 /* @Param("aCallbackHandler", "the callback handler for this AuthenticationServicePlugin")
  override function setCallback(aCallbackHandler : AuthenticationServicePluginCallbackHandler) {

    this.handler = aCallbackHandler
  } */

  /**
   * Performs username authentication for the username contained in the specified TDIC_UserNameAuthenticationSource.
   */
  private function doUserNameAuthentication(aUserNameAuthenticationSource : TDIC_UserNameAuthenticationSource) : String {

    var logTag =  LOG_TAG + "doUserNameAuthentication(TDIC_UserNameAuthenticationSource) - "
    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Entering")

    var userName = aUserNameAuthenticationSource.UserName
    var userPublicID = this.handler.findUser(userName)
    if (userPublicID != null) {

      if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "User with username " + userName + " found. Synchronizing external user data...")
      synchronizeUserData(userPublicID, userName)
    }
    else {

      if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "User with username " + userName + " not found")
      var configLogger = LoggerFactory.getLogger("Configuration")
      if (configLogger.InfoEnabled) configLogger.info("SSO Login attempt failed for user \'" + userName + "\'")
      throw new FailedLoginException("User with username " + userName + " not found")
    }

    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Exiting")
    return userPublicID
  }

  /**
   * Synchronizes the User with the specified PublicID using the user data identified by the specified username.
   */
  private function synchronizeUserData(userPublicID : String, userName : String) {

    var logTag =  LOG_TAG + "synchronizeUserData(String,String) - "
    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Entering")

    try {

      var userData = this.userDataProvider.getUserData(userName)
      var nvps = new java.util.ArrayList<UserValueNVP>()
      var nvp : UserValueNVP
      for (aKey in userData.Keys) {

        nvp = new UserValueNVP()
        nvp.Path = aKey
        nvp.Value = userData.get(aKey)
        nvps.add(nvp)
      }

      if (nvps.size() > 0) {

        if (LOGGER.DebugEnabled) {

          LOGGER.debug(logTag + "Synchronizing the following User data fields:")
          for (anNVP in nvps) {

            LOGGER.debug(logTag + "  " + anNVP.Path + " : " + anNVP.Value)
          }
        }
        this.handler.modifyUser(userPublicID, nvps.toTypedArray())
      }

      if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Exiting")
    }
    catch (e : Exception) {

      LOGGER.error(logTag + "ERROR synchronizing user data for User with PublicID " + userPublicID + ": " + e.getMessage())
    }
  }

  /**
   * Performs username authentication for the username and password contained in the specified UserNamePasswordAuthenticationSource.
   */
  private function doDefaultAuthentication(aUserNamePasswordAuthenticationSource : UserNamePasswordAuthenticationSource) : String {

    var logTag =  LOG_TAG + "doDefaultAuthentication(UserNamePasswordAuthenticationSource) - "
    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Entering")

    var userName = aUserNamePasswordAuthenticationSource.Username
    var userPublicID = this.handler.findUser(userName)
    if (userPublicID != null) {

      if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "User with username " + userName + " found. Verifying credentials...")
      var returnCode = this.handler.verifyInternalCredential(userPublicID, aUserNamePasswordAuthenticationSource.Password)
      if (returnCode == CredentialVerificationResult.BAD_USER_ID) {

        throw new FailedLoginException("Bad user name " + userName);
      }
      else if (returnCode == CredentialVerificationResult.WAIT_TO_RETRY) {

        throw new MustWaitToRetryException("Still within the login retry delay period");
      }
      else if (returnCode == CredentialVerificationResult.CREDENTIAL_LOCKED) {

        throw new LockedCredentialException("The specified account has been locked");
      }
      else if (returnCode == CredentialVerificationResult.PASSWORD_MISMATCH) {

        throw new FailedLoginException("Bad password for user " + userName);
      }
    }
    else {

      if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "User with username " + userName + " not found")
      throw new FailedLoginException("User with username " + userName + " not found")
    }

    if (LOGGER.DebugEnabled) LOGGER.debug(logTag + "Exiting")
    return userPublicID
  }
}