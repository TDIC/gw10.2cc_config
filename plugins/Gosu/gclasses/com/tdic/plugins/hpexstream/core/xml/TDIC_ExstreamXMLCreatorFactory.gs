/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package com.tdic.plugins.hpexstream.core.xml

uses gw.api.system.PLLoggerCategory
uses gw.document.SimpleSymbol
uses gw.lang.reflect.ReflectUtil
uses java.lang.Class
uses java.lang.ClassNotFoundException
uses java.lang.RuntimeException
uses java.util.Map
uses org.slf4j.LoggerFactory

class TDIC_ExstreamXMLCreatorFactory {
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  construct() {
  }

  /**
   * US656
   * 08/15/2014 shanem
   *
   * Returns product specific xml creator object
   *
   * Based on the contents of the param map, we determine which creator to return.
   * The Creator is constructed using reflection to keep this class generic across all XCenters.
   * Policy can include the account in the param map, we check if the BC Creator class exists, and if it does
   * we use the BC Creator, otherwise we return the PC Creator
   */
  @Param("aParamMap", "Parameter map containing GW entity to generate transaction data XML for.")
  @Returns("Product specific XMLCreator object")
  static function getExstreamXMLCreator(aParamMap: Map<Object, Object>): TDIC_ExstreamXMLCreator {
    _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - Entering")

    final var PC_CREATOR = "tdic.pc.integ.plugins.hpexstream.xml.TDIC_PCExstreamXMLCreator"
    final var BC_CREATOR = "tdic.bc.integ.plugins.hpexstream.xml.TDIC_BCExstreamXMLCreator"
    final var CC_CREATOR = "tdic.cc.integ.plugins.hpexstream.xml.TDIC_CCExstreamXMLCreator"

    var xmlCreator: TDIC_ExstreamXMLCreator

    //var aClaim = (aParamMap.get(("Claim")) typeis SimpleSymbol)?(aParamMap.get("Claim") as SimpleSymbol)?.Value as Claim : aParamMap.get("Claim") as Claim
    var aClaim : Claim
    if(aParamMap.get(("Claim")) typeis Claim){
      aClaim = aParamMap.get("Claim") as Claim
    }
    else if (aParamMap.get(("Claim")) typeis Document) {
      aClaim = (aParamMap.get(("Claim")) as Document).Claim as Claim
    }
    else if (aParamMap.get(("Claim")) typeis SimpleSymbol) {
        aClaim = (aParamMap.get("Claim") as SimpleSymbol)?.Value as Claim
    }

     if (aClaim != null) {
      _logger.info("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - ClaimCenter")
       xmlCreator = ReflectUtil.construct(CC_CREATOR, new Object[]{aClaim})
     } else {
      throw new RuntimeException("Incorrect entity supplied for XML generation...")
    }

    _logger.debug("TDIC_ExstreamXMLCreatorFactory#getExstreamXMLCreator(Map<Object, Object>) - Exiting")
    return xmlCreator
  }
}

