/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 */
package com.tdic.plugins.hpexstream.core.bo.commandcenter

uses com.tdic.plugins.hpexstream.core.webservice.commandcenteraction.soap.elements.Attribute
uses com.tdic.plugins.hpexstream.core.webservice.commandcenteraction.soap.elements.Entry
uses com.tdic.util.properties.PropertyUtil
uses org.slf4j.LoggerFactory

/**
 * Data transfer object that represents a request sent to Command Center's Action API.
 *
 * @author Miro Kubicek
 */
class TDIC_CommandCenterActionRequest {
  var _jobDefId: String as JobDefinitionId
  var _jobDefName: String as JobDefinitionName
  var _dataFiles: java.util.Map<String, String> as DataFiles
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  construct() {
    _dataFiles = new java.util.HashMap <String, String>()
  }

  construct(aJobDefId: String, aJobDefName: String, aDriverName: String, anEncodedDriverContent: String) {
    _jobDefId = aJobDefId
    _jobDefName = aJobDefName
    _dataFiles = new java.util.HashMap <String, String>()
    _dataFiles.put(aDriverName, anEncodedDriverContent)
  }

  /**
   *  US555
   *  10/13/2014 shanem
   *
   *  Gets the attributes of the XML model
   */
  @Returns("A List of XML Attributes")
  function getXMLModelAttributes(): java.util.List <Attribute> {
    _logger.debug("TDIC_CommandCenterActionRequest#getXMLModelAttributes() - Entering")
    var attributes = new java.util.ArrayList <Attribute> ()
    if (JobDefinitionId != null) {
      var jobDefAttribute = new Attribute()
      jobDefAttribute.Value_Attribute = JobDefinitionId
      jobDefAttribute.Name = "job_definition_id"
      attributes.add(jobDefAttribute)
    } else if (JobDefinitionName != null) {
      var jobDefAttribute = new Attribute()
      jobDefAttribute.Value_Attribute = JobDefinitionName
      jobDefAttribute.Name = "job_definition_name"
      attributes.add(jobDefAttribute)
    }
    var dataFilesMap = new com.tdic.plugins.hpexstream.core.webservice.commandcenteraction.soap.elements.Map()
    for (key in _dataFiles.Keys) {
      var dataFileEntry = new Entry()
      dataFileEntry.Name = key
      dataFileEntry.Value_Element = _dataFiles.get(key)
      dataFilesMap.Entry.add(dataFileEntry)
    }
    var dataFilesAttribute = new Attribute()
    dataFilesAttribute.Name = "data_files"
    dataFilesAttribute.Map = dataFilesMap
    attributes.add(dataFilesAttribute)

    /*
    US669
    Shane Murphy  03/16/2015

    Overridden Variables added to cater for one HP Dev server responding to multiple GW servers (Dev, PCINT, BCINT, etc.)
    GWURL - the endpoint for HP to respond to
    ENV - the environment
    */
    var overriddenValuesMap = new com.tdic.plugins.hpexstream.core.webservice.commandcenteraction.soap.elements.Map()
    var gwUrl = new Entry(){
        : Name = "GWURL",
        : Value_Element = PropertyUtil.getInstance().getProperty("CommandCenterAPIsoap11")
    }
    var env = new Entry(){
        : Name = "ENV",
        : Value_Element = gw.api.system.server.ServerUtil.getEnv().toUpperCase()
    }
    overriddenValuesMap.Entry.add(gwUrl)
    overriddenValuesMap.Entry.add(env)
    var overRiddenValuesAttribute = new Attribute()
    overRiddenValuesAttribute.Name = "overridden_variables"
    overRiddenValuesAttribute.Map = overriddenValuesMap
    attributes.add(overRiddenValuesAttribute)

    _logger.debug("TDIC_CommandCenterActionRequest#getXMLModelAttributes() - Exiting")
    return attributes
  }
}
