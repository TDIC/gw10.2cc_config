/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package com.tdic.plugins.hpexstream.core.bo

@Export
class TDIC_BaseBusinessObject {
  private var _templateNames: TDIC_TemplateIds as TemplateIds
  private var _systemGenearated: boolean as SystemGenerated
  private var _eventName: String as EventName
  private var _separateDacs : boolean as SeparateDocuments
  private var _user : User as CUser
  protected var _roots: Editable[] as RootEntities
  construct() {
  }
}
