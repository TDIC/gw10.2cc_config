package com.tdic.plugins.hpexstream.core.util

uses java.util.Date

class ExstreamMapParam {
  var _effectiveDate: Date as EffectiveDate
  var _expirationsDate: Date as ExpirationDate
  var _eventName: String as EventName
  var _templateId: String as TemplateId
  var _printOrder: int as PrintOrder
  var _docType: String as DocType
}