/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 */
package com.tdic.plugins.hpexstream.core.bo.commandcenter

class TDIC_CommandCenterResponse {
  var _response: String as response
  var _policyNumber: String as policyNumber
  construct()
  {
  }

  property get response(): String {
    return response
  }

  property get policyNumber(): String {
    return policyNumber
  }
}
