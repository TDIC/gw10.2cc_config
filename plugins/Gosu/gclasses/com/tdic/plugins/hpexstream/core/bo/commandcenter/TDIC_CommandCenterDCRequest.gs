/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 */
package com.tdic.plugins.hpexstream.core.bo.commandcenter

uses org.slf4j.LoggerFactory

/**
 * Data transfer object that represents a request sent to Command Center's Data Channel.
 *
 * @author Miro Kubicek
 */
class TDIC_CommandCenterDCRequest {
  var _channelName: String as ChannelName
  var _jobDefId: String as JobDefinitionId
  var _jobDefName: String as JobDefinitionName
  var _dataFiles: java.util.Map<String, String> as DataFiles
  var _overriddenVariables: java.util.Map<String, String> as OverriddenVariables
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  construct() {
    _dataFiles = new java.util.HashMap <String, String>()
    _overriddenVariables = new java.util.HashMap <String, String>()
  }

  /**
   * US555
   * 10/13/2014 shanem
   *
   * Provides XML representaiton of current request.      *
   */
  @Returns("XML representaiton of Data Channel Requests")
  function parse(): String {
    _logger.debug("TDIC_CommandCenterDCRequest#parse() - Entering")

    var requestStringBuilder = new StringBuilder()
    requestStringBuilder.append("<cc:dc-request xmlns:cc=\"http://www.hp.com/schema/cc-dc\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.hp.com/schema/cc-dc/DCRequest.xsd\">")
        .append("<cc:channel-name>")
        .append(_channelName)
        .append("</cc:channel-name>")
        .appendIf(_jobDefId != null, "<cc:attribute name=\"job_definition_id\" value=\"")
        .appendIf(_jobDefId != null, _jobDefId)
        .appendIf(_jobDefId != null, "\"/>")
        .appendIf(_jobDefName != null, "<cc:attribute name=\"job_definition_name\" value=\"")
        .appendIf(_jobDefName != null, _jobDefName)
        .appendIf(_jobDefName != null, "\"/>")
        .appendIf(_overriddenVariables.Count > 0, "<cc:attribute name=\"overridden_variables\">")
        .appendIf(_overriddenVariables.Count > 0, "<cc:map>")

    for (key in _overriddenVariables.Keys) {
      requestStringBuilder = requestStringBuilder.append("<cc:entry name=\"").append(key).append("\" value=\"").append(_overriddenVariables.get(key)).append("\"/>")
    }
    requestStringBuilder.appendIf(_overriddenVariables.Count > 0, "</cc:map>")
        .appendIf(_overriddenVariables.Count > 0, "</cc:attribute>")
        .append("<cc:attribute name=\"data_files\">")
        .append("<cc:map>")
    for (key in _dataFiles.Keys) {
      requestStringBuilder.append("<cc:entry name=\"").append(key).append("\" value=\"").append(_dataFiles.get(key)).append("\"/>")
    }
    requestStringBuilder.append("</cc:map>")
        .append("</cc:attribute>")

    requestStringBuilder.append("<cc:attribute name=\"overridden_variables\">")
        .append("<cc:map>")
        .append("<cc:entry name=\"ENV\" value=\""+ gw.api.system.server.ServerUtil.getEnv().toUpperCase() +"\"/>")
        .append("</cc:map>")
        .append("</cc:attribute>")
        .append("</cc:dc-request>")
    _logger.debug("TDIC_CommandCenterDCRequest#parse() - Parsed XML: ${requestStringBuilder.toString()}")

    _logger.debug("TDIC_CommandCenterDCRequest#parse() - Exiting")
    return requestStringBuilder.toString()
  }
}
