package com.tdic.api.claim

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.server.AvailabilityLevel
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.PermissionException
uses gw.transaction.Transaction
uses gw.xml.ws.annotation.WsiAvailability
uses gw.xml.ws.annotation.WsiGenInToolkit
uses gw.xml.ws.annotation.WsiWebService

@WsiWebService("http://guidewire.com/cc/ws/com/tdic/api/claim/ClaimAPI")
@WsiAvailability(AvailabilityLevel.MAINTENANCE)
@WsiGenInToolkit
@Export
class ClaimAPI {

/**
 * Create flat cancel policy activity on claims
 * @param policyNumber
 * @param plyEffDate
 * @param plyExpDate
 */
  @Throws(BadIdentifierException, "If the policyNumber,plyEffDate and plyExpDate doesn't correspond to an existing Policy.")
  @Throws(PermissionException, "If the caller does not have the appropriate permissions.")
  function createFlatCancelPolicyActivity(policyNumber:String,plyEffDate:Date,plyExpDate:Date){
    Transaction.runWithNewBundle(\ bundle -> {
      var clmsQry = Query.make(Claim).compare(Claim#State, Relop.Equals,ClaimState.TC_OPEN)
          .join("Policy").compare(Policy#PolicyNumber, Relop.Equals,policyNumber)
          .compare(Policy#EffectiveDate, Relop.Equals, plyEffDate)
          .compare(Policy#ExpirationDate, Relop.Equals, plyExpDate).select()
      for(clm in clmsQry){
        bundle.add(clm)
        var activity = clm.createActivityFromPattern(null, ActivityPattern.finder.getActivityPatternByCode("policy_flat_cancelled"))
        activity.assignToClaimOwner()
      }
    })
  }

/**
 * Activity Deductible Received From Insured
 * @param clmNumber
 */
  @Throws(BadIdentifierException, "If the clmNumber doesn't correspond to an existing Claim.")
  @Throws(PermissionException, "If the caller does not have the appropriate permissions.")
  function createActivityDeductibleReceivedFromInsured(clmNumber:String){
    Transaction.runWithNewBundle(\ bundle -> {
      var clm = Query.make(Claim).compare(Claim#ClaimNumber, Relop.Equals, clmNumber).select().first()
      bundle.add(clm)
      var activity = clm.createActivityFromPattern(null, ActivityPattern.finder.getActivityPatternByCode("deductible_received_from_Insured"))
      activity.assignToClaimOwner()
    })
  }
}