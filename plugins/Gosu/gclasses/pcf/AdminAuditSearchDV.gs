package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/audit_ext/AdminAuditSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AdminAuditSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  gw.acc.adminaudit.util.AdminAuditSearchCriteria) : void {
    __widgetOf(this, pcf.AdminAuditSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria})
  }
  
  function refreshVariables ($searchCriteria :  gw.acc.adminaudit.util.AdminAuditSearchCriteria) : void {
    __widgetOf(this, pcf.AdminAuditSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria})
  }
  
  
}