package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsSelectedDocsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseShareDocumentsSelectedDocsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($claim :  Claim, $sharedDocsLinks :  OutbdShareDocLink_Ext[], $hasCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.ShareBaseShareDocumentsSelectedDocsLV, SECTION_WIDGET_CLASS).setVariables(false, {$claim, $sharedDocsLinks, $hasCheckboxes})
  }
  
  function refreshVariables ($claim :  Claim, $sharedDocsLinks :  OutbdShareDocLink_Ext[], $hasCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.ShareBaseShareDocumentsSelectedDocsLV, SECTION_WIDGET_CLASS).setVariables(true, {$claim, $sharedDocsLinks, $hasCheckboxes})
  }
  
  
}