package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseShareDocumentsReviewScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($claim :  Claim, $folder :  ShareBase_Ext, $share :  OutboundSharing_Ext) : void {
    __widgetOf(this, pcf.ShareBaseShareDocumentsReviewScreen, SECTION_WIDGET_CLASS).setVariables(false, {$claim, $folder, $share})
  }
  
  function refreshVariables ($claim :  Claim, $folder :  ShareBase_Ext, $share :  OutboundSharing_Ext) : void {
    __widgetOf(this, pcf.ShareBaseShareDocumentsReviewScreen, SECTION_WIDGET_CLASS).setVariables(true, {$claim, $folder, $share})
  }
  
  
}