package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/SharedDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SharedDocumentsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($sharedDocuments :  List<Document>) : void {
    __widgetOf(this, pcf.SharedDocumentsLV, SECTION_WIDGET_CLASS).setVariables(false, {$sharedDocuments})
  }
  
  function refreshVariables ($sharedDocuments :  List<Document>) : void {
    __widgetOf(this, pcf.SharedDocumentsLV, SECTION_WIDGET_CLASS).setVariables(true, {$sharedDocuments})
  }
  
  
}