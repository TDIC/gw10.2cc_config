package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/lossdetails/TDIC_InjuryNewPersonOnlyPickerMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_InjuryNewPersonOnlyPickerMenuItemSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($requiredContactType :  Type, $parentContact :  Contact, $claim :  Claim, $contactRole :  typekey.ContactRole, $injuryIncident :  InjuryIncident) : void {
    __widgetOf(this, pcf.TDIC_InjuryNewPersonOnlyPickerMenuItemSet, SECTION_WIDGET_CLASS).setVariables(false, {$requiredContactType, $parentContact, $claim, $contactRole, $injuryIncident})
  }
  
  function refreshVariables ($requiredContactType :  Type, $parentContact :  Contact, $claim :  Claim, $contactRole :  typekey.ContactRole, $injuryIncident :  InjuryIncident) : void {
    __widgetOf(this, pcf.TDIC_InjuryNewPersonOnlyPickerMenuItemSet, SECTION_WIDGET_CLASS).setVariables(true, {$requiredContactType, $parentContact, $claim, $contactRole, $injuryIncident})
  }
  
  
}