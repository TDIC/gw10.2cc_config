package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseNewFolderDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseNewFolderDetailsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($Claim :  Claim, $Folder :  ShareBase_Ext) : void {
    __widgetOf(this, pcf.ShareBaseNewFolderDetailsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$Claim, $Folder})
  }
  
  function refreshVariables ($Claim :  Claim, $Folder :  ShareBase_Ext) : void {
    __widgetOf(this, pcf.ShareBaseNewFolderDetailsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$Claim, $Folder})
  }
  
  
}