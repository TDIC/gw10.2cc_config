package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/ASRecordsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ASRecordsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($ASRecordsList :  gw.api.database.IQueryBeanResult<AssignStrategy_TDIC>) : void {
    __widgetOf(this, pcf.ASRecordsLV, SECTION_WIDGET_CLASS).setVariables(false, {$ASRecordsList})
  }
  
  function refreshVariables ($ASRecordsList :  gw.api.database.IQueryBeanResult<AssignStrategy_TDIC>) : void {
    __widgetOf(this, pcf.ASRecordsLV, SECTION_WIDGET_CLASS).setVariables(true, {$ASRecordsList})
  }
  
  
}