package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/addressbook/shared/CCPropertyAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CCPropertyAddressInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($addressOwner :  gw.api.address.CCAddressOwner, $wizard :  gw.api.claim.NewClaimWizardInfo) : void {
    __widgetOf(this, pcf.CCPropertyAddressInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$addressOwner, $wizard})
  }
  
  function refreshVariables ($addressOwner :  gw.api.address.CCAddressOwner, $wizard :  gw.api.claim.NewClaimWizardInfo) : void {
    __widgetOf(this, pcf.CCPropertyAddressInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$addressOwner, $wizard})
  }
  
  
}