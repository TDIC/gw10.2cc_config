package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_AdditionalInfoInputSet.Adjudicator.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_AdditionalInfoInputSet_Adjudicator extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($contactHandle :  gw.api.contact.ContactHandle, $claimContact :  ClaimContact, $claim :  Claim, $contactRole :  typekey.ContactRole, $isAdditionalInfoVisible :  boolean) : void {
    __widgetOf(this, pcf.TDIC_AdditionalInfoInputSet_Adjudicator, SECTION_WIDGET_CLASS).setVariables(false, {$contactHandle, $claimContact, $claim, $contactRole, $isAdditionalInfoVisible})
  }
  
  function refreshVariables ($contactHandle :  gw.api.contact.ContactHandle, $claimContact :  ClaimContact, $claim :  Claim, $contactRole :  typekey.ContactRole, $isAdditionalInfoVisible :  boolean) : void {
    __widgetOf(this, pcf.TDIC_AdditionalInfoInputSet_Adjudicator, SECTION_WIDGET_CLASS).setVariables(true, {$contactHandle, $claimContact, $claim, $contactRole, $isAdditionalInfoVisible})
  }
  
  
}