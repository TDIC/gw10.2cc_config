package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderRequestReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseFolderRequestReviewScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($claim :  Claim, $folder :  ShareBase_Ext, $Request :  InboundRequest_Ext) : void {
    __widgetOf(this, pcf.ShareBaseFolderRequestReviewScreen, SECTION_WIDGET_CLASS).setVariables(false, {$claim, $folder, $Request})
  }
  
  function refreshVariables ($claim :  Claim, $folder :  ShareBase_Ext, $Request :  InboundRequest_Ext) : void {
    __widgetOf(this, pcf.ShareBaseFolderRequestReviewScreen, SECTION_WIDGET_CLASS).setVariables(true, {$claim, $folder, $Request})
  }
  
  
}