package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsAvailableDocsInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ShareBaseShareDocumentsAvailableDocsInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($claim :  Claim, $outboundSharing :  OutboundSharing_Ext) : void {
    __widgetOf(this, pcf.ShareBaseShareDocumentsAvailableDocsInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$claim, $outboundSharing})
  }
  
  function refreshVariables ($claim :  Claim, $outboundSharing :  OutboundSharing_Ext) : void {
    __widgetOf(this, pcf.ShareBaseShareDocumentsAvailableDocsInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$claim, $outboundSharing})
  }
  
  
}