package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_BasicInfoScreen.WC7.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class FNOLWizard_BasicInfoScreen_WC7 extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($Claim :  Claim, $Wizard :  gw.api.claim.NewClaimWizardInfo, $basicInfoUtils :  gw.pcf.fnol.BasicInfoScreenUtils) : void {
    __widgetOf(this, pcf.FNOLWizard_BasicInfoScreen_WC7, SECTION_WIDGET_CLASS).setVariables(false, {$Claim, $Wizard, $basicInfoUtils})
  }
  
  function refreshVariables ($Claim :  Claim, $Wizard :  gw.api.claim.NewClaimWizardInfo, $basicInfoUtils :  gw.pcf.fnol.BasicInfoScreenUtils) : void {
    __widgetOf(this, pcf.FNOLWizard_BasicInfoScreen_WC7, SECTION_WIDGET_CLASS).setVariables(true, {$Claim, $Wizard, $basicInfoUtils})
  }
  
  
}