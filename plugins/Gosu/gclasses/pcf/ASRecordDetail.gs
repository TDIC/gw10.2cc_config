package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/ASRecordDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ASRecordDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (ASRecord :  AssignStrategy_TDIC) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ASRecordDetail, {ASRecord}, 0)
  }
  
  static function drilldown (ASRecord :  AssignStrategy_TDIC) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ASRecordDetail, {ASRecord}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (ASRecord :  AssignStrategy_TDIC) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ASRecordDetail, {ASRecord}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (ASRecord :  AssignStrategy_TDIC) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ASRecordDetail, {ASRecord}, 0).goInMain()
  }
  
  static function printPage (ASRecord :  AssignStrategy_TDIC) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ASRecordDetail, {ASRecord}, 0).printPage()
  }
  
  static function push (ASRecord :  AssignStrategy_TDIC) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ASRecordDetail, {ASRecord}, 0).push()
  }
  
  
}