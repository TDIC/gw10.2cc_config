package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_NegotiationCoverageInputSet.property.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_NegotiationCoverageInputSet_property extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($claim :  Claim) : void {
    __widgetOf(this, pcf.TDIC_NegotiationCoverageInputSet_property, SECTION_WIDGET_CLASS).setVariables(false, {$claim})
  }
  
  function refreshVariables ($claim :  Claim) : void {
    __widgetOf(this, pcf.TDIC_NegotiationCoverageInputSet_property, SECTION_WIDGET_CLASS).setVariables(true, {$claim})
  }
  
  
}