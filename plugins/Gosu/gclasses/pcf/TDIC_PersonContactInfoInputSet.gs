package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_PersonContactInfoInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_PersonContactInfoInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($contactHandle :  gw.api.contact.ContactHandle, $claimContact :  ClaimContact, $contactRole :  typekey.ContactRole) : void {
    __widgetOf(this, pcf.TDIC_PersonContactInfoInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$contactHandle, $claimContact, $contactRole})
  }
  
  function refreshVariables ($contactHandle :  gw.api.contact.ContactHandle, $claimContact :  ClaimContact, $contactRole :  typekey.ContactRole) : void {
    __widgetOf(this, pcf.TDIC_PersonContactInfoInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$contactHandle, $claimContact, $contactRole})
  }
  
  
}