package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_ContactDetailToolbarButtonSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_ContactDetailToolbarButtonSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($contactHandle :  gw.api.contact.ContactHandle, $contactRole :  typekey.ContactRole, $canAccessLinkButtons :  boolean, $linkStatus :  gw.api.contact.ContactSystemLinkStatus, $canPick :  boolean, $allowEditInAddressBook :  boolean) : void {
    __widgetOf(this, pcf.TDIC_ContactDetailToolbarButtonSet, SECTION_WIDGET_CLASS).setVariables(false, {$contactHandle, $contactRole, $canAccessLinkButtons, $linkStatus, $canPick, $allowEditInAddressBook})
  }
  
  function refreshVariables ($contactHandle :  gw.api.contact.ContactHandle, $contactRole :  typekey.ContactRole, $canAccessLinkButtons :  boolean, $linkStatus :  gw.api.contact.ContactSystemLinkStatus, $canPick :  boolean, $allowEditInAddressBook :  boolean) : void {
    __widgetOf(this, pcf.TDIC_ContactDetailToolbarButtonSet, SECTION_WIDGET_CLASS).setVariables(true, {$contactHandle, $contactRole, $canAccessLinkButtons, $linkStatus, $canPick, $allowEditInAddressBook})
  }
  
  
}