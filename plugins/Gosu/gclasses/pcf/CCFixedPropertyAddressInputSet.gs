package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/addressbook/shared/CCFixedPropertyAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CCFixedPropertyAddressInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($addressOwner :  gw.api.address.CCAddressOwner) : void {
    __widgetOf(this, pcf.CCFixedPropertyAddressInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$addressOwner})
  }
  
  function refreshVariables ($addressOwner :  gw.api.address.CCAddressOwner) : void {
    __widgetOf(this, pcf.CCFixedPropertyAddressInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$addressOwner})
  }
  
  
}