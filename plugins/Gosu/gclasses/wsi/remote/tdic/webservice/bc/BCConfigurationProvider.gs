package wsi.remote.tdic.webservice.bc

uses javax.xml.namespace.QName
uses gw.xml.ws.WsdlConfig
uses gw.xml.ws.IWsiWebserviceConfigurationProvider
uses gw.api.util.LocaleUtil

@Export
class BCConfigurationProvider implements IWsiWebserviceConfigurationProvider {

  override function configure( serviceName : QName, portName : QName, config : WsdlConfig )  {
    config.Guidewire.Authentication.Username = "su"
    config.Guidewire.Authentication.Password = "gw"
    //set the locale header
    config.Guidewire.Locale = LocaleUtil.CurrentLanguage.toString()
  }

}