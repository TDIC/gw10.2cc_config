package acc.onbase.webservice

uses acc.onbase.api.si.CCModelBuilder
uses acc.onbase.api.si.schema.onbasemessage.OnBaseMessage
uses acc.onbase.util.LoggerFactory
uses acc.onbase.webservice.util.DocumentModelParser
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.locale.DisplayKey
uses gw.transaction.Transaction
uses gw.xml.ws.annotation.WsiAvailability
uses gw.xml.ws.annotation.WsiPermissions
uses gw.xml.ws.annotation.WsiWebService

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 01/30/2018 - Daniel Q. Yu
 * * Initial implementation
 */


/**
 * Guidewire web service returns claim and exposure metadata information.
 */
@WsiWebService("http://onbase/acc/claim/webservice/OnBaseDocumentAPI")
@WsiPermissions({SystemPermissionType.TC_SOAPADMIN})
@WsiAvailability(MULTIUSER)
class OnBaseDocumentAPI {
  /**
   * Logger for OnBaseDMS
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  function newDocumentArchived(doc : acc.onbase.api.si.model.documentmodel.Document) : String {

    var parser = new DocumentModelParser(doc)

    var document : entity.Document = null
    parser.validatePolicy()

    Transaction.runWithNewBundle(\bundle -> {
      document = parser.getNewDocument()
      document.DocUID = doc.DocUID
      document.DocumentIdentifier = "Imported from OnBase"
      document.Claim = parser.Claim
      document.Exposure = parser.Exposure
      document.Name = doc.Name
      document.Description = doc.Description
      document.Author = doc.Author
      document.Recipient = doc.Recipient
      document.SecurityType = doc.SecurityType
      document.Status = parser.Status
      document.Type = parser.Type
      document.Subtype = parser.Subtype
      document.MimeType = parser.MimeType
      document.DateCreated = java.util.Date.Now
      document.DateModified = java.util.Date.Now
      document.DMS = true
    })

    return document.PublicID
  }

  function asyncDocumentArchived(doc : acc.onbase.api.si.model.documentmodel.Document) : OnBaseMessage {
    var parser = new DocumentModelParser(doc)
    var document = parser.getPendingDocument()

    // Because GW side should have most current metadata information,
    // all other keyword values will not be checked and should NOT be passed back.
    // If keyword values are modified in OnBase, use documentUpdated webservice to update GW side.

    Transaction.runWithNewBundle(\bundle -> {
      document = bundle.add(document)
      document.DocUID = doc.DocUID
      document.DocumentIdentifier = "Async document archived"
      document.PendingDocUID = null
      document.DMS=true
      document.Status=DocumentStatusType.TC_FINAL
      if (doc.Type?.OnBaseName?.HasContent){
        document.Type= parser.Type
      }
      if (doc.Subtype?.OnBaseName?.HasContent){
        document.Subtype = parser.Subtype
      }
      if (doc.Name?.HasContent){
        document.Name = doc.Name
      }
    })

    // build a message out of the document and return it
    return CCModelBuilder.buildArchiveMessage(document).Message
  }

  function documentUpdated(doc : acc.onbase.api.si.model.documentmodel.Document) : String {
    // Not only the changed fields, but all fields must be passed in.

    var parser = new DocumentModelParser(doc)
    parser.validatePolicy()
    var document = parser.getExistingDocument()


    Transaction.runWithNewBundle(\bundle -> {
      document = bundle.add(document)
      document.DocUID = doc.DocUID
      document.DocumentIdentifier = "Updated from OnBase"
      document.Claim = parser.Claim
      document.Exposure = parser.Exposure
      document.Name = doc.Name
      document.SecurityType = doc.SecurityType
      if (doc.Description?.HasContent){
        document.Description = doc.Description
      }
      document.Author = doc.Author
      document.Recipient = doc.Recipient
      document.Status = parser.Status
      document.Type = parser.Type
      document.Subtype = parser.Subtype
      if (doc.MimeType != "application/octet-stream") {
        document.MimeType = parser.MimeType
      }
      document.DateModified = java.util.Date.Now
      document.DMS = true

    })

    return document.PublicID
  }
}