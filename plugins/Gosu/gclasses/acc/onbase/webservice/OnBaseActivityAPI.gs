package acc.onbase.webservice

uses acc.onbase.api.application.InboundRequestManager
uses acc.onbase.api.application.OutboundSharingManager
uses acc.onbase.util.LoggerFactory
uses entity.Activity
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.locale.DisplayKey
uses gw.transaction.Transaction
uses gw.xml.ws.annotation.WsiAvailability
uses gw.xml.ws.annotation.WsiPermissions
uses gw.xml.ws.annotation.WsiWebService

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 02/22/2018 - Daniel Q. Yu
 * * Initial implementation
 */


/**
 * Guidewire web service returns claim and exposure metadata information.
 */
@WsiWebService("http://onbase/acc/claim/webservice/OnBaseActivityAPI")
@WsiPermissions({SystemPermissionType.TC_SOAPADMIN})
@WsiAvailability(MULTIUSER)
class OnBaseActivityAPI {
  /**
   * Logger for OnBaseDMS
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  function createNewActivity(activity: acc.onbase.api.si.model.activitymodel.Activity): String {

    // Get the activity pattern code for this activity
    var activityPattern: ActivityPattern = null
    if (activity.ActivityPattern.Code.HasContent) {
      activityPattern = ActivityPattern.finder.getActivityPatternByCode(activity.ActivityPattern.Code)
      if (activityPattern == null) {
        //invalid activity pattern code entered
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidActivityCode", activity.ActivityPattern.Code))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidActivityCode", activity.ActivityPattern.Code))
      }
    } else {
      // no activity pattern code entered
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingActivityCode"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingActivityCode"))
    }

    // Get claim for the activity.
    var claim: Claim = null
    if (activity.Claim != null && activity.Claim.ClaimNumber.HasContent) {
      claim = Claim.finder.findClaimByClaimNumber(activity.Claim.ClaimNumber)
      if (claim == null) {
        //invalid claim entered
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidClaimNumber", activity.Claim.ClaimNumber))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidClaimNumber", activity.Claim.ClaimNumber))
      }
    } else {
      // no claim entered
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingClaimNumber"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingClaimNumber"))
    }

    // Get documents for this activity
    var docList = new ArrayList<Document>()
    if (activity.Documents.Entry.size() > 0) {
      foreach(e in activity.Documents.Entry) {
        var document = Query.make(entity.Document).compare(entity.Document#DocUID, Relop.Equals, e.Document.DocUID).select().AtMostOneRow
        if (document != null) {
          if (document.Claim == claim) {
            docList.add(document)
          } else {
            _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvliadClaimDocUID", e.Document.DocUID, claim.ClaimNumber))
            throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvliadClaimDocUID", e.Document.DocUID, claim.ClaimNumber))
          }
        } else {
          // docUID not found
          _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownDocUID", e.Document.DocUID))
          throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownDocUID", e.Document.DocUID))
        }
      }
    } else {
      // missing docUID
      _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocUID"))
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocUID"))
    }

    // Get exposure for the activity.
    var exposure: Exposure = null
    if (activity.Exposure != null && activity.Exposure.PublicID.HasContent) {
      exposure = claim.Exposures.firstWhere(\e -> e.PublicID.equalsIgnoreCase(activity.Exposure.PublicID))
      if (exposure == null) {
        _logger.error(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidExposureID", activity.Exposure.PublicID))
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidExposureID", activity.Exposure.PublicID))
      }
    }

    // Activity can also be linked to contact/matter/service requests
    // Add them later if necessary.


    // Create activity
    var newActivity: Activity = null
    Transaction.runWithNewBundle(\bundle -> {
      claim = bundle.add(claim)
      newActivity = claim.createActivityFromPattern(exposure, activityPattern)

      foreach(var doc in docList) {
        newActivity.addLinkedDocument(doc)
      }
      newActivity.Subject = "Activity created by document " + docList
    })

    return newActivity.PublicID
  }

  /**
   * Creates an activity with a link to a ShareBase folder
   * Parameters:
   * folderPublicID - public ID of the ShareBase folder
   * folderStatus - tracks how far in the process the folder has been verified to be
   * commChannelPublicID - public ID of the request/share
   * activityPatternCode - the activity pattern for the created activity
   * commChannelType - is this a request for new documents (inbound), or a sharing of existing documents (outbound)?
   */
  function createActivityWithShareBaseFolderLink(folderPublicID: String, folderStatus: String, commChannelPublicID: String, activityPatternCode: String, commChannelType: String): String {

    var commType: ShareBaseRequestType_Ext = null
    var sbFolder: ShareBase_Ext = null

    //Find the activity pattern
    var activityPattern: ActivityPattern = null
    if (activityPatternCode.HasContent) {
      activityPattern = ActivityPattern.finder.getActivityPatternByCode(activityPatternCode)
      if (activityPattern == null) {
        //invalid activity pattern code entered
        _logger.error("Activity Pattern Code " + activityPatternCode + " not found")
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidActivityCode", activityPatternCode))
      }
    } else {
      // no activity pattern entered
      _logger.error("Must provide an Activity pattern code")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingActivityCode"))
    }

    //Get the Communication Channel(Inbound/Outbound)
    if (!commChannelType.HasContent) {
      _logger.error("Must pass a valid communication direction(Inbound/outbound)")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingCommunicationDirection"))
    } else {
      commType = ShareBaseRequestType_Ext.get(commChannelType)  // get the typekey
      // Make sure we got a valid communication direction
      if (commType == null) {
        _logger.error("Invalid communication direction given at Activity creation")
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidCommunicationDirection"))
      }
    }

    //Find sharebase folder
    if (!folderPublicID.HasContent) {
      _logger.error("Missing ShareBase Folder ID")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingFolderID"))
    } else {
      sbFolder = Query.make(entity.ShareBase_Ext).compare(entity.ShareBase_Ext#PublicID, Equals, folderPublicID).select().AtMostOneRow
      if (sbFolder == null) {
        _logger.error("Invalid ShareBase Folder ID " + folderPublicID)
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidFolderID", folderPublicID))
      }
    }

    //Get Folder Status
    var sbFolderStatus: ShareBaseRequestStatus
    //throw an exception if Folder Status is not specified
    if (!folderStatus.HasContent) {
      _logger.error("Missing ShareBase Folder Status")
      throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingFolderStatus"))
    } else {
      sbFolderStatus = ShareBaseRequestStatus.get(folderStatus)
      if (sbFolderStatus == null) {
        _logger.error("ShareBase Folder : " + sbFolder.FolderName + ": Invalid communication status")
        throw new IllegalArgumentException(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidFolderStatus", sbFolder.FolderName))
      }
    }

    //Get inbound request/outbound share associated with the folder and assign the claim and owner
    var claim: Claim = null
    var owner: User = null
    if (commType == ShareBaseRequestType_Ext.TC_INBOUND) {
      var ibRequestManager = new InboundRequestManager()
      var ibRequest = ibRequestManager.findInboundRequestByID(commChannelPublicID)
      ibRequestManager.validateFolderForRequest(ibRequest, sbFolder)
      ibRequestManager.validateActivityFolderStatusForRequest(sbFolderStatus, sbFolder)
      claim = ibRequest.Claim
      owner = ibRequest.Owner
    } else if (commType == ShareBaseRequestType_Ext.TC_OUTBOUND) {
      var obSharingManager = new OutboundSharingManager()
      var obShare = obSharingManager.findOutboundShareByID(commChannelPublicID)
      obSharingManager.validateFolderForShare(obShare, sbFolder)
      obSharingManager.validateActivityFolderStatusForShare(sbFolderStatus, sbFolder)
      claim = obShare.Claim
      owner = obShare.Owner
    }

    //Create an activty with the Sharebase Folder URL set to the ExternalURL_Ext field. The activity is assigned to the owner of the request
    var activitySubject: String
    var description: String
    if (sbFolder.Status == ShareBaseRequestStatus.TC_ERROR) {
      if (commType == ShareBaseRequestType_Ext.TC_INBOUND) {
        activitySubject = DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_ShareBaseActivityErrorMonitoringSubject", sbFolder.FolderName)
        description = DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_ShareBaseActivityErrorMonitoringDescription", sbFolder.FolderName)
      } else if (commType == ShareBaseRequestType_Ext.TC_OUTBOUND) {
        activitySubject = DisplayKey.get("Accelerator.OnBase.WS.Error.STR_STR_GW_ShareBaseActivityErrorInSharingDocumentsSubject", sbFolder.FolderName)
        description = DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_ShareBaseActivityErrorInSharingDocumentsDecription", sbFolder.FolderName)
      }
    } else {
      if (commType == ShareBaseRequestType_Ext.TC_INBOUND) {
        activitySubject = DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_ShareBaseActivitySuccessUploadSubject", sbFolder.FolderName)
      } else if (commType == ShareBaseRequestType_Ext.TC_OUTBOUND) {
        activitySubject = DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_ShareBaseActivitySuccessShareSubject", sbFolder.FolderName)
      }
      description = DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_ShareBaseActivitySuccessDescription", sbFolder.FolderName)
    }

    var newActivity: Activity = null
    Transaction.runWithNewBundle(\bundle -> {
      //Update Folder link and Status
      sbFolder = bundle.add(sbFolder)
      sbFolder.Status = ShareBaseRequestStatus.get(folderStatus)
      claim = bundle.add(claim)
      newActivity = claim.createActivityFromPattern(null, activityPattern)
      newActivity.Subject = activitySubject
      newActivity.Description = description
      newActivity.AssignedUser = owner
      if (sbFolder.Status != ShareBaseRequestStatus.TC_ERROR) {  // if it's an error status, don't actually throw an exception because we want that reported
        newActivity.ExternalUrl_Ext = sbFolder.FolderLink
      }
    })
    return newActivity.PublicID
  }
}