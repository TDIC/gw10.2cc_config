package acc.onbase.configuration

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 *
 * Enum to select the global OnBase client
 */
enum OnBaseClientType {
  Unity, Web
}