package acc.onbase.api.sharebaseconfig

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 *
 * Interface to call ShareBase API
 */
interface ShareBaseInterface {

  public function getFoldersForLibraryfromAPI()

  public function createFolderfromAPI(folder: ShareBase_Ext)

  public function getShareBaseLink(folder: ShareBase_Ext)

  public function revokeShare(folder: ShareBase_Ext)

  public function deleteFolderInShareBase(folder: ShareBase_Ext)

}