package acc.onbase.api.pluginimpl

uses acc.onbase.api.application.DocumentArchival
uses acc.onbase.api.application.DocumentRetrieval
uses acc.onbase.api.exception.NullContentException
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
uses gw.api.util.DisplayableException
uses gw.document.DocumentContentsInfo
uses gw.plugin.InitializablePlugin
uses gw.plugin.Plugins
uses gw.plugin.document.IDocumentContentSource
uses gw.xml.ws.WsdlFault

uses java.io.InputStream

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 05/12/2016 - Anirudh Mohan
 * * Merged OnBaseDocumentContentSource.gs and DocumentContenSource.gs into 1 file for efficiency
 * * DOCUMENT_CONTENTS being used as a response type for getDocumentContentsInfo(Retrieving docs) instead of URL
 * <p>
 * 05/16/2016 - Anirudh Mohan
 * * Implemented the updateDocument function
 * <p>
 * 06/07/2016 - Anirudh Mohan
 * * Implemented removeDocument function
 * <p>
 * 06/08/2016 - Anirudh Mohan
 * * Updated the keyword in updateDoc from relateddocumenthandle to documentidforrevision
 * <p>
 * 06/21/2016 - Anirudh Mohan
 * * Fixed the getDocumentContentsInformation to be able to view docs from Web Client also by viewing it via a window.open
 * <p>
 * 01/09/2017 - Anirudh Mohan
 * * Removed GW Document's Claimant details from being added to onbase.
 * * Replaced hardcoded keyword names of exposure/contact/matter with the value in KeywordMap.
 * <p>
 * 01/10/2017 - Daniel Q. Yu
 * * Added claim security keyword to archived documents.
 * * Added exposure security level and role keywords to archived documents.
 * * Added document security level to archived documents.
 * * Change keyword names from string to KeywordMap enum values.
 * <p>
 * 03/13/2017 - Anirudh Mohan
 * * Added Service Requests
 * <p>
 * 05/01/2017 - Tori Brenneison
 * * Changes to GetDocumentContentsInformation
 * * Display content in Unity Client orphan window w/ instructions for user to close window
 * * Use document.location.href to avoid orphaned window for Web Client
 * * Use hidden iframe to avoid orphaned window in IE for Unity Client
 * <p>
 * 06/02/2017 - Tori Brenneison
 * * Changed location of OnBase client settings to ViewDocumentsUtil
 * <p>
 * 06/27/2017 - Chris Hoffman
 * * Refactored getDocumentContentsInformation to DocumentRetrieval.gs
 */

/**
 * IDocumentContentSource plugin implementation with OnBase as DMS.
 */
class DocumentContentSource implements IDocumentContentSource, InitializablePlugin {
  /**
   * Logger for OnBaseDMS
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.PluginLoggerCategory)

  /**
   * If true document can be stored, updated and removed from OnBase.
   */
  override property get InboundAvailable(): boolean {
    return true
  }

  /**
   * If true document can be searched and viewed from OnBase.
   */
  override property get OutboundAvailable(): boolean {
    return true
  }

  /**
   * Add a document to OnBase. Document content and metadata are archived to OnBase here.
   * <p>
   * This method returns false if we also want Guidewire OOTB implementation to save
   * the document metadata when IDocumentMetadataSource plugin is not used.
   *
   * @param documentContents The document content input stream.
   * @param document         The document object.
   * @return True if document meta data information has been saved. Or false then IDocumentMetadataSource.saveDocument will be called to save meta data.
   */
  override function addDocument(documentContents: InputStream, document: Document): boolean {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentContentSource.addDocument(" + documentContents + ", " + document + ")")
    }

    // Document content is null, update document meta data only.
    if (documentContents == null) {
      if (isDocument(document)) {
        document.DateModified = DateUtil.currentDate()
      }
      else {
        _logger.error("Calling DocumentContentSource.addDocument for new document without document content.")
        throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.DocumentArchiveFailure.STR_GW_ZeroByteException"))
      }
      if (_logger.DebugEnabled) {
        _logger.debug("Document " + document.DocUID + " has been added to OnBase.")
      }
      // return true to ignore IDocumentMetadataSource.saveDocument or false uses Guidewire OOTB implementation to save metadata again.
      return Plugins.isEnabled("IDocumentMetadataSource")
    }

    // Call acc.onbase.api.api.application to do the real work.
    var docUID = null as String
    try {
      var archivalApp = new DocumentArchival()
      docUID = archivalApp.archiveDocument(documentContents, document, OnBaseConfigurationFactory.Instance.AsyncDocumentFolder, OnBaseConfigurationFactory.Instance.AsyncDocumentSize)
    }
    catch (ex1: NullContentException) {
      if (isDocument(document)) {
        // Document is created document template and from rules, do nothing here.
      } else {
        _logger.error(document.Name + " contains zero byte document content.  Please remove the document from the upload list.",ex1)
        throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.DocumentArchiveFailure.STR_GW_ZeroByteDocumentAttempt", document.Name))
      }
      return Plugins.isEnabled("IDocumentMetadataSource")
    }
    catch (ex2: WsdlFault) {
      var errorMessage = DisplayKey.get("Accelerator.OnBase.Messaging.STR_GW_CannotArchive", document.Name)
      //if document content has been archived but keyword update fails, reset the docUID to null
      if (document.DocUID.HasContent) {
        document.DocUID = null
      }
      _logger.error("Adding document to OnBase failed!", ex2)
      throw new DisplayableException(errorMessage)
    }
    catch (ex3: Exception) {
      var errorMessage = DisplayKey.get("Accelerator.OnBase.Messaging.STR_GW_AddingDocFailed")
      _logger.error("Adding document to OnBase failed!", ex3)
      throw new DisplayableException(errorMessage)
    }

    if (docUID == null) {
      // No metadata needs to be saved, so return true here.
      return true
    }
    else {
      // return true to ignore IDocumentMetadataSource.saveDocument or false uses Guidewire OOTB implementation to save metadata again.
      return Plugins.isEnabled("IDocumentMetadataSource")
    }
  }

  /**
   * Display document in OnBase Unity/Web client.
   *
   * @param document                The document to be displayed.
   * @param includeDocumentContents If true includes document content. Currently not used by integration.
   * @return The DocumentContentsInfo object for this document.
   */
  override function getDocumentContentsInfo(document: Document, includeDocumentContents: boolean): DocumentContentsInfo {
    var retrievalApp = new DocumentRetrieval()
    return retrievalApp.getDocumentContentsInfo(document.DocUID, includeDocumentContents)
  }

  /**
   * Get the document content in OnBase for external use.
   *
   * @param document The document which content to be retrieved from OnBase.
   * @return The DocumentContentsInfo object for this document.
   */
  override function getDocumentContentsInfoForExternalUse(document: Document): DocumentContentsInfo {
    var dci = new DocumentContentsInfo(DocumentContentsInfo.ContentResponseType.DOCUMENT_CONTENTS, getDocumentInputStream(document), document.getMimeType())
    return dci
  }

  /**
   * Get the document content input stream from OnBase.
   *
   * @param document The document to be downloaded.
   * @return The document input stream.
   */
  function getDocumentInputStream(document: Document): InputStream {
    var retrievalApp = new DocumentRetrieval()
    return retrievalApp.getDocumentContent(document)
  }

  /**
   * Is this a valid document?
   *
   * @param document The document to be checked.
   * @return True if it is a valid document.
   */
  override function isDocument(document: Document): boolean {
    if (document.DocUID != null && !document.DocUID.equalsIgnoreCase("none")) {
      return true
    } else {
      return false
    }
  }

  /**
   * Remove document from OnBase.
   *
   * @param document The document to be deleted.
   * @return True if document has been deleted.
   */
  override function removeDocument(document: Document): boolean {
    //return true so that OnBaseDocumentMetadataSource won't be used
    //Note: if u want to update the keywords in OnBase then u have to do it HERE, before u return
    return false
  }

  /**
   * Update document in OnBase. Store the document as a revision
   *
   * @param document   The document to be updated.
   * @param documentIS The document content input stream.
   * @return True if document has been updated.
   */
  override function updateDocument(document: Document, documentIS: InputStream): boolean {
    //## todo: Implement me
    _logger.error("OnBaseDocumentContentSource.updateDocument not implemented.")
    throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.DocumentArchiveFailure.STR_GW_UpdateDocument"))
  }

  /**
   * Set plugin parameters.
   *
   * @param parameters The parameters for this plugin.
   */
  override property  set Parameters(parameters: Map<Object, Object>) {
  }
}
