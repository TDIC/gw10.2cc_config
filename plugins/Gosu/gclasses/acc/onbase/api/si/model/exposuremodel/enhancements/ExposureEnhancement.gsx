package acc.onbase.api.si.model.exposuremodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ExposureEnhancement : acc.onbase.api.si.model.exposuremodel.Exposure {
  public static function create(object : entity.Exposure) : acc.onbase.api.si.model.exposuremodel.Exposure {
    return new acc.onbase.api.si.model.exposuremodel.Exposure(object)
  }

  public static function create(object : entity.Exposure, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.exposuremodel.Exposure {
    return new acc.onbase.api.si.model.exposuremodel.Exposure(object, options)
  }

}