package acc.onbase.api.exception

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Server connection exception in the service-tier
 * <p>
 * Last Changes:
 * 02/11/2015 - Richard R. Kantimahanthi
 * * Initial implementation.
 */

class ServicesTierServerConnectionException extends ServicesTierException {
  construct(msg: String) {
    super(msg);
  }

  construct(msg: String, ex: Throwable) {
    super(msg, ex);
  }
}