package acc.onbase.api.application

uses acc.onbase.api.exception.InvalidContentException
uses acc.onbase.api.exception.NullContentException
uses acc.onbase.api.exception.WriteFailedException
uses acc.onbase.api.service.ServicesManager
uses acc.onbase.api.si.CCModelBuilder
uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
uses gw.api.util.DisplayableException

uses java.io.BufferedOutputStream
uses java.io.ByteArrayOutputStream
uses java.io.File
uses java.io.FileOutputStream
uses java.io.InputStream
/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 01/16/2015 - Daniel Q. Yu
 * * Initial implementation, refactored from OnBaseDocumentContentSource.gs
 * <p>
 * 01/23/2015 - Daniel Q. Yu
 * * Passing asyncFolder & asyncSize as method parameters.
 * * Throws InvalidContentException and WriteFailedException.
 * <p>
 * 02/05/2015 - Richard R. Kantimahanthi
 * * Modified 'archiveDocument' method signature to accept Document entity instead of the document metadata. This is done so in order to fire off the 'DocumentStore' custom event in case of Services Tier Exception.
 * * The event when fired will push the request into the GW message queue to be processed asynchronously.
 * <p>
 * 02/06/2015 - Richard R. Kantimahanthi
 * * Modified 'archiveDocument' method to also catch the 'BreakerOpenException' exception.
 * <p>
 * 02/23/2015 - Richard R. Kantimahanthi
 * * Modified 'archiveDocument' method to also catch the 'ServicesTierServerConnectionException' exception.
 * <p>
 * 01/28/2016 - Richard R. Kantimahanthi
 * * Added logic to map mime types for openxml office documents before archiving them to OnBase.
 * <p>
 * 03/24/2016 - Daniel Q. Yu
 * * Added Batch process link async document for async archive.
 * <p>
 * 05/23/2016 - Anirudh Mohan
 * * set document's PendingDocUID to a GUID/UUID value. And assigned it to the asyncdocumentid keyword;for async archive
 * <p>
 * 10/31/2017 - Chris Hoffman
 * * Removing breaker code
 */

/**
 * Document archival application.
 */
class DocumentArchival {
  /**
   * Logger
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

  /**
   * Archive a document to OnBase.
   *
   * @param documentContents The document content input stream.
   * @param document         The document to be added to OnBase.
   * @param asyncFolder      The async folder if using async document upload.
   * @param asyncSize        The async size limit if using async document upload.
   * @return The newly added document id or 0 if using async upload.
   */
  public function archiveDocument(documentContents: InputStream, document: Document, asyncFolder: File, asyncSize: long): String {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentArchival.archiveDocument(" + documentContents + ", " + document + ")")
    }

    // Document content is not null, compare document size with AsyncDocumentSize to determine sync or async upload method.
    var baos = new ByteArrayOutputStream()
    var localFile: File = null
    if (asyncFolder != null) {
      localFile = new File(asyncFolder, (UUID.randomUUID() as String))
    }
    var useAsync = isDocumentSizeOverAsyncSize(documentContents, baos, localFile, asyncSize) //Writes file to disk if size is greater than the async threshold

    // Document has zero byte content.
    if (baos.size() == 0) {
      _logger.error("Calling DocumentArchival.archiveDocument for new document with zero byte document content.");
      throw new NullContentException("Calling DocumentArchival.archiveDocument for new document with zero byte document content.")
    }

    var docID: String

    if (useAsync) {
      //PENDING**
      //NOTE:pendingdocuid needs to be set back to null later so that isDocumentPending doesn't have true in it.
      document.PendingDocUID = UUID.randomUUID().toString();
      var service = ServicesManager.getArchiveDocumentAsync();
      service.archiveDocument(localFile, document)
      if (_logger.DebugEnabled) {
        _logger.debug("Document " + localFile + " has been saved to local folder and waiting for OnBase to pick up later.")
      }
      document.DateCreated = DateUtil.currentDate()
      document.DateModified = DateUtil.currentDate()
      // No OnBase document id, just return 0.
      docID = "0"
    } else {
      // Sychronized upload document to OnBase.
      var service = ServicesManager.getArchiveDocumentSync();
      docID = service.archiveDocument(baos.toByteArray(), document)
      if (docID != null) {
        document.DocUID = docID
        document.DMS = true
        document.DateCreated = DateUtil.currentDate()
        document.DateModified = DateUtil.currentDate()
        var archiveBuilder = CCModelBuilder.buildArchiveMessage(document)
        // Update keywords to OnBase for Sync Documents.
        var updateService = ServicesManager.getSubmitMessageWithResponse();
        var updateResponse = updateService.submitMessageWithResponse(archiveBuilder.Message.asUTFString())
        //throw exception if the submit message response doesnt match the docID from document archival
        if (updateResponse != docID) {
          document.DocUID = null
          var errorMessage = DisplayKey.get("Accelerator.OnBase.Messaging.STR_GW_AddingDocFailed")
          _logger.error(DisplayKey.get("Accelerator.OnBase.Messaging.STR_GW_KeywordUpdateFailedOnArchival", document.Claim.ClaimNumber, updateResponse))
          throw new DisplayableException(errorMessage)
        }

      } else {
        var errorMessage = DisplayKey.get("Accelerator.OnBase.Messaging.STR_GW_AddingDocFailed")
        _logger.error(DisplayKey.get("Accelerator.OnBase.Messaging.STR_GW_MissingDocumentID"))
        throw new DisplayableException(errorMessage)
      }
    }

    return docID
  }

  /**
   * Is the document size larger than AsyncDocumentSize?
   * <p>
   * If true, then document content saved to localFile.
   * If false, then document content read into output byte array for synchronized upload.
   *
   * @param input     The document content input stream.
   * @param output    The document content in byte array output stream.
   * @param localFile The document local file if use async upload.
   * @param asyncSize The async size limit if using async document upload.
   * @return True if document size is over async size limit.
   */
  private function isDocumentSizeOverAsyncSize(input: InputStream, output: ByteArrayOutputStream, localFile: File, asyncSize: long): boolean {
    // Read document content to byte array but no more than AsyncDocumentSize
    var total = 0 as long
    try {
      var buf = new byte[1024]
      // Check if the Async is enabled. If not then read the input byte stream and return false.
      if (localFile != null) {
        while (total <= asyncSize) {
          var count = input.read(buf)
          if (count < 0) {
            break
          }
          output.write(buf, 0, count)
          total += count
        }
      } else {
        while (true) {
          var count = input.read(buf)
          if (count < 0) {
            break
          }
          output.write(buf, 0, count)
          total += count
        }
        output.flush();
        return false
      }
      output.flush();
    } catch (ex: Exception) {
      _logger.error("Unable to read document content to byte array or write document content to local file.", ex)
      throw new InvalidContentException("Unable to read document content to byte array or write document content to local file.")
    }
    if (_logger.DebugEnabled) {
      _logger.debug("Total document byte read : " + total + " and current async size is :" + asyncSize)
    }
    // All document content has been read into the byte array and smaller than AsyncDocumentSize, so return false.
    if (total <= asyncSize) {
      return false
    }

    // Save document content to local folder then return true.
    var fileOutputStream = null as BufferedOutputStream
    try {
      fileOutputStream = new BufferedOutputStream(new FileOutputStream(localFile))
      var buf = new byte[1024]
      // Write out previous read document content.
      if (total > 0) {
        fileOutputStream.write(output.toByteArray())
      }
      // Write out anything left in inputstream.
      while (true) {
        var count = input.read(buf)
        if (count < 0) {
          break
        }
        fileOutputStream.write(buf, 0, count)
      }
      fileOutputStream.close()
      return true
    } catch (ex: Exception) {
      _logger.error("Unable to write document content to local file " + localFile, ex)
      throw new WriteFailedException("Unable to write document content to local file " + localFile)
    } finally {
      // Close fileOutputStream again when exception thrown.
      try {
        if (fileOutputStream != null) {
          fileOutputStream.close()
        }
      } catch (ex: Exception) {
      }
    }
  }
}
