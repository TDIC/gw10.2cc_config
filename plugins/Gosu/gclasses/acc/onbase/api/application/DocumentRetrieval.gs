package acc.onbase.api.application

uses acc.onbase.api.security.SecurityManager
uses acc.onbase.api.service.ServicesManager
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.configuration.OnBaseClientType
uses acc.onbase.configuration.OnBaseWebClientType
uses acc.onbase.util.AePopUtils
uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey
uses gw.document.DocumentContentsInfo

uses java.io.InputStream

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 01/19/2015 - Daniel Q. Yu
 * * Initial implementation, refactored from OnBaseDocumentContentSource.gs
 * <p>
 * 4/1/2015 - mfowler
 * added AEPOPutil refactoring
 * <p>
 * 01/27/2017 - Daniel Q. Yu
 * * Added optional check sum to docpop URL.
 * <p>
 * 06/02/2017 - Tori Brenneison
 * * Changed location of OnBase client settings to ViewDocumentsUtil
 * <p>
 * 06/15/2017 - Chris Hoffman
 * * Added refactored methods from DocumentContentSource for viewing Contact Documents
 */

/**
 * Document retrieval application.
 */
class DocumentRetrieval {
  /** Logger */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

  /**
   * Get document content from OnBase.
   *
   * @param document The OnBase document.
   */
  public function getDocumentContent(document: Document): InputStream {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentRetrieval.getDocumentContent(" + document.DocUID + ")")
    }
    var service = ServicesManager.getDocumentContent()
    return service.getDocumentContent(document)
  }

  /**
   * Get document retrieval Unity URL.
   *
   * @param docId The document id.
   *
   * @return The encoded Unity URL for this document.
   */
  public function getDocumentUnityURL(docId: String): String {
    return AePopUtils.generateRetrievalUrl(docId)
  }

  /**
   * Get document retrieval web URL.
   *
   * @param docId The document id.
   * @param webClientType The web client type.
   *
   * @return The encoded web URL for this document.
   */
  public function getDocumentWebURL(docId: String, webClientType: OnBaseWebClientType): String {
    var docpopURL = OnBaseConfigurationFactory.Instance.PopURL + "/docpop/docpop.aspx?clientType=" + webClientType + "&docid=" + docId
    if (OnBaseConfigurationFactory.Instance.EnableDocPopURLCheckSum) {
      var hash = SecurityManager.computeDocIdCheckSum(docId)
      docpopURL = docpopURL + "&chksum=" + hash
    }
    return docpopURL
  }

  /**
   * Display document in OnBase Unity/Web client.
   *
   * @param docUID                The docUID of the document to be displayed.
   * @return The DocumentContentsInfo object for this document.
   */
   function getDocumentContentsInfo(docUID: String, includeDocumentContents: Boolean): DocumentContentsInfo {
    return getDocumentContentsInformation(docUID, includeDocumentContents, OnBaseConfigurationFactory.Instance.ClientType, OnBaseConfigurationFactory.Instance.WebClientType)
  }

  /**
   * Open document in Unity or Web client.
   *
   * @param docUID                  The docUID of the document to be opened.
   * @param includeDocumentContents If true then include document content. Currently not being used.
   * @param clientType              The client type which the document to be opened in.
   * @param webClientType           The web client type if using web client to open document.
   * @return The DocumentContentInfo which contains the document URL.
   */
  private function getDocumentContentsInformation(docUID: String, includeDocumentContents: boolean, clientType: OnBaseClientType, webClientType: OnBaseWebClientType): DocumentContentsInfo {
    var js = null as String
    var contents = null as String
    //dci's hidden frame is false by default.
    if (clientType == OnBaseClientType.Unity) {
      var uri = getDocumentUnityURL(docUID)
      contents = "<html><head><title>" + DisplayKey.get("Accelerator.OnBase.STR_GW_OpeningUnityClient_Title") + "</title></head><body><h1>" + DisplayKey.get("Accelerator.OnBase.STR_GW_OpeningUnityClient_Launch") + "</h1><h2>" + DisplayKey.get("Accelerator.OnBase.STR_GW_OpeningUnityClient_Close") + "</h2><iframe src ='" + uri + "' style=\"width:0;height:0;border:0;border:none;\"></iframe></body></html>"
    } else {
      var uri = getDocumentWebURL(docUID, webClientType)
      js = "document.location.href='" + uri + "';"
      contents = "<html><head><script>" + js + "</script></head></html>"
    }

    var dci = new DocumentContentsInfo(DocumentContentsInfo.ContentResponseType.DOCUMENT_CONTENTS, contents, "text/html")
    return dci
  }
}
