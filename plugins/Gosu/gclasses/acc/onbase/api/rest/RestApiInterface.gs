package acc.onbase.api.rest

uses acc.onbase.api.sharebaseconfig.ShareBaseInterface

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 *
 * Interface to call different API's like ShareBase
 */
interface RestApiInterface {

  public property get ShareBaseAPI(): ShareBaseInterface

  // this might contain other API Interfaces in future like LiveGenic
}