package acc.onbase.api.service

uses acc.onbase.api.service.interfaces.ArchiveDocumentAsyncInterface
uses acc.onbase.api.service.interfaces.ArchiveDocumentSyncInterface
uses acc.onbase.api.service.interfaces.DocumentContentInterface
uses acc.onbase.api.service.interfaces.ServicesManagerInterface
uses acc.onbase.api.service.interfaces.SubmitMessageInterface
uses acc.onbase.api.service.interfaces.SubmitMessageWithResponseInterface

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * <p>
 * 01/14/2015 - Daniel Q. Yu
 * * Initial implementation.
 * <p>
 * 01/16/2015 - Richard R. Kantimahanthi
 * * Implemented the rest of the service.
 * <p>
 * 02/02/2015 -  J. Walker
 * * Added Breaker pattern for fault responsiveness
 * <p>
 * 10/31/2017 - Chris Hoffman
 * * Removing breaker code
 * <p>
 * 02/20/2018 - Mike Sloan
 * * Removed all references to Document Composition, Get Document Info, and Script Dispatcher. Set default 'Get Document Content' method to use GetDoc service.
 */

/**
 * The manager class for all Guidewire API service.
 */
class ServicesManager{
  public static var _instance: ServicesManagerInterface as Instance = new ServicesManagerDefault()

  /** Asnyc archive document service. */
  public static function getArchiveDocumentAsync():ArchiveDocumentAsyncInterface {return Instance.ArchiveDocumentAsync}

  /** Sync archive document service. */
  public static function getArchiveDocumentSync(): ArchiveDocumentSyncInterface {return Instance.ArchiveDocumentSync}

  /** Get document content service. */
  public static function getDocumentContent() : DocumentContentInterface {
    return Instance.DocumentContent
  }

  /** Submit message service. */
  public static function getSubmitMessage() : SubmitMessageInterface {
    return Instance.SubmitMessage
  }

  /** Submit message with response service. */
  public static function getSubmitMessageWithResponse() : SubmitMessageWithResponseInterface {return Instance.SubmitMessageWithResponse}

}
