package acc.onbase.api.service.interfaces

uses gw.xml.XmlElement

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 *
 * Interface to submit an outgoing message to OnBase and get back a response
 */
interface SubmitMessageWithResponseInterface {

  /**
   * Submit a message to archive to OnBase
   *
   * @param payload string payload of message
   * @return the document handle or error message from onbase
   */
  function submitMessageWithResponse(payload : String) : String

  /**
   * Submit a message to archive to OnBase
   *
   * @param payload XML message
   * @return the document handle or error message from onbase
   */
  function submitMessageWithResponse(payload : XmlElement) : String

}