package acc.onbase.api.service.implementations.wsp.util

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 08/09/2016 - Daniel Q. Yu
 * * Switch between EISClientWithConfig and EISClientWithHeaders for WSP security levels.
 */

/**
 * Switch between EISClientWithConfig and EISClientWithHeaders for WSP security levels.
 */
class WSPUtil {
  /**
   * Get the WSP service object (Uncomment the proper WSP service)
   */
  //  public static function getWSPService(): acc.onbase.wsc.onbasewspwsc.soapservice.EISClientWithConfig {
  //    return new acc.onbase.wsc.onbasewspwsc.soapservice.EISClientWithConfig()
  //  }
  public static function getWSPService(): acc.onbase.wsc.onbasewspwsc.soapservice.EISClientWithHeaders {
    return new acc.onbase.wsc.onbasewspwsc.soapservice.EISClientWithHeaders()
  }
}