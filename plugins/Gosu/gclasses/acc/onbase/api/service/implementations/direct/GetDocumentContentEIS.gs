package acc.onbase.api.service.implementations.direct

uses acc.onbase.api.exception.ServiceErrorCodeException
uses acc.onbase.api.exception.ServicesTierException
uses acc.onbase.api.exception.ServicesTierServerConnectionException
uses acc.onbase.api.exception.ServicesTierServerErrorException
uses acc.onbase.api.service.interfaces.DocumentContentInterface
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.util.LoggerFactory
uses acc.onbase.wsc.onbasegetdocwsc.getdoc.GetDoc
uses acc.onbase.wsc.onbasegetdocwsc.getdoc.elements.FileDownload
uses acc.onbase.wsc.onbasegetdocwsc.getdoc.elements.FileRequest
uses gw.xml.ws.AsyncResponse
uses gw.xml.ws.WebServiceException
uses gw.xml.ws.WsdlFault
uses gw.xsd.w3c.soap11_envelope.Envelope

uses javax.crypto.Mac
uses javax.crypto.spec.SecretKeySpec
uses javax.xml.namespace.QName
uses java.io.InputStream

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 01/14/2015 - Daniel Q. Yu
 * * Initial implementation.
 * <p>
 * 01/27/2015 - Clayton Sandham
 * * Initial error handling implementation.
 * <p>
 * 01/30/2015 - Daniel Q. Yu
 * * Added stats tracking.
 * <p>
 * 02/12/2015 - Richard R. Kantimahanthi
 * * modified code to throw a 'ServicesTierServerConnectionException' in case of a HTTP 500 response from the service.
 */

/**
 * Implementation to call OnBase GetDoc service using EIS.
 */
class GetDocumentContentEIS implements DocumentContentInterface {
  /**
   * Logger.
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  /**
   * Get document content from OnBase. The content can be retrieved from document.FileContent input stream after this call.
   *
   * @param document The OnBase document.
   */
  override function getDocumentContent(document: Document): InputStream {
    if (_logger.isDebugEnabled()) {
      _logger.debug("Start executing GetDocumentContentEIS.getDocumentContent() using EIS service.")
    }
    var inStream: InputStream = null
    var key = OnBaseConfigurationFactory.Instance.GetDocChecksumKey
    //build the hash code.
    var secretKey = new SecretKeySpec(key.getBytes(), "HmacSHA256")
    var mac = Mac.getInstance("HmacSHA256")
    mac.init(secretKey)
    var hmacData = mac.doFinal(("" + document.DocUID).getBytes())
    var hash = gw.util.Base64Util.encode(hmacData)
    //instantiate the streaming service.
    var streamingService = new GetDoc()
    var fileRequest = new FileRequest()
    fileRequest.DocID = document.DocUID
    fileRequest.Hash = hash
    //send request to the streaming service.
    var response: AsyncResponse<FileDownload, Envelope>
    try {
      response = streamingService.async_GetData(fileRequest)
      var qn = new QName("http://tempuri.org/", "Status");
      if (response.ResponseEnvelope.Header.getChild(qn).$Text.containsIgnoreCase("Document retrieval failed")) {
        throw new ServiceErrorCodeException("Failure to retrieve document contents from OnBase")
      }
    } catch (ex: WebServiceException) {
      _logger.error("Exception occured in GetDocumentContentEIS.getDocumentContent(): " + ex.Message)
      if (ex.Message.contains("HTTP Response Code 500")) {
        throw new ServicesTierServerConnectionException("Server error while attempting to retrieve Document contents from OnBase", ex)
      } else {
        throw new ServicesTierException("Failure to retrieve document contents from OnBase", ex)
      }
    } catch (ex2: WsdlFault) {
      _logger.error("Exception occured in GetDocumentContentEIS.getDocumentContent(): " + ex2.Message)
      throw new ServicesTierServerErrorException("WSDL Fault while attempting to retrieve Document contents from OnBase", ex2)
    }
    var qn = new QName("http://tempuri.org/", "Status");
    if (response.ResponseEnvelope.Header.getChild(qn).$Text.equalsIgnoreCase("Success")) {
      inStream = response.get().FileContents.InputStream
      if (inStream == null) {
        _logger.error("No streaming content available for the document selected docID: '" + document.DocUID + "'")
        throw new ServicesTierException("No streaming content available for the document selected docID: '" + document.DocUID + "'")
      }

    } else {
      _logger.error("Could not download the file stream from OnBase - " + response.ResponseEnvelope.Header.getChild(qn).$Text)
      throw new ServicesTierException("Could not download the file stream from OnBase - " + response.ResponseEnvelope.Header.getChild(qn).$Text)
    }
    if (_logger.isDebugEnabled()) {
      _logger.debug("Finish executing GetDocumentContentEIS.getDocumentContent() using EIS service.")
    }
    return inStream
  }

}
