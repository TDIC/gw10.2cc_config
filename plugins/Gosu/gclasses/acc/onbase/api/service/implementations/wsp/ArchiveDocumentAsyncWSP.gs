package acc.onbase.api.service.implementations.wsp

uses acc.onbase.api.service.interfaces.ArchiveDocumentAsyncInterface
uses acc.onbase.api.si.template.AsyncUploadDip
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.util.LoggerFactory
uses com.tdic.util.misc.FtpUtil

uses java.io.File
uses java.io.FileNotFoundException
uses java.io.FileOutputStream
uses java.io.IOException
uses java.nio.file.Files
uses java.nio.file.Paths
uses java.util.zip.ZipEntry
uses java.util.zip.ZipOutputStream

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 02/10/2015 - dlittleton
 * * Initial implementation.
 */

/**
 * Implementation of the ArchiveDocumentASync interface that writes the
 * required DIP file locally.
 * <p>
 * Note that this technically doesn't use WSP at all, but it exists in
 * this package as it is expected to be used in conjunction with WSP.
 * <p>
 * Makes use of a gosu template to format the DIP file.
 *
 * @see AsyncUploadDip
 */
class ArchiveDocumentAsyncWSP implements ArchiveDocumentAsyncInterface {

  // File extension used for the self describing dip file.
  private static final var DIP_EXTENSION = ".dip"

  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  /**
   * Archive document asynchronously.
   *
   * @param documentFile The document local file.
   * @param document     The entity.document object.
   */
  public override function archiveDocument(documentFile: File, document: Document) {
    _logger.debug("Start executing archiveDocument Async using WSP service.")

    var dipFile = new File(documentFile.ParentFile, documentFile.Name + DIP_EXTENSION)

    var source = OnBaseConfigurationFactory.Instance.SourceCenterName

    using (var writer = new java.io.OutputStreamWriter(new java.io.FileOutputStream(dipFile))) {
      AsyncUploadDip.render(writer, document, documentFile.Name, source)
    }
    //Zip document file,.dip file and uplaod to FTP server location.
    zipFilesAndFtpUpload({documentFile.Path, dipFile.Path})

    _logger.debug("Finished executing archiveDocument Async using WSP service.")

  }

  /**
   * Zip multiple files and upload to FTP server.
   *
   * @param filePaths
   */
  private function zipFilesAndFtpUpload(filePaths : String[]) {
    try {
      var asyncFolder = OnBaseConfigurationFactory.Instance.AsyncDocumentFolder
      var zipDir = new File(asyncFolder.Path + "\\zip")
      if(!zipDir.exists())
        zipDir.mkdir()
      var zipFile = new File(zipDir + "\\"+ Paths.get(filePaths[0]).getFileName().toString()+".zip")

      var fos = new FileOutputStream(zipFile)
      var zos = new ZipOutputStream(fos)

      for (aFile in filePaths) {
        zos.putNextEntry(new ZipEntry(new File(aFile).getName()))

        var bytes = Files.readAllBytes(Paths.get(aFile))
        zos.write(bytes, 0, bytes.length);
        zos.closeEntry()
      }

      FtpUtil.uploadFile("onbase", zipFile)
      zos.close()
    }  catch (ex : FileNotFoundException) {
      _logger.error("Document upload file does not exist: " + ex);
    } catch (ex : IOException) {
      _logger.error("Unable to write upload file to zip folder: " + ex);
    }
  }

}