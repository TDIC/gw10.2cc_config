package acc.onbase.api.service.implementations.wsp.util

uses acc.onbase.api.security.SecurePropertiesManager
uses gw.xml.XmlElement
uses gw.xml.ws.WsdlConfig

uses javax.xml.namespace.QName
uses javax.xml.soap.MessageFactory
uses javax.xml.transform.OutputKeys
uses javax.xml.transform.TransformerFactory
uses javax.xml.transform.dom.DOMSource
uses javax.xml.transform.stream.StreamResult
uses java.io.StringWriter

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 08/01/2016 - Daniel Q. Yu
 * * Refactored from Justin Walker's test implementation.
 * <p>
 * 09/19/2016 - Daniel Q. Yu
 * * Removed dependency of apache library WSS4J
 */

/**
 * Configuration Provider to add user credential in WSP requests.
 */
class AuthenticationHandler implements gw.xml.ws.IWsiWebserviceConfigurationProvider {
  public static final var WSSE_PREFIX: String = "wsse"
  public static final var WSSE_NS: String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
  public static final var WSU_NS: String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
  public static final var PASSWORD_TYPE: String = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"

  construct() {
  }

  @Param("serviceName", "")
  @Param("portName", "")
  @Param("config", "")
  override public function configure(serviceName: QName, portName: QName, config: WsdlConfig): void {
    var soapMsg = MessageFactory.newInstance().createMessage()
    var soapPart = soapMsg.getSOAPPart().getEnvelope()

    var securityElem = soapPart.addChildElement("Security", WSSE_PREFIX, WSSE_NS)
    var tokenElem = securityElem.addChildElement("UsernameToken", WSSE_PREFIX)
    tokenElem.addNamespaceDeclaration("wsu", WSU_NS)
    tokenElem.addAttribute(QName.valueOf("wsu:Id"), "UsernameToken-" + UUID.randomUUID())
    var userElem = tokenElem.addChildElement("Username", WSSE_PREFIX)
    userElem.addTextNode(SecurePropertiesManager.getUsernameForWSP())
    var pwdElem = tokenElem.addChildElement("Password", WSSE_PREFIX)
    pwdElem.addTextNode(SecurePropertiesManager.getPasswordForWSP())
    pwdElem.addAttribute(QName.valueOf("Type"), PASSWORD_TYPE)


    var transformer = TransformerFactory.newInstance().newTransformer()

    var buffer = new StringWriter()
    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes")
    transformer.transform(new DOMSource(securityElem), new StreamResult(buffer))

    config.RequestSoapHeaders.add(XmlElement.parse(buffer.toString()))
  }
}
