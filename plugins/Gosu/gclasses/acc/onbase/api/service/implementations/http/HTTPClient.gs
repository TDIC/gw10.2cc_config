package acc.onbase.api.service.implementations.http

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */

uses acc.onbase.api.exception.ShareBaseAPIException
uses acc.onbase.util.LoggerFactory
uses org.json.simple.JSONObject
uses org.json.simple.parser.JSONParser

uses java.io.DataOutputStream
uses java.io.IOException
uses java.io.InputStreamReader
uses java.net.HttpURLConnection

public class HTTPClient implements HTTPClientInterface {

  private var _httpURLConnection: HttpURLConnection as HTTPURLConnection

  construct(httpURLConnection: HttpURLConnection) {
    this._httpURLConnection = httpURLConnection
  }

  var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

  public function setPostBodyParameters(bodyParameters: HashMap<String, Object>) {
    var dataOutputStream: DataOutputStream
    try {
      var jsonObject = new JSONObject(bodyParameters)
      dataOutputStream = new DataOutputStream(HTTPURLConnection.OutputStream)
      dataOutputStream.writeBytes(jsonObject.toString())
    } catch (ex: Exception) {
      _logger.error("Error in sending the post request", ex)
      throw new RuntimeException("Error in sending the post request", ex)
    } finally {
      dataOutputStream.flush()
      dataOutputStream.close()
    }
  }

  public function verifyStatusCode() {
    var statusCode = _httpURLConnection.ResponseCode
    if (!statusCodeIsSuccessful(statusCode)) {
      var errorResponse = HTTPURLConnection.ErrorStream.TextContent
      _logger.error("Failed : HTTP error code : " + statusCode + " " + errorResponse)
      throw new ShareBaseAPIException(statusCode, errorResponse)
    }
  }

  public function readResponse(): JSONObject {
    var inputStream: InputStreamReader
    var response: JSONObject
    try {
      inputStream = new InputStreamReader((HTTPURLConnection.InputStream))
      response = new JSONParser().parse(inputStream) as JSONObject
    } catch (ex: IOException) {
      _logger.error("Failed to read from the input stream", ex)
      throw new RuntimeException("Failed to read from the input stream", ex)
    } finally {
      inputStream.close()
    }
    return response
  }

  public function setRequestProperty(key: String, value: String) {
    _httpURLConnection.setRequestProperty(key, value)
  }

  public function disconnect() {
    _httpURLConnection.disconnect()
  }

  private function statusCodeIsSuccessful(statusCode: int): Boolean {
    return statusCode >= 200 and statusCode < 300
  }

}