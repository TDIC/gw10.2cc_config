package acc.onbase.api.service.implementations.http

uses org.json.simple.JSONObject

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */

/**
 * Interface for http utility methods
 */
interface HTTPClientInterface {

  public function setPostBodyParameters(bodyParameters: HashMap<String, Object>)

  public function verifyStatusCode()

  public function readResponse() : JSONObject

  public function setRequestProperty(key:String, value:String)

  public function disconnect()

}