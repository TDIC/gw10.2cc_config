package acc.onbase.api.security

uses gw.api.database.Query

class SecurityDemo {
  public static function demo() {
    print("======================")
    //1st claim
    printClaimSecurity(Query.make(Claim).select().first())
    print("======================")
    //2nd claim
    printClaimSecurity(Query.make(Claim).select().toList()[1])
  }

  public static function printClaimSecurity(c: Claim) {
    print("Claim Number: " + c.ClaimNumber)
    print("Claim Security Level: " + c.PermissionRequired)
    print("Claim Access Control List:" + java.util.Arrays.deepToString(c.Access))
    print("Claim Security Roles: " + SecurityManager.getClaimSecurityACL(c, 999999)[0])
  }
}