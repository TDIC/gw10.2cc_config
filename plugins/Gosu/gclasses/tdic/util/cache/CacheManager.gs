package tdic.util.cache

uses com.tdic.plugins.hpexstream.core.util.ExstreamMapParam
uses gw.api.system.server.ServerUtil
uses gw.api.util.ConfigAccess
uses org.apache.commons.collections.MultiHashMap
uses com.tdic.util.database.DatabaseManager
uses java.io.FileInputStream
uses java.io.IOException
uses java.lang.Exception
uses java.sql.Connection
uses java.sql.ResultSet
uses java.sql.SQLException
uses java.util.HashMap
uses java.util.Properties
uses com.tdic.util.misc.EmailUtil
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory

/**
 * User: skiriaki
 * Date: 8/20/14
 *
 * Cache manager to load the various integration db tables in memory
 * Code is common to all suite applications
 */
class CacheManager {
  private static var _logger = LoggerFactory.getLogger("TDIC_INTEGRATION")
  private static var env: String
  public static var PropertiesCache: HashMap<String, String>
  public static var WebServiceEndpointCache: HashMap<String, WebServiceParam>
  public static var AppDatabaseCache: HashMap<String, DatabaseParam>
  public static var ExternalCodeCache: HashMap<String, ExternalCodeParam>
  public static var ExstreamPayloadCache: MultiHashMap
  private static var Email_Sub = "Failure: Caching Integration properties failed for ${ServerUtil.getProduct().ProductName} on ${ServerUtil.getEnv()}-${ServerUtil.getServerId()}"
  /*
   * Called at server startup or whenever the cache needs to be refreshed
   * The cache consists of 3 matrix:
   * - properties (name value pairs - including the GWINT DB URL)
   * - web services' endpoint
   * - application databases info
   */

  static public function loadCache() {
    _logger.debug("CacheManager#loadCache() - Entering")

    var _prop = new Properties()
    var _propSso = new Properties()
    var _con: Connection
    var _input: FileInputStream
    var _inputSso: FileInputStream

    // init the cache
    PropertiesCache = new HashMap<String, String>()
    WebServiceEndpointCache = new HashMap<String, WebServiceParam>()
    AppDatabaseCache = new HashMap<String, DatabaseParam>()
    ExternalCodeCache = new HashMap<String, ExternalCodeParam>()
    ExstreamPayloadCache = new MultiHashMap()

    env = ServerUtil.getEnv()?.toLowerCase()
    if (env == null) {
      _logger.info("CacheManager#loadCache() - Could not get env")
      return
    } else {
      PropertiesCache.put("env", env)
      _logger.info("CacheManager#loadCache() - Using env=" + env)
    }

    var _app = ServerUtil.getProduct().ProductName
    if (_app == null) {
      _logger.info("CacheManager#loadCache() - Could not get app")
      return
    } else {
      PropertiesCache.put("app", _app)
      _logger.info("CacheManager#loadCache() - Using app=" + _app)
    }

    try {
      // load the property file IntegrationDbUrl & retrieves the GWINT DB URL from it base on the environment value
      if(env.equalsIgnoreCase("PERF") or env.equalsIgnoreCase("PROD")) {
        _input = new FileInputStream(ConfigAccess.getConfigFile("../../../../../dbprops/IntegrationDbUrl.properties").Path)
      }
      else {
        _input = new FileInputStream(ConfigAccess.getConfigFile("/gsrc/tdic/util/cache/IntegrationDbUrl.properties").Path)
      }
      _prop.load(_input)
      var _envIntDbUrl = _prop.getProperty(env)

      // Put the GWINT DB URL in PropertiesCache using key <env>.IntDBURL
      PropertiesCache.put(env + ".IntDBURL", _envIntDbUrl)

      try {
        // Get a connection from DatabaseManager using the GWINT DB URL
        _con = DatabaseManager.getConnection(_envIntDbUrl)
      } catch (e: Exception) {
        _logger.error("CacheManager#loadCache() - GWINT db is not there. Cache will not be loaded." +
            " To load the cache, create GWINT, load it with the data then load the cache from the custom batch",e)
        throw(e)
      }

      var _isSsoEnabled = false
      try{
        // load the property file SSO Properties & retrieves on/off switch on whether sso enabled or not.
        _inputSso = new FileInputStream(ConfigAccess.getConfigFile("../../../../../dbprops/sso.properties").Path)
        if(_inputSso!=null){
          _propSso.load(_inputSso)
          _isSsoEnabled = Coercions.makePBooleanFrom(_propSso.getProperty("isSsoEnabled"))
        }
        // Put the GWINT DB URL in PropertiesCache using key <env>.IntDBURL
        _logger.info("Is SSO Enabled:"+_isSsoEnabled)
        PropertiesCache.put("isSsoEnabled", _isSsoEnabled as String)
      } catch (e: Exception) {
        if("Local".equalsIgnoreCase(env)){
          _logger.warn("CacheManager#loadCache() - The SSO.properties not available in dbprops folder or unable to access. Please fix the error and restart the server")
        } else{
          _logger.warn("CacheManager#loadCache() - The SSO.properties not available in dbprops folder or unable to access. Please fix the error and restart the server"+e.StackTraceAsString)
        }
        PropertiesCache.put("isSsoEnabled", _isSsoEnabled as String)
        //return ---- Eating the exception here. If sso properties not found then by default SSO will be disabled.
      }

      // Issue sql statements (select) on that connection thru DatabaseManager to build the cache
      loadPropertiesCache(_con)
      _logger.info("loadPropertiesCache Completed: ${PropertiesCache?.Count}")
      loadWebServiceEndpointCache(_con)

      loadAppDatabaseCache(_con)

      loadExternalCodeCache(_con)

      loadExstreamPayloadCache(_con)

      _con.close()

      _logger.debug("CacheManager#loadCache() - Exiting")
    } catch (io: IOException) {
      _logger.error("CacheManager#loadCache() - IOException= " + io)
      sendEmail(io.StackTraceAsString)
      throw(io)
    } catch (sql: SQLException) {
      _logger.error("CacheManager#loadCache() - SQLException= " + sql)
      sendEmail(sql.StackTraceAsString)
      throw(sql)
    } catch (e: Exception) {
      _logger.error("CacheManager#loadCache() - Exception= " + e)
      sendEmail(e.StackTraceAsString)
      throw(e)
    } finally {
      try {
        if (_con != null) _con.close()
        if (_input != null) _input.close()
      } catch (fe: Exception) {
        _logger.error("CacheManager#loadCache() - finally Exception= " + fe)
        sendEmail(fe.StackTraceAsString)
        throw(fe)
      }
    }
  }

  /*
   * Load Web Service Endpoint Cache
   */

  static function loadWebServiceEndpointCache(connection: Connection) {
    _logger.debug("CacheManager#loadWebServiceEndpointCache(Connection) - Entering")
    var _sqlStmt: String
    var _resultSet: ResultSet

    _sqlStmt = "select WebService.Name, WebService.EndPoint, WebService.UserName, WebService.Password " +
        "from	WebService, Environment " +
        "where Environment.Name = '" + env + "' " +
        "and	WebService.EnvID = Environment.ID"
    _resultSet = DatabaseManager.executeQuery(connection, _sqlStmt)

    while (_resultSet.next()) {
      var _webServiceParam = new WebServiceParam()
      _webServiceParam.EndPoint = _resultSet.getString("instance")
      _webServiceParam.UserName = _resultSet.getString("userName")
      _webServiceParam.Password = _resultSet.getString("password")

      WebServiceEndpointCache.put(_resultSet.getString("name"), _webServiceParam)
    }

    _logger.debug("CacheManager#loadWebServiceEndpointCache(Connection) - Exiting")
  }

  /*
   * Load App Database Cache
   */

  static function loadAppDatabaseCache(connection: Connection) {
    _logger.debug("CacheManager#loadAppDatabaseCache(Connection) - Entering")

    var _sqlStmt: String
    var _resultSet: ResultSet

    _sqlStmt = "select distinct Application.Name, " +
        "AppDatabase.host, " +
        "AppDatabase.instance, " +
        "AppDatabase.port, " +
        "AppDatabase.dbName, " +
        "AppDatabase.userName, " +
        "AppDatabase.password " +
        "from	AppDatabase, Environment, Application " +
        "where Environment.Name  in ('" + env + "','all')" +
        "and	AppDatabase.AppID = Application.ID " +
        "and	AppDatabase.EnvID = Environment.ID"
    _resultSet = DatabaseManager.executeQuery(connection, _sqlStmt)

    while (_resultSet.next()) {
      var _databaseParam = new DatabaseParam()
      _databaseParam.Host = _resultSet.getString("host")
      _databaseParam.Instance = _resultSet.getString("instance")
      _databaseParam.Port = _resultSet.getString("port")
      _databaseParam.DbName = _resultSet.getString("dbName")
      _databaseParam.UserName = _resultSet.getString("userName")
      _databaseParam.Password = _resultSet.getString("password")

      AppDatabaseCache.put(_resultSet.getString("Name"), _databaseParam)
    }

    _logger.debug("CacheManager#loadAppDatabaseCache(Connection) - Exiting")
  }

  /*
   * Load External Code Cache
   */

  static function loadExternalCodeCache(connection: Connection) {
    _logger.debug("CacheManager#loadExternalCodeCache(Connection) - Entering")

    var _sqlStmt: String
    var _resultSet: ResultSet

    _sqlStmt = "select distinct ExternalCode.Code, " +
        "ExternalCode.SourceSystem, " +
        "ExternalCode.DisplayMessage, " +
        "ExternalCode.ActivityPattern, " +
        "ExternalCode.IsWarning, " +
        "ExternalCode.IsError, " +
        "ExternalCode.IsRetryable " +
        "from	ExternalCode"
    _resultSet = DatabaseManager.executeQuery(connection, _sqlStmt)

    while (_resultSet.next()) {
      var _externalCodeParam = new ExternalCodeParam()
      _externalCodeParam.SourceSystem = _resultSet.getString("SourceSystem")
      _externalCodeParam.DisplayMessage = _resultSet.getString("DisplayMessage")
      _externalCodeParam.ActivityPattern = _resultSet.getString("ActivityPattern")
      _externalCodeParam.IsWarning = Coercions.makePBooleanFrom(_resultSet.getString("IsWarning"))
      _externalCodeParam.IsError = Coercions.makePBooleanFrom(_resultSet.getString("IsError"))
      _externalCodeParam.IsRetryable = Coercions.makePBooleanFrom(_resultSet.getString("IsRetryable"))

      ExternalCodeCache.put(_resultSet.getString("Code"), _externalCodeParam)
    }

    _logger.debug("CacheManager#loadExternalCodeCache(Connection) - Exiting")
  }

  /*
   * Load Properties Cache
   */

  static function loadPropertiesCache(connection: Connection) {
    _logger.debug("CacheManager#loadPropertiesCache(Connection) - Entering")
    var _sqlStmt: String
    var _resultSet: ResultSet

    var _app = CacheManager.PropertiesCache.get("app")

    _sqlStmt = "select distinct Property.Name, Property.Value " +
        "from	Application, Property, Environment " +
        "where Environment.Name in ('" + env + "','all') " +
        "and Application.Name in ('" + _app + "','all') " +
        "and Property.AppID = Application.ID " +
        "and Property.EnvID = Environment.ID "

    _resultSet = DatabaseManager.executeQuery(connection, _sqlStmt)

    while (_resultSet.next()) {
      PropertiesCache.put(_resultSet.getString("Name"), _resultSet.getString("Value"))
    }

    _logger.debug("CacheManager#loadPropertiesCache(Connection) - Exiting")
  }

  static public function dbInsertProperty(name: String, value: String): boolean {
    var _sqlStmt = "insert into Property (EnvID, AppID, Name, Value) " +
        "select Environment.ID, Application.ID, '" + name + "','" + value + "' " +
        "from Environment, Application " +
        "where Environment.Name = 'all' and Application.Name = 'All'"
    return cuPropertyInDb(name, value, _sqlStmt)
  }

  static public function dbUpdateProperty(name: String, value: String): boolean {
    var _sqlStmt = "Update Property set Value='" + value + "'where Name='" + name + "'"
    return cuPropertyInDb(name, value, _sqlStmt)
  }

  static public function dbDeleteProperty(name: String): boolean {
    var _sqlStmt = "DELETE FROM Property where Name='" + name + "'"
    var propertyDeleted = cuPropertyInDb(name, null, _sqlStmt)
    PropertiesCache.remove(name)
    return propertyDeleted
  }

  static function cuPropertyInDb(name: String, value: String, sqlStmt: String): boolean {
    _logger.debug("CacheManager#persistProperty() - Entering")

    var _con: Connection

    if (PropertiesCache == null){
      _logger.debug("CacheManager#persistProperty() - Cache is not loaded, cannot persist '" + name + "';'" + value)
      return false
    }

    try {
      // Get the GWINT DB URL from PropertiesCache
      var _env = CacheManager.PropertiesCache.get("env")
      var _envIntDbUrl = CacheManager.PropertiesCache.get(_env + ".IntDBURL")

      try {
        // Get a connection from DatabaseManager using the GWINT DB URL
        _con = DatabaseManager.getConnection(_envIntDbUrl)
      } catch (e: Exception) {
        _logger.warn("CacheManager#persistProperty() - GWINT db error")
        return false
      }

      var _return = DatabaseManager.executeStmt(_con, sqlStmt)

      _con.close()
      _logger.debug("CacheManager#persistProperty() - Exiting")
      if (_return >= 1) {
        CacheManager.PropertiesCache.put(name, value)
        return true
      } else {
        return false
      }
    } catch (io: IOException) {
      _logger.error("CacheManager#persistProperty() - IOException= " + io)
      return false
    } catch (sql: SQLException) {
      _logger.error("CacheManager#persistProperty() - SQLException= " + sql)
      return false
    } catch (e: Exception) {
      _logger.error("CacheManager#persistProperty() - Exception= " + e)
      return false
    } finally {
      try {
        if (_con != null) _con.close()
      } catch (fe: Exception) {
        _logger.error("CacheManager#persistProperty() - finally Exception= " + fe)
      }
    }
  }

  /**
   * Load Exstream Payload Cache
   */
  static function loadExstreamPayloadCache(connection: Connection) {
    _logger.debug("CacheManager#loadExstreamPayloadCache(Connection) - Entering")
    var _sqlStmt: String
    var _resultSet: ResultSet

    var _app = CacheManager.PropertiesCache.get("app")

    _sqlStmt = "SELECT distinct Application.Name, " +
        "ExstreamPayload.Payload " +
        "FROM	ExstreamPayload, Application, Environment " +
        "WHERE Environment.Name  IN ('" + env + "','All')" +
        "AND Application.Name IN ('" + _app + "','All')" +
        "AND	ExstreamPayload.AppID = Application.ID " +
        "AND	ExstreamPayload.EnvID = Environment.ID"


    _resultSet = DatabaseManager.executeQuery(connection, _sqlStmt)
    var count = 0
    while (_resultSet.next()) {
      ExstreamPayloadCache.put(_resultSet.getString("Name"), _resultSet.getString("Payload"))
      count++
    }
    _logger.debug("CacheManager#loadExstreamPayloadCache(Connection) - Loaded ${count} Exstream Payloads")
    _logger.debug("CacheManager#loadExstreamPayloadCache(Connection) - Exiting")
  }

  /**
   * US669
   * 03/25/2016 SunnihithB
   *
   */
  @Returns("0 if not stored, 1 if sso enabled")
  static public function isSsoEnabled(): boolean {
    return Coercions.makePBooleanFrom(PropertiesCache.get("isSsoEnabled"))
  }


  /*
  Exstream Batch Store methods
   */

  /**
   * US669
   * 101/27/2015 Shane Murphy
   *
   */
  @Param("payload", "XML payload to store")
  @Returns("0 if not stored, 1 if stored successfully ")
  static public function dbInsertExstreamPayload(payload: String): boolean {
    _logger.debug("CacheManager#dbInsertExstreamPayload(String) - Entering")
    var _app = CacheManager.PropertiesCache.get("app")
    if (ExstreamPayloadCache == null){
      _logger.debug("CacheManager#persistExstreamPayload() - Cache is not loaded, cannot persist '" + _app + "';'" + payload)
      return false
    }
    var _sqlStmt = "insert into ExstreamPayload (EnvID, AppID, Payload) " +
        "select Environment.ID, Application.ID, '" + payload.replace("'", "''") + "' " +
        "from Environment, Application " +
        "where Environment.Name = '" + env + "' and Application.Name = '" + _app + "'"
    var _responseCode = persist(_sqlStmt)
    // Handle response
    if (_responseCode >= 1 && payload != null) {
      _logger.debug("CacheManager#persistExstreamPayload() - Exiting")
      CacheManager.ExstreamPayloadCache.put(_app, payload)
      return true
    } else {
      _logger.debug("CacheManager#persistExstreamPayload() - Failed")
      _logger.debug("CacheManager#persistExstreamPayload() - Exiting")
      return false
    }
  }

  /**
   * US669
   * 01/22/2015 Shane Murphy
   *
   * When we delete after the document batch sends, we delete everything for this app ID
   */
  @Returns("1 if delete successful")
  static public function dbDeleteExstreamPayloads(): boolean {
    _logger.debug("CacheManager#dbDeleteExstreamPayloads(String) - Entering")
    var _app = CacheManager.PropertiesCache.get("app")
    var _sqlStmt = "DELETE FROM ExstreamPayload where AppID= (select AppID from Application where Application.Name = '" + _app + "')"
    var propertyDeleted = persist(_sqlStmt)
    ExstreamPayloadCache.remove(ServerUtil.getProduct().ProductName)
    _logger.debug("CacheManager#dbDeleteExstreamPayloads(String) - Exiting")
    return propertyDeleted as boolean
  }

  /**
   * US669
   * 01/27/2015 Shane Murphy
   *
   * Persist state to the integration database
   */
  @Param("sqlStmt", "Statement to execute")
  @Returns("1 if stored/deleted successfully")
  static function persist(sqlStmt: String): int {
    _logger.debug("CacheManager#persistExstreamPayload() - Entering")
    var _app = CacheManager.PropertiesCache.get("app")
    var _con: Connection
    try {

      _con = getConnection()
      return DatabaseManager.executeStmt(_con, sqlStmt)

    } catch (io: IOException) {
      _logger.error("CacheManager#persistExstreamPayload() - IOException= " + io)
      return false as int
    } catch (sql: SQLException) {
      _logger.error("CacheManager#persistExstreamPayload() - SQLException= " + sql)
      return false as int
    } catch (e: Exception) {
      _logger.error("CacheManager#persistExstreamPayload() - Exception= " + e)
      return false as int
    } finally {
      try {
        if (_con != null){
          _con.close()
        }
      } catch (fe: Exception) {
        _logger.error("CacheManager#persistExstreamPayload() - finally Exception= " + fe)
      }
    }
  }

  /**
   * US669
   * 01/27/2015 Shane Murphy
   *
   * Utility method for getting a connection using the current env and DBURL in the CacheManager
   */
  @Returns ( "Connection" )
  static function getConnection(): Connection {
    _logger.trace("CacheManager#getConnection() - Entering")
    // Get the GWINT DB URL from PropertiesCache
    var _env = CacheManager.PropertiesCache.get("env")
    var _envIntDbUrl = CacheManager.PropertiesCache.get(_env + ".IntDBURL")
    var _con: Connection
    try {
      // Get a connection from DatabaseManager using the GWINT DB URL
      _con = DatabaseManager.getConnection(_envIntDbUrl)
    } catch (e: Exception) {
      _logger.warn("CacheManager#getConnection() - GWINT db error")
      return null
    }
    _logger.trace("CacheManager#getConnection() - Exiting")
    return _con
  }

  private static function sendEmail(info: String){
    var _DefaultToEmailIDs = CacheManager.PropertiesCache.get("DefaultToEmailIDs")
    EmailUtil.sendEmail(_DefaultToEmailIDs, Email_Sub, "Cache Manager is unable to load integration properties. Please fix the error and restart the server. \n ${info}")
  }
}

