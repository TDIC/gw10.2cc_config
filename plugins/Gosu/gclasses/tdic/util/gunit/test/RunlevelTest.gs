package tdic.util.gunit.test

uses gw.api.system.server.Runlevel
uses gw.testharness.RunLevel
uses gw.testharness.ServerTest
uses gw.testharness.TestBase
uses tdic.util.gunit.IRunlevelAccessor
uses tdic.util.gunit.TextTestRunner
uses tdic.util.gunit.test.runlevel.*
uses java.lang.Throwable

@ServerTest
@RunLevel(Runlevel.NONE)
class RunlevelTest extends TestBase {

  override function beforeMethod() {
    TestClassesRun.clearStaticState()
    RunlevelDefaultTest._testsInThisClassWereRun = false
  }

  override function afterMethod(possibleException : Throwable) {
    TestClassesRun.clearStaticState()
    RunlevelDefaultTest._testsInThisClassWereRun = false
  }

  function testCaptureWebServerStartupMessages() {
  }

  /**
   * <code>Runlevel.NONE</code> is the run level for Gosu Tester when Guidewire Studio is <b>not</b> connected to a web server.
   * Because this is the lowest possible run level, only "<code>@RunLevel(Runlevel.NONE)</code>" tests will be run.
   */
  function testRunlevelNONE() {
    var runner = new MockTextTestRunner(Runlevel.NONE)

    runner.runTestsInPackage(RunlevelNoneTest.Type.Namespace)

    assertEquals("[NONE]", TestClassesRun._runlevelsCalled.toString())
    assertFalse("Expecting that the tests in RunlevelDefaultTest class should NOT be run, as the default Runlevel is MULTIUSER.", RunlevelDefaultTest._testsInThisClassWereRun)
    assertEquals("PassingTests", 1, runner.TestReporter.PassingTests)
    assertEquals("FailingTests", 0, runner.TestReporter.FailingTests)
    assertEquals("TotalTests",   1, runner.TestReporter.TotalTests)
    assertEquals("TestClassesSkippedDueToRunlevel", 6, runner.TestReporter.TestClassesSkippedDueToRunlevel)
  }

  function testRunlevelGUIDEWIRE_STARTUP() {
    var runner = new MockTextTestRunner(Runlevel.GUIDEWIRE_STARTUP)

    runner.runTestsInPackage(RunlevelNoneTest.Type.Namespace)

    assertEquals("[NONE, GUIDEWIRE_STARTUP]", TestClassesRun._runlevelsCalled.toString())
    assertFalse("Expecting that the tests in RunlevelDefaultTest class should NOT be run, as the default Runlevel is MULTIUSER.", RunlevelDefaultTest._testsInThisClassWereRun)
    assertEquals("TotalTests", 2, runner.TestReporter.TotalTests)
    assertEquals("TestClassesSkippedDueToRunlevel", 5, runner.TestReporter.TestClassesSkippedDueToRunlevel)
  }

  function testRunlevelSHUTDOWN() {
    var runner = new MockTextTestRunner(Runlevel.SHUTDOWN)

    runner.runTestsInPackage(RunlevelNoneTest.Type.Namespace)

    assertEquals("[NONE, GUIDEWIRE_STARTUP, SHUTDOWN]", TestClassesRun._runlevelsCalled.toString())
    assertFalse("Expecting that the tests in RunlevelDefaultTest class should NOT be run, as the default Runlevel is MULTIUSER.", RunlevelDefaultTest._testsInThisClassWereRun)
    assertEquals("TotalTests", 3, runner.TestReporter.TotalTests)
    assertEquals("TestClassesSkippedDueToRunlevel", 4, runner.TestReporter.TestClassesSkippedDueToRunlevel)
  }

  function testRunlevelNODAEMONS() {
    var runner = new MockTextTestRunner(Runlevel.NODAEMONS)

    runner.runTestsInPackage(RunlevelNoneTest.Type.Namespace)

    assertEquals("[NONE, GUIDEWIRE_STARTUP, SHUTDOWN, NODAEMONS]", TestClassesRun._runlevelsCalled.toString())
    assertFalse("Expecting that the tests in RunlevelDefaultTest class should NOT be run, as the default Runlevel is MULTIUSER.", RunlevelDefaultTest._testsInThisClassWereRun)
    assertEquals("TotalTests", 4, runner.TestReporter.TotalTests)
    assertEquals("TestClassesSkippedDueToRunlevel", 3, runner.TestReporter.TestClassesSkippedDueToRunlevel)
  }

  function testRunlevelDAEMONS() {
    var runner = new MockTextTestRunner(Runlevel.DAEMONS)

    runner.runTestsInPackage(RunlevelNoneTest.Type.Namespace)

    assertEquals("[NONE, GUIDEWIRE_STARTUP, SHUTDOWN, NODAEMONS, DAEMONS]", TestClassesRun._runlevelsCalled.toString())
    assertFalse("Expecting that the tests in RunlevelDefaultTest class should NOT be run, as the default Runlevel is MULTIUSER.", RunlevelDefaultTest._testsInThisClassWereRun)
    assertEquals("TotalTests", 5, runner.TestReporter.TotalTests)
    assertEquals("TestClassesSkippedDueToRunlevel (MULTIUSER and RunlevelDefaultTest)", 2, runner.TestReporter.TestClassesSkippedDueToRunlevel)
  }

  /**
   * <code>Runlevel.MULTIUSER</code> is the run level for Gosu Tester when Guidewire Studio <b>is</b> connected to a web server.
   * Because this is the highest run level, all tests will be run.
   */
  function testRunlevelMULTIUSER() {
    var runner = new MockTextTestRunner(Runlevel.MULTIUSER)

    runner.runTestsInPackage(RunlevelNoneTest.Type.Namespace)

    assertEquals("[NONE, GUIDEWIRE_STARTUP, SHUTDOWN, NODAEMONS, DAEMONS, MULTIUSER]", TestClassesRun._runlevelsCalled.toString())
    assertTrue("Expecting that the tests in RunlevelDefaultTest class SHOULD run, as the default Runlevel is MULTIUSER -- which is the run level that this test simulates.",
        RunlevelDefaultTest._testsInThisClassWereRun)
    assertEquals("TotalTests (6 listed above + RunlevelDefaultTest)", 6 + 1, runner.TestReporter.TotalTests)
    assertEquals("TestClassesSkippedDueToRunlevel", 0, runner.TestReporter.TestClassesSkippedDueToRunlevel)
  }

  function testCaptureWebServerShutdownMessages() {
  }

  private static class MockTextTestRunner extends TextTestRunner {

    construct(simulatedServerRunlevel : Runlevel) {
      super(new MockRunLevelAccessor(simulatedServerRunlevel))
    }

  }

  private static class MockRunLevelAccessor implements IRunlevelAccessor {

    var _simulatedServerRunlevel : Runlevel

    construct(simulatedServerRunlevel : Runlevel) {
      _simulatedServerRunlevel = simulatedServerRunlevel
    }

    override function getCurrentServerRunlevel() : Runlevel {
      return _simulatedServerRunlevel
    }

  }

}
