package tdic.cc.integ.plugins.hpexstream.model.tdic_ccemploymentdatamodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement EmploymentDataEnhancement : tdic.cc.integ.plugins.hpexstream.model.tdic_ccemploymentdatamodel.EmploymentData {
  public static function create(object : entity.EmploymentData) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccemploymentdatamodel.EmploymentData {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccemploymentdatamodel.EmploymentData(object)
  }

  public static function create(object : entity.EmploymentData, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccemploymentdatamodel.EmploymentData {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccemploymentdatamodel.EmploymentData(object, options)
  }

}