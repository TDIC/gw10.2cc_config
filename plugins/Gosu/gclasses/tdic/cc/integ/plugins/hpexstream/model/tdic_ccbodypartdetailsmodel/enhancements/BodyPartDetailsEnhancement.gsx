package tdic.cc.integ.plugins.hpexstream.model.tdic_ccbodypartdetailsmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement BodyPartDetailsEnhancement : tdic.cc.integ.plugins.hpexstream.model.tdic_ccbodypartdetailsmodel.BodyPartDetails {
  public static function create(object : entity.BodyPartDetails) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccbodypartdetailsmodel.BodyPartDetails {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccbodypartdetailsmodel.BodyPartDetails(object)
  }

  public static function create(object : entity.BodyPartDetails, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccbodypartdetailsmodel.BodyPartDetails {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccbodypartdetailsmodel.BodyPartDetails(object, options)
  }

}