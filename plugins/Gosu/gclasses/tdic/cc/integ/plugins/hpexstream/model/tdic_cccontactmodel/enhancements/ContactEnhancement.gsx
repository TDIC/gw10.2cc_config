package tdic.cc.integ.plugins.hpexstream.model.tdic_cccontactmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ContactEnhancement : tdic.cc.integ.plugins.hpexstream.model.tdic_cccontactmodel.Contact {
  public static function create(object : entity.Contact) : tdic.cc.integ.plugins.hpexstream.model.tdic_cccontactmodel.Contact {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_cccontactmodel.Contact(object)
  }

  public static function create(object : entity.Contact, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.hpexstream.model.tdic_cccontactmodel.Contact {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_cccontactmodel.Contact(object, options)
  }

}