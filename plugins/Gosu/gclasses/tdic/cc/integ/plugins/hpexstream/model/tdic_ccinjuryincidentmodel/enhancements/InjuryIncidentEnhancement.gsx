package tdic.cc.integ.plugins.hpexstream.model.tdic_ccinjuryincidentmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement InjuryIncidentEnhancement : tdic.cc.integ.plugins.hpexstream.model.tdic_ccinjuryincidentmodel.InjuryIncident {
  public static function create(object : entity.InjuryIncident) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccinjuryincidentmodel.InjuryIncident {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccinjuryincidentmodel.InjuryIncident(object)
  }

  public static function create(object : entity.InjuryIncident, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccinjuryincidentmodel.InjuryIncident {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccinjuryincidentmodel.InjuryIncident(object, options)
  }

}