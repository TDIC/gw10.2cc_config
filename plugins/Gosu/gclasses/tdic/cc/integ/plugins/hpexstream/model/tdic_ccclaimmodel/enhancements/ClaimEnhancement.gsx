package tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ClaimEnhancement : tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimmodel.Claim {
  public static function create(object : entity.Claim) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimmodel.Claim {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimmodel.Claim(object)
  }

  public static function create(object : entity.Claim, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimmodel.Claim {
    return new tdic.cc.integ.plugins.hpexstream.model.tdic_ccclaimmodel.Claim(object, options)
  }

}