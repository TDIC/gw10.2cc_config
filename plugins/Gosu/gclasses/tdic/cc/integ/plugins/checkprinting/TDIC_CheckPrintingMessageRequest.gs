package tdic.cc.integ.plugins.checkprinting

uses gw.plugin.messaging.MessageRequest
uses org.slf4j.LoggerFactory
uses tdic.cc.integ.plugins.checkprinting.model.checkpaymentdtomodel.CheckPaymentDTO

/**
 * The Transport plugin is responsible to read message from the Check Printing Destination Queue
 * and update vendor number in check payment payload before send to transport plugin.
 * Created by RambabuN on 11/20/2019.
 */
class TDIC_CheckPrintingMessageRequest implements MessageRequest {

  static final var _logger = LoggerFactory.getLogger("TDIC_CheckPrinting")

  override function afterSend(message : Message) {

  }

  /**
   * Set VendorNumber on the message.
   * @param message
   * @return check payment payload.
   */
  override function beforeSend(message : Message) : String {
    _logger.debug("TDIC_CheckPrintingMessageRequest#beforeSend() - Entering")

    var checkPaymentDTO = CheckPaymentDTO.parse(message.Payload)

    if(message.MessageRoot typeis Check) {
      var vendorNumber = message.MessageRoot.FirstPayee.ClaimContact?.Contact.VendorNumber
      if(vendorNumber == null || !(vendorNumber.Empty)) {
        checkPaymentDTO.VendorNumber = vendorNumber
      }
    }
    _logger.debug("TDIC_CheckPrintingMessageRequest#beforeSend() - Exiting")
    return checkPaymentDTO.asUTFString()
  }

  override function shutdown() {

  }

  override function suspend() {

  }

  override function resume() {

  }

  override property set DestinationID(destinationID : int) {

  }
}