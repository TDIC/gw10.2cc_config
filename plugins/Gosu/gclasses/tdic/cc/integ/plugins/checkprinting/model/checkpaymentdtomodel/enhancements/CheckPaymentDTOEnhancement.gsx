package tdic.cc.integ.plugins.checkprinting.model.checkpaymentdtomodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement CheckPaymentDTOEnhancement : tdic.cc.integ.plugins.checkprinting.model.checkpaymentdtomodel.CheckPaymentDTO {
  public static function create(object : tdic.cc.integ.plugins.checkprinting.dto.CheckPaymentDTO) : tdic.cc.integ.plugins.checkprinting.model.checkpaymentdtomodel.CheckPaymentDTO {
    return new tdic.cc.integ.plugins.checkprinting.model.checkpaymentdtomodel.CheckPaymentDTO(object)
  }

  public static function create(object : tdic.cc.integ.plugins.checkprinting.dto.CheckPaymentDTO, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.checkprinting.model.checkpaymentdtomodel.CheckPaymentDTO {
    return new tdic.cc.integ.plugins.checkprinting.model.checkpaymentdtomodel.CheckPaymentDTO(object, options)
  }

}