package tdic.cc.integ.plugins.checkprinting.model.checkprintingfilemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement CheckPrintingFileEnhancement : tdic.cc.integ.plugins.checkprinting.model.checkprintingfilemodel.CheckPrintingFile {
  public static function create(object : tdic.cc.integ.plugins.checkprinting.dto.CheckPrintingFile) : tdic.cc.integ.plugins.checkprinting.model.checkprintingfilemodel.CheckPrintingFile {
    return new tdic.cc.integ.plugins.checkprinting.model.checkprintingfilemodel.CheckPrintingFile(object)
  }

  public static function create(object : tdic.cc.integ.plugins.checkprinting.dto.CheckPrintingFile, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.checkprinting.model.checkprintingfilemodel.CheckPrintingFile {
    return new tdic.cc.integ.plugins.checkprinting.model.checkprintingfilemodel.CheckPrintingFile(object, options)
  }

}