package tdic.cc.integ.plugins.sedgwick

uses gw.plugin.messaging.MessageRequest
uses gw.xml.XmlElement
uses tdic.cc.integ.plugins.sedgwick.helper.TDIC_VendorSpecificGXHelper
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: Praneethk
 * Date: 10/28/14
 * Time: 2:05 PM
 * This Class implements the functionality of feeding information to sedgwick.The request plugin's beforeSend() method
 * is utilized to transform the payload generated from gx mapping and write it to a flat file according to vendor spec.
 * This Class is called immediately before the message is sent. The output of the class is the destination of flat file
 * in TDIC Shared Folder.
 */
class TDIC_SedgwickMessageRequest implements MessageRequest {

  private var _logger = LoggerFactory.getLogger("TDIC_FEEDTOSEDGWICK")                        //Sedgwick Logger

  override function beforeSend(pMessage: Message): String {
    _logger.info("Entering-SedgwickMessageRequest#beforeSend()")
    if (pMessage.SenderRefID == null) {
      pMessage.SenderRefID = pMessage.PublicID
    }
    _logger.info("SenderRefID: " + pMessage.SenderRefID)
    _logger.info("pMessage.PublicID: "+ pMessage.PublicID )

   /*
    *Get sedgwick XML model
    */
    var payloadXml = XmlElement.parse(pMessage.Payload)             //Convert p1Payload String to XmlElement
    var v = new TDIC_VendorSpecificGXHelper ()                     //Object v for VendorspecificGXHelper
    var mappedXml = v.fieldMapping(payloadXml)                     //maps vendor specific codes to the generated payload
    _logger.info("Returning-SedgwickMessageRequest#beforeSend()")
    return mappedXml.asUTFString()
  }

  override function afterSend(pMessage: Message) {
    _logger.debug("Entering SedgwickMessageRequest#afterSend()")
    //Do nothing
    _logger.debug("Exiting SedgwickMessageRequest#afterSend()")
  }

  override function shutdown() {
    _logger.debug("Entering SedgwickMessageRequest#shutdown()")
    // Do Nothing
    _logger.debug("Leaving SedgwickMessageRequest#shutdown()")
  }

  override function suspend() {
    _logger.debug("Entering SedgwickMessageRequest#suspend()")
    // Do Nothing
    _logger.debug("Leaving SedgwickMessageRequest#suspend()")
  }

  override function resume() {
    _logger.debug("Entering SedgwickMessageRequest#resume()")
    // Do Nothing
    _logger.debug("Leaving SedgwickMessageRequest#resume()")
  }

  override property set DestinationID(destinationID : int) {

  }
}