package tdic.cc.integ.plugins.sedgwick

uses com.tdic.util.database.DatabaseUtil
uses gw.xml.XmlElement
uses org.slf4j.LoggerFactory
uses tdic.cc.integ.plugins.message.TDIC_MessagePlugin
uses java.sql.Connection
uses java.sql.PreparedStatement
uses java.text.SimpleDateFormat

/**
 * Created with IntelliJ IDEA.
 * User: Praneethk
 * Date: 10/24/14
 * Implementation of a Messaging Transport plugin to feed the details to Sedgwick.  This will write a message to the
 * integration database with the Claim Details, to be later retrieved and written to the flat file to
 * the Sedgwick.
 */
class TDIC_SedgwickMessageTransport extends  TDIC_MessagePlugin {
  private var _logger = LoggerFactory.getLogger("TDIC_FEEDTOSEDGWICK")             //Standard Logger

  override property set DestinationID(destinationID : int) {

  }

  /*
     * send method uses Gosu's Java file I/O capabilities to write the payload to a flat file.
     * The payload contains information about the evaluations for the vendor.
     */
  override function send(p0Message: Message, p1Payload: String) {
    _logger.trace("Entering sedgwickMessageTransport:send - Payload: " + p1Payload)
   /*
    * Check for duplicates
    */
    var history = gw.api.database.Query.make(MessageHistory).compare(MessageHistory#SenderRefID, Equals, p0Message.SenderRefID).select().AtMostOneRow
    if (history != null) {
      _logger.warn("Duplicate message found with SenderRefID: " + p0Message.SenderRefID)
      history = gw.transaction.Transaction.getCurrent().add(history)
      history.reportDuplicate()
      return
    }

    updateSedgwickFNOL(getClaimNumberFromXmlString(p1Payload),p1Payload)

    //Call batch process from here
    try{
      var sedgwickFNOLBatchSwamp = new gw.webservice.cc.MaintenanceToolsAPI ()
      sedgwickFNOLBatchSwamp.startBatchProcess("sedgwickfnol")
    }
    catch(e:Exception){
      _logger.error("Batch Cannot be executed" + e.LocalizedMessage)
    }

    p0Message.reportAck()
    _logger.info(" Acknowledged Message ID "+ p0Message.ID + " synchronously")    //logger debug change from info
    _logger.info("SedgwickMessageTransport#send(Message,String) - Exiting-Bye")
  }

  function getClaimNumberFromXmlString(xmlString:String):String{
    var xml = XmlElement.parse(xmlString)
    return xml.$Children.singleWhere( \ elt -> elt.$QName.LocalPart=="ClaimNumber").$Text
  }

  function updateSedgwickFNOL(claimNumber:String, payload:String){
    _logger.info("Updating to DB - Entering updateSedgwickFNOL")
    var sdfTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var dateUpdated = new java.util.Date()
    var datetime = sdfTimestamp.format(dateUpdated.toSQLDate())
    var _con: Connection = null
    var _prepStatement : PreparedStatement = null
    try{
      _con = DatabaseUtil.getIntegDBConnection()
      var _prepSqlStmt = "INSERT INTO FNOLSedgwick(claimNumber,payload,datetime) VALUES(?,?,?)"
      _prepStatement = _con.prepareStatement(_prepSqlStmt)
      _prepStatement.setString(1,claimNumber)
      _prepStatement.setString(2,payload)
      _prepStatement.setString(3,datetime)
      var _return =_prepStatement.executeUpdate()
      _con.commit()
    } finally{
      _con.close()
    }
    _logger.info("Updating to DB - Exiting updateSedgwickFNOL")
  }

  override function shutdown() {
    _logger.debug("Entering SedgwickMessageTransport#shutdown()")
    // Do Nothing
    _logger.debug("Leaving SedgwickMessageTransport#shutdown()")
  }

  override function resume() {
    _logger.debug("Entering SedgwickMessageTransport#resume()")
    // Do Nothing
    _logger.debug("Leaving SedgwickMessageTransport#resume()")
  }
}