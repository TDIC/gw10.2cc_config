package tdic.cc.integ.plugins.sedgwick.helper

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.database.DatabaseUtil
uses gw.api.util.StringUtil
uses java.sql.ResultSet
uses java.text.SimpleDateFormat

/*
 * Created with IntelliJ IDEA.
 * User: Praneethk
 * Date: 10/27/14
 * Time: 4:43 PM
 * Utility class to format the filename with month and counter as per the vendor specification of sedgwick.
 */

class TDIC_SedgwickDateUtil {

  static function getSedgwickCounter(): String {
    var dateFormatter = new SimpleDateFormat("MM")
    var _month = dateFormatter.format(new java.util.Date())
    var _monthForLastFile =   (getSedgwickData() != null)  ?  getSedgwickData()?.substring(0,2) : ""
    var _counterForLastFile =  (getSedgwickData() != null)  ?  getSedgwickData()?.substring(2) : ""
    var _counter:int
    if (_monthForLastFile.equals(_month)){
      _counter = Integer.parseInt(_counterForLastFile)
      _counter++
    } else {
      _counter=0
    }
    dateFormatter = new SimpleDateFormat("YYMM")
    var flatFileName = "TDI"+dateFormatter.format(new java.util.Date ())+"."+StringUtil.formatNumber(_counter,"000")

    return flatFileName
  }



  static function getSedgwickData() : String    {
    var _sqlStmt = "SELECT TOP 1 SedgwickMonth,SedgwickCounter FROM dbo.SedgwickHistory ORDER BY ID DESC"
    var _resultSet: ResultSet
    var resultString : String
    var _con = DatabaseUtil.getIntegDBConnection()
    _resultSet = DatabaseManager.executeQuery(_con, _sqlStmt)
    var count = 0
    while (_resultSet.next()) {
      resultString = _resultSet.getString("SedgwickMonth")+_resultSet.getString("SedgwickCounter")
    }
    _con.close()
    return resultString
  }
}
