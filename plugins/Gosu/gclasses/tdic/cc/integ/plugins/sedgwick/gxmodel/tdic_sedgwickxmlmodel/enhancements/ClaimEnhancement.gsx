package tdic.cc.integ.plugins.sedgwick.gxmodel.tdic_sedgwickxmlmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ClaimEnhancement : tdic.cc.integ.plugins.sedgwick.gxmodel.tdic_sedgwickxmlmodel.Claim {
  public static function create(object : entity.Claim) : tdic.cc.integ.plugins.sedgwick.gxmodel.tdic_sedgwickxmlmodel.Claim {
    return new tdic.cc.integ.plugins.sedgwick.gxmodel.tdic_sedgwickxmlmodel.Claim(object)
  }

  public static function create(object : entity.Claim, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.sedgwick.gxmodel.tdic_sedgwickxmlmodel.Claim {
    return new tdic.cc.integ.plugins.sedgwick.gxmodel.tdic_sedgwickxmlmodel.Claim(object, options)
  }

}