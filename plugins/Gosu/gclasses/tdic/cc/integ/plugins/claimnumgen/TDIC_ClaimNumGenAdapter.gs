package tdic.cc.integ.plugins.claimnumgen

/*
Claim numbers should generate in sequential order, beginning with one, and increase by one with every new claim.
    •The first three digits of the claim number reflect the year (YY) that the DOL occurred, preceded by 0.
    •The middle two digits of the claim number reflect the month that the DOL occurred.
    •The last six digits of the claim number, indicated by X below, are the actual claim number that needs to advance by one with every new claim entered into GW.
    0YY-MM-XXXXXX
    For example, the eighth claim occurring in January 2015, the claim number should generate as: 015-01-000008

    For temporary claims, doing TMP-CN-000001
*/

uses gw.plugin.claimnumbergen.IClaimNumGenAdapter
uses gw.pl.logging.LoggerCategory
uses gw.plugin.InitializablePlugin
uses java.util.Map
uses gw.api.util.StringUtil
uses gw.api.system.database.SequenceUtil
uses com.guidewire.pl.web.controller.UserDisplayableException
uses java.lang.Integer
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: skiriaki
 * Date: 3/12/15
 * Time: 1:35 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_ClaimNumGenAdapter implements IClaimNumGenAdapter, InitializablePlugin {
  private static var _logger = LoggerFactory.getLogger("TDIC_ClaimNumGen")
  /**
   * The sequence to use for generating claim numbers.
   */
  private var permClaimSeq : String
  private var tempClaimSeq : String
  private var riskClaimSeq : String

  // Changed this function to property as part of Upgrade to 10
  override property set Parameters(passedNameValue: Map<Object, Object>) {
    this.permClaimSeq = passedNameValue.get("PermClaimSeq") as String
    this.tempClaimSeq = passedNameValue.get("TempClaimSeq") as String
    this.riskClaimSeq = passedNameValue.get("RiskClaimSeq") as String
  }

  override function generateNewClaimNumber(claim: Claim): String {
    _logger.debug("ClaimNumGenAdapterImpl#generateNewClaimNumber() - Entering")

    if (this.permClaimSeq == null) {
      throw new UserDisplayableException("Unable to locate claim sequence")
    }
    if (claim == null) {
      throw new UserDisplayableException("Passed claim is invalid")
    }

    var date = claim.LossDate
    if (date == null) {
      throw new UserDisplayableException("Please specify loss date")
    }
    var claimNumber : String
    if(claim.RiskMgmtIncident_TDIC){
      claimNumber = "R" +
          StringUtil.substring(date.YearOfDate.toString(), 2, 4) +
          "-" +
          StringUtil.formatNumber(Coercions.makeDoubleFrom(Integer.toString(date.MonthOfYear)), "00") +
          "-" +
          StringUtil.formatNumber(SequenceUtil.next(1, this.riskClaimSeq), "000000")

    }else{
      claimNumber = "0" +
          StringUtil.substring(date.YearOfDate.toString(), 2, 4) +
          "-" +
          StringUtil.formatNumber(Coercions.makeDoubleFrom(Integer.toString(date.MonthOfYear)), "00") +
          "-" +
          StringUtil.formatNumber(SequenceUtil.next(1, this.permClaimSeq), "000000")
    }
    _logger.debug("ClaimNumGenAdapterImpl#generateNewClaimNumber() - Exiting")
    return claimNumber
  }

  override function generateTempClaimNumber(claim: Claim): String {
    _logger.debug("ClaimNumGenAdapterImpl#generateTempClaimNumber() - Entering")

    if (this.tempClaimSeq == null) {
      throw new UserDisplayableException("Unable to locate temporary sequence")
    }
    if (claim == null) {
      throw new UserDisplayableException("Passed claim is invalid")
    }

    var tempClaimNumber = "000-00-" + StringUtil.formatNumber(SequenceUtil.next(1, this.tempClaimSeq), "000000")

    _logger.debug("ClaimNumGenAdapterImpl#generateTempClaimNumber() - Exiting")
    return tempClaimNumber
  }

  override function cancelNewClaimNumber(claim: Claim, p1: String) {
    _logger.debug("ClaimNumGenAdapterImpl#cancelNewClaimNumber() - Entering")

    _logger.debug("ClaimNumGenAdapterImpl#cancelNewClaimNumber() - Exiting")
  }
}