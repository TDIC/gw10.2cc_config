package tdic.cc.integ.plugins.contact

uses gw.plugin.contact.ContactSystemPlugin
uses java.util.Set
uses gw.entity.IEntityType
uses java.util.Collection
uses gw.entity.IEntityPropertyInfo
uses gw.plugin.contact.search.DuplicateSearchResult
uses gw.plugin.contact.search.ContactSearchFilter
uses gw.plugin.contact.ContactCreateResult
uses gw.plugin.contact.search.ContactSearchResult

/**
 * US22
 * 08/26/2014 Rob Kelly
 *
 * Dummy ContactManager plugin implementation for Release A.
 * THIS PLUGIN IMPLEMENTATION PURPOSELY DOES NOTHING IN ORDER TO FULLY DISABLE CM INTEGRATION FOR RELEASE A.
 *
 * NOTE: This implementation will cause the following log ERROR for created contacts when moving from Step 1 to Step 2 of the New Claim Wizard:
 *
 *   Illegal state - trying to sync a contact "_CONTACT_NAME_" that is not linked.
 *
 * This is not in fact an error, but a bug in the implementation of the platform method gw.api.contact.ContactSystemUtil.syncToContact(Contact,Contact,boolean)
 * Portal request #140826-000016 has been created to track this bug.
 */
abstract class TDIC_DummyContactSystemPlugin implements ContactSystemPlugin {

  override function createContact(contact : Contact) : ContactCreateResult {
    return null
  }

  override function createContact(contact : Contact, transactionId : String) : ContactCreateResult {
    return null
  }

  override function retrieveContact(abUID : String) : Contact {
    return null
  }

  override function retrieveContact(abUID:String, relationships:Collection<ContactBidiRel>) : Contact {
    return null
  }

  override function retrieveRelatedContacts(contact : Contact, relationships:Collection<ContactBidiRel>) : Contact {
    return null
  }

  override function searchContacts(searchCriteria:ContactSearchCriteria, searchFilter:ContactSearchFilter) : ContactSearchResult {
    return null
  }

  override function findDuplicates(contact : Contact, searchFilter:ContactSearchFilter) : DuplicateSearchResult {
    return null
  }

  override function hasSyncableChanges(contact : Contact): boolean {
    return false
  }

  override function updateContact(contact : Contact) {
  }

  override function updateContact(contact : Contact, transactionId : String) {
  }

  override function createAsyncUpdate(messageCtx : MessageContext) {
  }

  override function sendAsyncUpdate(message:Message, payload:String) {
  }

  override function getSyncablePropertiesForType(type : IEntityType): Collection<IEntityPropertyInfo> {
    return null
  }

  override function getPersistPropertiesForType(type : IEntityType): Collection<IEntityPropertyInfo> {
    return null
  }

  override function getAllMappedPropertiesForType(type : IEntityType): Collection<IEntityPropertyInfo> {
    return null
  }

  override function getSyncableRelationshipsForContactType(type : IEntityType, onlyPrimary : boolean) : Set<ContactBidiRel> {
    return null
  }
}