package tdic.cc.integ.plugins.generalledger.dto

uses java.util.ArrayList
uses gw.api.util.DateUtil
uses java.text.SimpleDateFormat
uses java.util.List

class GLTransactionFile {

  static final var DATE_FORMAT_WITH_SLASH = "MM/dd/yyyy"

  static final var DATE_FORMAT_WITH_NO_SLASH = "MMddyyyy"

  /**
   * The List of check payments to write to the flat files.
   */
  var _glTransactions : List<GLTransactionDTO> as GLTransactions


  construct() {
    _glTransactions = new ArrayList<GLTransactionDTO>()

  }

  /**
   * Formats the current date in its full format for the flat files.
   */
  @Returns("A String containing the formatted current date in its full format for the flat files")
  static function formatCurrentDateFull() : String {
    var currentDateTime = DateUtil.currentDate()
    var dateFormat = new SimpleDateFormat(DATE_FORMAT_WITH_SLASH)
    return dateFormat.format(currentDateTime)
  }

 /* *//**
   * Formats the current date without any separators for the flat files.
   *//*
  @Returns("A String containing the formatted current date without any separators for the flat files")
  static function formatCurrentDateNoSeparators() : String {
    var currentDateTime = DateUtil.currentDate()
    var dateFormat = new SimpleDateFormat(DATE_FORMAT_WITH_NO_SLASH)
    return dateFormat.format(currentDateTime)
  }*/
}