package tdic.cc.integ.plugins.generalledger.model.gltransactiondtomodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLTransactionDTOEnhancement : tdic.cc.integ.plugins.generalledger.model.gltransactiondtomodel.GLTransactionDTO {
  public static function create(object : tdic.cc.integ.plugins.generalledger.dto.GLTransactionDTO) : tdic.cc.integ.plugins.generalledger.model.gltransactiondtomodel.GLTransactionDTO {
    return new tdic.cc.integ.plugins.generalledger.model.gltransactiondtomodel.GLTransactionDTO(object)
  }

  public static function create(object : tdic.cc.integ.plugins.generalledger.dto.GLTransactionDTO, options : gw.api.gx.GXOptions) : tdic.cc.integ.plugins.generalledger.model.gltransactiondtomodel.GLTransactionDTO {
    return new tdic.cc.integ.plugins.generalledger.model.gltransactiondtomodel.GLTransactionDTO(object, options)
  }

}