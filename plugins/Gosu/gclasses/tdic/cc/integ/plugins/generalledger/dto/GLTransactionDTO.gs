package tdic.cc.integ.plugins.generalledger.dto

uses java.math.BigDecimal

/**
 * GLTransactionDTO contains CC transaction info felds for General Ledger outbound.
 * Created by RambabuN on 12/17/2019.
 */
class GLTransactionDTO {

  var _trasactionID : String as TrasactionID
  var _sequenceNumber : Integer as SequenceNumber
  var _transactionSubtype : String as TransactionSubtype
  var _debitAccountingUnit : String as DebitAccountingUnit
  var _creditAccountingUnit : String as CreditAccountingUnit
  var _debitAccountNum : String as DebitAccountNum
  var _creditAccountNum : String as CreditAccountNum
  var _debitAmount : BigDecimal as DebitAmount
  var _creditAmount : BigDecimal as CreditAmount
  var _stateCode : String as StateCode
  var _lobCode : String as LOBCode
  var _accountingUnit : String as AccountingUnit
  var _accountNum : String as AccountNum
  var _description : String as Description
  var _amount : BigDecimal as Amount
  var _claimNumber : String as ClaimNumber
  var _ledgerSide : String as LedgerSide
  var _monthEndDate : String as MonthEndDate
  var _refCode : String as RefCode

}