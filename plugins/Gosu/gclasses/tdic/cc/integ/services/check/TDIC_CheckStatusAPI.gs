package tdic.cc.integ.services.check

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.server.AvailabilityLevel
uses gw.api.system.server.ServerUtil
uses gw.xml.ws.annotation.WsiAvailability
uses gw.xml.ws.annotation.WsiWebService
uses org.slf4j.LoggerFactory
uses tdic.cc.integ.services.check.dto.TDIC_CheckStatusRequest
uses tdic.cc.integ.services.check.dto.TDIC_CheckStatusResponse
uses tdic.cc.integ.services.check.dto.TDIC_CheckStatusDTO
uses tdic.cc.integ.services.check.dto.TDIC_CheckStatusResponseDTO
uses tdic.cc.integ.services.util.TDIC_WebServiceErrorCodes
uses tdic.cc.integ.services.util.dto.TDIC_ErrorMessageDTO

/**
 * This class is used to publish the webservice to update check status in GW from Lawson check system.
 * Created by RambabuN on 11/22/2019.
 */
@WsiWebService("http://tdic.com/tdic/cc/integ/services/check/TDIC_CheckStatusAPI")
@WsiAvailability(AvailabilityLevel.MAINTENANCE)
@Export
class TDIC_CheckStatusAPI {

  static final var _logger = LoggerFactory.getLogger("CHECK_STATUS_UPDATE")
  static final var CLASS_NAME = "TDIC_CheckStatusAPI"
  static final var API_USER = "iu"

  //Email
  static final var EMAIL_TO = PropertyUtil.getInstance().getProperty("CheckStatusNotificationEmail")
  static final var FAILURE_EMAIL_TO = PropertyUtil.getInstance().getProperty("CCInfraIssueNotificationEmail")
  static final var FAILURE_SUB = "Failure::${ServerUtil.Env}-Check Status Update Failed"
  static final var SUCCESS_SUB = "Success::${ServerUtil.Env}-Check Status Update completed successfully"
  static final var PARTIAL_SUCCESS_SUB =
      "PartialSuccess::${ServerUtil.Env}-Check Status Update completed successfully with errors and/or warnings"

  //Check Status Typecode mappings
  static final var MAPPER = gw.api.util.TypecodeMapperUtil.getTypecodeMapper()
  static final var TYPELIST = "TransactionStatus"
  static final var NAMESPACE = "tdic:lawson"

  //Check Status Records
  var _notMatchedRecordsList : List<TDIC_CheckStatusResponseDTO>
  var _errorCheckStatusRecordsList : List<TDIC_CheckStatusResponseDTO>

  /**
   * Function to update Lawson check status into GW.
   * @param request
   * @return TDIC_CheckStatusResponse.
   */
  function updateCheckStatus(request : TDIC_CheckStatusRequest) : TDIC_CheckStatusResponse {
    var logPrefix = "${CLASS_NAME}#updateCheckStatus(request)"
    _logger.info("${logPrefix} - start")
    _logger.info("${logPrefix} - ${request}")

    var response = new TDIC_CheckStatusResponse()
    try {
      _errorCheckStatusRecordsList = new ArrayList<TDIC_CheckStatusResponseDTO>()
      //validate Check Status request details
      var validDataCheckStatusRecordsList = validateCheckStatusRequestDetails(request.CheckStatusDTOList)

      //process valid check status records
      processCheckStatusUpdates(validDataCheckStatusRecordsList)

      //update response and send email
      if (_errorCheckStatusRecordsList.HasElements || _notMatchedRecordsList.HasElements) {
        if(_errorCheckStatusRecordsList.HasElements) {
          response.CheckStatusResponseDTOList = _errorCheckStatusRecordsList
        }
        EmailUtil.sendEmail(EMAIL_TO, PARTIAL_SUCCESS_SUB, "${buildEmailDescription()}")
      }
      else {
        EmailUtil.sendEmail(EMAIL_TO, SUCCESS_SUB, "Check status update successfully completed.")
      }
    } catch(e : Exception) {
      _logger.error("${logPrefix} - Check status update process failed with erorrs.", e)
      EmailUtil.sendEmail(FAILURE_EMAIL_TO, FAILURE_SUB,
          "Check status update failed with errors. Please review below error details. \n ${e.StackTraceAsString}")
      throw e
    }
    _logger.info("${logPrefix} - ${response}")
    _logger.info("${logPrefix} - end")
    return response
  }

  /**
   * Fuction to process the check status updates in GW.
   * @param checkStatusRecordsList
   */
  @Param("paymentRecords","List of Check records retrtieved for status updates")
  @Param("checkStatusRecords","Check status update records retrieved from Lawson system")
  private function processCheckStatusUpdates(checkStatusRecordsList : List<TDIC_CheckStatusDTO>) {
    var logPrefix = "${CLASS_NAME}#processCheckStatusUpdates(List, Map)"
    _logger.debug("${logPrefix} - start")

    _notMatchedRecordsList = new ArrayList<TDIC_CheckStatusResponseDTO>()

    if(checkStatusRecordsList.HasElements) {
      var checkPayments = retrieveCheckPayments(checkStatusRecordsList*.TransactionRecordID)
      var check : Check
      var errorMessagesList : List<TDIC_ErrorMessageDTO>
      for (checkStatusRecord in checkStatusRecordsList) {
        if(checkPayments.Count > 0) {
          check = checkPayments.firstWhere(\pr -> pr.PublicID?.equalsIgnoreCase(checkStatusRecord.TransactionRecordID))
        }
        errorMessagesList = new  ArrayList<TDIC_ErrorMessageDTO>()
        if (check != null) {
          //Update Lawson check status on Check entity.
          updateCheckPaymentStatusDetails(check, errorMessagesList, checkStatusRecord)
        } else {
          //Check not found with TransactionReferenceID in CC
          errorMessagesList.add(new TDIC_ErrorMessageDTO() { :
              ErrorCode = TDIC_WebServiceErrorCodes.CHECK_PAYMENT_RECORD_NOT_FOUND })
        }

        if (errorMessagesList.HasElements) {
          addToErrorCheckStatusRecords(checkStatusRecord, errorMessagesList)
        }
      }
    }
    _logger.debug("${logPrefix} - end")
  }

  /**
   * Function to update Lawson check status on Check entity
   * @param check
   * @param errorMessagesList
   * @param checkStatusRecord
   */
  private function updateCheckPaymentStatusDetails(check : Check, errorMessagesList : List<TDIC_ErrorMessageDTO>,
                                            checkStatusRecord : TDIC_CheckStatusDTO) {
    var logPrefix = "${CLASS_NAME}#updateCheckStatusDetails(check,errorMessagesList, checkStatusRecord)"
    try {
      var status = MAPPER.getInternalCodeByAlias(
          TYPELIST, NAMESPACE, checkStatusRecord.CheckStatus?.toUpperCase())
      if (status == null) {
        errorMessagesList.add(new TDIC_ErrorMessageDTO() { :
            ErrorCode = TDIC_WebServiceErrorCodes.INVALID_CHECK_STATUS_VALUE })
      } else {
        matchRecord(check, checkStatusRecord)
        _logger.info("${logPrefix} - Check status update starting for '${check.PublicID}'")
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          check = bundle.add(check)
          switch(TransactionStatus.get(status)) {
            case TransactionStatus.TC_VOIDED:
              check.voidCheck()
              break
            case TransactionStatus.TC_STOPPED:
              check.stopCheck()
              break
            default:
              check.Status = TransactionStatus.get(status)
              break
          }
          check.CheckNumber = checkStatusRecord.CheckNumber
          if (status == TransactionStatus.TC_ISSUED.Code) {
            check.IssueDate = checkStatusRecord.CheckIssueDate
          } else {
            check.StatusUpdateDate_TDIC = checkStatusRecord.CheckStatusDate
          }
          if(check.Bulked){
            try{
              var bulkInvoice = check.BulkInvoiceItem.BulkInvoice
              var bulkInvoiceFromDB = Query.make(BulkInvoice).compare(BulkInvoice#PublicID, Equals, bulkInvoice.PublicID).select().FirstResult
              var transactionStatus = TransactionStatus.get(status)
              if(bulkInvoiceFromDB!=null and bulkInvoiceFromDB.InvoiceItems.Count>0 and
                  not bulkInvoiceFromDB.InvoiceItems.hasMatch(\item ->
                      item.BulkInvoiceItemInfo.Check!=check and item.BulkInvoiceItemInfo.Check.Status!=transactionStatus)){
                bulkInvoice = bundle.add(bulkInvoice)
                if(transactionStatus == TransactionStatus.TC_ISSUED){
                  bulkInvoice.Status = BulkInvoiceStatus.TC_ISSUED
                  bulkInvoice.IssueDate = checkStatusRecord.CheckIssueDate
                } else if(transactionStatus == TransactionStatus.TC_CLEARED){
                  bulkInvoice.Status = BulkInvoiceStatus.TC_CLEARED
                } else if(transactionStatus == TransactionStatus.TC_VOIDED){
                  bulkInvoice.Status = BulkInvoiceStatus.TC_VOIDED
                } else if(transactionStatus == TransactionStatus.TC_STOPPED){
                  bulkInvoice.Status = BulkInvoiceStatus.TC_STOPPED
                }
              } else {
                _logger.debug("Bulk Invoice with Invoice Number ${check.BulkInvoiceItem.BulkInvoice.InvoiceNumber} has some checks out of ${transactionStatus}, Hence bulk invoice status is not updated.")
              }
            }catch(exception : Exception){
              _logger.error("Check ${check.PublicID} belongs to Bulk Invoice with Invoice Number ${check.BulkInvoiceItem.BulkInvoice.InvoiceNumber} status update failed.", exception)
            }
          }
        }, "su")
        _logger.info("${logPrefix} - Check status update completed for '${check.PublicID}'")
      }
    } catch (e : Exception) {
      _logger.error("${logPrefix} - Check status update process failed with erorrs.", e)
      errorMessagesList.add(new TDIC_ErrorMessageDTO() { :
          ErrorCode = TDIC_WebServiceErrorCodes.CHECK_STATUS_UPDATE_FAILED })
    }
    _logger.debug("${logPrefix} - end")
  }

  /**
   * Function to validate CheckStatusRequest details.
   * @param checkStatusRecordsList
   * @return list of valid check status DTO's
   */
  private function validateCheckStatusRequestDetails(
      checkStatusRecordsList : List<TDIC_CheckStatusDTO>): List<TDIC_CheckStatusDTO> {

    var validDataCheckStatusRecordsList = new ArrayList<TDIC_CheckStatusDTO>()
    var errorMessagesList = new ArrayList<TDIC_ErrorMessageDTO>()
    var errorRecord : TDIC_ErrorMessageDTO
    for(checkStatusRecord in checkStatusRecordsList) {
      if(isRequiredDataAvailable(checkStatusRecord)) {
        validDataCheckStatusRecordsList.add(checkStatusRecord)
      } else {
        //TransactionID is null
        if (!checkStatusRecord.TransactionRecordID.HasContent) {
          errorRecord = new TDIC_ErrorMessageDTO()
          errorRecord.ErrorCode = TDIC_WebServiceErrorCodes.CHECK_TRANSACTION_RECORD_ID_EMPTY
          errorMessagesList.add(errorRecord)
        }

        if (errorMessagesList.HasElements) {
          addToErrorCheckStatusRecords(checkStatusRecord, errorMessagesList)
        }
      }
    }
    return validDataCheckStatusRecordsList
  }

  /**
   * Function to validate required info TransactionRecordID on check status record.
   * @param checkStatusRecord
   * @return boolean.
   */
  private function isRequiredDataAvailable(checkStatusRecord : TDIC_CheckStatusDTO) : Boolean {
    return (checkStatusRecord!=null && checkStatusRecord.TransactionRecordID.HasContent)
  }

  /**
   * Function to add error messages for checkstatus record.
   * @param checkStatusRecord
   * @param errors
   */
  private function addToErrorCheckStatusRecords(
      checkStatusRecord : TDIC_CheckStatusDTO, errors : List<TDIC_ErrorMessageDTO>) {
    var checkStatusResponseRecord = new TDIC_CheckStatusResponseDTO()
    checkStatusResponseRecord.CheckStatusDTO = checkStatusRecord
    checkStatusResponseRecord.Errors = errors
    _errorCheckStatusRecordsList.add(checkStatusResponseRecord)
  }

  /**
   * Function to get check payments based on check publicID's.
   * @param publicIDs
   * @return
   */
  protected function retrieveCheckPayments(publicIDs : String[]) : IQueryBeanResult<Check> {
    var logPrefix = "${CLASS_NAME}#retrieveCheckPayments()"
    _logger.debug("${logPrefix}- start")
    var pQuery = Query.make(Check).compareIn(Check#PublicID, publicIDs).select()
    _logger.debug("${logPrefix} - end")
    return pQuery
  }

  /**
   * Function to match CheckStatus record with Check payment.
   * @param check
   * @param checkStatusRecord
   */
  private function matchRecord(check : Check, checkStatusRecord : TDIC_CheckStatusDTO) {
    var comments = new StringBuilder()
    if(check.IssueDate != checkStatusRecord.CheckIssueDate)
      comments.append("CheckIssueDate: ${check.IssueDate} vs ${checkStatusRecord.CheckIssueDate} not matching | ")

    if(check.CheckNumber != null && check.CheckNumber != checkStatusRecord.CheckNumber)
      comments.append("CheckNumber: ${check.CheckNumber} vs ${checkStatusRecord.CheckNumber} not matching | ")

    if(check.NetAmount.Amount != checkStatusRecord.CheckAmount)
      comments.append("Amount: ${check.NetAmount.Amount} vs ${checkStatusRecord.CheckAmount} not matching | ")

    var vendorNumber = check.FirstPayee.ClaimContact.Contact.VendorNumber
    if(vendorNumber != checkStatusRecord.VendorID)
      comments.append("VendorNumber: ${vendorNumber} vs ${checkStatusRecord.VendorID} not matching | ")

    if(comments.toString().HasContent) {
      var noMatchRecord = new TDIC_CheckStatusResponseDTO()
      noMatchRecord.CheckStatusDTO = checkStatusRecord
      noMatchRecord.Comments = comments.toString()
      _notMatchedRecordsList.add(noMatchRecord)
    }
  }

  /**
   * Function to build Email description.
   * @return email description.
   */
  private function buildEmailDescription() : String {
    var description = new StringBuilder()
    description.append("Check status update completed with Errors and/or Warnings.\n")
    if(_notMatchedRecordsList.HasElements) {
      description.append(
          "Check datails (CC Check Payment record vs Lawson check status record) that are not matching are :-\n")
      _notMatchedRecordsList.each( \ record -> {
        description.append("CheckNumber: '${record.CheckStatusDTO.CheckNumber}' -  ${record.Comments}\n")
      })
    }
    if(_errorCheckStatusRecordsList.HasElements) {
      description.append("Error Records are:-\n")
      _errorCheckStatusRecordsList.each( \ record -> {
        description.append("Error in processing check status update for the check number: " +
        "'${record.CheckStatusDTO.CheckNumber}'. Details are: ${record.Comments}\n")
      })
    }
    return description?.toString()
  }

}