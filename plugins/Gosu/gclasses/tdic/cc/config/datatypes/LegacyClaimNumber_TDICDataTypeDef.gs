package tdic.cc.config.datatypes

uses com.guidewire.pl.metadata.datatype2.constraints.FieldValidatorBasedStringConstraintsHandler
uses com.guidewire.pl.metadata.datatype2.persistence.FieldValidatorBasedVarcharPersistenceHandler
uses com.guidewire.pl.metadata.datatype2.presentation.FieldValidatorBasedPresentationHandler
uses gw.datatype.handler.IDataTypeConstraintsHandler
uses gw.datatype.handler.IDataTypePersistenceHandler
uses gw.datatype.handler.IDataTypePresentationHandler

class LegacyClaimNumber_TDICDataTypeDef extends com.guidewire.pl.metadata.datatype2.impl.FieldValidatorBasedEncryptableStringDataTypeDef {


  protected override property get DataTypeCustomizationName() : String {
    return "LegacyClaimNumber_TDICDataType"
  }

  override property get ConstraintsHandler() : IDataTypeConstraintsHandler {
    return new FieldValidatorBasedStringConstraintsHandler(DataTypeCustomizationName, getValidator(), getLogicalSize())
  }

  override property get PersistenceHandler() : IDataTypePersistenceHandler {
    return new FieldValidatorBasedVarcharPersistenceHandler(DataTypeCustomizationName,getEncryption(), getTrimWhitespace(), getLinguistic())
  }

  override property get PresentationHandler() : IDataTypePresentationHandler {
    return new FieldValidatorBasedPresentationHandler(DataTypeCustomizationName, getValidator(), getLogicalSize())
  }
}