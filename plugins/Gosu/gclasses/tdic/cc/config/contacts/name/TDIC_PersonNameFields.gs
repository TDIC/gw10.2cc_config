package tdic.cc.config.contacts.name

uses gw.api.name.PersonNameFields

/**
 * US1031, DE66
 * 01/06/2015 Rob Kelly
 *
 * Additional name fields for Persons at TDIC.
 */
interface TDIC_PersonNameFields extends PersonNameFields {

  property get Credential_TDIC() : Credential_TDIC
  property set Credential_TDIC(value : Credential_TDIC)
}