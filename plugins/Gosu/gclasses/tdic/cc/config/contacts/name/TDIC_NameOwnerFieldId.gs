package tdic.cc.config.contacts.name

uses gw.api.name.NameOwnerFieldId

/**
 * US1031, DE66
 * 01/06/2015 Rob Kelly
 *
 * Extension to NameOwnerFieldId for additional available fields for person name at TDIC.
 */
class TDIC_NameOwnerFieldId extends NameOwnerFieldId {

  protected construct(aName : String) {
    super(aName)
  }

  public static final var CREDENTIAL : NameOwnerFieldId = new TDIC_NameOwnerFieldId("Credential_TDIC")
}