package tdic.cc.config.contacts.name

uses gw.api.name.ContactNameOwner
uses gw.api.name.NameOwnerFieldId
uses gw.api.name.ContactNameFields

uses java.util.Set

/**
 * US22
 * 08/22/2014 Rob Kelly
 *
 * NameOwner for FNOL Contacts.
 */
class TDIC_ContactNameOwner extends ContactNameOwner {

  construct(fields : ContactNameFields) {

    super(fields)
  }

  override property get RequiredFields() : Set<NameOwnerFieldId> {

    return NameOwnerFieldId.FIRST_LAST_FIELDS
  }

}