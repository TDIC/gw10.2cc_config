package tdic.cc.config.contacts.address

uses gw.api.address.ContactHandleAddressOwner
uses gw.api.contact.ContactHandle
uses gw.api.address.AddressOwnerFieldId

uses java.util.Set

/**
 * US22
 * 08/22/2014 Rob Kelly
 *
 * AddressOwner for FNOL Doctor ContactHandlers.
 */
class TDIC_InjuredContactHandleAddressOwner extends ContactHandleAddressOwner {

  construct(contactHandle : ContactHandle) {
    super(contactHandle)
  }

  override property get RequiredFields() : Set<AddressOwnerFieldId> {
    return AddressOwnerFieldId.ADDRESSLINE1_CITY_STATE_ZIP
  }
}