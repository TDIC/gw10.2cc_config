package tdic.cc.config.fnol

uses tdic.cc.config.contacts.address.TDIC_ClaimAddressOwner
uses gw.api.address.CCAddressOwner
uses com.guidewire.pl.web.controller.UserDisplayableException
uses gw.api.locale.DisplayKey

/**
 * US22, US529, US521, US838
 * 10/13/2014 Rob Kelly
 *
 * A helper class for WC7 FNOL at TDIC.
 */
class TDIC_WC7FNOLHelper {

  /**
   * US529, US521
   * Returns the CCAddressOwner for the specified Claim.
   */
  static function getClaimAddressOwner(aClaim : Claim) : CCAddressOwner {
    if (aClaim.LossType == typekey.LossType.TC_WC7) {
      return new TDIC_ClaimAddressOwner(aClaim)
    }
    return aClaim.AddressOwner
  }

  /**
   * US22
   * Determine whether the TaxID field is required or not for the specified ClaimContact.
   */
  static function isTaxIdRequired(aClaimContact : ClaimContact) : boolean {
    if (aClaimContact.Roles == null) {
      return false
    }
    return aClaimContact.Roles.firstWhere( \ r -> r.Role == typekey.ContactRole.TC_INJURED or r.Role == typekey.ContactRole.TC_CLAIMANT) != null
  }

  /**
   * GCC-862 Need Ability to View Social Security Numbers on Third Party Claims for Manual ISO Reporting
   * @param taxID
   * @return
   */
  static function maskTaxID(aClaimContact : ClaimContact,person:Person,taxId:String):String{
    if(!aClaimContact.Claim?.isWorkCompClaim() and aClaimContact?.Roles?.hasMatch(\r -> r?.Role == typekey.ContactRole.TC_INJURED or r?.Role == typekey.ContactRole.TC_CLAIMANT)){
      return person.unMaskTaxId(taxId)
    }
   return person.maskTaxId(taxId)
  }

  /**
   * US22
   * Determine whether the TaxID field is required or not for the specified ContactRole.
   */
  static function isTaxIdRequired(aContactRole : typekey.ContactRole) : boolean {
    return (aContactRole == typekey.ContactRole.TC_INJURED or aContactRole == typekey.ContactRole.TC_CLAIMANT)
  }

  /**
   * US22
   * Determine whether the Date of Birth field is required or not for the specified ClaimContact.
   */
  static function isDOBRequired(aClaimContact : ClaimContact) : boolean {
    if (aClaimContact.Roles == null) {
      return false
    }
    return aClaimContact.Roles.firstWhere( \ r -> r.Role == typekey.ContactRole.TC_INJURED or r.Role == typekey.ContactRole.TC_CLAIMANT) != null
  }

  /**
   * US22
   * Determine whether the Date of Birth field is required or not for the specified ContactRole.
   */
  static function isDOBRequired(aContactRole : typekey.ContactRole) : boolean {
    return (aContactRole == typekey.ContactRole.TC_INJURED or aContactRole == typekey.ContactRole.TC_CLAIMANT)
  }

  /**
   * US22
   * Determine whether the Gender field is required or not for the specified ClaimContact.
   */
  static function isGenderRequired(aClaimContact : ClaimContact) : boolean {
    if (aClaimContact.Roles == null) {
      return false
    }
    return aClaimContact.Roles.firstWhere( \ r -> r.Role == typekey.ContactRole.TC_INJURED or r.Role == typekey.ContactRole.TC_CLAIMANT) != null
  }

  /**
   * US22
   * Determine whether the Gender field is required or not for the specified ContactRole.
   */
  static function isGenderRequired(aContactRole : typekey.ContactRole) : boolean {
    return (aContactRole == typekey.ContactRole.TC_INJURED or aContactRole == typekey.ContactRole.TC_CLAIMANT)
  }

  /**
   * US22, US838
   * Determine whether the Occupation field is required or not for the specified ClaimContact.
   */
  static function isOccupationRequired(aClaimContact : ClaimContact) : boolean {
    if(aClaimContact.Claim.LossType == LossType.TC_WC7){
      return true
    }
    if (aClaimContact.Roles == null) {
      return false
    }
    return aClaimContact.Roles.hasMatch( \ r -> r.Role == typekey.ContactRole.TC_INJURED or r.Role == typekey.ContactRole.TC_CLAIMANT )
  }

  /**
   * US22, US838
   * Determine whether the Occupation field is required or not for the specified ContactRole.
   */
  static function isOccupationRequired(aContactRole : typekey.ContactRole) : boolean {
    return (aContactRole == typekey.ContactRole.TC_INJURED or aContactRole == typekey.ContactRole.TC_CLAIMANT  or aContactRole == ContactRole.TC_REPORTER)
  }

  /**
   * US838
   * Returns the label to use for the Occupation field for the specified ClaimContact.
   */
  static function getOccupationLabel(aClaimContact : ClaimContact) : String {
    if (aClaimContact.Roles != null) {
      if (aClaimContact.Roles.firstWhere( \ r -> r.Role == typekey.ContactRole.TC_REPORTER) != null) {
        return DisplayKey.get("TDIC.Claim.ReportedBy.Occupation")
      }
    }
    return DisplayKey.get("Web.ContactDetail.Company.Occupation")
  }

  /**
   * US838
   * Returns the label to use for the Occupation field for the specified ContactRole.
   */
  static function getOccupationLabel(aContactRole : typekey.ContactRole) : String {
    if (aContactRole == typekey.ContactRole.TC_REPORTER) {
      return DisplayKey.get("TDIC.Claim.ReportedBy.Occupation")
    }
    return DisplayKey.get("Web.ContactDetail.Company.Occupation")
  }

  /**
   * US538
   * Ensure that all required fields are present on FNOL Wizard Step 2 Contacts.
   */
  static function ensureBasicInfoContactRequiredFieldsPresent(aClaim : Claim) : void {
    if (aClaim.reporter != null and aClaim.LossType == LossType.TC_WC7) {
      ensureReportedByRequiredFieldsPresent(aClaim)
    }
    if (aClaim.maincontact != null and aClaim.LossType == LossType.TC_WC7) {
      ensureMainContactRequiredFieldsPresent(aClaim)
    }
  }

  /**
   * US538, US838
   * Ensure that all required fields are present for the Claim reporter.
   */
  private static function ensureReportedByRequiredFieldsPresent(aClaim : Claim) : void {
    if (aClaim.reporter.WorkPhone == null) {
      throw new UserDisplayableException(DisplayKey.get("TDIC.FNOL.BasicInfo.ReportedByRequiredField", DisplayKey.get("Web.ContactDetail.Phone.Work") + " " + DisplayKey.get("Web.ContactDetail.Phone")))
    }

    if (aClaim.reporter.EmailAddress1 == null) {
      throw new UserDisplayableException(DisplayKey.get("TDIC.FNOL.BasicInfo.ReportedByRequiredField", DisplayKey.get("Web.ContactDetail.Email.Primary") + " " + DisplayKey.get("Web.ContactDetail.Email")))
    }

    if (aClaim.reporter typeis Person and aClaim.reporter.Occupation == null) {
      throw new UserDisplayableException(DisplayKey.get("TDIC.FNOL.BasicInfo.ReportedByRequiredField", DisplayKey.get("TDIC.Claim.ReportedBy.Occupation")))
    }
  }

  /**
   * US538
   * Ensure that all required fields are present for the Claim maincontact.
   */
  private static function ensureMainContactRequiredFieldsPresent(aClaim: Claim) : void {
    if (aClaim.maincontact.WorkPhone == null) {
      throw new UserDisplayableException(DisplayKey.get("TDIC.FNOL.BasicInfo.MainContactRequiredField", DisplayKey.get("Web.ContactDetail.Phone.Work") + " " + DisplayKey.get("Web.ContactDetail.Phone")))
    }

    if (aClaim.maincontact.EmailAddress1 == null) {
      throw new UserDisplayableException(DisplayKey.get("TDIC.FNOL.BasicInfo.MainContactRequiredField", DisplayKey.get("Web.ContactDetail.Email.Primary") + " " + DisplayKey.get("Web.ContactDetail.Email")))
    }
  }
}