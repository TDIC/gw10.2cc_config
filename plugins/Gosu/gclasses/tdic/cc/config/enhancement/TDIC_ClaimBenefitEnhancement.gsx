package tdic.cc.config.enhancement

/**
 * DE191
 * 04/01/2015 Rob Kelly
 *
 * Enhancement providing methods to support Claim Benefit functionality for Time Loss/Indemnity exposures.
 */
enhancement TDIC_ClaimBenefitEnhancement : entity.Claim {

  function getWC7LossTimeExposure() : Exposure {
    for (anExp in this.Exposures) {
      if (anExp.CoverageSubType== typekey.CoverageSubtype.TC_WC7WORKERSCOMP_WAGES_TDIC) {
        return anExp
      }
    }
    return null
  }
}
