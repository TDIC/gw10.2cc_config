package tdic.cc.config.enhancement

uses gw.api.database.Relop
uses gw.api.database.Query
uses gw.api.financials.CurrencyAmount
uses gw.api.financials.FinancialsCalculationUtil
uses gw.api.financials.FinancialsCalculations
uses gw.api.financials.FinancialsCalculationsForEditedTransaction
uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
uses gw.api.util.DisplayableException
uses gw.api.util.LocationUtil
uses gw.pl.currency.MonetaryAmount
uses tdic.cc.integ.plugins.hpexstream.util.TDIC_CCExstreamHelper
uses gw.pl.logging.LoggerCategory
uses org.slf4j.LoggerFactory
uses typekey.Transaction

uses java.io.StringWriter
uses java.math.BigDecimal

/**
 * US644
 * 11/25/2014 shanem
 *
 * Enhancement providing methods to create documents for a claim
 */
enhancement TDIC_ClaimEnhancement: entity.Claim {
  /**
   * US644
   * 11/25/2014 shanem
   *
   * Get template Ids for event name and create appropriate documents on the claim
   */
  @Param("eventName", "Event Name to create documents for")
  function createClaimDocumentsForEvent(eventName: String): void {
    var _exstreamLogger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
    _exstreamLogger.trace("TDIC_ClaimEnhancement#sendIssuanceRequest() - Entering.")
    var ccHelper = new TDIC_CCExstreamHelper()
    ccHelper.createDocumentStubs(eventName, this)
    _exstreamLogger.trace("TDIC_ClaimEnhancement#sendIssuanceRequest() - Notifying Exstream Message Queue.")
  }

  /**GW-2823
   * Unable to Retreive Severity Codes.
   * Modified to get the incident severity Codes.
   */
  function getIncidentWithSeverityCodes():Incident{
    var incidentOfClaim :Incident = null
    for(var incident in this.Incidents){
      if(incident.Severity != null){
        incidentOfClaim = incident
      }
    }
    return incidentOfClaim
  }
  /**
   * create by: SanjanaS
   * @description: method to get a list of Call Type Values based on the Claim Type selection
   * @create time: 3:57 PM 7/26/2019
    * @param claim Claim
   * @return: java.util.List<String>
   */

  function getCallType(claim: Claim) : List<String> {
    var callTypeList : List<String> = {}
    callTypeList.add("COVID-19")
    callTypeList.add("Teledentistry")
    if(claim.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY or
        claim.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY or
        claim.Type_TDIC == ClaimType_TDIC.TC_BUSINESSLIABILITY or
        claim.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE or
        claim.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY or
        claim.Type_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY){
      callTypeList.add("Abuse")
      callTypeList.add("ADA")
      callTypeList.add("Advertising")
      callTypeList.add("Allergy")
      callTypeList.add("Anesthesia")
      callTypeList.add("Anesthesia/Sedation")
      callTypeList.add("Arbitration")
      callTypeList.add("Aspirated/Swallowed Object")
      callTypeList.add("Bankruptcy")
      callTypeList.add("BBB")
      callTypeList.add("Billing/Collections")
      callTypeList.add("Bisphosphonate")
      callTypeList.add("Biopsy")
      callTypeList.add("Bleaching")
      callTypeList.add("Botox/Derma Filler")
      callTypeList.add("Burns/Lacerations")
      callTypeList.add("Call Back")
      callTypeList.add("Caregivers Authorization")
      callTypeList.add("CBCT")
      callTypeList.add("Closing/Separating from Practice")
      callTypeList.add("Coverage Questions")
      callTypeList.add("Crown/Bridge")
      callTypeList.add("Defamation/Slander")
      callTypeList.add("Dental Board")
      callTypeList.add("Dental screening")
      callTypeList.add("Denture")
      callTypeList.add("Deposition")
      callTypeList.add("Dismissal")
      callTypeList.add("Divorce")
      callTypeList.add("Drug Seeker")
      callTypeList.add("Endodontic (RCT)/Apico")
      callTypeList.add("Extraction")
      callTypeList.add("Failure to Diagnose")
      callTypeList.add("Failure to Refer")
      callTypeList.add("Fee Dispute")
      callTypeList.add("Graft")
      callTypeList.add("Health History")
      callTypeList.add("HIPAA/Privacy")
      callTypeList.add("Implants")
      callTypeList.add("Infection")
      callTypeList.add("Informed Consent")
      callTypeList.add("Informed Refusal")
      callTypeList.add("Intent-to-sue letter")
      callTypeList.add("Invisalign")
      callTypeList.add("MedPay")
      callTypeList.add("Non-Compliance")
      callTypeList.add("Ortho")
      callTypeList.add("Parasthesia")
      callTypeList.add("Patient Abandonment")
      callTypeList.add("Patient/Case Selection")
      callTypeList.add("Patient Dictating Treatment")
      callTypeList.add("Patient Unhappy/Angry")
      callTypeList.add("Peer Review")
      callTypeList.add("Perforation")
      callTypeList.add("Periodontal Disease")
      callTypeList.add("Premedication")
      callTypeList.add("Prescriptions")
      callTypeList.add("Radiographs")
      callTypeList.add("RDH")
      callTypeList.add("Records")
      callTypeList.add("Referral")
      callTypeList.add("Refund")
      callTypeList.add("Release of Liability")
      callTypeList.add("Retreatment")
      callTypeList.add("RM Discount")
      callTypeList.add("Seminar")
      callTypeList.add("Sleep Apnea")
      callTypeList.add("Slip and Fall")
      callTypeList.add("Small Claims")
      callTypeList.add("Social Media")
      callTypeList.add("Subpoena")
      callTypeList.add("Substance Abuse")
      callTypeList.add("Supervised Neglect")
      callTypeList.add("Surveillance Cameras")
      callTypeList.add("TMJ/TMD")
      callTypeList.add("Treating Staff")
      callTypeList.add("Veneers")
      callTypeList.add("Wrong Treatment Performed")
      callTypeList.add("3rd Party Payers")
      callTypeList.add("Other")

    }
    else if(claim.Type_TDIC == ClaimType_TDIC.TC_PROPERTY){
      callTypeList.add("Coverage Questions")
      callTypeList.add("Embezzlement")
      callTypeList.add("Equipment Breakdown")
      callTypeList.add("Identity Theft")
      callTypeList.add("Patient Injured")
      callTypeList.add("Theft")
      callTypeList.add("Vandalism")
      callTypeList.add("Other")

    }
    else if(claim.Type_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES){
      callTypeList.add("Arbitration")
      callTypeList.add("Associate")
      callTypeList.add("Contract Review")
      callTypeList.add("Coverage Questions")
      callTypeList.add("Disability/Leave")
      callTypeList.add("Discrimination")
      callTypeList.add("Employee Manual")
      callTypeList.add("Hostile Work Environment")
      callTypeList.add("Independent Contractor")
      callTypeList.add("Interview/Hire")
      callTypeList.add("Licensure")
      callTypeList.add("Management Issues")
      callTypeList.add("OSHA")
      callTypeList.add("Pregnancy")
      callTypeList.add("Termination")
      callTypeList.add("Sexual Harassment")
      callTypeList.add("Substance Abuse")
      callTypeList.add("Unemployment/EDD")
      callTypeList.add("Wage and Hour")
      callTypeList.add("Workers’ Compensation")
      callTypeList.add("Resignation")
      callTypeList.add("Wrongful Termination")
      callTypeList.add("Other")

    }
    else if(claim.Type_TDIC == ClaimType_TDIC.TC_WORKERSCOMPENSATION){
      callTypeList.add("Reasonable accommodation")
      callTypeList.add("Disability leave")
      callTypeList.add("Extending leave")
      callTypeList.add("Interactive process")
      callTypeList.add("MD note")
      callTypeList.add("Release back to work")
      callTypeList.add("Modified/restricted duties")
      callTypeList.add("Injury log")
      callTypeList.add("Referral to MD/lab")
      callTypeList.add("Post exposure protocol")
      callTypeList.add("Reporting injury ")
      callTypeList.add("Needle stick")
      callTypeList.add("Strain/sprain")
      callTypeList.add("Discrimination")
      callTypeList.add("Subpoena")
      callTypeList.add("Other")

    }
    return callTypeList
  }

  /**
   * create by: SanjanaS
   * @description: GCC-6- FNOL - Create an unverified Policy for Risk Management Incidents
   * method returns a list of values for Claim Type
   * @create time: 9:13 PM 7/17/2019
    * @param policyType PolicyType
   * @param claim Claim
   * @return: java.util.List<ClaimType_TDIC>
   */

  function getClaimType(policyType: PolicyType, claim: Claim): List<ClaimType_TDIC> {
    var claimTypeList : List<ClaimType_TDIC> = {}
    if (policyType != null) {
      if (policyType == PolicyType.TC_PROF_LIABILITY) {
        return ClaimType_TDIC.TF_PROFLIABTYPE.getTypeKeys()
      } else {
        if (policyType == PolicyType.TC_COMMERCIALPROPERTY) {
          claimTypeList.add(ClaimType_TDIC.TC_PROPERTY)
        }
        else if(policyType == PolicyType.TC_WC7WORKERSCOMP){
          claimTypeList.add(ClaimType_TDIC.TC_WORKERSCOMPENSATION)
        }
        return claimTypeList
      }
    }
    else return ClaimType_TDIC.getTypeKeys(false)
  }

  /**
   * create by: SanjanaS
   * @description: method to make Report Date mandatory based on the Claim Type.
   * @create time: 9:10 PM 7/17/2019
    * @param claim Claim
   * @return: Boolean
   */

  function setReportDate(policySearchCriteria: PolicySearchCriteria): Boolean {
    if (policySearchCriteria.ClaimType_TDIC != null) {
      if (policySearchCriteria.ClaimType_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY || policySearchCriteria.ClaimType_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE
          || policySearchCriteria.ClaimType_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES || policySearchCriteria.ClaimType_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY) {
        return true
      } else {
        return false
      }
    }
    return false
  }

  /**
   * create by: SanjanaS
   * @description: method that returns claim status
   * @create time: 3:27 PM 8/19/2019
    * @param claim Claim
   * @return: java.lang.String
   */
  function getClaimStatus(claim: Claim): String {
    var status = claim.ClaimStatus_TDIC
    if(claim.State != null){
      if(status != null and status == "Reopened"){
        status = "Reopened"
      }
      if(claim.State == ClaimState.TC_DRAFT){
        status = "Draft"
      }
      else if(claim.State == ClaimState.TC_OPEN and status != "Reopened"){
        status = "Open"
      }
      else if(claim.State == ClaimState.TC_CLOSED){
        status = "Closed"
      }
      else if(claim.State == ClaimState.TC_ARCHIVED){
        status = "Archived"
      }
    }
    return status
  }

  /**
   * create by: SanjanaS
   * @description: method that returns Claimant's name
   * @create time: 4:34 PM 8/23/2019
    * @param claim Claim
   * @return: java.lang.String
   */
  function getClaimant(claim: Claim): String {
    if(!claim.Policy.Verified){
      return claim.reporter.DisplayName
    }else if(claim.ClaimantDenorm.DisplayName != null){
      return claim.ClaimantDenorm.DisplayName
    }
    else return this.getContacts().firstWhere(\elt -> elt.Roles.hasMatch(\elt1 -> elt1.Role == ContactRole.TC_INJURED or elt1.Role == ContactRole.TC_CLAIMANT)) != null ?
          this.getContacts().firstWhere(\elt -> elt.Roles.hasMatch(\elt1 -> elt1.Role == ContactRole.TC_INJURED or elt1.Role == ContactRole.TC_CLAIMANT))?.DisplayName :""
  }

  /*
   * create by: SanjanaS
   * @description: property to get affected party contact
   * @create time: 8:44 PM 9/10/2019
   */

  property get affectedparty() : entity.Person {
    return this.getContactByRole(ContactRole.TC_AFFECTEDPARTY_TDIC) as entity.Person
  }

  property set affectedparty(value : entity.Person) {
    this.setContactByRole(ContactRole.TC_AFFECTEDPARTY_TDIC, value)
  }

  /*
   * create by: SanjanaS
   * @description: method to create subrogation activity
   * @create time: 9:45 PM 9/17/2019
   */
  function createSubrogationActivity(){
    if(this.Policy.Verified and this.FaultRating == FaultRating.TC_THIRDPARTY and
        (!this.Activities.hasMatch(\ a -> a.ActivityPattern.Code == "subro_responsible_party_added"))){
      var pattern = ActivityPattern.finder.getActivityPatternByCode("subro_responsible_party_added")
      var activity = this.createActivity(null, pattern, pattern.Subject, pattern.Description, Priority.TC_NORMAL, false, null, null)
      activity.assign(this.AssignedGroup, this.AssignedUser)
    }
  }

  /*
   * create by: SanjanaS
   * @description: method to set the visibility of Property Damage.
   * @create time: 4:58 PM 9/17/2019
   * @return: java.lang.boolean
   */
  function setPropertyVisibility(): Boolean{
    if(this.Type_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES ||
        this.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY ||
        this.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE ||
        this.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY){
      return false
    }
    else {
      return true
    }
  }

  /*
   * create by: SanjanaS
   * @description: method to set visibility of Injury Incident
   * @create time: 5:09 PM 9/17/2019
    * @param null
   * @return:
   */
  function setInjuryVisibility(): Boolean {
    if (this.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE ||
        this.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY) {
      return false
    }
    return true
  }
  /*
   * create by: SanjanaS
   * @description: method to validate Job Title is not null
   * GCC-533-Reported by 'View Contact Details' occupation is not blocking from advancing to next step
   * @create time: 6:53 PM 9/9/2019
   */
  function validateJobTitle(){
    if(this.LossType == LossType.TC_WC7 and (this.reporter as Person).Occupation == null){
      throw new Exception("Job Title cannot be empty. Please edit the contact to fill it.")
    }
  }

  /**
   * create by: SanjanaS
   * @description: method to reset reportability on checks
   * @create time: 5:54 PM 10/15/2019
   * @param null
   */

  function resetCheckReportability(check: Check){
    this.Checks.each(\chk -> {
      if(chk == check)
      chk.Reportability = null})
  }

  /*
* create by: NattuduraiK
* @description: method to check the claim is Workcomp or not
* @create time: 5:09 PM 10/22/2019
* @param null
* @return:Boolean
*/
  function isWorkCompClaim() : Boolean{
    return this.Type_TDIC == ClaimType_TDIC.TC_WORKERSCOMPENSATION
  }

  /**
   * @param claim
   * @description: method to set a Original claim CloseDate when re-closing  a calim using either IssuePayment or PaymentComplete ClaimClosedOutcome options.
   */
  function getClaimOrginalClosedDate(claim : Claim) {
    if (!claim.isWorkCompClaim()) {
      if (claim.ReopenedReason != null or (claim.ClosedOutcome == ClaimClosedOutcomeType.TC_ISSUEPAYMENT_TDIC or claim.ClosedOutcome == ClaimClosedOutcomeType.TC_PAYMENTSCOMPLETE)) {
        claim.CloseDate = claim.getOriginalValue("CloseDate") as Date
      }
    }
  }

  /**
   * @param claim
   * @return Claims closing outcomes warning Error messages
   * @description: method to validate Claim Closed Outcome Type Values based on the ClaimClosedOutcome Type selection
   */
  function validateClaimClosedOutcomeTypes(claim : Claim) {
    var filingDate : Date
    var arbitrationDate : Date
    var smallClaimHearingDate : Date
    var litigationResolution : ResolutionType
    var totalExpenseAmount = FinancialsCalculationUtil.getTotalPayments().getAmount(this, CostType.TC_DCCEXPENSE)
    var totalIndemnityAmount = FinancialsCalculationUtil.getTotalPayments().getAmount(this, CostType.TC_CLAIMCOST)
    var zeroCurrencyAmount = new CurrencyAmount("0.00", Currency.TC_USD)
    if (claim.Matters != null) {
      claim.Matters.each(\elt -> {
        if (elt.MatterType == MatterType.TC_LAWSUIT || elt.MatterType == MatterType.TC_ARBITRATION || elt.MatterType == MatterType.TC_HEARING || elt.MatterType == MatterType.TC_GENERAL || elt.MatterType == MatterType.TC_MEDIATION) {
          if (elt.FileDate != null) {
            filingDate = elt.FileDate
          }
          if (elt.ArbitrationDate != null) {
            arbitrationDate = elt.ArbitrationDate
          }
          if (elt.Resolution != null) {
            litigationResolution = elt.Resolution
          }
        } else if (elt.MatterType == MatterType.TC_SMALLCLAIMS_TDIC) {
          if (elt.HearingDate != null) {
            smallClaimHearingDate = elt.HearingDate
          }
        }
      })
    }
    var closedOutcome = claim.ClosedOutcome
    if ((closedOutcome == ClaimClosedOutcomeType.TC_ARBITRATIONDEFENSE_TDIC or closedOutcome == ClaimClosedOutcomeType.TC_ARBITRATIONPLAINTIFF_TDIC) and arbitrationDate == null) {
      LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("TDIC.Claim.ClosedReason.Rules.Arbitration.Validation.Error"))
    } else if ((closedOutcome == ClaimClosedOutcomeType.TC_CLOSEDNOLAWSUIT_TDIC or closedOutcome == ClaimClosedOutcomeType.TC_SETTLEDNOLAWSUIT_TDIC) and ((totalIndemnityAmount == null or totalIndemnityAmount == zeroCurrencyAmount) or filingDate != null)) {
      LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("TDIC.Claim.ClosedReason.Rules.NoLawSuit.Validation.Error"))
    } else if ((closedOutcome == ClaimClosedOutcomeType.TC_DEFENSEVERDICT_TDIC or closedOutcome == ClaimClosedOutcomeType.TC_DISMISSED_TDIC) and (litigationResolution == null or filingDate == null)) {
      LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("TDIC.Claim.ClosedReason.Rules.Defense.Validation.Error"))
    } else if ((closedOutcome == ClaimClosedOutcomeType.TC_INVALIDCLAIM_TDIC and totalIndemnityAmount != null) and (closedOutcome == ClaimClosedOutcomeType.TC_INVALIDCLAIM_TDIC and totalIndemnityAmount != zeroCurrencyAmount)) {
      LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("TDIC.Claim.ClosedReason.Rules.InValidClaim.Validation.Error"))
    } else if ((closedOutcome == ClaimClosedOutcomeType.TC_ISSUEPAYMENT_TDIC or closedOutcome == ClaimClosedOutcomeType.TC_PAYMENTSCOMPLETE) and (totalExpenseAmount == null or totalExpenseAmount == zeroCurrencyAmount)) {
      if (!claim.isWorkCompClaim()) {
        LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("TDIC.Claim.ClosedReason.Rules.IssuePayments.Validation.Error"))
      }
    } else if (closedOutcome == ClaimClosedOutcomeType.TC_JUDGEMENT_TDIC and filingDate == null) {
      LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("TDIC.Claim.ClosedReason.Rules.Judgement.Validation.Error"))
    } else if ((closedOutcome == ClaimClosedOutcomeType.TC_SETTLEDDURINGTRIAL_TDIC or closedOutcome == ClaimClosedOutcomeType.TC_SETTLEDPRIORTOTRIAL_TDIC) and ((totalIndemnityAmount == null or totalIndemnityAmount == zeroCurrencyAmount) or filingDate == null)) {
      LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("TDIC.Claim.ClosedReason.Rules.Settled.Validation.Error"))
    } else if ((closedOutcome == ClaimClosedOutcomeType.TC_SMALLCLAIMSDEFENSE_TDIC or closedOutcome == ClaimClosedOutcomeType.TC_SMALLCLAIMSPLAINTIFF_TDIC) and smallClaimHearingDate == null) {
      LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("TDIC.Claim.ClosedReason.Rules.SmallClaim.Validation.Error"))
    }

  }

  /**
   * create by: SanjanaS
   * @description: method to validate the reserve amount for EPL and CL coverges
   * @create time: 4:51 PM 11/3/2019
   * @param null
   * @return: null
   */
  function validateReserveAmount(currentClaim: Claim) {
    var claims = Query.make(Claim)
        .compare("LossDate", Relop.GreaterThanOrEquals, this.Policy.EffectiveDate.trimToMidnight())
        .compare("LossDate", Relop.LessThan, this.Policy.ExpirationDate.addDays(1).trimToMidnight())
        .join("Policy", entity.Policy, "ID")
        .compare(Policy#PolicyNumber, Relop.Equals, this.Policy.PolicyNumber).select().toList()
    var amountD : BigDecimal =0
    var amount2 : BigDecimal =0
    var amount4 : BigDecimal =0
    var amount5 : BigDecimal =0
    var amount6 : BigDecimal =0
    var covD : PolicyCoverage
    var cov2 : PolicyCoverage
    var cov4 : PolicyCoverage
    var cov5 : PolicyCoverage
    var cov6 : PolicyCoverage
    var errorMsg = new StringWriter()
    if(claims.Count != 0){
      for(claim in claims) {
        if (claim != currentClaim) {
          var reserves = claim.ReserveLines
          for (reserve in reserves) {
            var covType = reserve.Exposure.PrimaryCoverage
            if (covType != null) {
              if (covType.DisplayName == CoverageType.TC_GLDENTALEMPPRACLIABCOV_TDIC.DisplayName) {
                amountD = getReserveAmount(covType, reserve, amountD)
                covD = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
              }
              if (covType.DisplayName == CoverageType.TC_GLCYBCOV2_TDIC.DisplayName) {
                amount2 = getReserveAmount(covType, reserve, amount2)
                cov2 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
              }
              if (covType.DisplayName == CoverageType.TC_GLCYBCOV4_TDIC.DisplayName) {
                amount4 = getReserveAmount(covType, reserve, amount4)
                cov4 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
              }
              if (covType.DisplayName == CoverageType.TC_GLCYBCOV5_TDIC.DisplayName) {
                amount5 = getReserveAmount(covType, reserve, amount5)
                cov5 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
              }
              if (covType.DisplayName == CoverageType.TC_GLCYBCOV6_TDIC.DisplayName) {
                amount6 = getReserveAmount(covType, reserve, amount6)
                cov6 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
              }
            }
          }
        } else if (claim == currentClaim) {
          var reserves = currentClaim.ReserveLines
          for (reserve in reserves) {
            var covType = reserve.Exposure.PrimaryCoverage
            if (covType != null) {
              if (covType.DisplayName == CoverageType.TC_GLDENTALEMPPRACLIABCOV_TDIC.DisplayName) {
                amountD = getReserveAmount(covType, reserve, amountD)
                covD = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
              }
              if (covType.DisplayName == CoverageType.TC_GLCYBCOV2_TDIC.DisplayName) {
                amount2 = getReserveAmount(covType, reserve, amount2)
                cov2 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
              }
              if (covType.DisplayName == CoverageType.TC_GLCYBCOV4_TDIC.DisplayName) {
                amount4 = getReserveAmount(covType, reserve, amount4)
                cov4 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
              }
              if (covType.DisplayName == CoverageType.TC_GLCYBCOV5_TDIC.DisplayName) {
                amount5 = getReserveAmount(covType, reserve, amount5)
                cov5 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
              }
              if (covType.DisplayName == CoverageType.TC_GLCYBCOV6_TDIC.DisplayName) {
                amount6 = getReserveAmount(covType, reserve, amount6)
                cov6 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
              }
            }
          }
        }
      }
    }
    else if(currentClaim != null){
      var reserves = currentClaim.ReserveLines
      for (reserve in reserves) {
        var covType = reserve.Exposure.PrimaryCoverage
        if (covType != null) {
          if (covType.DisplayName == CoverageType.TC_GLDENTALEMPPRACLIABCOV_TDIC.DisplayName) {
            amountD = getReserveAmount(covType, reserve, amountD)
            covD = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
          }
          if (covType.DisplayName == CoverageType.TC_GLCYBCOV2_TDIC.DisplayName) {
            amount2 = getReserveAmount(covType, reserve, amount2)
            cov2 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
          }
          if (covType.DisplayName == CoverageType.TC_GLCYBCOV4_TDIC.DisplayName) {
            amount4 = getReserveAmount(covType, reserve, amount4)
            cov4 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
          }
          if (covType.DisplayName == CoverageType.TC_GLCYBCOV5_TDIC.DisplayName) {
            amount5 = getReserveAmount(covType, reserve, amount5)
            cov5 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
          }
          if (covType.DisplayName == CoverageType.TC_GLCYBCOV6_TDIC.DisplayName) {
            amount6 = getReserveAmount(covType, reserve, amount6)
            cov6 = this.Policy.Coverages.firstWhere(\elt -> elt.Type == covType)
          }
        }
      }
    }

    if(amountD != null and covD.ExposureLimit != null and amountD > covD.ExposureLimit.Amount){
      errorMsg.append(DisplayKey.get("TDIC.Claim.Reserve.Validation", covD.Type, amountD.ofCurrency(covD.ExposureLimit.Currency), covD.ExposureLimit) + "\n")
    }
    if(amount2 != null and cov2.ExposureLimit != null and amount2 > cov2.ExposureLimit.Amount){
      errorMsg.append(DisplayKey.get("TDIC.Claim.Reserve.Validation", cov2.Type, amount2.ofCurrency(cov2.ExposureLimit.Currency), cov2.ExposureLimit)+"\n")
    }
    if(amount4 != null and cov4.ExposureLimit != null and amount4 > cov4.ExposureLimit.Amount){
      errorMsg.append(DisplayKey.get("TDIC.Claim.Reserve.Validation", cov4.Type, amount4.ofCurrency(cov4.ExposureLimit.Currency), cov4.ExposureLimit)+"\n")
    }
    if(amount5 != null and cov5.ExposureLimit != null and amount5 > cov5.ExposureLimit.Amount){
      errorMsg.append(DisplayKey.get("TDIC.Claim.Reserve.Validation", cov5.Type, amount5.ofCurrency(cov5.ExposureLimit.Currency), cov5.ExposureLimit)+"\n")
    }
    if(amount6 != null and cov6.ExposureLimit != null and amount6 > cov6.ExposureLimit.Amount){
      errorMsg.append(DisplayKey.get("TDIC.Claim.Reserve.Validation", cov6.Type, amount6.ofCurrency(cov6.ExposureLimit.Currency), cov6.ExposureLimit)+"\n")
    }
    if(errorMsg.toString() != null and errorMsg.toString() != ""){
      throw new DisplayableException(errorMsg.toString())
    }
  }

  /**
   * create by: SanjanaS
   * @description: method to return Reserve amount on a particular Reserve Line
   * @create time: 4:52 PM 11/3/2019
   * @param covType CoverageType, reserve ReserveLine, amount BigDecimal
   * @return: java.math.BigDecimal
   */
  function getReserveAmount(covType: CoverageType, reserve: ReserveLine, amount: BigDecimal): BigDecimal{
    /**
     * GWPS-2157, combine limit of reserve in claim & coverage level
     * indemnity and expenses are a combined single limit for EPL (Coverage D), Cyber (Coverage 4, 5 and 6 only) claims.
     */
    var claimLevelReserves : BigDecimal
    if(this.Policy.Coverages.hasMatch(\cov -> cov.Type == covType) or
        (this.Policy.Properties.hasMatch(\prop -> prop.Coverages.hasMatch(\cov1 -> cov1.Type == covType)))) {
      if ((this.Type_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES && covType == CoverageType.TC_GLDENTALEMPPRACLIABCOV_TDIC) or
          (this.Type_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY && (covType == CoverageType.TC_GLCYBCOV4_TDIC or covType == CoverageType.TC_GLCYBCOV5_TDIC or
              covType == CoverageType.TC_GLCYBCOV6_TDIC))) {
        claimLevelReserves = 0
        reserve.Transactions.each(\elt -> {
          if (elt.Subtype == Transaction.TC_RESERVE) {
            amount += elt.TransactionAmount.Amount
          }
        })
        var claimLevel = this.ReserveLines.where(\reserveLine -> reserveLine.Exposure == null)
        claimLevel.each(\claimReserveLine -> claimReserveLine.Transactions.each(\tran -> {
          if (tran.Subtype == Transaction.TC_RESERVE)
            claimLevelReserves += tran.TransactionAmount.Amount
        }
        ))
        amount = amount + claimLevelReserves
      }
      else {
        reserve.Transactions.each(\elt -> {
          amount += elt.TransactionAmount.Amount
        })
      }
    }
    return amount
  }



  /**
   * create by: NattuduraiK
   * @description: method to return claim Executive(Bradley Reager) GroupUser Entity
   * @create time: 4:52 PM 11/4/2019
   * @param null
   * @return: GroupUser
   */
  function getClaimExecutive(): GroupUser{
    return Query.make(GroupUser)
        .join(GroupUser#User)
        .compare(User#PublicID, Relop.Equals, "tdic_user:10015").select().first()

  }

  /**
   * create by: NattuduraiK
   * @description: method to check the siu activity need to be  created or not by siu score and claim type
   * @create time: 4:52 PM 12/9/2019
   * @param null
   * @return: Boolean
   */
  function createSIUescalatian(): Boolean{

    var score = util.SIUTotalScore.getSIUTotalScore(this)
    if (checkSIUQuestionScoreChanged() != true)
      return false

    if(this.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY and score >= 5)
    return true
    else if ((this.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY  or this.Type_TDIC == ClaimType_TDIC.TC_BUSINESSLIABILITY) and score >= 5)
      return  true
    else if((this.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY  or this.Type_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY)and score >= 2)
      return true
    else if(this.Type_TDIC == ClaimType_TDIC.TC_PROPERTY and score >= 3)
      return true
      return  false
  }

  /**
   * create by: NattuduraiK
   * @description: method to  update the SIU Question score and check if the SIU question score is modified or not
   * @create time: 4:52 PM 12/16/2019
   * @param null
   * @return: Boolean
   */

  function checkSIUQuestionScoreChanged() : boolean
  {
    var FraudQS = this.getQuestionSets(util.QuestionUtils.getAppropriateQuestionSet(this))
    var FraudQSScore = util.QuestionUtils.getQuestionSetTotalScore(this, FraudQS);
    this.SIUQuestionScore_TDIC = FraudQSScore
    if(this.getOriginalValue(Claim#SIUQuestionScore_TDIC) != FraudQSScore )
      return true
    return false

  }

  property get claimant_TDIC() : String{
    return getClaimant(this)
  }

  property get claimantaddress_TDIC() : Address{
    var addr : Address
    if(this.claimant.PrimaryAddress == null){
      addr = this.getContacts().firstWhere(\elt -> elt.Roles.hasMatch(\elt1 -> elt1.Role == ContactRole.TC_INJURED or elt1.Role == ContactRole.TC_CLAIMANT))?.AddressOwner.Address
    }
    return addr
  }

  property get dateofbirth_TDIC() : Date{
    if(this.claimant != null){
      return (this.claimant as Person).DateOfBirth
    }
    else {
      return this.getContacts().firstWhere(\elt -> elt.Roles.hasMatch(\elt1 -> elt1.Role == ContactRole.TC_INJURED or elt1.Role == ContactRole.TC_CLAIMANT))?.Person.DateOfBirth
    }
  }

  property get EventName_TDIC(): String {
    var eventName_TDIC : String
    var amount = FinancialsCalculationUtil.getTotalPayments().getAmount(this)
    if(this.State == ClaimState.TC_CLOSED && this.Type_TDIC ==ClaimType_TDIC.TC_PROFESSIONALLIABILITY){
      if(amount==0 || amount==null){
        eventName_TDIC = "PLNPCLOSE"
      }
      else{
        eventName_TDIC = "PLCLOSE"
      }
    }
    else if(this.State == ClaimState.TC_CLOSED && this.Type_TDIC ==ClaimType_TDIC.TC_PROPERTY){
      eventName_TDIC = "CPCLOSE"
    }
    else if(this.State == ClaimState.TC_OPEN && this.Type_TDIC ==ClaimType_TDIC.TC_PROPERTY){
      eventName_TDIC = "CPFNOL"
    }
    else if(this.State == ClaimState.TC_OPEN && this.Type_TDIC ==ClaimType_TDIC.TC_WORKERSCOMPENSATION){
      eventName_TDIC = "FNOL"
    }
    else if(this.State == ClaimState.TC_OPEN && this.CoverageInQuestion == true &&
           (this.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY or
               this.Type_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES or
               this.Type_TDIC == ClaimType_TDIC.TC_BUSINESSLIABILITY)){
      eventName_TDIC = "PLFNOL"
    }
    else if(this.State == ClaimState.TC_OPEN && this.FirstNoticeSuit == true &&
        (this.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY or
            this.Type_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES or
            this.Type_TDIC == ClaimType_TDIC.TC_BUSINESSLIABILITY)){
      eventName_TDIC = "PLNOIFNOL"
    }
    return eventName_TDIC
  }

  property get claimantSSN_TDIC() : String{
    if(this.claimant != null){
      return (this.claimant as Person).SSNOfficialID
    }
    else {
      return this.getContacts().firstWhere(\elt -> elt.Roles.hasMatch(\elt1 -> elt1.Role == ContactRole.TC_INJURED or elt1.Role == ContactRole.TC_CLAIMANT))?.Person.TaxID
    }
  }

  property get claimantGender_TDIC() : String{
    if(this.claimant != null){
      return (this.claimant as Person).Gender.DisplayName
    }
    else {
      return this.getContacts().firstWhere(\elt -> elt.Roles.hasMatch(\elt1 -> elt1.Role == ContactRole.TC_INJURED or elt1.Role == ContactRole.TC_CLAIMANT))?.Person.Gender.DisplayName
    }
  }

  property get claimantMaritalStatus_TDIC() : String{
    if(this.claimant != null){
      return (this.claimant as Person).MaritalStatus.DisplayName
    }
    else {
      return this.getContacts().firstWhere(\elt -> elt.Roles.hasMatch(\elt1 -> elt1.Role == ContactRole.TC_INJURED or elt1.Role == ContactRole.TC_CLAIMANT))?.Person.TaxFilingStatus.DisplayName
    }
  }

  property get claimantOccupation_TDIC() : String{
    if(this.claimant != null){
      return (this.claimant as Person).Occupation
    }
    else {
      return this.getContacts().firstWhere(\elt -> elt.Roles.hasMatch(\elt1 -> elt1.Role == ContactRole.TC_INJURED or elt1.Role == ContactRole.TC_CLAIMANT))?.Person.Occupation
    }
  }

  property get deductible_TDIC() : CurrencyAmount {
    return this.Policy.Properties*.Coverages*.DeductibleAmount.firstWhere(\elt -> elt.Amount != null)
  }

  property get TotalPaymentInclusingPending_TDIC():BigDecimal{
    var totalAmt = FinancialsCalculationUtil.getTotalPaymentsIncludingPending().getAmount(this).Amount
    if(totalAmt != null and totalAmt > 0)
      return totalAmt
    return 0
  }

  property get OnbasePolicyNumber_TDIC() : String {

    if(this.LoadCommandID != null){
      // this.Policy.PolicyNumber = "CA 061939-5-C3"  legacyPolcyNum = "0619395"
      var legacyPolicyNum : String = ""
      legacyPolicyNum = this.Policy.PolicyNumber?.substring(3, 11)?.replace("-","")
      return legacyPolicyNum
    } else {
      return this.Policy.PolicyNumber
    }
  }

  property get isSubrogationEscExists() : boolean {
    if (this.SubrogationSummary.EscalateSubro == true and this.SubrogationSummary.SubroReferralDate != null) {
      return true
    }
    return false
  }

  function addWarningMsgOnClickOnVoidOrStop(){
    LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("TDIC.Web.Policy.VoidOrStop.WarningMessage"))
  }

  function validateVendorNumber(check : Check) {
    if(check.Claim.LossType != LossType.TC_WC7) {
      check.Payees.each(\elt -> {
        if (elt.ClaimContact.Contact.VendorNumber == "99" or elt.ClaimContact.Contact.VendorNumber == null) {
          throw new DisplayableException(DisplayKey.get("TDIC.Rules.Validation.Transaction.Check.Vendor.Error"))
        }
      })
    }
  }

}
