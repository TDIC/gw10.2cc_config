package tdic.cc.config.reserves

uses gw.api.financials.CurrencyAmount
uses gw.api.financials.ReserveWizardHelper
uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
uses gw.financials.CoverageLimitRulesUtil

uses java.math.BigDecimal
uses java.util.List

/**
 * US1028, DE65
 * 01/05/2015 robk
 * <p>
 * A Helper class for creating reserves.
 */
class TDIC_ReservesHelper {

  public static function getValidCostCategories(aTransaction : Transaction, aWizardHelper : ReserveWizardHelper) : List<typekey.CostCategory> {

    var validCostCategories = new java.util.ArrayList<CostCategory>()
    if (aTransaction.Subtype == typekey.Transaction.TC_RESERVE) {
      if (aTransaction.Claim.isWorkCompClaim() and aWizardHelper != null) {
        var validCostCategoriesByCoverage = aWizardHelper.getValidCostCategories(aTransaction)

        if (aTransaction.Exposure != null) {
          validCostCategories.addAll(validCostCategoriesByCoverage.where(\cc -> cc.hasCategory(aTransaction.Exposure.CoverageSubType)))
        } else {
          validCostCategories.addAll(validCostCategoriesByCoverage)
        }
        return validCostCategories
      }
      if ((aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES or aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE) and (aTransaction.Exposure == null or aTransaction.CostType == null))
        return validCostCategories
      var validCostCategoriesByCoverage = CostCategory.getTypeKeys(false)
      if (aTransaction.Exposure != null) {
        if (aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY) {
          validCostCategories.addAll(validCostCategoriesByCoverage.where(\cc -> (((aTransaction.CostType != null and cc.hasCategory(aTransaction.CostType)) or (aTransaction.CostType == null))
              and cc.hasCategory(aTransaction.Claim.Type_TDIC)
              and cc.hasCategory(aTransaction.Exposure.PrimaryCoverage)
              and cc != CostCategory.TC_OTHER)))
        } else if (aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY and aTransaction.CostType != CostType.TC_CLAIMCOST) {
          validCostCategories.addAll(validCostCategoriesByCoverage.where(\cc -> (((aTransaction.CostType != null and cc.hasCategory(aTransaction.CostType)) or (aTransaction.CostType == null))
              and cc.hasCategory(aTransaction.Claim.Type_TDIC)
              and cc.hasCategory(aTransaction.Exposure.PrimaryCoverage))))
        } else if (aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES and aTransaction.CostType != CostType.TC_CLAIMCOST) {
          validCostCategories.addAll(validCostCategoriesByCoverage.where(\cc -> (((aTransaction.CostType != null and cc.hasCategory(aTransaction.CostType)) or (aTransaction.CostType == null))
              and cc.hasCategory(aTransaction.Claim.Type_TDIC)
              and cc.hasCategory(aTransaction.Exposure.PrimaryCoverage))))
        } else if (aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE and aTransaction.CostType != null) {
          validCostCategories.addAll(validCostCategoriesByCoverage.where(\cc -> (((aTransaction.CostType != null and cc.hasCategory(aTransaction.CostType)) or (aTransaction.CostType == null))
              and cc.hasCategory(aTransaction.Claim.Type_TDIC)
              and cc.hasCategory(aTransaction.Exposure.PrimaryCoverage)
              and cc != CostCategory.TC_OTHER
              and cc != CostCategory.TC_UNSPECIFIED)))
        } else if (aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY and aTransaction.Exposure != null and aTransaction.CostType != CostType.TC_CLAIMCOST) {
          validCostCategories.addAll(validCostCategoriesByCoverage.where(\cc -> (((aTransaction.CostType != null and cc.hasCategory(aTransaction.CostType)) or (aTransaction.CostType == null))
              and cc.hasCategory(aTransaction.Claim.Type_TDIC)
              and cc.hasCategory(aTransaction.Exposure.PrimaryCoverage)
              and cc != CostCategory.TC_UNSPECIFIED
              and cc != CostCategory.TC_OTHER)))
        } else {
          // validCostCategories.addAll(validCostCategoriesByCoverage.where( \ cc -> cc.hasCategory(aTransaction.Exposure.CoverageSubType)))
          validCostCategories.addAll(validCostCategoriesByCoverage.where(\cc -> (((aTransaction.CostType != null and cc.hasCategory(aTransaction.CostType)) or (aTransaction.CostType == null))
              and cc.hasCategory(aTransaction.Claim.Type_TDIC)
              and cc.hasCategory(aTransaction.Exposure.PrimaryCoverage)
              and cc != CostCategory.TC_OTHER)))
        }
      } else if (aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY) {
        if (aTransaction.Exposure == null) {
          return null
        }
      } else {
        validCostCategories.addAll(validCostCategoriesByCoverage.where(\cc -> (((aTransaction.CostType != null and cc.hasCategory(aTransaction.CostType)) or (aTransaction.CostType == null))
            and cc.hasCategory(aTransaction.Claim.Type_TDIC))))
      }
    }
    return validCostCategories
  }

  /**
   * create by: SanjanaS
   *
   * @param transaction Transaction, wizardHelper ReserveWizardHelper
   * @description: method to populate Cost Type values
   * @create time: 3:32 PM 11/6/2019
   * @return: java.util.List<typekey.CostType>
   */
  public static function getCostTypes(transaction : Transaction, wizardHelper : ReserveWizardHelper) : List<typekey.CostType> {
    var costTypes = new ArrayList<CostType>()
    var allCostTypes = CostType.getTypeKeys(false)
    if (transaction.Claim.isWorkCompClaim()) {
      costTypes.addAll(wizardHelper.getValidCostTypes(transaction))
      return costTypes
    }

    if ((transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES or transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE) and transaction.Exposure == null) {
      return costTypes
    }
    if (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE and transaction.Exposure != null) {
      costTypes.add(CostType.TC_DCCEXPENSE)
      return costTypes
    }

    if ((transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_PROPERTY ||
        transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY ||
        transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_BUSINESSLIABILITY ||
        transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY)) {
      if (transaction.Exposure.PrimaryCoverage != null) {
        costTypes.add(CostType.TC_CLAIMCOST)
      } else {
        costTypes.addAll(allCostTypes.where(\elt -> elt != CostType.TC_EXPENSES_TDIC and elt != CostType.TC_CLAIMCOST))
      }
      return costTypes
    }
    if (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY) {
      if (transaction.Exposure == null) {
        return null
      } else {
        costTypes.addAll(allCostTypes.where(\elt -> elt != CostType.TC_EXPENSES_TDIC))
      }
      return costTypes
    }
    if (transaction.Exposure.PrimaryCoverage == CoverageType.TC_GLCYBCOV1_TDIC ||
        transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE) {
      costTypes.addAll(allCostTypes.where(\elt -> elt != CostType.TC_CLAIMCOST and elt != CostType.TC_EXPENSES_TDIC))
      return costTypes
    }
    costTypes.addAll(allCostTypes.where(\elt -> elt != CostType.TC_EXPENSES_TDIC))
    return costTypes
  }

  /**
   * create by: SanjanaS
   *
   * @param aTransaction Transaction, WizardHelper gw.api.financials.RecoveryReserveWizardHelper
   * @description: method to get exposures added on the claim
   * @create time: 5:30 PM 11/8/2019
   * @return: java.util.List<Exposure>
   */
  public static function getExposures(aTransaction : Transaction, WizardHelper : gw.api.financials.ReserveWizardHelper) : List<Exposure> {
    if (aTransaction.Claim.isWorkCompClaim()) {
      return WizardHelper.getValidExposures(aTransaction)
    }
//    if(aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE){
//      var expo = aTransaction.Claim.Exposures.where(\elt -> elt.PrimaryCoverage != CoverageType.TC_GLREGULATORYAUTHCOV_TDIC)
//      return expo.toList()
//    }
    return aTransaction.Claim.Exposures.toList()
  }

  /**
   * create by: NattuduraiK
   *
   * @param aTransaction Transaction
   * @description: method to check the reserve limit should be restricted or not
   * @create time: 5:30 PM 12/17/2019
   * @return: Boolean
   */

  public static function isRestricReserveforTransaction(aTransaction : Transaction) : Boolean {
    if (aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_PROPERTY) {
      if (aTransaction.CostType == CostType.TC_CLAIMCOST
          and aTransaction.Exposure.PrimaryCoverage == CoverageType.TC_BOPBUILDINGCOV) {
        if ((aTransaction.Claim.Policy.Offerings_TDIC == Offering_TDIC.TC_LRP
            and aTransaction.CostCategory == CostCategory.TC_BOPOUTDOORPROPERTY_TDIC)) {
          return false
        }
      } else if (aTransaction.CostType == CostType.TC_CLAIMCOST
          and aTransaction.Exposure.PrimaryCoverage == CoverageType.TC_BOPPERSONALPROPCOV) {
        if (aTransaction.Claim.Policy.Offerings_TDIC == Offering_TDIC.TC_BOP
            and aTransaction.CostCategory == CostCategory.TC_BOPLOSSOFINCOMETDIC) {
          return false
        }
      }
    }
    if (aTransaction.Claim.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY and
        aTransaction.Exposure.CoverageSubType == CoverageSubtype.TC_GLIDTHEFTRECOVERYCOVFAMILY_TDIC) {
      return false
    }
    return true
  }


  public static function restrictCreatingReserve(transactionSet : TransactionSet) {

    var warning = false

    var costType = CostType.TC_CLAIMCOST
    var exposureSet = new java.util.HashSet<Exposure>()
    var cov :Coverage

    for (transaction in transactionSet.Transactions) {
      // checking the transaction is need to be restrict or not
      if (TDIC_ReservesHelper.isRestricReserveforTransaction(transaction) and transaction.Exposure != null) {
        var exposure = transaction.Exposure
        var exposureLimit : CurrencyAmount = null
        // exposure limit to validate the total reserve of the exposure
        if(exposure != null){
          cov = getCoverage(transaction)
        }
        if (cov != null){
          exposureLimit = cov.ExposureLimit
          if(cov.Type == CoverageType.TC_BOPORDLAW_TDIC or cov.Type == CoverageType.TC_GLCYBCOV1_TDIC or cov.Type == CoverageType.TC_GLCYBCOV2_TDIC
              or cov.Type == CoverageType.TC_GLCYBCOV4_TDIC){
            var covLimit = cov.CovTerms.firstWhere(\elt -> elt.DisplayName.containsIgnoreCase(transaction.CostCategory.DisplayName)).Value
            if(covLimit != null and !covLimit.isEmpty()){
              covLimit = covLimit.replace("$","")
              covLimit = covLimit.replace(",","")
              exposureLimit = new CurrencyAmount(covLimit,Currency.TC_USD)
            }
          }
        }
        if(exposureLimit == null and transaction?.CostCategory == CostCategory.TC_BOPRENTALINCOME_TDIC){
          exposureLimit = new CurrencyAmount(ScriptParameters.ReserveRestrictionAmount,Currency.TC_USD)
        }
        //  if no exposurelimit then it won't restrict the reserve
        if (exposureLimit != null) {
          var isSubCov : Boolean = true
          var isItFirst : Boolean = true
          if (transaction.Exposure.PrimaryCoverage == cov.Type)
            isSubCov = false

          //GWPS-1934 : Allowing user to increase CL reserves on exposure if Offerings are null
          if(transactionSet.Claim.Policy.Offerings_TDIC == null and transactionSet.Claim.Type_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY) {
            warning = true
          }
          var compareInLimitCurrency = CoverageLimitRulesUtil?.shouldCompareInReservingCurrency({exposure}, costType, exposureLimit.Currency)
          if (compareInLimitCurrency) {
            exposureLimit = exposureLimit.convert(transactionSet.Claim.Currency, java.math.RoundingMode.UP)
          }
          var amt : BigDecimal = 0
          var combined  = new CurrencyAmount(BigDecimal.ZERO)
          if (isSubCov) {
            var costcategory = transaction.CostCategory
            var combinedCalc = CoverageLimitRulesUtil.getCommittedAndFutureAndPendingApprovalPaymentsAndReserves()
                .withExposure(exposure)
                .withCostType(costType)
                .withCostCategory(costcategory)
            combined = CoverageLimitRulesUtil.getCalcValueForCompare(
                combinedCalc,
               Currency.TC_USD, compareInLimitCurrency)
          } else {
            if(not (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY and
                transaction.Exposure.CoverageSubType == CoverageSubtype.TC_GLIDTHEFTRECOVERYCOV_TDIC)){
              var validcostcatgories = getValidCostCategories(transaction, null)
              for (costcategory in validcostcatgories) {
                if (checkCostcategory(costcategory, transaction)) {
                  var combinedCalc = CoverageLimitRulesUtil.getCommittedAndFutureAndPendingApprovalPaymentsAndReserves()
                      .withExposure(exposure)
                      .withCostType(costType)
                      .withCostCategory(costcategory)
                  var calcualtingamount = CoverageLimitRulesUtil.getCalcValueForCompare(
                      combinedCalc,
                      Currency.TC_USD, compareInLimitCurrency)
                  if (isItFirst) {
                    combined = CoverageLimitRulesUtil.getCalcValueForCompare(
                        combinedCalc,
                        Currency.TC_USD, compareInLimitCurrency)
                    isItFirst = false
                  } else {
                    combined = combined + CoverageLimitRulesUtil.getCalcValueForCompare(
                        combinedCalc,
                        Currency.TC_USD, compareInLimitCurrency)
                  }
                }
              }
            }
          }
//          if (cov.Type == CoverageType.TC_BOPPOLLCLEANREMOVAL_TDIC) {
//            combined = transactionSet.Claim.calculateCombinedLimit(costType, exposure)
//          } else if (cov.Type == CoverageType.TC_BOPARSONREWARD_TDIC) {
//            combined = transactionSet.Claim.calculateCombinedLimit(costType, exposure)
//          } else if (cov.Type == CoverageType.TC_BOPEDPCOV_TDIC) {
//            combined = transactionSet.Claim.calculateCombinedLimit(costType, exposure)
//          }
          if ((combined == null or combined.Amount == 0.00) and exposure != null ) {
            if(not (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY and
                exposure.CoverageSubType == CoverageSubtype.TC_GLIDTHEFTRECOVERYCOV_TDIC)) {
              for (reserve in exposure.ReserveLines.where(\elt -> elt.CostType == transaction.CostType and elt.CostCategory == transaction.CostCategory)) {
                if (reserve.Exposure == exposure) {
                  for (trans in reserve.Transactions) {
                    amt += trans.Amount
                  }
                }
              }
            } else {
              var costTypes = CostType.getTypeKeys(false).where(\costTypeObj -> costTypeObj!=CostType.TC_EXPENSES_TDIC)
              for(costTypeObj in costTypes){
                for (reserve in exposure.ReserveLines.where(\elt -> elt.CostType == costTypeObj)) {
                  for (trans in reserve.Transactions) {
                    amt += trans.Amount
                  }
                }
              }
            }
            combined = amt.ofCurrency(exposureLimit.Currency)
          }

          if (cov.Type == CoverageType.TC_GLDENTISTPROFLIABCOV_TDIC) {
            combined = transactionSet.Claim.calculateCombinedLimit(costType, exposure,transaction.CostCategory)
            if(transaction typeis Reserve){
              combined=transaction.TransactionAmount+combined
            }
           }else  if (cov.Type == CoverageType.TC_BOPPOLLCLEANREMOVAL_TDIC) {
             combined = transactionSet.Claim.calculateCombinedLimit(costType, exposure,transaction.CostCategory)
            if(transaction typeis Reserve){
              combined=transaction.TransactionAmount+combined
            }
          } else if (cov.Type == CoverageType.TC_BOPARSONREWARD_TDIC) {
            combined = transactionSet.Claim.calculateCombinedLimit(costType, exposure,transaction.CostCategory)
            if(transaction typeis Reserve){
              combined=transaction.TransactionAmount+combined
            }
          } else if (cov.Type == CoverageType.TC_BOPEDPCOV_TDIC) {
            combined = transactionSet.Claim.calculateCombinedLimit(costType, exposure,transaction.CostCategory)
            if(transaction typeis Reserve){
              combined=transaction.TransactionAmount+combined
            }
          } else if(transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY and cov.Type == CoverageType.TC_GLIDTHEFTRECOVERYCOV_TDIC){
            var costTypes = CostType.getTypeKeys(false).where(\costTypeObj -> costTypeObj!=CostType.TC_EXPENSES_TDIC)
            for(costTypeObj in costTypes){
              var combinedAmountFromOthers = transactionSet.Claim.calculateCombinedLimit(costTypeObj, exposure)
              if(transaction typeis Reserve){
                combined=combined+combinedAmountFromOthers
              }
            }
          }
          if (combined > exposureLimit) {
            //If Warning is set to TRUE, throw a warning, but permit the transaction to go through if the user desires so
            if (warning == true) {
              transactionSet.reject(null, null, TC_PAYMENT,
                  DisplayKey.get("TDIC.Rules.Validation.Transaction.ReserveExceedsExposureLimitWarning",
                      exposure, CurrencyUtil.renderAsCurrency(combined),
                      CurrencyUtil.renderAsCurrency(exposureLimit)))
              break;
            } else {
              //If Warning is set to FALSE, block the transaction from going through
              transactionSet.reject(TC_PAYMENT,
                  DisplayKey.get("TDIC.Rules.Validation.Transaction.ReserveExceedsExposureLimitError",
                      exposure, CurrencyUtil.renderAsCurrency(combined),
                      CurrencyUtil.renderAsCurrency(exposureLimit)),
                  null, null)
              break;
            }
          }
        }
      }
    }

  }

  public static function getCoverage(transaction : Transaction) : Coverage {
    var cov : Coverage = null

    // getting the sub coverages based on the claim types
    if (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY) {
      if (transaction.CostCategory == CostCategory.TC_GLMEDPAYCOV_TDIC) {
        cov = getCoveragebyCoverageType(transaction, CoverageType.TC_GLMEDPAYCOV_TDIC)
      }
    } else if (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_BUSINESSLIABILITY) {
      if (transaction.CostCategory == CostCategory.TC_GLDENTISTPROFLIABCOV_TDIC) {
        cov = getCoveragebyCoverageType(transaction, CoverageType.TC_GLDENTALBUSINESSLIABCOV_TDIC)

      } else if (transaction.CostCategory == CostCategory.TC_GLDENTALEMPBENLIAB_TDIC) {
        cov = getCoveragebyCoverageType(transaction, CoverageType.TC_GLDENTALEMPBENLIAB_TDIC)

      } else if (transaction.CostCategory == CostCategory.TC_GLDENTALMEDWASTECOV_TDIC) {
        cov = getCoveragebyCoverageType(transaction, CoverageType.TC_GLDENTALMEDWASTECOV_TDIC)

      } else if (transaction.CostCategory == CostCategory.TC_GLMEDPAYCOV_TDIC or transaction.CostCategory == CostCategory.TC_PATIENTPROPERTY_TDIC) {
        cov = getCoveragebyCoverageType(transaction, CoverageType.TC_GLMEDPAYCOV_TDIC)
      }
    } else if (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY) {
      if (transaction.CostCategory == CostCategory.TC_BOPDENTALGENLIABILITYCOV_TDIC) {
        cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPDENTALGENLIABILITYCOV_TDIC)

      } else if (transaction.CostCategory == CostCategory.TC_BOPBUILDINGOWNERSLIABCOV_TDIC) {
        cov = getCoveragebyCoverageType(transaction, CoverageType.TC_GLDENTALEMPBENLIAB_TDIC)
      } else if (transaction.CostCategory == CostCategory.TC_BOPWASTOPGAP_TDIC) {
        cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPWASTOPGAP_TDIC)
      }
    } else if (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_PROPERTY) {
      if (transaction.Exposure.PrimaryCoverage == CoverageType.TC_BOPPERSONALPROPCOV) {
        if (transaction.CostCategory == CostCategory.TC_MONEYANDSECURITIES_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPMONEYSECCOV_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_GOLDANDOTHERPRECIOUSMETALS_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPGOLDPREMETALS_TDIC)
        } else if (transaction.CostCategory == CostCategory.TC_BOPVALUABLEPAPERSCOV_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPVALUABLEPAPERSCOV_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_BOPACCRECEIVABLESCOV_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPACCRECEIVABLESCOV_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_BOPFINEARTSCOV_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPFINEARTSCOV_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_BOPEMPDISCOV_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPEMPDISCOV_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_BOPSIGNS_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPSIGNS_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_BOPPOLLCLEANREMOVAL_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPPOLLCLEANREMOVAL_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_BOPARSONREWARD_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPARSONREWARD_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_BOPNEWBUILDINGCOV_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPNEWBUILDINGCOV_TDIC)
        } else if (transaction.CostCategory == CostCategory.TC_BOPBPPDEBRISREMOVAL_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPBPPDEBRISREMOVAL_TDIC)
        } else if (transaction.CostCategory == CostCategory.TC_BOPBPPOFFPREMISES_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPBPPOFFPREMISES_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_BOPPERSONALEFFECTS_TDIC) {

          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPPERSONALEFFECTS_TDIC)
        } else if (transaction.CostCategory == CostCategory.TC_BOPEDPCOV_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPEDPCOV_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_BOPDIGITALXRAYCOV_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPDIGITALXRAYCOV_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_LOCKKEYREPLACEMENTCOVERAGE) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPLOCKANDKEYCOV_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_EQUIPMENTBREAKDOWNDONTRESTRICTRESERVES_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPEQUIPBREAKCOV_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_BOPEXTRAEXPENSECOV_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPEXTRAEXPENSECOV_TDIC)

        } else if (transaction.CostCategory == CostCategory.TC_POLLUTION_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPPOLLUTIONCOV)
        }else if (transaction.CostCategory == CostCategory.TC_BOPFIREDEPSERCHARGE_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPFIREDEPSERCHARGE_TDIC)
        }else if (transaction.CostCategory == CostCategory.TC_BOPFIREEXTRECHARGE_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPFIREEXTRECHARGE_TDIC)
        } else if (transaction.CostCategory == CostCategory.TC_BOPENCCOVFINECOND_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPENCCOVFINECOND_TDIC)
        } else if (transaction.CostCategory == CostCategory.TC_BOPENCCOVMONEYSCOND_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPENCCOVMONEYSCOND_TDIC)
        } else if (transaction.CostCategory == CostCategory.TC_BOPENCCOVENDTCOND_TDIC) {
          cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPENCCOVENDTCOND_TDIC)
        }

      }
      if (transaction.Exposure.PrimaryCoverage == CoverageType.TC_BOPBUILDINGCOV) {
        if (transaction.Claim.Policy.Offerings_TDIC == Offering_TDIC.TC_BOP) {
          if (transaction.CostCategory == CostCategory.TC_BOPLOSSOUTDOORTSP_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPLOSSOUTDOORTSP_TDIC)
          } else if (transaction.CostCategory == CostCategory.TC_BOPNEWBUILDINGCOV_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPNEWBUILDINGCOV_TDIC)
          } else if (transaction.CostCategory == CostCategory.TC_BOPBUILDEBRISREMOVAL_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPBUILDEBRISREMOVAL_TDIC)
          } else if (transaction.CostCategory == CostCategory.TC_BOPFUNGICOV_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPFUNGICOV_TDIC)
          } else if (transaction.CostCategory == CostCategory.TC_EQUIPMENTBREAKDOWNDONTRESTRICTRESERVES_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPEQUIPBREAKCOV_TDIC)

          } else if (transaction.CostCategory == CostCategory.TC_BOPEXTRAEXPENSECOV_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPEXTRAEXPENSECOV_TDIC)

          } else if (transaction.CostCategory == CostCategory.TC_POLLUTION_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPPOLLUTIONCOV)
          } else if (transaction.CostCategory == CostCategory.TC_BOPVALUABLEPAPERSCOV_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPVALUABLEPAPERSCOV_TDIC)
          } else if (transaction.CostCategory == CostCategory.TC_BOPILMINESUBCOV_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPILMINESUBCOV_TDIC)
          } else if (transaction.CostCategory == CostCategory.TC_BOPPOLLCLEANREMOVAL_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPPOLLCLEANREMOVAL_TDIC)
          }
        }
        if (transaction.Claim.Policy.Offerings_TDIC == Offering_TDIC.TC_LRP) {
          if (transaction.CostCategory == CostCategory.TC_BOPPOLLCLEANREMOVAL_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPPOLLCLEANREMOVAL_TDIC)
          } else if (transaction.CostCategory == CostCategory.TC_BOPBUILDEBRISREMOVAL_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPBUILDEBRISREMOVAL_TDIC)
          } else if (transaction.CostCategory == CostCategory.TC_BOPFUNGICOV_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPFUNGICOV_TDIC)
          } else if (transaction.CostCategory == CostCategory.TC_EQUIPMENTBREAKDOWNDONTRESTRICTRESERVES_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPEQUIPBREAKCOV_TDIC)

          } else if (transaction.CostCategory == CostCategory.TC_BOPEXTRAEXPENSECOV_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPEXTRAEXPENSECOV_TDIC)

          } else if (transaction.CostCategory == CostCategory.TC_POLLUTION_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPPOLLUTIONCOV)
          } else if (transaction.CostCategory == CostCategory.TC_BOPVALUABLEPAPERSCOV_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPVALUABLEPAPERSCOV_TDIC)
          } else if (transaction.CostCategory == CostCategory.TC_BOPILMINESUBCOV_TDIC) {
            cov = getCoveragebyCoverageType(transaction, CoverageType.TC_BOPILMINESUBCOV_TDIC)
          }
        }
      }

    }

    //
    // if sub coverage null then getting the coverage from exposure
    if (cov == null or !cov.HasExposureLimit) {
      if (transaction.Exposure.Coverage != null)
        cov = transaction.Exposure.Coverage
      else {
        // if exposure doesn't have coverage then getting the coverage from policy coverage by coverage type
        cov = getCoveragebyCoverageType(transaction, transaction.Exposure.PrimaryCoverage)
      }
    }
    if (cov != null)
      return cov

    return null

  }


  public static function getCoveragebyCoverageType(transaction : Transaction, coverageType : CoverageType) : Coverage {
    var cov : Coverage = null
    cov = transaction.Claim.Policy.Coverages.firstWhere(\elt -> elt.Type == coverageType)
    if (cov == null) {
      transaction?.Claim.Policy?.RiskUnits.each(\elt -> {
          if (elt != null) {
              (elt as LocationBasedRU)?.Coverages?.each(\pc ->{
                  if (pc != null) {
                    var propertyCoverage = pc as PropertyCoverage
                    if (propertyCoverage.Type == coverageType) {
                      cov = pc as PropertyCoverage
                    }
                  }
               })
             }
          })
      }
    return cov
  }

  public static function checkCostcategory(costcategory : CostCategory, transaction : Transaction) : Boolean {
    var covtype : CoverageType = null
    if (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY) {
      if (costcategory == CostCategory.TC_GLDENTISTPROFLIABCOV_TDIC) {
        covtype = CoverageType.TC_GLDENTISTPROFLIABCOV_TDIC
      } else if (costcategory == CostCategory.TC_GLMEDPAYCOV_TDIC) {
        covtype = CoverageType.TC_GLMEDPAYCOV_TDIC
      }
    }
    if (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_BUSINESSLIABILITY) {
      if (costcategory == CostCategory.TC_GLDENTISTPROFLIABCOV_TDIC) {
        covtype = CoverageType.TC_GLDENTALBUSINESSLIABCOV_TDIC
      } else if (costcategory == CostCategory.TC_GLDENTALEMPBENLIAB_TDIC) {
        covtype = CoverageType.TC_GLDENTALEMPBENLIAB_TDIC
      } else if (costcategory == CostCategory.TC_GLDENTALMEDWASTECOV_TDIC) {
        covtype = CoverageType.TC_GLDENTALMEDWASTECOV_TDIC
      } else if (costcategory == CostCategory.TC_GLMEDPAYCOV_TDIC or costcategory == CostCategory.TC_PATIENTPROPERTY_TDIC) {
        covtype = CoverageType.TC_GLMEDPAYCOV_TDIC
      }
    }
    if (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY) {
      if (costcategory == CostCategory.TC_BOPDENTALGENLIABILITYCOV_TDIC) {
        covtype = CoverageType.TC_BOPDENTALGENLIABILITYCOV_TDIC

      } else if (costcategory == CostCategory.TC_BOPBUILDINGOWNERSLIABCOV_TDIC) {
        covtype = CoverageType.TC_GLDENTALEMPBENLIAB_TDIC

      }
    }
    if (transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_PROPERTY) {
      if (transaction.Exposure.PrimaryCoverage == CoverageType.TC_BOPPERSONALPROPCOV) {
        if (costcategory == CostCategory.TC_MONEYANDSECURITIES_TDIC) {
          covtype = CoverageType.TC_BOPMONEYSECCOV_TDIC
        }
        if (costcategory == CostCategory.TC_GOLDANDOTHERPRECIOUSMETALS_TDIC) {
          covtype = CoverageType.TC_BOPGOLDPREMETALS_TDIC
        }
        if (costcategory == CostCategory.TC_BOPVALUABLEPAPERSCOV_TDIC) {
          covtype = CoverageType.TC_BOPVALUABLEPAPERSCOV_TDIC
        }
        if (costcategory == CostCategory.TC_BOPACCRECEIVABLESCOV_TDIC) {
          covtype = CoverageType.TC_BOPACCRECEIVABLESCOV_TDIC
        }
        if (costcategory == CostCategory.TC_BOPFINEARTSCOV_TDIC) {
          covtype = CoverageType.TC_BOPFINEARTSCOV_TDIC
        }
        if (costcategory == CostCategory.TC_BOPEMPDISCOV_TDIC) {
          covtype = CoverageType.TC_BOPEMPDISCOV_TDIC
        }
        if (costcategory == CostCategory.TC_BOPSIGNS_TDIC) {
          covtype = CoverageType.TC_BOPSIGNS_TDIC
        }
        if (costcategory == CostCategory.TC_BOPPOLLCLEANREMOVAL_TDIC) {
          covtype = CoverageType.TC_BOPPOLLCLEANREMOVAL_TDIC
        }
        if (costcategory == CostCategory.TC_BOPFIREEXTRECHARGE_TDIC) {
          covtype = CoverageType.TC_BOPFIREDEPSERCHARGE_TDIC
        }

        if (costcategory == CostCategory.TC_BOPARSONREWARD_TDIC) {
          covtype = CoverageType.TC_BOPARSONREWARD_TDIC
        }
        if (costcategory == CostCategory.TC_BOPNEWBUILDINGCOV_TDIC) {
          covtype = CoverageType.TC_BOPNEWBUILDINGCOV_TDIC
        }
        if (costcategory == CostCategory.TC_BOPBPPDEBRISREMOVAL_TDIC) {
          covtype = CoverageType.TC_BOPBPPDEBRISREMOVAL_TDIC
        }
        if (costcategory == CostCategory.TC_BOPBPPOFFPREMISES_TDIC) {
          covtype = CoverageType.TC_BOPBPPOFFPREMISES_TDIC
        }
        if (costcategory == CostCategory.TC_BOPPERSONALEFFECTS_TDIC) {
          covtype = CoverageType.TC_BOPPERSONALEFFECTS_TDIC
        }
        if (costcategory == CostCategory.TC_BOPEDPCOV_TDIC) {
          covtype = CoverageType.TC_BOPEDPCOV_TDIC
        }
        if (costcategory == CostCategory.TC_BOPDIGITALXRAYCOV_TDIC) {
          covtype = CoverageType.TC_BOPDIGITALXRAYCOV_TDIC
        }
        if (costcategory == CostCategory.TC_LOCKKEYREPLACEMENTCOVERAGE) {
          covtype = CoverageType.TC_BOPLOCKANDKEYCOV_TDIC
        }
        if (costcategory == CostCategory.TC_EQUIPMENTBREAKDOWNDONTRESTRICTRESERVES_TDIC) {
          covtype = CoverageType.TC_BOPEQUIPBREAKCOV_TDIC
        }
        if (costcategory == CostCategory.TC_BOPEXTRAEXPENSECOV_TDIC) {
          covtype = CoverageType.TC_BOPEXTRAEXPENSECOV_TDIC
        }
        if (costcategory == CostCategory.TC_POLLUTION_TDIC) {
          covtype = CoverageType.TC_BOPPOLLUTIONCOV
        }
      }
      if (transaction.Exposure.PrimaryCoverage == CoverageType.TC_BOPBUILDINGCOV) {
        if (transaction.Claim.Policy.Offerings_TDIC == Offering_TDIC.TC_BOP) {
          if (costcategory == CostCategory.TC_BOPLOSSOUTDOORTSP_TDIC) {
            covtype = CoverageType.TC_BOPLOSSOUTDOORTSP_TDIC
          } else if (costcategory == CostCategory.TC_BOPNEWBUILDINGCOV_TDIC) {
            covtype = CoverageType.TC_BOPNEWBUILDINGCOV_TDIC

          } else if (costcategory == CostCategory.TC_BOPBUILDEBRISREMOVAL_TDIC) {
            covtype = CoverageType.TC_BOPBUILDEBRISREMOVAL_TDIC

          } else if (costcategory == CostCategory.TC_BOPFUNGICOV_TDIC) {
            covtype = CoverageType.TC_BOPFUNGICOV_TDIC

          } else if (costcategory == CostCategory.TC_EQUIPMENTBREAKDOWNDONTRESTRICTRESERVES_TDIC) {
            covtype = CoverageType.TC_BOPEQUIPBREAKCOV_TDIC

          } else if (costcategory == CostCategory.TC_BOPEXTRAEXPENSECOV_TDIC) {
            covtype = CoverageType.TC_BOPEXTRAEXPENSECOV_TDIC


          } else if (costcategory == CostCategory.TC_POLLUTION_TDIC) {
            covtype = CoverageType.TC_BOPPOLLUTIONCOV

          } else if (costcategory == CostCategory.TC_BOPVALUABLEPAPERSCOV_TDIC) {
            covtype = CoverageType.TC_BOPVALUABLEPAPERSCOV_TDIC
          } else if (costcategory == CostCategory.TC_BOPILMINESUBCOV_TDIC) {
            covtype = CoverageType.TC_BOPILMINESUBCOV_TDIC
          } else if (costcategory == CostCategory.TC_BOPPOLLCLEANREMOVAL_TDIC) {
            covtype = CoverageType.TC_BOPPOLLCLEANREMOVAL_TDIC
          }
        } else if (transaction.Claim.Policy.Offerings_TDIC == Offering_TDIC.TC_LRP) {
          if (costcategory == CostCategory.TC_BOPPOLLCLEANREMOVAL_TDIC) {
            covtype = CoverageType.TC_BOPPOLLCLEANREMOVAL_TDIC

          } else if (costcategory == CostCategory.TC_BOPBUILDEBRISREMOVAL_TDIC) {
            covtype = CoverageType.TC_BOPBUILDEBRISREMOVAL_TDIC

          } else if (costcategory == CostCategory.TC_BOPFUNGICOV_TDIC) {
            covtype = CoverageType.TC_BOPFUNGICOV_TDIC

          } else if (costcategory == CostCategory.TC_EQUIPMENTBREAKDOWNDONTRESTRICTRESERVES_TDIC) {
            covtype = CoverageType.TC_BOPEQUIPBREAKCOV_TDIC


          } else if (costcategory == CostCategory.TC_BOPEXTRAEXPENSECOV_TDIC) {
            covtype = CoverageType.TC_BOPEXTRAEXPENSECOV_TDIC


          } else if (costcategory == CostCategory.TC_POLLUTION_TDIC) {
            covtype = CoverageType.TC_BOPPOLLUTIONCOV

          } else if (costcategory == CostCategory.TC_BOPVALUABLEPAPERSCOV_TDIC) {
            covtype = CoverageType.TC_BOPVALUABLEPAPERSCOV_TDIC

          } else if (costcategory == CostCategory.TC_BOPILMINESUBCOV_TDIC) {
            covtype = CoverageType.TC_BOPILMINESUBCOV_TDIC

          }
        }
      }
    }
    if (covtype != null) {
      if (transaction.Exposure.PrimaryCoverage == covtype) {
        return true
      } else {
        var coverage = getCoveragebyCoverageType(transaction, covtype)
        if (coverage == null) {
          return true
        } else if (coverage.Type == transaction.Exposure.PrimaryCoverage)
          return true
        else if (!coverage.HasExposureLimit)
          return true
        else {
          return false
        }
      }
    }
    return true
  }

}