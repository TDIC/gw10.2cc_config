package tdic.cc.config.assignment

/**
 * GWNW 45
 * 07/17/2020 Harish Barrenka
 * Helper class for Desktop Queued Activities page functionalities.
 */
class DesktopQueuedActivitiesHelper {

  /*
   * @return AssignableQueue[] - Filters new claim queue from Queues returned from OOB method if user is not having queue read permission.
   */
  function retrieveQueues() : entity.AssignableQueue[]{
    var queuesList = new ArrayList<AssignableQueue>()
    var queuesFromOOBLogic = gw.api.desktop.DesktopQueuedActivitiesUtil.getQueues()
    if(queuesFromOOBLogic.Count>0){
      queuesFromOOBLogic.each(\queue -> {
        if(new AssignmentStrategyUtil().queueNotExistInStrategy(queue.Name)){
          queuesList.add(queue)
        } else {
          if(perm.System.newclmqueueread_TDIC){
            queuesList.add(queue)
          }
        }
      })
    }
    return queuesList.toTypedArray()
  }

  /*
   * @return AssignableQueue - Returns null if user is not having queue read permission and Queue is configured in Assignemnt Strategy.
   */
  function selectedQueue() : entity.AssignableQueue{
    var selectedQueue = gw.api.desktop.DesktopQueuedActivitiesUtil.getSelectedQueue()
    if(selectedQueue!=null){
      if(new AssignmentStrategyUtil().queueNotExistInStrategy(selectedQueue.Name)){
        return selectedQueue
      } else {
        if(perm.System.newclmqueueread_TDIC){
          return selectedQueue
        }
      }
    }
    return null
  }

  /*
   * @return IQueryBeanResult<ActivityDesktopView> - Filters activities assigned to the queue if user is not having queue read permission
   *                                                  and Queue is configured in Assignemnt Strategy, if not returns as per OOB function.
   */
  function activitiesForSelectedQueue() : gw.api.database.IQueryBeanResult<ActivityDesktopView>{
    var selectedQueue = gw.api.desktop.DesktopQueuedActivitiesUtil.SelectedQueue
    if(selectedQueue!=null){
      if(new AssignmentStrategyUtil().queueNotExistInStrategy(selectedQueue.Name)){
        return gw.api.desktop.DesktopQueuedActivitiesUtil.getActivitiesForSelectedQueue() as gw.api.database.IQueryBeanResult<ActivityDesktopView>
      } else {
        if(perm.System.newclmqueueread_TDIC){
          return gw.api.desktop.DesktopQueuedActivitiesUtil.getActivitiesForSelectedQueue() as gw.api.database.IQueryBeanResult<ActivityDesktopView>
        }
      }
    }
    return null
  }
}