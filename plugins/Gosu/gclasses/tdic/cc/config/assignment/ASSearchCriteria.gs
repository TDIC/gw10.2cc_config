package tdic.cc.config.assignment

/**
 * GWNW 45
 * 07/17/2020 Harish Barrenka
 * Search Criteria used to Query and fetch results from AssignmentStrategry_TDIC table.
 */
class ASSearchCriteria {
  var assignTo : ASAssignTo_TDIC as AssignTo
  var claimType : ClaimType_TDIC as ClaimType
}