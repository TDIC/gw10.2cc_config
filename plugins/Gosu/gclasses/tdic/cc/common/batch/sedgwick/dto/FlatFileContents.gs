package tdic.cc.common.batch.sedgwick.dto

uses java.util.HashMap
uses java.util.ArrayList


/**
 * Created with IntelliJ IDEA.
 * User: Praneethk
 * Date: 4/29/15
 * Time: 11:28 AM
 * US674 Claim updates from Sedgwick
 * This class is used by FlatFileUtilityHelper to return both the decoded fields and their respective values along with the plain lines.
 * The lines are used to write the error file when a record is not processed correctly.
 */
class FlatFileContents {
  public var multipleFlatFileContentsMap:HashMap<String,HashMap<String,ArrayList<HashMap<String, String>>>>
  public var multipleFlatFileLineMap:HashMap<String,HashMap<String,ArrayList<String>>>

  construct(multiFlatFileContentsMap:HashMap<String,HashMap<String,ArrayList<HashMap<String, String>>>>,multiFlatFileLineMap:HashMap<String,HashMap<String,ArrayList<String>>>){
    multipleFlatFileContentsMap = multiFlatFileContentsMap
    multipleFlatFileLineMap = multiFlatFileLineMap
  }
}