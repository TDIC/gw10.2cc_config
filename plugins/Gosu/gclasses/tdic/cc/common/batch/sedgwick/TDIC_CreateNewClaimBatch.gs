package tdic.cc.common.batch.sedgwick

uses com.tdic.util.properties.PropertyUtil
uses gw.api.util.ConfigAccess
uses gw.processes.BatchProcessBase
uses java.io.File
uses java.util.ArrayList
uses gw.pl.exception.GWConfigurationException
uses gw.pl.util.FileUtil
uses org.slf4j.LoggerFactory
uses tdic.cc.common.batch.sedgwick.helper.TDIC_CreateNewClaim


/**
 * Created with IntelliJ IDEA.
 * User: praneethk
 * Date: 3/1/17
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_CreateNewClaimBatch extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("TDIC_FEEDTOSEDGWICK")
  private static var CLASS_NAME : String = "TDIC_CreateClaimsFromMigrationBatchJob"
  private var headerPath : String
  private var excelPath : String
  private var decryptedPath : String
  private var errorPath : String

  construct() {
    super(BatchProcessType.TC_MIGRATIONCLAIMS)
    headerPath = ConfigAccess.getConfigFile(PropertyUtil.getInstance().getProperty("headerPath")).Path
    excelPath = PropertyUtil.getInstance().getProperty("sedgwick.migrationFromSedgwickExcelPath")
    decryptedPath = PropertyUtil.getInstance().getProperty("sedgwick.sharedMigrationFromSedgwickDecryptedPath")
    errorPath = PropertyUtil.getInstance().getProperty("sedgwick.sharedMigrationUpdateFeedErrorPath")
  }

  /**
   * US674 Claim updates from Sedgwick
   * praneethk
   * Determines if the process should run the doWork() method, based on successful retrieval of configured properties from int database.
   */

  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  override function checkInitialConditions() : boolean {
    var logPrefix = "${CLASS_NAME}#checkInitialConditions()"
    return Boolean.TRUE
  }

  /*
   * US674 Claim updates from Sedgwick
   * Runs the actual batch process
  */
  override function doWork() {
    _logger.debug("TDIC_UpdateFeedBatchProcess#doWork() - Entering")

    //Check For any Processing Failed Files in Error Directory. If Found Batch Will Terminate From #doWork()
    if(TerminateRequested){
      return
    }

    /*
     * Check For any Files to be processed in Encrypted Directory. This method will get the files from encrypted folder
     * in the order it was recieved and will be processed accordingly by decrypting the file using PGPUtility and store
     * the file in a temporary directory and process from temp directory. After processing successfully/even incase of error
     * the file will be deleted recursively/ moved to error directory.
     */

    var decryptedFolder = new File(decryptedPath)
    var decryptedFiles = decryptedFolder.listFiles()
    processFiles(decryptedFiles,decryptedPath) //Process decrypted files
    _logger.debug("TDIC_UpdateFeedBatchProcess#doWork() - Exiting")
  }

  /*
   * This Method will check if the provided path for the directory is empty.
   * If Empty Return True Else False
   */
  function isEmpty(dirPath:String):boolean{
    var folder = new File(dirPath)
    var files = folder.listFiles()
    return files.length==0?true:false
  }

  /*
  * The processFiles Method is the main method will which call the base helper class to process the decrypted data.
  * This Method will return the names of the processedFiles.
  */
  function processFiles(decFileArray:File[],filePath:String):ArrayList<String>{

    var processedFiles = new ArrayList<String>()
    for(var decFile in decFileArray){
      incrementOperationsCompleted()
      var decFileName=decFile.Name //Get only the file name by replacing the directory path with blank string
      var sedgwickMigration  = new TDIC_CreateNewClaim(decFile.toString())
      var errorString = sedgwickMigration.addClaims()
      processedFiles.add(decFile.toString())
      if(errorString.length>0){        //If there is an error in processing the file
        incrementOperationsFailed()
        _logger.error("TDIC_UpdateFeedBatchJob#doWork() - Processing cannot be finished with errors. There is some error. Writing to "+errorPath+decFileName)
        FileUtil.write(new File(errorPath+decFileName),errorString) //Write corresponding lines to error dir
        return processedFiles
      }
    }
    return processedFiles
  }

  /**
   * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.   *
   * @return A boolean to indicate that the process can be stopped.
   */

  override function requestTermination() : boolean {
    _logger.info("TDIC_UpdateFeedBatchProcess#requestTermination() - Terminate requested for batch process.")
    super.requestTermination()// Set TerminateRequested to true by calling super method
    return true
  }

}