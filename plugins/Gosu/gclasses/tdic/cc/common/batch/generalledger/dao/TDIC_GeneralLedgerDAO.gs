package tdic.cc.common.batch.generalledger.dao

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses gw.api.system.database.SequenceUtil
uses gw.api.util.StringUtil
uses org.slf4j.LoggerFactory
uses tdic.cc.integ.plugins.generalledger.dto.GLTransactionDTO
uses tdic.cc.integ.plugins.generalledger.dto.GLTransactionFile
uses tdic.cc.integ.plugins.generalledger.exception.TDIC_GeneralLedgerException

uses java.sql.CallableStatement
uses java.sql.Connection
uses java.sql.SQLException
uses java.sql.Statement

/**
 * TDIC_GeneralLedgerDAO is responsible to
 * Insert cc Transaction info to table: GLCCTransactions of GWINT DB.
 * Update processed CC transaction records to true in table: GLCCTransactions.
 * Fetch unprocessed CC transaction records from table: GLCCTransactions.
 * Created by RambabuN on 12/12/2019.
 */
class TDIC_GeneralLedgerDAO {

  static final var _logger = LoggerFactory.getLogger("TDIC_GeneralLedger")

  //Insert SP Transaction records.
  static final var INSERT_GL_CC_TRANSACTIONS = "dbo.SP_Insert_GLCCTransactions ?,?,?,?,?,?,?,?,?,?,?,?,?"

  //Fectch SP Transaction records
  static final var SELECT_GLCCTRANSACTIONS_SP : String = "{ call dbo.SP_Select_GLCCTransactions() }"

  //SQL query to update processed transaction records to true.
  static final var UPDATE_GL_CC_TRANSACTIONS : String = "UPDATE GLCCTRANSACTIONS SET PROCESSED = 1 WHERE PROCESSED = 0"

  static final var CLOSE_PERENTHESIS = ")"
  static final var COMMA = ","

  static final var GL_TRANSACTION_SEQUENCE_KEY = "gl_trans_seq"
  static final var SEQUENCE_NUM_FORMAT = "0000"

  static final var OFF_SET = " OFF"

  var _conn : Connection
  //ID's for processed transaction records.
  //var _transactionRowIDs : String


  /**
   * Function to initialize the DB Connection Object.
   */
  function initialize() {
    _logger.debug("Initalizing DB connections - TDIC_GeneralLedgerDAO.initialize()")

    _conn = DatabaseManager.getConnection(PropertyUtil.getInstance().getProperty("IntDBURL"))
    _conn.setAutoCommit(true)

    _logger.debug("Exit TDIC_GeneralLedgerDAO.initialize()")
  }

  /**
   * Cleans up the database connection.
   */
  function cleanConnections() {
    DatabaseManager.closeConnection(_conn)
  }

  /**
   * Function to Insert CC transactions to Integration GWINT DB table :GLCCTransactions.
   * @param gltransaction
   */
  function insertCCGLTransactions(
      gltransaction : tdic.cc.integ.plugins.generalledger.model.gltransactiondtomodel.GLTransactionDTO) {

    _logger.debug("Enter TDIC_GeneralLedgerDAO.insertCCGLTransactions()")
    var _inDebitGLTransactionCalStmt : CallableStatement
    var _inCreditGLTransactionCalStmt : CallableStatement

    try {

      _inDebitGLTransactionCalStmt = _conn.prepareCall(INSERT_GL_CC_TRANSACTIONS)
      _inDebitGLTransactionCalStmt.setString(1, gltransaction.TrasactionID)
      _inDebitGLTransactionCalStmt.setString(2, null)
          //StringUtil.formatNumber(SequenceUtil.next(1, GL_TRANSACTION_SEQUENCE_KEY), SEQUENCE_NUM_FORMAT))
      _inDebitGLTransactionCalStmt.setString(3, gltransaction.TransactionSubtype)
      _inDebitGLTransactionCalStmt.setString(4, gltransaction.DebitAccountingUnit)
      if(gltransaction.DebitAccountNum=="20320000") {
        _inDebitGLTransactionCalStmt.setString(5, "00")
        _inDebitGLTransactionCalStmt.setString(6, "00")
      } else {
        _inDebitGLTransactionCalStmt.setString(5, gltransaction.StateCode)
        _inDebitGLTransactionCalStmt.setString(6, gltransaction.LOBCode)
      }

      _inDebitGLTransactionCalStmt.setString(7, gltransaction.DebitAccountNum)
      _inDebitGLTransactionCalStmt.setString(8, gltransaction.Description)
      _inDebitGLTransactionCalStmt.setBigDecimal(9, gltransaction.DebitAmount)
      _inDebitGLTransactionCalStmt.setString(10, gltransaction.ClaimNumber)
      _inDebitGLTransactionCalStmt.setString(11, LedgerSide.TC_DEBIT.Code)
      _inDebitGLTransactionCalStmt.setString(12, gltransaction.MonthEndDate)
      _inDebitGLTransactionCalStmt.setString(13, gltransaction.RefCode)

      _inCreditGLTransactionCalStmt = _conn.prepareCall(INSERT_GL_CC_TRANSACTIONS)
      _inCreditGLTransactionCalStmt.setString(1, gltransaction.TrasactionID)
      _inCreditGLTransactionCalStmt.setString(2, null)
          //StringUtil.formatNumber(SequenceUtil.next(1, GL_TRANSACTION_SEQUENCE_KEY), SEQUENCE_NUM_FORMAT))
      _inCreditGLTransactionCalStmt.setString(3, gltransaction.TransactionSubtype)
      _inCreditGLTransactionCalStmt.setString(4, gltransaction.CreditAccountingUnit)
      if(gltransaction.CreditAccountNum=="20320000") {
        _inCreditGLTransactionCalStmt.setString(5, "00")
        _inCreditGLTransactionCalStmt.setString(6, "00")
      } else {
        _inCreditGLTransactionCalStmt.setString(5, gltransaction.StateCode)
        _inCreditGLTransactionCalStmt.setString(6, gltransaction.LOBCode)
      }

      _inCreditGLTransactionCalStmt.setString(7, gltransaction.CreditAccountNum)
      _inCreditGLTransactionCalStmt.setString(8, (gltransaction.Description + OFF_SET))
      _inCreditGLTransactionCalStmt.setBigDecimal(9, gltransaction.CreditAmount)
      _inCreditGLTransactionCalStmt.setString(10, gltransaction.ClaimNumber)
      _inCreditGLTransactionCalStmt.setString(11, LedgerSide.TC_CREDIT.Code)
      _inCreditGLTransactionCalStmt.setString(12, gltransaction.MonthEndDate)
      _inCreditGLTransactionCalStmt.setString(13, gltransaction.RefCode)

      _inDebitGLTransactionCalStmt.executeUpdate()
      _inCreditGLTransactionCalStmt.executeUpdate()
   } catch (ex : SQLException) {
      _logger.error(this.Class.Name + "Error Occurred while inserting CC transaction " +
      "[ ClaimNumber: ${gltransaction.ClaimNumber}, TrasactionPublicID: ${gltransaction.TrasactionID}, " +
      " TransactionSubtype: ${gltransaction.TransactionSubtype} ] ")

      //Throw exception
      throw new TDIC_GeneralLedgerException("Error Occurred while inserting Transaction " +
          "[ ClaimNumber: ${gltransaction.ClaimNumber}, TrasactionPublicID: ${gltransaction.TrasactionID}," +
          "TransactionSubtype: ${gltransaction.TransactionSubtype} ] ", ex)
    } finally {
      //Release connections.
      DatabaseManager.closeCallableStatement(_inDebitGLTransactionCalStmt)
      DatabaseManager.closeCallableStatement(_inCreditGLTransactionCalStmt)
    }
    _logger.debug("Exit TDIC_GeneralLedgerDAO.insertCCGLTransactions()")
  }

  /**
   * Function to fetch unprocessed CC Transactions from table:GLCCTransactions integration DB GWINT.
   * @return _glTransactionFile
   */
  function fetchGLTransactions() : GLTransactionFile {
    _logger.debug("Enter TDIC_GeneralLedgerDAO.fetchGLTransactions()")
    var _selGLTransactionsCalStmt : CallableStatement
    var  _glTransactionFile : GLTransactionFile
    var sequenceNum = 1
    try {
      //var _rowIds = new ArrayList<Integer>()
      //var _transIDs = new ArrayList<String>()

      _selGLTransactionsCalStmt = _conn.prepareCall(SELECT_GLCCTRANSACTIONS_SP)
      var _resultSet = _selGLTransactionsCalStmt.executeQuery()
      _glTransactionFile = new GLTransactionFile()

      while (_resultSet.next()) {
        var glTransactionDTO = new GLTransactionDTO()
        //_rowIds.add(_resultSet.getInt(1))
        //_transIDs.add(_resultSet.getString(2))
        glTransactionDTO.SequenceNumber = (sequenceNum)
        sequenceNum = sequenceNum + 1
        glTransactionDTO.AccountingUnit = _resultSet.getString(1)
        glTransactionDTO.StateCode = _resultSet.getString(2)
        glTransactionDTO.LOBCode = _resultSet.getString(3)
        glTransactionDTO.AccountNum = _resultSet.getString(4)
        glTransactionDTO.Description = _resultSet.getString(5)
        glTransactionDTO.Amount = _resultSet.getBigDecimal(6)
        glTransactionDTO.MonthEndDate = _resultSet.getString(7)
        glTransactionDTO.RefCode = _resultSet.getString(8)

        _glTransactionFile.GLTransactions.add(glTransactionDTO)
        //Join all the processed CC transaction record ID's as string.
        //_transactionRowIDs = _rowIds.join(COMMA)
      }
      //_logger.debug("${_rowIds.Count} CC transaction records are fetched successfylly.public ID's: ${_transIDs}")
    } catch (ex : SQLException) {
      _logger.error("Failed to fetch CC transactions form table GEINT:GLCCTransactions.", ex)
      throw new TDIC_GeneralLedgerException(ex.Message, ex)
    } finally {
      //Release Connections
      DatabaseManager.closeCallableStatement(_selGLTransactionsCalStmt)
    }
    _logger.debug("Exit TDIC_GeneralLedgerDAO.fetchGLTransactions()")
    return _glTransactionFile
  }

  /**
   * Function to update processed CC transactions processed records status to true after writing
   * to outbound files.
   */
  function updateProcessedGLTransactions() {
    _logger.debug("Enter TDIC_GeneralLedgerDAO.updateProcessedGLTransactions()")
    var _sqlStatement : Statement

    try {
      var sqlString = new StringBuilder().append(UPDATE_GL_CC_TRANSACTIONS)
          //.append(_transactionRowIDs)
          //.append(CLOSE_PERENTHESIS)
      _sqlStatement = _conn.createStatement()
      _sqlStatement.executeUpdate(sqlString.toString())
      _conn.commit()
    } catch (ex : SQLException) {
      _logger.error("Failed to update CC Trnsactions records status to processed.")
      throw new TDIC_GeneralLedgerException(ex.Message, ex)
    } finally {
      //Release Connections
      if (_sqlStatement != null) _sqlStatement.close()
    }
    _logger.debug("Exit TDIC_GeneralLedgerDAO.updateProcessedGLTransactions()")
  }

}