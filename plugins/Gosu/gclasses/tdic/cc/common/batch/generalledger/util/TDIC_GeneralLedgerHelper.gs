package tdic.cc.common.batch.generalledger.util

uses com.tdic.util.delimitedfile.DelimitedFileImpl
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.gx.GXOptions
uses gw.xml.XmlElement
uses org.apache.commons.io.FileUtils
uses org.slf4j.LoggerFactory
uses tdic.cc.common.batch.generalledger.dao.TDIC_GeneralLedgerDAO
uses tdic.cc.integ.plugins.generalledger.dto.GLTransactionDTO
uses tdic.cc.integ.plugins.generalledger.dto.GLTransactionFile
uses tdic.cc.integ.plugins.generalledger.exception.TDIC_GeneralLedgerException

uses java.io.File
uses java.io.IOException
uses java.text.SimpleDateFormat

uses entity.Transaction

/**
 * TDIC_GeneralLedgerHelper is responsible to
 * Insert cc Transaction info to table: GLCCTransactions of GWINT DB.
 * Fetch unprocessed records from GLCCTransactions and write to General Ledger outbound files.
 * Write XML formated CC Transactions to Flat file.
 * Generating payload for GLTransactionDTO from CC Transaction entity.
 * Created by RambabuN on 12/16/2019.
 */
class TDIC_GeneralLedgerHelper {

  static final var _logger = LoggerFactory.getLogger("TDIC_GeneralLedger")

  static final var DATE_FORMAT_MM_dd_yyyy_HHmmss : String = "MM-dd-yyyy-HHmmss"
  static final var DATE_FORMAT_WITH_SLASH = "MM/dd/yyyy"
  static final var DATE_FORMAT_WITH_NO_SLASH = "MMddyyyy"
  static final var GL_FILESDIRECTORY_NAME_PROPERTY = "generalLedger.outbound.filesdirectory.name"
  static final var GL_FILE_NAME_PROPERTY = "generalLedger.outbound.file.name"

  static final var MAPPER = gw.api.util.TypecodeMapperUtil.getTypecodeMapper()
  static final var NAMESPACE_GL_ACCOUNT_UNIT = "tdic:glaccountunit"
  static final var NAMESPACE_GL = "tdic:generalledger"
  static final var NAMESPACE_GL_LOB_CODE = "tdic:gllobcode"
  static final var NAMESPACE_GL_ERE = "tdic:generalledgerere"
  static final var NAMESPACE_GL_LOB_CODE_ERE = "tdic:gllobcodeere"

  //File extensions for General Ledger outbound files.
  static final var GL_OUTBOUND_FILE_EXTENSION = ".GLT"

  //Vendor specification files for General Ledger outbound file.
  static final var INVOICE_VENDOR_SPEC_FILE_NAME = "LawsonCCGLSpec.xlsx"

 //Key to generate CC Transaction Sequence Number.
  static final var GL_TRANSACTION_SEQUENCE_KEY = "gl_trans_seq"
  static final var CHAR_HYPHEN = "-"
  static final var CHAR_UNDERSCORE = "_"
  static final var CHAR_SPACE = " "

  static final var PL_LINE = "pl"
  static final var BOP_LINE = "bop"
  static final var WC_LINE = "wc"
  static final var WC_EXPENSE = "exp"
  static final var WC_INDEMNITY = "ind"
  static final var WC_MEDICAL = "med"
  static final var LINE_CATEGORY_SUBRO_GROUP = "Subro"
  static final var GL_ACCNUM_NAMESPACE = "tdic:gl"
  static final var ACCNUM = "accnum"

  static final var PAY = "PAY"
  static final var RSV = "RSV"
  static final var RRS = "RRS"
  static final var REC = "REC"


  /**
   * Function to insert CC transaction details into GWINT database.
   *
   * @param gltransaction
   */
  static function insertGLTransaction(
      gltransaction : tdic.cc.integ.plugins.generalledger.model.gltransactiondtomodel.GLTransactionDTO) {

    var glDAO = new TDIC_GeneralLedgerDAO()
    try {
      //Initialize DAO
      glDAO.initialize()
      glDAO.insertCCGLTransactions(gltransaction)

      _logger.debug("insertGLTransaction(): CC Transaction info : ${gltransaction.TrasactionID} inserted successfully")
    }  catch (ex : TDIC_GeneralLedgerException) {
      _logger.error("Failed to insert CC transaction info for PublicID# ${gltransaction.TrasactionID}")
      throw ex
    }finally {
      //close any open connections
      glDAO.cleanConnections()
    }
  }

  /**
   * Function to generate General Ledger outbound file name.
   * @param fileExtension
   * @return fileName
   */
  private static function getGLOutboundFileName(fileExtension : String) : String {
    var propUtil = PropertyUtil.getInstance()
    var fileDir = propUtil.getProperty(GL_FILESDIRECTORY_NAME_PROPERTY)
    var fileName = propUtil.getProperty(GL_FILE_NAME_PROPERTY)
    return new StringBuilder().append(fileDir).append(fileName).append(CHAR_HYPHEN)
        .append(new SimpleDateFormat(DATE_FORMAT_MM_dd_yyyy_HHmmss).format(Date.CurrentDate))
        .append(fileExtension).toString()
  }

  /**
   * Function to generate CC transaction payload string from transaction entity.
   * @param transaction
   * @return CC transaction payload String.
   */
  static function getGLTransactionDTOPayload(transaction : Transaction, forReserve: Boolean = null) : String {
    _logger.debug("Enter TDIC_GeneralLedgerHelper.getGLTransactionDTOPayload()")

    var transactionDTO = new GLTransactionDTO()

    transactionDTO.TrasactionID = transaction.PublicID
    transactionDTO.TransactionSubtype = transaction.Subtype.Code
    transactionDTO.ClaimNumber = transaction.Claim.ClaimNumber
    var ledgerTypeName = LedgerSide.RelativeName
    var ledgerTypeDebit = LedgerSide.TC_DEBIT.Code
    var ledgerTypeCredit = LedgerSide.TC_CREDIT.Code
    var lobCode = transaction.Claim.LOBCode
    if(((lobCode== LOBCode.TC_GLLINE || lobCode== LOBCode.TC_BOPLINE) && transaction typeis RecoveryReserve && transaction.RecoveryCategory == RecoveryCategory.TC_DEDUCTIBLE) ||
        (lobCode== LOBCode.TC_WC7LINE && transaction typeis Recovery && (transaction.RecoveryCategory == RecoveryCategory.TC_SUBRO ||
            transaction.RecoveryCategory == RecoveryCategory.TC_REFUNDS_TDIC))){
      transactionDTO.CreditAccountingUnit =
          MAPPER.getAliasByInternalCode(ledgerTypeName, NAMESPACE_GL_ACCOUNT_UNIT, ledgerTypeDebit)
      transactionDTO.DebitAccountingUnit =
          MAPPER.getAliasByInternalCode(ledgerTypeName, NAMESPACE_GL_ACCOUNT_UNIT, ledgerTypeCredit)

    }else {
      transactionDTO.DebitAccountingUnit =
          MAPPER.getAliasByInternalCode(ledgerTypeName, NAMESPACE_GL_ACCOUNT_UNIT, ledgerTypeDebit)
      transactionDTO.CreditAccountingUnit =
          MAPPER.getAliasByInternalCode(ledgerTypeName, NAMESPACE_GL_ACCOUNT_UNIT, ledgerTypeCredit)
    }

    var stateCode = transaction.Claim.JurisdictionState != null ? transaction.Claim.JurisdictionState.Code :
        transaction.Claim.Policy.PolicyLocations.first().Address?.State.Code

    transactionDTO.StateCode = MAPPER.getAliasByInternalCode(Jurisdiction.RelativeName, NAMESPACE_GL, stateCode)

    var costType = getCostType(transaction)
    var accNumNameSpace = generateAccNumNameSpace(transaction, costType,forReserve)

    transactionDTO.DebitAccountNum = MAPPER.getAliasByInternalCode(ledgerTypeName, accNumNameSpace, ledgerTypeDebit)
    transactionDTO.CreditAccountNum = MAPPER.getAliasByInternalCode(ledgerTypeName, accNumNameSpace, ledgerTypeCredit)
    transactionDTO.DebitAmount = transaction.Amount
    transactionDTO.CreditAmount = transaction.Amount?.negate()

    if(forReserve != null && forReserve == Boolean.TRUE){
      transactionDTO.TransactionSubtype = typekey.Transaction.TC_RESERVE.Code
      transactionDTO.DebitAmount = transaction.Amount?.negate()
      transactionDTO.CreditAmount = transaction.Amount
    }

    //Populate Description and LOB code.
    populateDescriptionAndLOBCode(transaction, transactionDTO,costType,forReserve)
    var calendar = Calendar.getInstance()
    var lastDate = calendar.getActualMaximum(Calendar.DATE)
    calendar.set(Calendar.DATE, lastDate)
    transactionDTO.MonthEndDate = new SimpleDateFormat(DATE_FORMAT_WITH_SLASH).format(calendar.getTime())
    //var monthEndDate = new SimpleDateFormat(DATE_FORMAT_WITH_NO_SLASH).format(calendar.getTime())
    transactionDTO.RefCode = stateCode
        //new StringBuilder().append(stateCode).append(monthEndDate).toString()

    var options = new GXOptions() { : Incremental = true}

    //Generate GLTransactionDTO payload xml
    var glTransactionDTOModel = new tdic.cc.integ.plugins.generalledger.model.gltransactiondtomodel.GLTransactionDTO()
        .create(transactionDTO, options)
    _logger.debug("Exit TDIC_GeneralLedgerHelper.getGLTransactionDTOPayload()")

    return glTransactionDTOModel.asUTFString()
  }

  /**
   * Function to find credit/debit accoung number based on alias name configured in typecodemapping.xml
   * Generating alias name dinamically based on trnasaction type.
   * Reserve --> tdic:gl_[LOBCode]_[CostType]_[TransactionSubType]_accnum
   * Payment(Has non Subro group category lines ) -->tdic:gl_[LOBCode]_[CostType]_[TransactionSubType]_accnum
   * Payment(HasSubro group category lines) -->tdic:gl_[LOBCode]_[CostType]_[TransactionSubType]_[LineCategory]_accnum
   * RecoveryReserve --> tdic:gl_[LOBCode]_[CostType]_[TransactionSubType]_[RecoveryCategory]_accnum
   * Recovery --> tdic:gl_[LOBCode]_[CostType]_[TransactionSubType]_[RecoveryCategory]_accnum
   * @param transaction
   * @param costType
   */
  static private function generateAccNumNameSpace(transaction : Transaction, costType : String, forReserve : Boolean = null) : String {
    var accNameSpace : String
    var claim = transaction.Claim
    var codesList = new ArrayList<String>()
    var lob : String = null
    switch(claim.LOBCode) {
      case LOBCode.TC_GLLINE:
      case LOBCode.TC_OTHER_LIAB:
        lob = PL_LINE
        break
      case LOBCode.TC_BOPLINE:
        lob = BOP_LINE
        break
      default:
        lob = WC_LINE
    }
    codesList.add(GL_ACCNUM_NAMESPACE)
    codesList.add(lob)
    codesList.add(costType)
    codesList.add(forReserve!=null && forReserve ? typekey.Transaction.TC_RESERVE.Code.toLowerCase() : transaction.Subtype.Code.toLowerCase())
    if(forReserve == null && transaction typeis Payment) {
      //Check for the payment has all the line categories belongs Subro Group.(Line category starts with Subro).
      if(transaction.LineItems.allMatch(\elt1 -> elt1.LineCategory?.Code?.startsWith(LINE_CATEGORY_SUBRO_GROUP)))
        codesList.add(LINE_CATEGORY_SUBRO_GROUP.toLowerCase())
    } else if(transaction typeis RecoveryReserve) {
      codesList.add(transaction.RecoveryCategory.Description.toLowerCase())
    } else if(transaction typeis Recovery) {
      codesList.add(transaction.RecoveryCategory.Description.toLowerCase())
    }
    codesList.add(ACCNUM)
    accNameSpace = codesList.HasElements ? codesList.join(CHAR_UNDERSCORE) : null
    _logger.debug("Method:generateAccNumNameSpace() - transaction: ${transaction.PublicID} " +
    "account name space is ${accNameSpace}")
    return accNameSpace
  }

  /**
   * Get cost type for WC,PL and BOP based on Exposure type.
   * @param transaction
   * @return costType.
   */
  private static function getCostType(transaction : Transaction) : String {
    var costType : String
    if(transaction.Claim.LOBCode == LOBCode.TC_WC7LINE) {
      if(transaction.Exposure == null)
        costType = WC_EXPENSE
      else if(transaction.Exposure.ExposureType == ExposureType.TC_WCINJURYDAMAGE)
        costType = WC_MEDICAL
      else if(transaction.Exposure.ExposureType == ExposureType.TC_LOSTWAGES)
        costType = WC_INDEMNITY
    } else {
      costType =
          MAPPER.getAliasByInternalCode(CostType.RelativeName, NAMESPACE_GL, transaction.CostType.Code)?.toLowerCase()
    }
    _logger.debug("Method:getCostType() - transaction: ${transaction.PublicID} cost type is ${costType}")
    return costType
  }

  /**
   * Function to populate Description and LOBCode data on GLTransactionDTO.
   * @param transaction
   * @param transactionDTO
   * @param costType
   */
  private static function populateDescriptionAndLOBCode(
      transaction : Transaction, transactionDTO : GLTransactionDTO, costType : String,forReserve : Boolean = null) {

    var claim = transaction.Claim
    var typeListName = ClaimType_TDIC.RelativeName
    var aliasName = claim.Type_TDIC.Code
    var lobNameSpace = NAMESPACE_GL_LOB_CODE
    var claimTypeNameSpace = NAMESPACE_GL
    if(claim.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY) {
      if(claim.Policy.Offerings_TDIC == Offering_TDIC.TC_PROFLIABCLAIMSMADE && claim.Policy.ERE_TDIC) {
        lobNameSpace = NAMESPACE_GL_LOB_CODE_ERE
        claimTypeNameSpace = NAMESPACE_GL_ERE
      } else if(isPolicyOfferingClaimsMadeOrOccurence(claim.Policy)) {
        typeListName = Offering_TDIC.RelativeName
        aliasName = claim.Policy.Offerings_TDIC.Code
      }
    } else if(isClaimTypePropertyAndCostCategoryEquipmentBreakdown(transaction)) {
      typeListName = CostCategory.RelativeName
      aliasName = transaction.CostCategory.Code
    }

    var lobCode = MAPPER.getAliasByInternalCode(typeListName, lobNameSpace, aliasName)
    var claimTypeCode = MAPPER.getAliasByInternalCode(typeListName, claimTypeNameSpace, aliasName)

    var stateCode = MAPPER.getInternalCodeByAlias(Jurisdiction.RelativeName, NAMESPACE_GL, transactionDTO.StateCode)
    var description = new StringBuilder().append(stateCode).append(CHAR_SPACE).
        append(claimTypeCode).append(CHAR_SPACE).append(costType.toUpperCase()).append(CHAR_SPACE)

    if(transaction typeis Payment)
      description.append(forReserve!=null && forReserve  ? RSV : PAY)
    else if(transaction typeis Reserve)
      description.append(RSV)
    else if(transaction typeis RecoveryReserve)
      description.append(RRS)
    else if(transaction typeis Recovery)
      description.append(REC)

    if(forReserve == null && isTrnasactionVoidedOrRecoded(transaction.Status)) {
      description.append(CHAR_SPACE)
          .append(MAPPER.getAliasByInternalCode(TransactionStatus.RelativeName, NAMESPACE_GL, transaction.Status.Code))
    }
    transactionDTO.Description = description.toString()
    transactionDTO.LOBCode = lobCode
    _logger.debug("Method:populateDescriptionAndLOBCode() - transaction: ${transaction.PublicID}" +
     " description is ${transactionDTO.Description}")
    _logger.debug(
        "Method:populateDescriptionAndLOBCode() - transaction: ${transaction.PublicID} cost type is ${lobCode}")
  }

  /**
   * Function to check policy offering type is ClaimsMade or Occurence.
   * @param policy
   * @return Boolean
   */
  private static function isPolicyOfferingClaimsMadeOrOccurence(policy : Policy) : Boolean {
    return (policy.Offerings_TDIC == Offering_TDIC.TC_PROFLIABCLAIMSMADE ||
        policy.Offerings_TDIC == Offering_TDIC.TC_PROFLIABOCCURENCE)
  }

  /**
   * Function to check transaction has Claim type is Property and Cost Category is EquipmentBreakdown.
   * @param transaction
   * @return
   */
  private static function isClaimTypePropertyAndCostCategoryEquipmentBreakdown(transaction : Transaction) : Boolean {
    return transaction.Claim.Type_TDIC == ClaimType_TDIC.TC_PROPERTY &&
        transaction.CostCategory == CostCategory.TC_EQUIPMENTBREAKDOWNDONTRESTRICTRESERVES_TDIC
  }

  /**
   * Function to check CC transaction status is voided or recoded.
   * @param transaction
   * @return Boolean
   */
  private static function isTrnasactionVoidedOrRecoded(transactionStatus : TransactionStatus) : Boolean {
    return  transactionStatus == TransactionStatus.TC_VOIDED || transactionStatus == TransactionStatus.TC_RECODED
  }

  /**
   * Functuion to wtite General Ledger outbound files to outbound directory.
   * Outbound file : TDIC-LA-PCM-MM-DD-YYYY-HHmmss.GLT
   */
  static function writeGeneralLedgerOutboundFiles() {

    _logger.debug("Enter TDIC_GeneralLedgerHelper.writeGeneralLedgerOutboundFiles()")
    var glDAO : TDIC_GeneralLedgerDAO

    try {
      glDAO = new TDIC_GeneralLedgerDAO()
      //Initialize DAO
      glDAO.initialize()

      //Fectch unprocessed CC transactions.
      var glTransactionFile = glDAO.fetchGLTransactions()

      if(glTransactionFile.GLTransactions.HasElements) {
        var glOutBoundFile = new File(getGLOutboundFileName(GL_OUTBOUND_FILE_EXTENSION))

        var glTransactions = convertDataToFlatFileLines(INVOICE_VENDOR_SPEC_FILE_NAME, glTransactionFile)

        FileUtils.writeLines(glOutBoundFile, glTransactions)
        _logger.debug("General Ledger outbound file ${glOutBoundFile} written successfully.")

        /**On sucessfull writing CC transactions to file, Update processed flag to true in
         * table GEINT:GLCCTransactions.*/
        glDAO.updateProcessedGLTransactions()
        _logger.debug("Updated processed CC transaction records status to true.")

        // Reset Transaction Sequence Number to 0 after writing General Ledger outbound files successfully.
        //resetTransactionSequenceNumber()
      }else {
        _logger.debug("There are no CC transaction records to write into General Ledger Outbound file")
      }
    } catch (ioe : IOException) {
      _logger.error("IOException caught in batch process: ", ioe)
      throw new TDIC_GeneralLedgerException(ioe.Message, ioe)
    } catch (ex : Exception) {
      _logger.error("Exception while writing CC transaction records to General Ledger Outbound file", ex)
      throw new TDIC_GeneralLedgerException(ex.Message, ex)
    } finally {
      //close any open connections
      glDAO.cleanConnections()
    }
    _logger.debug("Exit TDIC_GeneralLedgerHelper.writeGeneralLedgerOutboundFiles()")
  }

  /**
   * Converts data to the lines that can be written to the destination flat file.
   * @param vendorSpecName
   * @param flatFileData
   * @return List of processed rows.
   */
  @Param("vendorSpecName", "A String containing the name of the vendor spec spreadsheet to use when encoding the file")
  @Param("flatFileData", "The object containing the data to write to the destination flat file")
  @Returns("A List<String> containing the liens to be written to the destination flat file")
  @Throws(Exception, "If there are any unexpected errors encoding the data")
  static function convertDataToFlatFileLines(vendorSpecName : String, flatFileData : GLTransactionFile) : List<String> {
    var gxModel =
        new tdic.cc.integ.plugins.generalledger.model.gltransactionfilemodel.GLTransactionFile().create(flatFileData)
    _logger.debug("TDIC_GeneralLedgerHelper#convertDataToFlatFileLines - General Ledger GX Model Generated: ")
    _logger.debug(gxModel.asUTFString())
    var xml = XmlElement.parse(gxModel.asUTFString())
    var df = new DelimitedFileImpl()
    return df.encode(vendorSpecName, xml)
  }

  /**
   * Function to reset Transaction Sequence Number to 0.
   */
  private static function resetTransactionSequenceNumber() {

    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      var seqResult = Query.make(Sequence).compare(Sequence#SequenceKey, Equals, GL_TRANSACTION_SEQUENCE_KEY).select()
      if(seqResult.Count > 0) {
        var invoiceSequece = seqResult.AtMostOneRow
        bundle.add(invoiceSequece)
        invoiceSequece.SequenceNumber = 0
      }
    })
  }

  /**
   * Function to check the transaction has valid cost type.
   * @param transaction
   * @return
   */
  static function isValidCostTypeForGL(transaction : Transaction) : boolean {
    var result : boolean = false
    var lobCode = transaction.Claim.LOBCode
    var claimType = transaction.Claim.Type_TDIC
    var costType = transaction.CostType
    var onlySubroLines = transaction typeis Payment ?
        transaction.LineItems?.allMatch(\elt1 -> elt1.LineCategory?.Code?.startsWith(LINE_CATEGORY_SUBRO_GROUP)) : false
    if(claimType != ClaimType_TDIC.TC_WORKERSCOMPENSATION) {
      if(costType == CostType.TC_CLAIMCOST) {
        result = ((transaction typeis Payment && !(onlySubroLines)) ||
            (transaction typeis RecoveryReserve && (transaction.RecoveryCategory == RecoveryCategory.TC_REINSURANCE_TDIC ||
                transaction.RecoveryCategory == RecoveryCategory.TC_DEDUCTIBLE)) || transaction typeis Reserve)

      } else if(costType == CostType.TC_DCCEXPENSE) {
        result = (transaction typeis Payment || (transaction typeis RecoveryReserve &&
            transaction.RecoveryCategory == RecoveryCategory.TC_REINSURANCE_TDIC) || transaction typeis Reserve)
      }
    }
    return result
  }

  /**
   * Check the transaction has valid cost type for WC Line and GL.
   * @param transaction
   * @param hasOnlySubroLines
   * @return
   */
  private static function isWCTransactionHasValidCostType(
      transaction : Transaction, hasOnlySubroLines : boolean) : boolean {
    var result : boolean = false
    var exposure = transaction.Exposure?.ExposureType
    if (transaction.Exposure == null)
      result = (transaction typeis Payment ||
          (transaction typeis RecoveryReserve && transaction.RecoveryCategory ==
              RecoveryCategory.TC_REINSURANCE_TDIC) || transaction typeis Reserve ||
          (transaction typeis Recovery && (transaction.RecoveryCategory == RecoveryCategory.TC_SUBRO ||
          transaction.RecoveryCategory == RecoveryCategory.TC_REFUNDS_TDIC)))
    else if (exposure == ExposureType.TC_WCINJURYDAMAGE || exposure == ExposureType.TC_LOSTWAGES)
      result = ((transaction typeis Payment && !(hasOnlySubroLines))) || (transaction typeis RecoveryReserve &&
          transaction.RecoveryCategory == RecoveryCategory.TC_REINSURANCE_TDIC) || transaction typeis Reserve ||
          (transaction typeis Recovery && (transaction.RecoveryCategory == RecoveryCategory.TC_SUBRO ||
          transaction.RecoveryCategory == RecoveryCategory.TC_REFUNDS_TDIC))
    return result
  }
}