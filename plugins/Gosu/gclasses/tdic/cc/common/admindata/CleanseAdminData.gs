package tdic.cc.common.admindata

uses java.util.ArrayList
uses javax.xml.parsers.DocumentBuilderFactory
uses java.io.File
uses org.w3c.dom.Document;
uses java.io.FileInputStream
uses java.io.FileOutputStream
uses javax.xml.transform.dom.DOMSource
uses javax.xml.transform.TransformerFactory
uses javax.xml.transform.stream.StreamResult
uses org.w3c.dom.Element
uses org.w3c.dom.Node
uses java.util.HashMap
uses org.w3c.dom.Text
uses org.w3c.dom.NodeList

uses java.util.List

abstract class CleanseAdminData {
  private static var _directoryPath = "../../../config/datafiles";

  protected construct() {
  }

  /**
   * Create the correct instance for the specified product code.
   */
  public static function getInstance (gwProductCode : String) : CleanseAdminData {
    var instance : CleanseAdminData;

    switch (gwProductCode) {
      case "bc" :
          instance = new CleanseBCAdminData();
          break;
      case "cc" :
          instance = new CleanseCCAdminData();
          break;
      case "pc" :
          instance = new CleansePCAdminData();
          break;
      default:
        print ("ERROR:  Unable to create cleanse admin data instance for Guidewire product code: " + gwProductCode);
    }

    return instance;
  }

  /**
   * Cleanse the admin data.
   */
  public function cleanseAdminData() : void {
    var adminData : Document;

    for (filename in Filenames) {
      adminData = loadAdminDataFile(filename);

      cleanseAdminDataFile (filename, getImportElement(adminData));
      sortAdminDataFile (adminData);

      saveAdminDataFile (adminData, filename);
    }
  }

  /**
   * Get the list of admin data files.
   */
  protected property get Filenames() : List<String> {
    var retval = new ArrayList<String>();
    retval.add ("admin.xml");
    return retval;
  }

  /**
   * Load the admin data file.
   */
  private function loadAdminDataFile (filename : String) : Document {
    print ("Loading " + filename);

    var file = new File(_directoryPath + "/" + filename);

    var factory = DocumentBuilderFactory.newInstance();
    var builder = factory.newDocumentBuilder();
    var document = builder.parse(file);

    return document;
  }

  /**
   * Cleanse the admin data file.
   */
  protected function cleanseAdminDataFile (filename : String, import : Element) : void {
    if (filename == "admin.xml") {
      removeElements  (import, {"UserSettings"});

      removeSubElements (import, "Credential", {"FailedAttempts", "FailedTime", "LockDate"});
      removeSubElements (import, "User",       {"BackupUsers", "UserSettings", "VacationStatus"});
    }
  }

  /**
   * Sort the admin data file so version control can more easily show changes.
   */
  private function sortAdminDataFile (adminData : Document) : void {
    var element : Element;
    var elementList : List<Element>;
    var import = getImportElement(adminData);
    var node : Node;
    var text : Text;

    // Get elements
    var elements = new HashMap<String,List<Element>>();
    var emptyTextNodes = new ArrayList<Text>();
    var nodeList = import.ChildNodes;

    if (nodeList.Length > 0) {
      for (i in 0 .. (nodeList.Length-1)) {
        node = nodeList.item(i);
        if (node.NodeType == Node.ELEMENT_NODE) {
          element = node as Element;
          elementList = elements.get(element.NodeName);
          if (elementList == null) {
            elementList = new ArrayList<Element>();
            elements.put (element.NodeName, elementList);
          }
          elementList.add (element);
        } else if (node.NodeType == Node.TEXT_NODE) {
          text = node as Text;
          if (text.TextContent.trim() == "") {
            emptyTextNodes.add (text);
          }
        }
      }
    }

    // Sort elements
    for (elementName in elements.Keys) {
      elementList = elements.get(elementName);
      elementList = elementList.sortBy(\e -> e.getAttribute("public-id"));
      elements.put (elementName, elementList);
    }

    // Remove unsorted elements and empty text nodes
    for (elementName in elements.Keys) {
      elementList = elements.get(elementName);
      for (oldElement in elementList) {
        import.removeChild (oldElement);
      }
    }
    for (oldText in emptyTextNodes) {
      import.removeChild (oldText);
    }

    // Add sorted elements
    for (elementName in elements.Keys.order()) {
      elementList = elements.get(elementName);
      for (newElement in elementList) {
        text = adminData.createTextNode("\n  ");
        import.appendChild (text);
        import.appendChild (newElement);
      }
    }
    text = adminData.createTextNode("\n");
    import.appendChild (text);
  }

  /**
   * Save the admin data file.
   */
  private function saveAdminDataFile (adminData : Document, filename : String) : void {
    var backupFilename = filename.replace(".xml", "-backup.xml");

    // Create a backup of the original file.
    print ("Copying " + filename + " to " + backupFilename);
    copyFile (_directoryPath + "/" + filename, _directoryPath + "/" + backupFilename);

    // Save the file.
    print ("Saving " + filename);
    var transformerFactory = TransformerFactory.newInstance();
    var transformer = transformerFactory.newTransformer();
    var source = new DOMSource(adminData);
    var result = new StreamResult(new File(_directoryPath + "/" + filename));
    transformer.transform(source, result);

    if (filename.contains(" ")) {
      copyFile (_directoryPath + "/" + filename.replace(" ", "%20"), _directoryPath + "/" + filename);
      deleteFile (_directoryPath + "/" + filename.replace(" ", "%20"));
    }
  }

  /**
   * Copy a file.
   */
  private function copyFile (fromPath : String, toPath : String) : void {
    var fromFile = new File (fromPath);
    var toFile = new File (toPath);

    var is = new FileInputStream(fromFile);
    var os = new FileOutputStream(toFile);
    var buffer = new byte[1024];
    var len : int;

    len = is.read(buffer);
    while (len > 0) {
      os.write (buffer, 0, len);
      len = is.read(buffer);
    }

    is.close();
    os.close();
  }

  /**
   * Delete a file.
   */
  private function deleteFile (filePath : String) : void {
    var file = new File (filePath);
    file.delete();
  }

  /**
   * Remove elements
   */
  protected function removeElements (import : Element, elementNames : String[]) : void {
    var elementNodeList : NodeList;
    var node : Node;
    var removedNodes = new ArrayList<Node>();

    print ("Removing the following elements: " + elementNames.join(", "));

    elementNodeList = import.ChildNodes;
    for (i in 0 .. (elementNodeList.Length-1)) {
      node = elementNodeList.item(i);
      if (node.NodeType == Node.ELEMENT_NODE and elementNames.contains(node.NodeName)) {
        removedNodes.add (node);
        node = elementNodeList.item(i-1);
        if (node.NodeType == Node.TEXT_NODE and node.NodeValue.trim() == "") {
          removedNodes.add (node);
        }
      }
    }

    for (removedNode in removedNodes) {
      import.removeChild (removedNode);
    }
  }

  /**
   * Remove element sub-elements
   */
  protected function removeSubElements (import : Element, elementName : String, subElementNames : String[]) : void {
    var elementNodeList : NodeList;
    var node : Node;
    var removedNodes : List<Node>;

    print (elementName + " - Removing the following sub-elements: " + subElementNames.join(", "));

    for (element in getChildElements (import, elementName)) {
      removedNodes = new ArrayList<Node>();

      elementNodeList = element.ChildNodes;
      for (i in 0 .. (elementNodeList.Length-1)) {
        node = elementNodeList.item(i);
        if (node.NodeType == Node.ELEMENT_NODE and subElementNames.contains(node.NodeName)) {
          removedNodes.add (node);
          node = elementNodeList.item(i-1);
          if (node.NodeType == Node.TEXT_NODE and node.NodeValue.trim() == "") {
            removedNodes.add (node);
          }
        }
      }

      for (removedNode in removedNodes) {
        element.removeChild (removedNode);
      }
    }
  }

  /**
   * Get Child Elements
   */
  protected function getChildElements (element : Element, name : String) : List<Element> {
    var retval = new ArrayList<Element>();
    var node : Node;

    var nodeList = element.ChildNodes;
    for (i in 0 .. (nodeList.Length-1)) {
      node = nodeList.item(i);
      if (node.NodeType == Node.ELEMENT_NODE and node.NodeName == name) {
        retval.add (node as Element);
      }
    }

    return retval;
  }

  /**
   * Get Single Element
   */
  protected function getSingleElement (element : Element, name : String) : Element {
    var retval : Element;

    var elements = getChildElements (element, name);
    if (elements.Count == 1) {
      retval = elements.first();
    }

    return retval;
  }

  /**
   * Get Single Element Value
   */
  protected function getSingleElementValue (element : Element, name : String) : String {
    var retval : String;

    var subElement = getSingleElement (element, name);
    if (subElement != null) {
      var nodeList = subElement.ChildNodes;
      if (nodeList.Length == 1) {
        retval = nodeList.item(0).NodeValue;
      }
    }

    return retval;
  }

  /**
   * Get Import Element
   */
  private function getImportElement (adminData : Document) : Element {
    var retval : Element;

    var nodeList = adminData.getElementsByTagName("import");
    if (nodeList.Length == 1) {
      var node = nodeList.item(0);
      if (node.NodeType == Node.ELEMENT_NODE) {
        retval = node as Element;
      }
    }

    return retval;
  }
}