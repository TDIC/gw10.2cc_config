/**
 * This will delete the completed workflows older than the specified days.  This will delete any
 * work items, log messages, and the workflow itself. It does not deal with UserRoleAssignment nor
 * Activity (though it does set the Activity's reference to the workflow to null) and it does not
 * handle added foreign keys by default.  The customer can add foreign key extensions and links
 * using the ILinkPropertyInfo arguments stubbed out below.
 *
 * This is a permanent delete, as opposed to just retiring the workflow.
 *
 * @param WorkflowPurgeDaysOld the number of days that have passed since the last update of the workflow
 *
 * This executes directly against the database, bypassing global cache.
 */
package gw.processes

uses gw.api.system.PLConfigParameters
uses gw.api.admin.WorkflowUtil
uses gw.api.upgrade.Coercions
uses gw.entity.ILinkPropertyInfo
uses gw.api.database.Query

@Export
class PurgeWorkflows extends BatchProcessBase
{
  var _succDays = PLConfigParameters.WorkflowPurgeDaysOld.Value

  construct() {
    this(null)
  }
  
  construct(arguments : Object[]) {
    super(TC_PURGEWORKFLOWS)
    if (arguments != null) {
      _succDays = arguments[1] != null ? (Coercions.makeIntFrom(arguments[1])) : _succDays
    }
  }

  override property get Description() : String {
    return "purge(daysOld=${_succDays})"
  }

  override function doWork() : void {
    deleteAssociatedDocuments_TDIC()
    OperationsCompleted = WorkflowUtil.deleteOldWorkflowsFromDatabase( _succDays )
  }

  /**
   * This method will delete Related AssctdDocument Entries for all the Workflows the Batch job is going to Purge
   */
  function deleteAssociatedDocuments_TDIC() {
    var workflowQuery = Query.make(entity.Workflow)
    workflowQuery.compare("State", Equals, WorkflowState.TC_COMPLETED)
    workflowQuery.compare("Subtype", Equals, typekey.Workflow.TC_DOCUMENTAPPROVALWF_TDIC )
    workflowQuery.compare("UpdateTime", LessThan, Date.CurrentDate.addDays(-_succDays))
    var workflows = workflowQuery.select().toList()
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      for (wrkFlow in workflows) {
        var assctdDocumentQueryuery = Query.make(AssctdDocument)
        assctdDocumentQueryuery.compare("Owner", Equals, wrkFlow.ID)
        var result = assctdDocumentQueryuery.select().first()
        if (result != null) {
          bundle.delete(result)
        }
      }
    })
  }



}
