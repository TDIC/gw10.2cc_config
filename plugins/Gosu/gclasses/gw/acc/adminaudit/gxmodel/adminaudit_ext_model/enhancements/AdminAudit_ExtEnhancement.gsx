package gw.acc.adminaudit.gxmodel.adminaudit_ext_model.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AdminAudit_ExtEnhancement : gw.acc.adminaudit.gxmodel.adminaudit_ext_model.AdminAudit_Ext {
  public static function create(object : entity.AdminAudit_Ext) : gw.acc.adminaudit.gxmodel.adminaudit_ext_model.AdminAudit_Ext {
    return new gw.acc.adminaudit.gxmodel.adminaudit_ext_model.AdminAudit_Ext(object)
  }

  public static function create(object : entity.AdminAudit_Ext, options : gw.api.gx.GXOptions) : gw.acc.adminaudit.gxmodel.adminaudit_ext_model.AdminAudit_Ext {
    return new gw.acc.adminaudit.gxmodel.adminaudit_ext_model.AdminAudit_Ext(object, options)
  }

}