package gw.acc.adminaudit.util

uses gw.api.database. *
uses gw.api.path.Paths

uses java.lang.Exception

/*********************************************************************************
 *
 * Purpose:  Provides drop down list data for the admin audit search page and dynamically
 *           builds and executes the search query returning the search results
 *           to the UI
 *
 *********************************************************************************/
class AdminAuditSearchUtil {
  construct() {
  }
  private static var _LOGGER = org.slf4j.LoggerFactory.getLogger("gw.acc.adminaudit.util.AdminAuditSearchUtil")
  /******************************************************************************
   * Purpose: Creates unique array of entity names from audit data
   *
   * Parameters: none
   * Return: String[] - array of entity names
   ******************************************************************************/
  public static function modifiedEntities(): String[] {
    var retVal: String[]
    var i : int = 0
    try
    {
      var q = Query.make(AdminAudit_Ext)
      q.withDistinct(true)
      // retVal = q.select(\a -> a.ModifiedEntityName).toTypedArray()
      var resultSet = q.select({QuerySelectColumns.pathWithAlias("ModifiedEntityName", Paths.make(AdminAudit_Ext#ModifiedEntityName))
      }).toTypedArray()
      retVal = new String[resultSet.Count]{}
      for(res in resultSet){
        retVal[i] = res.getColumn("ModifiedEntityName").toString()
        i++
      }

    }
    catch (e: Exception)
    {
      _LOGGER.error(e.getMessage())
    }
    return retVal
  }

  /******************************************************************************
   * Purpose: Creates a unique array of user names who have made changes that
   * were captured by the auditing functionality
   *
   * Parameters: none
   * Return: String[] - array of entity names
   ******************************************************************************/
  public static function modifiedBy(): String[] {
    var retVal: String[]
    var i : int = 0
    try
    {
      var q = Query.make(AdminAudit_Ext)
      q.withDistinct(true)
      //retVal = q.select(\a -> a.ModifiedByUserName).toTypedArray()
      var resultSet = q.select({QuerySelectColumns.pathWithAlias("ModifiedUserName", Paths.make(AdminAudit_Ext#ModifiedByUserName))
      }).toTypedArray()
      retVal = new String[resultSet.Count]{}
      for(res in resultSet){
        retVal[i] = res.getColumn("ModifiedUserName").toString()
        i++
      }
    }
    catch (e: Exception)
    {
      _LOGGER.error(e.getMessage())
    }
    return retVal
  }

  public static function validSearchParam(searchCriteria: AdminAuditSearchCriteria): Boolean {
    var retVal = searchCriteria.StartDate != null || searchCriteria.EndDate != null || searchCriteria.EntityName != null || searchCriteria.ModifiedBy != null
    return retVal
  }

  /******************************************************************************
   * Purpose: Dynamically build and execute query based on provided search criteria
   *
   * Parameters: AdminAuditSearchCriteria - contains filter elements
   * Return: AdminAudit_ExtQuery - search results
   ******************************************************************************/
  public static function executeSearch(searchCriteria: gw.acc.adminaudit.util.AdminAuditSearchCriteria): IQueryBeanResult<AdminAudit_Ext> {
    var retVal: IQueryBeanResult<AdminAudit_Ext>

    if (!validSearchParam(searchCriteria))
      return null
    try
    {
      var q = gw.api.database.Query.make(AdminAudit_Ext)
      if (searchCriteria.ModifiedBy != null && searchCriteria.ModifiedBy != "")
      {
        q.compare(AdminAudit_Ext#ModifiedByUserName, Equals, searchCriteria.ModifiedBy)
      }
      if (searchCriteria.EntityName != null && searchCriteria.EntityName != "")
      {
        q.compare(AdminAudit_Ext#ModifiedEntityName, Equals, searchCriteria.EntityName)
      }
      //check to see if at least one date was provided
      if (searchCriteria.EndDate != null || searchCriteria.StartDate != null)
      {
        //knowing that at least one date value has been provided ensure that both are provided or defaulted
        if (searchCriteria.EndDate == null)
          searchCriteria.EndDate = searchCriteria.StartDate.addDays(1)
        if (searchCriteria.StartDate == null)
          searchCriteria.StartDate = searchCriteria.EndDate.addDays(- 1)
        //the between function will not typically give the results an end user will expect as it will not include the
        //current day's values (e.g. if EndDate is 1/10/2011 it will not include audited values on 1/10/2011).  So, locally
        //modify the end date for the query
        var end = searchCriteria.EndDate.addDays(1)
        q.between(AdminAudit_Ext#ModifiedDate, searchCriteria.StartDate, end)
      }
      retVal = q.select()
      retVal.setPageSize(1)//ScriptParameters.AdminAuditSearchResultLimit)
    }
    catch (e: Exception)
    {
      _LOGGER.error(e.getMessage())
    }
    return retVal
  }
}
