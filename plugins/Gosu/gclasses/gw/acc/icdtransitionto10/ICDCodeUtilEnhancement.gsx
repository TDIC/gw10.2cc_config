package gw.acc.icdtransitionto10

uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.util.DisplayableException
uses gw.api.locale.DisplayKey

/**
 * Adds updated methods to {@link ICDCodeUtil} to add support for ICD-9.
 */
enhancement ICDCodeUtilEnhancement : libraries.ICDCodeUtil {
  @Param("queryCode", "The code to search for")
  @Param("queryBodySystem", "The body system to search for")
  @Param("queryDesc", "The text to search for in the description")
  @Param("queryFor9", "Include ICD-9 codes?")
  @Param("queryFor10", "Include ICD-10 codes?")
  @Param("adminQuery", "Ignore effective dates?")
  @Returns("A query result containing matching codes")
  static function queryICD(queryCode : String, queryBodySystem : ICDBodySystem, queryDesc : String,
                           queryFor9 : boolean, queryFor10 : boolean, adminQuery : boolean) : IQueryBeanResult<entity.ICDCode>  {
    var icdQuery = Query.make(entity.ICDCode)
    if ((queryCode == null) and (queryBodySystem == null) and (queryDesc == null)) {
      throw new DisplayableException(DisplayKey.get("Accelerator.ICDTransitionTo10.ICDCode.InsufficientICDCodeSearchCriteria"))
    }
    if (not (queryFor9 or queryFor10)) {
      throw new DisplayableException(DisplayKey.get("Accelerator.ICDTransitionTo10.ICDCode.MustIncluedAtLeastOneEdition"))
    }
    if (queryCode != null) {
      icdQuery.startsWith(ICDCode#Code, queryCode, true)
    }
    if (queryBodySystem != null) {
      icdQuery.compare(ICDCode#BodySystem, Relop.Equals, queryBodySystem )
    }
    if (queryDesc != null) {
      icdQuery.contains(ICDCode#CodeDesc, queryDesc, true)
    }
    if (not queryFor10) {
      icdQuery.compare(ICDCode#ICDEdition_Ext, Relop.Equals, ICDEdition_Ext.TC_ICD9)
    } else if (not queryFor9) {
      icdQuery.compare(ICDCode#ICDEdition_Ext, Relop.Equals, ICDEdition_Ext.TC_ICD10)
    }
    if (not adminQuery) {
      var currDate = gw.api.util.DateUtil.currentDate()
      icdQuery.and(\ r -> {
        r.or(\ restrict -> restrict.compare(ICDCode#AvailabilityDate, Equals, null)
            .compare(ICDCode#AvailabilityDate, Relop.LessThanOrEquals, currDate))
        r.or(\ restrict -> restrict.compare(ICDCode#ExpiryDate, Equals, null)
            .compare(ICDCode#ExpiryDate, Relop.GreaterThanOrEquals, currDate))
      })
    }

    return icdQuery.select()
  }
}
