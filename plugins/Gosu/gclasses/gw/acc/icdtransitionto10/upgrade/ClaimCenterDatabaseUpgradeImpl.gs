package gw.acc.icdtransitionto10.upgrade

uses gw.api.database.upgrade.DatamodelChangeWithoutArchivedDocumentChange
uses gw.api.datamodel.upgrade.CustomerDatamodelUpgrade
uses gw.plugin.upgrade.IDatamodelUpgrade
uses java.util.List

/**
 * A {@link IDatamodelUpgrade} plug-in that updates the ICDCode table with a
 * version number.
 */
class ClaimCenterDatabaseUpgradeImpl extends CustomerDatamodelUpgrade
    implements IDatamodelUpgrade {
  override property get BeforeUpgradeDatamodelChanges()
      : List<gw.api.datamodel.upgrade.IDatamodelChange<gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger>> {
    return {}
  }

  override property get AfterUpgradeDatamodelChanges()
      : List<gw.api.datamodel.upgrade.IDatamodelChange<gw.api.database.upgrade.after.AfterUpgradeVersionTrigger>> {
    // Insert ICD Edition number as 10 for existing rows
    return { DatamodelChangeWithoutArchivedDocumentChange.make(new ICDCodeTableAddEditionValue()) }
  }
}