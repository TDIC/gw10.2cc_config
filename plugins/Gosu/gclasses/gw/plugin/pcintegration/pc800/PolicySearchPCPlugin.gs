package gw.plugin.pcintegration.pc800

uses gw.api.util.DisplayableException
uses gw.api.util.LocaleUtil
uses gw.plugin.InitializablePlugin
uses gw.plugin.policy.search.IPolicySearchAdapter
uses wsi.remote.gw.webservice.pc.pc800.ccpolicysearchintegration.CCPolicySearchIntegration
uses wsi.remote.gw.webservice.pc.pc800.entities.types.complex.CCPCFilteringCriteria
uses wsi.remote.tdic.webservice.bc.policybilling.tdic_policybillingapi.TDIC_PolicyBillingAPI
uses wsi.remote.tdic.webservice.bc.policybilling.tdic_policybillingapi.anonymous.elements.TDIC_PolicyBillingRequest_PolicyNumbersList
uses wsi.remote.tdic.webservice.bc.policybilling.tdic_policybillingapi.types.complex.TDIC_PolicyBillingRequest
uses java.lang.Throwable
uses java.util.ArrayList
uses java.util.Date
uses java.util.HashSet
uses java.util.Map
uses gw.api.locale.DisplayKey

/**
 * Implementation of the PolicySearchAdapter that calls into PC.
 */
@Export
class PolicySearchPCPlugin implements IPolicySearchAdapter, InitializablePlugin {

  static final var unsupportedPolicyTypes = new HashSet<String>() { PolicyType.TC_FARMOWNERS.Code,
                                                                    PolicyType.TC_PROF_LIABILITY.Code,
                                                                    PolicyType.TC_TRAVEL_PER.Code }

  /**
   * Retrieves the policy indicated by policySummary from the PC instance.
   */
  override function retrievePolicyFromPolicySummary( policySummary : PolicySummary ) : PolicyRetrievalResultSet {
    if( policySummary.PolicyType != null && !isSupportedPolicyType( policySummary.PolicyType ) ) {
      throw new DisplayableException(DisplayKey.get("Java.PolicyRefresh.UnsupportedPolicyType", policySummary.PolicyType ))
    }
    
    var pcFilter = new CCPCFilteringCriteria()
    //if you can't select risk units for the specific policySummary, add all risk units to the summary
    //else filter the risk units by selection
    if(!policySummary.CanSelectRiskUnits){
      if(policySummary.Vehicles!=null){
        policySummary.Vehicles.each( \ vehicle -> vehicle.setFieldValue("Selected",true))
      }
      if(policySummary.Properties!=null){
        policySummary.Properties.each( \ aProperty -> aProperty.setFieldValue("Selected",true))
      }
    }else{
      pcFilter.PolicySystemIDs.Entry = createRiskUnitFilter(policySummary).toList()
    }
    return retrievePolicy( policySummary.PolicyNumber, policySummary.LossDate, pcFilter )
  }
  
  /**
   * Retrieves the policy again from the PC instance.
   */
  override function retrievePolicyFromPolicy( policy : Policy ) : PolicyRetrievalResultSet   {
    if( policy.PolicyType != null && !isSupportedPolicyType( policy.PolicyType ) )    {
      throw new DisplayableException( DisplayKey.get("Java.PolicyRefresh.UnsupportedPolicyType", policy.PolicyType ))
    }
    return retrievePolicy( policy.PolicyNumber, policy.Claim.LossDate, new CCPCFilteringCriteria() )
  }
  
  /**
   * Search for policies on the PC instance given the search criteria.
   */
  override function searchPolicies( criteria : PolicySearchCriteria ) : PolicySearchResultSet   {
    if( criteria.PolicyType != null && !isSupportedPolicyType( criteria.PolicyType ) )    {
      throw new DisplayableException(DisplayKey.get("Java.PolicyRefresh.UnsupportedPolicyType", criteria.PolicyType ))
    }
    var pcCriteria = PolicySearchConverter.INSTANCE.createPCSearchCriteria( criteria )

    // Just create an empty filter by default
    var pcFilter = new CCPCFilteringCriteria()

    var pcSummaries = PolicySearchService.searchForPolicies( pcCriteria, pcFilter )
    var ccSummaries = PolicySearchConverter.INSTANCE.convertPCPolicySummary( pcSummaries )

    ccSummaries.each(\ s -> { s.LossDate = criteria.LossDate})
    //US1137 - Kesava Tavva - Filter PolicySummary list by its PaidThroughDate by retrieving it from BillingCenter.
    if(criteria.LossDate != null && ccSummaries.HasElements)    {
      ccSummaries = filterByPaidThroughDate(ccSummaries, criteria.LossDate)
    }

    var resultSet = new PolicySearchResultSet()
    resultSet.Summaries = ccSummaries
    return resultSet
  }

  function searchPoliciesForMigration( criteria : PolicySearchCriteria ) : PolicySearchResultSet   {
    if( criteria.PolicyType != null && !isSupportedPolicyType( criteria.PolicyType ) )    {
      throw new DisplayableException(DisplayKey.get("Java.PolicyRefresh.UnsupportedPolicyType", criteria.PolicyType ))
    }

    var pcCriteria = PolicySearchConverter.INSTANCE.createPCSearchCriteria( criteria )
    // Just create an empty filter by default
    var pcFilter = new CCPCFilteringCriteria()

    var pcSummaries = PolicySearchService.searchForPolicies( pcCriteria, pcFilter )
    var ccSummaries = PolicySearchConverter.INSTANCE.convertPCPolicySummary( pcSummaries )
    ccSummaries.each(\ s -> { s.LossDate = criteria.LossDate})

    var resultSet = new PolicySearchResultSet()
    resultSet.Summaries = ccSummaries
    return resultSet
  }
  
  /**
   * Retrieves the policy summary for the policy from the PC instance.
   */
  override function retrievePolicySummaryFromPolicy(policy : Policy) : PolicySummary {
    var criteria = new PolicySearchCriteria()
    criteria.LossDate = policy.Claim.LossDate
    criteria.PolicyNumber = policy.PolicyNumber
    
    var results = searchPolicies(criteria)
    var numResults = results.Summaries.Count
    if (numResults == 0)
      return null
    var policySummary = results.Summaries[0]
    policySummary.LossDate = criteria.LossDate
    return policySummary
  }

  // create the list of risk unit psids to pass to the policy system as a filter
  private function createRiskUnitFilter(policySummary : PolicySummary) : String[] {
  // set the policy system id filter based on policySummary selections
  var psidFilter = new ArrayList<String>()
  if(policySummary.getVehicles()!=null){
    for (var vehicle in policySummary.Vehicles) {
      if (vehicle.Selected){
        psidFilter.add(vehicle.PolicySystemId)
      }
    }
  }
  if(policySummary.getProperties()!=null){
    for (var aProperty in policySummary.Properties) {
      if (aProperty.Selected){
        psidFilter.add(aProperty.PolicySystemId)
      }
    }
  }

  return psidFilter?.toTypedArray()
}

  // lazily initializes the policy search service

  private property get PolicySearchService() : CCPolicySearchIntegration  {
      //create new ws client on each call
      var pcSearchService = new CCPolicySearchIntegration()
      //set the locale header
      pcSearchService.Config.Guidewire.Locale = LocaleUtil.CurrentLanguage.toString()
      return pcSearchService
    }

  /**
   * retrieves the policy indicated by the policynumber and lossDate
   */
  private function retrievePolicy( policyNumber : String, lossDate: Date, pcFilter : CCPCFilteringCriteria ) : PolicyRetrievalResultSet   {
    if (lossDate == null) {
      throw new DisplayableException(DisplayKey.get("Java.PolicyItemHandler.LossDateRequired"))
    }

    var resultSet = new PolicyRetrievalResultSet()
    try {
      var env = PolicySearchService.retrievePolicy( policyNumber, lossDate, pcFilter )
      resultSet.Result = PolicySearchConverter.INSTANCE.convertPCPolicy( env.CCPolicy.$TypeInstance )
      // US22, robk: All Policy Contacts should have CM status of "Not linked to the Address Book"
      for (aPolicyContact in resultSet.Result.Contacts) {
        aPolicyContact.Contact.AddressBookUID = null
      }
    }
    catch( e : Throwable ) {
      throw new DisplayableException( DisplayKey.get("Java.PolicyRefresh.ErrorRetrieving"), e )
    }
    
    if(PolicyStatus.TC_ARCHIVED == resultSet.Result.Status) {
      throw new DisplayableException(DisplayKey.get("Java.PolicyRefresh.PolicyIsArchived"))
    }
    resultSet.NotUnique = false
    return resultSet
  }

  private function isSupportedPolicyType( policyType : PolicyType ) : boolean  {
    return !unsupportedPolicyTypes.contains(policyType.Code)
  }

  /*
  override function setParameters(p0 : Map<Object, Object>) {
    //## todo: Implement me
  }*/

  /**
   * US1137
   * 03/04/2015 Kesava Tavva
   *
   * Property to get instance of PolicyBillingService API
   */
  @Returns("TDIC_PolicyBillingAPI, Instance of TDIC_PolicyBillingAPI")
  private property get PolicyBillingService() : TDIC_PolicyBillingAPI  {
    //create new ws client on each call
    return new TDIC_PolicyBillingAPI()
  }

  /**
   * US1137
   * 03/04/2015 Kesava Tavva
   * GW-445 11/09/2015 Praneeth
   * Retrieve Paid through dates, FirstInvoicePaidFull and UnpaidPolicyChangeInvoice from Billing Center for list of policies from Policy summary list.
   * Compare Loss date, Paidthroughdate, FirstInvoicePaidFull and UnpaidPolicyChangeInvoice to filter Policy summary search results.
   */
  @Param("ccSummaries","List of PolicySummary results")
  @Param("lossDate", "Claim loss date")
  @Returns("PolicySummary[], List of PolicySummary results filtered by comparing its PaidThroughDate from BillingCenter")
  @Throws(DisplayableException, "If communication error or any other SOAP problem occurs.")
  private function filterByPaidThroughDate(ccSummaries : PolicySummary[], lossDate : Date) : PolicySummary[] {
    var filteredSummaries = new ArrayList<PolicySummary>()
    try{
       var req = new TDIC_PolicyBillingRequest()
      req.PolicyNumbersList = new TDIC_PolicyBillingRequest_PolicyNumbersList()
      ccSummaries.each( \ summary -> { req.PolicyNumbersList.Entry.add(summary.PolicyNumber) })
      var response = PolicyBillingService.getPaidThroughDatesForPolicies(req)
      var ccSummary : PolicySummary
      for (result in response.PolicyBillingDTOList.Entry){
        ccSummary = ccSummaries.firstWhere( \ summary -> summary.PolicyNumber == result.PolicyNumber)
        ccSummary.PaidThruDate_TDIC = result.PaidThroughDate
        ccSummary.CancellationDate_TDIC = result.CancellationDate
        ccSummary.FirstInvoicePaidFull_TDIC = result.FirstInvoicePaidFull?YesNo.TC_YES:YesNo.TC_NO
        ccSummary.UnpaidPolicyChangeInvoice_TDIC = result.UnpaidPolicyChangeInvoice?YesNo.TC_YES:YesNo.TC_NO

        if(result.CancellationDate == null){
          if(ccSummary.LossDate <= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(true) and result.UnpaidPolicyChangeInvoice.equals(true)){
            filteredSummaries.add(ccSummary)
          }
          else if(ccSummary.LossDate <= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(true) and result.UnpaidPolicyChangeInvoice.equals(false)){
            filteredSummaries.add(ccSummary)
          }
          else if(ccSummary.LossDate <= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(false) and result.UnpaidPolicyChangeInvoice.equals(true)){
            filteredSummaries.add(ccSummary)
          }
          else if(ccSummary.LossDate <= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(false) and result.UnpaidPolicyChangeInvoice.equals(false)){
            filteredSummaries.add(ccSummary)
          }
          else if(ccSummary.LossDate >= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(false) and result.UnpaidPolicyChangeInvoice.equals(true)){
            filteredSummaries.add(ccSummary)
          }
          else if(ccSummary.LossDate >= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(true) and result.UnpaidPolicyChangeInvoice.equals(true)){
            filteredSummaries.add(ccSummary)
          }
        }
        else if (ccSummary.LossDate < result.CancellationDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(true)){
           filteredSummaries.add(ccSummary)
        }
      }
    }catch(e : Throwable){
      throw new DisplayableException( DisplayKey.get("Java.PolicyRefresh.ErrorRetrievingPaidThroughDates", e.Message))
    }
    return filteredSummaries?.toTypedArray()
  }

  override property set Parameters(map : Map) {

  }
}
