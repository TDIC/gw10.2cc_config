package gw.plugin.pcintegration.pc1000.mapping

uses java.math.BigDecimal

class PLLegacyCovTermDataMap_TDIC {

  var eventname : String as EVENTNAME
  var eventstartdate : Date as EVENTSTARTDATE
  var eventenddate : Date as EVENTENDDATE
  var address : String as ADDRESS
  var onsite : String as ONSITE
  var managers_lessors_or_premises : String as MANAGERS_LESSORS_OR_PREMISES
  var equipment_lessors : String as EQUIPMENT_LESSORS
  var GOVERNMENT_AGENCIES_OR_SUBDIVISIONS : String as GOVERNMENT_AGENCIES_OR_SUBDIVISION
  var cda_pda : String as CDA_PDA
  var endoreffdt : Date as ENDOREFFDT
  var endorexpdt : Date as ENDOREXPDT
  var nameofdentist : String as NAMEOFDENTIST
  var additionalinsured : String as ADDITIONALINSURED
  var nameofdoctor : String as NAMEOFDOCTOR
  var State : String as STATE
  var certificate_holder : String as CERTIFICATE_HOLDER
  var covtermpatterncode : String as COVTERMPATTERNCODE
  var covTermValue : String as COVTERMVALUE
  var covTermStringValue : String as COVTERMSTRINGVALUE
  var covTermDateValue : String as COVTERMDATEVALUE

}