package gw.plugin.pcintegration.pc1000.mapping

/**
 * GINTEG-1178 : This DTO will hold Legacy Policy data from Legacy Data Mart
 */
class BOPLegacyPolDataMapper_TDIC {

  var cccontact_feinofficialid : String as CCCONTACT_FEINOFFICIALID
  var cccontact_name : String as CCCONTACT_NAME
  var cccontact_taxid : String as CCCONTACT_TAXID
  var cccontact_vendornumber : String as CCCONTACT_VENDORNUMBER
  var officialids_officialidinsuredandtype : String as OFFICIALIDS_OFFICIALIDINSUREDANDTYPE
  var OfficialIDs_OfficialIDType : String as OFFICIALIDS_OFFICIALIDTYPE
  var ContactType : String as CONTACTTYPE
  var OfficialIDs_OfficialIDValue : String as OFFICIALIDS_OFFICIALIDVALUE
  var ccpolicy_currency : String as CCPOLICY_CURRENCY
  var ccpolicy_effectivedate : Date as CCPOLICY_EFFECTIVEDATE
  var ccpolicy_expirationdate : Date as CCPOLICY_EXPIRATIONDATE
  var ccpolicy_originaleffectivedate : Date as CCPOLICY_ORIGINALEFFECTIVEDATE
  var ccpolicy_policynumber : String as CCPOLICY_POLICYNUMBER
  var ccpolicy_policysuffix : String as CCPOLICY_POLICYSUFFIX
  var ccpolicy_policytype : String as CCPOLICY_POLICYTYPE
  var ccpolicy_producercode : String as CCPOLICY_PRODUCERCODE
  var ccpolicy_status : String as CCPOLICY_STATUS
  var ccpolicy_totalproperties : int as CCPOLICY_TOTALPROPERTIES
  var ccpolicy_totalvehicles : int as CCPOLICY_TOTALVEHICLES
  var ccpolicy_underwrittingco : String as CCPOLICY_UNDERWRITTINGCO
  var ccpolicy_industrycode_tdic : String as CCPOLICY_INDUSTRYCODE_TDIC
  var pcholder_addressline1 : String as PCHOLDER_ADDRESSLINE1
  var pcholder_addressline2	: String as PCHOLDER_ADDRESSLINE2
  var pcholder_city : String as PCHOLDER_CITY
  var pcholder_state : String as PCHOLDER_STATE
  var pcholder_postalcode : String as PCHOLDER_POSTALCODE

  var CCAddress_AddressType : String as CCADDRESS_ADDRESSTYPE
  var PCLoc_AddressLine1 : String as PCLOC_ADDRESSLINE1
  var PCLoc_AddressLine2 : String as PCLOC_ADDRESSLINE2
  var PCLoc_City : String as PCLOC_CITY
  var PCLoc_State : String as PCLOC_STATE
  var PCLoc_PostalCode : String as PCLOC_POSTALCODE
  var CCBuilding_BuildingNumber : int as BUILDINGNUMBER
  var CCContact_LastName : String as CCCONTACT_LASTNAME
  var CCContact_FirstName : String as CCCONTACT_FIRSTNAME
  var CCContact_OrgName : String as CCCONTACT_ORGNAME
  var LocationNumber : int as LOCATIONNUMBER
  var PrimaryLocation : String as PRIMARYLOCATION

  public var riskUnits : List<BOPLegacyRUMapper_TDIC> as BUILDINGS

}