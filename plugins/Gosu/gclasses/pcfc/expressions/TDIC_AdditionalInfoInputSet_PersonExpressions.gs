package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.contact.ContactHandle
@javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_AdditionalInfoInputSet.Person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_AdditionalInfoInputSet_PersonExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_AdditionalInfoInputSet.Person.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_AdditionalInfoInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_47 () : void {
      AddressBookPickerPopup.push(statictypeof (Person.Employer), claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_49 () : void {
      if (Person.Employer != null) { ClaimContactDetailPopup.push(Person.Employer, claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_50 () : void {
      ClaimContactDetailPopup.push(Person.Employer, claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_48 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Person.Employer), claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_51 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Person.Employer, claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_44 (def :  pcf.ClaimNewCompanyOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Person.Employer), Person, claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_45 (def :  pcf.ClaimNewCompanyOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Person.Employer), Person, claim)
    }
    
    // 'value' attribute on TypeKeyInput (id=TaxFilingStatus_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 46, column 42
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.TaxFilingStatus = (__VALUE_TO_SET as typekey.TaxFilingStatusType)
    }
    
    // 'value' attribute on DateInput (id=DateOfBirth_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 54, column 42
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.DateOfBirth = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Gender_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 62, column 42
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.Gender = (__VALUE_TO_SET as typekey.GenderType)
    }
    
    // 'value' attribute on PrivacyInput (id=TaxID_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 39, column 42
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.TaxID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 78, column 93
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.PreferredCurrency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on TextInput (id=Occupation_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 87, column 34
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.Occupation = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_54 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.Employer = (__VALUE_TO_SET as entity.Company)
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=TaxID_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 39, column 42
    function encryptionExpression_5 (VALUE :  java.lang.String) : java.lang.String {
      return Person.maskTaxId(VALUE)
    }
    
    // 'label' attribute on TextInput (id=Occupation_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 87, column 34
    function label_36 () : java.lang.Object {
      return tdic.cc.config.fnol.TDIC_WC7FNOLHelper.getOccupationLabel(contactRole)
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_52 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Person.Employer); var result = eval("Person.Employer = claim.resolveContact(Person.Employer) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'required' attribute on PrivacyInput (id=TaxID_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 39, column 42
    function required_1 () : java.lang.Boolean {
      return claim.LossType == LossType.TC_WC7 ? tdic.cc.config.fnol.TDIC_WC7FNOLHelper.isTaxIdRequired(contactRole):false
    }
    
    // 'required' attribute on DateInput (id=DateOfBirth_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 54, column 42
    function required_16 () : java.lang.Boolean {
      return claim.LossType == LossType.TC_WC7 ? tdic.cc.config.fnol.TDIC_WC7FNOLHelper.isDOBRequired(contactRole):false
    }
    
    // 'required' attribute on TypeKeyInput (id=Gender_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 62, column 42
    function required_24 () : java.lang.Boolean {
      return claim.LossType == LossType.TC_WC7 ? tdic.cc.config.fnol.TDIC_WC7FNOLHelper.isGenderRequired(contactRole) :false
    }
    
    // 'required' attribute on TextInput (id=Occupation_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 87, column 34
    function required_37 () : java.lang.Boolean {
      return claim.LossType == LossType.TC_WC7 ? tdic.cc.config.fnol.TDIC_WC7FNOLHelper.isOccupationRequired(contactRole) :false
    }
    
    // 'validationExpression' attribute on DateInput (id=DateOfBirth_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 54, column 42
    function validationExpression_14 () : java.lang.Object {
      return (Person.DateOfBirth == null or Person.DateOfBirth <= gw.api.upgrade.Coercions.makeDateFrom("today")) ? null : DisplayKey.get("Web.ContactDetail.AdditionalInfo.DateOfBirth.FutureError")
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_56 () : java.lang.Object {
      return claim.RelatedCompanyArray
    }
    
    // 'value' attribute on PrivacyInput (id=TaxID_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 39, column 42
    function valueRoot_4 () : java.lang.Object {
      return Person
    }
    
    // 'value' attribute on DateInput (id=DateOfBirth_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 54, column 42
    function value_17 () : java.util.Date {
      return Person.DateOfBirth
    }
    
    // 'value' attribute on PrivacyInput (id=TaxID_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 39, column 42
    function value_2 () : java.lang.String {
      return Person.TaxID
    }
    
    // 'value' attribute on TypeKeyInput (id=Gender_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 62, column 42
    function value_25 () : typekey.GenderType {
      return Person.Gender
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 78, column 93
    function value_31 () : typekey.Currency {
      return Person.PreferredCurrency
    }
    
    // 'value' attribute on TextInput (id=Occupation_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 87, column 34
    function value_38 () : java.lang.String {
      return Person.Occupation
    }
    
    // 'value' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_53 () : entity.Company {
      return Person.Employer
    }
    
    // 'value' attribute on TypeKeyInput (id=TaxFilingStatus_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 46, column 42
    function value_9 () : typekey.TaxFilingStatusType {
      return Person.TaxFilingStatus
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_57 ($$arg :  entity.Company[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_57 ($$arg :  gw.api.database.IQueryBeanResult<entity.Company>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_57 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_58 () : void {
      var __valueRangeArg = claim.RelatedCompanyArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_57(__valueRangeArg)
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Organization_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 98, column 35
    function verifyValueType_62 () : void {
      var __valueTypeArg : entity.Company
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'visible' attribute on PrivacyInput (id=TaxID_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 39, column 42
    function visible_0 () : java.lang.Boolean {
      return isAdditionalInfoVisible
    }
    
    // 'visible' attribute on TypeKeyInput (id=Currency_Input) at TDIC_AdditionalInfoInputSet.Person.pcf: line 78, column 93
    function visible_30 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode() and isAdditionalInfoVisible
    }
    
    // 'visible' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_43 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=Organization_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_46 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Person.Employer), claim, null as List<SpecialistService>)" != "" && false
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get claimContact () : ClaimContact {
      return getRequireValue("claimContact", 0) as ClaimContact
    }
    
    property set claimContact ($arg :  ClaimContact) {
      setRequireValue("claimContact", 0, $arg)
    }
    
    property get contactHandle () : gw.api.contact.ContactHandle {
      return getRequireValue("contactHandle", 0) as gw.api.contact.ContactHandle
    }
    
    property set contactHandle ($arg :  gw.api.contact.ContactHandle) {
      setRequireValue("contactHandle", 0, $arg)
    }
    
    property get contactRole () : typekey.ContactRole {
      return getRequireValue("contactRole", 0) as typekey.ContactRole
    }
    
    property set contactRole ($arg :  typekey.ContactRole) {
      setRequireValue("contactRole", 0, $arg)
    }
    
    property get isAdditionalInfoVisible () : boolean {
      return getRequireValue("isAdditionalInfoVisible", 0) as java.lang.Boolean
    }
    
    property set isAdditionalInfoVisible ($arg :  boolean) {
      setRequireValue("isAdditionalInfoVisible", 0, $arg)
    }
    
    
    property get Person() : Person { return contactHandle.Contact as Person; }
    
    
  }
  
  
}