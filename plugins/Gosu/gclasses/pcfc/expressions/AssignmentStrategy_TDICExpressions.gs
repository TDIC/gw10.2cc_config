package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/AssignmentStrategy_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AssignmentStrategy_TDICExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/AssignmentStrategy_TDIC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AssignmentStrategy_TDICExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=AssignmentStrategy_NewAssignmentStrategyButton) at AssignmentStrategy_TDIC.pcf: line 22, column 85
    function action_1 () : void {
      NewAssignmentStrategy.go()
    }
    
    // 'action' attribute on ToolbarButton (id=AssignmentStrategy_NewAssignmentStrategyButton) at AssignmentStrategy_TDIC.pcf: line 22, column 85
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewAssignmentStrategy.createDestination()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=ASRecords_DeleteButton) at AssignmentStrategy_TDIC.pcf: line 28, column 62
    function allCheckedRowsAction_4 (CheckedValues :  entity.AssignStrategy_TDIC[], CheckedValuesErrors :  java.util.Map) : void {
      new tdic.cc.config.assignment.AssignmentStrategyUtil().removeAssignmentStrategies(CheckedValues)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=ASRecords_DeleteButton) at AssignmentStrategy_TDIC.pcf: line 28, column 62
    function available_3 () : java.lang.Boolean {
      return perm.ActivityPattern.delete
    }
    
    // 'def' attribute on PanelRef at AssignmentStrategy_TDIC.pcf: line 31, column 51
    function def_onEnter_5 (def :  pcf.ASRecordsLV) : void {
      def.onEnter(AssignStrategyRecords)
    }
    
    // 'def' attribute on PanelRef at AssignmentStrategy_TDIC.pcf: line 31, column 51
    function def_refreshVariables_6 (def :  pcf.ASRecordsLV) : void {
      def.refreshVariables(AssignStrategyRecords)
    }
    
    // 'initialValue' attribute on Variable at AssignmentStrategy_TDIC.pcf: line 13, column 75
    function initialValue_0 () : gw.api.database.IQueryBeanResult<AssignStrategy_TDIC> {
      return gw.api.database.Query.make(entity.AssignStrategy_TDIC).select()
    }
    
    // Page (id=AssignmentStrategy_TDIC) at AssignmentStrategy_TDIC.pcf: line 7, column 75
    static function parent_7 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    property get AssignStrategyRecords () : gw.api.database.IQueryBeanResult<AssignStrategy_TDIC> {
      return getVariableValue("AssignStrategyRecords", 0) as gw.api.database.IQueryBeanResult<AssignStrategy_TDIC>
    }
    
    property set AssignStrategyRecords ($arg :  gw.api.database.IQueryBeanResult<AssignStrategy_TDIC>) {
      setVariableValue("AssignStrategyRecords", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.AssignmentStrategy_TDIC {
      return super.CurrentLocation as pcf.AssignmentStrategy_TDIC
    }
    
    
  }
  
  
}