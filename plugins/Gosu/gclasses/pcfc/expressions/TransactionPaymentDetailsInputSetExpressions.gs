package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/financials/transactions/TransactionPaymentDetailsInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransactionPaymentDetailsInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/financials/transactions/TransactionPaymentDetailsInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransactionPaymentDetailsInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on CheckBoxInput (id=CMSFlag_Input) at TransactionPaymentDetailsInputSet.pcf: line 18, column 37
    function valueRoot_1 () : java.lang.Object {
      return Payment
    }
    
    // 'value' attribute on TypeKeyInput (id=Check_Status_Input) at TransactionPaymentDetailsInputSet.pcf: line 23, column 46
    function valueRoot_4 () : java.lang.Object {
      return Payment.Check
    }
    
    // 'value' attribute on CheckBoxInput (id=CMSFlag_Input) at TransactionPaymentDetailsInputSet.pcf: line 18, column 37
    function value_0 () : java.lang.Boolean {
      return Payment.CMSFlag_TDIC
    }
    
    // 'value' attribute on DateInput (id=Check_ScheduledSendDate_Input) at TransactionPaymentDetailsInputSet.pcf: line 36, column 48
    function value_12 () : java.util.Date {
      return Payment.Check.ScheduledSendDate
    }
    
    // 'value' attribute on DateInput (id=Check_IssueDate_Input) at TransactionPaymentDetailsInputSet.pcf: line 40, column 40
    function value_15 () : java.util.Date {
      return Payment.Check.IssueDate
    }
    
    // 'value' attribute on TextInput (id=Check_CheckNumber_Input) at TransactionPaymentDetailsInputSet.pcf: line 44, column 42
    function value_18 () : java.lang.String {
      return Payment.Check.CheckNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=Check_BankAccount_Input) at TransactionPaymentDetailsInputSet.pcf: line 49, column 40
    function value_21 () : typekey.BankAccount {
      return Payment.Check.BankAccount
    }
    
    // 'value' attribute on DateInput (id=Check_DateOfService_Input) at TransactionPaymentDetailsInputSet.pcf: line 54, column 64
    function value_25 () : java.util.Date {
      return Payment.Check.DateOfService
    }
    
    // 'value' attribute on TypeKeyInput (id=Check_Status_Input) at TransactionPaymentDetailsInputSet.pcf: line 23, column 46
    function value_3 () : typekey.TransactionStatus {
      return Payment.Check.Status
    }
    
    // 'value' attribute on TextInput (id=Check_ServicePeriod_Input) at TransactionPaymentDetailsInputSet.pcf: line 59, column 63
    function value_30 () : java.lang.String {
      return Payment.Check.ServicePeriodString
    }
    
    // 'value' attribute on TypeKeyInput (id=Check_PaymentMethod_Input) at TransactionPaymentDetailsInputSet.pcf: line 64, column 42
    function value_34 () : typekey.PaymentMethod {
      return Payment.Check.PaymentMethod
    }
    
    // 'value' attribute on TextInput (id=Check_InvoiceNumber_Input) at TransactionPaymentDetailsInputSet.pcf: line 68, column 44
    function value_37 () : java.lang.String {
      return Payment.Check.InvoiceNumber
    }
    
    // 'value' attribute on TextInput (id=Check_PayTo_Input) at TransactionPaymentDetailsInputSet.pcf: line 27, column 36
    function value_6 () : java.lang.String {
      return Payment.Check.PayTo
    }
    
    // 'value' attribute on CurrencyInput (id=Check_NetAmount_Input) at TransactionPaymentDetailsInputSet.pcf: line 32, column 57
    function value_9 () : gw.api.financials.CurrencyAmountPair {
      return Payment.Check.NetAmountPair
    }
    
    // 'visible' attribute on DateInput (id=Check_DateOfService_Input) at TransactionPaymentDetailsInputSet.pcf: line 54, column 64
    function visible_24 () : java.lang.Boolean {
      return !Payment.Check.hasExposureWithServicePeriod()
    }
    
    // 'visible' attribute on TextInput (id=Check_ServicePeriod_Input) at TransactionPaymentDetailsInputSet.pcf: line 59, column 63
    function visible_29 () : java.lang.Boolean {
      return Payment.Check.hasExposureWithServicePeriod()
    }
    
    property get Payment () : Payment {
      return getRequireValue("Payment", 0) as Payment
    }
    
    property set Payment ($arg :  Payment) {
      setRequireValue("Payment", 0, $arg)
    }
    
    
  }
  
  
}