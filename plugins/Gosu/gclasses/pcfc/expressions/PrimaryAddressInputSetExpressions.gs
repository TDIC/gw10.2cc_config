package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.address.CCAddressOwner
uses tdic.cc.config.contacts.address.TDIC_DoctorContactHandleAddressOwner
uses tdic.cc.config.contacts.address.TDIC_InjuredContactHandleAddressOwner
uses tdic.cc.config.contacts.address.TDIC_MedCareOrgContactHandleAddressOwner
uses typekey.Contact
@javax.annotation.Generated("config/web/pcf/shared/contacts/PrimaryAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PrimaryAddressInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/contacts/PrimaryAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PrimaryAddressInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PrimaryAddressInputSet.pcf: line 19, column 61
    function def_onEnter_0 (def :  pcf.TDIC_CCAddressInputSet) : void {
      def.onEnter(getAddressOwner_TDIC())
    }
    
    // 'def' attribute on InputSetRef at PrimaryAddressInputSet.pcf: line 19, column 61
    function def_refreshVariables_1 (def :  pcf.TDIC_CCAddressInputSet) : void {
      def.refreshVariables(getAddressOwner_TDIC())
    }
    
    property get contactHandle () : gw.api.contact.ContactHandle {
      return getRequireValue("contactHandle", 0) as gw.api.contact.ContactHandle
    }
    
    property set contactHandle ($arg :  gw.api.contact.ContactHandle) {
      setRequireValue("contactHandle", 0, $arg)
    }
    
    
    /**
     * US22
     * 08/26/2014 robk
     */
        property get ClaimContact_TDIC() : ClaimContact {
          return contactHandle typeis ClaimContact ? (contactHandle) : null;
        }
    
        /**
         * US22
         * 08/26/2014 robk
         */
        function getAddressOwner_TDIC() : CCAddressOwner {
          if (contactHandle.Contact.Subtype == typekey.Contact.TC_DOCTOR) {
            return new TDIC_DoctorContactHandleAddressOwner(contactHandle)
          } else if (contactHandle.Contact.Subtype == typekey.Contact.TC_MEDICALCAREORG) {
            return new TDIC_MedCareOrgContactHandleAddressOwner(contactHandle)
          } else if (ClaimContact != null) {
            if (ClaimContact_TDIC.Roles != null and ClaimContact_TDIC.Roles.firstWhere(\r -> r.Role == typekey.ContactRole.TC_INJURED or r.Role == typekey.ContactRole.TC_CLAIMANT) != null) {
              return new TDIC_InjuredContactHandleAddressOwner(contactHandle)
            }
          }else if(contactHandle.Contact.Subtype == typekey.Contact.TC_PERSON){
           return new TDIC_InjuredContactHandleAddressOwner(contactHandle)
          }
         return contactHandle.AddressOwner
        }
    
    
  }
  
  
}