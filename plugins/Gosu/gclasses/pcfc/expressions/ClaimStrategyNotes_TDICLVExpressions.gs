package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/notes/ClaimStrategyNotes_TDICLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClaimStrategyNotes_TDICLVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/notes/ClaimStrategyNotes_TDICLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimStrategyNotes_TDICLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RowIterator at ClaimStrategyNotes_TDICLV.pcf: line 16, column 46
    function value_12 () : java.util.List<Note> {
      return getClaimStrategyNotes()
    }
    
    property get NoteList () : gw.api.database.IQueryBeanResult<Note> {
      return getRequireValue("NoteList", 0) as gw.api.database.IQueryBeanResult<Note>
    }
    
    property set NoteList ($arg :  gw.api.database.IQueryBeanResult<Note>) {
      setRequireValue("NoteList", 0, $arg)
    }
    
    function getClaimStrategyNotes() : List<Note> {
      var claimStrategyNotes = NoteList.where(\elt -> elt.ClaimStrategy_TDIC)
      var finalListOfNotes : List<Note>
      finalListOfNotes = claimStrategyNotes.subList(0, (claimStrategyNotes.size() - (Math.max(claimStrategyNotes.size() - 10, 0))))
      return finalListOfNotes
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/notes/ClaimStrategyNotes_TDICLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ClaimStrategyNotes_TDICLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'iconColor' attribute on Link (id=ConfidentialIcon) at ClaimStrategyNotes_TDICLV.pcf: line 29, column 38
    function iconColor_0 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_ALERT_ERROR
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at ClaimStrategyNotes_TDICLV.pcf: line 39, column 45
    function valueRoot_4 () : java.lang.Object {
      return Note
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at ClaimStrategyNotes_TDICLV.pcf: line 50, column 25
    function value_10 () : java.lang.String {
      return DisplayKey.get("Web.Note.Author",  Note.Author, gw.api.util.StringUtil.formatDate(Note.AuthoringDate, "short"), gw.api.util.StringUtil.formatTime(Note.AuthoringDate, "short"))
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at ClaimStrategyNotes_TDICLV.pcf: line 39, column 45
    function value_3 () : java.lang.String {
      return Note.Subject
    }
    
    // 'value' attribute on NoteBodyInput (id=Body_Input) at ClaimStrategyNotes_TDICLV.pcf: line 44, column 32
    function value_7 () : java.lang.String {
      return Note.Body
    }
    
    // 'visible' attribute on ContentInput at ClaimStrategyNotes_TDICLV.pcf: line 24, column 41
    function visible_1 () : java.lang.Boolean {
      return Note.Confidential
    }
    
    // 'visible' attribute on TextInput (id=Subject_Input) at ClaimStrategyNotes_TDICLV.pcf: line 39, column 45
    function visible_2 () : java.lang.Boolean {
      return Note.Subject != null
    }
    
    property get Note () : Note {
      return getIteratedValue(1) as Note
    }
    
    
  }
  
  
}