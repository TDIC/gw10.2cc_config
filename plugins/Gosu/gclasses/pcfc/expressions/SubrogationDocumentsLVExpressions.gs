package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/subrogation/General/SubrogationDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SubrogationDocumentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/subrogation/General/SubrogationDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SubrogationDocumentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at SubrogationDocumentsLV.pcf: line 40, column 147
    function action_10 () : void {
      document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at SubrogationDocumentsLV.pcf: line 49, column 145
    function action_15 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at SubrogationDocumentsLV.pcf: line 60, column 77
    function action_19 () : void {
      DocumentDetailsPopup.push(document)
    }
    
    // 'action' attribute on Link (id=DownloadLink) at SubrogationDocumentsLV.pcf: line 69, column 90
    function action_24 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=UploadLink) at SubrogationDocumentsLV.pcf: line 77, column 88
    function action_29 () : void {
      UploadDocumentContentPopup.push(document)
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at SubrogationDocumentsLV.pcf: line 60, column 77
    function action_dest_20 () : pcf.api.Destination {
      return pcf.DocumentDetailsPopup.createDestination(document)
    }
    
    // 'action' attribute on Link (id=UploadLink) at SubrogationDocumentsLV.pcf: line 77, column 88
    function action_dest_30 () : pcf.api.Destination {
      return pcf.UploadDocumentContentPopup.createDestination(document)
    }
    
    // 'available' attribute on Link (id=NameLinkWeb) at SubrogationDocumentsLV.pcf: line 49, column 145
    function available_13 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=ViewPropertiesLink) at SubrogationDocumentsLV.pcf: line 60, column 77
    function available_18 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'available' attribute on Link (id=DownloadLink) at SubrogationDocumentsLV.pcf: line 69, column 90
    function available_22 () : java.lang.Boolean {
      return documentsActionsHelper.isDownloadDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=UploadLink) at SubrogationDocumentsLV.pcf: line 77, column 88
    function available_27 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at SubrogationDocumentsLV.pcf: line 40, column 147
    function available_8 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document) 
    }
    
    // 'icon' attribute on Link (id=DownloadLink) at SubrogationDocumentsLV.pcf: line 69, column 90
    function icon_26 () : java.lang.String {
      return "document_download" 
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at SubrogationDocumentsLV.pcf: line 27, column 32
    function icon_7 () : java.lang.String {
      return document.Icon
    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at SubrogationDocumentsLV.pcf: line 40, column 147
    function label_11 () : java.lang.Object {
      return document.Name
    }
    
    // 'label' attribute on Link (id=DocumentsLV_ActionsDisabled) at SubrogationDocumentsLV.pcf: line 82, column 75
    function label_33 () : java.lang.Object {
      return documentsActionsHelper.AsynchronousActionsMessage
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at SubrogationDocumentsLV.pcf: line 40, column 147
    function tooltip_12 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(document)
    }
    
    // 'tooltip' attribute on Link (id=ViewPropertiesLink) at SubrogationDocumentsLV.pcf: line 60, column 77
    function tooltip_21 () : java.lang.String {
      return documentsActionsHelper.ViewDocumentPropertiesTooltip
    }
    
    // 'tooltip' attribute on Link (id=DownloadLink) at SubrogationDocumentsLV.pcf: line 69, column 90
    function tooltip_25 () : java.lang.String {
      return documentsActionsHelper.DownloadDocumentContentTooltip
    }
    
    // 'tooltip' attribute on Link (id=UploadLink) at SubrogationDocumentsLV.pcf: line 77, column 88
    function tooltip_31 () : java.lang.String {
      return documentsActionsHelper.UploadDocumentContentTooltip
    }
    
    // 'value' attribute on TextCell (id=RelatedTo_Cell) at SubrogationDocumentsLV.pcf: line 87, column 59
    function valueRoot_35 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TextCell (id=RelatedTo_Cell) at SubrogationDocumentsLV.pcf: line 87, column 59
    function value_34 () : java.lang.String {
      return document.RelatedTo as java.lang.String
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at SubrogationDocumentsLV.pcf: line 93, column 45
    function value_37 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at SubrogationDocumentsLV.pcf: line 99, column 58
    function value_40 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at SubrogationDocumentsLV.pcf: line 104, column 51
    function value_43 () : typekey.DocumentStatusType {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at SubrogationDocumentsLV.pcf: line 109, column 36
    function value_46 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at SubrogationDocumentsLV.pcf: line 117, column 42
    function value_49 () : java.util.Date {
      return document.DateModified
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at SubrogationDocumentsLV.pcf: line 49, column 145
    function visible_14 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on Link (id=DownloadLink) at SubrogationDocumentsLV.pcf: line 69, column 90
    function visible_23 () : java.lang.Boolean {
      return documentsActionsHelper.isDownloadDocumentContentVisible(document)
    }
    
    // 'visible' attribute on Link (id=UploadLink) at SubrogationDocumentsLV.pcf: line 77, column 88
    function visible_28 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentVisible(document)
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_ActionsDisabled) at SubrogationDocumentsLV.pcf: line 82, column 75
    function visible_32 () : java.lang.Boolean {
      return documentsActionsHelper.isDocumentPending(document)
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at SubrogationDocumentsLV.pcf: line 40, column 147
    function visible_9 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    property get document () : entity.Document {
      return getIteratedValue(1) as entity.Document
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/subrogation/General/SubrogationDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SubrogationDocumentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at SubrogationDocumentsLV.pcf: line 13, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'value' attribute on TextCell (id=RelatedTo_Cell) at SubrogationDocumentsLV.pcf: line 87, column 59
    function sortValue_1 (document :  entity.Document) : java.lang.Object {
      return document.RelatedTo as java.lang.String
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at SubrogationDocumentsLV.pcf: line 93, column 45
    function sortValue_2 (document :  entity.Document) : java.lang.Object {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at SubrogationDocumentsLV.pcf: line 99, column 58
    function sortValue_3 (document :  entity.Document) : java.lang.Object {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at SubrogationDocumentsLV.pcf: line 104, column 51
    function sortValue_4 (document :  entity.Document) : java.lang.Object {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at SubrogationDocumentsLV.pcf: line 109, column 36
    function sortValue_5 (document :  entity.Document) : java.lang.Object {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at SubrogationDocumentsLV.pcf: line 117, column 42
    function sortValue_6 (document :  entity.Document) : java.lang.Object {
      return document.DateModified
    }
    
    // 'value' attribute on RowIterator at SubrogationDocumentsLV.pcf: line 19, column 37
    function value_52 () : entity.Document[] {
      return documents
    }
    
    property get documents () : Document[] {
      return getRequireValue("documents", 0) as Document[]
    }
    
    property set documents ($arg :  Document[]) {
      setRequireValue("documents", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    
  }
  
  
}