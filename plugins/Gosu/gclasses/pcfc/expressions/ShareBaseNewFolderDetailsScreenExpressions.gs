package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseNewFolderDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseNewFolderDetailsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseNewFolderDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseNewFolderDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=name_Input) at ShareBaseNewFolderDetailsScreen.pcf: line 26, column 38
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      Folder.FolderName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PasswordInput (id=linkPassword_Input) at ShareBaseNewFolderDetailsScreen.pcf: line 38, column 40
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      Folder.LinkPassword = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PasswordInput (id=confirmPassword_Input) at ShareBaseNewFolderDetailsScreen.pcf: line 46, column 36
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      ConfirmPassword = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=linkexp_Input) at ShareBaseNewFolderDetailsScreen.pcf: line 33, column 38
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      Folder.Expiration = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'validationExpression' attribute on PasswordInput (id=confirmPassword_Input) at ShareBaseNewFolderDetailsScreen.pcf: line 46, column 36
    function validationExpression_14 () : java.lang.Object {
      return (Folder.LinkPassword == ConfirmPassword) ? null : DisplayKey.get("Accelerator.OnBase.Wizard.NewShareBaseFolderWizard.ErrorMessage.STR_GW_PasswordsDontMatch")
    }
    
    // 'validationExpression' attribute on DateInput (id=linkexp_Input) at ShareBaseNewFolderDetailsScreen.pcf: line 33, column 38
    function validationExpression_4 () : java.lang.Object {
      return  Folder.Expiration.compareIgnoreTime(gw.api.util.DateUtil.currentDate()) >= 0 ? null : DisplayKey.get("Accelerator.OnBase.Wizard.NewShareBaseFolderWizard.ErrorMessage.STR_GW_PastDate")  
    }
    
    // 'value' attribute on TextInput (id=name_Input) at ShareBaseNewFolderDetailsScreen.pcf: line 26, column 38
    function valueRoot_2 () : java.lang.Object {
      return Folder
    }
    
    // 'value' attribute on TextInput (id=name_Input) at ShareBaseNewFolderDetailsScreen.pcf: line 26, column 38
    function value_0 () : java.lang.String {
      return Folder.FolderName
    }
    
    // 'value' attribute on PasswordInput (id=linkPassword_Input) at ShareBaseNewFolderDetailsScreen.pcf: line 38, column 40
    function value_10 () : java.lang.String {
      return Folder.LinkPassword
    }
    
    // 'value' attribute on PasswordInput (id=confirmPassword_Input) at ShareBaseNewFolderDetailsScreen.pcf: line 46, column 36
    function value_15 () : java.lang.String {
      return ConfirmPassword
    }
    
    // 'value' attribute on DateInput (id=linkexp_Input) at ShareBaseNewFolderDetailsScreen.pcf: line 33, column 38
    function value_5 () : java.util.Date {
      return Folder.Expiration
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get ConfirmPassword () : String {
      return getVariableValue("ConfirmPassword", 0) as String
    }
    
    property set ConfirmPassword ($arg :  String) {
      setVariableValue("ConfirmPassword", 0, $arg)
    }
    
    property get Folder () : ShareBase_Ext {
      return getRequireValue("Folder", 0) as ShareBase_Ext
    }
    
    property set Folder ($arg :  ShareBase_Ext) {
      setRequireValue("Folder", 0, $arg)
    }
    
    
  }
  
  
}