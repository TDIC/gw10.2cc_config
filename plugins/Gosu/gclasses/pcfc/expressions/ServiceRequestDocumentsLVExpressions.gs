package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/servicerequests/ServiceRequestDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ServiceRequestDocumentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/servicerequests/ServiceRequestDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ServiceRequestDocumentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at ServiceRequestDocumentsLV.pcf: line 48, column 147
    function action_13 () : void {
      document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at ServiceRequestDocumentsLV.pcf: line 57, column 145
    function action_18 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at ServiceRequestDocumentsLV.pcf: line 68, column 77
    function action_22 () : void {
      DocumentDetailsPopup.push(document, not serviceRequest.AlreadyPromoted)
    }
    
    // 'action' attribute on Link (id=DownloadLink) at ServiceRequestDocumentsLV.pcf: line 77, column 129
    function action_27 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=UploadLink) at ServiceRequestDocumentsLV.pcf: line 85, column 127
    function action_32 () : void {
      UploadDocumentContentPopup.push(document)
    }
    
    // 'action' attribute on Link (id=Unlink) at ServiceRequestDocumentsLV.pcf: line 92, column 91
    function action_36 () : void {
      serviceRequest.unlinkDocumentForUI(document)
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at ServiceRequestDocumentsLV.pcf: line 68, column 77
    function action_dest_23 () : pcf.api.Destination {
      return pcf.DocumentDetailsPopup.createDestination(document, not serviceRequest.AlreadyPromoted)
    }
    
    // 'action' attribute on Link (id=UploadLink) at ServiceRequestDocumentsLV.pcf: line 85, column 127
    function action_dest_33 () : pcf.api.Destination {
      return pcf.UploadDocumentContentPopup.createDestination(document)
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at ServiceRequestDocumentsLV.pcf: line 48, column 147
    function available_11 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document) 
    }
    
    // 'available' attribute on Link (id=NameLinkWeb) at ServiceRequestDocumentsLV.pcf: line 57, column 145
    function available_16 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=ViewPropertiesLink) at ServiceRequestDocumentsLV.pcf: line 68, column 77
    function available_21 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'available' attribute on Link (id=DownloadLink) at ServiceRequestDocumentsLV.pcf: line 77, column 129
    function available_25 () : java.lang.Boolean {
      return documentsActionsHelper.isDownloadDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=UploadLink) at ServiceRequestDocumentsLV.pcf: line 85, column 127
    function available_30 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=Unlink) at ServiceRequestDocumentsLV.pcf: line 92, column 91
    function available_35 () : java.lang.Boolean {
      return documentsActionsHelper.isRemoveDocumentLinkAvailable(documentInfoPair)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at ServiceRequestDocumentsLV.pcf: line 35, column 32
    function icon_10 () : java.lang.String {
      return document.Icon
    }
    
    // 'icon' attribute on Link (id=DownloadLink) at ServiceRequestDocumentsLV.pcf: line 77, column 129
    function icon_29 () : java.lang.String {
      return "document_download" 
    }
    
    // 'icon' attribute on Link (id=Unlink) at ServiceRequestDocumentsLV.pcf: line 92, column 91
    function icon_38 () : java.lang.String {
      return "document_remove"  
    }
    
    // 'initialValue' attribute on Variable at ServiceRequestDocumentsLV.pcf: line 27, column 33
    function initialValue_9 () : entity.Document {
      return documentInfoPair.Second
    }
    
    // RowIterator at ServiceRequestDocumentsLV.pcf: line 23, column 112
    function initializeVariables_61 () : void {
        document = documentInfoPair.Second;

    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at ServiceRequestDocumentsLV.pcf: line 48, column 147
    function label_14 () : java.lang.Object {
      return document.Name
    }
    
    // 'label' attribute on Link (id=DocumentsLV_ActionsDisabled) at ServiceRequestDocumentsLV.pcf: line 97, column 75
    function label_40 () : java.lang.Object {
      return documentsActionsHelper.AsynchronousActionsMessage
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at ServiceRequestDocumentsLV.pcf: line 48, column 147
    function tooltip_15 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(document)
    }
    
    // 'tooltip' attribute on Link (id=ViewPropertiesLink) at ServiceRequestDocumentsLV.pcf: line 68, column 77
    function tooltip_24 () : java.lang.String {
      return documentsActionsHelper.ViewDocumentPropertiesTooltip
    }
    
    // 'tooltip' attribute on Link (id=DownloadLink) at ServiceRequestDocumentsLV.pcf: line 77, column 129
    function tooltip_28 () : java.lang.String {
      return documentsActionsHelper.DownloadDocumentContentTooltip
    }
    
    // 'tooltip' attribute on Link (id=UploadLink) at ServiceRequestDocumentsLV.pcf: line 85, column 127
    function tooltip_34 () : java.lang.String {
      return documentsActionsHelper.UploadDocumentContentTooltip
    }
    
    // 'tooltip' attribute on Link (id=Unlink) at ServiceRequestDocumentsLV.pcf: line 92, column 91
    function tooltip_37 () : java.lang.String {
      return documentsActionsHelper.RemoveDocumentLinkTooltip(documentInfoPair)
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ServiceRequestDocumentsLV.pcf: line 104, column 45
    function valueRoot_42 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on DateCell (id=DateSpecialistNotified_Cell) at ServiceRequestDocumentsLV.pcf: line 141, column 66
    function valueRoot_59 () : java.lang.Object {
      return documentInfoPair.First
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ServiceRequestDocumentsLV.pcf: line 104, column 45
    function value_41 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at ServiceRequestDocumentsLV.pcf: line 110, column 58
    function value_44 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'value' attribute on TextCell (id=RelatedStatementType_Cell) at ServiceRequestDocumentsLV.pcf: line 115, column 62
    function value_47 () : java.lang.String {
      return getRelatedStatementType(documentInfoPair)
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ServiceRequestDocumentsLV.pcf: line 120, column 51
    function value_49 () : typekey.DocumentStatusType {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at ServiceRequestDocumentsLV.pcf: line 125, column 36
    function value_52 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at ServiceRequestDocumentsLV.pcf: line 133, column 42
    function value_55 () : java.util.Date {
      return document.DateModified
    }
    
    // 'value' attribute on DateCell (id=DateSpecialistNotified_Cell) at ServiceRequestDocumentsLV.pcf: line 141, column 66
    function value_58 () : java.util.Date {
      return documentInfoPair.First.DateSpecialistNotified
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at ServiceRequestDocumentsLV.pcf: line 48, column 147
    function visible_12 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at ServiceRequestDocumentsLV.pcf: line 57, column 145
    function visible_17 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on Link (id=DownloadLink) at ServiceRequestDocumentsLV.pcf: line 77, column 129
    function visible_26 () : java.lang.Boolean {
      return documentsActionsHelper.isDownloadDocumentContentVisible(document) and not serviceRequest.AlreadyPromoted
    }
    
    // 'visible' attribute on Link (id=UploadLink) at ServiceRequestDocumentsLV.pcf: line 85, column 127
    function visible_31 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentVisible(document) and not serviceRequest.AlreadyPromoted
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_ActionsDisabled) at ServiceRequestDocumentsLV.pcf: line 97, column 75
    function visible_39 () : java.lang.Boolean {
      return documentsActionsHelper.isDocumentPending(document)
    }
    
    property get document () : entity.Document {
      return getVariableValue("document", 1) as entity.Document
    }
    
    property set document ($arg :  entity.Document) {
      setVariableValue("document", 1, $arg)
    }
    
    property get documentInfoPair () : gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document> {
      return getIteratedValue(1) as gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/servicerequests/ServiceRequestDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ServiceRequestDocumentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at ServiceRequestDocumentsLV.pcf: line 14, column 108
    function initialValue_0 () : java.util.List<gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>> {
      return serviceRequest.ViewableDocumentLinksAndDocuments
    }
    
    // 'initialValue' attribute on Variable at ServiceRequestDocumentsLV.pcf: line 18, column 52
    function initialValue_1 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ServiceRequestDocumentsLV.pcf: line 104, column 45
    function sortValue_2 (documentInfoPair :  gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>) : java.lang.Object {
      var document : entity.Document = (documentInfoPair.Second)
return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at ServiceRequestDocumentsLV.pcf: line 110, column 58
    function sortValue_3 (documentInfoPair :  gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>) : java.lang.Object {
      var document : entity.Document = (documentInfoPair.Second)
return document.Subtype
    }
    
    // 'value' attribute on TextCell (id=RelatedStatementType_Cell) at ServiceRequestDocumentsLV.pcf: line 115, column 62
    function sortValue_4 (documentInfoPair :  gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>) : java.lang.Object {
      var document : entity.Document = (documentInfoPair.Second)
return getRelatedStatementType(documentInfoPair)
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ServiceRequestDocumentsLV.pcf: line 120, column 51
    function sortValue_5 (documentInfoPair :  gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>) : java.lang.Object {
      var document : entity.Document = (documentInfoPair.Second)
return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at ServiceRequestDocumentsLV.pcf: line 125, column 36
    function sortValue_6 (documentInfoPair :  gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>) : java.lang.Object {
      var document : entity.Document = (documentInfoPair.Second)
return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at ServiceRequestDocumentsLV.pcf: line 133, column 42
    function sortValue_7 (documentInfoPair :  gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>) : java.lang.Object {
      var document : entity.Document = (documentInfoPair.Second)
return document.DateModified
    }
    
    // 'value' attribute on DateCell (id=DateSpecialistNotified_Cell) at ServiceRequestDocumentsLV.pcf: line 141, column 66
    function sortValue_8 (documentInfoPair :  gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>) : java.lang.Object {
      var document : entity.Document = (documentInfoPair.Second)
return documentInfoPair.First.DateSpecialistNotified
    }
    
    // 'value' attribute on RowIterator at ServiceRequestDocumentsLV.pcf: line 23, column 112
    function value_62 () : java.util.List<gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>> {
      return documentInfoPairs
    }
    
    property get documentInfoPairs () : java.util.List<gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>> {
      return getVariableValue("documentInfoPairs", 0) as java.util.List<gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>>
    }
    
    property set documentInfoPairs ($arg :  java.util.List<gw.util.Pair<entity.ServiceRequestDocumentLink, entity.Document>>) {
      setVariableValue("documentInfoPairs", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get serviceRequest () : ServiceRequest {
      return getRequireValue("serviceRequest", 0) as ServiceRequest
    }
    
    property set serviceRequest ($arg :  ServiceRequest) {
      setRequireValue("serviceRequest", 0, $arg)
    }
    
    function getRelatedStatementType(docLinkPair : gw.util.Pair<ServiceRequestDocumentLink,Document>) : String {
      if (not docLinkPair.Second.IsServiceRequestStatementDocument) {
        return ""
      }
      
      var statementDocInfos = docLinkPair.First.StatementDocumentLinks
      if (statementDocInfos.allMatch(\ s -> s.ServiceRequestStatement typeis ServiceRequestQuote)) {
        return DisplayKey.get("Web.ServiceRequest.Quote.Proper")
      } else if (statementDocInfos.allMatch(\ s -> s.ServiceRequestStatement typeis ServiceRequestInvoice)) {
        return DisplayKey.get("Web.ServiceRequest.Invoice.Proper")
      }
      return DisplayKey.get("LV.Claim.ServiceRequest.Document.QuoteAndInvoice")
    }
    
    
  }
  
  
}