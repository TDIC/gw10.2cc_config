package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/financials/transactions/ClaimTransactionCMSFlag_TDICPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClaimTransactionCMSFlag_TDICPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/financials/transactions/ClaimTransactionCMSFlag_TDICPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimTransactionCMSFlag_TDICPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (payment :  Payment) : int {
      return 0
    }
    
    // 'value' attribute on CheckBoxInput (id=CMSFlag_Input) at ClaimTransactionCMSFlag_TDICPopup.pcf: line 26, column 43
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      payment.CMSFlag_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // EditButtons at ClaimTransactionCMSFlag_TDICPopup.pcf: line 18, column 32
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at ClaimTransactionCMSFlag_TDICPopup.pcf: line 18, column 32
    function pickValue_0 () : Payment {
      return payment
    }
    
    // 'value' attribute on CheckBoxInput (id=CMSFlag_Input) at ClaimTransactionCMSFlag_TDICPopup.pcf: line 26, column 43
    function valueRoot_4 () : java.lang.Object {
      return payment
    }
    
    // 'value' attribute on CheckBoxInput (id=CMSFlag_Input) at ClaimTransactionCMSFlag_TDICPopup.pcf: line 26, column 43
    function value_2 () : java.lang.Boolean {
      return payment.CMSFlag_TDIC
    }
    
    override property get CurrentLocation () : pcf.ClaimTransactionCMSFlag_TDICPopup {
      return super.CurrentLocation as pcf.ClaimTransactionCMSFlag_TDICPopup
    }
    
    property get payment () : Payment {
      return getVariableValue("payment", 0) as Payment
    }
    
    property set payment ($arg :  Payment) {
      setVariableValue("payment", 0, $arg)
    }
    
    
  }
  
  
}