package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/lossdetails/LossDetailsPanelSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LossDetailsPanelSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/lossdetails/LossDetailsPanelSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends LossDetailsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=AddNote_TDIC) at LossDetailsPanelSet.default.pcf: line 111, column 45
    function action_48 () : void {
      NewNoteWorksheet.goInWorkspace(Claim)
    }
    
    // 'action' attribute on Link (id=AddNote_TDIC) at LossDetailsPanelSet.default.pcf: line 111, column 45
    function action_dest_49 () : pcf.api.Destination {
      return pcf.NewNoteWorksheet.createDestination(Claim)
    }
    
    // 'available' attribute on Link (id=AddNote_TDIC) at LossDetailsPanelSet.default.pcf: line 111, column 45
    function available_47 () : java.lang.Boolean {
      return InEditMode
    }
    
    // 'def' attribute on ListViewInput (id=MedicalDiagnosisLVInput) at LossDetailsPanelSet.default.pcf: line 82, column 35
    function def_onEnter_45 (def :  pcf.MedicalDiagnosisLV) : void {
      def.onEnter(Claim.InjuryIncidentsOnly.first(), true)
    }
    
    // 'def' attribute on ListViewInput (id=MedicalDiagnosisLVInput) at LossDetailsPanelSet.default.pcf: line 82, column 35
    function def_refreshVariables_46 (def :  pcf.MedicalDiagnosisLV) : void {
      def.refreshVariables(Claim.InjuryIncidentsOnly.first(), true)
    }
    
    // 'value' attribute on CheckBoxInput (id=Exclude_From_CMS_Input) at LossDetailsPanelSet.default.pcf: line 41, column 52
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ExcludeFromCMS_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=HICN_Number_Input) at LossDetailsPanelSet.default.pcf: line 46, column 48
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.HICNNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=HICN_Lookup_Status_Input) at LossDetailsPanelSet.default.pcf: line 52, column 64
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.HICNLookupStatus_TDIC = (__VALUE_TO_SET as typekey.HICNLookUpStatusType_TDIC)
    }
    
    // 'value' attribute on DateInput (id=CMS_Reported_Date_Input) at LossDetailsPanelSet.default.pcf: line 57, column 53
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.CMSReportedDate_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=CMS_Response_Code_Type_Input) at LossDetailsPanelSet.default.pcf: line 63, column 63
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.CMSResponseCodeType_TDIC = (__VALUE_TO_SET as typekey.CMSResponseCodeType_TDIC)
    }
    
    // 'value' attribute on TypeKeyInput (id=CMS_Response_Code_Value_Input) at LossDetailsPanelSet.default.pcf: line 69, column 64
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.CMSResponseCodeValue_TDIC = (__VALUE_TO_SET as typekey.CMSResponseCodeValue_TDIC)
    }
    
    // 'value' attribute on CheckBoxInput (id=Exclude_From_CMS_Input) at LossDetailsPanelSet.default.pcf: line 41, column 52
    function valueRoot_20 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on CurrencyInput (id=CMS_TOPC_Amomunt_Input) at LossDetailsPanelSet.default.pcf: line 74, column 33
    function valueRoot_43 () : java.lang.Object {
      return Claim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_PROFESSIONALLIABILITY_TDIC or elt.ExposureType==ExposureType.TC_BUSINESSLIABILITY_TDIC or elt.ExposureType==ExposureType.TC_EMPPRACTICESLIAB_TDIC or elt.ExposureType==ExposureType.TC_GENERALLIABILITY_TDIC)?.ExposureRpt
    }
    
    // 'value' attribute on CheckBoxInput (id=Exclude_From_CMS_Input) at LossDetailsPanelSet.default.pcf: line 41, column 52
    function value_18 () : java.lang.Boolean {
      return Claim.ExcludeFromCMS_TDIC
    }
    
    // 'value' attribute on TextInput (id=HICN_Number_Input) at LossDetailsPanelSet.default.pcf: line 46, column 48
    function value_22 () : java.lang.String {
      return Claim.HICNNumber_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=HICN_Lookup_Status_Input) at LossDetailsPanelSet.default.pcf: line 52, column 64
    function value_26 () : typekey.HICNLookUpStatusType_TDIC {
      return Claim.HICNLookupStatus_TDIC
    }
    
    // 'value' attribute on DateInput (id=CMS_Reported_Date_Input) at LossDetailsPanelSet.default.pcf: line 57, column 53
    function value_30 () : java.util.Date {
      return Claim.CMSReportedDate_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=CMS_Response_Code_Type_Input) at LossDetailsPanelSet.default.pcf: line 63, column 63
    function value_34 () : typekey.CMSResponseCodeType_TDIC {
      return Claim.CMSResponseCodeType_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=CMS_Response_Code_Value_Input) at LossDetailsPanelSet.default.pcf: line 69, column 64
    function value_38 () : typekey.CMSResponseCodeValue_TDIC {
      return Claim.CMSResponseCodeValue_TDIC
    }
    
    // 'value' attribute on CurrencyInput (id=CMS_TOPC_Amomunt_Input) at LossDetailsPanelSet.default.pcf: line 74, column 33
    function value_42 () : gw.api.financials.CurrencyAmount {
      return Claim?.Exposures?.firstWhere( \ elt -> elt.ExposureType==ExposureType.TC_PROFESSIONALLIABILITY_TDIC or elt.ExposureType==ExposureType.TC_BUSINESSLIABILITY_TDIC or elt.ExposureType==ExposureType.TC_EMPPRACTICESLIAB_TDIC or elt.ExposureType==ExposureType.TC_GENERALLIABILITY_TDIC)?.ExposureRpt?.TotalPayments
    }
    
    property get injury () : InjuryIncident {
      return getVariableValue("injury", 1) as InjuryIncident
    }
    
    property set injury ($arg :  InjuryIncident) {
      setVariableValue("injury", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/lossdetails/LossDetailsPanelSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LossDetailsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_onEnter_0 (def :  pcf.LossDetailsDV_Auto) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_onEnter_10 (def :  pcf.LossDetailsDV_Wc7) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_onEnter_12 (def :  pcf.LossDetailsDV_default) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 25, column 38
    function def_onEnter_15 (def :  pcf.ISODetailsDV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_onEnter_2 (def :  pcf.LossDetailsDV_Gl) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_onEnter_4 (def :  pcf.LossDetailsDV_Pr) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_onEnter_6 (def :  pcf.LossDetailsDV_Trav) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_onEnter_8 (def :  pcf.LossDetailsDV_Wc) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_refreshVariables_1 (def :  pcf.LossDetailsDV_Auto) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_refreshVariables_11 (def :  pcf.LossDetailsDV_Wc7) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_refreshVariables_13 (def :  pcf.LossDetailsDV_default) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 25, column 38
    function def_refreshVariables_16 (def :  pcf.ISODetailsDV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_refreshVariables_3 (def :  pcf.LossDetailsDV_Gl) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_refreshVariables_5 (def :  pcf.LossDetailsDV_Pr) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_refreshVariables_7 (def :  pcf.LossDetailsDV_Trav) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function def_refreshVariables_9 (def :  pcf.LossDetailsDV_Wc) : void {
      def.refreshVariables(Claim)
    }
    
    // 'mode' attribute on PanelRef at LossDetailsPanelSet.default.pcf: line 18, column 34
    function mode_14 () : java.lang.Object {
      return Claim.LossType
    }
    
    // 'visible' attribute on Card (id=Claim_ISOCard) at LossDetailsPanelSet.default.pcf: line 23, column 74
    function visible_17 () : java.lang.Boolean {
      return Claim.ISOClaimLevelMessaging and Claim.Policy.Verified
    }
    
    // 'visible' attribute on Card (id=Claim_CMSCard) at LossDetailsPanelSet.default.pcf: line 30, column 162
    function visible_50 () : java.lang.Boolean {
      return Claim.Policy.Verified and (Claim.Policy.PolicyType == PolicyType.TC_GENERALLIABILITY or Claim.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY)
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    
  }
  
  
}