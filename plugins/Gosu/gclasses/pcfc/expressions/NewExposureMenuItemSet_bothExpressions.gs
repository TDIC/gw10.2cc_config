package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.exposure.NewExposureMenuItem
@javax.annotation.Generated("config/web/pcf/claim/newexposure/NewExposureMenuItemSet.both.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewExposureMenuItemSet_bothExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/newexposure/NewExposureMenuItemSet.both.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MenuItemTreeEntry2ExpressionsImpl extends NewExposureMenuItemSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=item) at NewExposureMenuItemSet.both.pcf: line 41, column 38
    function action_6 () : void {
      NewExposure.go(claim, item.CoverageType, item.CoverageSubtype, item.lazyLoadCoverage(claim))
    }
    
    // 'action' attribute on MenuItem (id=item) at NewExposureMenuItemSet.both.pcf: line 41, column 38
    function action_dest_7 () : pcf.api.Destination {
      return pcf.NewExposure.createDestination(claim, item.CoverageType, item.CoverageSubtype, item.lazyLoadCoverage(claim))
    }
    
    // 'available' attribute on MenuItem (id=item) at NewExposureMenuItemSet.both.pcf: line 41, column 38
    function available_5 () : java.lang.Boolean {
      return (item.CoverageType != null and item.CoverageSubtype != null) or item.Children.length > 0
    }
    
    // 'children' attribute on MenuItemTree at NewExposureMenuItemSet.both.pcf: line 35, column 59
    function children_9 () : java.lang.Object {
      return item.Children
    }
    
    // 'label' attribute on MenuItem (id=item) at NewExposureMenuItemSet.both.pcf: line 41, column 38
    function label_8 () : java.lang.Object {
      return item.DisplayLabel
    }
    
    property get item () : gw.api.exposure.NewExposureMenuItem {
      return getIteratedValue(1) as gw.api.exposure.NewExposureMenuItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/newexposure/NewExposureMenuItemSet.both.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MenuItemTreeEntryExpressionsImpl extends NewExposureMenuItemSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=item) at NewExposureMenuItemSet.both.pcf: line 24, column 38
    function action_0 () : void {
      NewExposure.go(claim, item.CoverageType, item.CoverageSubtype, item.lazyLoadCoverage(claim))
    }
    
    // 'action' attribute on MenuItem (id=item) at NewExposureMenuItemSet.both.pcf: line 24, column 38
    function action_dest_1 () : pcf.api.Destination {
      return pcf.NewExposure.createDestination(claim, item.CoverageType, item.CoverageSubtype, item.lazyLoadCoverage(claim))
    }
    
    // 'children' attribute on MenuItemTree at NewExposureMenuItemSet.both.pcf: line 19, column 59
    function children_3 () : java.lang.Object {
      return item.Children
    }
    
    // 'label' attribute on MenuItem (id=item) at NewExposureMenuItemSet.both.pcf: line 24, column 38
    function label_2 () : java.lang.Object {
      return item.DisplayLabel
    }
    
    property get item () : gw.api.exposure.NewExposureMenuItem {
      return getIteratedValue(1) as gw.api.exposure.NewExposureMenuItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/newexposure/NewExposureMenuItemSet.both.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewExposureMenuItemSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on MenuItemTree at NewExposureMenuItemSet.both.pcf: line 19, column 59
    function value_4 () : gw.api.exposure.NewExposureMenuItem[] {
      return getExposureMenuItem(claim)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    
    function getExposureMenuItem(clm: Claim): gw.api.exposure.NewExposureMenuItem[]{
      if(clm.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY and clm.LossType == LossType.TC_PR){
        return getExposureMenuItemByCoverage(clm)
      }if(clm.Type_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY){
        var availableExposures = gw.api.exposure.NewExposureMenuUtil.createItemsByCoverageType(clm).toList()
        var desiredExposures = new HashSet<NewExposureMenuItem>()
        availableExposures.each(\avExp -> {
          if (avExp.CoverageSubtype != null and avExp.CoverageSubtype?.hasCategory(clm?.CL_CoverageType_TDIC)) {
            desiredExposures.add(avExp)
          } else if (avExp.CoverageSubtype == null) {
            var childrenExposures = avExp.Children?.where(\childExp -> childExp.CoverageSubtype != null and childExp?.CoverageSubtype?.hasCategory(clm?.CL_CoverageType_TDIC))
            if (childrenExposures?.HasElements) {
              childrenExposures.each(\childExpo -> {
                desiredExposures.add(childExpo)
              })
            }
          }
        })
       return desiredExposures.toTypedArray()
      }else{
        var availableExposures = gw.api.exposure.NewExposureMenuUtil.createItemsByCoverageType(clm).toList()
        var desiredExposures = new HashSet<NewExposureMenuItem>()
        if(claim.Policy.Offerings_TDIC == Offering_TDIC.TC_LRP and clm.LossType == LossType.TC_PR){
          availableExposures.each(\avExp -> {
            if (avExp.CoverageSubtype != null and avExp.CoverageSubtype.hasCategory(clm.Type_TDIC)) {
              if(avExp.CoverageSubtype.DisplayName.equalsIgnoreCase("Building") or avExp.CoverageSubtype.DisplayName.equalsIgnoreCase("Ordinance or Law"))
                desiredExposures.add(avExp)
            } 
          })
         return desiredExposures.toTypedArray()
        }
        availableExposures.each(\avExp -> {
          if (avExp.CoverageSubtype != null and avExp.CoverageSubtype.hasCategory(clm.Type_TDIC)) {
            desiredExposures.add(avExp)
          } else if (avExp.CoverageSubtype == null) {
            var childrenExposures = avExp.Children.where(\childExp -> childExp.CoverageSubtype != null and childExp.CoverageSubtype.hasCategory(clm.Type_TDIC))
            if (childrenExposures.HasElements) {
              childrenExposures.each(\childExpo -> {
                desiredExposures.add(childExpo)
              })
            }
          }
        })
        return desiredExposures.toTypedArray()
      }  
    }
    
    function getExposureMenuItemByCoverage(clmp: Claim): gw.api.exposure.NewExposureMenuItem[]{
      var availableExposures = gw.api.exposure.NewExposureMenuUtil.createItemsByCoverage(clmp).toList()
      var desiredExposures = new HashSet<NewExposureMenuItem>()
      availableExposures.each(\avExp -> {
        if (avExp.CoverageSubtype != null and avExp.CoverageSubtype.hasCategory(clmp.Type_TDIC)) {
          desiredExposures.add(avExp)
        } else if (avExp.CoverageSubtype == null and avExp.Children.HasElements) {
          var childrenExposures = avExp.Children
          for(childExposure in childrenExposures){
            if(childExposure.CoverageSubtype != null and childExposure.CoverageSubtype.hasCategory(clmp.Type_TDIC)){
              desiredExposures.add(childExposure)
            }
            else if(childExposure.CoverageSubtype == null and childExposure.Children.HasElements){
              var subChildrenExposures = childExposure.Children.where(\childExp -> childExp.CoverageSubtype != null and childExp.CoverageSubtype.hasCategory(clmp.Type_TDIC))
              if(subChildrenExposures.HasElements){
                subChildrenExposures.each(\childExpo -> {
                  desiredExposures.add(childExpo)
                })
              }
            }
          }
        }
      })
      return desiredExposures.toTypedArray()
    }
    
    
  }
  
  
}