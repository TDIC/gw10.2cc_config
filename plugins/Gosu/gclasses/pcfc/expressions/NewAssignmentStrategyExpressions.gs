package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/NewAssignmentStrategy.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewAssignmentStrategyExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/NewAssignmentStrategy.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewAssignmentStrategyExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewAssignmentStrategy) at NewAssignmentStrategy.pcf: line 12, column 100
    function afterCancel_3 () : void {
      AssignmentStrategy_TDIC.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewAssignmentStrategy) at NewAssignmentStrategy.pcf: line 12, column 100
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.AssignmentStrategy_TDIC.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewAssignmentStrategy) at NewAssignmentStrategy.pcf: line 12, column 100
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      AssignmentStrategy_TDIC.go()
    }
    
    // 'def' attribute on ScreenRef at NewAssignmentStrategy.pcf: line 20, column 55
    function def_onEnter_1 (def :  pcf.ASRecordDetailScreen) : void {
      def.onEnter(AssignmentStrategy)
    }
    
    // 'def' attribute on ScreenRef at NewAssignmentStrategy.pcf: line 20, column 55
    function def_refreshVariables_2 (def :  pcf.ASRecordDetailScreen) : void {
      def.refreshVariables(AssignmentStrategy)
    }
    
    // 'initialValue' attribute on Variable at NewAssignmentStrategy.pcf: line 18, column 42
    function initialValue_0 () : entity.AssignStrategy_TDIC {
      return new AssignStrategy_TDIC()
    }
    
    // 'parent' attribute on Page (id=NewAssignmentStrategy) at NewAssignmentStrategy.pcf: line 12, column 100
    static function parent_6 () : pcf.api.Destination {
      return pcf.AssignmentStrategy_TDIC.createDestination()
    }
    
    property get AssignmentStrategy () : entity.AssignStrategy_TDIC {
      return getVariableValue("AssignmentStrategy", 0) as entity.AssignStrategy_TDIC
    }
    
    property set AssignmentStrategy ($arg :  entity.AssignStrategy_TDIC) {
      setVariableValue("AssignmentStrategy", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.NewAssignmentStrategy {
      return super.CurrentLocation as pcf.NewAssignmentStrategy
    }
    
    
  }
  
  
}