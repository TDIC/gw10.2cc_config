package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/summary/ClaimSummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClaimSummaryExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/summary/ClaimSummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimFinancialsPieChartExpressionsImpl extends ClaimSummaryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'categoryLabel' attribute on DataSeries at ClaimSummary.pcf: line 137, column 38
    function categoryLabel_43 (value :  Map.Entry<String, java.lang.Integer>) : java.lang.String {
      return value.Key
    }
    
    // 'dataValues' attribute on DataSeries at ClaimSummary.pcf: line 137, column 38
    function dataValues_44 () : java.lang.Object {
      return gw.api.financials.FinancialsPieChartUtil.getFinancialsParameters(Claim).entrySet().toList()
    }
    
    // 'fillColor' attribute on DataSeries at ClaimSummary.pcf: line 137, column 38
    function fillColor_47 () : List<gw.api.web.color.GWColor> {
      return pieFillColors
    }
    
    // 'initialValue' attribute on Variable at ClaimSummary.pcf: line 126, column 62
    function initialValue_41 () : List<gw.api.web.color.GWColor> {
      return {gw.api.web.color.GWColor.THEME_NUMBERS_POSITIVE, gw.api.web.color.GWColor.THEME_NUMBERS_NEUTRAL}
    }
    
    // 'initialValue' attribute on Variable at ClaimSummary.pcf: line 130, column 62
    function initialValue_42 () : List<gw.api.web.color.GWColor> {
      return {gw.api.web.color.GWColor.THEME_APP_BACKGROUND}
    }
    
    // 'strokeColor' attribute on DataSeries at ClaimSummary.pcf: line 137, column 38
    function strokeColor_46 () : List<gw.api.web.color.GWColor> {
      return pieStrokeColors
    }
    
    // 'value' attribute on DataSeries at ClaimSummary.pcf: line 137, column 38
    function value_45 (value :  Map.Entry<String, java.lang.Integer>) : java.lang.Object {
      return value.Value
    }
    
    property get pieFillColors () : List<gw.api.web.color.GWColor> {
      return getVariableValue("pieFillColors", 1) as List<gw.api.web.color.GWColor>
    }
    
    property set pieFillColors ($arg :  List<gw.api.web.color.GWColor>) {
      setVariableValue("pieFillColors", 1, $arg)
    }
    
    property get pieStrokeColors () : List<gw.api.web.color.GWColor> {
      return getVariableValue("pieStrokeColors", 1) as List<gw.api.web.color.GWColor>
    }
    
    property set pieStrokeColors ($arg :  List<gw.api.web.color.GWColor>) {
      setVariableValue("pieStrokeColors", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/summary/ClaimSummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimSummaryExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Claim :  Claim) : int {
      return 0
    }
    
    static function __constructorIndex (Claim :  Claim, excludeConfidentialNotes :  boolean) : int {
      return 1
    }
    
    // 'action' attribute on ToolbarButton (id=demolink) at ClaimSummary.pcf: line 37, column 28
    function action_2 () : void {
      OnBaseDemoPopup.push(Claim)
    }
    
    // 'action' attribute on Link (id=ViewClaimOtherInstructions) at ClaimSummary.pcf: line 240, column 115
    function action_73 () : void {
      ClaimOtherInstructionWorksheet.goInWorkspace(Claim)
    }
    
    // 'action' attribute on ToolbarButton (id=demolink) at ClaimSummary.pcf: line 37, column 28
    function action_dest_3 () : pcf.api.Destination {
      return pcf.OnBaseDemoPopup.createDestination(Claim)
    }
    
    // 'action' attribute on Link (id=ViewClaimOtherInstructions) at ClaimSummary.pcf: line 240, column 115
    function action_dest_74 () : pcf.api.Destination {
      return pcf.ClaimOtherInstructionWorksheet.createDestination(Claim)
    }
    
    // 'beforeCommit' attribute on Page (id=ClaimSummary) at ClaimSummary.pcf: line 10, column 64
    function beforeCommit_99 (pickedValue :  java.lang.Object) : void {
      operationCallbackHelper.BeforeCommitAction()
    }
    
    // 'canVisit' attribute on Page (id=ClaimSummary) at ClaimSummary.pcf: line 10, column 64
    static function canVisit_100 (Claim :  Claim, excludeConfidentialNotes :  boolean) : java.lang.Boolean {
      return perm.Claim.view(Claim) and perm.System.viewclaimsummary and (Claim.State != ClaimState.TC_DRAFT)
    }
    
    // 'def' attribute on InputSetRef (id=WC_RTW) at ClaimSummary.pcf: line 103, column 132
    function def_onEnter_39 (def :  pcf.ReturnToWorkInputSet_WC) : void {
      def.onEnter(gw.config.LOBAbstraction.ForClaim.ForLossType.isWorkComp(Claim) ? Claim.ensureClaimInjuryIncident() : null, false)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 261, column 63
    function def_onEnter_82 (def :  pcf.ClaimSummaryExposuresLV) : void {
      def.onEnter(Claim, Claim.Exposures)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 267, column 70
    function def_onEnter_84 (def :  pcf.ServiceRequestLV) : void {
      def.onEnter(Claim, true, operationCallbackHelper)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 277, column 51
    function def_onEnter_86 (def :  pcf.ClaimSummaryActivitiesLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 284, column 75
    function def_onEnter_89 (def :  pcf.ClaimSummaryMattersLV) : void {
      def.onEnter(Claim.Matters, Claim)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 290, column 77
    function def_onEnter_91 (def :  pcf.ClaimSummaryAssociatedClaimsLV) : void {
      def.onEnter(Claim, Claim.Associations)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 300, column 104
    function def_onEnter_93 (def :  pcf.NotesLV) : void {
      def.onEnter(excludeConfidentialNotes ? Claim.NonconfidentialNotes : Claim.ViewableNotes)
    }
    
    // 'def' attribute on PanelRef (id=ClaimStrategyNotesLV) at ClaimSummary.pcf: line 307, column 39
    function def_onEnter_95 (def :  pcf.ClaimStrategyNotes_TDICLV) : void {
      def.onEnter(excludeConfidentialNotes ? Claim.NonconfidentialNotes : Claim.ViewableNotes)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 314, column 75
    function def_onEnter_97 (def :  pcf.PeopleInvolvedLV) : void {
      def.onEnter(Claim, Claim.getContactsWithPreload())
    }
    
    // 'def' attribute on InputSetRef (id=WC_RTW) at ClaimSummary.pcf: line 103, column 132
    function def_refreshVariables_40 (def :  pcf.ReturnToWorkInputSet_WC) : void {
      def.refreshVariables(gw.config.LOBAbstraction.ForClaim.ForLossType.isWorkComp(Claim) ? Claim.ensureClaimInjuryIncident() : null, false)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 261, column 63
    function def_refreshVariables_83 (def :  pcf.ClaimSummaryExposuresLV) : void {
      def.refreshVariables(Claim, Claim.Exposures)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 267, column 70
    function def_refreshVariables_85 (def :  pcf.ServiceRequestLV) : void {
      def.refreshVariables(Claim, true, operationCallbackHelper)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 277, column 51
    function def_refreshVariables_87 (def :  pcf.ClaimSummaryActivitiesLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 284, column 75
    function def_refreshVariables_90 (def :  pcf.ClaimSummaryMattersLV) : void {
      def.refreshVariables(Claim.Matters, Claim)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 290, column 77
    function def_refreshVariables_92 (def :  pcf.ClaimSummaryAssociatedClaimsLV) : void {
      def.refreshVariables(Claim, Claim.Associations)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 300, column 104
    function def_refreshVariables_94 (def :  pcf.NotesLV) : void {
      def.refreshVariables(excludeConfidentialNotes ? Claim.NonconfidentialNotes : Claim.ViewableNotes)
    }
    
    // 'def' attribute on PanelRef (id=ClaimStrategyNotesLV) at ClaimSummary.pcf: line 307, column 39
    function def_refreshVariables_96 (def :  pcf.ClaimStrategyNotes_TDICLV) : void {
      def.refreshVariables(excludeConfidentialNotes ? Claim.NonconfidentialNotes : Claim.ViewableNotes)
    }
    
    // 'def' attribute on PanelRef at ClaimSummary.pcf: line 314, column 75
    function def_refreshVariables_98 (def :  pcf.PeopleInvolvedLV) : void {
      def.refreshVariables(Claim, Claim.getContactsWithPreload())
    }
    
    // 'highInclusive' attribute on BarInput (id=DaysOpenProgressBar_Input) at ClaimSummary.pcf: line 59, column 75
    function highInclusive_6 () : java.lang.Boolean {
      return !claimUIHelper.onlyTargetValueExists(Claim.DaysOpenClaimMetric.Limit)
    }
    
    // 'highThreshold' attribute on BarInput (id=DaysOpenProgressBar_Input) at ClaimSummary.pcf: line 59, column 75
    function highThreshold_7 () : java.lang.Double {
      return claimUIHelper.HighThreshold
    }
    
    // 'iconColor' attribute on Link (id=EmployerThreePointContact) at ClaimSummary.pcf: line 85, column 79
    function iconColor_30 () : gw.api.web.color.GWColor {
      return Claim.getWCContactEmployerActivityStatus().Second
    }
    
    // 'iconColor' attribute on Link (id=EmployeeThreePointContact) at ClaimSummary.pcf: line 91, column 79
    function iconColor_33 () : gw.api.web.color.GWColor {
      return Claim.getWCContactEmployeeActivityStatus().Second
    }
    
    // 'iconColor' attribute on Link (id=MedicalProviderThreePointContact) at ClaimSummary.pcf: line 97, column 82
    function iconColor_36 () : gw.api.web.color.GWColor {
      return Claim.getWCContactMedProviderActivityStatus().Second
    }
    
    // 'icon' attribute on Link (id=EmployerThreePointContact) at ClaimSummary.pcf: line 85, column 79
    function icon_29 () : java.lang.String {
      return Claim.getWCContactEmployerActivityStatus().First
    }
    
    // 'icon' attribute on Link (id=EmployeeThreePointContact) at ClaimSummary.pcf: line 91, column 79
    function icon_32 () : java.lang.String {
      return Claim.getWCContactEmployeeActivityStatus().First
    }
    
    // 'icon' attribute on Link (id=MedicalProviderThreePointContact) at ClaimSummary.pcf: line 97, column 82
    function icon_35 () : java.lang.String {
      return Claim.getWCContactMedProviderActivityStatus().First
    }
    
    // 'initialValue' attribute on Variable at ClaimSummary.pcf: line 25, column 73
    function initialValue_0 () : gw.vendormanagement.ServiceRequestOperationCallbackHelper {
      return new gw.vendormanagement.ServiceRequestOperationCallbackHelper()
    }
    
    // 'initialValue' attribute on Variable at ClaimSummary.pcf: line 29, column 49
    function initialValue_1 () : gw.api.claim.ClaimSummaryUIHelper {
      return new gw.api.claim.ClaimSummaryUIHelper(Claim)
    }
    
    // 'label' attribute on BarInput (id=DaysOpenProgressBar_Input) at ClaimSummary.pcf: line 59, column 75
    function label_5 () : java.lang.Object {
      return Claim.State.DisplayName
    }
    
    // 'lowInclusive' attribute on BarInput (id=DaysOpenProgressBar_Input) at ClaimSummary.pcf: line 59, column 75
    function lowInclusive_8 () : java.lang.Boolean {
      return claimUIHelper.noYellowValueExists(Claim.DaysOpenClaimMetric.Limit)
    }
    
    // 'lowThreshold' attribute on BarInput (id=DaysOpenProgressBar_Input) at ClaimSummary.pcf: line 59, column 75
    function lowThreshold_9 () : java.lang.Double {
      return claimUIHelper.LowThreshold
    }
    
    // Page (id=ClaimSummary) at ClaimSummary.pcf: line 10, column 64
    static function parent_101 (Claim :  Claim, excludeConfidentialNotes :  boolean) : pcf.api.Destination {
      return pcf.ClaimSummaryGroup.createDestination(Claim)
    }
    
    // 'percentage' attribute on BarInput (id=DaysOpenProgressBar_Input) at ClaimSummary.pcf: line 59, column 75
    function percentage_10 () : java.lang.Double {
      return claimUIHelper.DaysOpenPercentage
    }
    
    // 'status' attribute on BarInput (id=DaysOpenProgressBar_Input) at ClaimSummary.pcf: line 59, column 75
    function status_11 () : java.lang.String {
      return claimUIHelper.OpenStatus
    }
    
    // 'tooltip' attribute on Link (id=EmployerThreePointContact) at ClaimSummary.pcf: line 85, column 79
    function tooltip_28 () : java.lang.String {
      return Claim.getWCContactEmployerActivityStatus().Third
    }
    
    // 'tooltip' attribute on Link (id=EmployeeThreePointContact) at ClaimSummary.pcf: line 91, column 79
    function tooltip_31 () : java.lang.String {
      return Claim.getWCContactEmployeeActivityStatus().Third
    }
    
    // 'tooltip' attribute on Link (id=MedicalProviderThreePointContact) at ClaimSummary.pcf: line 97, column 82
    function tooltip_34 () : java.lang.String {
      return Claim.getWCContactMedProviderActivityStatus().Third
    }
    
    // 'value' attribute on TextInput (id=Paid_Input) at ClaimSummary.pcf: line 151, column 68
    function valueRoot_51 () : java.lang.Object {
      return Claim.TotalPaidClaimMetric
    }
    
    // 'value' attribute on TypeKeyInput (id=TypeOfClaim_Input) at ClaimSummary.pcf: line 200, column 53
    function valueRoot_62 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on TextInput (id=Location_Input) at ClaimSummary.pcf: line 247, column 61
    function valueRoot_77 () : java.lang.Object {
      return Claim.LossLocation
    }
    
    // 'value' attribute on TextInput (id=DaysOpenText_Input) at ClaimSummary.pcf: line 64, column 76
    function value_15 () : java.lang.String {
      return DisplayKey.get("Web.ClaimSummary.Headline.Basics.ClaimOpenWithState",  Claim.State.DisplayName, claimUIHelper.OpenStatus)
    }
    
    // 'value' attribute on TextInput (id=ClaimClosedText_Input) at ClaimSummary.pcf: line 70, column 41
    function value_20 () : java.lang.String {
      return claimUIHelper.getClaimClosedText()
    }
    
    // 'value' attribute on TextInput (id=WCInjuryWorkStatus_Input) at ClaimSummary.pcf: line 75, column 92
    function value_25 () : java.lang.String {
      return Claim.getWorkStatusMessage()
    }
    
    // 'value' attribute on TextInput (id=TotalGrossIncurred_Input) at ClaimSummary.pcf: line 146, column 105
    function value_48 () : java.lang.String {
      return gw.api.util.CurrencyUtil.renderAsCurrency(Claim.ClaimRpt.TotalIncurredGross)
    }
    
    // 'value' attribute on TextInput (id=Paid_Input) at ClaimSummary.pcf: line 151, column 68
    function value_50 () : java.lang.String {
      return Claim.TotalPaidClaimMetric.DisplayValue
    }
    
    // 'value' attribute on TextInput (id=Recovered_Input) at ClaimSummary.pcf: line 156, column 102
    function value_53 () : java.lang.String {
      return gw.api.util.CurrencyUtil.renderAsCurrency(Claim.ClaimRpt.TotalRecoveries)
    }
    
    // 'value' attribute on InputIterator (id=RiskIndicatorIterator) at ClaimSummary.pcf: line 178, column 53
    function value_60 () : entity.ClaimIndicator[] {
      return Claim.ClaimIndicators.where( \ c -> c.IsOn).orderBy( \ e -> ((e.Subtype.Priority) as java.lang.Comparable<java.lang.Object>)).toTypedArray()
    }
    
    // 'value' attribute on TypeKeyInput (id=TypeOfClaim_Input) at ClaimSummary.pcf: line 200, column 53
    function value_61 () : typekey.ClaimType_TDIC {
      return Claim.Type_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=HasMinorIndicator_Input) at ClaimSummary.pcf: line 208, column 53
    function value_64 () : java.lang.Boolean {
      return Claim.HasMinorContact_TDIC
    }
    
    // 'value' attribute on DateInput (id=LossDate_Input) at ClaimSummary.pcf: line 229, column 45
    function value_67 () : java.util.Date {
      return Claim.LossDate
    }
    
    // 'value' attribute on DateInput (id=ReportDate_Input) at ClaimSummary.pcf: line 233, column 49
    function value_70 () : java.util.Date {
      return Claim.ReportedDate
    }
    
    // 'value' attribute on TextInput (id=Location_Input) at ClaimSummary.pcf: line 247, column 61
    function value_76 () : java.lang.String {
      return Claim.LossLocation.DisplayName
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at ClaimSummary.pcf: line 253, column 48
    function value_79 () : java.lang.String {
      return Claim.Description
    }
    
    // 'visible' attribute on TextInput (id=DaysOpenText_Input) at ClaimSummary.pcf: line 64, column 76
    function visible_14 () : java.lang.Boolean {
      return !Claim.Closed and !claimUIHelper.showBarInput()
    }
    
    // 'visible' attribute on TextInput (id=ClaimClosedText_Input) at ClaimSummary.pcf: line 70, column 41
    function visible_18 () : java.lang.Boolean {
      return Claim.Closed
    }
    
    // 'visible' attribute on TextInput (id=WCInjuryWorkStatus_Input) at ClaimSummary.pcf: line 75, column 92
    function visible_24 () : java.lang.Boolean {
      return gw.config.LOBAbstraction.ForClaim.ForLossType.isWorkComp(Claim)
    }
    
    // 'visible' attribute on ContentInput (id=ThreePointContactIcons) at ClaimSummary.pcf: line 79, column 143
    function visible_37 () : java.lang.Boolean {
      return gw.config.LOBAbstraction.ForClaim.ForLossType.isWorkComp(Claim) and Claim.shouldShowWCThreePointContactActivities()
    }
    
    // 'visible' attribute on InputSetRef (id=WC_RTW) at ClaimSummary.pcf: line 103, column 132
    function visible_38 () : java.lang.Boolean {
      return gw.config.LOBAbstraction.ForClaim.ForLossType.isWorkComp(Claim) and Claim.getLossTimeExposure() != null
    }
    
    // 'visible' attribute on BarInput (id=DaysOpenProgressBar_Input) at ClaimSummary.pcf: line 59, column 75
    function visible_4 () : java.lang.Boolean {
      return !Claim.Closed and claimUIHelper.showBarInput()
    }
    
    // 'visible' attribute on Label (id=RiskIndicatorNone) at ClaimSummary.pcf: line 173, column 79
    function visible_55 () : java.lang.Boolean {
      return !Claim.ClaimIndicators.hasMatch( \ c -> c.IsOn)
    }
    
    // 'visible' attribute on ContentInput at ClaimSummary.pcf: line 236, column 60
    function visible_75 () : java.lang.Boolean {
      return Claim.hasOtherInstructions()
    }
    
    // 'visible' attribute on PanelRef at ClaimSummary.pcf: line 284, column 75
    function visible_88 () : java.lang.Boolean {
      return perm.Matter.view(Claim) and perm.System.viewmatters
    }
    
    property get Claim () : Claim {
      return getVariableValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setVariableValue("Claim", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.ClaimSummary {
      return super.CurrentLocation as pcf.ClaimSummary
    }
    
    property get claimUIHelper () : gw.api.claim.ClaimSummaryUIHelper {
      return getVariableValue("claimUIHelper", 0) as gw.api.claim.ClaimSummaryUIHelper
    }
    
    property set claimUIHelper ($arg :  gw.api.claim.ClaimSummaryUIHelper) {
      setVariableValue("claimUIHelper", 0, $arg)
    }
    
    property get excludeConfidentialNotes () : boolean {
      return getVariableValue("excludeConfidentialNotes", 0) as java.lang.Boolean
    }
    
    property set excludeConfidentialNotes ($arg :  boolean) {
      setVariableValue("excludeConfidentialNotes", 0, $arg)
    }
    
    property get operationCallbackHelper () : gw.vendormanagement.ServiceRequestOperationCallbackHelper {
      return getVariableValue("operationCallbackHelper", 0) as gw.vendormanagement.ServiceRequestOperationCallbackHelper
    }
    
    property set operationCallbackHelper ($arg :  gw.vendormanagement.ServiceRequestOperationCallbackHelper) {
      setVariableValue("operationCallbackHelper", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/summary/ClaimSummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ClaimSummaryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'iconColor' attribute on Link (id=IndicatorIcon) at ClaimSummary.pcf: line 186, column 107
    function iconColor_58 () : gw.api.web.color.GWColor {
      return indicator.IconColor
    }
    
    // 'icon' attribute on Link (id=IndicatorIcon) at ClaimSummary.pcf: line 186, column 107
    function icon_57 () : java.lang.String {
      return indicator.Icon
    }
    
    // 'label' attribute on Link (id=IndicatorText) at ClaimSummary.pcf: line 189, column 45
    function label_59 () : java.lang.Object {
      return indicator.Text
    }
    
    // 'tooltip' attribute on Link (id=IndicatorIcon) at ClaimSummary.pcf: line 186, column 107
    function tooltip_56 () : java.lang.String {
      return indicator.HoverText != indicator.Text ? indicator.HoverText : ""
    }
    
    property get indicator () : entity.ClaimIndicator {
      return getIteratedValue(1) as entity.ClaimIndicator
    }
    
    
  }
  
  
}