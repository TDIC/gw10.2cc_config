package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/documents/DocumentDetailsEditInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentDetailsEditInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/documents/DocumentDetailsEditInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentDetailsEditInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at DocumentDetailsEditInputSet.default.pcf: line 71, column 143
    function action_31 () : void {
      document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at DocumentDetailsEditInputSet.default.pcf: line 80, column 141
    function action_36 () : void {
      document.downloadContent()
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at DocumentDetailsEditInputSet.default.pcf: line 71, column 143
    function available_29 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document) 
    }
    
    // 'available' attribute on CheckBoxInput (id=allEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 33, column 61
    function available_3 () : java.lang.Boolean {
      return documentDetailsCCHelper.AllowFieldsSubset
    }
    
    // 'available' attribute on Link (id=NameLinkWeb) at DocumentDetailsEditInputSet.default.pcf: line 80, column 141
    function available_34 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document)
    }
    
    // 'value' attribute on CheckBoxInput (id=sectionEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 148, column 61
    function defaultSetter_105 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.SectionEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=Section_Input) at DocumentDetailsEditInputSet.default.pcf: line 158, column 46
    function defaultSetter_112 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.Section = (__VALUE_TO_SET as typekey.DocumentSection)
    }
    
    // 'value' attribute on CheckBoxInput (id=relatedToEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 167, column 61
    function defaultSetter_120 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.RelatedToEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=RelatedTo_Input) at DocumentDetailsEditInputSet.default.pcf: line 183, column 61
    function defaultSetter_130 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.RelatedTo = (__VALUE_TO_SET as gw.pl.persistence.core.Bean)
    }
    
    // 'value' attribute on CheckBoxInput (id=nameEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 47, column 61
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.NameEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=authorEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 211, column 61
    function defaultSetter_150 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.AuthorEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at DocumentDetailsEditInputSet.default.pcf: line 220, column 49
    function defaultSetter_157 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.Author = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=recipientEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 228, column 61
    function defaultSetter_164 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.RecipientEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Recipient_Input) at DocumentDetailsEditInputSet.default.pcf: line 237, column 52
    function defaultSetter_171 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.Recipient = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=inboundEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 246, column 61
    function defaultSetter_178 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.InboundEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InBound_Input) at DocumentDetailsEditInputSet.default.pcf: line 255, column 50
    function defaultSetter_185 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.Inbound = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=statusEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 263, column 61
    function defaultSetter_192 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.StatusEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at DocumentDetailsEditInputSet.default.pcf: line 274, column 49
    function defaultSetter_199 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.Status = (__VALUE_TO_SET as typekey.DocumentStatusType)
    }
    
    // 'value' attribute on CheckBoxInput (id=securityTypeEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 282, column 61
    function defaultSetter_206 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.SecurityTypeEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=SecurityType_Input) at DocumentDetailsEditInputSet.default.pcf: line 292, column 48
    function defaultSetter_214 (__VALUE_TO_SET :  java.lang.Object) : void {
      securityType = (__VALUE_TO_SET as typekey.DocumentSecurityType)
    }
    
    // 'value' attribute on CheckBoxInput (id=typeEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 303, column 61
    function defaultSetter_220 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.TypeEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at DocumentDetailsEditInputSet.default.pcf: line 314, column 40
    function defaultSetter_227 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.Type = (__VALUE_TO_SET as typekey.DocumentType)
    }
    
    // 'value' attribute on CheckBoxInput (id=subtypeEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 322, column 59
    function defaultSetter_234 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.SubtypeEnabled_ext = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at DocumentDetailsEditInputSet.default.pcf: line 58, column 47
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=Subtype_Input) at DocumentDetailsEditInputSet.default.pcf: line 334, column 53
    function defaultSetter_244 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.Subtype_ext = (__VALUE_TO_SET as typekey.OnBaseDocumentSubtype_Ext)
    }
    
    // 'value' attribute on CheckBoxInput (id=descriptionEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 89, column 61
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.DescriptionEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at DocumentDetailsEditInputSet.default.pcf: line 98, column 54
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=mimeTypeEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 106, column 61
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.MimeTypeEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=allEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 33, column 61
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.AllFieldsEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=MimeType_Input) at DocumentDetailsEditInputSet.default.pcf: line 120, column 73
    function defaultSetter_69 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.MimeType = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=languageEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 128, column 119
    function defaultSetter_83 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.LanguageEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=Language_Input) at DocumentDetailsEditInputSet.default.pcf: line 140, column 67
    function defaultSetter_93 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentDetailsCCHelper.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'editable' attribute on CheckBoxInput (id=relatedToEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 167, column 61
    function editable_117 () : java.lang.Boolean {
      return documentDetailsCCHelper.RelatedToEditable
    }
    
    // 'editable' attribute on CheckBoxInput (id=mimeTypeEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 106, column 61
    function editable_55 () : java.lang.Boolean {
      return not fromTemplate
    }
    
    // 'editable' attribute on CheckBoxInput (id=languageEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 128, column 119
    function editable_80 () : java.lang.Boolean {
      return documentDetailsCCHelper.Language == null or not fromTemplate
    }
    
    // 'initialValue' attribute on Variable at DocumentDetailsEditInputSet.default.pcf: line 17, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'initialValue' attribute on Variable at DocumentDetailsEditInputSet.default.pcf: line 21, column 24
    function initialValue_1 () : Document {
      return documentDetailsCCHelper.getDocuments().Count == 1 ? documentDetailsCCHelper.getDocuments().single() : null
    }
    
    // 'initialValue' attribute on Variable at DocumentDetailsEditInputSet.default.pcf: line 25, column 36
    function initialValue_2 () : DocumentSecurityType {
      return documentDetailsCCHelper.SecurityType == null ? DocumentSecurityType.TC_UNRESTRICTED : documentDetailsCCHelper.SecurityType
    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at DocumentDetailsEditInputSet.default.pcf: line 71, column 143
    function label_32 () : java.lang.Object {
      return document.Name
    }
    
    // 'onChange' attribute on PostOnChange at DocumentDetailsEditInputSet.default.pcf: line 294, column 73
    function onChange_211 () : void {
      documentDetailsCCHelper.SecurityType = securityType
    }
    
    // 'optionGroupLabel' attribute on RangeInput (id=RelatedTo_Input) at DocumentDetailsEditInputSet.default.pcf: line 183, column 61
    function optionGroupLabel_132 (VALUE :  gw.pl.persistence.core.Bean) : java.lang.String {
      return gw.pcf.RelatedToUtil.getOptionGroupLabel(VALUE as KeyableBean)
    }
    
    // 'optionLabel' attribute on RangeInput (id=RelatedTo_Input) at DocumentDetailsEditInputSet.default.pcf: line 183, column 61
    function optionLabel_133 (VALUE :  gw.pl.persistence.core.Bean) : java.lang.String {
      return gw.pcf.RelatedToUtil.getOptionLabel(VALUE as KeyableBean)
    }
    
    // 'optionLabel' attribute on RangeInput (id=MimeType_Input) at DocumentDetailsEditInputSet.default.pcf: line 120, column 73
    function optionLabel_71 (VALUE :  java.lang.String) : java.lang.String {
      return gw.document.DocumentsUtil.getMimeTypeLabel(VALUE)
    }
    
    // 'value' attribute on Reflect at DocumentDetailsEditInputSet.default.pcf: line 338, column 79
    function reflectionValue_239 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getOnlyDocumentSubType(VALUE)
    }
    
    // 'required' attribute on TextInput (id=Name_Input) at DocumentDetailsEditInputSet.default.pcf: line 58, column 47
    function required_22 () : java.lang.Boolean {
      return !documentDetailsCCHelper.AllowFieldsSubset
    }
    
    // 'value' attribute on TextCell (id=ServiceRequestDisplayName_Cell) at DocumentDetailsEditInputSet.default.pcf: line 199, column 53
    function sortValue_141 (ServiceRequest :  entity.ServiceRequest) : java.lang.Object {
      return ServiceRequest.DisplayName
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at DocumentDetailsEditInputSet.default.pcf: line 71, column 143
    function tooltip_33 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(document)
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at DocumentDetailsEditInputSet.default.pcf: line 183, column 61
    function valueRange_134 () : java.lang.Object {
      return documentDetailsCCHelper.RelatedToCandidates
    }
    
    // 'valueRange' attribute on Reflect at DocumentDetailsEditInputSet.default.pcf: line 338, column 79
    function valueRange_241 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(VALUE)
    }
    
    // 'valueRange' attribute on RangeInput (id=Subtype_Input) at DocumentDetailsEditInputSet.default.pcf: line 334, column 53
    function valueRange_246 () : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(documentDetailsCCHelper.Type)
    }
    
    // 'valueRange' attribute on RangeInput (id=MimeType_Input) at DocumentDetailsEditInputSet.default.pcf: line 120, column 73
    function valueRange_72 () : java.lang.Object {
      return documentDetailsCCHelper.getMimeTypeList(documentDetailsCCHelper.MimeType)
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentDetailsEditInputSet.default.pcf: line 140, column 67
    function valueRange_95 () : java.lang.Object {
      return LanguageType.getTypeKeys( false )
    }
    
    // 'value' attribute on CheckBoxInput (id=allEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 33, column 61
    function valueRoot_7 () : java.lang.Object {
      return documentDetailsCCHelper
    }
    
    // 'value' attribute on CheckBoxInput (id=sectionEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 148, column 61
    function value_104 () : java.lang.Boolean {
      return documentDetailsCCHelper.SectionEnabled
    }
    
    // 'value' attribute on TypeKeyInput (id=Section_Input) at DocumentDetailsEditInputSet.default.pcf: line 158, column 46
    function value_111 () : typekey.DocumentSection {
      return documentDetailsCCHelper.Section
    }
    
    // 'value' attribute on CheckBoxInput (id=relatedToEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 167, column 61
    function value_119 () : java.lang.Boolean {
      return documentDetailsCCHelper.RelatedToEnabled
    }
    
    // 'value' attribute on RangeInput (id=RelatedTo_Input) at DocumentDetailsEditInputSet.default.pcf: line 183, column 61
    function value_129 () : gw.pl.persistence.core.Bean {
      return documentDetailsCCHelper.RelatedTo
    }
    
    // 'value' attribute on CheckBoxInput (id=nameEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 47, column 61
    function value_14 () : java.lang.Boolean {
      return documentDetailsCCHelper.NameEnabled
    }
    
    // 'value' attribute on RowIterator at DocumentDetailsEditInputSet.default.pcf: line 194, column 49
    function value_145 () : entity.ServiceRequest[] {
      return documentDetailsCCHelper.RelatedServiceRequests
    }
    
    // 'value' attribute on CheckBoxInput (id=authorEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 211, column 61
    function value_149 () : java.lang.Boolean {
      return documentDetailsCCHelper.AuthorEnabled
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at DocumentDetailsEditInputSet.default.pcf: line 220, column 49
    function value_156 () : java.lang.String {
      return documentDetailsCCHelper.Author
    }
    
    // 'value' attribute on CheckBoxInput (id=recipientEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 228, column 61
    function value_163 () : java.lang.Boolean {
      return documentDetailsCCHelper.RecipientEnabled
    }
    
    // 'value' attribute on TextInput (id=Recipient_Input) at DocumentDetailsEditInputSet.default.pcf: line 237, column 52
    function value_170 () : java.lang.String {
      return documentDetailsCCHelper.Recipient
    }
    
    // 'value' attribute on CheckBoxInput (id=inboundEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 246, column 61
    function value_177 () : java.lang.Boolean {
      return documentDetailsCCHelper.InboundEnabled
    }
    
    // 'value' attribute on BooleanRadioInput (id=InBound_Input) at DocumentDetailsEditInputSet.default.pcf: line 255, column 50
    function value_184 () : java.lang.Boolean {
      return documentDetailsCCHelper.Inbound
    }
    
    // 'value' attribute on CheckBoxInput (id=statusEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 263, column 61
    function value_191 () : java.lang.Boolean {
      return documentDetailsCCHelper.StatusEnabled
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at DocumentDetailsEditInputSet.default.pcf: line 274, column 49
    function value_198 () : typekey.DocumentStatusType {
      return documentDetailsCCHelper.Status
    }
    
    // 'value' attribute on CheckBoxInput (id=securityTypeEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 282, column 61
    function value_205 () : java.lang.Boolean {
      return documentDetailsCCHelper.SecurityTypeEnabled
    }
    
    // 'value' attribute on TypeKeyInput (id=SecurityType_Input) at DocumentDetailsEditInputSet.default.pcf: line 292, column 48
    function value_213 () : typekey.DocumentSecurityType {
      return securityType
    }
    
    // 'value' attribute on CheckBoxInput (id=typeEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 303, column 61
    function value_219 () : java.lang.Boolean {
      return documentDetailsCCHelper.TypeEnabled
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at DocumentDetailsEditInputSet.default.pcf: line 314, column 40
    function value_226 () : typekey.DocumentType {
      return documentDetailsCCHelper.Type
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at DocumentDetailsEditInputSet.default.pcf: line 58, column 47
    function value_23 () : java.lang.String {
      return documentDetailsCCHelper.Name
    }
    
    // 'value' attribute on CheckBoxInput (id=subtypeEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 322, column 59
    function value_233 () : java.lang.Boolean {
      return documentDetailsCCHelper.SubtypeEnabled_ext
    }
    
    // 'value' attribute on RangeInput (id=Subtype_Input) at DocumentDetailsEditInputSet.default.pcf: line 334, column 53
    function value_243 () : typekey.OnBaseDocumentSubtype_Ext {
      return documentDetailsCCHelper.Subtype_ext
    }
    
    // 'value' attribute on CheckBoxInput (id=descriptionEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 89, column 61
    function value_42 () : java.lang.Boolean {
      return documentDetailsCCHelper.DescriptionEnabled
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at DocumentDetailsEditInputSet.default.pcf: line 98, column 54
    function value_49 () : java.lang.String {
      return documentDetailsCCHelper.Description
    }
    
    // 'value' attribute on CheckBoxInput (id=allEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 33, column 61
    function value_5 () : java.lang.Boolean {
      return documentDetailsCCHelper.AllFieldsEnabled
    }
    
    // 'value' attribute on CheckBoxInput (id=mimeTypeEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 106, column 61
    function value_57 () : java.lang.Boolean {
      return documentDetailsCCHelper.MimeTypeEnabled
    }
    
    // 'value' attribute on RangeInput (id=MimeType_Input) at DocumentDetailsEditInputSet.default.pcf: line 120, column 73
    function value_68 () : java.lang.String {
      return documentDetailsCCHelper.MimeType
    }
    
    // 'value' attribute on CheckBoxInput (id=languageEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 128, column 119
    function value_82 () : java.lang.Boolean {
      return documentDetailsCCHelper.LanguageEnabled
    }
    
    // 'value' attribute on RangeInput (id=Language_Input) at DocumentDetailsEditInputSet.default.pcf: line 140, column 67
    function value_92 () : typekey.LanguageType {
      return documentDetailsCCHelper.Language
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at DocumentDetailsEditInputSet.default.pcf: line 183, column 61
    function verifyValueRangeIsAllowedType_135 ($$arg :  gw.pl.persistence.core.Bean[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at DocumentDetailsEditInputSet.default.pcf: line 183, column 61
    function verifyValueRangeIsAllowedType_135 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Subtype_Input) at DocumentDetailsEditInputSet.default.pcf: line 334, column 53
    function verifyValueRangeIsAllowedType_247 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Subtype_Input) at DocumentDetailsEditInputSet.default.pcf: line 334, column 53
    function verifyValueRangeIsAllowedType_247 ($$arg :  typekey.OnBaseDocumentSubtype_Ext[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MimeType_Input) at DocumentDetailsEditInputSet.default.pcf: line 120, column 73
    function verifyValueRangeIsAllowedType_73 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MimeType_Input) at DocumentDetailsEditInputSet.default.pcf: line 120, column 73
    function verifyValueRangeIsAllowedType_73 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentDetailsEditInputSet.default.pcf: line 140, column 67
    function verifyValueRangeIsAllowedType_96 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentDetailsEditInputSet.default.pcf: line 140, column 67
    function verifyValueRangeIsAllowedType_96 ($$arg :  typekey.LanguageType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=RelatedTo_Input) at DocumentDetailsEditInputSet.default.pcf: line 183, column 61
    function verifyValueRange_136 () : void {
      var __valueRangeArg = documentDetailsCCHelper.RelatedToCandidates
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_135(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Subtype_Input) at DocumentDetailsEditInputSet.default.pcf: line 334, column 53
    function verifyValueRange_248 () : void {
      var __valueRangeArg = acc.onbase.util.PCFUtils.getDocumentSubTypeRange(documentDetailsCCHelper.Type)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_247(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=MimeType_Input) at DocumentDetailsEditInputSet.default.pcf: line 120, column 73
    function verifyValueRange_74 () : void {
      var __valueRangeArg = documentDetailsCCHelper.getMimeTypeList(documentDetailsCCHelper.MimeType)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_73(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentDetailsEditInputSet.default.pcf: line 140, column 67
    function verifyValueRange_97 () : void {
      var __valueRangeArg = LanguageType.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_96(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=RelatedTo_Input) at DocumentDetailsEditInputSet.default.pcf: line 183, column 61
    function visible_128 () : java.lang.Boolean {
      return documentDetailsCCHelper.RelatedToVisible
    }
    
    // 'visible' attribute on ListViewInput at DocumentDetailsEditInputSet.default.pcf: line 186, column 61
    function visible_146 () : java.lang.Boolean {
      return !documentDetailsCCHelper.RelatedToVisible
    }
    
    // 'visible' attribute on TextInput (id=Name_Input) at DocumentDetailsEditInputSet.default.pcf: line 58, column 47
    function visible_21 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at DocumentDetailsEditInputSet.default.pcf: line 71, column 143
    function visible_30 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at DocumentDetailsEditInputSet.default.pcf: line 80, column 141
    function visible_35 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on ContentInput (id=DocumentLink) at DocumentDetailsEditInputSet.default.pcf: line 62, column 69
    function visible_39 () : java.lang.Boolean {
      return not CurrentLocation.InEditMode and document != null
    }
    
    // 'visible' attribute on RangeInput (id=MimeType_Input) at DocumentDetailsEditInputSet.default.pcf: line 120, column 73
    function visible_66 () : java.lang.Boolean {
      return fromTemplate || documentDetailsCCHelper.ShowMimeType
    }
    
    // 'visible' attribute on CheckBoxInput (id=languageEnabled_Input) at DocumentDetailsEditInputSet.default.pcf: line 128, column 119
    function visible_81 () : java.lang.Boolean {
      return documentDetailsCCHelper.AllowFieldsSubset && LanguageType.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on RangeInput (id=Language_Input) at DocumentDetailsEditInputSet.default.pcf: line 140, column 67
    function visible_91 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    property get document () : Document {
      return getVariableValue("document", 0) as Document
    }
    
    property set document ($arg :  Document) {
      setVariableValue("document", 0, $arg)
    }
    
    property get documentDetailsCCHelper () : gw.document.DocumentDetailsCCHelper {
      return getRequireValue("documentDetailsCCHelper", 0) as gw.document.DocumentDetailsCCHelper
    }
    
    property set documentDetailsCCHelper ($arg :  gw.document.DocumentDetailsCCHelper) {
      setRequireValue("documentDetailsCCHelper", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get fromTemplate () : boolean {
      return getRequireValue("fromTemplate", 0) as java.lang.Boolean
    }
    
    property set fromTemplate ($arg :  boolean) {
      setRequireValue("fromTemplate", 0, $arg)
    }
    
    property get securityType () : DocumentSecurityType {
      return getVariableValue("securityType", 0) as DocumentSecurityType
    }
    
    property set securityType ($arg :  DocumentSecurityType) {
      setVariableValue("securityType", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/documents/DocumentDetailsEditInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DocumentDetailsEditInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=ServiceRequestDisplayName_Cell) at DocumentDetailsEditInputSet.default.pcf: line 199, column 53
    function valueRoot_143 () : java.lang.Object {
      return ServiceRequest
    }
    
    // 'value' attribute on TextCell (id=ServiceRequestDisplayName_Cell) at DocumentDetailsEditInputSet.default.pcf: line 199, column 53
    function value_142 () : java.lang.String {
      return ServiceRequest.DisplayName
    }
    
    property get ServiceRequest () : entity.ServiceRequest {
      return getIteratedValue(1) as entity.ServiceRequest
    }
    
    
  }
  
  
}