package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_NewLossDetailsScreen.Gl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FNOLWizard_NewLossDetailsScreen_GlExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_NewLossDetailsScreen.Gl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FNOLWizard_NewLossDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=AddInjuryButton) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 190, column 50
    function action_119 () : void {
      FNOLInjuryIncidentPopup.push(claim, null)
    }
    
    // 'action' attribute on ToolbarButton (id=AddPropertyDamageButton) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 197, column 52
    function action_123 () : void {
      NewFixedPropertyIncidentPopup.push(claim)
    }
    
    // 'action' attribute on ToolbarButton (id=AddInjuryButton) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 190, column 50
    function action_dest_120 () : pcf.api.Destination {
      return pcf.FNOLInjuryIncidentPopup.createDestination(claim, null)
    }
    
    // 'action' attribute on ToolbarButton (id=AddPropertyDamageButton) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 197, column 52
    function action_dest_124 () : pcf.api.Destination {
      return pcf.NewFixedPropertyIncidentPopup.createDestination(claim)
    }
    
    // 'def' attribute on PanelRef at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 168, column 64
    function def_onEnter_112 (def :  pcf.OptionalNoteCV) : void {
      def.onEnter(claim)
    }
    
    // 'def' attribute on PanelRef (id=FireDamageQuestionsPanelSet) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 172, column 50
    function def_onEnter_115 (def :  pcf.FireDamageQuestionsPanelSet) : void {
      def.onEnter(claim)
    }
    
    // 'def' attribute on PanelRef (id=IncidentPanelRef) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 179, column 166
    function def_onEnter_127 (def :  pcf.InjuryAndFixedPropertyIncidentsPanelSet) : void {
      def.onEnter(claim, wizard)
    }
    
    // 'def' attribute on ListViewInput (id=WitnessLV) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 212, column 31
    function def_onEnter_129 (def :  pcf.EditableWitnessesLV) : void {
      def.onEnter(claim.getClaimContactRolesByRole(ContactRole.TC_WITNESS), claim, ContactRole.TC_WITNESS)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 223, column 31
    function def_onEnter_131 (def :  pcf.EditableOfficialsLV) : void {
      def.onEnter(claim)
    }
    
    // 'def' attribute on ListViewInput (id=PoliceReportLV) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 235, column 31
    function def_onEnter_133 (def :  pcf.MetroReportsLV) : void {
      def.onEnter(claim)
    }
    
    // 'def' attribute on InputSetRef (id=AddressDetailInputSetRef) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 147, column 109
    function def_onEnter_89 (def :  pcf.CCAddressInputSet) : void {
      def.onEnter(wizard.GeneralLiabilityHelper.getAddressesWithoutPrimaryLocation())
    }
    
    // 'def' attribute on PanelRef at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 168, column 64
    function def_refreshVariables_113 (def :  pcf.OptionalNoteCV) : void {
      def.refreshVariables(claim)
    }
    
    // 'def' attribute on PanelRef (id=FireDamageQuestionsPanelSet) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 172, column 50
    function def_refreshVariables_116 (def :  pcf.FireDamageQuestionsPanelSet) : void {
      def.refreshVariables(claim)
    }
    
    // 'def' attribute on PanelRef (id=IncidentPanelRef) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 179, column 166
    function def_refreshVariables_128 (def :  pcf.InjuryAndFixedPropertyIncidentsPanelSet) : void {
      def.refreshVariables(claim, wizard)
    }
    
    // 'def' attribute on ListViewInput (id=WitnessLV) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 212, column 31
    function def_refreshVariables_130 (def :  pcf.EditableWitnessesLV) : void {
      def.refreshVariables(claim.getClaimContactRolesByRole(ContactRole.TC_WITNESS), claim, ContactRole.TC_WITNESS)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 223, column 31
    function def_refreshVariables_132 (def :  pcf.EditableOfficialsLV) : void {
      def.refreshVariables(claim)
    }
    
    // 'def' attribute on ListViewInput (id=PoliceReportLV) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 235, column 31
    function def_refreshVariables_134 (def :  pcf.MetroReportsLV) : void {
      def.refreshVariables(claim)
    }
    
    // 'def' attribute on InputSetRef (id=AddressDetailInputSetRef) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 147, column 109
    function def_refreshVariables_90 (def :  pcf.CCAddressInputSet) : void {
      def.refreshVariables(wizard.GeneralLiabilityHelper.getAddressesWithoutPrimaryLocation())
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 23, column 38
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=WeatherRelated_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 163, column 44
    function defaultSetter_107 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.WeatherRelated = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_Fault_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 257, column 46
    function defaultSetter_136 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.FaultRating = (__VALUE_TO_SET as typekey.FaultRating)
    }
    
    // 'value' attribute on RangeInput (id=ClaimPermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 265, column 52
    function defaultSetter_140 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.PermissionRequired = (__VALUE_TO_SET as typekey.ClaimSecurityType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossCause_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 48, column 43
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.LossCause = (__VALUE_TO_SET as typekey.LossCause)
    }
    
    // 'value' attribute on TypeKeyInput (id=ResultType_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 58, column 81
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.ResultType_TDIC = (__VALUE_TO_SET as typekey.ResultType_TDIC)
    }
    
    // 'value' attribute on CheckBoxInput (id=Status_IncidentReport_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 63, column 41
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.IncidentReport = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 72, column 44
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.CallType_TDIC = (__VALUE_TO_SET as String)
    }
    
    // 'value' attribute on TextInput (id=Other_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 82, column 45
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.Other_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Status_CoverageQuestion_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 89, column 44
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.CoverageInQuestion = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Notification_FirstNoticeSuit_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 95, column 44
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.FirstNoticeSuit = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=DateOfNotice_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 102, column 39
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.ReportedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_HowReported_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 108, column 48
    function defaultSetter_69 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.HowReported = (__VALUE_TO_SET as typekey.HowReportedType)
    }
    
    // 'value' attribute on Choice (id=PrimaryLocationChoice) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 121, column 42
    function defaultSetter_80 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizard.GeneralLiabilityHelper.LocationChoice = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 157, column 44
    function defaultSetter_98 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.Catastrophe = (__VALUE_TO_SET as entity.Catastrophe)
    }
    
    // 'icon' attribute on ToolbarButton (id=AddInjuryButton) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 190, column 50
    function icon_121 () : java.lang.String {
      return gw.api.claim.IncidentIconSet.INJURY.ButtonIcon
    }
    
    // 'icon' attribute on ToolbarButton (id=AddPropertyDamageButton) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 197, column 52
    function icon_125 () : java.lang.String {
      return gw.api.claim.IncidentIconSet.PROPERTY_LIABILITY.ButtonIcon
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 50, column 87
    function onChange_15 () : void {
      wizard.GeneralLiabilityHelper.setDamageAccordingToLossCause()
    }
    
    // 'option' attribute on Choice (id=PrimaryLocationChoice) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 121, column 42
    function option_78 () : java.lang.Object {
      return gw.api.claim.FnolWizardGeneralLiabilityHelper.PRIMARY_LOCATION
    }
    
    // 'option' attribute on Choice (id=OtherLocationChoice) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 137, column 40
    function option_91 () : java.lang.Object {
      return gw.api.claim.FnolWizardGeneralLiabilityHelper.OTHER_LOCATION
    }
    
    // 'required' attribute on TextInput (id=Other_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 82, column 45
    function required_44 () : java.lang.Boolean {
      return claim.CallType_TDIC == "Other"
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 30, column 35
    function validationExpression_4 () : java.lang.Object {
      return claim.LossDate == null || claim.LossDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'validationExpression' attribute on DateInput (id=DateOfNotice_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 102, column 39
    function validationExpression_62 () : java.lang.Object {
      return claim.ReportedDate != null and claim.ReportedDate > gw.api.util.DateUtil.currentDate() ? DisplayKey.get("Java.Validation.Date.ForbidFuture") : null
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 157, column 44
    function valueRange_100 () : java.lang.Object {
      return gw.api.admin.CatastropheUtil.getCatastrophes()
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimPermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 265, column 52
    function valueRange_142 () : java.lang.Object {
      return gw.api.claim.ClaimUtil.getAvailableTypes()
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 72, column 44
    function valueRange_38 () : java.lang.Object {
      return claim.getCallType(claim)
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 23, column 38
    function valueRoot_2 () : java.lang.Object {
      return claim
    }
    
    // 'value' attribute on TextInput (id=PropertyAddress_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 130, column 73
    function valueRoot_76 () : java.lang.Object {
      return claim.Policy.PrimaryLocation.Address
    }
    
    // 'value' attribute on Choice (id=PrimaryLocationChoice) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 121, column 42
    function valueRoot_81 () : java.lang.Object {
      return wizard.GeneralLiabilityHelper
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 23, column 38
    function value_0 () : java.lang.String {
      return claim.Description
    }
    
    // 'value' attribute on BooleanRadioInput (id=WeatherRelated_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 163, column 44
    function value_106 () : java.lang.Boolean {
      return claim.WeatherRelated
    }
    
    // 'value' attribute on BooleanRadioInput (id=RiskManagementIncident_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 40, column 48
    function value_12 () : java.lang.Boolean {
      return claim.RiskMgmtIncident_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_Fault_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 257, column 46
    function value_135 () : typekey.FaultRating {
      return claim.FaultRating
    }
    
    // 'value' attribute on RangeInput (id=ClaimPermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 265, column 52
    function value_139 () : typekey.ClaimSecurityType {
      return claim.PermissionRequired
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossCause_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 48, column 43
    function value_17 () : typekey.LossCause {
      return claim.LossCause
    }
    
    // 'value' attribute on TypeKeyInput (id=ResultType_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 58, column 81
    function value_23 () : typekey.ResultType_TDIC {
      return claim.ResultType_TDIC
    }
    
    // 'value' attribute on CheckBoxInput (id=Status_IncidentReport_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 63, column 41
    function value_29 () : java.lang.Boolean {
      return claim.IncidentReport
    }
    
    // 'value' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 72, column 44
    function value_35 () : String {
      return claim.CallType_TDIC
    }
    
    // 'value' attribute on TextInput (id=Other_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 82, column 45
    function value_45 () : java.lang.String {
      return claim.Other_TDIC
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 30, column 35
    function value_5 () : java.util.Date {
      return claim.LossDate
    }
    
    // 'value' attribute on BooleanRadioInput (id=Status_CoverageQuestion_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 89, column 44
    function value_51 () : java.lang.Boolean {
      return claim.CoverageInQuestion
    }
    
    // 'value' attribute on BooleanRadioInput (id=Notification_FirstNoticeSuit_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 95, column 44
    function value_57 () : java.lang.Boolean {
      return claim.FirstNoticeSuit
    }
    
    // 'value' attribute on DateInput (id=DateOfNotice_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 102, column 39
    function value_63 () : java.util.Date {
      return claim.ReportedDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_HowReported_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 108, column 48
    function value_68 () : typekey.HowReportedType {
      return claim.HowReported
    }
    
    // 'value' attribute on TextInput (id=PrimaryLocationChoiceLabel_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 126, column 45
    function value_73 () : java.lang.Object {
      return null
    }
    
    // 'value' attribute on TextInput (id=PropertyAddress_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 130, column 73
    function value_75 () : java.lang.String {
      return claim.Policy.PrimaryLocation.Address.DisplayName
    }
    
    // 'value' attribute on Choice (id=PrimaryLocationChoice) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 121, column 42
    function value_79 () : java.lang.String {
      return wizard.GeneralLiabilityHelper.LocationChoice
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossType_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 36, column 41
    function value_9 () : typekey.LossType {
      return claim.LossType
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 157, column 44
    function value_97 () : entity.Catastrophe {
      return claim.Catastrophe
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 157, column 44
    function verifyValueRangeIsAllowedType_101 ($$arg :  entity.Catastrophe[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 157, column 44
    function verifyValueRangeIsAllowedType_101 ($$arg :  gw.api.database.IQueryBeanResult<entity.Catastrophe>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 157, column 44
    function verifyValueRangeIsAllowedType_101 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimPermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 265, column 52
    function verifyValueRangeIsAllowedType_143 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimPermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 265, column 52
    function verifyValueRangeIsAllowedType_143 ($$arg :  typekey.ClaimSecurityType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 72, column 44
    function verifyValueRangeIsAllowedType_39 ($$arg :  String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 72, column 44
    function verifyValueRangeIsAllowedType_39 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 157, column 44
    function verifyValueRange_102 () : void {
      var __valueRangeArg = gw.api.admin.CatastropheUtil.getCatastrophes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_101(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimPermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 265, column 52
    function verifyValueRange_144 () : void {
      var __valueRangeArg = gw.api.claim.ClaimUtil.getAvailableTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_143(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 72, column 44
    function verifyValueRange_40 () : void {
      var __valueRangeArg = claim.getCallType(claim)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_39(__valueRangeArg)
    }
    
    // 'visible' attribute on PanelRef at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 168, column 64
    function visible_111 () : java.lang.Boolean {
      return wizard.displayNotesSection_tdic(claim)
    }
    
    // 'visible' attribute on PanelRef (id=FireDamageQuestionsPanelSet) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 172, column 50
    function visible_114 () : java.lang.Boolean {
      return claim.PropertyFireDamage != null
    }
    
    // 'visible' attribute on ToolbarButton (id=AddInjuryButton) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 190, column 50
    function visible_118 () : java.lang.Boolean {
      return claim.setInjuryVisibility()
    }
    
    // 'visible' attribute on ToolbarButton (id=AddPropertyDamageButton) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 197, column 52
    function visible_122 () : java.lang.Boolean {
      return claim.setPropertyVisibility()
    }
    
    // 'visible' attribute on PanelRef (id=IncidentPanelRef) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 179, column 166
    function visible_126 () : java.lang.Boolean {
      return claim.Policy.Verified and claim.Type_TDIC != ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY and claim.Type_TDIC != ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE
    }
    
    // 'visible' attribute on TypeKeyInput (id=Claim_LossCause_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 48, column 43
    function visible_16 () : java.lang.Boolean {
      return claim.Policy.Verified
    }
    
    // 'visible' attribute on TypeKeyInput (id=ResultType_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 58, column 81
    function visible_22 () : java.lang.Boolean {
      return claim.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY
    }
    
    // 'visible' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 72, column 44
    function visible_34 () : java.lang.Boolean {
      return !claim.Policy.Verified
    }
    
    // 'visible' attribute on InputSet at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 116, column 91
    function visible_83 () : java.lang.Boolean {
      return claim.Policy.PrimaryLocation != null && claim.Policy.Verified
    }
    
    // 'visible' attribute on InputSetRef (id=AddressDetailInputSetRef) at FNOLWizard_NewLossDetailsScreen.Gl.pcf: line 147, column 109
    function visible_88 () : java.lang.Boolean {
      return wizard.GeneralLiabilityHelper.IsOtherLocationChosen && claim.Policy.Verified
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get wizard () : gw.api.claim.NewClaimWizardInfo {
      return getRequireValue("wizard", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set wizard ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setRequireValue("wizard", 0, $arg)
    }
    
    
  }
  
  
}