package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_BasicInfoScreen.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FNOLWizard_BasicInfoScreen_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_BasicInfoScreen.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FNOLWizard_BasicInfoScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_101 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_103 () : void {
      if (Claim.maincontact != null) { ClaimContactDetailPopup.push(Claim.maincontact, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_105 () : void {
      ClaimContactDetailPopup.push(Claim.maincontact, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_11 () : void {
      if (Claim.reporter != null) { ClaimContactDetailPopup.push(Claim.reporter, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_124 () : void {
      AddressBookPickerPopup.push(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_126 () : void {
      if (Claim.affectedparty != null) { ClaimContactDetailPopup.push(Claim.affectedparty, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_128 () : void {
      ClaimContactDetailPopup.push(Claim.affectedparty, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_13 () : void {
      ClaimContactDetailPopup.push(Claim.reporter, Claim)
    }
    
    // 'action' attribute on ButtonInput (id=EditContact_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 126, column 49
    function action_77 () : void {
      ClaimContactDetailPopup.push(Claim.reporter, Claim, true)
    }
    
    // 'action' attribute on TextInput (id=Insured_Name_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 135, column 43
    function action_80 () : void {
      ClaimContactDetailPopup.push(Claim.Insured, Claim, true)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_9 () : void {
      AddressBookPickerPopup.push(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_10 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_102 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_106 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.maincontact, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_125 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_129 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.affectedparty, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_14 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.reporter, Claim)
    }
    
    // 'action' attribute on ButtonInput (id=EditContact_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 126, column 49
    function action_dest_78 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.reporter, Claim, true)
    }
    
    // 'action' attribute on TextInput (id=Insured_Name_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 135, column 43
    function action_dest_81 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.Insured, Claim, true)
    }
    
    // 'def' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_121 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(Person,null,Claim,typekey.ContactRole.TC_AFFECTEDPARTY_TDIC)
    }
    
    // 'def' attribute on PanelRef (id=RightPanel) at FNOLWizard_BasicInfoScreen.default.pcf: line 203, column 82
    function def_onEnter_157 (def :  pcf.FNOLWizard_BasicInfoRightPanelSet_Auto) : void {
      def.onEnter(Claim, Wizard)
    }
    
    // 'def' attribute on PanelRef (id=RightPanel) at FNOLWizard_BasicInfoScreen.default.pcf: line 203, column 82
    function def_onEnter_159 (def :  pcf.FNOLWizard_BasicInfoRightPanelSet_Gl) : void {
      def.onEnter(Claim, Wizard)
    }
    
    // 'def' attribute on PanelRef (id=RightPanel) at FNOLWizard_BasicInfoScreen.default.pcf: line 203, column 82
    function def_onEnter_161 (def :  pcf.FNOLWizard_BasicInfoRightPanelSet_Pr) : void {
      def.onEnter(Claim, Wizard)
    }
    
    // 'def' attribute on PanelRef (id=RightPanel) at FNOLWizard_BasicInfoScreen.default.pcf: line 203, column 82
    function def_onEnter_163 (def :  pcf.FNOLWizard_BasicInfoRightPanelSet_Trav) : void {
      def.onEnter(Claim, Wizard)
    }
    
    // 'def' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_6 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(entity.Person.Type, null, Claim)
    }
    
    // 'def' attribute on InputSetRef (id=reporter_businessphone) at FNOLWizard_BasicInfoScreen.default.pcf: line 103, column 49
    function def_onEnter_60 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#WorkPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.BusinessPhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reporter_homephone) at FNOLWizard_BasicInfoScreen.default.pcf: line 108, column 49
    function def_onEnter_64 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#HomePhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.HomePhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reporter_mobile) at FNOLWizard_BasicInfoScreen.default.pcf: line 113, column 49
    function def_onEnter_68 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, Person#CellPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.Mobile.Label"), false))
    }
    
    // 'def' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_98 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Claim.maincontact), null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_122 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(Person,null,Claim,typekey.ContactRole.TC_AFFECTEDPARTY_TDIC)
    }
    
    // 'def' attribute on PanelRef (id=RightPanel) at FNOLWizard_BasicInfoScreen.default.pcf: line 203, column 82
    function def_refreshVariables_158 (def :  pcf.FNOLWizard_BasicInfoRightPanelSet_Auto) : void {
      def.refreshVariables(Claim, Wizard)
    }
    
    // 'def' attribute on PanelRef (id=RightPanel) at FNOLWizard_BasicInfoScreen.default.pcf: line 203, column 82
    function def_refreshVariables_160 (def :  pcf.FNOLWizard_BasicInfoRightPanelSet_Gl) : void {
      def.refreshVariables(Claim, Wizard)
    }
    
    // 'def' attribute on PanelRef (id=RightPanel) at FNOLWizard_BasicInfoScreen.default.pcf: line 203, column 82
    function def_refreshVariables_162 (def :  pcf.FNOLWizard_BasicInfoRightPanelSet_Pr) : void {
      def.refreshVariables(Claim, Wizard)
    }
    
    // 'def' attribute on PanelRef (id=RightPanel) at FNOLWizard_BasicInfoScreen.default.pcf: line 203, column 82
    function def_refreshVariables_164 (def :  pcf.FNOLWizard_BasicInfoRightPanelSet_Trav) : void {
      def.refreshVariables(Claim, Wizard)
    }
    
    // 'def' attribute on InputSetRef (id=reporter_businessphone) at FNOLWizard_BasicInfoScreen.default.pcf: line 103, column 49
    function def_refreshVariables_61 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#WorkPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.BusinessPhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reporter_homephone) at FNOLWizard_BasicInfoScreen.default.pcf: line 108, column 49
    function def_refreshVariables_65 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#HomePhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.HomePhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reporter_mobile) at FNOLWizard_BasicInfoScreen.default.pcf: line 113, column 49
    function def_refreshVariables_69 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, Person#CellPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.Mobile.Label"), false))
    }
    
    // 'def' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_7 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(entity.Person.Type, null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_99 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Claim.maincontact), null, Claim)
    }
    
    // 'value' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_109 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.maincontact = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_132 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.affectedparty = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_MainContactType_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 186, column 90
    function defaultSetter_145 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.MainContactType = (__VALUE_TO_SET as typekey.PersonRelationType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_AffectedPartyType_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 194, column 92
    function defaultSetter_152 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.AffectedPartType_TDIC = (__VALUE_TO_SET as typekey.PersonRelationType)
    }
    
    // 'value' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.reporter = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TypeKeyInput (id=HowReported_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 44, column 52
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.HowReported = (__VALUE_TO_SET as typekey.HowReportedType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_ReportedByType_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 64, column 54
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ReportedByType = (__VALUE_TO_SET as typekey.PersonRelationType)
    }
    
    // 'value' attribute on DateInput (id=Notification_ReportedDate_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 73, column 43
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ReportedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=reporter_primarytype_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 98, column 49
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      (Claim.reporter as Person).PrimaryPhone = (__VALUE_TO_SET as typekey.PrimaryPhoneType)
    }
    
    // 'value' attribute on TextInput (id=reporter_email_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 120, column 49
    function defaultSetter_72 (__VALUE_TO_SET :  java.lang.Object) : void {
      (Claim.reporter as Person).EmailAddress1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=MainContactChoice_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 154, column 60
    function defaultSetter_94 (__VALUE_TO_SET :  java.lang.Object) : void {
      basicInfoUtils.MainContactSamePerson = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'filter' attribute on TypeKeyInput (id=Claim_ReportedByType_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 64, column 54
    function filter_29 (VALUE :  typekey.PersonRelationType, VALUES :  typekey.PersonRelationType[]) : java.lang.Boolean {
      return VALUE.hasCategory(claimPolicyType)
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard_BasicInfoScreen.default.pcf: line 20, column 26
    function initialValue_0 () : PolicyType {
      return Claim.Policy.PolicyType
    }
    
    // 'label' attribute on Label at FNOLWizard_BasicInfoScreen.default.pcf: line 146, column 212
    function label_91 () : java.lang.String {
      return (!Claim.Policy.Verified)?DisplayKey.get("TDIC.Web.BasicInfoScreen.BasicInfoDetailViewPanel.AffectedParty"):DisplayKey.get("NVV.Claim.NewClaimPeople.Claim.MainContact")
    }
    
    // 'mode' attribute on PanelRef (id=RightPanel) at FNOLWizard_BasicInfoScreen.default.pcf: line 203, column 82
    function mode_165 () : java.lang.Object {
      return gw.config.LOBAbstraction.ForClaim.ForLossType.getUIMode(Claim)
    }
    
    // 'onChange' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 55, column 81
    function onChange_12 () : void {
      basicInfoUtils.setReportedByToInsured()
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizard_BasicInfoScreen.default.pcf: line 65, column 101
    function onChange_25 () : void {
      basicInfoUtils.fillMainContactFromReportedByForSamePerson()
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizard_BasicInfoScreen.default.pcf: line 155, column 80
    function onChange_92 () : void {
      basicInfoUtils.setAffectedParty_TDIC()
    }
    
    // 'onPick' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_107 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.maincontact); var result = eval("Claim.maincontact = Claim.resolveContact(Claim.maincontact) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_130 (PickedValue :  Contact) : void {
      var contactType = entity.Person.Type; var result = eval("Claim.affectedparty = Claim.resolveContact(Claim.affectedparty) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_15 (PickedValue :  Contact) : void {
      var contactType = entity.Person.Type; var result = eval("Claim.reporter = Claim.resolveContact(Claim.reporter) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'validationExpression' attribute on DateInput (id=Notification_ReportedDate_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 73, column 43
    function validationExpression_31 () : java.lang.Object {
      return Claim.ReportedDate != null and Claim.ReportedDate > gw.api.util.DateUtil.currentDate() ? DisplayKey.get("Java.Validation.Date.ForbidFuture") : null
    }
    
    // 'validationExpression' attribute on TypeKeyInput (id=reporter_primarytype_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 98, column 49
    function validationExpression_49 () : java.lang.Object {
      return ((Claim.reporter as Person).PrimaryPhone == null or ((Claim.reporter as Person).PrimaryPhone == TC_WORK and (Claim.reporter as Person).WorkPhone != null) or  ((Claim.reporter as Person).PrimaryPhone == TC_HOME and (Claim.reporter as Person).HomePhone != null) or ((Claim.reporter as Person).PrimaryPhone == TC_MOBILE and (Claim.reporter as Person).CellPhone != null)) ? null : DisplayKey.get("Web.ContactDetail.Phone.PrimaryPhone.Error")
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_19 () : java.lang.Object {
      return Claim.RelatedPersonArray
    }
    
    // 'value' attribute on TypeKeyInput (id=HowReported_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 44, column 52
    function valueRoot_3 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on DateInput (id=DOB_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 81, column 49
    function valueRoot_40 () : java.lang.Object {
      return (Claim.reporter as Person)
    }
    
    // 'value' attribute on TextInput (id=Insured_Address_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 139, column 65
    function valueRoot_86 () : java.lang.Object {
      return Claim.Insured
    }
    
    // 'value' attribute on BooleanRadioInput (id=MainContactChoice_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 154, column 60
    function valueRoot_95 () : java.lang.Object {
      return basicInfoUtils
    }
    
    // 'value' attribute on TypeKeyInput (id=HowReported_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 44, column 52
    function value_1 () : typekey.HowReportedType {
      return Claim.HowReported
    }
    
    // 'value' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_108 () : entity.Person {
      return Claim.maincontact
    }
    
    // 'value' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_131 () : entity.Person {
      return Claim.affectedparty
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_MainContactType_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 186, column 90
    function value_144 () : typekey.PersonRelationType {
      return Claim.MainContactType
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_AffectedPartyType_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 194, column 92
    function value_151 () : typekey.PersonRelationType {
      return Claim.AffectedPartType_TDIC
    }
    
    // 'value' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_16 () : entity.Contact {
      return Claim.reporter
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_ReportedByType_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 64, column 54
    function value_26 () : typekey.PersonRelationType {
      return Claim.ReportedByType
    }
    
    // 'value' attribute on DateInput (id=Notification_ReportedDate_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 73, column 43
    function value_32 () : java.util.Date {
      return Claim.ReportedDate
    }
    
    // 'value' attribute on DateInput (id=DOB_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 81, column 49
    function value_39 () : java.util.Date {
      return (Claim.reporter as Person).DateOfBirth
    }
    
    // 'value' attribute on TextInput (id=reporter_Address_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 89, column 49
    function value_45 () : java.lang.String {
      return (Claim.reporter as Person).PrimaryAddressDisplayValue
    }
    
    // 'value' attribute on TypeKeyInput (id=reporter_primarytype_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 98, column 49
    function value_52 () : typekey.PrimaryPhoneType {
      return (Claim.reporter as Person).PrimaryPhone
    }
    
    // 'value' attribute on TextInput (id=reporter_email_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 120, column 49
    function value_71 () : java.lang.String {
      return (Claim.reporter as Person).EmailAddress1
    }
    
    // 'value' attribute on TextInput (id=Insured_Name_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 135, column 43
    function value_82 () : entity.Contact {
      return Claim.Insured
    }
    
    // 'value' attribute on TextInput (id=Insured_Address_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 139, column 65
    function value_85 () : java.lang.String {
      return Claim.Insured.PrimaryAddressDisplayValue
    }
    
    // 'value' attribute on TextInput (id=Insured_WorkPhone_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 143, column 56
    function value_88 () : java.lang.String {
      return Claim.Insured.PrimaryPhoneValue
    }
    
    // 'value' attribute on BooleanRadioInput (id=MainContactChoice_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 154, column 60
    function value_93 () : java.lang.Boolean {
      return basicInfoUtils.MainContactSamePerson
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_112 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_112 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_112 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_135 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_135 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_135 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_20 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_20 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_20 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_113 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_112(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_136 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_135(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_21 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_20(__valueRangeArg)
    }
    
    // 'valueType' attribute on ClaimContactInput (id=MainContact_Name_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 166, column 90
    function verifyValueType_119 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 178, column 91
    function verifyValueType_142 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'visible' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_100 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function visible_104 () : java.lang.Boolean {
      return !basicInfoUtils.MainContactSamePerson and Claim.Policy.Verified
    }
    
    // 'visible' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_123 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(entity.Person.Type, Claim, null as List<SpecialistService>)" != "" && false
    }
    
    // 'visible' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function visible_127 () : java.lang.Boolean {
      return !basicInfoUtils.MainContactSamePerson and !Claim.Policy.Verified
    }
    
    // 'visible' attribute on TypeKeyInput (id=Claim_AffectedPartyType_Input) at FNOLWizard_BasicInfoScreen.default.pcf: line 194, column 92
    function visible_150 () : java.lang.Boolean {
      return !basicInfoUtils.MainContactSamePerson and !Claim.Policy.Verified 
    }
    
    // 'visible' attribute on Label at FNOLWizard_BasicInfoScreen.default.pcf: line 76, column 49
    function visible_37 () : java.lang.Boolean {
      return Claim.reporter != null
    }
    
    // 'visible' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_5 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_8 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(entity.Person.Type, Claim, null as List<SpecialistService>)" != "" && true
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get Wizard () : gw.api.claim.NewClaimWizardInfo {
      return getRequireValue("Wizard", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set Wizard ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setRequireValue("Wizard", 0, $arg)
    }
    
    property get basicInfoUtils () : gw.pcf.fnol.BasicInfoScreenUtils {
      return getRequireValue("basicInfoUtils", 0) as gw.pcf.fnol.BasicInfoScreenUtils
    }
    
    property set basicInfoUtils ($arg :  gw.pcf.fnol.BasicInfoScreenUtils) {
      setRequireValue("basicInfoUtils", 0, $arg)
    }
    
    property get claimPolicyType () : PolicyType {
      return getVariableValue("claimPolicyType", 0) as PolicyType
    }
    
    property set claimPolicyType ($arg :  PolicyType) {
      setVariableValue("claimPolicyType", 0, $arg)
    }
    
    property get contact () : Contact {
      return getVariableValue("contact", 0) as Contact
    }
    
    property set contact ($arg :  Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    
  }
  
  
}