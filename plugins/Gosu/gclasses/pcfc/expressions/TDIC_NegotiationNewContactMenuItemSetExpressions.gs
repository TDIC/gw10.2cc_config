package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_NegotiationNewContactMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_NegotiationNewContactMenuItemSetExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_NegotiationNewContactMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_NegotiationNewContactMenuItemSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_NewPerson) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 13, column 73
    function action_0 () : void {
      NewContactPopup.push(typekey.Contact.TC_PERSON, null,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Company) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 37, column 74
    function action_10 () : void {
      NewContactPopup.push(typekey.Contact.TC_COMPANY, null,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Adjudicator) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 44, column 88
    function action_12 () : void {
      TDIC_NewContactwithdiffNamePopup.push(typekey.Contact.TC_ATTORNEY, null ,claim,"Plaintiff Attorney")
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Attorney) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 48, column 86
    function action_14 () : void {
      TDIC_NewContactwithdiffNamePopup.push(typekey.Contact.TC_ATTORNEY, null ,claim,"Defense Attorney")
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LawFirm) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 52, column 73
    function action_16 () : void {
      NewContactPopup.push(typekey.Contact.TC_LAWFIRM, null,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LegalVenue) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 56, column 79
    function action_18 () : void {
      TDIC_NewContactwithdiffNamePopup.push(typekey.Contact.TC_ATTORNEY, null ,claim,"Mediator")
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Doctor) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 20, column 71
    function action_2 () : void {
      NewContactPopup.push(typekey.Contact.TC_DOCTOR,null,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_MedicalCareOrg) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 24, column 80
    function action_4 () : void {
      NewContactPopup.push(typekey.Contact.TC_MEDICALCAREORG, null, claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_CompanyVendor) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 28, column 79
    function action_6 () : void {
      NewContactPopup.push(typekey.Contact.TC_COMPANYVENDOR, null,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_PersonVendor) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 32, column 78
    function action_8 () : void {
      NewContactPopup.push(typekey.Contact.TC_PERSONVENDOR, null,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_NewPerson) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 13, column 73
    function action_dest_1 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_PERSON, null,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Company) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 37, column 74
    function action_dest_11 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_COMPANY, null,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Adjudicator) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 44, column 88
    function action_dest_13 () : pcf.api.Destination {
      return pcf.TDIC_NewContactwithdiffNamePopup.createDestination(typekey.Contact.TC_ATTORNEY, null ,claim,"Plaintiff Attorney")
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Attorney) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 48, column 86
    function action_dest_15 () : pcf.api.Destination {
      return pcf.TDIC_NewContactwithdiffNamePopup.createDestination(typekey.Contact.TC_ATTORNEY, null ,claim,"Defense Attorney")
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LawFirm) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 52, column 73
    function action_dest_17 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_LAWFIRM, null,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LegalVenue) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 56, column 79
    function action_dest_19 () : pcf.api.Destination {
      return pcf.TDIC_NewContactwithdiffNamePopup.createDestination(typekey.Contact.TC_ATTORNEY, null ,claim,"Mediator")
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Doctor) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 20, column 71
    function action_dest_3 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_DOCTOR,null,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_MedicalCareOrg) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 24, column 80
    function action_dest_5 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_MEDICALCAREORG, null, claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_CompanyVendor) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 28, column 79
    function action_dest_7 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_COMPANYVENDOR, null,claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_PersonVendor) at TDIC_NegotiationNewContactMenuItemSet.pcf: line 32, column 78
    function action_dest_9 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_PERSONVENDOR, null,claim)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    
  }
  
  
}