package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/contacts/ClaimNewContactPickerMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClaimNewContactPickerMenuItemSetExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/contacts/ClaimNewContactPickerMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimNewContactPickerMenuItemSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=NewContactPickerMenuItemSet_NewPerson) at ClaimNewContactPickerMenuItemSet.pcf: line 21, column 96
    function action_1 () : void {
      NewContactPopup.push(typekey.Contact.TC_PERSON, parentContact, claim)
    }
    
    // 'action' attribute on MenuItem (id=NewLawFirm) at ClaimNewContactPickerMenuItemSet.pcf: line 72, column 73
    function action_10 () : void {
      NewContactPopup.push(typekey.Contact.TC_LAWFIRM, parentContact, claim)
    }
    
    // 'action' attribute on MenuItem (id=NewContactPickerMenuItemSet_NewCompany) at ClaimNewContactPickerMenuItemSet.pcf: line 57, column 71
    function action_4 () : void {
      NewContactPopup.push(typekey.Contact.TC_COMPANY, parentContact, claim)
    }
    
    // 'action' attribute on MenuItem (id=NewAdjudicator) at ClaimNewContactPickerMenuItemSet.pcf: line 64, column 88
    function action_6 () : void {
      NewContactPopup.push(typekey.Contact.TC_ADJUDICATOR, parentContact, claim)
    }
    
    // 'action' attribute on MenuItem (id=NewAttorney) at ClaimNewContactPickerMenuItemSet.pcf: line 68, column 86
    function action_8 () : void {
      NewContactPopup.push(typekey.Contact.TC_ATTORNEY, parentContact, claim)
    }
    
    // 'action' attribute on MenuItem (id=NewLawFirm) at ClaimNewContactPickerMenuItemSet.pcf: line 72, column 73
    function action_dest_11 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_LAWFIRM, parentContact, claim)
    }
    
    // 'action' attribute on MenuItem (id=NewContactPickerMenuItemSet_NewPerson) at ClaimNewContactPickerMenuItemSet.pcf: line 21, column 96
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_PERSON, parentContact, claim)
    }
    
    // 'action' attribute on MenuItem (id=NewContactPickerMenuItemSet_NewCompany) at ClaimNewContactPickerMenuItemSet.pcf: line 57, column 71
    function action_dest_5 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_COMPANY, parentContact, claim)
    }
    
    // 'action' attribute on MenuItem (id=NewAdjudicator) at ClaimNewContactPickerMenuItemSet.pcf: line 64, column 88
    function action_dest_7 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_ADJUDICATOR, parentContact, claim)
    }
    
    // 'action' attribute on MenuItem (id=NewAttorney) at ClaimNewContactPickerMenuItemSet.pcf: line 68, column 86
    function action_dest_9 () : pcf.api.Destination {
      return pcf.NewContactPopup.createDestination(typekey.Contact.TC_ATTORNEY, parentContact, claim)
    }
    
    // 'visible' attribute on MenuItem (id=NewContactPickerMenuItemSet_NewPerson) at ClaimNewContactPickerMenuItemSet.pcf: line 21, column 96
    function visible_0 () : java.lang.Boolean {
      return requiredContactType == entity.Contact or requiredContactType == entity.Person
    }
    
    // 'visible' attribute on MenuItem (id=NewContactPickerMenuItemSet_NewCompany) at ClaimNewContactPickerMenuItemSet.pcf: line 57, column 71
    function visible_3 () : java.lang.Boolean {
      return requiredContactType.isAssignableFrom(entity.Company)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get parentContact () : Contact {
      return getRequireValue("parentContact", 0) as Contact
    }
    
    property set parentContact ($arg :  Contact) {
      setRequireValue("parentContact", 0, $arg)
    }
    
    property get requiredContactType () : Type {
      return getRequireValue("requiredContactType", 0) as Type
    }
    
    property set requiredContactType ($arg :  Type) {
      setRequireValue("requiredContactType", 0, $arg)
    }
    
    property get ShowNewVendor() : boolean {
      return {entity.Contact,
              entity.Company,
              entity.Person,
              entity.PersonVendor,
              entity.CompanyVendor,
              entity.AutoRepairShop,
              entity.AutoTowingAgcy,
              entity.Doctor,
              entity.MedicalCareOrg}.contains(requiredContactType)
    }
    
    property get ShowNewLegal() : boolean {
      return {entity.Contact,
              entity.Attorney,
              entity.LawFirm,
              entity.Company,
              entity.LegalVenue,
              entity.Person,
              entity.Adjudicator}.contains(requiredContactType)
    }
    
    
  }
  
  
}