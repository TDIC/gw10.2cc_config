package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderRequestContactScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseFolderRequestContactScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderRequestContactScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseFolderRequestContactScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_MedicalCareOrg) at ShareBaseFolderRequestContactScreen.pcf: line 65, column 86
    function action_11 () : void {
      ShareBaseFolderRequestNewContactPopup.push(claim, typekey.Contact.TC_MEDICALCAREORG, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_CompanyVendor) at ShareBaseFolderRequestContactScreen.pcf: line 69, column 85
    function action_13 () : void {
      ShareBaseFolderRequestNewContactPopup.push(claim, typekey.Contact.TC_COMPANYVENDOR, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Company) at ShareBaseFolderRequestContactScreen.pcf: line 74, column 77
    function action_15 () : void {
      ShareBaseFolderRequestNewContactPopup.push(claim, typekey.Contact.TC_COMPANY, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Adjudicator) at ShareBaseFolderRequestContactScreen.pcf: line 81, column 83
    function action_17 () : void {
      ShareBaseFolderRequestNewContactPopup.push(claim, typekey.Contact.TC_ADJUDICATOR, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Attorney) at ShareBaseFolderRequestContactScreen.pcf: line 85, column 80
    function action_19 () : void {
      ShareBaseFolderRequestNewContactPopup.push(claim, typekey.Contact.TC_ATTORNEY, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LawFirm) at ShareBaseFolderRequestContactScreen.pcf: line 89, column 79
    function action_21 () : void {
      ShareBaseFolderRequestNewContactPopup.push(claim, typekey.Contact.TC_LAWFIRM, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LegalVenue) at ShareBaseFolderRequestContactScreen.pcf: line 93, column 82
    function action_23 () : void {
      ShareBaseFolderRequestNewContactPopup.push(claim, typekey.Contact.TC_LEGALVENUE, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_NewPerson) at ShareBaseFolderRequestContactScreen.pcf: line 46, column 76
    function action_3 () : void {
      ShareBaseFolderRequestNewContactPopup.push(claim, typekey.Contact.TC_PERSON, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_AutoRepairShop) at ShareBaseFolderRequestContactScreen.pcf: line 53, column 86
    function action_5 () : void {
      ShareBaseFolderRequestNewContactPopup.push(claim, typekey.Contact.TC_AUTOREPAIRSHOP, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_AutoTowingAgcy) at ShareBaseFolderRequestContactScreen.pcf: line 57, column 86
    function action_7 () : void {
      ShareBaseFolderRequestNewContactPopup.push(claim, typekey.Contact.TC_AUTOTOWINGAGCY, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Doctor) at ShareBaseFolderRequestContactScreen.pcf: line 61, column 78
    function action_9 () : void {
      ShareBaseFolderRequestNewContactPopup.push(claim, typekey.Contact.TC_DOCTOR, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Doctor) at ShareBaseFolderRequestContactScreen.pcf: line 61, column 78
    function action_dest_10 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestNewContactPopup.createDestination(claim, typekey.Contact.TC_DOCTOR, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_MedicalCareOrg) at ShareBaseFolderRequestContactScreen.pcf: line 65, column 86
    function action_dest_12 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestNewContactPopup.createDestination(claim, typekey.Contact.TC_MEDICALCAREORG, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_CompanyVendor) at ShareBaseFolderRequestContactScreen.pcf: line 69, column 85
    function action_dest_14 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestNewContactPopup.createDestination(claim, typekey.Contact.TC_COMPANYVENDOR, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Company) at ShareBaseFolderRequestContactScreen.pcf: line 74, column 77
    function action_dest_16 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestNewContactPopup.createDestination(claim, typekey.Contact.TC_COMPANY, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Adjudicator) at ShareBaseFolderRequestContactScreen.pcf: line 81, column 83
    function action_dest_18 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestNewContactPopup.createDestination(claim, typekey.Contact.TC_ADJUDICATOR, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Attorney) at ShareBaseFolderRequestContactScreen.pcf: line 85, column 80
    function action_dest_20 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestNewContactPopup.createDestination(claim, typekey.Contact.TC_ATTORNEY, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LawFirm) at ShareBaseFolderRequestContactScreen.pcf: line 89, column 79
    function action_dest_22 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestNewContactPopup.createDestination(claim, typekey.Contact.TC_LAWFIRM, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LegalVenue) at ShareBaseFolderRequestContactScreen.pcf: line 93, column 82
    function action_dest_24 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestNewContactPopup.createDestination(claim, typekey.Contact.TC_LEGALVENUE, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_NewPerson) at ShareBaseFolderRequestContactScreen.pcf: line 46, column 76
    function action_dest_4 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestNewContactPopup.createDestination(claim, typekey.Contact.TC_PERSON, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_AutoRepairShop) at ShareBaseFolderRequestContactScreen.pcf: line 53, column 86
    function action_dest_6 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestNewContactPopup.createDestination(claim, typekey.Contact.TC_AUTOREPAIRSHOP, Request)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_AutoTowingAgcy) at ShareBaseFolderRequestContactScreen.pcf: line 57, column 86
    function action_dest_8 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestNewContactPopup.createDestination(claim, typekey.Contact.TC_AUTOTOWINGAGCY, Request)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=UnlinkContacts) at ShareBaseFolderRequestContactScreen.pcf: line 38, column 110
    function checkedRowAction_2 (element :  entity.Contact, CheckedValue :  entity.Contact) : void {
      Request.removeRecipient((CheckedValue as Contact))
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=linkContactsOG) at ShareBaseFolderRequestContactScreen.pcf: line 109, column 107
    function checkedRowAction_27 (element :  entity.Contact, CheckedValue :  entity.Contact) : void {
      Request.addRecipient(CheckedValue as Contact)
    }
    
    // 'def' attribute on PanelRef (id=selectedContacts) at ShareBaseFolderRequestContactScreen.pcf: line 30, column 29
    function def_onEnter_25 (def :  pcf.ShareBaseFolderRequestContactsLV) : void {
      def.onEnter(claim, Request.LinkedRecipients,true)
    }
    
    // 'def' attribute on PanelRef (id=availableContacts) at ShareBaseFolderRequestContactScreen.pcf: line 101, column 30
    function def_onEnter_28 (def :  pcf.ShareBaseFolderAvailableContactsLV) : void {
      def.onEnter(claim,Request.ContactsNotLinked)
    }
    
    // 'def' attribute on PanelRef (id=selectedContacts) at ShareBaseFolderRequestContactScreen.pcf: line 30, column 29
    function def_refreshVariables_26 (def :  pcf.ShareBaseFolderRequestContactsLV) : void {
      def.refreshVariables(claim, Request.LinkedRecipients,true)
    }
    
    // 'def' attribute on PanelRef (id=availableContacts) at ShareBaseFolderRequestContactScreen.pcf: line 101, column 30
    function def_refreshVariables_29 (def :  pcf.ShareBaseFolderAvailableContactsLV) : void {
      def.refreshVariables(claim,Request.ContactsNotLinked)
    }
    
    // 'label' attribute on AlertBar (id=NoUserEmail) at ShareBaseFolderRequestContactScreen.pcf: line 27, column 70
    function label_1 () : java.lang.Object {
      return DisplayKey.get("Accelerator.OnBase.ShareBase.STR_GW_WarningNoEmailForUser", User.util.CurrentUser.Contact.DisplayName)
    }
    
    // 'visible' attribute on AlertBar (id=NoUserEmail) at ShareBaseFolderRequestContactScreen.pcf: line 27, column 70
    function visible_0 () : java.lang.Boolean {
      return User.util.CurrentUser.Contact.EmailAddress1 == null
    }
    
    property get Request () : InboundRequest_Ext {
      return getRequireValue("Request", 0) as InboundRequest_Ext
    }
    
    property set Request ($arg :  InboundRequest_Ext) {
      setRequireValue("Request", 0, $arg)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    
  }
  
  
}