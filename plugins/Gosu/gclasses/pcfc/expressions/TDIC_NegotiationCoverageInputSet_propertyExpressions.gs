package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.financials.CurrencyAmount
@javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_NegotiationCoverageInputSet.property.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_NegotiationCoverageInputSet_propertyExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_NegotiationCoverageInputSet.property.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_NegotiationCoverageInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at TDIC_NegotiationCoverageInputSet.property.pcf: line 25, column 26
    function valueRoot_2 () : java.lang.Object {
      return cov.Type
    }
    
    // 'value' attribute on TextCell (id=limit_Cell) at TDIC_NegotiationCoverageInputSet.property.pcf: line 29, column 27
    function valueRoot_5 () : java.lang.Object {
      return cov
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at TDIC_NegotiationCoverageInputSet.property.pcf: line 25, column 26
    function value_1 () : java.lang.String {
      return cov.Type.DisplayName
    }
    
    // 'value' attribute on TextCell (id=limit_Cell) at TDIC_NegotiationCoverageInputSet.property.pcf: line 29, column 27
    function value_4 () : gw.api.financials.CurrencyAmount {
      return cov.ExposureLimit
    }
    
    property get cov () : PropertyCoverage {
      return getIteratedValue(1) as PropertyCoverage
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/newother/TDIC_NegotiationCoverageInputSet.property.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_NegotiationCoverageInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label at TDIC_NegotiationCoverageInputSet.property.pcf: line 12, column 44
    function label_0 () : java.lang.String {
      return claim.Type_TDIC.DisplayName
    }
    
    // 'value' attribute on RowIterator at TDIC_NegotiationCoverageInputSet.property.pcf: line 21, column 67
    function value_7 () : java.util.ArrayList<PropertyCoverage> {
      return getCoverages()
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    
    function getCoverages() : ArrayList<PropertyCoverage> {      
     var covList = new ArrayList<PropertyCoverage>()
      claim.Policy?.RiskUnits.each(\elt ->  {
          if (elt != null) {
            (elt as LocationBasedRU)?.Coverages?.each(\pc ->  {
                if (pc != null) {
                  var propertyCoverage = pc as PropertyCoverage
                  if ((claim.Type_TDIC == ClaimType_TDIC.TC_PROPERTY
                    and (propertyCoverage.Type == CoverageType.TC_BOPBUILDINGCOV
                    or propertyCoverage.Type == CoverageType.TC_BOPPERSONALPROPCOV
                    or propertyCoverage.Type == CoverageType.TC_BOPEXTRAEXPENSECOV_TDIC
                    or propertyCoverage.Type == CoverageType.TC_BOPVALUABLEPAPERSCOV_TDIC
                    or propertyCoverage.Type == CoverageType.TC_BOPLOSSOFINCOMETDIC )) 
                    or (claim.Type_TDIC != ClaimType_TDIC.TC_PROPERTY)) {
                    covList.add(pc as PropertyCoverage)                    
                  }
                }
            })
          }
        }
      )
     return covList
    }
    
    
  }
  
  
}