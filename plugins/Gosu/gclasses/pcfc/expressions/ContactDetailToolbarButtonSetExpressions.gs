package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses java.lang.Exception
uses com.guidewire.pl.web.controller.UserDisplayableException
@javax.annotation.Generated("config/web/pcf/shared/contacts/ContactDetailToolbarButtonSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactDetailToolbarButtonSetExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/contacts/ContactDetailToolbarButtonSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactDetailToolbarButtonSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=CheckDuplicatesButton) at ContactDetailToolbarButtonSet.pcf: line 67, column 147
    function action_12 () : void {
      checkForDuplicates(true)
    }
    
    // 'action' attribute on ToolbarButton (id=CustomUpdateButton) at ContactDetailToolbarButtonSet.pcf: line 52, column 147
    function action_2 () : void {
      customUpdate()
    }
    
    // 'available' attribute on ToolbarButton (id=CustomUpdateButton) at ContactDetailToolbarButtonSet.pcf: line 52, column 147
    function available_0 () : java.lang.Boolean {
      return checkDuplicate
    }
    
    // EditButtons at ContactDetailToolbarButtonSet.pcf: line 55, column 124
    function label_5 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at ContactDetailToolbarButtonSet.pcf: line 55, column 124
    function pickValue_3 () : Contact {
      return canPick ? Contact : null
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ContactDetailToolbarButtonSet.pcf: line 59, column 39
    function toolbarButtonSet_onEnter_7 (def :  pcf.LinkContactToolbarButtonSet) : void {
      def.onEnter(contactHandle, linkStatus)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ContactDetailToolbarButtonSet.pcf: line 62, column 110
    function toolbarButtonSet_onEnter_9 (def :  pcf.ViewAddressBookToolbarButtonSet) : void {
      def.onEnter(contactHandle, linkStatus, allowEditInAddressBook)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ContactDetailToolbarButtonSet.pcf: line 62, column 110
    function toolbarButtonSet_refreshVariables_10 (def :  pcf.ViewAddressBookToolbarButtonSet) : void {
      def.refreshVariables(contactHandle, linkStatus, allowEditInAddressBook)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ContactDetailToolbarButtonSet.pcf: line 59, column 39
    function toolbarButtonSet_refreshVariables_8 (def :  pcf.LinkContactToolbarButtonSet) : void {
      def.refreshVariables(contactHandle, linkStatus)
    }
    
    // 'visible' attribute on ToolbarButton (id=CustomUpdateButton) at ContactDetailToolbarButtonSet.pcf: line 52, column 147
    function visible_1 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and Contact.PublicID == null and Contact.AddressBookUID == null and claim.LossType != LossType.TC_WC7
    }
    
    // 'visible' attribute on ToolbarButton (id=CheckDuplicatesButton) at ContactDetailToolbarButtonSet.pcf: line 67, column 147
    function visible_11 () : java.lang.Boolean {
      return Contact.PublicID == null and Contact.AddressBookUID == null and CurrentLocation.InEditMode and claim.LossType != LossType.TC_WC7
    }
    
    // 'updateVisible' attribute on EditButtons at ContactDetailToolbarButtonSet.pcf: line 55, column 124
    function visible_4 () : java.lang.Boolean {
      return !(Contact.PublicID == null and Contact.AddressBookUID == null) or claim.LossType == LossType.TC_WC7
    }
    
    // 'visible' attribute on ToolbarButtonSetRef at ContactDetailToolbarButtonSet.pcf: line 59, column 39
    function visible_6 () : java.lang.Boolean {
      return canAccessLinkButtons
    }
    
    property get allowEditInAddressBook () : boolean {
      return getRequireValue("allowEditInAddressBook", 0) as java.lang.Boolean
    }
    
    property set allowEditInAddressBook ($arg :  boolean) {
      setRequireValue("allowEditInAddressBook", 0, $arg)
    }
    
    property get alreadyCheckedDuplicates () : boolean {
      return getVariableValue("alreadyCheckedDuplicates", 0) as java.lang.Boolean
    }
    
    property set alreadyCheckedDuplicates ($arg :  boolean) {
      setVariableValue("alreadyCheckedDuplicates", 0, $arg)
    }
    
    property get canAccessLinkButtons () : boolean {
      return getRequireValue("canAccessLinkButtons", 0) as java.lang.Boolean
    }
    
    property set canAccessLinkButtons ($arg :  boolean) {
      setRequireValue("canAccessLinkButtons", 0, $arg)
    }
    
    property get canEdit () : boolean {
      return getRequireValue("canEdit", 0) as java.lang.Boolean
    }
    
    property set canEdit ($arg :  boolean) {
      setRequireValue("canEdit", 0, $arg)
    }
    
    property get canPick () : boolean {
      return getRequireValue("canPick", 0) as java.lang.Boolean
    }
    
    property set canPick ($arg :  boolean) {
      setRequireValue("canPick", 0, $arg)
    }
    
    property get checkDuplicate () : Boolean {
      return getVariableValue("checkDuplicate", 0) as Boolean
    }
    
    property set checkDuplicate ($arg :  Boolean) {
      setVariableValue("checkDuplicate", 0, $arg)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get contactHandle () : gw.api.contact.ContactHandle {
      return getRequireValue("contactHandle", 0) as gw.api.contact.ContactHandle
    }
    
    property set contactHandle ($arg :  gw.api.contact.ContactHandle) {
      setRequireValue("contactHandle", 0, $arg)
    }
    
    property get linkStatus () : gw.api.contact.ContactSystemLinkStatus {
      return getRequireValue("linkStatus", 0) as gw.api.contact.ContactSystemLinkStatus
    }
    
    property set linkStatus ($arg :  gw.api.contact.ContactSystemLinkStatus) {
      setRequireValue("linkStatus", 0, $arg)
    }
    
    
    
        property get Contact() : Contact {
          return contactHandle.Contact;
        }
    
        property get ClaimContact() : ClaimContact {
          return contactHandle typeis ClaimContact ? (contactHandle) : null;
        }
    
        function customUpdate() {
          if (!alreadyCheckedDuplicates and Contact.PublicID == null and Contact.AddressBookUID == null and (gw.plugin.contact.ContactSystemApprovalUtil.shouldCreateInContactSystem(Contact) or ClaimContact == null)) {
            if (!checkForDuplicates(false)) {
              commitContact_TDIC()
            }
          } else {
              commitContact_TDIC()
          }
        }
    
        function checkForDuplicates(showMessage : boolean) : boolean {
          var hasDuplicates : boolean
          
          try {
            hasDuplicates = gw.api.contact.ContactUtil.findDuplicates(Contact, ClaimContact, showMessage, null)
            checkDuplicate = true
          } catch (e : Exception) {
            if (showMessage) {
              throw e
            }
          }
          alreadyCheckedDuplicates = true;
          return hasDuplicates;
        }
    
        /**
          * US22
          * 08/26/2014 robk
          */
        function requiredPhoneFieldsPresent_TDIC() : boolean {
          if (ClaimContact.Roles != null and ClaimContact.Roles.firstWhere( \ r -> r.Role == typekey.ContactRole.TC_INJURED or r.Role == typekey.ContactRole.TC_CLAIMANT) != null) {
            var injuredPerson = Contact as Person
            if (injuredPerson.WorkPhone == null and injuredPerson.HomePhone == null and injuredPerson.CellPhone == null) {
              throw new UserDisplayableException(DisplayKey.get("TDIC.ContactDetailToolbarButtonSet.InjuredPersonPhoneRequired"))
            }
          }
          return true
        }
    
        /**
         * US22
         * 08/26/2014 robk
         *
         * Check that required phone fields are present before committing contact.
         */
        function commitContact_TDIC() {
          if (requiredPhoneFieldsPresent_TDIC()) {
            gw.api.contact.ContactUtil.customCommit(canPick, Contact, CurrentLocation)
          }
        }
    
      
    
    
  }
  
  
}