package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/SharedDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SharedDocumentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/SharedDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SharedDocumentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at SharedDocumentsLV.pcf: line 50, column 145
    function action_11 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at SharedDocumentsLV.pcf: line 44, column 147
    function action_8 () : void {
      document.viewOnBaseDocument()
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at SharedDocumentsLV.pcf: line 31, column 32
    function icon_6 () : java.lang.String {
      return document.Icon
    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at SharedDocumentsLV.pcf: line 44, column 147
    function label_9 () : java.lang.Object {
      return document.Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at SharedDocumentsLV.pcf: line 57, column 45
    function valueRoot_14 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at SharedDocumentsLV.pcf: line 57, column 45
    function value_13 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at SharedDocumentsLV.pcf: line 63, column 58
    function value_16 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at SharedDocumentsLV.pcf: line 68, column 51
    function value_19 () : typekey.DocumentStatusType {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at SharedDocumentsLV.pcf: line 73, column 36
    function value_22 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at SharedDocumentsLV.pcf: line 82, column 24
    function value_25 () : java.util.Date {
      return document.DateModified
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at SharedDocumentsLV.pcf: line 50, column 145
    function visible_10 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at SharedDocumentsLV.pcf: line 44, column 147
    function visible_7 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    property get document () : entity.Document {
      return getIteratedValue(1) as entity.Document
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/onbase/SharedDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SharedDocumentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on LinkCell (id=Name) at SharedDocumentsLV.pcf: line 38, column 23
    function sortValue_0 (document :  entity.Document) : java.lang.Object {
      return document.Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at SharedDocumentsLV.pcf: line 57, column 45
    function sortValue_1 (document :  entity.Document) : java.lang.Object {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at SharedDocumentsLV.pcf: line 63, column 58
    function sortValue_2 (document :  entity.Document) : java.lang.Object {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at SharedDocumentsLV.pcf: line 68, column 51
    function sortValue_3 (document :  entity.Document) : java.lang.Object {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at SharedDocumentsLV.pcf: line 73, column 36
    function sortValue_4 (document :  entity.Document) : java.lang.Object {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at SharedDocumentsLV.pcf: line 82, column 24
    function sortValue_5 (document :  entity.Document) : java.lang.Object {
      return document.DateModified
    }
    
    // 'value' attribute on RowIterator (id=SelectedDocumentsIterator) at SharedDocumentsLV.pcf: line 22, column 40
    function value_28 () : List<Document> {
      return sharedDocuments
    }
    
    // 'valueType' attribute on RowIterator (id=SelectedDocumentsIterator) at SharedDocumentsLV.pcf: line 22, column 40
    function verifyValueTypeIsAllowedType_29 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator (id=SelectedDocumentsIterator) at SharedDocumentsLV.pcf: line 22, column 40
    function verifyValueTypeIsAllowedType_29 ($$arg :  gw.api.iterator.IteratorBackingData) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator (id=SelectedDocumentsIterator) at SharedDocumentsLV.pcf: line 22, column 40
    function verifyValueTypeIsAllowedType_29 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator (id=SelectedDocumentsIterator) at SharedDocumentsLV.pcf: line 22, column 40
    function verifyValueType_30 () : void {
      var __valueTypeArg : List<Document>
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the valueType is not a valid type for use with an iterator
      // The valueType for an iterator must be an array or extend from List or IQueryBeanResult
      verifyValueTypeIsAllowedType_29(__valueTypeArg)
    }
    
    property get sharedDocuments () : List<Document> {
      return getRequireValue("sharedDocuments", 0) as List<Document>
    }
    
    property set sharedDocuments ($arg :  List<Document>) {
      setRequireValue("sharedDocuments", 0, $arg)
    }
    
    
  }
  
  
}