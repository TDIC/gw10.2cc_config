package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/lossdetails/LossDetailsDV.Wc7.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LossDetailsDV_Wc7Expressions {
  @javax.annotation.Generated("config/web/pcf/claim/lossdetails/LossDetailsDV.Wc7.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LossDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_110 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.claimant), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_112 () : void {
      if (Claim.claimant != null) { ClaimContactDetailPopup.push(Claim.claimant, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_114 () : void {
      ClaimContactDetailPopup.push(Claim.claimant, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_212 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.supervisor), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_214 () : void {
      if (Claim.supervisor != null) { ClaimContactDetailPopup.push(Claim.supervisor, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_216 () : void {
      ClaimContactDetailPopup.push(Claim.supervisor, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_464 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.reporter), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_466 () : void {
      if (Claim.reporter != null) { ClaimContactDetailPopup.push(Claim.reporter, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_468 () : void {
      ClaimContactDetailPopup.push(Claim.reporter, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_494 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_496 () : void {
      if (Claim.maincontact != null) { ClaimContactDetailPopup.push(Claim.maincontact, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_498 () : void {
      ClaimContactDetailPopup.push(Claim.maincontact, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_111 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.claimant), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_115 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.claimant, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_213 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.supervisor), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_217 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.supervisor, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_465 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.reporter), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_469 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.reporter, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_495 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_499 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.maincontact, Claim)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=MakeFirstButton) at LossDetailsDV.Wc7.pcf: line 682, column 111
    function checkedRowAction_543 (element :  entity.BodyPartDetails, CheckedValue :  entity.BodyPartDetails) : void {
      Claim.ensureClaimInjuryIncident().FirstBodyPart = CheckedValue
    }
    
    // 'def' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_107 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Claim.claimant), null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_209 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Claim.supervisor), null, Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Wc7.pcf: line 340, column 58
    function def_onEnter_286 (def :  pcf.EditableConcurrentEmploymentLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_461 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Claim.reporter), null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_491 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Claim.maincontact), null, Claim)
    }
    
    // 'def' attribute on InputSetRef at LossDetailsDV.Wc7.pcf: line 661, column 42
    function def_onEnter_541 (def :  pcf.CompensableInputSet) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Wc7.pcf: line 670, column 41
    function def_onEnter_545 (def :  pcf.EditableBodyPartDetailsLV) : void {
      def.onEnter(Claim.ensureClaimInjuryIncident(), true)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Wc7.pcf: line 691, column 41
    function def_onEnter_548 (def :  pcf.EditableConcurrentEmploymentLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Wc7.pcf: line 703, column 41
    function def_onEnter_551 (def :  pcf.EditableOtherBenefitsLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on InputSetRef at LossDetailsDV.Wc7.pcf: line 102, column 42
    function def_onEnter_77 (def :  pcf.TDIC_CCAddressInputSet) : void {
      def.onEnter(tdic.cc.config.fnol.TDIC_WC7FNOLHelper.getClaimAddressOwner(Claim))
    }
    
    // 'def' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_108 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Claim.claimant), null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_210 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Claim.supervisor), null, Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Wc7.pcf: line 340, column 58
    function def_refreshVariables_287 (def :  pcf.EditableConcurrentEmploymentLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_462 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Claim.reporter), null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_492 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Claim.maincontact), null, Claim)
    }
    
    // 'def' attribute on InputSetRef at LossDetailsDV.Wc7.pcf: line 661, column 42
    function def_refreshVariables_542 (def :  pcf.CompensableInputSet) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Wc7.pcf: line 670, column 41
    function def_refreshVariables_546 (def :  pcf.EditableBodyPartDetailsLV) : void {
      def.refreshVariables(Claim.ensureClaimInjuryIncident(), true)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Wc7.pcf: line 691, column 41
    function def_refreshVariables_549 (def :  pcf.EditableConcurrentEmploymentLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on ListViewInput at LossDetailsDV.Wc7.pcf: line 703, column 41
    function def_refreshVariables_552 (def :  pcf.EditableOtherBenefitsLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on InputSetRef at LossDetailsDV.Wc7.pcf: line 102, column 42
    function def_refreshVariables_78 (def :  pcf.TDIC_CCAddressInputSet) : void {
      def.refreshVariables(tdic.cc.config.fnol.TDIC_WC7FNOLHelper.getClaimAddressOwner(Claim))
    }
    
    // 'value' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_118 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.claimant = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claimant_ContactProhibited_Input) at LossDetailsDV.Wc7.pcf: line 155, column 42
    function defaultSetter_131 (__VALUE_TO_SET :  java.lang.Object) : void {
      ContactProhibited = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CurrencyInput (id=EmploymentData_GrossWages_Input) at LossDetailsDV.Wc7.pcf: line 210, column 42
    function defaultSetter_170 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.GrossWages_TDIC = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_ClassCodeByLocation_Input) at LossDetailsDV.Wc7.pcf: line 216, column 25
    function defaultSetter_175 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimWorkComp.ClassCodeByLocation = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at LossDetailsDV.Wc7.pcf: line 227, column 42
    function defaultSetter_180 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.ClassCode = (__VALUE_TO_SET as entity.ClassCode)
    }
    
    // 'value' attribute on DateInput (id=EmploymentData_HireDate_Input) at LossDetailsDV.Wc7.pcf: line 241, column 42
    function defaultSetter_190 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.HireDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_HireState_Input) at LossDetailsDV.Wc7.pcf: line 249, column 42
    function defaultSetter_197 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.HireState = (__VALUE_TO_SET as typekey.State)
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at LossDetailsDV.Wc7.pcf: line 19, column 43
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_EmploymentStatus_Input) at LossDetailsDV.Wc7.pcf: line 256, column 42
    function defaultSetter_204 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.EmploymentStatus = (__VALUE_TO_SET as typekey.EmploymentStatusType)
    }
    
    // 'value' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_220 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.supervisor = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_NumDaysWorkedPerWeek_Input) at LossDetailsDV.Wc7.pcf: line 274, column 42
    function defaultSetter_233 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.NumDaysWorked = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_NumHoursWorkedPerDay_Input) at LossDetailsDV.Wc7.pcf: line 281, column 42
    function defaultSetter_239 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.NumHoursWorked = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_PayScheme_Input) at LossDetailsDV.Wc7.pcf: line 288, column 42
    function defaultSetter_245 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.PayScheme_TDIC = (__VALUE_TO_SET as typekey.PaySchemeType_TDIC)
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_PaidFullWages_Input) at LossDetailsDV.Wc7.pcf: line 294, column 42
    function defaultSetter_251 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.PaidFull = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=Claim_DateLastWorked_Input) at LossDetailsDV.Wc7.pcf: line 301, column 42
    function defaultSetter_257 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimInjuryIncident.DateLastWorked_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=Claim_DateReturnedToWork_Input) at LossDetailsDV.Wc7.pcf: line 308, column 42
    function defaultSetter_263 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimInjuryIncident.ReturnToWorkDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_WagePaymentContinued_Input) at LossDetailsDV.Wc7.pcf: line 314, column 42
    function defaultSetter_269 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.WagePaymentCont = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ModifiedDutyAvailable_Input) at LossDetailsDV.Wc7.pcf: line 320, column 42
    function defaultSetter_275 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ModifiedDutyAvail = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=ConcurrentEmployment_Input) at LossDetailsDV.Wc7.pcf: line 327, column 41
    function defaultSetter_281 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ConcurrentEmp = (__VALUE_TO_SET as typekey.YesNo)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_Severity_Input) at LossDetailsDV.Wc7.pcf: line 359, column 42
    function defaultSetter_293 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.getIncidentWithSeverityCodes().Severity = (__VALUE_TO_SET as typekey.SeverityType)
    }
    
    // 'value' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Wc7.pcf: line 53, column 42
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.CallType_TDIC = (__VALUE_TO_SET as String)
    }
    
    // 'value' attribute on TextInput (id=YearLastExposed_Input) at LossDetailsDV.Wc7.pcf: line 368, column 136
    function defaultSetter_301 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ensureClaimInjuryIncident().YearLastExposed_TDIC = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on DateInput (id=Claim_ExposureBegan_Input) at LossDetailsDV.Wc7.pcf: line 378, column 42
    function defaultSetter_309 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ExposureBegan = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=Claim_ExposureEnded_Input) at LossDetailsDV.Wc7.pcf: line 387, column 42
    function defaultSetter_316 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ExposureEnded = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_DeathReport_Input) at LossDetailsDV.Wc7.pcf: line 394, column 42
    function defaultSetter_322 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.DeathReport = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=DeathDate_Input) at LossDetailsDV.Wc7.pcf: line 400, column 72
    function defaultSetter_328 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.DeathDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_InjuryIllnessCause_Input) at LossDetailsDV.Wc7.pcf: line 408, column 42
    function defaultSetter_334 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.LossCause = (__VALUE_TO_SET as typekey.LossCause)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_AccidentType_Input) at LossDetailsDV.Wc7.pcf: line 415, column 42
    function defaultSetter_340 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.AccidentType = (__VALUE_TO_SET as typekey.AccidentType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_PrimaryInjury_Input) at LossDetailsDV.Wc7.pcf: line 423, column 42
    function defaultSetter_346 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ensureClaimInjuryIncident().GeneralInjuryType = (__VALUE_TO_SET as typekey.InjuryType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_DetailedInjury_Input) at LossDetailsDV.Wc7.pcf: line 431, column 42
    function defaultSetter_352 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ensureClaimInjuryIncident().DetailedInjuryType = (__VALUE_TO_SET as typekey.DetailedInjuryType)
    }
    
    // 'value' attribute on TextInput (id=InjuryDescription_Input) at LossDetailsDV.Wc7.pcf: line 437, column 42
    function defaultSetter_358 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ensureClaimInjuryIncident().Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_MedicalReport_Input) at LossDetailsDV.Wc7.pcf: line 451, column 42
    function defaultSetter_365 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.MedicalReport = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_TimeLossReport_Input) at LossDetailsDV.Wc7.pcf: line 459, column 42
    function defaultSetter_371 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.TimeLossReport = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_EmployerLiability_Input) at LossDetailsDV.Wc7.pcf: line 467, column 42
    function defaultSetter_377 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmployerLiability = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Wc7.pcf: line 476, column 42
    function defaultSetter_383 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Catastrophe = (__VALUE_TO_SET as entity.Catastrophe)
    }
    
    // 'value' attribute on DateInput (id=Claim_InjuryIllnessDate_Input) at LossDetailsDV.Wc7.pcf: line 491, column 41
    function defaultSetter_396 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.LossDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=Other_Input) at LossDetailsDV.Wc7.pcf: line 63, column 43
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Other_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EmploymentData_InjuryStartDate_Input) at LossDetailsDV.Wc7.pcf: line 510, column 42
    function defaultSetter_412 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentData.InjuryStartTime = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=Claim_DateReportedtoEmployer_Input) at LossDetailsDV.Wc7.pcf: line 528, column 42
    function defaultSetter_428 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.DateRptdToEmployer = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ClaimFormGiven_Input) at LossDetailsDV.Wc7.pcf: line 536, column 42
    function defaultSetter_435 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.DateFormGivenToEmp = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Notification_FirstNoticeSuit_Input) at LossDetailsDV.Wc7.pcf: line 554, column 42
    function defaultSetter_450 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.FirstNoticeSuit = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=DateOfNotice_Input) at LossDetailsDV.Wc7.pcf: line 71, column 43
    function defaultSetter_47 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ReportedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_472 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.reporter = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_ReportedByType_Input) at LossDetailsDV.Wc7.pcf: line 580, column 41
    function defaultSetter_485 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ReportedByType = (__VALUE_TO_SET as typekey.PersonRelationType)
    }
    
    // 'value' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_502 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.maincontact = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_EmploymentInjury_Input) at LossDetailsDV.Wc7.pcf: line 617, column 42
    function defaultSetter_517 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmploymentInjury = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjuredOnRegularJob_Input) at LossDetailsDV.Wc7.pcf: line 623, column 42
    function defaultSetter_523 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.InjuredRegularJob = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=EmployerQuestionsValidity_Input) at LossDetailsDV.Wc7.pcf: line 630, column 41
    function defaultSetter_529 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmpQusValidity = (__VALUE_TO_SET as typekey.YesNo)
    }
    
    // 'value' attribute on TextInput (id=EmployerValidityReason_Input) at LossDetailsDV.Wc7.pcf: line 639, column 77
    function defaultSetter_535 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EmployerValidityReason = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_HowReported_Input) at LossDetailsDV.Wc7.pcf: line 78, column 43
    function defaultSetter_54 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.HowReported = (__VALUE_TO_SET as typekey.HowReportedType)
    }
    
    // 'value' attribute on TextInput (id=Claim_ActivityPerformed_Input) at LossDetailsDV.Wc7.pcf: line 92, column 42
    function defaultSetter_66 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ActivityPerformed = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Claim_EquipmentUsed_Input) at LossDetailsDV.Wc7.pcf: line 99, column 42
    function defaultSetter_72 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.EquipmentUsed = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=LocationCode_Input) at LossDetailsDV.Wc7.pcf: line 110, column 41
    function defaultSetter_81 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.LocationCode = (__VALUE_TO_SET as entity.PolicyLocation)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InsuredPremises_Input) at LossDetailsDV.Wc7.pcf: line 119, column 42
    function defaultSetter_90 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.InsuredPremises = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=JurisdictionState_Input) at LossDetailsDV.Wc7.pcf: line 127, column 42
    function defaultSetter_96 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.JurisdictionState = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=Claimant_TaxNumber_Input) at LossDetailsDV.Wc7.pcf: line 184, column 41
    function encryptionExpression_158 (VALUE :  java.lang.String) : java.lang.String {
      return Claim.claimant.maskTaxId(VALUE)
    }
    
    // 'filter' attribute on TypeKeyInput (id=EmploymentData_HireState_Input) at LossDetailsDV.Wc7.pcf: line 249, column 42
    function filter_199 (VALUE :  typekey.State, VALUES :  typekey.State[]) : java.lang.Boolean {
      return VALUE.hasCategory(Country.TC_US)
    }
    
    // 'filter' attribute on TypeKeyInput (id=Claim_Severity_Input) at LossDetailsDV.Wc7.pcf: line 359, column 42
    function filter_295 (VALUE :  typekey.SeverityType, VALUES :  typekey.SeverityType[]) : java.lang.Boolean {
      return VALUE.hasCategory(LossType.TC_WC)
    }
    
    // 'filter' attribute on TypeKeyInput (id=Claim_ReportedByType_Input) at LossDetailsDV.Wc7.pcf: line 580, column 41
    function filter_487 (VALUE :  typekey.PersonRelationType, VALUES :  typekey.PersonRelationType[]) : java.lang.Boolean {
      return VALUE.hasCategory(Claim.Policy.PolicyType)
    }
    
    // 'filter' attribute on TypeKeyInput (id=JurisdictionState_Input) at LossDetailsDV.Wc7.pcf: line 127, column 42
    function filter_98 (VALUE :  typekey.Jurisdiction, VALUES :  typekey.Jurisdiction[]) : java.lang.Boolean {
      return VALUE.hasCategory(JurisdictionType.TC_INSURANCE)
    }
    
    // 'onChange' attribute on PostOnChange at LossDetailsDV.Wc7.pcf: line 493, column 74
    function onChange_392 () : void {
      gw.pcf.ClaimLossDetailsUtil.changedLossDate(Claim)
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_116 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.claimant); var result = eval("Claim.claimant = Claim.resolveContact(Claim.claimant) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_218 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.supervisor); var result = eval("Claim.supervisor = Claim.resolveContact(Claim.supervisor) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_470 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.reporter); var result = eval("Claim.reporter = Claim.resolveContact(Claim.reporter) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_500 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.maincontact); var result = eval("Claim.maincontact = Claim.resolveContact(Claim.maincontact) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'value' attribute on Reflect at LossDetailsDV.Wc7.pcf: line 163, column 44
    function reflectionValue_134 (TRIGGER_INDEX :  int, VALUE :  entity.Person) : java.lang.Object {
      return VALUE.PrimaryPhoneValue
    }
    
    // 'value' attribute on Reflect at LossDetailsDV.Wc7.pcf: line 172, column 53
    function reflectionValue_141 (TRIGGER_INDEX :  int, VALUE :  entity.Person) : java.lang.Object {
      return VALUE.PrimaryAddressDisplayValue
    }
    
    // 'value' attribute on Reflect at LossDetailsDV.Wc7.pcf: line 187, column 32
    function reflectionValue_153 (TRIGGER_INDEX :  int, VALUE :  entity.Person) : java.lang.Object {
      return VALUE.TaxID
    }
    
    // Reflect at LossDetailsDV.Wc7.pcf: line 582, column 42
    function reflectionValue_482 (TRIGGER_INDEX :  int, VALUE :  entity.Contact) : java.lang.Object {
      return (VALUE==Claim.Insured) ? ("self") : (true) ? ("") : "<NOCHANGE>"
    }
    
    // 'required' attribute on TextInput (id=Other_Input) at LossDetailsDV.Wc7.pcf: line 63, column 43
    function required_38 () : java.lang.Boolean {
      return Claim.CallType_TDIC == "Other"
    }
    
    // 'validationExpression' attribute on DateInput (id=EmploymentData_HireDate_Input) at LossDetailsDV.Wc7.pcf: line 241, column 42
    function validationExpression_187 () : java.lang.Object {
      return Claim.EmploymentData.HireDate == null || Claim.EmploymentData.HireDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'validationExpression' attribute on TextInput (id=YearLastExposed_Input) at LossDetailsDV.Wc7.pcf: line 368, column 136
    function validationExpression_298 () : java.lang.Object {
      return Claim.ensureClaimInjuryIncident().YearLastExposed_TDIC == null or Claim.ensureClaimInjuryIncident().YearLastExposed_TDIC <= Claim.LossDate.YearOfDate ? null : DisplayKey.get("TDIC.Validation.YearLastExposed.NotAfterLossDateYear")
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_ExposureBegan_Input) at LossDetailsDV.Wc7.pcf: line 378, column 42
    function validationExpression_306 () : java.lang.Object {
      return Claim.ExposureBegan == null || Claim.ExposureBegan < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_lossTime_Input) at LossDetailsDV.Wc7.pcf: line 502, column 42
    function validationExpression_401 () : java.lang.Object {
      return Claim.LossDate != null || Claim.LossDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'validationExpression' attribute on DateInput (id=EmploymentData_InjuryStartDate_Input) at LossDetailsDV.Wc7.pcf: line 510, column 42
    function validationExpression_409 () : java.lang.Object {
      return Claim.EmploymentData.InjuryStartTime == null || Claim.EmploymentData.InjuryStartTime < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_DateReportedtoEmployer_Input) at LossDetailsDV.Wc7.pcf: line 528, column 42
    function validationExpression_425 () : java.lang.Object {
      return Claim.DateRptdToEmployer != null and Claim.DateRptdToEmployer > gw.api.util.DateUtil.currentDate() ? DisplayKey.get("Java.Validation.Date.ForbidFuture") : null
    }
    
    // 'validationExpression' attribute on DateInput (id=DateOfNotice_Input) at LossDetailsDV.Wc7.pcf: line 71, column 43
    function validationExpression_44 () : java.lang.Object {
      return Claim.ReportedDate != null and Claim.ReportedDate > gw.api.util.DateUtil.currentDate() ? DisplayKey.get("Java.Validation.Date.ForbidFuture") : null
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_LossDate_Input) at LossDetailsDV.Wc7.pcf: line 27, column 43
    function validationExpression_6 () : java.lang.Object {
      return Claim.LossDate == null || Claim.LossDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_120 () : java.lang.Object {
      return Claim.RelatedPersonArray
    }
    
    // 'valueRange' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at LossDetailsDV.Wc7.pcf: line 227, column 42
    function valueRange_182 () : java.lang.Object {
      return FilteredClassCodes
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Wc7.pcf: line 53, column 42
    function valueRange_32 () : java.lang.Object {
      return Claim.getCallType(Claim)
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Wc7.pcf: line 476, column 42
    function valueRange_385 () : java.lang.Object {
      return gw.api.admin.CatastropheUtil.getCatastrophes()
    }
    
    // 'valueRange' attribute on RangeInput (id=LocationCode_Input) at LossDetailsDV.Wc7.pcf: line 110, column 41
    function valueRange_83 () : java.lang.Object {
      return Claim.PolicyProperties
    }
    
    // 'value' attribute on TextInput (id=JurisdictionClaimNumber_Input) at LossDetailsDV.Wc7.pcf: line 133, column 26
    function valueRoot_102 () : java.lang.Object {
      return Claim.ClaimWorkComp
    }
    
    // 'value' attribute on TextInput (id=Claimant_Phone_Input) at LossDetailsDV.Wc7.pcf: line 160, column 41
    function valueRoot_138 () : java.lang.Object {
      return Claim.claimant
    }
    
    // 'value' attribute on CurrencyInput (id=EmploymentData_GrossWages_Input) at LossDetailsDV.Wc7.pcf: line 210, column 42
    function valueRoot_171 () : java.lang.Object {
      return Claim.EmploymentData
    }
    
    // 'value' attribute on DateInput (id=Claim_DateLastWorked_Input) at LossDetailsDV.Wc7.pcf: line 301, column 42
    function valueRoot_258 () : java.lang.Object {
      return Claim.ClaimInjuryIncident
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_Severity_Input) at LossDetailsDV.Wc7.pcf: line 359, column 42
    function valueRoot_294 () : java.lang.Object {
      return Claim.getIncidentWithSeverityCodes()
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at LossDetailsDV.Wc7.pcf: line 19, column 43
    function valueRoot_3 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on TextInput (id=YearLastExposed_Input) at LossDetailsDV.Wc7.pcf: line 368, column 136
    function valueRoot_302 () : java.lang.Object {
      return Claim.ensureClaimInjuryIncident()
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at LossDetailsDV.Wc7.pcf: line 19, column 43
    function value_1 () : java.lang.String {
      return Claim.Description
    }
    
    // 'value' attribute on TextInput (id=JurisdictionClaimNumber_Input) at LossDetailsDV.Wc7.pcf: line 133, column 26
    function value_101 () : java.lang.String {
      return Claim.ClaimWorkComp.JurisdictionClaimNumber
    }
    
    // 'value' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_117 () : entity.Person {
      return Claim.claimant
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claimant_ContactProhibited_Input) at LossDetailsDV.Wc7.pcf: line 155, column 42
    function value_130 () : java.lang.Boolean {
      return ContactProhibited
    }
    
    // 'value' attribute on TextInput (id=Claimant_Phone_Input) at LossDetailsDV.Wc7.pcf: line 160, column 41
    function value_137 () : java.lang.String {
      return Claim.claimant.PrimaryPhoneValue
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossType_Input) at LossDetailsDV.Wc7.pcf: line 34, column 43
    function value_14 () : typekey.LossType {
      return Claim.LossType
    }
    
    // 'value' attribute on TextInput (id=Claimant_Address_Input) at LossDetailsDV.Wc7.pcf: line 169, column 41
    function value_144 () : java.lang.String {
      return Claim.claimant.PrimaryAddressDisplayValue
    }
    
    // 'value' attribute on DateInput (id=Claimant_DateOfBirth_Input) at LossDetailsDV.Wc7.pcf: line 178, column 41
    function value_149 () : java.util.Date {
      return Claim.claimant.DateOfBirth
    }
    
    // 'value' attribute on PrivacyInput (id=Claimant_TaxNumber_Input) at LossDetailsDV.Wc7.pcf: line 184, column 41
    function value_156 () : java.lang.String {
      return Claim.claimant.TaxID
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_Occupation_Input) at LossDetailsDV.Wc7.pcf: line 193, column 41
    function value_162 () : java.lang.String {
      return Claim.claimant.Occupation
    }
    
    // 'value' attribute on CurrencyInput (id=EmploymentData_GrossWages_Input) at LossDetailsDV.Wc7.pcf: line 210, column 42
    function value_169 () : gw.api.financials.CurrencyAmount {
      return Claim.EmploymentData.GrossWages_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_ClassCodeByLocation_Input) at LossDetailsDV.Wc7.pcf: line 216, column 25
    function value_174 () : java.lang.Boolean {
      return Claim.ClaimWorkComp.ClassCodeByLocation
    }
    
    // 'value' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at LossDetailsDV.Wc7.pcf: line 227, column 42
    function value_179 () : entity.ClassCode {
      return Claim.EmploymentData.ClassCode
    }
    
    // 'value' attribute on DateInput (id=EmploymentData_HireDate_Input) at LossDetailsDV.Wc7.pcf: line 241, column 42
    function value_189 () : java.util.Date {
      return Claim.EmploymentData.HireDate
    }
    
    // 'value' attribute on BooleanRadioInput (id=RiskManagementIncident_Input) at LossDetailsDV.Wc7.pcf: line 39, column 43
    function value_19 () : java.lang.Boolean {
      return Claim.RiskMgmtIncident_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_HireState_Input) at LossDetailsDV.Wc7.pcf: line 249, column 42
    function value_196 () : typekey.State {
      return Claim.EmploymentData.HireState
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_EmploymentStatus_Input) at LossDetailsDV.Wc7.pcf: line 256, column 42
    function value_203 () : typekey.EmploymentStatusType {
      return Claim.EmploymentData.EmploymentStatus
    }
    
    // 'value' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_219 () : entity.Person {
      return Claim.supervisor
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_NumDaysWorkedPerWeek_Input) at LossDetailsDV.Wc7.pcf: line 274, column 42
    function value_232 () : java.math.BigDecimal {
      return Claim.EmploymentData.NumDaysWorked
    }
    
    // 'value' attribute on TextInput (id=EmploymentData_NumHoursWorkedPerDay_Input) at LossDetailsDV.Wc7.pcf: line 281, column 42
    function value_238 () : java.math.BigDecimal {
      return Claim.EmploymentData.NumHoursWorked
    }
    
    // 'value' attribute on CheckBoxInput (id=Status_IncidentReport_Input) at LossDetailsDV.Wc7.pcf: line 44, column 43
    function value_24 () : java.lang.Boolean {
      return Claim.IncidentReport
    }
    
    // 'value' attribute on TypeKeyInput (id=EmploymentData_PayScheme_Input) at LossDetailsDV.Wc7.pcf: line 288, column 42
    function value_244 () : typekey.PaySchemeType_TDIC {
      return Claim.EmploymentData.PayScheme_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_PaidFullWages_Input) at LossDetailsDV.Wc7.pcf: line 294, column 42
    function value_250 () : java.lang.Boolean {
      return Claim.EmploymentData.PaidFull
    }
    
    // 'value' attribute on DateInput (id=Claim_DateLastWorked_Input) at LossDetailsDV.Wc7.pcf: line 301, column 42
    function value_256 () : java.util.Date {
      return Claim.ClaimInjuryIncident.DateLastWorked_TDIC
    }
    
    // 'value' attribute on DateInput (id=Claim_DateReturnedToWork_Input) at LossDetailsDV.Wc7.pcf: line 308, column 42
    function value_262 () : java.util.Date {
      return Claim.ClaimInjuryIncident.ReturnToWorkDate
    }
    
    // 'value' attribute on BooleanRadioInput (id=EmploymentData_WagePaymentContinued_Input) at LossDetailsDV.Wc7.pcf: line 314, column 42
    function value_268 () : java.lang.Boolean {
      return Claim.EmploymentData.WagePaymentCont
    }
    
    // 'value' attribute on BooleanRadioInput (id=ModifiedDutyAvailable_Input) at LossDetailsDV.Wc7.pcf: line 320, column 42
    function value_274 () : java.lang.Boolean {
      return Claim.ModifiedDutyAvail
    }
    
    // 'value' attribute on TypeKeyInput (id=ConcurrentEmployment_Input) at LossDetailsDV.Wc7.pcf: line 327, column 41
    function value_280 () : typekey.YesNo {
      return Claim.ConcurrentEmp
    }
    
    // 'value' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Wc7.pcf: line 53, column 42
    function value_29 () : String {
      return Claim.CallType_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_Severity_Input) at LossDetailsDV.Wc7.pcf: line 359, column 42
    function value_292 () : typekey.SeverityType {
      return Claim.getIncidentWithSeverityCodes().Severity
    }
    
    // 'value' attribute on TextInput (id=YearLastExposed_Input) at LossDetailsDV.Wc7.pcf: line 368, column 136
    function value_300 () : java.lang.Integer {
      return Claim.ensureClaimInjuryIncident().YearLastExposed_TDIC
    }
    
    // 'value' attribute on DateInput (id=Claim_ExposureBegan_Input) at LossDetailsDV.Wc7.pcf: line 378, column 42
    function value_308 () : java.util.Date {
      return Claim.ExposureBegan
    }
    
    // 'value' attribute on DateInput (id=Claim_ExposureEnded_Input) at LossDetailsDV.Wc7.pcf: line 387, column 42
    function value_315 () : java.util.Date {
      return Claim.ExposureEnded
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_DeathReport_Input) at LossDetailsDV.Wc7.pcf: line 394, column 42
    function value_321 () : java.lang.Boolean {
      return Claim.DeathReport
    }
    
    // 'value' attribute on DateInput (id=DeathDate_Input) at LossDetailsDV.Wc7.pcf: line 400, column 72
    function value_327 () : java.util.Date {
      return Claim.DeathDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_InjuryIllnessCause_Input) at LossDetailsDV.Wc7.pcf: line 408, column 42
    function value_333 () : typekey.LossCause {
      return Claim.LossCause
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_AccidentType_Input) at LossDetailsDV.Wc7.pcf: line 415, column 42
    function value_339 () : typekey.AccidentType {
      return Claim.AccidentType
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_PrimaryInjury_Input) at LossDetailsDV.Wc7.pcf: line 423, column 42
    function value_345 () : typekey.InjuryType {
      return Claim.ensureClaimInjuryIncident().GeneralInjuryType
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_DetailedInjury_Input) at LossDetailsDV.Wc7.pcf: line 431, column 42
    function value_351 () : typekey.DetailedInjuryType {
      return Claim.ensureClaimInjuryIncident().DetailedInjuryType
    }
    
    // 'value' attribute on TextInput (id=InjuryDescription_Input) at LossDetailsDV.Wc7.pcf: line 437, column 42
    function value_357 () : java.lang.String {
      return Claim.ensureClaimInjuryIncident().Description
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_MedicalReport_Input) at LossDetailsDV.Wc7.pcf: line 451, column 42
    function value_364 () : java.lang.Boolean {
      return Claim.MedicalReport
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_TimeLossReport_Input) at LossDetailsDV.Wc7.pcf: line 459, column 42
    function value_370 () : java.lang.Boolean {
      return Claim.TimeLossReport
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjurySeverity_EmployerLiability_Input) at LossDetailsDV.Wc7.pcf: line 467, column 42
    function value_376 () : java.lang.Boolean {
      return Claim.EmployerLiability
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Wc7.pcf: line 476, column 42
    function value_382 () : entity.Catastrophe {
      return Claim.Catastrophe
    }
    
    // 'value' attribute on TextInput (id=Other_Input) at LossDetailsDV.Wc7.pcf: line 63, column 43
    function value_39 () : java.lang.String {
      return Claim.Other_TDIC
    }
    
    // 'value' attribute on DateInput (id=EmploymentData_InjuryStartDate_Input) at LossDetailsDV.Wc7.pcf: line 510, column 42
    function value_411 () : java.util.Date {
      return Claim.EmploymentData.InjuryStartTime
    }
    
    // 'value' attribute on DateInput (id=Claim_DateReportedtoEmployer_Input) at LossDetailsDV.Wc7.pcf: line 528, column 42
    function value_427 () : java.util.Date {
      return Claim.DateRptdToEmployer
    }
    
    // 'value' attribute on DateInput (id=ClaimFormGiven_Input) at LossDetailsDV.Wc7.pcf: line 536, column 42
    function value_434 () : java.util.Date {
      return Claim.DateFormGivenToEmp
    }
    
    // 'value' attribute on BooleanRadioInput (id=Notification_FirstNoticeSuit_Input) at LossDetailsDV.Wc7.pcf: line 554, column 42
    function value_449 () : java.lang.Boolean {
      return Claim.FirstNoticeSuit
    }
    
    // 'value' attribute on DateInput (id=DateOfNotice_Input) at LossDetailsDV.Wc7.pcf: line 71, column 43
    function value_46 () : java.util.Date {
      return Claim.ReportedDate
    }
    
    // 'value' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_471 () : entity.Contact {
      return Claim.reporter
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_ReportedByType_Input) at LossDetailsDV.Wc7.pcf: line 580, column 41
    function value_484 () : typekey.PersonRelationType {
      return Claim.ReportedByType
    }
    
    // 'value' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_501 () : entity.Person {
      return Claim.maincontact
    }
    
    // 'value' attribute on BooleanRadioInput (id=Claim_EmploymentInjury_Input) at LossDetailsDV.Wc7.pcf: line 617, column 42
    function value_516 () : java.lang.Boolean {
      return Claim.EmploymentInjury
    }
    
    // 'value' attribute on BooleanRadioInput (id=InjuredOnRegularJob_Input) at LossDetailsDV.Wc7.pcf: line 623, column 42
    function value_522 () : java.lang.Boolean {
      return Claim.InjuredRegularJob
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=EmployerQuestionsValidity_Input) at LossDetailsDV.Wc7.pcf: line 630, column 41
    function value_528 () : typekey.YesNo {
      return Claim.EmpQusValidity
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_HowReported_Input) at LossDetailsDV.Wc7.pcf: line 78, column 43
    function value_53 () : typekey.HowReportedType {
      return Claim.HowReported
    }
    
    // 'value' attribute on TextInput (id=EmployerValidityReason_Input) at LossDetailsDV.Wc7.pcf: line 639, column 77
    function value_534 () : java.lang.String {
      return Claim.EmployerValidityReason
    }
    
    // 'value' attribute on TextInput (id=Claim_ActivityPerformed_Input) at LossDetailsDV.Wc7.pcf: line 92, column 42
    function value_65 () : java.lang.String {
      return Claim.ActivityPerformed
    }
    
    // 'value' attribute on TextInput (id=Claim_EquipmentUsed_Input) at LossDetailsDV.Wc7.pcf: line 99, column 42
    function value_71 () : java.lang.String {
      return Claim.EquipmentUsed
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at LossDetailsDV.Wc7.pcf: line 27, column 43
    function value_8 () : java.util.Date {
      return Claim.LossDate
    }
    
    // 'value' attribute on RangeInput (id=LocationCode_Input) at LossDetailsDV.Wc7.pcf: line 110, column 41
    function value_80 () : entity.PolicyLocation {
      return Claim.LocationCode
    }
    
    // 'value' attribute on BooleanRadioInput (id=InsuredPremises_Input) at LossDetailsDV.Wc7.pcf: line 119, column 42
    function value_89 () : java.lang.Boolean {
      return Claim.InsuredPremises
    }
    
    // 'value' attribute on TypeKeyInput (id=JurisdictionState_Input) at LossDetailsDV.Wc7.pcf: line 127, column 42
    function value_95 () : typekey.Jurisdiction {
      return Claim.JurisdictionState
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_121 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_121 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_121 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at LossDetailsDV.Wc7.pcf: line 227, column 42
    function verifyValueRangeIsAllowedType_183 ($$arg :  entity.ClassCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at LossDetailsDV.Wc7.pcf: line 227, column 42
    function verifyValueRangeIsAllowedType_183 ($$arg :  gw.api.database.IQueryBeanResult<entity.ClassCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at LossDetailsDV.Wc7.pcf: line 227, column 42
    function verifyValueRangeIsAllowedType_183 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_223 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_223 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_223 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Wc7.pcf: line 53, column 42
    function verifyValueRangeIsAllowedType_33 ($$arg :  String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Wc7.pcf: line 53, column 42
    function verifyValueRangeIsAllowedType_33 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Wc7.pcf: line 476, column 42
    function verifyValueRangeIsAllowedType_386 ($$arg :  entity.Catastrophe[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Wc7.pcf: line 476, column 42
    function verifyValueRangeIsAllowedType_386 ($$arg :  gw.api.database.IQueryBeanResult<entity.Catastrophe>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Wc7.pcf: line 476, column 42
    function verifyValueRangeIsAllowedType_386 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_475 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_475 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_475 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_505 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_505 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_505 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=LocationCode_Input) at LossDetailsDV.Wc7.pcf: line 110, column 41
    function verifyValueRangeIsAllowedType_84 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=LocationCode_Input) at LossDetailsDV.Wc7.pcf: line 110, column 41
    function verifyValueRangeIsAllowedType_84 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=LocationCode_Input) at LossDetailsDV.Wc7.pcf: line 110, column 41
    function verifyValueRangeIsAllowedType_84 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_122 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_121(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=EmploymentData_ClassCode_Input) at LossDetailsDV.Wc7.pcf: line 227, column 42
    function verifyValueRange_184 () : void {
      var __valueRangeArg = FilteredClassCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_183(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_224 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_223(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at LossDetailsDV.Wc7.pcf: line 53, column 42
    function verifyValueRange_34 () : void {
      var __valueRangeArg = Claim.getCallType(Claim)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_33(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at LossDetailsDV.Wc7.pcf: line 476, column 42
    function verifyValueRange_387 () : void {
      var __valueRangeArg = gw.api.admin.CatastropheUtil.getCatastrophes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_386(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_476 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_475(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_506 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_505(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=LocationCode_Input) at LossDetailsDV.Wc7.pcf: line 110, column 41
    function verifyValueRange_85 () : void {
      var __valueRangeArg = Claim.PolicyProperties
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_84(__valueRangeArg)
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Claimant_Picker_Input) at LossDetailsDV.Wc7.pcf: line 149, column 42
    function verifyValueType_128 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at LossDetailsDV.Wc7.pcf: line 267, column 42
    function verifyValueType_230 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=MainContact_Picker_Input) at LossDetailsDV.Wc7.pcf: line 600, column 42
    function verifyValueType_512 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'visible' attribute on TextAreaInput (id=Description_Input) at LossDetailsDV.Wc7.pcf: line 19, column 43
    function visible_0 () : java.lang.Boolean {
      return !Claim.Policy.Verified
    }
    
    // 'visible' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_106 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=Claimant_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_109 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.claimant), Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=EmploymentData_SupervisorPicker_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_211 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.supervisor), Claim, null as List<SpecialistService>)" != "" && false
    }
    
    // 'visible' attribute on ListViewInput at LossDetailsDV.Wc7.pcf: line 340, column 58
    function visible_285 () : java.lang.Boolean {
      return Claim.ConcurrentEmp == YesNo.TC_YES 
    }
    
    // 'visible' attribute on TextInput (id=YearLastExposed_Input) at LossDetailsDV.Wc7.pcf: line 368, column 136
    function visible_299 () : java.lang.Boolean {
      return Claim.ensureClaimInjuryIncident().GeneralInjuryType == typekey.InjuryType.TC_OCCUPATIONAL and Claim.Policy.Verified
    }
    
    // 'visible' attribute on DateInput (id=DeathDate_Input) at LossDetailsDV.Wc7.pcf: line 400, column 72
    function visible_326 () : java.lang.Boolean {
      return Claim.DeathReport == true and Claim.Policy.Verified
    }
    
    // 'visible' attribute on ClaimContactInput (id=ReportedBy_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_463 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.reporter), Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=MainContact_Picker_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_493 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on TextInput (id=EmployerValidityReason_Input) at LossDetailsDV.Wc7.pcf: line 639, column 77
    function visible_533 () : java.lang.Boolean {
      return Claim.EmpQusValidity == TC_YES and Claim.Policy.Verified
    }
    
    // 'visible' attribute on TextAreaInput (id=OccurrenceDescription_Input) at LossDetailsDV.Wc7.pcf: line 86, column 42
    function visible_58 () : java.lang.Boolean {
      return Claim.Policy.Verified
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get ContactProhibited() : boolean {
      return Claim.getClaimContact(Claim.claimant).ContactProhibited
    }
    property set ContactProhibited(prohibited : boolean) {
      var claimContact = Claim.getClaimContact(Claim.claimant)
      if (claimContact != null) claimContact.ContactProhibited = prohibited
    }
    
    property get FilteredClassCodes() : ClassCode[] {
      if(Claim.ClaimWorkComp.ClassCodeByLocation) {
        return Claim.Policy.ClassCodes.where( \ code -> Claim.LocationCode.LocationBasedRisks*.ClassCode.contains(code) )
      }
      else {
        return Claim.Policy.ClassCodes
      }
    }
    
    
  }
  
  
}