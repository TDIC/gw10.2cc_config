package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/audit_ext/AdminAudit.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminAuditExpressions {
  @javax.annotation.Generated("config/web/pcf/audit_ext/AdminAudit.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminAuditExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=searchButton) at AdminAudit.pcf: line 31, column 96
    function action_1 () : void {
      adminAuditSearchResults = gw.acc.adminaudit.util.AdminAuditSearchUtil.executeSearch(searchCriteria)
    }
    
    // 'action' attribute on ToolbarButton (id=ToolbarButton) at AdminAudit.pcf: line 35, column 95
    function action_2 () : void {
      clearAll()
    }
    
    // 'afterEnter' attribute on Page (id=AdminAudit) at AdminAudit.pcf: line 13, column 86
    function afterEnter_8 () : void {
      clearAll()
    }
    
    // 'available' attribute on PanelRef at AdminAudit.pcf: line 40, column 25
    function available_3 () : java.lang.Boolean {
      return searchCriteria.StartDate != null || searchCriteria.EndDate != null || searchCriteria.EntityName != "" || searchCriteria.ModifiedBy != ""
    }
    
    // 'canVisit' attribute on Page (id=AdminAudit) at AdminAudit.pcf: line 13, column 86
    static function canVisit_9 () : java.lang.Boolean {
      return perm.System.viewAdminHistory_ext
    }
    
    // 'def' attribute on PanelRef at AdminAudit.pcf: line 40, column 25
    function def_onEnter_4 (def :  pcf.AdminAuditSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at AdminAudit.pcf: line 45, column 60
    function def_onEnter_6 (def :  pcf.AdminAuditEntriesLV) : void {
      def.onEnter(adminAuditSearchResults)
    }
    
    // 'def' attribute on PanelRef at AdminAudit.pcf: line 40, column 25
    function def_refreshVariables_5 (def :  pcf.AdminAuditSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at AdminAudit.pcf: line 45, column 60
    function def_refreshVariables_7 (def :  pcf.AdminAuditEntriesLV) : void {
      def.refreshVariables(adminAuditSearchResults)
    }
    
    // 'initialValue' attribute on Variable at AdminAudit.pcf: line 23, column 63
    function initialValue_0 () : gw.acc.adminaudit.util.AdminAuditSearchCriteria {
      return new gw.acc.adminaudit.util.AdminAuditSearchCriteria()
    }
    
    // Page (id=AdminAudit) at AdminAudit.pcf: line 13, column 86
    static function parent_10 () : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    property get AuditEntries () : gw.api.database.IQueryBeanResult<AdminAudit_Ext> {
      return getVariableValue("AuditEntries", 0) as gw.api.database.IQueryBeanResult<AdminAudit_Ext>
    }
    
    property set AuditEntries ($arg :  gw.api.database.IQueryBeanResult<AdminAudit_Ext>) {
      setVariableValue("AuditEntries", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.AdminAudit {
      return super.CurrentLocation as pcf.AdminAudit
    }
    
    property get adminAuditSearchResults () : gw.api.database.IQueryBeanResult<AdminAudit_Ext> {
      return getVariableValue("adminAuditSearchResults", 0) as gw.api.database.IQueryBeanResult<AdminAudit_Ext>
    }
    
    property set adminAuditSearchResults ($arg :  gw.api.database.IQueryBeanResult<AdminAudit_Ext>) {
      setVariableValue("adminAuditSearchResults", 0, $arg)
    }
    
    property get searchCriteria () : gw.acc.adminaudit.util.AdminAuditSearchCriteria {
      return getVariableValue("searchCriteria", 0) as gw.acc.adminaudit.util.AdminAuditSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.acc.adminaudit.util.AdminAuditSearchCriteria) {
      setVariableValue("searchCriteria", 0, $arg)
    }
    
    function clearAll() : void {
          searchCriteria = new gw.acc.adminaudit.util.AdminAuditSearchCriteria();
          //adminAuditSearchResults = gwservices.adminaudit.util.AdminAuditSearchUtil.executeSearch(searchCriteria);
          adminAuditSearchResults = null;
        }
    
    
  }
  
  
}