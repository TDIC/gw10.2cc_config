package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.cc.claim.NewClaimPolicyDescription
@javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizardFindPolicyPanelSet.Create.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FNOLWizardFindPolicyPanelSet_CreateExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizardFindPolicyPanelSet.Create.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FNOLWizardFindPolicyPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 170, column 81
    function def_onEnter_100 (def :  pcf.NewClaimEndorsementsLV) : void {
      def.onEnter(Claim.Policy.Endorsements, Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 186, column 75
    function def_onEnter_104 (def :  pcf.NewClaimStatCodesLV) : void {
      def.onEnter(Claim.Policy.StatCodes, Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_onEnter_64 (def :  pcf.NewClaimPolicyGeneralPanelSet_Auto) : void {
      def.onEnter(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_onEnter_66 (def :  pcf.NewClaimPolicyGeneralPanelSet_Gl) : void {
      def.onEnter(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_onEnter_68 (def :  pcf.NewClaimPolicyGeneralPanelSet_Pr) : void {
      def.onEnter(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_onEnter_70 (def :  pcf.NewClaimPolicyGeneralPanelSet_Trav) : void {
      def.onEnter(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_onEnter_72 (def :  pcf.NewClaimPolicyGeneralPanelSet_Wc) : void {
      def.onEnter(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_onEnter_74 (def :  pcf.NewClaimPolicyGeneralPanelSet_Wc7) : void {
      def.onEnter(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 116, column 36
    function def_onEnter_77 (def :  pcf.OptionalNoteCV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 124, column 43
    function def_onEnter_82 (def :  pcf.PolicyLocationLDV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 138, column 73
    function def_onEnter_89 (def :  pcf.NewClaimVehiclesLV) : void {
      def.onEnter(Claim.Policy.Vehicles, Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 154, column 47
    function def_onEnter_95 (def :  pcf.PolicyTripLV) : void {
      def.onEnter( Claim, Wizard )
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 170, column 81
    function def_refreshVariables_101 (def :  pcf.NewClaimEndorsementsLV) : void {
      def.refreshVariables(Claim.Policy.Endorsements, Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 186, column 75
    function def_refreshVariables_105 (def :  pcf.NewClaimStatCodesLV) : void {
      def.refreshVariables(Claim.Policy.StatCodes, Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_refreshVariables_65 (def :  pcf.NewClaimPolicyGeneralPanelSet_Auto) : void {
      def.refreshVariables(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_refreshVariables_67 (def :  pcf.NewClaimPolicyGeneralPanelSet_Gl) : void {
      def.refreshVariables(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_refreshVariables_69 (def :  pcf.NewClaimPolicyGeneralPanelSet_Pr) : void {
      def.refreshVariables(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_refreshVariables_71 (def :  pcf.NewClaimPolicyGeneralPanelSet_Trav) : void {
      def.refreshVariables(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_refreshVariables_73 (def :  pcf.NewClaimPolicyGeneralPanelSet_Wc) : void {
      def.refreshVariables(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function def_refreshVariables_75 (def :  pcf.NewClaimPolicyGeneralPanelSet_Wc7) : void {
      def.refreshVariables(Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 116, column 36
    function def_refreshVariables_78 (def :  pcf.OptionalNoteCV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 124, column 43
    function def_refreshVariables_83 (def :  pcf.PolicyLocationLDV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 138, column 73
    function def_refreshVariables_90 (def :  pcf.NewClaimVehiclesLV) : void {
      def.refreshVariables(Claim.Policy.Vehicles, Claim.Policy)
    }
    
    // 'def' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 154, column 47
    function def_refreshVariables_96 (def :  pcf.PolicyTripLV) : void {
      def.refreshVariables( Claim, Wizard )
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 49, column 42
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicy.UnverifiedPolicyType = (__VALUE_TO_SET as typekey.PolicyType)
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimLossType_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 62, column 46
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Type_TDIC = (__VALUE_TO_SET as typekey.ClaimType_TDIC)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 72, column 52
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicy.UnverifiedPolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=ClaimSpecialityCode_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 84, column 95
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.SpecialityCode_TDIC = (__VALUE_TO_SET as typekey.SpecialityCode_TDIC)
    }
    
    // 'value' attribute on RangeInput (id=ClaimState_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 92, column 45
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.JurisdictionState = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 100, column 43
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      claimLossDate.LossDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=Claim_lossTime_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 107, column 43
    function defaultSetter_59 (__VALUE_TO_SET :  java.lang.Object) : void {
      claimLossDate.LossTime = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber2_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 39, column 58
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ClaimNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on RangeInput (id=ClaimSpecialityCode_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 84, column 95
    function editable_31 () : java.lang.Boolean {
      return true 
    }
    
    // 'editable' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function editable_63 () : java.lang.Boolean {
      return !Claim.Policy.Verified
    }
    
    // 'filter' attribute on TypeKeyInput (id=Type_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 49, column 42
    function filter_17 (VALUE :  typekey.PolicyType, VALUES :  typekey.PolicyType[]) : java.lang.Object {
      return PolicyType.TF_SEARCHPOLICYTYPE_TDIC.getTypeKeys()
    }
    
    // 'filter' attribute on TypeKeyInput (id=ClaimLossType_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 62, column 46
    function filter_24 (VALUE :  typekey.ClaimType_TDIC, VALUES :  typekey.ClaimType_TDIC[]) : java.lang.Boolean {
      return  filterClaimTypes(VALUE)
    }
    
    // 'initialValue' attribute on Variable at FNOLWizardFindPolicyPanelSet.Create.pcf: line 11, column 53
    function initialValue_0 () : gw.cc.claim.NewClaimPolicyDescription {
      return initNewPolicy()
    }
    
    // 'initialValue' attribute on Variable at FNOLWizardFindPolicyPanelSet.Create.pcf: line 21, column 29
    function initialValue_1 () : java.util.Set {
      return Claim.Policy != null ? gw.api.policy.PolicyTabUtil.getTabSet(Claim.Policy) : null
    }
    
    // 'initialValue' attribute on Variable at FNOLWizardFindPolicyPanelSet.Create.pcf: line 25, column 43
    function initialValue_2 () : gw.api.claim.NewClaimMode[] {
      return initClaimModeArray()
    }
    
    // 'initialValue' attribute on Variable at FNOLWizardFindPolicyPanelSet.Create.pcf: line 29, column 47
    function initialValue_3 () : gw.api.claim.ClaimLossDateProxy {
      return new gw.api.claim.ClaimLossDateProxy(Claim)
    }
    
    // 'mode' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 114, column 78
    function mode_76 () : java.lang.Object {
      return gw.config.LOBAbstraction.ForClaim.ForLossType.getUIMode(Claim)
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizardFindPolicyPanelSet.Create.pcf: line 51, column 94
    function onChange_12 () : void {
      Claim.Type_TDIC = null; setPolicy(CurrentLocation as pcf.api.Wizard)
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizardFindPolicyPanelSet.Create.pcf: line 64, column 101
    function onChange_19 () : void {
      setPolicy(CurrentLocation as pcf.api.Wizard);setClaimFirstAndFinalStatus();
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizardFindPolicyPanelSet.Create.pcf: line 74, column 71
    function onChange_26 () : void {
      setPolicy(CurrentLocation as pcf.api.Wizard);
    }
    
    // 'showConfirmMessage' attribute on TypeKeyInput (id=Type_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 49, column 42
    function showConfirmMessage_13 () : java.lang.Boolean {
      return Claim.Policy.PolicyType != null
    }
    
    // 'showConfirmMessage' attribute on TypeKeyInput (id=ClaimLossType_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 62, column 46
    function showConfirmMessage_20 () : java.lang.Boolean {
      return Claim.Policy.PolicyType != null and Claim.LossType != Wizard.LossType 
    }
    
    // 'title' attribute on Card (id=PolicyLocationsCard) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 121, column 217
    function title_85 () : java.lang.String {
      return !Claim.Policy.Verified or (Claim.Policy.TotalProperties == Claim.Policy.Properties.length) ? DisplayKey.get("JSP.NewClaimPolicyDetails.Policy.Locations") as String : DisplayKey.get("JSP.NewClaimPolicyDetails.Policy.Locations.PartialList", Claim.Policy.TotalProperties)
    }
    
    // 'title' attribute on TitleBar at FNOLWizardFindPolicyPanelSet.Create.pcf: line 140, column 306
    function title_86 () : java.lang.String {
      return  (!Claim.Policy.Verified or (Claim.Policy.TotalVehicles == Claim.Policy.Vehicles.length)) ? DisplayKey.get("JSP.NewClaimPolicyDetails.Policy.Vehicles") as String : DisplayKey.get("JSP.NewClaimPolicyDetails.Policy.Vehicles.PartialList", Claim.Policy.TotalVehicles)
    }
    
    // 'title' attribute on Card (id=VehiclesCard) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 136, column 164
    function title_92 () : java.lang.String {
      return (!Claim.Policy.Verified or (Claim.Policy.TotalVehicles == Claim.Policy.Vehicles.length)) ? DisplayKey.get("JSP.NewClaimPolicyDetails.Policy.Vehicles") as String : DisplayKey.get("JSP.NewClaimPolicyDetails.Policy.Vehicles.PartialList", Claim.Policy.TotalVehicles)
    }
    
    // 'validationExpression' attribute on TextInput (id=ClaimNumber2_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 39, column 58
    function validationExpression_4 () : java.lang.Object {
      return !Claim.DuplicateClaimNumber ? null : DisplayKey.get("Java.NewClaimWizard.Error.ClaimNumberNotUnique", Claim.ClaimNumber)
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 100, column 43
    function validationExpression_51 () : java.lang.Object {
      return Claim.validateLossDate()
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_lossTime_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 107, column 43
    function validationExpression_57 () : java.lang.Object {
      return Claim.LossDate != null || Claim.LossDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimSpecialityCode_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 84, column 95
    function valueRange_36 () : java.lang.Object {
      return SpecialityCode_TDIC.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimState_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 92, column 45
    function valueRange_46 () : java.lang.Object {
      return Jurisdiction.TF_RISKINCIDENTVALIDSTATES.TypeKeys
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 49, column 42
    function valueRoot_16 () : java.lang.Object {
      return newPolicy
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 100, column 43
    function valueRoot_54 () : java.lang.Object {
      return claimLossDate
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber2_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 39, column 58
    function valueRoot_8 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 49, column 42
    function value_14 () : typekey.PolicyType {
      return newPolicy.UnverifiedPolicyType
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimLossType_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 62, column 46
    function value_21 () : typekey.ClaimType_TDIC {
      return Claim.Type_TDIC
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 72, column 52
    function value_27 () : java.lang.String {
      return newPolicy.UnverifiedPolicyNumber
    }
    
    // 'value' attribute on RangeInput (id=ClaimSpecialityCode_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 84, column 95
    function value_33 () : typekey.SpecialityCode_TDIC {
      return Claim.SpecialityCode_TDIC
    }
    
    // 'value' attribute on RangeInput (id=ClaimState_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 92, column 45
    function value_43 () : typekey.Jurisdiction {
      return Claim.JurisdictionState
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 100, column 43
    function value_52 () : java.util.Date {
      return claimLossDate.LossDate
    }
    
    // 'value' attribute on DateInput (id=Claim_lossTime_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 107, column 43
    function value_58 () : java.util.Date {
      return claimLossDate.LossTime
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber2_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 39, column 58
    function value_6 () : java.lang.String {
      return Claim.ClaimNumber
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimSpecialityCode_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 84, column 95
    function verifyValueRangeIsAllowedType_37 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimSpecialityCode_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 84, column 95
    function verifyValueRangeIsAllowedType_37 ($$arg :  typekey.SpecialityCode_TDIC[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimState_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 92, column 45
    function verifyValueRangeIsAllowedType_47 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimState_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 92, column 45
    function verifyValueRangeIsAllowedType_47 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimSpecialityCode_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 84, column 95
    function verifyValueRange_38 () : void {
      var __valueRangeArg = SpecialityCode_TDIC.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_37(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=ClaimState_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 92, column 45
    function verifyValueRange_48 () : void {
      var __valueRangeArg = Jurisdiction.TF_RISKINCIDENTVALIDSTATES.TypeKeys
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_47(__valueRangeArg)
    }
    
    // 'visible' attribute on Card (id=StatCodeCard) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 184, column 165
    function visible_106 () : java.lang.Boolean {
      return policyTabSet != null && policyTabSet.contains("Statcodes") && allRequiredFieldsExist() && Claim.Policy.Verified
    }
    
    // 'visible' attribute on RangeInput (id=ClaimSpecialityCode_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 84, column 95
    function visible_32 () : java.lang.Boolean {
      return (newPolicy.UnverifiedPolicyType== PolicyType.TC_GENERALLIABILITY)
    }
    
    // 'visible' attribute on TextInput (id=ClaimNumber2_Input) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 39, column 58
    function visible_5 () : java.lang.Boolean {
      return !Claim.ClaimNumberGenerationEnabled
    }
    
    // 'addVisible' attribute on IteratorButtons at FNOLWizardFindPolicyPanelSet.Create.pcf: line 129, column 84
    function visible_79 () : java.lang.Boolean {
      return  !Claim.Policy.Verified and perm.Policy.edit(Claim)
    }
    
    // 'visible' attribute on PanelRef at FNOLWizardFindPolicyPanelSet.Create.pcf: line 124, column 43
    function visible_81 () : java.lang.Boolean {
      return Claim.Policy.Verified
    }
    
    // 'visible' attribute on Card (id=PolicyLocationsCard) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 121, column 217
    function visible_84 () : java.lang.Boolean {
      return policyTabSet != null && (policyTabSet.contains("Properties") or policyTabSet.contains("Classcodes")) && allRequiredFieldsExist() && Claim.Policy.Verified
    }
    
    // 'removeVisible' attribute on IteratorButtons at FNOLWizardFindPolicyPanelSet.Create.pcf: line 145, column 56
    function visible_88 () : java.lang.Boolean {
      return perm.Policy.edit(Claim)
    }
    
    // 'visible' attribute on Card (id=VehiclesCard) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 136, column 164
    function visible_91 () : java.lang.Boolean {
      return policyTabSet != null && policyTabSet.contains("Vehicles") && allRequiredFieldsExist() && Claim.Policy.Verified
    }
    
    // 'visible' attribute on Card (id=TripsCard) at FNOLWizardFindPolicyPanelSet.Create.pcf: line 152, column 161
    function visible_97 () : java.lang.Boolean {
      return policyTabSet != null && policyTabSet.contains("Trips") && allRequiredFieldsExist() && Claim.Policy.Verified
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get Wizard () : gw.api.claim.NewClaimWizardInfo {
      return getRequireValue("Wizard", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set Wizard ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setRequireValue("Wizard", 0, $arg)
    }
    
    property get claimLossDate () : gw.api.claim.ClaimLossDateProxy {
      return getVariableValue("claimLossDate", 0) as gw.api.claim.ClaimLossDateProxy
    }
    
    property set claimLossDate ($arg :  gw.api.claim.ClaimLossDateProxy) {
      setVariableValue("claimLossDate", 0, $arg)
    }
    
    property get claimModeArray () : gw.api.claim.NewClaimMode[] {
      return getVariableValue("claimModeArray", 0) as gw.api.claim.NewClaimMode[]
    }
    
    property set claimModeArray ($arg :  gw.api.claim.NewClaimMode[]) {
      setVariableValue("claimModeArray", 0, $arg)
    }
    
    property get newPolicy () : gw.cc.claim.NewClaimPolicyDescription {
      return getVariableValue("newPolicy", 0) as gw.cc.claim.NewClaimPolicyDescription
    }
    
    property set newPolicy ($arg :  gw.cc.claim.NewClaimPolicyDescription) {
      setVariableValue("newPolicy", 0, $arg)
    }
    
    property get policyTabSet () : java.util.Set {
      return getVariableValue("policyTabSet", 0) as java.util.Set
    }
    
    property set policyTabSet ($arg :  java.util.Set) {
      setVariableValue("policyTabSet", 0, $arg)
    }
    
    
        function filterClaimTypes(claimType : ClaimType_TDIC): Boolean {
          if(newPolicy.UnverifiedPolicyType == null) return true
         var claimTypeList = ClaimType_TDIC.getTypeKeys().where(\elt1 -> elt1.hasCategory(newPolicy.UnverifiedPolicyType))
          return claimTypeList.hasMatch(\elt1 -> elt1== claimType)
        }
        
    function initNewPolicy() : NewClaimPolicyDescription {
      Claim.RiskMgmtIncident_TDIC = true
      if (Wizard.PolicyDescription.UnverifiedPolicyType != null) {
        return Wizard.PolicyDescription
      }
      else {
        var policyDescription = Wizard.PolicyDescription
        var user: User = User.util.getCurrentUser()
        policyDescription.UnverifiedPolicyType = user.PolicyType
        return policyDescription
      }
    }
    
    function setClaimFirstAndFinalStatus() {  
      Claim.FirstAndFinal = ("AutoFirstAndFinal" == Wizard.ClaimMode.QuickClaimMode.Mode)
    }
    
    
    function getSelectedClaimMode(modeArray : gw.api.claim.NewClaimMode[]) : gw.api.claim.NewClaimMode {
      var result : gw.api.claim.NewClaimMode = null
      if ((modeArray != null) && (modeArray.length > 0)){
        result = modeArray[0]
        if (Wizard.ClaimMode != null){
          foreach (indivMode in modeArray){
            if (Wizard.ClaimMode == indivMode){
              result = Wizard.ClaimMode  
            }
          }      
        }
      }  
      return result
    }
    
    function initClaimModeArray() : gw.api.claim.NewClaimMode[] {
      var modeArray = Wizard.getAvailableClaimModes(newPolicy.UnverifiedPolicyType)
      var user: User = User.util.getCurrentUser()
      if ((user.LossType != null) && (Wizard.LossType == null)) {
        Wizard.setClaimMode(user.LossType, user.QuickClaim as String)
        if (newPolicy.UnverifiedPolicyType == null){
          var availablePolicyTypes = Wizard.getAvailablePolicyTypes()
          if ((availablePolicyTypes != null) && (availablePolicyTypes.length > 0)) {
            newPolicy.UnverifiedPolicyType = availablePolicyTypes[0]
            modeArray = Wizard.getAvailableClaimModes(newPolicy.UnverifiedPolicyType)
          }
          setPolicy(CurrentLocation as pcf.api.Wizard)
        }
      } else {
        Wizard.ClaimMode = getSelectedClaimMode(modeArray)
      }
      return modeArray
    }
    
    function allRequiredFieldsExist() : boolean {
      return newPolicy.UnverifiedPolicyNumber != null && newPolicy.UnverifiedPolicyType != null && Wizard.getLossType() != null
    }
    
    function hasLossTypeChanged() : boolean {
      return Claim.LossType != null and Wizard.LossType != Claim.LossType
    }
    
    function hasPolicyTypeChanged() : boolean {
      return newPolicy.UnverifiedPolicyType != null and newPolicy.UnverifiedPolicyType != Claim.Policy.PolicyType
    }
    
    function setPolicy(location : pcf.api.Wizard) {
      var policyChanged = false
      if (Claim.Policy != null and newPolicy.UnverifiedPolicyNumber != Claim.Policy.PolicyNumber) {
        Claim.Policy.PolicyNumber = newPolicy.UnverifiedPolicyNumber
      }
      
      if(Claim.Policy.PolicyType != null and newPolicy.UnverifiedPolicyType != Claim.Policy.PolicyType){
        Claim.CallType_TDIC = null
        Claim.Other_TDIC = null
      }
      
      if (hasPolicyTypeChanged()) {
        claimModeArray = initClaimModeArray()
        if (Claim.Policy != null) {
            Claim.Policy.removeAllRoles()
        }
        //If we're changing claim types, we're nuking all the claim contacts.
        policyChanged = true
      }     
      
        
      if (allRequiredFieldsExist()) {
        /*if (hasLossTypeChanged() or policyChanged) {
          //if the claim loss type has changed, then must reset the wizard by reentering the wizard.
          var lossDate = Claim.LossDate
          var policyType = newPolicy.UnverifiedPolicyType
          var policyNumber = newPolicy.UnverifiedPolicyNumber
          location.cancel()
          pcf.FNOLWizard.go(policyType, policyNumber, Wizard.ClaimMode, lossDate, Wizard.PageMode)
        }*/
        
        //when claimMode is changed, the losstype is not changed on claim yet, so must set the claim loss type before setting policy
        if (Claim.LossType == null or Wizard.LossType != Claim.LossType) {
          Claim.LossType = Wizard.LossType
          policyChanged = true
        }
        
        if (policyChanged) {
          newPolicy.setPolicyVerified(false)
          Wizard.PolicyDescription = newPolicy
          Wizard.setPolicy()
          if (Claim.Policy != null) {
            Claim.Policy.removeAllRoles()
          }
          policyTabSet = gw.api.policy.PolicyTabUtil.getTabSet(Claim.Policy)
        }
      }
    }
    
    
  }
  
  
}