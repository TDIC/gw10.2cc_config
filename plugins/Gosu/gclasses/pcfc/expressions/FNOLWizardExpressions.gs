package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FNOLWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FNOLWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    static function __constructorIndex (Claim :  Claim) : int {
      return 3
    }
    
    static function __constructorIndex (policyType :  PolicyType, policyNumber :  String, claimMode :  gw.api.claim.NewClaimMode, LossDate :  java.util.Date, PageMode :  boolean) : int {
      return 2
    }
    
    static function __constructorIndex (PageMode :  boolean) : int {
      return 1
    }
    
    static function __constructorIndex (searchCriteriaAndSummary :  gw.api.claim.NewClaimPolicySearchCriteriaAndSummary, claimMode :  gw.api.claim.NewClaimMode, LossDate :  java.util.Date, PageMode :  boolean) : int {
      return 4
    }
    
    // 'afterCancel' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    function afterCancel_121 () : void {
      ClaimCenterStartupPage.go()
    }
    
    // 'afterCancel' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    function afterCancel_dest_122 () : pcf.api.Destination {
      return pcf.ClaimCenterStartupPage.createDestination()
    }
    
    // 'afterFinish' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    function afterFinish_132 () : void {
      wizardInfo.afterFinish()
    }
    
    // 'allowFinish' attribute on WizardStep (id=ClassicDocuments) at FNOLWizard.pcf: line 125, column 152
    function allowFinish_40 () : java.lang.Boolean {
      return !gw.plugin.Plugins.isEnabled(gw.plugin.document.IDocumentMetadataSource)
    }
    
    // 'allowNext' attribute on WizardStep (id=FindPolicy) at FNOLWizard.pcf: line 69, column 49
    function allowNext_4 () : java.lang.Boolean {
      return wizardInfo.IsLossTypeSet
    }
    
    // 'available' attribute on WizardStepSet (id=GeneralPropertyWizardStepSet) at FNOLWizard.pcf: line 84, column 47
    function available_50 () : java.lang.Boolean {
      return wizardInfo.useOldWizardStep()
    }
    
    // 'beforeCommit' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    function beforeCommit_123 (pickedValue :  java.lang.Object) : void {
      wizardInfo.onBeforeCommit()
    }
    
    // 'canVisit' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    static function canVisit_124 (Claim :  Claim, LossDate :  java.util.Date, PageMode :  boolean, claimMode :  gw.api.claim.NewClaimMode, policyNumber :  String, policyType :  PolicyType, searchCriteriaAndSummary :  gw.api.claim.NewClaimPolicySearchCriteriaAndSummary) : java.lang.Boolean {
      return perm.Claim.create
    }
    
    // 'finishConfirmation' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    function confirmMessage_120 () : java.lang.String {
      return wizardInfo.getFinishConfirmation()
    }
    
    // 'handlesValidationIssue' attribute on WizardStep (id=PolicyGeneral) at FNOLWizard.pcf: line 262, column 73
    function handlesValidationIssue_104 (VALUE :  java.lang.Object, ISSUE :  gw.api.validation.ValidationIssue) : java.lang.Boolean {
      return VALUE typeis entity.Policy
    }
    
    // 'handlesValidationIssue' attribute on WizardStep (id=ClassicMainContacts) at FNOLWizard.pcf: line 92, column 158
    function handlesValidationIssue_17 (VALUE :  java.lang.Object, ISSUE :  gw.api.validation.ValidationIssue) : java.lang.Boolean {
      return wizardInfo.isMainContactOrReporter(VALUE)
    }
    
    // 'handlesValidationIssue' attribute on WizardStep (id=ClassicLossDetails) at FNOLWizard.pcf: line 103, column 323
    function handlesValidationIssue_23 (VALUE :  java.lang.Object, ISSUE :  gw.api.validation.ValidationIssue) : java.lang.Boolean {
      return VALUE typeis entity.Claim or VALUE typeis entity.Incident
    }
    
    // 'handlesValidationIssue' attribute on WizardStep (id=ClassicExposures) at FNOLWizard.pcf: line 112, column 54
    function handlesValidationIssue_31 (VALUE :  java.lang.Object, ISSUE :  gw.api.validation.ValidationIssue) : java.lang.Boolean {
      return VALUE typeis entity.Exposure
    }
    
    // 'handlesValidationIssue' attribute on WizardStep (id=ClassicPartiesInvolved) at FNOLWizard.pcf: line 119, column 158
    function handlesValidationIssue_36 (VALUE :  java.lang.Object, ISSUE :  gw.api.validation.ValidationIssue) : java.lang.Boolean {
      return !wizardInfo.isMainContactOrReporter(VALUE)
    }
    
    // 'infoBar' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    function infoBar_onEnter_125 (def :  pcf.ClaimInfoBar) : void {
      def.onEnter(Claim)
    }
    
    // 'infoBar' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    function infoBar_refreshVariables_126 (def :  pcf.ClaimInfoBar) : void {
      def.refreshVariables(Claim)
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard.pcf: line 33, column 30
    function initialValue_0 () : java.util.Date {
      return null
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard.pcf: line 50, column 47
    function initialValue_1 () : gw.api.claim.NewClaimWizardInfo {
      return new gw.api.claim.NewClaimWizardInfo(CurrentLocation, gw.api.claim.NewClaimCheck)
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard.pcf: line 54, column 21
    function initialValue_2 () : Claim {
      return wizardInfo.createClaim()
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard.pcf: line 58, column 48
    function initialValue_3 () : gw.pcf.fnol.BasicInfoScreenUtils {
      return new gw.pcf.fnol.BasicInfoScreenUtils(Claim)
    }
    
    // 'label' attribute on WizardStep (id=SelectRiskUnitsStep) at FNOLWizard.pcf: line 80, column 48
    function label_11 () : java.lang.String {
      return wizardInfo.SelectRiskUnitsLabel
    }
    
    // 'label' attribute on WizardStep (id=ClassicMainContacts) at FNOLWizard.pcf: line 92, column 158
    function label_18 () : java.lang.String {
      return "OLD " + DisplayKey.get("Wizard.NewClaimWizard.NewClaimMainContacts.Label")
    }
    
    // 'menuActions' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    function menuActions_onEnter_127 (def :  pcf.FNOLMenuActions) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'menuActions' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    function menuActions_refreshVariables_128 (def :  pcf.FNOLMenuActions) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'mode' attribute on WizardStep (id=ClassicLossDetails) at FNOLWizard.pcf: line 103, column 323
    function mode_24 () : java.lang.Object {
      return Claim.LossType
    }
    
    // 'mode' attribute on WizardStep (id=Services) at FNOLWizard.pcf: line 172, column 81
    function mode_81 () : java.lang.Object {
      return gw.config.LOBAbstraction.ForClaim.ForLossType.getUIMode(Claim)
    }
    
    // 'onEnter' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function onEnter_64 () : void {
      gw.api.claim.FnolServiceRequestHelper.cleanAndFinishServiceRequests(wizardInfo) /* clean up when going back from Services step */; wizardInfo.setDefaultValue(Claim)
    }
    
    // 'onEnter' attribute on WizardStep (id=Summary) at FNOLWizard.pcf: line 179, column 57
    function onEnter_92 () : void {
      wizardInfo.addExposureForEveryIncidentIfPossible()
    }
    
    // 'onExit' attribute on WizardStep (id=SelectRiskUnitsStep) at FNOLWizard.pcf: line 80, column 48
    function onExit_12 () : void {
      wizardInfo.onRiskUnitSelectExit()
    }
    
    // 'onExit' attribute on WizardStep (id=ClassicMainContacts) at FNOLWizard.pcf: line 92, column 158
    function onExit_19 () : void {
      wizardInfo.checkForDuplicatesNoWarning()
    }
    
    // 'onExit' attribute on WizardStep (id=ClassicLossDetails) at FNOLWizard.pcf: line 103, column 323
    function onExit_25 () : void {
      wizardInfo.finishSetupServiceRequests()
    }
    
    // 'onExit' attribute on WizardStep (id=ClassicExposures) at FNOLWizard.pcf: line 112, column 54
    function onExit_32 () : void {
      wizardInfo.finishSetupServiceRequests()
    }
    
    // 'onExit' attribute on WizardStep (id=FindPolicy) at FNOLWizard.pcf: line 69, column 49
    function onExit_5 () : void {
      wizardInfo.FindPolicyExit()
    }
    
    // 'onExit' attribute on WizardStep (id=BasicInfo) at FNOLWizard.pcf: line 150, column 155
    function onExit_54 () : void {
      tdic.cc.config.fnol.TDIC_WC7FNOLHelper.ensureBasicInfoContactRequiredFieldsPresent(Claim); wizardInfo.BasicInfoExit()
    }
    
    // 'onExit' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function onExit_65 () : void {
      wizardInfo.LossDetailsExit();wizardInfo.addAggregateLimits_TDIC((CurrentLocation as pcf.api.Location))
    }
    
    // 'onExit' attribute on WizardStep (id=Services) at FNOLWizard.pcf: line 172, column 81
    function onExit_82 () : void {
      gw.api.claim.FnolServiceRequestHelper.cleanAndFinishServiceRequests(wizardInfo)
    }
    
    // 'onFirstEnter' attribute on WizardStep (id=ClassicLossDetails) at FNOLWizard.pcf: line 103, column 323
    function onFirstEnter_26 () : void {
      wizardInfo.initClaim()
    }
    
    // 'onFirstEnter' attribute on WizardStep (id=FindPolicy) at FNOLWizard.pcf: line 69, column 49
    function onFirstEnter_6 () : void {
      wizardInfo.FindPolicyFirstEnter()
    }
    
    // 'onFirstEnter' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function onFirstEnter_66 () : void {
      wizardInfo.LossDetailsFirstEnter()
    }
    
    // 'parent' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    static function parent_129 (Claim :  Claim, LossDate :  java.util.Date, PageMode :  boolean, claimMode :  gw.api.claim.NewClaimMode, policyNumber :  String, policyType :  PolicyType, searchCriteriaAndSummary :  gw.api.claim.NewClaimPolicySearchCriteriaAndSummary) : pcf.api.Destination {
      return pcf.ClaimTabForward.createDestination()
    }
    
    // 'screen' attribute on WizardStep (id=PartiesInvolved) at FNOLWizard.pcf: line 249, column 51
    function screen_onEnter_100 (def :  pcf.NewClaimWizard_PartiesInvolvedScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=PolicyGeneral) at FNOLWizard.pcf: line 262, column 73
    function screen_onEnter_105 (def :  pcf.NewClaimWizard_PolicyGeneralScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=PolicyDetails) at FNOLWizard.pcf: line 271, column 73
    function screen_onEnter_109 (def :  pcf.NewClaimWizard_PolicyDetailsScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Documents) at FNOLWizard.pcf: line 281, column 51
    function screen_onEnter_113 (def :  pcf.FNOLWizard_DocumentsScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Notes) at FNOLWizard.pcf: line 290, column 51
    function screen_onEnter_117 (def :  pcf.FNOLWizard_NotesScreen) : void {
      def.onEnter(Claim)
    }
    
    // 'screen' attribute on WizardStep (id=SelectRiskUnitsStep) at FNOLWizard.pcf: line 80, column 48
    function screen_onEnter_13 (def :  pcf.FNOLWizard_PickPolicyRiskUnitsScreen) : void {
      def.onEnter( Claim, wizardInfo )
    }
    
    // 'screen' attribute on WizardStep (id=ClassicMainContacts) at FNOLWizard.pcf: line 92, column 158
    function screen_onEnter_20 (def :  pcf.NewClaimWizard_MainContactsScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicLossDetails) at FNOLWizard.pcf: line 103, column 323
    function screen_onEnter_27 (def :  pcf.NewClaimWizard_LossDetailsScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicExposures) at FNOLWizard.pcf: line 112, column 54
    function screen_onEnter_33 (def :  pcf.NewClaimWizard_ExposuresScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicPartiesInvolved) at FNOLWizard.pcf: line 119, column 158
    function screen_onEnter_37 (def :  pcf.NewClaimWizard_PartiesInvolvedScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicDocuments) at FNOLWizard.pcf: line 125, column 152
    function screen_onEnter_41 (def :  pcf.NewClaimWizard_DocumentsScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicAssign) at FNOLWizard.pcf: line 131, column 57
    function screen_onEnter_44 (def :  pcf.NewClaimWizard_AssignScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicSummary) at FNOLWizard.pcf: line 136, column 150
    function screen_onEnter_47 (def :  pcf.NewClaimWizard_SummaryScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=BasicInfo) at FNOLWizard.pcf: line 150, column 155
    function screen_onEnter_55 (def :  pcf.FNOLWizard_BasicInfoScreen_WC) : void {
      def.onEnter(Claim, wizardInfo,basicInfoUtils)
    }
    
    // 'screen' attribute on WizardStep (id=BasicInfo) at FNOLWizard.pcf: line 150, column 155
    function screen_onEnter_57 (def :  pcf.FNOLWizard_BasicInfoScreen_WC7) : void {
      def.onEnter(Claim, wizardInfo,basicInfoUtils)
    }
    
    // 'screen' attribute on WizardStep (id=BasicInfo) at FNOLWizard.pcf: line 150, column 155
    function screen_onEnter_59 (def :  pcf.FNOLWizard_BasicInfoScreen_default) : void {
      def.onEnter(Claim, wizardInfo,basicInfoUtils)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_onEnter_67 (def :  pcf.FNOLWizard_NewLossDetailsScreen_Gl) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_onEnter_69 (def :  pcf.FNOLWizard_NewLossDetailsScreen_PR) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=FindPolicy) at FNOLWizard.pcf: line 69, column 49
    function screen_onEnter_7 (def :  pcf.FNOLWizard_FindPolicyScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_onEnter_71 (def :  pcf.FNOLWizard_NewLossDetailsScreen_Trav) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_onEnter_73 (def :  pcf.FNOLWizard_NewLossDetailsScreen_WC) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_onEnter_75 (def :  pcf.FNOLWizard_NewLossDetailsScreen_WC7) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_onEnter_77 (def :  pcf.FNOLWizard_NewLossDetailsScreen_default) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Services) at FNOLWizard.pcf: line 172, column 81
    function screen_onEnter_83 (def :  pcf.FNOLWizard_ServicesScreen_Auto) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Services) at FNOLWizard.pcf: line 172, column 81
    function screen_onEnter_85 (def :  pcf.FNOLWizard_ServicesScreen_Gl) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Services) at FNOLWizard.pcf: line 172, column 81
    function screen_onEnter_87 (def :  pcf.FNOLWizard_ServicesScreen_Pr) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Summary) at FNOLWizard.pcf: line 179, column 57
    function screen_onEnter_93 (def :  pcf.FNOLWizard_AssignSaveScreen) : void {
      def.onEnter(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=PartiesInvolved) at FNOLWizard.pcf: line 249, column 51
    function screen_refreshVariables_101 (def :  pcf.NewClaimWizard_PartiesInvolvedScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=PolicyGeneral) at FNOLWizard.pcf: line 262, column 73
    function screen_refreshVariables_106 (def :  pcf.NewClaimWizard_PolicyGeneralScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=PolicyDetails) at FNOLWizard.pcf: line 271, column 73
    function screen_refreshVariables_110 (def :  pcf.NewClaimWizard_PolicyDetailsScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Documents) at FNOLWizard.pcf: line 281, column 51
    function screen_refreshVariables_114 (def :  pcf.FNOLWizard_DocumentsScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Notes) at FNOLWizard.pcf: line 290, column 51
    function screen_refreshVariables_118 (def :  pcf.FNOLWizard_NotesScreen) : void {
      def.refreshVariables(Claim)
    }
    
    // 'screen' attribute on WizardStep (id=SelectRiskUnitsStep) at FNOLWizard.pcf: line 80, column 48
    function screen_refreshVariables_14 (def :  pcf.FNOLWizard_PickPolicyRiskUnitsScreen) : void {
      def.refreshVariables( Claim, wizardInfo )
    }
    
    // 'screen' attribute on WizardStep (id=ClassicMainContacts) at FNOLWizard.pcf: line 92, column 158
    function screen_refreshVariables_21 (def :  pcf.NewClaimWizard_MainContactsScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicLossDetails) at FNOLWizard.pcf: line 103, column 323
    function screen_refreshVariables_28 (def :  pcf.NewClaimWizard_LossDetailsScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicExposures) at FNOLWizard.pcf: line 112, column 54
    function screen_refreshVariables_34 (def :  pcf.NewClaimWizard_ExposuresScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicPartiesInvolved) at FNOLWizard.pcf: line 119, column 158
    function screen_refreshVariables_38 (def :  pcf.NewClaimWizard_PartiesInvolvedScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicDocuments) at FNOLWizard.pcf: line 125, column 152
    function screen_refreshVariables_42 (def :  pcf.NewClaimWizard_DocumentsScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicAssign) at FNOLWizard.pcf: line 131, column 57
    function screen_refreshVariables_45 (def :  pcf.NewClaimWizard_AssignScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=ClassicSummary) at FNOLWizard.pcf: line 136, column 150
    function screen_refreshVariables_48 (def :  pcf.NewClaimWizard_SummaryScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=BasicInfo) at FNOLWizard.pcf: line 150, column 155
    function screen_refreshVariables_56 (def :  pcf.FNOLWizard_BasicInfoScreen_WC) : void {
      def.refreshVariables(Claim, wizardInfo,basicInfoUtils)
    }
    
    // 'screen' attribute on WizardStep (id=BasicInfo) at FNOLWizard.pcf: line 150, column 155
    function screen_refreshVariables_58 (def :  pcf.FNOLWizard_BasicInfoScreen_WC7) : void {
      def.refreshVariables(Claim, wizardInfo,basicInfoUtils)
    }
    
    // 'screen' attribute on WizardStep (id=BasicInfo) at FNOLWizard.pcf: line 150, column 155
    function screen_refreshVariables_60 (def :  pcf.FNOLWizard_BasicInfoScreen_default) : void {
      def.refreshVariables(Claim, wizardInfo,basicInfoUtils)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_refreshVariables_68 (def :  pcf.FNOLWizard_NewLossDetailsScreen_Gl) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_refreshVariables_70 (def :  pcf.FNOLWizard_NewLossDetailsScreen_PR) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_refreshVariables_72 (def :  pcf.FNOLWizard_NewLossDetailsScreen_Trav) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_refreshVariables_74 (def :  pcf.FNOLWizard_NewLossDetailsScreen_WC) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_refreshVariables_76 (def :  pcf.FNOLWizard_NewLossDetailsScreen_WC7) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=LossDetails) at FNOLWizard.pcf: line 161, column 323
    function screen_refreshVariables_78 (def :  pcf.FNOLWizard_NewLossDetailsScreen_default) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=FindPolicy) at FNOLWizard.pcf: line 69, column 49
    function screen_refreshVariables_8 (def :  pcf.FNOLWizard_FindPolicyScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Services) at FNOLWizard.pcf: line 172, column 81
    function screen_refreshVariables_84 (def :  pcf.FNOLWizard_ServicesScreen_Auto) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Services) at FNOLWizard.pcf: line 172, column 81
    function screen_refreshVariables_86 (def :  pcf.FNOLWizard_ServicesScreen_Gl) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Services) at FNOLWizard.pcf: line 172, column 81
    function screen_refreshVariables_88 (def :  pcf.FNOLWizard_ServicesScreen_Pr) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'screen' attribute on WizardStep (id=Summary) at FNOLWizard.pcf: line 179, column 57
    function screen_refreshVariables_94 (def :  pcf.FNOLWizard_AssignSaveScreen) : void {
      def.refreshVariables(Claim, wizardInfo)
    }
    
    // 'tabBar' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    function tabBar_onEnter_130 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on NewClaimWizard (id=FNOLWizard) at FNOLWizard.pcf: line 16, column 70
    function tabBar_refreshVariables_131 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    // 'title' attribute on WizardStep (id=SelectRiskUnitsStep) at FNOLWizard.pcf: line 80, column 48
    function title_15 () : java.lang.String {
      return wizardInfo.SelectRiskUnitsTitle
    }
    
    // 'title' attribute on WizardStep (id=ClassicMainContacts) at FNOLWizard.pcf: line 92, column 158
    function title_22 () : java.lang.String {
      return DisplayKey.get("Wizard.NewClaimWizard.NewClaimMainContacts.Step",  wizardInfo.CurrentStepNumber ,  wizardInfo.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=ClassicLossDetails) at FNOLWizard.pcf: line 103, column 323
    function title_29 () : java.lang.String {
      return Claim.Policy.Verified ? DisplayKey.get("Wizard.NewClaimWizard.NewClaimLossDetails.Step",  wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps) : DisplayKey.get("TDIC.Wizard.NewClaimWizard.NewClaimLossDetails.Step",  wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=ClassicExposures) at FNOLWizard.pcf: line 112, column 54
    function title_35 () : java.lang.String {
      return DisplayKey.get("Wizard.NewClaimWizard.NewClaimExposures.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=ClassicPartiesInvolved) at FNOLWizard.pcf: line 119, column 158
    function title_39 () : java.lang.String {
      return DisplayKey.get("Wizard.NewClaimWizard.NewClaimPartiesInvolved.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=ClassicDocuments) at FNOLWizard.pcf: line 125, column 152
    function title_43 () : java.lang.String {
      return DisplayKey.get("Wizard.NewClaimWizard.NewClaimDocuments.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=ClassicAssign) at FNOLWizard.pcf: line 131, column 57
    function title_46 () : java.lang.String {
      return getSaveAssignScreenDisplayTitle_TDIC()
    }
    
    // 'title' attribute on WizardStep (id=ClassicSummary) at FNOLWizard.pcf: line 136, column 150
    function title_49 () : java.lang.String {
      return DisplayKey.get("Wizard.NewClaimWizard.NewClaimSummary.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=BasicInfo) at FNOLWizard.pcf: line 150, column 155
    function title_61 () : java.lang.String {
      return DisplayKey.get("Wizard.NewClaimWizard.NewClaimMainContacts.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=Services) at FNOLWizard.pcf: line 172, column 81
    function title_89 () : java.lang.String {
      return DisplayKey.get("Wizard.NewClaimWizard.NewClaimServices.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=FindPolicy) at FNOLWizard.pcf: line 69, column 49
    function title_9 () : java.lang.String {
      return wizardInfo.SearchPolicyStepTitle
    }
    
    // 'visible' attribute on WizardStep (id=SelectRiskUnitsStep) at FNOLWizard.pcf: line 80, column 48
    function visible_10 () : java.lang.Boolean {
      return wizardInfo.CanSelectRiskUnits
    }
    
    // 'visible' attribute on WizardStep (id=PolicyGeneral) at FNOLWizard.pcf: line 262, column 73
    function visible_103 () : java.lang.Boolean {
      return perm.Policy.view(Claim) and perm.System.viewpolicy
    }
    
    // 'visible' attribute on WizardStep (id=ClassicExposures) at FNOLWizard.pcf: line 112, column 54
    function visible_30 () : java.lang.Boolean {
      return wizardInfo.ExposureListChangeable
    }
    
    // 'visible' attribute on WizardStep (id=Services) at FNOLWizard.pcf: line 172, column 81
    function visible_80 () : java.lang.Boolean {
      return Claim.Policy.Verified and wizardInfo.IsServicesStepAvailable
    }
    
    // 'visible' attribute on WizardStepSet (id=FullWizardStepSet) at FNOLWizard.pcf: line 141, column 48
    function visible_97 () : java.lang.Boolean {
      return !wizardInfo.useOldWizardStep()
    }
    
    // '$$wizardStepAvailable' attribute on WizardStep (id=PartiesInvolved) at FNOLWizard.pcf: line 249, column 51
    function wizardStepAvailable_102 () : java.lang.Boolean {
      return wizardInfo.IndependentStepAllowed
    }
    
    // '$$wizardStepAvailable' attribute on WizardStep (id=Services) at FNOLWizard.pcf: line 172, column 81
    function wizardStepAvailable_90 () : java.lang.Boolean {
      return wizardInfo.IsServicesStepAvailable
    }
    
    property get Claim () : Claim {
      return getVariableValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setVariableValue("Claim", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.FNOLWizard {
      return super.CurrentLocation as pcf.FNOLWizard
    }
    
    property get LossDate () : java.util.Date {
      return getVariableValue("LossDate", 0) as java.util.Date
    }
    
    property set LossDate ($arg :  java.util.Date) {
      setVariableValue("LossDate", 0, $arg)
    }
    
    property get PageMode () : boolean {
      return getVariableValue("PageMode", 0) as java.lang.Boolean
    }
    
    property set PageMode ($arg :  boolean) {
      setVariableValue("PageMode", 0, $arg)
    }
    
    property get basicInfoUtils () : gw.pcf.fnol.BasicInfoScreenUtils {
      return getVariableValue("basicInfoUtils", 0) as gw.pcf.fnol.BasicInfoScreenUtils
    }
    
    property set basicInfoUtils ($arg :  gw.pcf.fnol.BasicInfoScreenUtils) {
      setVariableValue("basicInfoUtils", 0, $arg)
    }
    
    property get claimMode () : gw.api.claim.NewClaimMode {
      return getVariableValue("claimMode", 0) as gw.api.claim.NewClaimMode
    }
    
    property set claimMode ($arg :  gw.api.claim.NewClaimMode) {
      setVariableValue("claimMode", 0, $arg)
    }
    
    property get policyNumber () : String {
      return getVariableValue("policyNumber", 0) as String
    }
    
    property set policyNumber ($arg :  String) {
      setVariableValue("policyNumber", 0, $arg)
    }
    
    property get policyType () : PolicyType {
      return getVariableValue("policyType", 0) as PolicyType
    }
    
    property set policyType ($arg :  PolicyType) {
      setVariableValue("policyType", 0, $arg)
    }
    
    property get searchCriteriaAndSummary () : gw.api.claim.NewClaimPolicySearchCriteriaAndSummary {
      return getVariableValue("searchCriteriaAndSummary", 0) as gw.api.claim.NewClaimPolicySearchCriteriaAndSummary
    }
    
    property set searchCriteriaAndSummary ($arg :  gw.api.claim.NewClaimPolicySearchCriteriaAndSummary) {
      setVariableValue("searchCriteriaAndSummary", 0, $arg)
    }
    
    property get wizardInfo () : gw.api.claim.NewClaimWizardInfo {
      return getVariableValue("wizardInfo", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set wizardInfo ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setVariableValue("wizardInfo", 0, $arg)
    }
    
    function getSaveAssignScreenDisplayTitle_TDIC() : String {
          if (Claim.Policy.Verified)
            return DisplayKey.get("Wizard.NewClaimWizard.NewClaimAssign.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
          else {
            if (Claim.Policy.PolicyType == PolicyType.TC_WC7WORKERSCOMP) {
              return DisplayKey.get("TDIC.Wizard.NewClaimWizard.NewClaimAssign.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
            } else if (Claim.Policy.PolicyType == PolicyType.TC_BUSINESSOWNERS) {
              return DisplayKey.get("TDIC.Wizard.NewClaimWizard.NewClaimAssign.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
            } else if (Claim.Policy.PolicyType == PolicyType.TC_GENERALLIABILITY) {
              return DisplayKey.get("TDIC.Wizard.NewClaimWizard.NewClaimAssign.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
            }
          }
          return ""
        }
    
        function getLossDetailsScreenDisplayTitle_TDIC() : String {
          if (Claim.Policy.Verified) {
            return DisplayKey.get("Wizard.NewClaimWizard.NewClaimLossDetails.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
          } else {
            return DisplayKey.get("TDIC.Wizard.NewClaimWizard.NewClaimLossDetails.Step", wizardInfo.CurrentStepNumber, wizardInfo.TotalNumberOfSteps)
          }
        }
    
    
  }
  
  
}