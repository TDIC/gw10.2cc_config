package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/policy/CovTerm/ClaimPolicyCovTermsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClaimPolicyCovTermsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/policy/CovTerm/ClaimPolicyCovTermsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimPolicyCovTermsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on RowIterator at ClaimPolicyCovTermsLV.pcf: line 20, column 36
    function editable_5 () : java.lang.Boolean {
      return !Coverage.Policy.Verified
    }
    
    // 'value' attribute on TextCell (id=covSortOrderCell_Cell) at ClaimPolicyCovTermsLV.pcf: line 28, column 28
    function sortValue_0 (CovTerm :  entity.CovTerm) : java.lang.Object {
      return CovTerm.CovTermOrder
    }
    
    // 'value' attribute on TypeKeyCell (id=CovTermPattern_Cell) at ClaimPolicyCovTermsLV.pcf: line 36, column 47
    function sortValue_1 (CovTerm :  entity.CovTerm) : java.lang.Object {
      return CovTerm.CovTermPattern
    }
    
    // 'value' attribute on TypeKeyCell (id=ModelRestriction_Cell) at ClaimPolicyCovTermsLV.pcf: line 43, column 25
    function sortValue_2 (CovTerm :  entity.CovTerm) : java.lang.Object {
      return CovTerm.ModelRestriction
    }
    
    // 'value' attribute on TextCell (id=Amount_Cell) at ClaimPolicyCovTermsLV.pcf: line 51, column 34
    function sortValue_3 (CovTerm :  entity.CovTerm) : java.lang.Object {
      return CovTerm.Value
    }
    
    // 'value' attribute on TypeKeyCell (id=ModelAggregation_Cell) at ClaimPolicyCovTermsLV.pcf: line 57, column 25
    function sortValue_4 (CovTerm :  entity.CovTerm) : java.lang.Object {
      return CovTerm.ModelAggregation
    }
    
    // 'toRemove' attribute on RowIterator at ClaimPolicyCovTermsLV.pcf: line 20, column 36
    function toRemove_32 (CovTerm :  entity.CovTerm) : void {
      Coverage.removeFromCovTerms(CovTerm)
    }
    
    // 'value' attribute on RowIterator at ClaimPolicyCovTermsLV.pcf: line 20, column 36
    function value_33 () : entity.CovTerm[] {
      return Coverage.CovTerms.sortBy(\ c -> c.CovTermOrder)
    }
    
    property get Coverage () : Coverage {
      return getRequireValue("Coverage", 0) as Coverage
    }
    
    property set Coverage ($arg :  Coverage) {
      setRequireValue("Coverage", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/policy/CovTerm/ClaimPolicyCovTermsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ClaimPolicyCovTermsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TypeKeyCell (id=CovTermPattern_Cell) at ClaimPolicyCovTermsLV.pcf: line 36, column 47
    function action_9 () : void {
      CovTermPopup.push(CovTerm, CurrentLocation.InEditMode)
    }
    
    // 'action' attribute on TypeKeyCell (id=CovTermPattern_Cell) at ClaimPolicyCovTermsLV.pcf: line 36, column 47
    function action_dest_10 () : pcf.api.Destination {
      return pcf.CovTermPopup.createDestination(CovTerm, CurrentLocation.InEditMode)
    }
    
    // 'def' attribute on ModalCellRef at ClaimPolicyCovTermsLV.pcf: line 47, column 39
    function def_onEnter_17 (def :  pcf.CovTermAmount_Classification) : void {
      def.onEnter(CovTerm)
    }
    
    // 'def' attribute on ModalCellRef at ClaimPolicyCovTermsLV.pcf: line 47, column 39
    function def_onEnter_19 (def :  pcf.CovTermAmount_Financial) : void {
      def.onEnter(CovTerm)
    }
    
    // 'def' attribute on ModalCellRef at ClaimPolicyCovTermsLV.pcf: line 47, column 39
    function def_onEnter_21 (def :  pcf.CovTermAmount_Numeric) : void {
      def.onEnter(CovTerm)
    }
    
    // 'def' attribute on ModalCellRef at ClaimPolicyCovTermsLV.pcf: line 47, column 39
    function def_onEnter_23 (def :  pcf.CovTermAmount_default) : void {
      def.onEnter(CovTerm)
    }
    
    // 'def' attribute on ModalCellRef at ClaimPolicyCovTermsLV.pcf: line 47, column 39
    function def_refreshVariables_18 (def :  pcf.CovTermAmount_Classification) : void {
      def.refreshVariables(CovTerm)
    }
    
    // 'def' attribute on ModalCellRef at ClaimPolicyCovTermsLV.pcf: line 47, column 39
    function def_refreshVariables_20 (def :  pcf.CovTermAmount_Financial) : void {
      def.refreshVariables(CovTerm)
    }
    
    // 'def' attribute on ModalCellRef at ClaimPolicyCovTermsLV.pcf: line 47, column 39
    function def_refreshVariables_22 (def :  pcf.CovTermAmount_Numeric) : void {
      def.refreshVariables(CovTerm)
    }
    
    // 'def' attribute on ModalCellRef at ClaimPolicyCovTermsLV.pcf: line 47, column 39
    function def_refreshVariables_24 (def :  pcf.CovTermAmount_default) : void {
      def.refreshVariables(CovTerm)
    }
    
    // 'mode' attribute on ModalCellRef at ClaimPolicyCovTermsLV.pcf: line 47, column 39
    function mode_25 () : java.lang.Object {
      return CovTerm.CovTermMode
    }
    
    // 'value' attribute on TextCell (id=covSortOrderCell_Cell) at ClaimPolicyCovTermsLV.pcf: line 28, column 28
    function valueRoot_7 () : java.lang.Object {
      return CovTerm
    }
    
    // 'value' attribute on TypeKeyCell (id=CovTermPattern_Cell) at ClaimPolicyCovTermsLV.pcf: line 36, column 47
    function value_11 () : typekey.CovTermPattern {
      return CovTerm.CovTermPattern
    }
    
    // 'value' attribute on TypeKeyCell (id=ModelRestriction_Cell) at ClaimPolicyCovTermsLV.pcf: line 43, column 25
    function value_14 () : typekey.CovTermModelRest {
      return CovTerm.ModelRestriction
    }
    
    // 'value' attribute on TextCell (id=Amount_Cell) at ClaimPolicyCovTermsLV.pcf: line 51, column 34
    function value_26 () : java.lang.String {
      return CovTerm.Value
    }
    
    // 'value' attribute on TypeKeyCell (id=ModelAggregation_Cell) at ClaimPolicyCovTermsLV.pcf: line 57, column 25
    function value_29 () : typekey.CovTermModelAgg {
      return CovTerm.ModelAggregation
    }
    
    // 'value' attribute on TextCell (id=covSortOrderCell_Cell) at ClaimPolicyCovTermsLV.pcf: line 28, column 28
    function value_6 () : java.lang.Integer {
      return CovTerm.CovTermOrder
    }
    
    property get CovTerm () : entity.CovTerm {
      return getIteratedValue(1) as entity.CovTerm
    }
    
    
  }
  
  
}