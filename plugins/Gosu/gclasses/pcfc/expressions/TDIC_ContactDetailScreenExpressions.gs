package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_ContactDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_ContactDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_ContactDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_ContactDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ButtonInput (id=ShowActivitiesButton_Input) at TDIC_ContactDetailScreen.pcf: line 106, column 116
    function action_26 () : void {
      helper.revealContent(ACTIVITIES)
    }
    
    // 'action' attribute on ButtonInput (id=ShowClaimsButton_Input) at TDIC_ContactDetailScreen.pcf: line 129, column 113
    function action_33 () : void {
      helper.revealContent(CLAIMS)
    }
    
    // 'action' attribute on ButtonInput (id=ShowExposuresButton_Input) at TDIC_ContactDetailScreen.pcf: line 152, column 116
    function action_40 () : void {
      helper.revealContent(EXPOSURES)
    }
    
    // 'action' attribute on ButtonInput (id=ShowMattersButton_Input) at TDIC_ContactDetailScreen.pcf: line 175, column 114
    function action_47 () : void {
      helper.revealContent(MATTERS)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 59, column 34
    function def_onEnter_10 (def :  pcf.TDIC_ContactBasicsDV_default) : void {
      def.onEnter(contactHandle, contactRole, showRoles, linkStatus, claim)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 72, column 50
    function def_onEnter_16 (def :  pcf.AddressesPanelSet) : void {
      def.onEnter(contactHandle)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 85, column 64
    function def_onEnter_22 (def :  pcf.ContactRelatedContactsDV) : void {
      def.onEnter(contactHandle, claim)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 111, column 57
    function def_onEnter_29 (def :  pcf.AddressBookContactActivitiesLV) : void {
      def.onEnter(Contact)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 134, column 53
    function def_onEnter_36 (def :  pcf.AddressBookContactClaimsLV) : void {
      def.onEnter(Contact)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 59, column 34
    function def_onEnter_4 (def :  pcf.TDIC_ContactBasicsDV_Company) : void {
      def.onEnter(contactHandle, contactRole, showRoles, linkStatus, claim)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 157, column 56
    function def_onEnter_43 (def :  pcf.AddressBookContactExposuresLV) : void {
      def.onEnter(Contact)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 180, column 54
    function def_onEnter_50 (def :  pcf.AddressBookContactMattersLV) : void {
      def.onEnter(Contact)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 190, column 62
    function def_onEnter_56 (def :  pcf.ReviewsPanelSet) : void {
      def.onEnter(claim, Contact, true, false)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 59, column 34
    function def_onEnter_6 (def :  pcf.TDIC_ContactBasicsDV_Person) : void {
      def.onEnter(contactHandle, contactRole, showRoles, linkStatus, claim)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 59, column 34
    function def_onEnter_8 (def :  pcf.TDIC_ContactBasicsDV_Place) : void {
      def.onEnter(contactHandle, contactRole, showRoles, linkStatus, claim)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 59, column 34
    function def_refreshVariables_11 (def :  pcf.TDIC_ContactBasicsDV_default) : void {
      def.refreshVariables(contactHandle, contactRole, showRoles, linkStatus, claim)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 72, column 50
    function def_refreshVariables_17 (def :  pcf.AddressesPanelSet) : void {
      def.refreshVariables(contactHandle)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 85, column 64
    function def_refreshVariables_23 (def :  pcf.ContactRelatedContactsDV) : void {
      def.refreshVariables(contactHandle, claim)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 111, column 57
    function def_refreshVariables_30 (def :  pcf.AddressBookContactActivitiesLV) : void {
      def.refreshVariables(Contact)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 134, column 53
    function def_refreshVariables_37 (def :  pcf.AddressBookContactClaimsLV) : void {
      def.refreshVariables(Contact)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 157, column 56
    function def_refreshVariables_44 (def :  pcf.AddressBookContactExposuresLV) : void {
      def.refreshVariables(Contact)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 59, column 34
    function def_refreshVariables_5 (def :  pcf.TDIC_ContactBasicsDV_Company) : void {
      def.refreshVariables(contactHandle, contactRole, showRoles, linkStatus, claim)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 180, column 54
    function def_refreshVariables_51 (def :  pcf.AddressBookContactMattersLV) : void {
      def.refreshVariables(Contact)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 190, column 62
    function def_refreshVariables_57 (def :  pcf.ReviewsPanelSet) : void {
      def.refreshVariables(claim, Contact, true, false)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 59, column 34
    function def_refreshVariables_7 (def :  pcf.TDIC_ContactBasicsDV_Person) : void {
      def.refreshVariables(contactHandle, contactRole, showRoles, linkStatus, claim)
    }
    
    // 'def' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 59, column 34
    function def_refreshVariables_9 (def :  pcf.TDIC_ContactBasicsDV_Place) : void {
      def.refreshVariables(contactHandle, contactRole, showRoles, linkStatus, claim)
    }
    
    // 'initialValue' attribute on Variable at TDIC_ContactDetailScreen.pcf: line 46, column 50
    function initialValue_0 () : gw.api.contact.ContactDetailHelper {
      return new gw.api.contact.ContactDetailHelper(contactHandle.Contact.IsHighlyLinked)
    }
    
    // 'initialValue' attribute on Variable at TDIC_ContactDetailScreen.pcf: line 50, column 23
    function initialValue_1 () : boolean {
      return claim.State == typekey.ClaimState.TC_DRAFT
    }
    
    // 'mode' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 59, column 34
    function mode_12 () : java.lang.Object {
      return Contact.Subtype
    }
    
    // 'onSelect' attribute on Card (id=ContactBasicsCard) at TDIC_ContactDetailScreen.pcf: line 56, column 80
    function onSelect_13 () : void {
      helper.CurrentCard = BASICS
    }
    
    // 'onSelect' attribute on Card (id=ContactAddressesCard) at TDIC_ContactDetailScreen.pcf: line 70, column 43
    function onSelect_19 () : void {
      helper.CurrentCard = ADDRESSES
    }
    
    // 'onSelect' attribute on Card (id=ContactRelatedContactsCard) at TDIC_ContactDetailScreen.pcf: line 83, column 66
    function onSelect_25 () : void {
      helper.CurrentCard = RELATED_CONTACTS
    }
    
    // 'onSelect' attribute on Card (id=AddressBookContactActivitiesCard) at TDIC_ContactDetailScreen.pcf: line 96, column 74
    function onSelect_32 () : void {
      helper.CurrentCard = ACTIVITIES
    }
    
    // 'onSelect' attribute on Card (id=AddressBookContactClaimsCard) at TDIC_ContactDetailScreen.pcf: line 119, column 71
    function onSelect_39 () : void {
      helper.CurrentCard = CLAIMS
    }
    
    // 'onSelect' attribute on Card (id=AddressBookContactExposuresCard) at TDIC_ContactDetailScreen.pcf: line 142, column 74
    function onSelect_46 () : void {
      helper.CurrentCard = EXPOSURES
    }
    
    // 'onSelect' attribute on Card (id=AddressBookContactMattersCard) at TDIC_ContactDetailScreen.pcf: line 165, column 72
    function onSelect_53 () : void {
      helper.CurrentCard = MATTERS
    }
    
    // 'onSelect' attribute on Card (id=AddressBookContactReviewCard) at TDIC_ContactDetailScreen.pcf: line 188, column 94
    function onSelect_59 () : void {
      helper.CurrentCard = REVIEWS
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at TDIC_ContactDetailScreen.pcf: line 75, column 163
    function toolbarButtonSet_onEnter_14 (def :  pcf.ContactDetailToolbarButtonSet) : void {
      def.onEnter(contactHandle, canAccessLinkButtons, linkStatus, canPick, allowEditInAddressBook, canEdit, claim)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at TDIC_ContactDetailScreen.pcf: line 62, column 165
    function toolbarButtonSet_onEnter_2 (def :  pcf.TDIC_ContactDetailToolbarButtonSet) : void {
      def.onEnter(contactHandle, contactRole, canAccessLinkButtons, linkStatus, canPick, allowEditInAddressBook)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at TDIC_ContactDetailScreen.pcf: line 88, column 163
    function toolbarButtonSet_onEnter_20 (def :  pcf.ContactDetailToolbarButtonSet) : void {
      def.onEnter(contactHandle, canAccessLinkButtons, linkStatus, canPick, allowEditInAddressBook, canEdit, claim)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at TDIC_ContactDetailScreen.pcf: line 193, column 163
    function toolbarButtonSet_onEnter_54 (def :  pcf.ContactDetailToolbarButtonSet) : void {
      def.onEnter(contactHandle, canAccessLinkButtons, linkStatus, canPick, allowEditInAddressBook, canEdit, claim)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at TDIC_ContactDetailScreen.pcf: line 75, column 163
    function toolbarButtonSet_refreshVariables_15 (def :  pcf.ContactDetailToolbarButtonSet) : void {
      def.refreshVariables(contactHandle, canAccessLinkButtons, linkStatus, canPick, allowEditInAddressBook, canEdit, claim)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at TDIC_ContactDetailScreen.pcf: line 88, column 163
    function toolbarButtonSet_refreshVariables_21 (def :  pcf.ContactDetailToolbarButtonSet) : void {
      def.refreshVariables(contactHandle, canAccessLinkButtons, linkStatus, canPick, allowEditInAddressBook, canEdit, claim)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at TDIC_ContactDetailScreen.pcf: line 62, column 165
    function toolbarButtonSet_refreshVariables_3 (def :  pcf.TDIC_ContactDetailToolbarButtonSet) : void {
      def.refreshVariables(contactHandle, contactRole, canAccessLinkButtons, linkStatus, canPick, allowEditInAddressBook)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at TDIC_ContactDetailScreen.pcf: line 193, column 163
    function toolbarButtonSet_refreshVariables_55 (def :  pcf.ContactDetailToolbarButtonSet) : void {
      def.refreshVariables(contactHandle, canAccessLinkButtons, linkStatus, canPick, allowEditInAddressBook, canEdit, claim)
    }
    
    // 'visible' attribute on Card (id=ContactAddressesCard) at TDIC_ContactDetailScreen.pcf: line 70, column 43
    function visible_18 () : java.lang.Boolean {
      return !(Contact typeis Place)
    }
    
    // 'visible' attribute on Card (id=ContactRelatedContactsCard) at TDIC_ContactDetailScreen.pcf: line 83, column 66
    function visible_24 () : java.lang.Boolean {
      return not (contactHandle.Contact typeis UserContact)
    }
    
    // 'visible' attribute on DetailViewPanel at TDIC_ContactDetailScreen.pcf: line 99, column 57
    function visible_27 () : java.lang.Boolean {
      return helper.isWarningVisible(ACTIVITIES)
    }
    
    // 'visible' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 111, column 57
    function visible_28 () : java.lang.Boolean {
      return helper.isContentVisible(ACTIVITIES)
    }
    
    // 'visible' attribute on Card (id=AddressBookContactActivitiesCard) at TDIC_ContactDetailScreen.pcf: line 96, column 74
    function visible_31 () : java.lang.Boolean {
      return  showRelatedObjects and perm.Claim.genericviewactivity
    }
    
    // 'visible' attribute on DetailViewPanel at TDIC_ContactDetailScreen.pcf: line 122, column 53
    function visible_34 () : java.lang.Boolean {
      return helper.isWarningVisible(CLAIMS)
    }
    
    // 'visible' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 134, column 53
    function visible_35 () : java.lang.Boolean {
      return helper.isContentVisible(CLAIMS)
    }
    
    // 'visible' attribute on Card (id=AddressBookContactClaimsCard) at TDIC_ContactDetailScreen.pcf: line 119, column 71
    function visible_38 () : java.lang.Boolean {
      return  showRelatedObjects and perm.Claim.genericviewclaim
    }
    
    // 'visible' attribute on DetailViewPanel at TDIC_ContactDetailScreen.pcf: line 145, column 56
    function visible_41 () : java.lang.Boolean {
      return helper.isWarningVisible(EXPOSURES)
    }
    
    // 'visible' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 157, column 56
    function visible_42 () : java.lang.Boolean {
      return helper.isContentVisible(EXPOSURES)
    }
    
    // 'visible' attribute on Card (id=AddressBookContactExposuresCard) at TDIC_ContactDetailScreen.pcf: line 142, column 74
    function visible_45 () : java.lang.Boolean {
      return  showRelatedObjects and perm.Claim.genericviewexposure
    }
    
    // 'visible' attribute on DetailViewPanel at TDIC_ContactDetailScreen.pcf: line 168, column 54
    function visible_48 () : java.lang.Boolean {
      return helper.isWarningVisible(MATTERS)
    }
    
    // 'visible' attribute on PanelRef at TDIC_ContactDetailScreen.pcf: line 180, column 54
    function visible_49 () : java.lang.Boolean {
      return helper.isContentVisible(MATTERS)
    }
    
    // 'visible' attribute on Card (id=AddressBookContactMattersCard) at TDIC_ContactDetailScreen.pcf: line 165, column 72
    function visible_52 () : java.lang.Boolean {
      return  showRelatedObjects and perm.Claim.genericviewmatter
    }
    
    // 'visible' attribute on Card (id=AddressBookContactReviewCard) at TDIC_ContactDetailScreen.pcf: line 188, column 94
    function visible_58 () : java.lang.Boolean {
      return util.ReviewPageHelper.shouldDisplayReviewTab(Contact) and perm.Review.list
    }
    
    property get allowEditInAddressBook () : boolean {
      return getRequireValue("allowEditInAddressBook", 0) as java.lang.Boolean
    }
    
    property set allowEditInAddressBook ($arg :  boolean) {
      setRequireValue("allowEditInAddressBook", 0, $arg)
    }
    
    property get canAccessLinkButtons () : boolean {
      return getRequireValue("canAccessLinkButtons", 0) as java.lang.Boolean
    }
    
    property set canAccessLinkButtons ($arg :  boolean) {
      setRequireValue("canAccessLinkButtons", 0, $arg)
    }
    
    property get canEdit () : boolean {
      return getVariableValue("canEdit", 0) as java.lang.Boolean
    }
    
    property set canEdit ($arg :  boolean) {
      setVariableValue("canEdit", 0, $arg)
    }
    
    property get canPick () : boolean {
      return getRequireValue("canPick", 0) as java.lang.Boolean
    }
    
    property set canPick ($arg :  boolean) {
      setRequireValue("canPick", 0, $arg)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get contactHandle () : gw.api.contact.ContactHandle {
      return getRequireValue("contactHandle", 0) as gw.api.contact.ContactHandle
    }
    
    property set contactHandle ($arg :  gw.api.contact.ContactHandle) {
      setRequireValue("contactHandle", 0, $arg)
    }
    
    property get contactRole () : typekey.ContactRole {
      return getRequireValue("contactRole", 0) as typekey.ContactRole
    }
    
    property set contactRole ($arg :  typekey.ContactRole) {
      setRequireValue("contactRole", 0, $arg)
    }
    
    property get helper () : gw.api.contact.ContactDetailHelper {
      return getVariableValue("helper", 0) as gw.api.contact.ContactDetailHelper
    }
    
    property set helper ($arg :  gw.api.contact.ContactDetailHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get linkStatus () : gw.api.contact.ContactSystemLinkStatus {
      return getRequireValue("linkStatus", 0) as gw.api.contact.ContactSystemLinkStatus
    }
    
    property set linkStatus ($arg :  gw.api.contact.ContactSystemLinkStatus) {
      setRequireValue("linkStatus", 0, $arg)
    }
    
    property get showRelatedObjects () : boolean {
      return getRequireValue("showRelatedObjects", 0) as java.lang.Boolean
    }
    
    property set showRelatedObjects ($arg :  boolean) {
      setRequireValue("showRelatedObjects", 0, $arg)
    }
    
    property get showRoles () : boolean {
      return getRequireValue("showRoles", 0) as java.lang.Boolean
    }
    
    property set showRoles ($arg :  boolean) {
      setRequireValue("showRoles", 0, $arg)
    }
    
    property get Contact() : Contact { return contactHandle.Contact; }
    
    
  }
  
  
}