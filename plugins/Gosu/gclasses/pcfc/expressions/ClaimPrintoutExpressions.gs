package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClaimPrintoutExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Associations1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 161, column 53
    function locationRef_23 () : pcf.api.Destination {
      return pcf.ClaimAssociations.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Associations1Section) at ClaimPrintout.pcf: line 156, column 48
    function printable_25 () : java.lang.Boolean {
      return perm.ClaimAssociation.view
    }
    
    // PrintSection (id=Associations1Section) at ClaimPrintout.pcf: line 156, column 48
    function visible_24 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Associations2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 564, column 46
    function locationRef_137 (ClaimAssociation :  ClaimAssociation) : pcf.api.Destination {
      return pcf.ClaimAssociationDetail.createDestination(Claim, ClaimAssociation)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 560, column 52
    function locationRef_138 () : pcf.api.Destination {
      return pcf.ClaimAssociations.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Associations2Section) at ClaimPrintout.pcf: line 555, column 48
    function printable_140 () : java.lang.Boolean {
      return perm.ClaimAssociation.view
    }
    
    // PrintSection (id=Associations2Section) at ClaimPrintout.pcf: line 555, column 48
    function visible_139 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Associations3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Associations3Section) at ClaimPrintout.pcf: line 1217, column 48
    function defaultSetter_371 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1222, column 53
    function locationRef_364 () : pcf.api.Destination {
      return pcf.ClaimAssociations.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1232, column 46
    function locationRef_365 (ClaimAssociation :  ClaimAssociation) : pcf.api.Destination {
      return pcf.ClaimAssociationDetail.createDestination(Claim, ClaimAssociation)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1228, column 52
    function locationRef_366 () : pcf.api.Destination {
      return pcf.ClaimAssociations.createDestination(Claim)
    }
    
    // PrintSection (id=Associations3Section) at ClaimPrintout.pcf: line 1217, column 48
    function value_368 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Associations3Section) at ClaimPrintout.pcf: line 1217, column 48
    function visible_367 () : java.lang.Boolean {
      return perm.ClaimAssociation.view
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Associations3Section) at ClaimPrintout.pcf: line 1217, column 48
    function visible_369 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Associations3Section) at ClaimPrintout.pcf: line 1217, column 48
    function visible_373 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Checks1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 298, column 57
    function locationRef_63 () : pcf.api.Destination {
      return pcf.ClaimFinancialsChecks.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Checks1Section) at ClaimPrintout.pcf: line 293, column 52
    function printable_65 () : java.lang.Boolean {
      return perm.Claim.viewpayments(Claim)
    }
    
    // PrintSection (id=Checks1Section) at ClaimPrintout.pcf: line 293, column 52
    function visible_64 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Checks2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 774, column 39
    function locationRef_198 (CheckView :  CheckView) : pcf.api.Destination {
      return pcf.ClaimFinancialsChecksDetailPrint.createDestination(Claim, CheckView)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 770, column 56
    function locationRef_199 () : pcf.api.Destination {
      return pcf.ClaimFinancialsChecks.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Checks2Section) at ClaimPrintout.pcf: line 765, column 52
    function printable_201 () : java.lang.Boolean {
      return perm.Claim.viewpayments(Claim)
    }
    
    // PrintSection (id=Checks2Section) at ClaimPrintout.pcf: line 765, column 52
    function visible_200 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Checks3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Checks3Section) at ClaimPrintout.pcf: line 1695, column 52
    function defaultSetter_559 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1700, column 57
    function locationRef_552 () : pcf.api.Destination {
      return pcf.ClaimFinancialsChecks.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1710, column 39
    function locationRef_553 (CheckView :  CheckView) : pcf.api.Destination {
      return pcf.ClaimFinancialsChecksDetailPrint.createDestination(Claim, CheckView)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1706, column 56
    function locationRef_554 () : pcf.api.Destination {
      return pcf.ClaimFinancialsChecks.createDestination(Claim)
    }
    
    // PrintSection (id=Checks3Section) at ClaimPrintout.pcf: line 1695, column 52
    function value_556 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Checks3Section) at ClaimPrintout.pcf: line 1695, column 52
    function visible_555 () : java.lang.Boolean {
      return perm.Claim.viewpayments(Claim)
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Checks3Section) at ClaimPrintout.pcf: line 1695, column 52
    function visible_557 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Checks3Section) at ClaimPrintout.pcf: line 1695, column 52
    function visible_561 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimLossEmployerLiability1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossEmployerLiability1Section) at ClaimPrintout.pcf: line 204, column 121
    function label_39 () : java.lang.String {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompEmployersLiability.Description
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 209, column 70
    function locationRef_37 () : pcf.api.Destination {
      return pcf.TopLevelExposureDetail.createDestination(employerLiability)
    }
    
    // 'printable' attribute on PrintSection (id=ClaimLossEmployerLiability1Section) at ClaimPrintout.pcf: line 204, column 121
    function printable_40 () : java.lang.Boolean {
      return helper.isWorkersCompClaim() and employerLiability != null and perm.Exposure.view(employerLiability)
    }
    
    // PrintSection (id=ClaimLossEmployerLiability1Section) at ClaimPrintout.pcf: line 204, column 121
    function visible_38 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimLossEmployerLiability2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossEmployerLiability2Section) at ClaimPrintout.pcf: line 608, column 121
    function label_154 () : java.lang.String {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompEmployersLiability.Description
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 613, column 70
    function locationRef_152 () : pcf.api.Destination {
      return pcf.TopLevelExposureDetail.createDestination(employerLiability)
    }
    
    // 'printable' attribute on PrintSection (id=ClaimLossEmployerLiability2Section) at ClaimPrintout.pcf: line 608, column 121
    function printable_155 () : java.lang.Boolean {
      return helper.isWorkersCompClaim() and employerLiability != null and perm.Exposure.view(employerLiability)
    }
    
    // PrintSection (id=ClaimLossEmployerLiability2Section) at ClaimPrintout.pcf: line 608, column 121
    function visible_153 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimLossEmployerLiabilitySectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=ClaimLossEmployerLiabilitySection) at ClaimPrintout.pcf: line 1276, column 121
    function defaultSetter_414 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossEmployerLiabilitySection) at ClaimPrintout.pcf: line 1276, column 121
    function label_412 () : java.lang.Object {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompEmployersLiability.Description
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossEmployerLiabilitySection) at ClaimPrintout.pcf: line 1276, column 121
    function label_418 () : java.lang.String {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompEmployersLiability.Description
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1281, column 70
    function locationRef_408 () : pcf.api.Destination {
      return pcf.TopLevelExposureDetail.createDestination(employerLiability)
    }
    
    // PrintSection (id=ClaimLossEmployerLiabilitySection) at ClaimPrintout.pcf: line 1276, column 121
    function value_410 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=ClaimLossEmployerLiabilitySection) at ClaimPrintout.pcf: line 1276, column 121
    function visible_409 () : java.lang.Boolean {
      return helper.isWorkersCompClaim() and employerLiability != null and perm.Exposure.view(employerLiability)
    }
    
    // 'childrenVisible' attribute on PrintSection (id=ClaimLossEmployerLiabilitySection) at ClaimPrintout.pcf: line 1276, column 121
    function visible_411 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=ClaimLossEmployerLiabilitySection) at ClaimPrintout.pcf: line 1276, column 121
    function visible_417 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimLossIndemityTimeLoss1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossIndemityTimeLoss1Section) at ClaimPrintout.pcf: line 192, column 119
    function label_35 () : java.lang.String {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompCoverageOtherThanMedical.Description
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 197, column 69
    function locationRef_33 () : pcf.api.Destination {
      return pcf.TopLevelExposureDetail.createDestination(indemityTimeLoss)
    }
    
    // 'printable' attribute on PrintSection (id=ClaimLossIndemityTimeLoss1Section) at ClaimPrintout.pcf: line 192, column 119
    function printable_36 () : java.lang.Boolean {
      return helper.isWorkersCompClaim() and indemityTimeLoss != null and perm.Exposure.view(indemityTimeLoss)
    }
    
    // PrintSection (id=ClaimLossIndemityTimeLoss1Section) at ClaimPrintout.pcf: line 192, column 119
    function visible_34 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimLossIndemityTimeLoss2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossIndemityTimeLoss2Section) at ClaimPrintout.pcf: line 596, column 119
    function label_150 () : java.lang.String {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompCoverageOtherThanMedical.Description
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 601, column 69
    function locationRef_148 () : pcf.api.Destination {
      return pcf.TopLevelExposureDetail.createDestination(indemityTimeLoss)
    }
    
    // 'printable' attribute on PrintSection (id=ClaimLossIndemityTimeLoss2Section) at ClaimPrintout.pcf: line 596, column 119
    function printable_151 () : java.lang.Boolean {
      return helper.isWorkersCompClaim() and indemityTimeLoss != null and perm.Exposure.view(indemityTimeLoss)
    }
    
    // PrintSection (id=ClaimLossIndemityTimeLoss2Section) at ClaimPrintout.pcf: line 596, column 119
    function visible_149 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimLossIndemityTimeLoss3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=ClaimLossIndemityTimeLoss3Section) at ClaimPrintout.pcf: line 1264, column 119
    function defaultSetter_402 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossIndemityTimeLoss3Section) at ClaimPrintout.pcf: line 1264, column 119
    function label_400 () : java.lang.Object {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompCoverageOtherThanMedical.Description
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossIndemityTimeLoss3Section) at ClaimPrintout.pcf: line 1264, column 119
    function label_406 () : java.lang.String {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompCoverageOtherThanMedical.Description
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1269, column 69
    function locationRef_396 () : pcf.api.Destination {
      return pcf.TopLevelExposureDetail.createDestination(indemityTimeLoss)
    }
    
    // PrintSection (id=ClaimLossIndemityTimeLoss3Section) at ClaimPrintout.pcf: line 1264, column 119
    function value_398 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=ClaimLossIndemityTimeLoss3Section) at ClaimPrintout.pcf: line 1264, column 119
    function visible_397 () : java.lang.Boolean {
      return helper.isWorkersCompClaim() and indemityTimeLoss != null and perm.Exposure.view(indemityTimeLoss)
    }
    
    // 'childrenVisible' attribute on PrintSection (id=ClaimLossIndemityTimeLoss3Section) at ClaimPrintout.pcf: line 1264, column 119
    function visible_399 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=ClaimLossIndemityTimeLoss3Section) at ClaimPrintout.pcf: line 1264, column 119
    function visible_405 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimLossMedicalDetail1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossMedicalDetail1Section) at ClaimPrintout.pcf: line 180, column 115
    function label_31 () : java.lang.String {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompCoverageMedicalOnly.Description
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 185, column 67
    function locationRef_29 () : pcf.api.Destination {
      return pcf.TopLevelExposureDetail.createDestination(medicalDetails)
    }
    
    // 'printable' attribute on PrintSection (id=ClaimLossMedicalDetail1Section) at ClaimPrintout.pcf: line 180, column 115
    function printable_32 () : java.lang.Boolean {
      return helper.isWorkersCompClaim() and medicalDetails != null and perm.Exposure.view(medicalDetails)
    }
    
    // PrintSection (id=ClaimLossMedicalDetail1Section) at ClaimPrintout.pcf: line 180, column 115
    function visible_30 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimLossMedicalDetail2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossMedicalDetail2Section) at ClaimPrintout.pcf: line 584, column 115
    function label_146 () : java.lang.String {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompCoverageMedicalOnly.Description
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 589, column 67
    function locationRef_144 () : pcf.api.Destination {
      return pcf.TopLevelExposureDetail.createDestination(medicalDetails)
    }
    
    // 'printable' attribute on PrintSection (id=ClaimLossMedicalDetail2Section) at ClaimPrintout.pcf: line 584, column 115
    function printable_147 () : java.lang.Boolean {
      return helper.isWorkersCompClaim() and medicalDetails != null and perm.Exposure.view(medicalDetails)
    }
    
    // PrintSection (id=ClaimLossMedicalDetail2Section) at ClaimPrintout.pcf: line 584, column 115
    function visible_145 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimLossMedicalDetail3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=ClaimLossMedicalDetail3Section) at ClaimPrintout.pcf: line 1252, column 115
    function defaultSetter_390 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossMedicalDetail3Section) at ClaimPrintout.pcf: line 1252, column 115
    function label_388 () : java.lang.Object {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompCoverageMedicalOnly.Description
    }
    
    // 'label' attribute on PrintSection (id=ClaimLossMedicalDetail3Section) at ClaimPrintout.pcf: line 1252, column 115
    function label_394 () : java.lang.String {
      return gw.config.CoverageSubtypeAbstraction.WorkersCompCoverageMedicalOnly.Description
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1257, column 67
    function locationRef_384 () : pcf.api.Destination {
      return pcf.TopLevelExposureDetail.createDestination(medicalDetails)
    }
    
    // PrintSection (id=ClaimLossMedicalDetail3Section) at ClaimPrintout.pcf: line 1252, column 115
    function value_386 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=ClaimLossMedicalDetail3Section) at ClaimPrintout.pcf: line 1252, column 115
    function visible_385 () : java.lang.Boolean {
      return helper.isWorkersCompClaim() and medicalDetails != null and perm.Exposure.view(medicalDetails)
    }
    
    // 'childrenVisible' attribute on PrintSection (id=ClaimLossMedicalDetail3Section) at ClaimPrintout.pcf: line 1252, column 115
    function visible_387 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=ClaimLossMedicalDetail3Section) at ClaimPrintout.pcf: line 1252, column 115
    function visible_393 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimPrintoutExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Claim :  Claim) : int {
      return 0
    }
    
    // 'action' attribute on NonDownloadPrintOutButton at ClaimPrintout.pcf: line 101, column 58
    function action_10 () : void {
      CurrentLocation.cancel()
    }
    
    // 'action' attribute on PrintOutButton (id=ClaimPrint) at ClaimPrintout.pcf: line 98, column 21
    function action_9 () : void {
      gw.api.print.PrintOutAction.printPrintOut(printSettings)
    }
    
    // 'canVisit' attribute on PrintOut (id=ClaimPrintout) at ClaimPrintout.pcf: line 53, column 89
    static function canVisit_663 (Claim :  Claim) : java.lang.Boolean {
      return perm.Claim.view(Claim) and perm.Claim.print(Claim)
    }
    
    // 'value' attribute on PrintOut (id=ClaimPrintout) at ClaimPrintout.pcf: line 53, column 89
    function defaultSetter_89 (__VALUE_TO_SET :  java.lang.Object) : void {
      Choice = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at ClaimPrintout.pcf: line 63, column 48
    function initialValue_0 () : gw.api.print.ClaimPrintoutHelper {
      return new gw.api.print.ClaimPrintoutHelper(Claim)
    }
    
    // 'initialValue' attribute on Variable at ClaimPrintout.pcf: line 67, column 42
    function initialValue_1 () : gw.api.print.PrintSettings {
      return helper.createPrintSettings()
    }
    
    // 'initialValue' attribute on Variable at ClaimPrintout.pcf: line 71, column 24
    function initialValue_2 () : Exposure {
      return helper.getMedicalDetails()
    }
    
    // 'initialValue' attribute on Variable at ClaimPrintout.pcf: line 75, column 24
    function initialValue_3 () : Exposure {
      return helper.getIndemityTimeLoss()
    }
    
    // 'initialValue' attribute on Variable at ClaimPrintout.pcf: line 79, column 24
    function initialValue_4 () : Exposure {
      return helper.getEmployerLiability()
    }
    
    // 'initialValue' attribute on Variable at ClaimPrintout.pcf: line 83, column 23
    function initialValue_5 () : boolean {
      return (helper.isWorkersCompClaim() or helper.isHomeownersClaim()) and (gw.api.policy.PolicyTabUtil.hasTab(Claim, "Properties") or gw.api.policy.PolicyTabUtil.hasTab(Claim, "Classcodes"))
    }
    
    // 'initialValue' attribute on Variable at ClaimPrintout.pcf: line 87, column 23
    function initialValue_6 () : boolean {
      return helper.isTravelClaim() and (gw.api.policy.PolicyTabUtil.hasTab(Claim, "Trips") or gw.api.policy.PolicyTabUtil.hasTab(Claim, "Classcodes"))
    }
    
    // 'initialValue' attribute on Variable at ClaimPrintout.pcf: line 91, column 23
    function initialValue_7 () : boolean {
      return (helper.isWorkersCompClaim() or helper.isHomeownersClaim()) and gw.api.policy.PolicyTabUtil.hasTab(Claim, "Statcodes")
    }
    
    // 'label' attribute on PrintGroup (id=AllClaimPagesWithDetails) at ClaimPrintout.pcf: line 395, column 260
    function label_8 () : java.lang.Object {
      return gw.config.LOBAbstraction.ForClaim.ForLossType.isWorkComp(Claim) ? DisplayKey.get("Java.PrintClaimOptionsForm.Label.AllClaimPagesWithDetailsForWC") : DisplayKey.get("Java.PrintClaimOptionsForm.Label.AllClaimPagesWithDetails")
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 930, column 32
    function locationRef_246 (Activity :  Activity) : pcf.api.Destination {
      return pcf.ActivityDetailPrint.createDestination(Activity)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 935, column 40
    function locationRef_247 (ClaimAssociation :  ClaimAssociation) : pcf.api.Destination {
      return pcf.ClaimAssociationDetail.createDestination(Claim, ClaimAssociation)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 940, column 32
    function locationRef_248 (Exposure :  Exposure) : pcf.api.Destination {
      return pcf.ExposureDetail.createDestination(Exposure)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 946, column 36
    function locationRef_249 (selectedClaimContact :  ClaimContact) : pcf.api.Destination {
      return pcf.ClaimContactPrintDetails.createDestination(Claim, selectedClaimContact)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 951, column 38
    function locationRef_250 (ClaimUserModel :  ClaimUserModel) : pcf.api.Destination {
      return pcf.ClaimUserPrintDetails.createDestination(Claim, ClaimUserModel)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 958, column 33
    function locationRef_251 (VehicleRU :  VehicleRU) : pcf.api.Destination {
      return pcf.ClaimPolicyVehicleDetail.createDestination(Claim, VehicleRU)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 965, column 38
    function locationRef_253 (PolicyLocation :  PolicyLocation) : pcf.api.Destination {
      return pcf.PolicyLocationPrint.createDestination(PolicyLocation, Claim)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 971, column 35
    function locationRef_255 (Endorsement :  Endorsement) : pcf.api.Destination {
      return pcf.ClaimPolicyEndorsementDetail.createDestination(Claim, Endorsement)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 977, column 32
    function locationRef_256 (StatCode :  StatCode) : pcf.api.Destination {
      return pcf.ClaimPolicyStatCodeDetail.createDestination(Claim, StatCode)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 983, column 38
    function locationRef_257 (AggregateLimit :  AggregateLimit) : pcf.api.Destination {
      return pcf.ClaimPolicyAggregateLimitDetail.createDestination(Claim, AggregateLimit)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 988, column 39
    function locationRef_258 (TransactionView :  TransactionView) : pcf.api.Destination {
      return pcf.TransactionDetailPrint.createDestination(Claim, TransactionView)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 993, column 33
    function locationRef_259 (CheckView :  CheckView) : pcf.api.Destination {
      return pcf.ClaimFinancialsChecksDetailPrint.createDestination(Claim, CheckView)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 998, column 32
    function locationRef_260 (DocumentParam :  Document) : pcf.api.Destination {
      return pcf.DocumentDetailsPrint.createDestination(Claim, DocumentParam)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 1004, column 34
    function locationRef_261 (Evaluation :  Evaluation) : pcf.api.Destination {
      return pcf.ClaimEvaluationPrintDetail.createDestination(Evaluation)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 1010, column 35
    function locationRef_262 (Negotiation :  Negotiation) : pcf.api.Destination {
      return pcf.ClaimNegotiationPrintDetail.createDestination(Negotiation)
    }
    
    // 'locationRef' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 1015, column 30
    function locationRef_263 (Matter :  Matter) : pcf.api.Destination {
      return pcf.MatterDetailPage.createDestination(Claim, Matter)
    }
    
    // 'printable' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 958, column 33
    function printable_252 () : java.lang.Boolean {
      return helper.isAutoClaim()
    }
    
    // 'printable' attribute on PrintLocationDetail at ClaimPrintout.pcf: line 965, column 38
    function printable_254 () : java.lang.Boolean {
      return canShowClaimPolicyLocations
    }
    
    // 'value' attribute on PrintOut (id=ClaimPrintout) at ClaimPrintout.pcf: line 53, column 89
    function value_88 () : java.lang.String {
      return Choice
    }
    
    property get Choice () : java.lang.String {
      return getVariableValue("Choice", 0) as java.lang.String
    }
    
    property set Choice ($arg :  java.lang.String) {
      setVariableValue("Choice", 0, $arg)
    }
    
    property get Claim () : Claim {
      return getVariableValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setVariableValue("Claim", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.ClaimPrintout {
      return super.CurrentLocation as pcf.ClaimPrintout
    }
    
    property get canShowClaimPolicyLocations () : boolean {
      return getVariableValue("canShowClaimPolicyLocations", 0) as java.lang.Boolean
    }
    
    property set canShowClaimPolicyLocations ($arg :  boolean) {
      setVariableValue("canShowClaimPolicyLocations", 0, $arg)
    }
    
    property get canShowClaimPolicyTrips () : boolean {
      return getVariableValue("canShowClaimPolicyTrips", 0) as java.lang.Boolean
    }
    
    property set canShowClaimPolicyTrips ($arg :  boolean) {
      setVariableValue("canShowClaimPolicyTrips", 0, $arg)
    }
    
    property get canShowPolicyStatCodes () : boolean {
      return getVariableValue("canShowPolicyStatCodes", 0) as java.lang.Boolean
    }
    
    property set canShowPolicyStatCodes ($arg :  boolean) {
      setVariableValue("canShowPolicyStatCodes", 0, $arg)
    }
    
    property get employerLiability () : Exposure {
      return getVariableValue("employerLiability", 0) as Exposure
    }
    
    property set employerLiability ($arg :  Exposure) {
      setVariableValue("employerLiability", 0, $arg)
    }
    
    property get helper () : gw.api.print.ClaimPrintoutHelper {
      return getVariableValue("helper", 0) as gw.api.print.ClaimPrintoutHelper
    }
    
    property set helper ($arg :  gw.api.print.ClaimPrintoutHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get indemityTimeLoss () : Exposure {
      return getVariableValue("indemityTimeLoss", 0) as Exposure
    }
    
    property set indemityTimeLoss ($arg :  Exposure) {
      setVariableValue("indemityTimeLoss", 0, $arg)
    }
    
    property get medicalDetails () : Exposure {
      return getVariableValue("medicalDetails", 0) as Exposure
    }
    
    property set medicalDetails ($arg :  Exposure) {
      setVariableValue("medicalDetails", 0, $arg)
    }
    
    property get printSettings () : gw.api.print.PrintSettings {
      return getVariableValue("printSettings", 0) as gw.api.print.PrintSettings
    }
    
    property set printSettings ($arg :  gw.api.print.PrintSettings) {
      setVariableValue("printSettings", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimSnapshot2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 877, column 54
    function locationRef_224 () : pcf.api.Destination {
      return pcf.ClaimSnapshotLossDetails.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 880, column 55
    function locationRef_226 () : pcf.api.Destination {
      return pcf.ClaimSnapshotPartiesInvolved.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 883, column 84
    function locationRef_228 () : pcf.api.Destination {
      return pcf.ClaimSnapshotPolicy.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 886, column 85
    function locationRef_230 () : pcf.api.Destination {
      return pcf.ClaimSnapshotExposures.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 889, column 53
    function locationRef_232 () : pcf.api.Destination {
      return pcf.ClaimSnapshotNotes.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 892, column 47
    function locationRef_234 () : pcf.api.Destination {
      return pcf.ClaimSnapshotDocuments.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 895, column 32
    function locationRef_235 () : pcf.api.Destination {
      return pcf.ClaimSnapshotExtraFields.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 877, column 54
    function printable_223 () : java.lang.Boolean {
      return perm.System.viewclaimbasics
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 880, column 55
    function printable_225 () : java.lang.Boolean {
      return perm.System.viewclaimparties
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 883, column 84
    function printable_227 () : java.lang.Boolean {
      return perm.Policy.view(Claim.Policy) and perm.System.viewpolicy
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 886, column 85
    function printable_229 () : java.lang.Boolean {
      return !helper.isWorkersCompClaim() and perm.System.viewexposures
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 889, column 53
    function printable_231 () : java.lang.Boolean {
      return perm.System.viewclaimnotes
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 892, column 47
    function printable_233 () : java.lang.Boolean {
      return perm.System.viewdocs
    }
    
    // 'printable' attribute on PrintSection (id=ClaimSnapshot2Section) at ClaimPrintout.pcf: line 871, column 52
    function printable_237 () : java.lang.Boolean {
      return helper.canPrintClaimSnapshot()
    }
    
    // PrintSection (id=ClaimSnapshot2Section) at ClaimPrintout.pcf: line 871, column 52
    function visible_236 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClaimSnapshot3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=ClaimSnapshot3Section) at ClaimPrintout.pcf: line 1838, column 77
    function defaultSetter_643 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on PrintSection (id=ClaimSnapshot3Section) at ClaimPrintout.pcf: line 1838, column 77
    function label_641 () : java.lang.Object {
      return gw.config.LOBAbstraction.ForClaim.ForLossType.isWorkComp(Claim) ? DisplayKey.get("Java.PrintClaimOptionsForm.Options.FirstReportOfInjurySnapshot") : DisplayKey.get("Java.PrintClaimOptionsForm.Options.FNOLSnapshot")
    }
    
    // 'label' attribute on PrintSection (id=ClaimSnapshot3Section) at ClaimPrintout.pcf: line 1838, column 77
    function label_647 () : java.lang.String {
      return gw.config.LOBAbstraction.ForClaim.ForLossType.isWorkComp(Claim) ? DisplayKey.get("Java.PrintClaimOptionsForm.Options.FirstReportOfInjurySnapshot") : DisplayKey.get("Java.PrintClaimOptionsForm.Options.FNOLSnapshot")
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1844, column 54
    function locationRef_626 () : pcf.api.Destination {
      return pcf.ClaimSnapshotLossDetails.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1847, column 55
    function locationRef_628 () : pcf.api.Destination {
      return pcf.ClaimSnapshotPartiesInvolved.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1850, column 84
    function locationRef_630 () : pcf.api.Destination {
      return pcf.ClaimSnapshotPolicy.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1853, column 85
    function locationRef_632 () : pcf.api.Destination {
      return pcf.ClaimSnapshotExposures.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1856, column 53
    function locationRef_634 () : pcf.api.Destination {
      return pcf.ClaimSnapshotNotes.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1859, column 47
    function locationRef_636 () : pcf.api.Destination {
      return pcf.ClaimSnapshotDocuments.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1862, column 32
    function locationRef_637 () : pcf.api.Destination {
      return pcf.ClaimSnapshotExtraFields.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1844, column 54
    function printable_625 () : java.lang.Boolean {
      return perm.System.viewclaimbasics
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1847, column 55
    function printable_627 () : java.lang.Boolean {
      return perm.System.viewclaimparties
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1850, column 84
    function printable_629 () : java.lang.Boolean {
      return perm.Policy.view(Claim.Policy) and perm.System.viewpolicy
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1853, column 85
    function printable_631 () : java.lang.Boolean {
      return !helper.isWorkersCompClaim() and perm.System.viewexposures
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1856, column 53
    function printable_633 () : java.lang.Boolean {
      return perm.System.viewclaimnotes
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1859, column 47
    function printable_635 () : java.lang.Boolean {
      return perm.System.viewdocs
    }
    
    // PrintSection (id=ClaimSnapshot3Section) at ClaimPrintout.pcf: line 1838, column 77
    function value_639 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=ClaimSnapshot3Section) at ClaimPrintout.pcf: line 1838, column 77
    function visible_638 () : java.lang.Boolean {
      return false // OOTB condition: helper.canPrintClaimSnapshot()
    }
    
    // 'childrenVisible' attribute on PrintSection (id=ClaimSnapshot3Section) at ClaimPrintout.pcf: line 1838, column 77
    function visible_640 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=ClaimSnapshot3Section) at ClaimPrintout.pcf: line 1838, column 77
    function visible_646 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Documents1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 322, column 61
    function locationRef_69 () : pcf.api.Destination {
      return pcf.ClaimDocumentsPrint.createDestination(Claim, true)
    }
    
    // 'printable' attribute on PrintSection (id=Documents1Section) at ClaimPrintout.pcf: line 317, column 42
    function printable_71 () : java.lang.Boolean {
      return perm.System.viewdocs
    }
    
    // PrintSection (id=Documents1Section) at ClaimPrintout.pcf: line 317, column 42
    function visible_70 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Documents2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 799, column 61
    function locationRef_205 () : pcf.api.Destination {
      return pcf.ClaimDocumentsPrint.createDestination(Claim, true)
    }
    
    // 'printable' attribute on PrintSection (id=Documents2Section) at ClaimPrintout.pcf: line 794, column 42
    function printable_207 () : java.lang.Boolean {
      return perm.System.viewdocs
    }
    
    // PrintSection (id=Documents2Section) at ClaimPrintout.pcf: line 794, column 42
    function visible_206 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Documents3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Documents3Section) at ClaimPrintout.pcf: line 1736, column 42
    function defaultSetter_579 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1741, column 61
    function locationRef_573 () : pcf.api.Destination {
      return pcf.ClaimDocumentsPrint.createDestination(Claim, true)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1747, column 62
    function locationRef_574 () : pcf.api.Destination {
      return pcf.ClaimDocumentsPrint.createDestination(Claim, false)
    }
    
    // PrintSection (id=Documents3Section) at ClaimPrintout.pcf: line 1736, column 42
    function value_576 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Documents3Section) at ClaimPrintout.pcf: line 1736, column 42
    function visible_575 () : java.lang.Boolean {
      return perm.System.viewdocs
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Documents3Section) at ClaimPrintout.pcf: line 1736, column 42
    function visible_577 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Documents3Section) at ClaimPrintout.pcf: line 1736, column 42
    function visible_581 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Evaluations1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 334, column 52
    function locationRef_72 () : pcf.api.Destination {
      return pcf.ClaimEvaluations.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Evaluations1Section) at ClaimPrintout.pcf: line 329, column 48
    function printable_74 () : java.lang.Boolean {
      return perm.System.viewclaimevals
    }
    
    // PrintSection (id=Evaluations1Section) at ClaimPrintout.pcf: line 329, column 48
    function visible_73 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Evaluations2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 816, column 40
    function locationRef_208 (Evaluation :  Evaluation) : pcf.api.Destination {
      return pcf.ClaimEvaluationPrintDetail.createDestination(Evaluation)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 812, column 51
    function locationRef_209 () : pcf.api.Destination {
      return pcf.ClaimEvaluations.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Evaluations2Section) at ClaimPrintout.pcf: line 806, column 48
    function printable_211 () : java.lang.Boolean {
      return perm.System.viewclaimevals
    }
    
    // PrintSection (id=Evaluations2Section) at ClaimPrintout.pcf: line 806, column 48
    function visible_210 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Evaluations3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Evaluations3Section) at ClaimPrintout.pcf: line 1754, column 48
    function defaultSetter_590 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1759, column 52
    function locationRef_583 () : pcf.api.Destination {
      return pcf.ClaimEvaluations.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1770, column 40
    function locationRef_584 (Evaluation :  Evaluation) : pcf.api.Destination {
      return pcf.ClaimEvaluationPrintDetail.createDestination(Evaluation)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1766, column 51
    function locationRef_585 () : pcf.api.Destination {
      return pcf.ClaimEvaluations.createDestination(Claim)
    }
    
    // PrintSection (id=Evaluations3Section) at ClaimPrintout.pcf: line 1754, column 48
    function value_587 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Evaluations3Section) at ClaimPrintout.pcf: line 1754, column 48
    function visible_586 () : java.lang.Boolean {
      return perm.System.viewclaimevals
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Evaluations3Section) at ClaimPrintout.pcf: line 1754, column 48
    function visible_588 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Evaluations3Section) at ClaimPrintout.pcf: line 1754, column 48
    function visible_592 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Exposures1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 222, column 50
    function locationRef_41 () : pcf.api.Destination {
      return pcf.ClaimExposures.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Exposures1Section) at ClaimPrintout.pcf: line 217, column 80
    function printable_43 () : java.lang.Boolean {
      return !helper.isWorkersCompClaim() and perm.System.viewexposures
    }
    
    // PrintSection (id=Exposures1Section) at ClaimPrintout.pcf: line 217, column 80
    function visible_42 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Exposures2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 630, column 38
    function locationRef_156 (Exposure :  Exposure) : pcf.api.Destination {
      return pcf.ExposureDetail.createDestination(Exposure)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 626, column 49
    function locationRef_157 () : pcf.api.Destination {
      return pcf.ClaimExposures.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Exposures2Section) at ClaimPrintout.pcf: line 621, column 80
    function printable_159 () : java.lang.Boolean {
      return !helper.isWorkersCompClaim() and perm.System.viewexposures
    }
    
    // PrintSection (id=Exposures2Section) at ClaimPrintout.pcf: line 621, column 80
    function visible_158 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Exposures3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Exposures3Section) at ClaimPrintout.pcf: line 1289, column 103
    function defaultSetter_427 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1294, column 50
    function locationRef_420 () : pcf.api.Destination {
      return pcf.ClaimExposures.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1304, column 38
    function locationRef_421 (Exposure :  Exposure) : pcf.api.Destination {
      return pcf.ExposureDetail.createDestination(Exposure)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1300, column 49
    function locationRef_422 () : pcf.api.Destination {
      return pcf.ClaimExposures.createDestination(Claim)
    }
    
    // PrintSection (id=Exposures3Section) at ClaimPrintout.pcf: line 1289, column 103
    function value_424 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Exposures3Section) at ClaimPrintout.pcf: line 1289, column 103
    function visible_423 () : java.lang.Boolean {
      return false// OOTB condition:!helper.isWorkersCompClaim() and perm.System.viewexposures
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Exposures3Section) at ClaimPrintout.pcf: line 1289, column 103
    function visible_425 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Exposures3Section) at ClaimPrintout.pcf: line 1289, column 103
    function visible_429 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FinancialsSummary1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 259, column 63
    function locationRef_50 () : pcf.api.Destination {
      return pcf.FinancialsSummaryTitlePrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 261, column 73
    function locationRef_51 () : pcf.api.Destination {
      return pcf.FinancialsSummaryExposurePrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 263, column 77
    function locationRef_52 () : pcf.api.Destination {
      return pcf.FinancialsSummaryExposureOnlyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 265, column 73
    function locationRef_53 () : pcf.api.Destination {
      return pcf.FinancialsSummaryClaimantPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 267, column 73
    function locationRef_54 () : pcf.api.Destination {
      return pcf.FinancialsSummaryCoveragePrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 269, column 78
    function locationRef_55 () : pcf.api.Destination {
      return pcf.FinancialsSummaryClaimCostOnlyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 271, column 82
    function locationRef_56 () : pcf.api.Destination {
      return pcf.FinancialsSummaryReservingCurrencyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 273, column 81
    function locationRef_57 () : pcf.api.Destination {
      return pcf.FinancialsSummaryRecoveryCategoryPrint.createDestination(Claim, false)
    }
    
    // 'printable' attribute on PrintSection (id=FinancialsSummary1Section) at ClaimPrintout.pcf: line 254, column 44
    function printable_59 () : java.lang.Boolean {
      return perm.System.viewfinsum
    }
    
    // PrintSection (id=FinancialsSummary1Section) at ClaimPrintout.pcf: line 254, column 44
    function visible_58 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FinancialsSummary2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 727, column 63
    function locationRef_184 () : pcf.api.Destination {
      return pcf.FinancialsSummaryTitlePrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 729, column 73
    function locationRef_185 () : pcf.api.Destination {
      return pcf.FinancialsSummaryExposurePrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 731, column 77
    function locationRef_186 () : pcf.api.Destination {
      return pcf.FinancialsSummaryExposureOnlyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 733, column 73
    function locationRef_187 () : pcf.api.Destination {
      return pcf.FinancialsSummaryClaimantPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 735, column 73
    function locationRef_188 () : pcf.api.Destination {
      return pcf.FinancialsSummaryCoveragePrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 737, column 78
    function locationRef_189 () : pcf.api.Destination {
      return pcf.FinancialsSummaryClaimCostOnlyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 739, column 82
    function locationRef_190 () : pcf.api.Destination {
      return pcf.FinancialsSummaryReservingCurrencyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 741, column 81
    function locationRef_191 () : pcf.api.Destination {
      return pcf.FinancialsSummaryRecoveryCategoryPrint.createDestination(Claim, false)
    }
    
    // 'printable' attribute on PrintSection (id=FinancialsSummary2Section) at ClaimPrintout.pcf: line 722, column 44
    function printable_193 () : java.lang.Boolean {
      return perm.System.viewfinsum
    }
    
    // PrintSection (id=FinancialsSummary2Section) at ClaimPrintout.pcf: line 722, column 44
    function visible_192 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FinancialsSummary3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=FinancialsSummary3Section) at ClaimPrintout.pcf: line 1509, column 44
    function defaultSetter_517 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1514, column 63
    function locationRef_491 () : pcf.api.Destination {
      return pcf.FinancialsSummaryTitlePrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1516, column 73
    function locationRef_492 () : pcf.api.Destination {
      return pcf.FinancialsSummaryExposurePrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1518, column 77
    function locationRef_493 () : pcf.api.Destination {
      return pcf.FinancialsSummaryExposureOnlyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1520, column 73
    function locationRef_494 () : pcf.api.Destination {
      return pcf.FinancialsSummaryClaimantPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1522, column 73
    function locationRef_495 () : pcf.api.Destination {
      return pcf.FinancialsSummaryCoveragePrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1524, column 78
    function locationRef_496 () : pcf.api.Destination {
      return pcf.FinancialsSummaryClaimCostOnlyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1526, column 82
    function locationRef_497 () : pcf.api.Destination {
      return pcf.FinancialsSummaryReservingCurrencyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1528, column 81
    function locationRef_498 () : pcf.api.Destination {
      return pcf.FinancialsSummaryRecoveryCategoryPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1534, column 63
    function locationRef_499 () : pcf.api.Destination {
      return pcf.FinancialsSummaryTitlePrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1536, column 73
    function locationRef_500 () : pcf.api.Destination {
      return pcf.FinancialsSummaryExposurePrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1542, column 63
    function locationRef_501 () : pcf.api.Destination {
      return pcf.FinancialsSummaryTitlePrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1544, column 77
    function locationRef_502 () : pcf.api.Destination {
      return pcf.FinancialsSummaryExposureOnlyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1550, column 63
    function locationRef_503 () : pcf.api.Destination {
      return pcf.FinancialsSummaryTitlePrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1552, column 73
    function locationRef_504 () : pcf.api.Destination {
      return pcf.FinancialsSummaryClaimantPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1558, column 63
    function locationRef_505 () : pcf.api.Destination {
      return pcf.FinancialsSummaryTitlePrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1560, column 73
    function locationRef_506 () : pcf.api.Destination {
      return pcf.FinancialsSummaryCoveragePrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1566, column 63
    function locationRef_507 () : pcf.api.Destination {
      return pcf.FinancialsSummaryTitlePrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1568, column 78
    function locationRef_508 () : pcf.api.Destination {
      return pcf.FinancialsSummaryClaimCostOnlyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1574, column 63
    function locationRef_509 () : pcf.api.Destination {
      return pcf.FinancialsSummaryTitlePrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1576, column 82
    function locationRef_510 () : pcf.api.Destination {
      return pcf.FinancialsSummaryReservingCurrencyPrint.createDestination(Claim, false)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1582, column 63
    function locationRef_511 () : pcf.api.Destination {
      return pcf.FinancialsSummaryTitlePrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1584, column 81
    function locationRef_512 () : pcf.api.Destination {
      return pcf.FinancialsSummaryRecoveryCategoryPrint.createDestination(Claim, false)
    }
    
    // PrintSection (id=FinancialsSummary3Section) at ClaimPrintout.pcf: line 1509, column 44
    function value_514 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=FinancialsSummary3Section) at ClaimPrintout.pcf: line 1509, column 44
    function visible_513 () : java.lang.Boolean {
      return perm.System.viewfinsum
    }
    
    // 'childrenVisible' attribute on PrintSection (id=FinancialsSummary3Section) at ClaimPrintout.pcf: line 1509, column 44
    function visible_515 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=FinancialsSummary3Section) at ClaimPrintout.pcf: line 1509, column 44
    function visible_519 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FinancialsTransaction1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 286, column 125
    function locationRef_60 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.ALL)
    }
    
    // 'printable' attribute on PrintSection (id=FinancialsTransaction1Section) at ClaimPrintout.pcf: line 280, column 62
    function printable_62 () : java.lang.Boolean {
      return perm.Claim.viewtransactiondetails(Claim)
    }
    
    // PrintSection (id=FinancialsTransaction1Section) at ClaimPrintout.pcf: line 280, column 62
    function visible_61 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FinancialsTransaction2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 757, column 45
    function locationRef_194 (TransactionView :  TransactionView) : pcf.api.Destination {
      return pcf.TransactionDetailPrint.createDestination(Claim, TransactionView)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 753, column 124
    function locationRef_195 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.ALL)
    }
    
    // 'printable' attribute on PrintSection (id=FinancialsTransaction2Section) at ClaimPrintout.pcf: line 748, column 62
    function printable_197 () : java.lang.Boolean {
      return perm.Claim.viewtransactiondetails(Claim)
    }
    
    // PrintSection (id=FinancialsTransaction2Section) at ClaimPrintout.pcf: line 748, column 62
    function visible_196 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FinancialsTransactionSectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=FinancialsTransactionSection) at ClaimPrintout.pcf: line 1591, column 62
    function defaultSetter_548 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1597, column 125
    function locationRef_521 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.ALL)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1607, column 45
    function locationRef_522 (TransactionView :  TransactionView) : pcf.api.Destination {
      return pcf.TransactionDetailPrint.createDestination(Claim, TransactionView)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1603, column 124
    function locationRef_523 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.ALL)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1616, column 129
    function locationRef_524 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.RESERVE)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1627, column 45
    function locationRef_526 (TransactionView :  TransactionView) : pcf.api.Destination {
      return pcf.TransactionDetailPrint.createDestination(Claim, TransactionView)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1623, column 128
    function locationRef_527 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.RESERVE)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1636, column 129
    function locationRef_529 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.PAYMENT)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1647, column 45
    function locationRef_531 (TransactionView :  TransactionView) : pcf.api.Destination {
      return pcf.TransactionDetailPrint.createDestination(Claim, TransactionView)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1643, column 128
    function locationRef_532 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.PAYMENT)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1656, column 130
    function locationRef_534 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.RECOVERY)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1667, column 45
    function locationRef_536 (TransactionView :  TransactionView) : pcf.api.Destination {
      return pcf.TransactionDetailPrint.createDestination(Claim, TransactionView)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1663, column 129
    function locationRef_537 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.RECOVERY)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1676, column 138
    function locationRef_539 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.RECOVERY_RESERVE)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1687, column 45
    function locationRef_541 (TransactionView :  TransactionView) : pcf.api.Destination {
      return pcf.TransactionDetailPrint.createDestination(Claim, TransactionView)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1683, column 137
    function locationRef_542 () : pcf.api.Destination {
      return pcf.ClaimFinancialsTransactionsPrint.createDestination(Claim, gw.api.financials.ClaimFinancialsTransactionsOption.RECOVERY_RESERVE)
    }
    
    // 'printable' attribute on PrintOption (id=FinancialsTransactionReservesWithoutDetailsOption) at ClaimPrintout.pcf: line 1614, column 54
    function printable_525 () : java.lang.Boolean {
      return perm.Claim.viewreserves(Claim)
    }
    
    // 'printable' attribute on PrintOption (id=FinancialsTransactionPaymentsWithoutDetailsOption) at ClaimPrintout.pcf: line 1634, column 54
    function printable_530 () : java.lang.Boolean {
      return perm.Claim.viewpayments(Claim)
    }
    
    // 'printable' attribute on PrintOption (id=FinancialsTransactionRecoveriesWithoutDetailsOption) at ClaimPrintout.pcf: line 1654, column 56
    function printable_535 () : java.lang.Boolean {
      return perm.Claim.viewrecoveries(Claim)
    }
    
    // 'printable' attribute on PrintOption (id=FinancialsTransactionRecoveryReservesWithoutDetailsOption) at ClaimPrintout.pcf: line 1674, column 119
    function printable_540 () : java.lang.Boolean {
      return perm.Claim.viewrecoveryreserves(Claim) and gw.api.print.ClaimPrintoutHelper.UseRecoveryReserves
    }
    
    // PrintSection (id=FinancialsTransactionSection) at ClaimPrintout.pcf: line 1591, column 62
    function value_545 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=FinancialsTransactionSection) at ClaimPrintout.pcf: line 1591, column 62
    function visible_544 () : java.lang.Boolean {
      return perm.Claim.viewtransactiondetails(Claim)
    }
    
    // 'childrenVisible' attribute on PrintSection (id=FinancialsTransactionSection) at ClaimPrintout.pcf: line 1591, column 62
    function visible_546 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=FinancialsTransactionSection) at ClaimPrintout.pcf: line 1591, column 62
    function visible_550 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class History1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'filter' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 386, column 48
    function filter_84 (VALUE :  gw.entity.TypeKey) : gw.api.filters.IFilter {
      return gw.api.util.CoreFilters.ALL
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 386, column 48
    function locationRef_85 () : pcf.api.Destination {
      return pcf.ClaimHistory.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=History1Section) at ClaimPrintout.pcf: line 379, column 50
    function printable_87 () : java.lang.Boolean {
      return perm.System.viewclaimhistory
    }
    
    // PrintSection (id=History1Section) at ClaimPrintout.pcf: line 379, column 50
    function visible_86 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class History2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'filter' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 910, column 48
    function filter_238 (VALUE :  gw.entity.TypeKey) : gw.api.filters.IFilter {
      return gw.api.util.CoreFilters.ALL
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 910, column 48
    function locationRef_239 () : pcf.api.Destination {
      return pcf.ClaimHistory.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=History2Section) at ClaimPrintout.pcf: line 903, column 50
    function printable_241 () : java.lang.Boolean {
      return perm.System.viewclaimhistory
    }
    
    // PrintSection (id=History2Section) at ClaimPrintout.pcf: line 903, column 50
    function visible_240 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class History3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=History3Section) at ClaimPrintout.pcf: line 1870, column 50
    function defaultSetter_657 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'filter' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1877, column 48
    function filter_649 (VALUE :  gw.entity.TypeKey) : gw.api.filters.IFilter {
      return gw.api.util.CoreFilters.ALL
    }
    
    // 'filter' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1885, column 48
    function filter_651 (VALUE :  HistoryType) : gw.api.filters.IFilter {
      return new gw.api.filters.TypeKeyFilter(VALUE, History#Type.PropertyInfo as gw.entity.ITypekeyPropertyInfo)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1877, column 48
    function locationRef_650 () : pcf.api.Destination {
      return pcf.ClaimHistory.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1885, column 48
    function locationRef_652 () : pcf.api.Destination {
      return pcf.ClaimHistory.createDestination(Claim)
    }
    
    // PrintSection (id=History3Section) at ClaimPrintout.pcf: line 1870, column 50
    function value_654 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=History3Section) at ClaimPrintout.pcf: line 1870, column 50
    function visible_653 () : java.lang.Boolean {
      return perm.System.viewclaimhistory
    }
    
    // 'childrenVisible' attribute on PrintSection (id=History3Section) at ClaimPrintout.pcf: line 1870, column 50
    function visible_655 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=History3Section) at ClaimPrintout.pcf: line 1870, column 50
    function visible_659 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class HomeownerPropertyIncident2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 510, column 106
    function locationRef_119 () : pcf.api.Destination {
      return pcf.DwellingIncidentPrint.createDestination(Claim, helper.DwellingIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 514, column 122
    function locationRef_121 () : pcf.api.Destination {
      return pcf.PropertyContentsIncidentPrint.createDestination(Claim, helper.PersonalPropertyIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 518, column 118
    function locationRef_123 () : pcf.api.Destination {
      return pcf.OtherStructureIncidentPrint.createDestination(Claim, helper.OtherStructureIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 522, column 118
    function locationRef_125 () : pcf.api.Destination {
      return pcf.LivingExpensesIncidentPrint.createDestination(Claim, helper.LivingExpensesIncident)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 510, column 106
    function printable_118 () : java.lang.Boolean {
      return helper.DwellingIncident != null and perm.Incident.view(helper.DwellingIncident)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 514, column 122
    function printable_120 () : java.lang.Boolean {
      return helper.PersonalPropertyIncident != null and perm.Incident.view(helper.PersonalPropertyIncident)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 518, column 118
    function printable_122 () : java.lang.Boolean {
      return helper.OtherStructureIncident != null and perm.Incident.view(helper.OtherStructureIncident)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 522, column 118
    function printable_124 () : java.lang.Boolean {
      return helper.LivingExpensesIncident != null and perm.Incident.view(helper.LivingExpensesIncident)
    }
    
    // 'printable' attribute on PrintOption (id=HomeownerIncidentOption) at ClaimPrintout.pcf: line 506, column 50
    function printable_126 () : java.lang.Boolean {
      return helper.isHomeownersClaim()
    }
    
    // PrintSection (id=HomeownerPropertyIncident2Section) at ClaimPrintout.pcf: line 502, column 48
    function visible_127 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class HomeownerPropertyIncident3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=HomeownerPropertyIncident3Section) at ClaimPrintout.pcf: line 1164, column 48
    function defaultSetter_346 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1172, column 106
    function locationRef_334 () : pcf.api.Destination {
      return pcf.DwellingIncidentPrint.createDestination(Claim, helper.DwellingIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1176, column 122
    function locationRef_336 () : pcf.api.Destination {
      return pcf.PropertyContentsIncidentPrint.createDestination(Claim, helper.PersonalPropertyIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1180, column 118
    function locationRef_338 () : pcf.api.Destination {
      return pcf.OtherStructureIncidentPrint.createDestination(Claim, helper.OtherStructureIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1184, column 118
    function locationRef_340 () : pcf.api.Destination {
      return pcf.LivingExpensesIncidentPrint.createDestination(Claim, helper.LivingExpensesIncident)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1172, column 106
    function printable_333 () : java.lang.Boolean {
      return helper.DwellingIncident != null and perm.Incident.view(helper.DwellingIncident)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1176, column 122
    function printable_335 () : java.lang.Boolean {
      return helper.PersonalPropertyIncident != null and perm.Incident.view(helper.PersonalPropertyIncident)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1180, column 118
    function printable_337 () : java.lang.Boolean {
      return helper.OtherStructureIncident != null and perm.Incident.view(helper.OtherStructureIncident)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1184, column 118
    function printable_339 () : java.lang.Boolean {
      return helper.LivingExpensesIncident != null and perm.Incident.view(helper.LivingExpensesIncident)
    }
    
    // 'printable' attribute on PrintOption (id=HomeownerIncidentOption) at ClaimPrintout.pcf: line 1168, column 50
    function printable_341 () : java.lang.Boolean {
      return helper.isHomeownersClaim()
    }
    
    // PrintSection (id=HomeownerPropertyIncident3Section) at ClaimPrintout.pcf: line 1164, column 48
    function value_343 () : java.lang.Object {
      return null
    }
    
    // 'childrenVisible' attribute on PrintSection (id=HomeownerPropertyIncident3Section) at ClaimPrintout.pcf: line 1164, column 48
    function visible_344 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=HomeownerPropertyIncident3Section) at ClaimPrintout.pcf: line 1164, column 48
    function visible_348 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InjuryIncident2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 493, column 44
    function locationRef_114 (InjuryIncident :  InjuryIncident) : pcf.api.Destination {
      return pcf.InjuryIncidentDetailPrint.createDestination(InjuryIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 489, column 57
    function locationRef_116 () : pcf.api.Destination {
      return pcf.InjuryIncidentsPrint.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 489, column 57
    function printable_115 () : java.lang.Boolean {
      return !helper.InjuryIncidents.IsEmpty
    }
    
    // PrintSection (id=InjuryIncident2Section) at ClaimPrintout.pcf: line 483, column 106
    function visible_117 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InjuryIncident3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=InjuryIncident3Section) at ClaimPrintout.pcf: line 1145, column 106
    function defaultSetter_331 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1155, column 44
    function locationRef_325 (InjuryIncident :  InjuryIncident) : pcf.api.Destination {
      return pcf.InjuryIncidentDetailPrint.createDestination(InjuryIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1151, column 57
    function locationRef_327 () : pcf.api.Destination {
      return pcf.InjuryIncidentsPrint.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1151, column 57
    function printable_326 () : java.lang.Boolean {
      return !helper.InjuryIncidents.IsEmpty
    }
    
    // PrintSection (id=InjuryIncident3Section) at ClaimPrintout.pcf: line 1145, column 106
    function value_328 () : java.lang.Object {
      return null
    }
    
    // 'childrenVisible' attribute on PrintSection (id=InjuryIncident3Section) at ClaimPrintout.pcf: line 1145, column 106
    function visible_329 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=InjuryIncident3Section) at ClaimPrintout.pcf: line 1145, column 106
    function visible_332 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Litigation1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 371, column 48
    function locationRef_81 () : pcf.api.Destination {
      return pcf.ClaimMatters.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Litigation1Section) at ClaimPrintout.pcf: line 366, column 45
    function printable_83 () : java.lang.Boolean {
      return perm.System.viewmatters
    }
    
    // PrintSection (id=Litigation1Section) at ClaimPrintout.pcf: line 366, column 45
    function visible_82 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Litigation2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 863, column 36
    function locationRef_219 (Matter :  Matter) : pcf.api.Destination {
      return pcf.MatterDetailPage.createDestination(Claim, Matter)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 859, column 47
    function locationRef_220 () : pcf.api.Destination {
      return pcf.ClaimMatters.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Litigation2Section) at ClaimPrintout.pcf: line 854, column 45
    function printable_222 () : java.lang.Boolean {
      return perm.System.viewmatters
    }
    
    // PrintSection (id=Litigation2Section) at ClaimPrintout.pcf: line 854, column 45
    function visible_221 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Litigation3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Litigation3Section) at ClaimPrintout.pcf: line 1815, column 45
    function defaultSetter_621 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1820, column 48
    function locationRef_614 () : pcf.api.Destination {
      return pcf.ClaimMatters.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1830, column 36
    function locationRef_615 (Matter :  Matter) : pcf.api.Destination {
      return pcf.MatterDetailPage.createDestination(Claim, Matter)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1826, column 47
    function locationRef_616 () : pcf.api.Destination {
      return pcf.ClaimMatters.createDestination(Claim)
    }
    
    // PrintSection (id=Litigation3Section) at ClaimPrintout.pcf: line 1815, column 45
    function value_618 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Litigation3Section) at ClaimPrintout.pcf: line 1815, column 45
    function visible_617 () : java.lang.Boolean {
      return perm.System.viewmatters
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Litigation3Section) at ClaimPrintout.pcf: line 1815, column 45
    function visible_619 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Litigation3Section) at ClaimPrintout.pcf: line 1815, column 45
    function visible_623 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LossDetails1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 149, column 52
    function locationRef_20 () : pcf.api.Destination {
      return pcf.ClaimLossDetails.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=LossDetails1Section) at ClaimPrintout.pcf: line 144, column 49
    function printable_22 () : java.lang.Boolean {
      return perm.System.viewclaimbasics
    }
    
    // PrintSection (id=LossDetails1Section) at ClaimPrintout.pcf: line 144, column 49
    function visible_21 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LossDetails2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 440, column 52
    function locationRef_102 () : pcf.api.Destination {
      return pcf.ClaimLossDetails.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=LossDetails2Section) at ClaimPrintout.pcf: line 435, column 49
    function printable_104 () : java.lang.Boolean {
      return perm.System.viewclaimbasics
    }
    
    // PrintSection (id=LossDetails2Section) at ClaimPrintout.pcf: line 435, column 49
    function visible_103 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LossDetails3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=LossDetails3Section) at ClaimPrintout.pcf: line 1096, column 49
    function defaultSetter_300 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1101, column 52
    function locationRef_295 () : pcf.api.Destination {
      return pcf.ClaimLossDetails.createDestination(Claim)
    }
    
    // PrintSection (id=LossDetails3Section) at ClaimPrintout.pcf: line 1096, column 49
    function value_297 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=LossDetails3Section) at ClaimPrintout.pcf: line 1096, column 49
    function visible_296 () : java.lang.Boolean {
      return perm.System.viewclaimbasics
    }
    
    // 'childrenVisible' attribute on PrintSection (id=LossDetails3Section) at ClaimPrintout.pcf: line 1096, column 49
    function visible_298 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=LossDetails3Section) at ClaimPrintout.pcf: line 1096, column 49
    function visible_302 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Negotiations1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 347, column 53
    function locationRef_75 () : pcf.api.Destination {
      return pcf.ClaimNegotiations.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Negotiations1Section) at ClaimPrintout.pcf: line 341, column 48
    function printable_77 () : java.lang.Boolean {
      return perm.System.viewclaimngtns
    }
    
    // PrintSection (id=Negotiations1Section) at ClaimPrintout.pcf: line 341, column 48
    function visible_76 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Negotiations2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 834, column 41
    function locationRef_212 (Negotiation :  Negotiation) : pcf.api.Destination {
      return pcf.ClaimNegotiationPrintDetail.createDestination(Negotiation)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 830, column 52
    function locationRef_213 () : pcf.api.Destination {
      return pcf.ClaimNegotiations.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Negotiations2Section) at ClaimPrintout.pcf: line 824, column 48
    function printable_215 () : java.lang.Boolean {
      return perm.System.viewclaimngtns
    }
    
    // PrintSection (id=Negotiations2Section) at ClaimPrintout.pcf: line 824, column 48
    function visible_214 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Negotiations3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Negotiations3Section) at ClaimPrintout.pcf: line 1778, column 48
    function defaultSetter_601 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1784, column 53
    function locationRef_594 () : pcf.api.Destination {
      return pcf.ClaimNegotiations.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1795, column 41
    function locationRef_595 (Negotiation :  Negotiation) : pcf.api.Destination {
      return pcf.ClaimNegotiationPrintDetail.createDestination(Negotiation)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1791, column 52
    function locationRef_596 () : pcf.api.Destination {
      return pcf.ClaimNegotiations.createDestination(Claim)
    }
    
    // PrintSection (id=Negotiations3Section) at ClaimPrintout.pcf: line 1778, column 48
    function value_598 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Negotiations3Section) at ClaimPrintout.pcf: line 1778, column 48
    function visible_597 () : java.lang.Boolean {
      return perm.System.viewclaimngtns
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Negotiations3Section) at ClaimPrintout.pcf: line 1778, column 48
    function visible_599 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Negotiations3Section) at ClaimPrintout.pcf: line 1778, column 48
    function visible_603 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Notes1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 310, column 54
    function locationRef_66 () : pcf.api.Destination {
      return pcf.ClaimAllNotesPrint.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Notes1Section) at ClaimPrintout.pcf: line 305, column 48
    function printable_68 () : java.lang.Boolean {
      return perm.System.viewclaimnotes
    }
    
    // PrintSection (id=Notes1Section) at ClaimPrintout.pcf: line 305, column 48
    function visible_67 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Notes2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 787, column 54
    function locationRef_202 () : pcf.api.Destination {
      return pcf.ClaimAllNotesPrint.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Notes2Section) at ClaimPrintout.pcf: line 782, column 48
    function printable_204 () : java.lang.Boolean {
      return perm.System.viewclaimnotes
    }
    
    // PrintSection (id=Notes2Section) at ClaimPrintout.pcf: line 782, column 48
    function visible_203 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Notes3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Notes3Section) at ClaimPrintout.pcf: line 1718, column 48
    function defaultSetter_569 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1723, column 65
    function locationRef_563 () : pcf.api.Destination {
      return pcf.ClaimNoConfidentialNotesPrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1729, column 54
    function locationRef_564 () : pcf.api.Destination {
      return pcf.ClaimAllNotesPrint.createDestination(Claim)
    }
    
    // PrintSection (id=Notes3Section) at ClaimPrintout.pcf: line 1718, column 48
    function value_566 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Notes3Section) at ClaimPrintout.pcf: line 1718, column 48
    function visible_565 () : java.lang.Boolean {
      return perm.System.viewclaimnotes
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Notes3Section) at ClaimPrintout.pcf: line 1718, column 48
    function visible_567 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Notes3Section) at ClaimPrintout.pcf: line 1718, column 48
    function visible_571 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PartiesInvolved1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 234, column 49
    function locationRef_44 () : pcf.api.Destination {
      return pcf.ClaimContacts.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=PartiesInvolved1Section) at ClaimPrintout.pcf: line 229, column 50
    function printable_46 () : java.lang.Boolean {
      return perm.System.viewclaimparties
    }
    
    // PrintSection (id=PartiesInvolved1Section) at ClaimPrintout.pcf: line 229, column 50
    function visible_45 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PartiesInvolved2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 648, column 49
    function locationRef_160 (selectedClaimContact :  entity.ClaimContact) : pcf.api.Destination {
      return pcf.ClaimContactPrintDetails.createDestination(Claim, selectedClaimContact) 
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 644, column 48
    function locationRef_161 () : pcf.api.Destination {
      return pcf.ClaimContacts.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=PartiesInvolved2Section) at ClaimPrintout.pcf: line 638, column 50
    function printable_163 () : java.lang.Boolean {
      return perm.System.viewclaimparties
    }
    
    // PrintSection (id=PartiesInvolved2Section) at ClaimPrintout.pcf: line 638, column 50
    function visible_162 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PartiesInvolved3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=PartiesInvolved3Section) at ClaimPrintout.pcf: line 1312, column 50
    function defaultSetter_438 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1317, column 49
    function locationRef_431 () : pcf.api.Destination {
      return pcf.ClaimContacts.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1328, column 42
    function locationRef_432 (selectedClaimContact :  ClaimContact) : pcf.api.Destination {
      return pcf.ClaimContactPrintDetails.createDestination(Claim, selectedClaimContact)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1324, column 48
    function locationRef_433 () : pcf.api.Destination {
      return pcf.ClaimContacts.createDestination(Claim)
    }
    
    // PrintSection (id=PartiesInvolved3Section) at ClaimPrintout.pcf: line 1312, column 50
    function value_435 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=PartiesInvolved3Section) at ClaimPrintout.pcf: line 1312, column 50
    function visible_434 () : java.lang.Boolean {
      return perm.System.viewclaimparties
    }
    
    // 'childrenVisible' attribute on PrintSection (id=PartiesInvolved3Section) at ClaimPrintout.pcf: line 1312, column 50
    function visible_436 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=PartiesInvolved3Section) at ClaimPrintout.pcf: line 1312, column 50
    function visible_440 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Policy1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 247, column 54
    function locationRef_47 () : pcf.api.Destination {
      return pcf.ClaimPolicyGeneral.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Policy1Section) at ClaimPrintout.pcf: line 241, column 44
    function printable_49 () : java.lang.Boolean {
      return perm.System.viewpolicy
    }
    
    // PrintSection (id=Policy1Section) at ClaimPrintout.pcf: line 241, column 44
    function visible_48 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Policy2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 662, column 54
    function locationRef_164 () : pcf.api.Destination {
      return pcf.ClaimPolicyGeneral.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 670, column 39
    function locationRef_165 (VehicleRU :  VehicleRU) : pcf.api.Destination {
      return pcf.ClaimPolicyVehicleDetail.createDestination(Claim, VehicleRU)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 666, column 46
    function locationRef_167 () : pcf.api.Destination {
      return pcf.ClaimPolicyVehicles.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 679, column 44
    function locationRef_168 (PolicyLocation :  PolicyLocation) : pcf.api.Destination {
      return pcf.PolicyLocationPrint.createDestination(PolicyLocation, Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 675, column 53
    function locationRef_170 () : pcf.api.Destination {
      return pcf.ClaimPolicyLocations.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 688, column 36
    function locationRef_171 (aTripRU :  TripRU) : pcf.api.Destination {
      return pcf.TripRUPrint.createDestination(Claim, aTripRU)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 684, column 49
    function locationRef_173 () : pcf.api.Destination {
      return pcf.ClaimPolicyTrips.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 696, column 41
    function locationRef_174 (Endorsement :  Endorsement) : pcf.api.Destination {
      return pcf.ClaimPolicyEndorsementDetail.createDestination(Claim, Endorsement)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 692, column 58
    function locationRef_175 () : pcf.api.Destination {
      return pcf.ClaimPolicyEndorsements.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 705, column 38
    function locationRef_176 (StatCode :  StatCode) : pcf.api.Destination {
      return pcf.ClaimPolicyStatCodeDetail.createDestination(Claim, StatCode)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 701, column 48
    function locationRef_178 () : pcf.api.Destination {
      return pcf.ClaimPolicyStatCodes.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 714, column 44
    function locationRef_179 (AggregateLimit :  AggregateLimit) : pcf.api.Destination {
      return pcf.ClaimPolicyAggregateLimitDetail.createDestination(Claim, AggregateLimit)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 710, column 151
    function locationRef_181 () : pcf.api.Destination {
      return pcf.ClaimPolicyAggregateLimits.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 666, column 46
    function printable_166 () : java.lang.Boolean {
      return helper.isAutoClaim()
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 675, column 53
    function printable_169 () : java.lang.Boolean {
      return canShowClaimPolicyLocations
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 684, column 49
    function printable_172 () : java.lang.Boolean {
      return canShowClaimPolicyTrips
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 701, column 48
    function printable_177 () : java.lang.Boolean {
      return canShowPolicyStatCodes
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 710, column 151
    function printable_180 () : java.lang.Boolean {
      return gw.api.policy.PolicyTabUtil.hasTab(Claim, "AggregateLimits") and perm.Policy.view(Claim) and perm.System.viewpolicy
    }
    
    // 'printable' attribute on PrintSection (id=Policy2Section) at ClaimPrintout.pcf: line 656, column 44
    function printable_183 () : java.lang.Boolean {
      return perm.System.viewpolicy
    }
    
    // PrintSection (id=Policy2Section) at ClaimPrintout.pcf: line 656, column 44
    function visible_182 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Policy3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Policy3Section) at ClaimPrintout.pcf: line 1336, column 44
    function defaultSetter_487 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1342, column 54
    function locationRef_442 () : pcf.api.Destination {
      return pcf.ClaimPolicyGeneral.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1356, column 39
    function locationRef_443 (VehicleRU :  VehicleRU) : pcf.api.Destination {
      return pcf.ClaimPolicyVehicleDetail.createDestination(Claim, VehicleRU)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1352, column 54
    function locationRef_444 () : pcf.api.Destination {
      return pcf.ClaimPolicyVehicles.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1371, column 44
    function locationRef_446 (PolicyLocation :  PolicyLocation) : pcf.api.Destination {
      return pcf.PolicyLocationPrint.createDestination(PolicyLocation, Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1367, column 53
    function locationRef_448 () : pcf.api.Destination {
      return pcf.ClaimPolicyLocations.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1385, column 36
    function locationRef_450 (aTripRU :  TripRU) : pcf.api.Destination {
      return pcf.TripRUPrint.createDestination(Claim, aTripRU)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1381, column 51
    function locationRef_451 () : pcf.api.Destination {
      return pcf.ClaimPolicyTrips.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1398, column 41
    function locationRef_453 (Endorsement :  Endorsement) : pcf.api.Destination {
      return pcf.ClaimPolicyEndorsementDetail.createDestination(Claim, Endorsement)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1394, column 58
    function locationRef_454 () : pcf.api.Destination {
      return pcf.ClaimPolicyEndorsements.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1411, column 38
    function locationRef_455 (StatCode :  StatCode) : pcf.api.Destination {
      return pcf.ClaimPolicyStatCodeDetail.createDestination(Claim, StatCode)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1407, column 60
    function locationRef_456 () : pcf.api.Destination {
      return pcf.ClaimPolicyStatCodesPrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1425, column 44
    function locationRef_458 (AggregateLimit :  AggregateLimit) : pcf.api.Destination {
      return pcf.ClaimPolicyAggregateLimitDetail.createDestination(Claim, AggregateLimit)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1421, column 151
    function locationRef_460 () : pcf.api.Destination {
      return pcf.ClaimPolicyAggregateLimits.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1434, column 54
    function locationRef_461 () : pcf.api.Destination {
      return pcf.ClaimPolicyGeneral.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1441, column 39
    function locationRef_462 (VehicleRU :  VehicleRU) : pcf.api.Destination {
      return pcf.ClaimPolicyVehicleDetail.createDestination(Claim, VehicleRU)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1437, column 54
    function locationRef_463 () : pcf.api.Destination {
      return pcf.ClaimPolicyVehicles.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1449, column 54
    function locationRef_465 () : pcf.api.Destination {
      return pcf.ClaimPolicyGeneral.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1457, column 39
    function locationRef_466 (VehicleRU :  VehicleRU) : pcf.api.Destination {
      return pcf.ClaimPolicyVehicleDetail.createDestination(Claim, VehicleRU)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1453, column 46
    function locationRef_468 () : pcf.api.Destination {
      return pcf.ClaimPolicyVehicles.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1466, column 44
    function locationRef_469 (PolicyLocation :  PolicyLocation) : pcf.api.Destination {
      return pcf.PolicyLocationPrint.createDestination(PolicyLocation, Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1462, column 53
    function locationRef_471 () : pcf.api.Destination {
      return pcf.ClaimPolicyLocations.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1475, column 36
    function locationRef_472 (aTripRU :  TripRU) : pcf.api.Destination {
      return pcf.TripRUPrint.createDestination(Claim, aTripRU)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1471, column 49
    function locationRef_474 () : pcf.api.Destination {
      return pcf.ClaimPolicyTrips.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1483, column 41
    function locationRef_475 (Endorsement :  Endorsement) : pcf.api.Destination {
      return pcf.ClaimPolicyEndorsementDetail.createDestination(Claim, Endorsement)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1479, column 58
    function locationRef_476 () : pcf.api.Destination {
      return pcf.ClaimPolicyEndorsements.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1492, column 38
    function locationRef_477 (StatCode :  StatCode) : pcf.api.Destination {
      return pcf.ClaimPolicyStatCodeDetail.createDestination(Claim, StatCode)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1488, column 48
    function locationRef_479 () : pcf.api.Destination {
      return pcf.ClaimPolicyStatCodes.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1501, column 44
    function locationRef_480 (AggregateLimit :  AggregateLimit) : pcf.api.Destination {
      return pcf.ClaimPolicyAggregateLimitDetail.createDestination(Claim, AggregateLimit)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1497, column 151
    function locationRef_482 () : pcf.api.Destination {
      return pcf.ClaimPolicyAggregateLimits.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOption (id=PolicyVehiclesLocationsOption) at ClaimPrintout.pcf: line 1349, column 44
    function printable_445 () : java.lang.Boolean {
      return helper.isAutoClaim()
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1367, column 53
    function printable_447 () : java.lang.Boolean {
      return canShowClaimPolicyLocations
    }
    
    // 'printable' attribute on PrintOption (id=PolicyTrips) at ClaimPrintout.pcf: line 1378, column 47
    function printable_452 () : java.lang.Boolean {
      return canShowClaimPolicyTrips
    }
    
    // 'printable' attribute on PrintOption (id=PolicyStatCodesOption) at ClaimPrintout.pcf: line 1405, column 46
    function printable_457 () : java.lang.Boolean {
      return canShowPolicyStatCodes
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1421, column 151
    function printable_459 () : java.lang.Boolean {
      return gw.api.policy.PolicyTabUtil.hasTab(Claim, "AggregateLimits") and perm.Policy.view(Claim) and perm.System.viewpolicy
    }
    
    // PrintSection (id=Policy3Section) at ClaimPrintout.pcf: line 1336, column 44
    function value_484 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Policy3Section) at ClaimPrintout.pcf: line 1336, column 44
    function visible_483 () : java.lang.Boolean {
      return perm.System.viewpolicy
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Policy3Section) at ClaimPrintout.pcf: line 1336, column 44
    function visible_485 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Policy3Section) at ClaimPrintout.pcf: line 1336, column 44
    function visible_489 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PropertyIncident2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 475, column 51
    function locationRef_109 (fixedPropertyIncident :  FixedPropertyIncident) : pcf.api.Destination {
      return pcf.FixedPropertyIncidentDetailPrint.createDestination(fixedPropertyIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 471, column 64
    function locationRef_111 () : pcf.api.Destination {
      return pcf.FixedPropertyIncidentPrint.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 471, column 64
    function printable_110 () : java.lang.Boolean {
      return !helper.FixedPropertyIncidents.IsEmpty
    }
    
    // 'printable' attribute on PrintSection (id=PropertyIncident2Section) at ClaimPrintout.pcf: line 465, column 49
    function printable_113 () : java.lang.Boolean {
      return perm.System.viewclaimbasics
    }
    
    // PrintSection (id=PropertyIncident2Section) at ClaimPrintout.pcf: line 465, column 49
    function visible_112 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PropertyIncident3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=PropertyIncident3Section) at ClaimPrintout.pcf: line 1127, column 49
    function defaultSetter_321 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1137, column 51
    function locationRef_314 (fixedPropertyIncident :  FixedPropertyIncident) : pcf.api.Destination {
      return pcf.FixedPropertyIncidentDetailPrint.createDestination(fixedPropertyIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1133, column 64
    function locationRef_316 () : pcf.api.Destination {
      return pcf.FixedPropertyIncidentPrint.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1133, column 64
    function printable_315 () : java.lang.Boolean {
      return !helper.FixedPropertyIncidents.IsEmpty
    }
    
    // PrintSection (id=PropertyIncident3Section) at ClaimPrintout.pcf: line 1127, column 49
    function value_318 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=PropertyIncident3Section) at ClaimPrintout.pcf: line 1127, column 49
    function visible_317 () : java.lang.Boolean {
      return perm.System.viewclaimbasics
    }
    
    // 'childrenVisible' attribute on PrintSection (id=PropertyIncident3Section) at ClaimPrintout.pcf: line 1127, column 49
    function visible_319 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=PropertyIncident3Section) at ClaimPrintout.pcf: line 1127, column 49
    function visible_323 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SpecialInvestigations1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 173, column 45
    function locationRef_26 () : pcf.api.Destination {
      return pcf.SIDetails.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=SpecialInvestigations1Section) at ClaimPrintout.pcf: line 168, column 49
    function printable_28 () : java.lang.Boolean {
      return perm.System.viewclaimbasics
    }
    
    // PrintSection (id=SpecialInvestigations1Section) at ClaimPrintout.pcf: line 168, column 49
    function visible_27 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SpecialInvestigations2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 577, column 45
    function locationRef_141 () : pcf.api.Destination {
      return pcf.SIDetails.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=SpecialInvestigations2Section) at ClaimPrintout.pcf: line 572, column 49
    function printable_143 () : java.lang.Boolean {
      return perm.System.viewclaimbasics
    }
    
    // PrintSection (id=SpecialInvestigations2Section) at ClaimPrintout.pcf: line 572, column 49
    function visible_142 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SpecialInvestigations3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=SpecialInvestigations3Section) at ClaimPrintout.pcf: line 1240, column 49
    function defaultSetter_380 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1245, column 45
    function locationRef_375 () : pcf.api.Destination {
      return pcf.SIDetails.createDestination(Claim)
    }
    
    // PrintSection (id=SpecialInvestigations3Section) at ClaimPrintout.pcf: line 1240, column 49
    function value_377 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=SpecialInvestigations3Section) at ClaimPrintout.pcf: line 1240, column 49
    function visible_376 () : java.lang.Boolean {
      return perm.System.viewclaimbasics
    }
    
    // 'childrenVisible' attribute on PrintSection (id=SpecialInvestigations3Section) at ClaimPrintout.pcf: line 1240, column 49
    function visible_378 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=SpecialInvestigations3Section) at ClaimPrintout.pcf: line 1240, column 49
    function visible_382 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Subrogation1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 359, column 54
    function locationRef_78 () : pcf.api.Destination {
      return pcf.SubrogationGeneral.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Subrogation1Section) at ClaimPrintout.pcf: line 354, column 87
    function printable_80 () : java.lang.Boolean {
      return perm.System.viewsubrodetails and Claim.SubrogationSummary != null
    }
    
    // PrintSection (id=Subrogation1Section) at ClaimPrintout.pcf: line 354, column 87
    function visible_79 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Subrogation2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 847, column 54
    function locationRef_216 () : pcf.api.Destination {
      return pcf.SubrogationGeneral.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Subrogation2Section) at ClaimPrintout.pcf: line 842, column 87
    function printable_218 () : java.lang.Boolean {
      return perm.System.viewsubrodetails and Claim.SubrogationSummary != null
    }
    
    // PrintSection (id=Subrogation2Section) at ClaimPrintout.pcf: line 842, column 87
    function visible_217 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Subrogation3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Subrogation3Section) at ClaimPrintout.pcf: line 1803, column 87
    function defaultSetter_610 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1808, column 54
    function locationRef_605 () : pcf.api.Destination {
      return pcf.SubrogationGeneral.createDestination(Claim)
    }
    
    // PrintSection (id=Subrogation3Section) at ClaimPrintout.pcf: line 1803, column 87
    function value_607 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Subrogation3Section) at ClaimPrintout.pcf: line 1803, column 87
    function visible_606 () : java.lang.Boolean {
      return perm.System.viewsubrodetails and Claim.SubrogationSummary != null
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Subrogation3Section) at ClaimPrintout.pcf: line 1803, column 87
    function visible_608 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Subrogation3Section) at ClaimPrintout.pcf: line 1803, column 87
    function visible_612 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Summary1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 119, column 48
    function locationRef_11 () : pcf.api.Destination {
      return pcf.ClaimSummary.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 122, column 115
    function locationRef_13 () : pcf.api.Destination {
      return pcf.ClaimStatus.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 125, column 32
    function locationRef_14 () : pcf.api.Destination {
      return pcf.ClaimKeyMetrics.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 122, column 115
    function printable_12 () : java.lang.Boolean {
      return perm.Claim.own and perm.System.viewclaimsummary and (Claim.State != ClaimState.TC_DRAFT)
    }
    
    // 'printable' attribute on PrintSection (id=Summary1Section) at ClaimPrintout.pcf: line 114, column 50
    function printable_16 () : java.lang.Boolean {
      return perm.System.viewclaimsummary
    }
    
    // PrintSection (id=Summary1Section) at ClaimPrintout.pcf: line 114, column 50
    function visible_15 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Summary2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 405, column 48
    function locationRef_92 () : pcf.api.Destination {
      return pcf.ClaimSummary.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 408, column 115
    function locationRef_94 () : pcf.api.Destination {
      return pcf.ClaimStatus.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 411, column 32
    function locationRef_95 () : pcf.api.Destination {
      return pcf.ClaimKeyMetrics.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 408, column 115
    function printable_93 () : java.lang.Boolean {
      return perm.Claim.own and perm.System.viewclaimsummary and (Claim.State != ClaimState.TC_DRAFT)
    }
    
    // 'printable' attribute on PrintSection (id=Summary2Section) at ClaimPrintout.pcf: line 400, column 50
    function printable_97 () : java.lang.Boolean {
      return perm.System.viewclaimsummary
    }
    
    // PrintSection (id=Summary2Section) at ClaimPrintout.pcf: line 400, column 50
    function visible_96 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Summary3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Summary3Section) at ClaimPrintout.pcf: line 1028, column 50
    function defaultSetter_280 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1033, column 48
    function locationRef_266 () : pcf.api.Destination {
      return pcf.ClaimSummary.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1036, column 115
    function locationRef_268 () : pcf.api.Destination {
      return pcf.ClaimStatus.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1039, column 32
    function locationRef_269 () : pcf.api.Destination {
      return pcf.ClaimKeyMetrics.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1045, column 48
    function locationRef_270 () : pcf.api.Destination {
      return pcf.ClaimSummary.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1051, column 54
    function locationRef_271 () : pcf.api.Destination {
      return pcf.ClaimSummary.createDestination(Claim, true)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1059, column 115
    function locationRef_273 () : pcf.api.Destination {
      return pcf.ClaimStatus.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1066, column 51
    function locationRef_275 () : pcf.api.Destination {
      return pcf.ClaimKeyMetrics.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1036, column 115
    function printable_267 () : java.lang.Boolean {
      return perm.Claim.own and perm.System.viewclaimsummary and (Claim.State != ClaimState.TC_DRAFT)
    }
    
    // PrintSection (id=Summary3Section) at ClaimPrintout.pcf: line 1028, column 50
    function value_277 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Summary3Section) at ClaimPrintout.pcf: line 1028, column 50
    function visible_276 () : java.lang.Boolean {
      return perm.System.viewclaimsummary
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Summary3Section) at ClaimPrintout.pcf: line 1028, column 50
    function visible_278 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Summary3Section) at ClaimPrintout.pcf: line 1028, column 50
    function visible_282 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TravelIncident2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 539, column 42
    function locationRef_129 (TripIncident :  TripIncident) : pcf.api.Destination {
      return pcf.TripIncidentDetailPrint.createDestination(Claim, TripIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 535, column 55
    function locationRef_131 () : pcf.api.Destination {
      return pcf.TripIncidentsPrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 547, column 45
    function locationRef_132 (BaggageIncident :  BaggageIncident) : pcf.api.Destination {
      return pcf.BaggageIncidentDetailPrint.createDestination(Claim, BaggageIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 543, column 58
    function locationRef_134 () : pcf.api.Destination {
      return pcf.BaggageIncidentsPrint.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 535, column 55
    function printable_130 () : java.lang.Boolean {
      return !helper.TripIncidents.IsEmpty
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 543, column 58
    function printable_133 () : java.lang.Boolean {
      return !helper.BaggageIncidents.IsEmpty
    }
    
    // 'printable' attribute on PrintSection (id=TravelIncident2Section) at ClaimPrintout.pcf: line 529, column 44
    function printable_136 () : java.lang.Boolean {
      return helper.isTravelClaim()
    }
    
    // PrintSection (id=TravelIncident2Section) at ClaimPrintout.pcf: line 529, column 44
    function visible_135 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TravelIncident3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=TravelIncident3Section) at ClaimPrintout.pcf: line 1191, column 44
    function defaultSetter_360 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1201, column 42
    function locationRef_350 (TripIncident :  TripIncident) : pcf.api.Destination {
      return pcf.TripIncidentDetailPrint.createDestination(Claim, TripIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1197, column 55
    function locationRef_352 () : pcf.api.Destination {
      return pcf.TripIncidentsPrint.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1209, column 45
    function locationRef_353 (BaggageIncident :  BaggageIncident) : pcf.api.Destination {
      return pcf.BaggageIncidentDetailPrint.createDestination(Claim, BaggageIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1205, column 58
    function locationRef_355 () : pcf.api.Destination {
      return pcf.BaggageIncidentsPrint.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1197, column 55
    function printable_351 () : java.lang.Boolean {
      return !helper.TripIncidents.IsEmpty
    }
    
    // 'printable' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1205, column 58
    function printable_354 () : java.lang.Boolean {
      return !helper.BaggageIncidents.IsEmpty
    }
    
    // PrintSection (id=TravelIncident3Section) at ClaimPrintout.pcf: line 1191, column 44
    function value_357 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=TravelIncident3Section) at ClaimPrintout.pcf: line 1191, column 44
    function visible_356 () : java.lang.Boolean {
      return helper.isTravelClaim()
    }
    
    // 'childrenVisible' attribute on PrintSection (id=TravelIncident3Section) at ClaimPrintout.pcf: line 1191, column 44
    function visible_358 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=TravelIncident3Section) at ClaimPrintout.pcf: line 1191, column 44
    function visible_362 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class VehicleIncident2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 456, column 45
    function locationRef_105 (vehicleIncident :  VehicleIncident) : pcf.api.Destination {
      return pcf.VehicleIncidentDetailPrint.createDestination(vehicleIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 452, column 55
    function locationRef_106 () : pcf.api.Destination {
      return pcf.VehicleIncidentPrint.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=VehicleIncident2Section) at ClaimPrintout.pcf: line 447, column 74
    function printable_108 () : java.lang.Boolean {
      return helper.isAutoClaim() and perm.System.viewclaimbasics
    }
    
    // PrintSection (id=VehicleIncident2Section) at ClaimPrintout.pcf: line 447, column 74
    function visible_107 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class VehicleIncident3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=VehicleIncident3Section) at ClaimPrintout.pcf: line 1109, column 74
    function defaultSetter_310 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1118, column 45
    function locationRef_304 (vehicleIncident :  VehicleIncident) : pcf.api.Destination {
      return pcf.VehicleIncidentDetailPrint.createDestination(vehicleIncident)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1114, column 55
    function locationRef_305 () : pcf.api.Destination {
      return pcf.VehicleIncidentPrint.createDestination(Claim)
    }
    
    // PrintSection (id=VehicleIncident3Section) at ClaimPrintout.pcf: line 1109, column 74
    function value_307 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=VehicleIncident3Section) at ClaimPrintout.pcf: line 1109, column 74
    function visible_306 () : java.lang.Boolean {
      return helper.isAutoClaim() and perm.System.viewclaimbasics
    }
    
    // 'childrenVisible' attribute on PrintSection (id=VehicleIncident3Section) at ClaimPrintout.pcf: line 1109, column 74
    function visible_308 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=VehicleIncident3Section) at ClaimPrintout.pcf: line 1109, column 74
    function visible_312 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Workplan1SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 137, column 49
    function locationRef_17 () : pcf.api.Destination {
      return pcf.ClaimWorkplan.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Workplan1Section) at ClaimPrintout.pcf: line 132, column 46
    function printable_19 () : java.lang.Boolean {
      return perm.System.viewworkplan
    }
    
    // PrintSection (id=Workplan1Section) at ClaimPrintout.pcf: line 132, column 46
    function visible_18 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithoutDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Workplan2SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 427, column 38
    function locationRef_98 (Activity :  Activity) : pcf.api.Destination {
      return pcf.ActivityDetailPrint.createDestination(Activity)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 423, column 48
    function locationRef_99 () : pcf.api.Destination {
      return pcf.ClaimWorkplan.createDestination(Claim)
    }
    
    // 'printable' attribute on PrintSection (id=Workplan2Section) at ClaimPrintout.pcf: line 418, column 46
    function printable_101 () : java.lang.Boolean {
      return perm.System.viewworkplan
    }
    
    // PrintSection (id=Workplan2Section) at ClaimPrintout.pcf: line 418, column 46
    function visible_100 () : java.lang.Boolean {
      return Choice == "AllClaimPagesWithDetailsChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/printing/ClaimPrintout.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Workplan3SectionExpressionsImpl extends ClaimPrintoutExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PrintSection (id=Workplan3Section) at ClaimPrintout.pcf: line 1073, column 46
    function defaultSetter_291 (__VALUE_TO_SET :  java.lang.Object) : void {
      __selectedValue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1078, column 49
    function locationRef_284 () : pcf.api.Destination {
      return pcf.ClaimWorkplan.createDestination(Claim)
    }
    
    // 'locationRef' attribute on PrintDetail at ClaimPrintout.pcf: line 1088, column 38
    function locationRef_285 (Activity :  Activity) : pcf.api.Destination {
      return pcf.ActivityDetailPrint.createDestination(Activity)
    }
    
    // 'locationRef' attribute on PrintOptionLocation at ClaimPrintout.pcf: line 1084, column 48
    function locationRef_286 () : pcf.api.Destination {
      return pcf.ClaimWorkplan.createDestination(Claim)
    }
    
    // PrintSection (id=Workplan3Section) at ClaimPrintout.pcf: line 1073, column 46
    function value_288 () : java.lang.Object {
      return null
    }
    
    // 'printable' attribute on PrintSection (id=Workplan3Section) at ClaimPrintout.pcf: line 1073, column 46
    function visible_287 () : java.lang.Boolean {
      return perm.System.viewworkplan
    }
    
    // 'childrenVisible' attribute on PrintSection (id=Workplan3Section) at ClaimPrintout.pcf: line 1073, column 46
    function visible_289 () : java.lang.Boolean {
      return __selectedValue
    }
    
    // PrintSection (id=Workplan3Section) at ClaimPrintout.pcf: line 1073, column 46
    function visible_293 () : java.lang.Boolean {
      return Choice == "CustomChoice"
    }
    
    property get __selectedValue () : boolean {
      return getPrintSectionSelectedValue(1) as java.lang.Boolean
    }
    
    property set __selectedValue ($arg :  boolean) {
      setPrintSectionSelectedValue(1, $arg)
    }
    
    
  }
  
  
}