package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/audit_ext/AdminAuditEntriesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminAuditEntriesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/audit_ext/AdminAuditEntriesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminAuditEntriesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=EntityName_Cell) at AdminAuditEntriesLV.pcf: line 19, column 45
    function sortValue_0 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.ModifiedEntityName
    }
    
    // 'value' attribute on TextCell (id=TriggerEvent_Cell) at AdminAuditEntriesLV.pcf: line 23, column 43
    function sortValue_1 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.TriggerEventName
    }
    
    // 'value' attribute on TextCell (id=ModifiedObject_Cell) at AdminAuditEntriesLV.pcf: line 27, column 45
    function sortValue_2 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.ModifiedObjectName
    }
    
    // 'value' attribute on TextCell (id=ModifiedField_Cell) at AdminAuditEntriesLV.pcf: line 31, column 44
    function sortValue_3 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.ModifiedFieldName
    }
    
    // 'value' attribute on TextCell (id=ModifiedBy_Cell) at AdminAuditEntriesLV.pcf: line 45, column 45
    function sortValue_4 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.ModifiedByUserName
    }
    
    // 'value' attribute on TextCell (id=ModifiedByUserID_Cell) at AdminAuditEntriesLV.pcf: line 49, column 43
    function sortValue_5 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.ModifiedByUserID
    }
    
    // 'value' attribute on DateCell (id=ModifiedDate_Cell) at AdminAuditEntriesLV.pcf: line 55, column 39
    function sortValue_6 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.ModifiedDate
    }
    
    // 'value' attribute on RowIterator at AdminAuditEntriesLV.pcf: line 14, column 74
    function value_34 () : gw.api.database.IQueryBeanResult<AdminAudit_Ext> {
      return AuditEntries
    }
    
    property get AuditEntries () : gw.api.database.IQueryBeanResult<AdminAudit_Ext> {
      return getRequireValue("AuditEntries", 0) as gw.api.database.IQueryBeanResult<AdminAudit_Ext>
    }
    
    property set AuditEntries ($arg :  gw.api.database.IQueryBeanResult<AdminAudit_Ext>) {
      setRequireValue("AuditEntries", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/audit_ext/AdminAuditEntriesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminAuditEntriesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=EntityName_Cell) at AdminAuditEntriesLV.pcf: line 19, column 45
    function valueRoot_8 () : java.lang.Object {
      return entry
    }
    
    // 'value' attribute on TextCell (id=TriggerEvent_Cell) at AdminAuditEntriesLV.pcf: line 23, column 43
    function value_10 () : java.lang.String {
      return entry.TriggerEventName
    }
    
    // 'value' attribute on TextCell (id=ModifiedObject_Cell) at AdminAuditEntriesLV.pcf: line 27, column 45
    function value_13 () : java.lang.String {
      return entry.ModifiedObjectName
    }
    
    // 'value' attribute on TextCell (id=ModifiedField_Cell) at AdminAuditEntriesLV.pcf: line 31, column 44
    function value_16 () : java.lang.String {
      return entry.ModifiedFieldName
    }
    
    // 'value' attribute on TextCell (id=OriginalValue_Cell) at AdminAuditEntriesLV.pcf: line 36, column 40
    function value_19 () : java.lang.String {
      return entry.OriginalValue
    }
    
    // 'value' attribute on TextCell (id=NewValue_Cell) at AdminAuditEntriesLV.pcf: line 41, column 35
    function value_22 () : java.lang.String {
      return entry.NewValue
    }
    
    // 'value' attribute on TextCell (id=ModifiedBy_Cell) at AdminAuditEntriesLV.pcf: line 45, column 45
    function value_25 () : java.lang.String {
      return entry.ModifiedByUserName
    }
    
    // 'value' attribute on TextCell (id=ModifiedByUserID_Cell) at AdminAuditEntriesLV.pcf: line 49, column 43
    function value_28 () : java.lang.String {
      return entry.ModifiedByUserID
    }
    
    // 'value' attribute on DateCell (id=ModifiedDate_Cell) at AdminAuditEntriesLV.pcf: line 55, column 39
    function value_31 () : java.util.Date {
      return entry.ModifiedDate
    }
    
    // 'value' attribute on TextCell (id=EntityName_Cell) at AdminAuditEntriesLV.pcf: line 19, column 45
    function value_7 () : java.lang.String {
      return entry.ModifiedEntityName
    }
    
    property get entry () : AdminAudit_Ext {
      return getIteratedValue(1) as AdminAudit_Ext
    }
    
    
  }
  
  
}