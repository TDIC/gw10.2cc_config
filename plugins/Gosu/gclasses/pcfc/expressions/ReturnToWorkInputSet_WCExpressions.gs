package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/exposures/ReturnToWorkInputSet.WC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ReturnToWorkInputSet_WCExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/exposures/ReturnToWorkInputSet.WC.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ReturnToWorkInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReturnToModWorkValid_Input) at ReturnToWorkInputSet.WC.pcf: line 35, column 30
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      Injury.ReturnToModWorkValid = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=ReturnToModWorkDate_Input) at ReturnToWorkInputSet.WC.pcf: line 45, column 45
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      Injury.ReturnToModWorkDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=DateDisabilityBegan_Input) at ReturnToWorkInputSet.WC.pcf: line 23, column 48
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      Injury.DateDisabilityBegan_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReturnToModWorkActual_Input) at ReturnToWorkInputSet.WC.pcf: line 58, column 46
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      Injury.ReturnToModWorkActual = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReturnToWorkValid_Input) at ReturnToWorkInputSet.WC.pcf: line 73, column 30
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      Injury.ReturnToWorkValid = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=ReturnToWorkDate_Input) at ReturnToWorkInputSet.WC.pcf: line 81, column 39
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      Injury.ReturnToWorkDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReturnToWorkActual_Input) at ReturnToWorkInputSet.WC.pcf: line 94, column 43
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      Injury.ReturnToWorkActual = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=DateLastWorked_Input) at ReturnToWorkInputSet.WC.pcf: line 29, column 43
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      Injury.DateLastWorked_TDIC = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'initialValue' attribute on Variable at ReturnToWorkInputSet.WC.pcf: line 17, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'onChange' attribute on PostOnChange at ReturnToWorkInputSet.WC.pcf: line 47, column 334
    function onChange_15 () : void {
      if (Injury.ReturnToModWorkDate.compareIgnoreTime(Today) == -1) {Injury.ReturnToModWorkActual = true} else if (Injury.ReturnToModWorkDate.compareIgnoreTime(Today) == 0) {Injury.ReturnToModWorkActual = null}  else if (Injury.ReturnToModWorkDate.compareIgnoreTime(Today) == 1) {Injury.ReturnToModWorkActual = false}
    }
    
    // 'onChange' attribute on PostOnChange at ReturnToWorkInputSet.WC.pcf: line 83, column 316
    function onChange_41 () : void {
      if (Injury.ReturnToWorkDate.compareIgnoreTime(Today) == -1) {Injury.ReturnToWorkActual = true} else if (Injury.ReturnToWorkDate.compareIgnoreTime(Today) == 0) {Injury.ReturnToWorkActual = null}  else if (Injury.ReturnToWorkDate.compareIgnoreTime(Today) == 1) {Injury.ReturnToWorkActual = false}
    }
    
    // 'required' attribute on BooleanRadioInput (id=ReturnToModWorkActual_Input) at ReturnToWorkInputSet.WC.pcf: line 58, column 46
    function required_25 () : java.lang.Boolean {
      return Injury.ReturnToModWorkDate != null
    }
    
    // 'required' attribute on BooleanRadioInput (id=ReturnToWorkActual_Input) at ReturnToWorkInputSet.WC.pcf: line 94, column 43
    function required_49 () : java.lang.Boolean {
      return Injury.ReturnToWorkDate != null
    }
    
    // 'validationExpression' attribute on DateInput (id=ReturnToModWorkDate_Input) at ReturnToWorkInputSet.WC.pcf: line 45, column 45
    function validationExpression_16 () : java.lang.Object {
      return ((Injury.ReturnToModWorkActual and Injury.ReturnToModWorkDate > gw.api.util.DateUtil.currentDate()) ?  DisplayKey.get("Warning.Date.RTW.IfActualForbidFuture") : null)
    }
    
    // 'validationExpression' attribute on DateInput (id=ReturnToWorkDate_Input) at ReturnToWorkInputSet.WC.pcf: line 81, column 39
    function validationExpression_42 () : java.lang.Object {
      return ((Injury.ReturnToWorkActual and Injury.ReturnToWorkDate > gw.api.util.DateUtil.currentDate()) ?  DisplayKey.get("Warning.Date.RTW.IfActualForbidFuture") : null)
    }
    
    // 'value' attribute on DateInput (id=DateDisabilityBegan_Input) at ReturnToWorkInputSet.WC.pcf: line 23, column 48
    function valueRoot_3 () : java.lang.Object {
      return Injury
    }
    
    // 'value' attribute on DateInput (id=DateDisabilityBegan_Input) at ReturnToWorkInputSet.WC.pcf: line 23, column 48
    function value_1 () : java.util.Date {
      return Injury.DateDisabilityBegan_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReturnToModWorkValid_Input) at ReturnToWorkInputSet.WC.pcf: line 35, column 30
    function value_10 () : java.lang.Boolean {
      return Injury.ReturnToModWorkValid
    }
    
    // 'value' attribute on DateInput (id=ReturnToModWorkDate_Input) at ReturnToWorkInputSet.WC.pcf: line 45, column 45
    function value_18 () : java.util.Date {
      return Injury.ReturnToModWorkDate
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReturnToModWorkActual_Input) at ReturnToWorkInputSet.WC.pcf: line 58, column 46
    function value_26 () : java.lang.Boolean {
      return Injury.ReturnToModWorkActual
    }
    
    // 'value' attribute on TextInput (id=RTModWReadOnlyMSG_Input) at ReturnToWorkInputSet.WC.pcf: line 64, column 87
    function value_32 () : java.lang.String {
      return (((Injury.ReturnToModWorkDate== null ? null :( Injury.ReturnToModWorkActual) ? Injury.ReturnToModWorkDate.format( "short" ) : DisplayKey.get("NVV.Incident.Injury.RTW.Msg.Projected", Injury.ReturnToModWorkDate.format( "short" ), (-1 * Injury.ReturnToModWorkDate.DaysSince) ))))
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReturnToWorkValid_Input) at ReturnToWorkInputSet.WC.pcf: line 73, column 30
    function value_36 () : java.lang.Boolean {
      return Injury.ReturnToWorkValid
    }
    
    // 'value' attribute on DateInput (id=ReturnToWorkDate_Input) at ReturnToWorkInputSet.WC.pcf: line 81, column 39
    function value_43 () : java.util.Date {
      return Injury.ReturnToWorkDate
    }
    
    // 'value' attribute on DateInput (id=DateLastWorked_Input) at ReturnToWorkInputSet.WC.pcf: line 29, column 43
    function value_5 () : java.util.Date {
      return Injury.DateLastWorked_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReturnToWorkActual_Input) at ReturnToWorkInputSet.WC.pcf: line 94, column 43
    function value_50 () : java.lang.Boolean {
      return Injury.ReturnToWorkActual
    }
    
    // 'value' attribute on TextInput (id=RTWReadOnlyMSG_Input) at ReturnToWorkInputSet.WC.pcf: line 100, column 84
    function value_56 () : java.lang.String {
      return (((Injury.ReturnToWorkDate== null ? null :( Injury.ReturnToWorkActual) ? Injury.ReturnToWorkDate.format( "short" ) : DisplayKey.get("NVV.Incident.Injury.RTW.Msg.Projected", Injury.ReturnToWorkDate.format( "short" ), (-1 * Injury.ReturnToWorkDate.DaysSince) ))))
    }
    
    // 'visible' attribute on TextInput (id=RTModWReadOnlyMSG_Input) at ReturnToWorkInputSet.WC.pcf: line 64, column 87
    function visible_31 () : java.lang.Boolean {
      return Injury.ReturnToModWorkValid and (CurrentLocation.InEditMode != true)
    }
    
    // 'visible' attribute on TextInput (id=RTWReadOnlyMSG_Input) at ReturnToWorkInputSet.WC.pcf: line 100, column 84
    function visible_55 () : java.lang.Boolean {
      return Injury.ReturnToWorkValid and (CurrentLocation.InEditMode != true)
    }
    
    // 'visible' attribute on BooleanRadioInput (id=ReturnToModWorkValid_Input) at ReturnToWorkInputSet.WC.pcf: line 35, column 30
    function visible_9 () : java.lang.Boolean {
      return DetailedView
    }
    
    property get DetailedView () : Boolean {
      return getRequireValue("DetailedView", 0) as Boolean
    }
    
    property set DetailedView ($arg :  Boolean) {
      setRequireValue("DetailedView", 0, $arg)
    }
    
    property get Injury () : InjuryIncident {
      return getRequireValue("Injury", 0) as InjuryIncident
    }
    
    property set Injury ($arg :  InjuryIncident) {
      setRequireValue("Injury", 0, $arg)
    }
    
    property get Today () : java.util.Date {
      return getVariableValue("Today", 0) as java.util.Date
    }
    
    property set Today ($arg :  java.util.Date) {
      setVariableValue("Today", 0, $arg)
    }
    
    
  }
  
  
}