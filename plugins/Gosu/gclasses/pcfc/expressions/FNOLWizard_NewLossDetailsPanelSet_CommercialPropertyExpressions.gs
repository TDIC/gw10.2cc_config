package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FNOLWizard_NewLossDetailsPanelSet_CommercialPropertyExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FNOLWizard_NewLossDetailsPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 312, column 29
    function def_onEnter_178 (def :  pcf.EditableFixedPropertyIncidentsLV) : void {
      def.onEnter(claim)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 326, column 27
    function def_onEnter_180 (def :  pcf.EditableInjuryIncidentsLV) : void {
      def.onEnter(claim)
    }
    
    // 'def' attribute on InputSetRef at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 346, column 56
    function def_onEnter_182 (def :  pcf.CCAddressInputSet) : void {
      def.onEnter(claim.AddressOwner)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 355, column 27
    function def_onEnter_184 (def :  pcf.EditableOfficialsLV) : void {
      def.onEnter(claim)
    }
    
    // 'def' attribute on PanelRef at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 364, column 36
    function def_onEnter_187 (def :  pcf.OptionalNoteCV) : void {
      def.onEnter(claim)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 372, column 29
    function def_onEnter_189 (def :  pcf.PriorClaimsLV) : void {
      def.onEnter(claim.findPriorClaimsByPolicyNumber(claim.Policy.PolicyNumber, {ClaimState.TC_DRAFT}))
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 381, column 27
    function def_onEnter_191 (def :  pcf.MetroReportsLV) : void {
      def.onEnter(claim)
    }
    
    // 'def' attribute on InputSetRef (id=AddressDetailInputSetRef) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 142, column 84
    function def_onEnter_72 (def :  pcf.CCAddressInputSet) : void {
      def.onEnter(wizard.GeneralLiabilityHelper.getAddressesWithoutPrimaryLocation())
    }
    
    // 'def' attribute on PanelRef (id=FireDamageQuestionsPanelSet) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 164, column 50
    function def_onEnter_92 (def :  pcf.FireDamageQuestionsPanelSet) : void {
      def.onEnter(claim)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 312, column 29
    function def_refreshVariables_179 (def :  pcf.EditableFixedPropertyIncidentsLV) : void {
      def.refreshVariables(claim)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 326, column 27
    function def_refreshVariables_181 (def :  pcf.EditableInjuryIncidentsLV) : void {
      def.refreshVariables(claim)
    }
    
    // 'def' attribute on InputSetRef at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 346, column 56
    function def_refreshVariables_183 (def :  pcf.CCAddressInputSet) : void {
      def.refreshVariables(claim.AddressOwner)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 355, column 27
    function def_refreshVariables_185 (def :  pcf.EditableOfficialsLV) : void {
      def.refreshVariables(claim)
    }
    
    // 'def' attribute on PanelRef at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 364, column 36
    function def_refreshVariables_188 (def :  pcf.OptionalNoteCV) : void {
      def.refreshVariables(claim)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 372, column 29
    function def_refreshVariables_190 (def :  pcf.PriorClaimsLV) : void {
      def.refreshVariables(claim.findPriorClaimsByPolicyNumber(claim.Policy.PolicyNumber, {ClaimState.TC_DRAFT}))
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 381, column 27
    function def_refreshVariables_192 (def :  pcf.MetroReportsLV) : void {
      def.refreshVariables(claim)
    }
    
    // 'def' attribute on InputSetRef (id=AddressDetailInputSetRef) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 142, column 84
    function def_refreshVariables_73 (def :  pcf.CCAddressInputSet) : void {
      def.refreshVariables(wizard.GeneralLiabilityHelper.getAddressesWithoutPrimaryLocation())
    }
    
    // 'def' attribute on PanelRef (id=FireDamageQuestionsPanelSet) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 164, column 50
    function def_refreshVariables_93 (def :  pcf.FireDamageQuestionsPanelSet) : void {
      def.refreshVariables(claim)
    }
    
    // 'value' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 205, column 28
    function defaultSetter_107 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.PermissionRequired = (__VALUE_TO_SET as typekey.ClaimSecurityType)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ArsonInvolved_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 231, column 64
    function defaultSetter_124 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.PropertyFireDamage.Arson = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=LocationOfTheft_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 238, column 53
    function defaultSetter_130 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.LocationOfTheft = (__VALUE_TO_SET as typekey.LocationOfTheft)
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_Fault_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 253, column 27
    function defaultSetter_142 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.FaultRating = (__VALUE_TO_SET as typekey.FaultRating)
    }
    
    // 'value' attribute on TextInput (id=Notification_AtFaultPercentage_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 264, column 50
    function defaultSetter_147 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.Fault = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on CheckBoxInput (id=StatusIncidentReport_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 297, column 41
    function defaultSetter_171 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.IncidentReport = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossCause_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 50, column 27
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.LossCause = (__VALUE_TO_SET as typekey.LossCause)
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 25, column 38
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 66, column 44
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.CallType_TDIC = (__VALUE_TO_SET as String)
    }
    
    // 'value' attribute on TextInput (id=Other_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 76, column 45
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.Other_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Status_CoverageQuestion_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 83, column 28
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.CoverageInQuestion = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Notification_FirstNoticeSuit_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 89, column 28
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.FirstNoticeSuit = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=DateOfNotice_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 96, column 39
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.ReportedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_HowReported_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 102, column 48
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.HowReported = (__VALUE_TO_SET as typekey.HowReportedType)
    }
    
    // 'value' attribute on Choice (id=PrimaryLocationChoice) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 116, column 42
    function defaultSetter_65 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizard.GeneralLiabilityHelper.LocationChoice = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 152, column 28
    function defaultSetter_80 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.Catastrophe = (__VALUE_TO_SET as entity.Catastrophe)
    }
    
    // 'value' attribute on BooleanRadioInput (id=WeatherRelated_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 158, column 28
    function defaultSetter_87 (__VALUE_TO_SET :  java.lang.Object) : void {
      claim.WeatherRelated = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 17, column 53
    function initialValue_0 () : gw.api.claim.FnolWizardPropertyHelper {
      return new gw.api.claim.FnolWizardPropertyHelper(claim)
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 224, column 72
    function onChange_117 () : void {
      PropertyHelper.createDamageTypesPerLossCause()
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 52, column 87
    function onChange_16 () : void {
      wizard.GeneralLiabilityHelper.setDamageAccordingToLossCause()
    }
    
    // 'option' attribute on Choice (id=PrimaryLocationChoice) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 116, column 42
    function option_63 () : java.lang.Object {
      return gw.api.claim.FnolWizardGeneralLiabilityHelper.PRIMARY_LOCATION
    }
    
    // 'option' attribute on Choice (id=OtherLocationChoice) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 132, column 40
    function option_74 () : java.lang.Object {
      return gw.api.claim.FnolWizardGeneralLiabilityHelper.OTHER_LOCATION
    }
    
    // 'required' attribute on TextInput (id=Other_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 76, column 45
    function required_34 () : java.lang.Boolean {
      return claim.CallType_TDIC == "Other"
    }
    
    // 'validationExpression' attribute on DateInput (id=Notification_ReportedDate_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 281, column 39
    function validationExpression_156 () : java.lang.Object {
      return claim.ReportedDate == null || claim.ReportedDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'validationExpression' attribute on DateInput (id=DateOfNotice_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 96, column 39
    function validationExpression_48 () : java.lang.Object {
      return claim.ReportedDate != null and claim.ReportedDate > gw.api.util.DateUtil.currentDate() ? DisplayKey.get("Java.Validation.Date.ForbidFuture") : null
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 32, column 35
    function validationExpression_5 () : java.lang.Object {
      return claim.LossDate == null || claim.LossDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 205, column 28
    function valueRange_109 () : java.lang.Object {
      return gw.api.claim.ClaimUtil.getAvailableTypes()
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 66, column 44
    function valueRange_28 () : java.lang.Object {
      return claim.getCallType(claim)
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 152, column 28
    function valueRange_82 () : java.lang.Object {
      return gw.api.admin.CatastropheUtil.getCatastrophes()
    }
    
    // 'value' attribute on BooleanRadioInput (id=ArsonInvolved_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 231, column 64
    function valueRoot_125 () : java.lang.Object {
      return claim.PropertyFireDamage
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 25, column 38
    function valueRoot_3 () : java.lang.Object {
      return claim
    }
    
    // 'value' attribute on TextInput (id=PropertyAddress_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 125, column 73
    function valueRoot_61 () : java.lang.Object {
      return claim.Policy.PrimaryLocation.Address
    }
    
    // 'value' attribute on Choice (id=PrimaryLocationChoice) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 116, column 42
    function valueRoot_66 () : java.lang.Object {
      return wizard.GeneralLiabilityHelper
    }
    
    // 'value' attribute on TextInput (id=Policy_PolicyNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 176, column 46
    function valueRoot_95 () : java.lang.Object {
      return claim.Policy
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 25, column 38
    function value_1 () : java.lang.String {
      return claim.Description
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossType_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 38, column 41
    function value_10 () : typekey.LossType {
      return claim.LossType
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LOBCode_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 190, column 40
    function value_100 () : typekey.LOBCode {
      return claim.LOBCode
    }
    
    // 'value' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 205, column 28
    function value_106 () : typekey.ClaimSecurityType {
      return claim.PermissionRequired
    }
    
    // 'value' attribute on BooleanRadioInput (id=ArsonInvolved_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 231, column 64
    function value_123 () : java.lang.Boolean {
      return claim.PropertyFireDamage.Arson
    }
    
    // 'value' attribute on TypeKeyInput (id=LocationOfTheft_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 238, column 53
    function value_129 () : typekey.LocationOfTheft {
      return claim.LocationOfTheft
    }
    
    // 'value' attribute on BooleanRadioInput (id=RiskManagementIncident_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 42, column 48
    function value_13 () : java.lang.Boolean {
      return claim.RiskMgmtIncident_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_Fault_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 253, column 27
    function value_141 () : typekey.FaultRating {
      return claim.FaultRating
    }
    
    // 'value' attribute on TextInput (id=Notification_AtFaultPercentage_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 264, column 50
    function value_146 () : java.math.BigDecimal {
      return claim.Fault
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossCause_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 50, column 27
    function value_17 () : typekey.LossCause {
      return claim.LossCause
    }
    
    // 'value' attribute on CheckBoxInput (id=Status_IncidentReport_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 57, column 41
    function value_21 () : java.lang.Boolean {
      return claim.IncidentReport
    }
    
    // 'value' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 66, column 44
    function value_25 () : String {
      return claim.CallType_TDIC
    }
    
    // 'value' attribute on TextInput (id=Other_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 76, column 45
    function value_35 () : java.lang.String {
      return claim.Other_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=Status_CoverageQuestion_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 83, column 28
    function value_40 () : java.lang.Boolean {
      return claim.CoverageInQuestion
    }
    
    // 'value' attribute on BooleanRadioInput (id=Notification_FirstNoticeSuit_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 89, column 28
    function value_44 () : java.lang.Boolean {
      return claim.FirstNoticeSuit
    }
    
    // 'value' attribute on DateInput (id=DateOfNotice_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 96, column 39
    function value_49 () : java.util.Date {
      return claim.ReportedDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_HowReported_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 102, column 48
    function value_54 () : typekey.HowReportedType {
      return claim.HowReported
    }
    
    // 'value' attribute on TextInput (id=PrimaryLocationChoiceLabel_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 121, column 45
    function value_58 () : java.lang.Object {
      return null
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 32, column 35
    function value_6 () : java.util.Date {
      return claim.LossDate
    }
    
    // 'value' attribute on TextInput (id=PropertyAddress_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 125, column 73
    function value_60 () : java.lang.String {
      return claim.Policy.PrimaryLocation.Address.DisplayName
    }
    
    // 'value' attribute on Choice (id=PrimaryLocationChoice) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 116, column 42
    function value_64 () : java.lang.String {
      return wizard.GeneralLiabilityHelper.LocationChoice
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 152, column 28
    function value_79 () : entity.Catastrophe {
      return claim.Catastrophe
    }
    
    // 'value' attribute on BooleanRadioInput (id=WeatherRelated_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 158, column 28
    function value_86 () : java.lang.Boolean {
      return claim.WeatherRelated
    }
    
    // 'value' attribute on TextInput (id=Policy_PolicyNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 176, column 46
    function value_94 () : java.lang.String {
      return claim.Policy.PolicyNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=Policy_Status_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 181, column 45
    function value_97 () : typekey.PolicyStatus {
      return claim.Policy.Status
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 205, column 28
    function verifyValueRangeIsAllowedType_110 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 205, column 28
    function verifyValueRangeIsAllowedType_110 ($$arg :  typekey.ClaimSecurityType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CatastropheCatastropheNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 246, column 43
    function verifyValueRangeIsAllowedType_138 ($$arg :  entity.Catastrophe[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CatastropheCatastropheNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 246, column 43
    function verifyValueRangeIsAllowedType_138 ($$arg :  gw.api.database.IQueryBeanResult<entity.Catastrophe>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CatastropheCatastropheNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 246, column 43
    function verifyValueRangeIsAllowedType_138 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 66, column 44
    function verifyValueRangeIsAllowedType_29 ($$arg :  String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 66, column 44
    function verifyValueRangeIsAllowedType_29 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 152, column 28
    function verifyValueRangeIsAllowedType_83 ($$arg :  entity.Catastrophe[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 152, column 28
    function verifyValueRangeIsAllowedType_83 ($$arg :  gw.api.database.IQueryBeanResult<entity.Catastrophe>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 152, column 28
    function verifyValueRangeIsAllowedType_83 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 205, column 28
    function verifyValueRange_111 () : void {
      var __valueRangeArg = gw.api.claim.ClaimUtil.getAvailableTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_110(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=CatastropheCatastropheNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 246, column 43
    function verifyValueRange_139 () : void {
      var __valueRangeArg = gw.api.admin.CatastropheUtil.getCatastrophes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_138(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 66, column 44
    function verifyValueRange_30 () : void {
      var __valueRangeArg = claim.getCallType(claim)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_29(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 152, column 28
    function verifyValueRange_84 () : void {
      var __valueRangeArg = gw.api.admin.CatastropheUtil.getCatastrophes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_83(__valueRangeArg)
    }
    
    // 'visible' attribute on BooleanRadioInput (id=ArsonInvolved_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 231, column 64
    function visible_122 () : java.lang.Boolean {
      return PropertyHelper.PropertyFireDamage.Present
    }
    
    // 'visible' attribute on TypeKeyInput (id=LocationOfTheft_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 238, column 53
    function visible_128 () : java.lang.Boolean {
      return claim.LossCause == TC_BURGLARY
    }
    
    // 'visible' attribute on TextInput (id=Notification_AtFaultPercentage_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 264, column 50
    function visible_145 () : java.lang.Boolean {
      return  claim.FaultRating == TC_1 
    }
    
    // 'visible' attribute on DetailViewPanel at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 169, column 39
    function visible_186 () : java.lang.Boolean {
      return claim.Policy.Verified
    }
    
    // 'visible' attribute on RangeInput (id=CallType_Input) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 66, column 44
    function visible_24 () : java.lang.Boolean {
      return !claim.Policy.Verified
    }
    
    // 'visible' attribute on InputSet at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 111, column 67
    function visible_68 () : java.lang.Boolean {
      return false // claim.Policy.PrimaryLocation != null
    }
    
    // 'visible' attribute on InputSetRef (id=AddressDetailInputSetRef) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 142, column 84
    function visible_71 () : java.lang.Boolean {
      return false //wizard.GeneralLiabilityHelper.IsOtherLocationChosen
    }
    
    // 'visible' attribute on PanelRef (id=FireDamageQuestionsPanelSet) at FNOLWizard_NewLossDetailsPanelSet.CommercialProperty.pcf: line 164, column 50
    function visible_91 () : java.lang.Boolean {
      return claim.PropertyFireDamage != null
    }
    
    property get PropertyHelper () : gw.api.claim.FnolWizardPropertyHelper {
      return getVariableValue("PropertyHelper", 0) as gw.api.claim.FnolWizardPropertyHelper
    }
    
    property set PropertyHelper ($arg :  gw.api.claim.FnolWizardPropertyHelper) {
      setVariableValue("PropertyHelper", 0, $arg)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get wizard () : gw.api.claim.NewClaimWizardInfo {
      return getRequireValue("wizard", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set wizard ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setRequireValue("wizard", 0, $arg)
    }
    
    
  }
  
  
}