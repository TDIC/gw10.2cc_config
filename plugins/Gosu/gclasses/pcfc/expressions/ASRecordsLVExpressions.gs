package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/ASRecordsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ASRecordsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/ASRecordsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ASRecordsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TypeKeyCell (id=AssignTo_Cell) at ASRecordsLV.pcf: line 28, column 48
    function sortValue_0 (ASRecord :  entity.AssignStrategy_TDIC) : java.lang.Object {
      return ASRecord.AssignTo
    }
    
    // 'value' attribute on TypeKeyCell (id=ClaimType_Cell) at ASRecordsLV.pcf: line 34, column 47
    function sortValue_1 (ASRecord :  entity.AssignStrategy_TDIC) : java.lang.Object {
      return ASRecord.ClaimType
    }
    
    // 'value' attribute on TextCell (id=GroupName_Cell) at ASRecordsLV.pcf: line 39, column 39
    function sortValue_2 (ASRecord :  entity.AssignStrategy_TDIC) : java.lang.Object {
      return ASRecord.GroupName
    }
    
    // 'value' attribute on TextCell (id=Queue_Cell) at ASRecordsLV.pcf: line 43, column 39
    function sortValue_3 (ASRecord :  entity.AssignStrategy_TDIC) : java.lang.Object {
      return ASRecord.QueueName
    }
    
    // 'value' attribute on RowIterator at ASRecordsLV.pcf: line 19, column 86
    function value_18 () : gw.api.database.IQueryBeanResult<entity.AssignStrategy_TDIC> {
      return ASRecordsList
    }
    
    property get ASRecordsList () : gw.api.database.IQueryBeanResult<AssignStrategy_TDIC> {
      return getRequireValue("ASRecordsList", 0) as gw.api.database.IQueryBeanResult<AssignStrategy_TDIC>
    }
    
    property set ASRecordsList ($arg :  gw.api.database.IQueryBeanResult<AssignStrategy_TDIC>) {
      setRequireValue("ASRecordsList", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/ASRecordsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ASRecordsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TypeKeyCell (id=AssignTo_Cell) at ASRecordsLV.pcf: line 28, column 48
    function action_4 () : void {
      ASRecordDetail.go(ASRecord)
    }
    
    // 'action' attribute on TypeKeyCell (id=AssignTo_Cell) at ASRecordsLV.pcf: line 28, column 48
    function action_dest_5 () : pcf.api.Destination {
      return pcf.ASRecordDetail.createDestination(ASRecord)
    }
    
    // 'value' attribute on TypeKeyCell (id=AssignTo_Cell) at ASRecordsLV.pcf: line 28, column 48
    function valueRoot_7 () : java.lang.Object {
      return ASRecord
    }
    
    // 'value' attribute on TextCell (id=GroupName_Cell) at ASRecordsLV.pcf: line 39, column 39
    function value_12 () : java.lang.String {
      return ASRecord.GroupName
    }
    
    // 'value' attribute on TextCell (id=Queue_Cell) at ASRecordsLV.pcf: line 43, column 39
    function value_15 () : java.lang.String {
      return ASRecord.QueueName
    }
    
    // 'value' attribute on TypeKeyCell (id=AssignTo_Cell) at ASRecordsLV.pcf: line 28, column 48
    function value_6 () : typekey.ASAssignTo_TDIC {
      return ASRecord.AssignTo
    }
    
    // 'value' attribute on TypeKeyCell (id=ClaimType_Cell) at ASRecordsLV.pcf: line 34, column 47
    function value_9 () : typekey.ClaimType_TDIC {
      return ASRecord.ClaimType
    }
    
    property get ASRecord () : entity.AssignStrategy_TDIC {
      return getIteratedValue(1) as entity.AssignStrategy_TDIC
    }
    
    
  }
  
  
}