package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderRequestWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseFolderRequestWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderRequestWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseFolderRequestWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (claim :  Claim) : int {
      return 0
    }
    
    // 'beforeCancel' attribute on Wizard (id=ShareBaseFolderRequestWizard) at ShareBaseFolderRequestWizard.pcf: line 10, column 73
    function beforeCancel_13 () : void {
      folder.deleteFolderIfExists()
    }
    
    // 'beforeCommit' attribute on Wizard (id=ShareBaseFolderRequestWizard) at ShareBaseFolderRequestWizard.pcf: line 10, column 73
    function beforeCommit_14 (pickedValue :  java.lang.Object) : void {
      gw.api.system.bundle.BundleCommitCondition.verify(claim);IBRequestManager.buildRequest(folder,Request)
    }
    
    // 'initialValue' attribute on Variable at ShareBaseFolderRequestWizard.pcf: line 16, column 44
    function initialValue_0 () : gw.api.web.wizard.WizardInfo {
      return new gw.api.web.wizard.WizardInfo(CurrentLocation)
    }
    
    // 'initialValue' attribute on Variable at ShareBaseFolderRequestWizard.pcf: line 20, column 64
    function initialValue_1 () : acc.onbase.api.application.InboundRequestManager {
      return new acc.onbase.api.application.InboundRequestManager()
    }
    
    // 'initialValue' attribute on Variable at ShareBaseFolderRequestWizard.pcf: line 27, column 29
    function initialValue_2 () : ShareBase_Ext {
      return new ShareBase_Ext()
    }
    
    // 'initialValue' attribute on Variable at ShareBaseFolderRequestWizard.pcf: line 31, column 34
    function initialValue_3 () : InboundRequest_Ext {
      return createRequest()
    }
    
    // 'onResume' attribute on Wizard (id=ShareBaseFolderRequestWizard) at ShareBaseFolderRequestWizard.pcf: line 10, column 73
    function onResume_15 () : void {
      gw.api.system.bundle.BundleCommitCondition.verify(claim)
    }
    
    // 'screen' attribute on WizardStep (id=ReviewRequest) at ShareBaseFolderRequestWizard.pcf: line 47, column 185
    function screen_onEnter_10 (def :  pcf.ShareBaseFolderRequestReviewScreen) : void {
      def.onEnter(claim, folder, Request)
    }
    
    // 'screen' attribute on WizardStep (id=ShareBaseNewFolderDetails) at ShareBaseFolderRequestWizard.pcf: line 37, column 186
    function screen_onEnter_4 (def :  pcf.ShareBaseNewFolderDetailsScreen) : void {
      def.onEnter(claim, folder)
    }
    
    // 'screen' attribute on WizardStep (id=RequestContacts) at ShareBaseFolderRequestWizard.pcf: line 42, column 187
    function screen_onEnter_7 (def :  pcf.ShareBaseFolderRequestContactScreen) : void {
      def.onEnter(claim, Request)
    }
    
    // 'screen' attribute on WizardStep (id=ReviewRequest) at ShareBaseFolderRequestWizard.pcf: line 47, column 185
    function screen_refreshVariables_11 (def :  pcf.ShareBaseFolderRequestReviewScreen) : void {
      def.refreshVariables(claim, folder, Request)
    }
    
    // 'screen' attribute on WizardStep (id=ShareBaseNewFolderDetails) at ShareBaseFolderRequestWizard.pcf: line 37, column 186
    function screen_refreshVariables_5 (def :  pcf.ShareBaseNewFolderDetailsScreen) : void {
      def.refreshVariables(claim, folder)
    }
    
    // 'screen' attribute on WizardStep (id=RequestContacts) at ShareBaseFolderRequestWizard.pcf: line 42, column 187
    function screen_refreshVariables_8 (def :  pcf.ShareBaseFolderRequestContactScreen) : void {
      def.refreshVariables(claim, Request)
    }
    
    // 'tabBar' attribute on Wizard (id=ShareBaseFolderRequestWizard) at ShareBaseFolderRequestWizard.pcf: line 10, column 73
    function tabBar_onEnter_16 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=ShareBaseFolderRequestWizard) at ShareBaseFolderRequestWizard.pcf: line 10, column 73
    function tabBar_refreshVariables_17 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    // 'title' attribute on WizardStep (id=ReviewRequest) at ShareBaseFolderRequestWizard.pcf: line 47, column 185
    function title_12 () : java.lang.String {
      return DisplayKey.get("Accelerator.OnBase.Wizard.NewShareBaseFolderWizard.STR_GW_ReviewAndSubmit_withStepCount", wizard.CurrentStepNumber, wizard.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=ShareBaseNewFolderDetails) at ShareBaseFolderRequestWizard.pcf: line 37, column 186
    function title_6 () : java.lang.String {
      return DisplayKey.get("Accelerator.OnBase.Wizard.NewShareBaseFolderWizard.STR_GW_FolderDetails_withStepCount",  wizard.CurrentStepNumber ,  wizard.TotalNumberOfSteps)
    }
    
    // 'title' attribute on WizardStep (id=RequestContacts) at ShareBaseFolderRequestWizard.pcf: line 42, column 187
    function title_9 () : java.lang.String {
      return DisplayKey.get("Accelerator.OnBase.Wizard.NewShareBaseFolderWizard.STR_GW_SelectContacts_withStepCount",  wizard.CurrentStepNumber ,  wizard.TotalNumberOfSteps)
    }
    
    override property get CurrentLocation () : pcf.ShareBaseFolderRequestWizard {
      return super.CurrentLocation as pcf.ShareBaseFolderRequestWizard
    }
    
    property get IBRequestManager () : acc.onbase.api.application.InboundRequestManager {
      return getVariableValue("IBRequestManager", 0) as acc.onbase.api.application.InboundRequestManager
    }
    
    property set IBRequestManager ($arg :  acc.onbase.api.application.InboundRequestManager) {
      setVariableValue("IBRequestManager", 0, $arg)
    }
    
    property get Request () : InboundRequest_Ext {
      return getVariableValue("Request", 0) as InboundRequest_Ext
    }
    
    property set Request ($arg :  InboundRequest_Ext) {
      setVariableValue("Request", 0, $arg)
    }
    
    property get claim () : Claim {
      return getVariableValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setVariableValue("claim", 0, $arg)
    }
    
    property get folder () : ShareBase_Ext {
      return getVariableValue("folder", 0) as ShareBase_Ext
    }
    
    property set folder ($arg :  ShareBase_Ext) {
      setVariableValue("folder", 0, $arg)
    }
    
    property get wizard () : gw.api.web.wizard.WizardInfo {
      return getVariableValue("wizard", 0) as gw.api.web.wizard.WizardInfo
    }
    
    property set wizard ($arg :  gw.api.web.wizard.WizardInfo) {
      setVariableValue("wizard", 0, $arg)
    }
    
    function createRequest() : InboundRequest_Ext {
      var request = new InboundRequest_Ext() {
          :Claim = claim
      }
    
      // if current user has an email, add them to the request
      if( User.util.CurrentUser.Contact.EmailAddress1 != null ){
        request.addRecipient(User.util.CurrentUser.Contact)
      }
    
      return request
    }
    
    
  }
  
  
}