package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/NewClaimPolicyGeneralPanelSet.Trav.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewClaimPolicyGeneralPanelSet_TravExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/NewClaimPolicyGeneralPanelSet.Trav.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewClaimPolicyGeneralPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_103 () : void {
      AddressBookPickerPopup.push(statictypeof (Policy.policyholder), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_105 () : void {
      if (Policy.policyholder != null) { ClaimContactDetailPopup.push(Policy.policyholder, Policy.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_108 () : void {
      ClaimContactDetailPopup.push(Policy.policyholder, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_128 () : void {
      AddressBookPickerPopup.push(statictypeof (Policy.doingbusinessas), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_130 () : void {
      if (Policy.doingbusinessas != null) { ClaimContactDetailPopup.push(Policy.doingbusinessas, Policy.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_133 () : void {
      ClaimContactDetailPopup.push(Policy.doingbusinessas, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_167 () : void {
      AddressBookPickerPopup.push(statictypeof (Policy.agent), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_169 () : void {
      if (Policy.agent != null) { ClaimContactDetailPopup.push(Policy.agent, Policy.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_172 () : void {
      ClaimContactDetailPopup.push(Policy.agent, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_202 () : void {
      AddressBookPickerPopup.push(statictypeof (Policy.underwriter), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_204 () : void {
      if (Policy.underwriter != null) { ClaimContactDetailPopup.push(Policy.underwriter, Policy.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_207 () : void {
      ClaimContactDetailPopup.push(Policy.underwriter, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_67 () : void {
      AddressBookPickerPopup.push(statictypeof (Policy.insured), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_69 () : void {
      if (Policy.insured != null) { ClaimContactDetailPopup.push(Policy.insured, Policy.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_71 () : void {
      ClaimContactDetailPopup.push(Policy.insured, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_104 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Policy.policyholder), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_109 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Policy.policyholder, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_129 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Policy.doingbusinessas), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_134 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Policy.doingbusinessas, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_168 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Policy.agent), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_173 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Policy.agent, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_203 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Policy.underwriter), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_208 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Policy.underwriter, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_68 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Policy.insured), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_72 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Policy.insured, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_100 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Policy.policyholder), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_125 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Policy.doingbusinessas), null, Policy.Claim)
    }
    
    // 'def' attribute on ListViewInput at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 132, column 37
    function def_onEnter_153 (def :  pcf.EditableAdditionalInsuredLV) : void {
      def.onEnter(Policy.getClaimContactRolesByRole(TC_COVEREDPARTY), Policy.Claim, Policy, TC_COVEREDPARTY)
    }
    
    // 'def' attribute on ListViewInput at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 146, column 37
    function def_onEnter_159 (def :  pcf.EditableExcludedPartiesLV) : void {
      def.onEnter(Policy.getClaimContactRolesByRole(TC_EXCLUDEDPARTY), Policy, TC_EXCLUDEDPARTY)
    }
    
    // 'def' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_164 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Policy.agent), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_199 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Policy.underwriter), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_64 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Policy.insured), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_101 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Policy.policyholder), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_126 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Policy.doingbusinessas), null, Policy.Claim)
    }
    
    // 'def' attribute on ListViewInput at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 132, column 37
    function def_refreshVariables_154 (def :  pcf.EditableAdditionalInsuredLV) : void {
      def.refreshVariables(Policy.getClaimContactRolesByRole(TC_COVEREDPARTY), Policy.Claim, Policy, TC_COVEREDPARTY)
    }
    
    // 'def' attribute on ListViewInput at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 146, column 37
    function def_refreshVariables_160 (def :  pcf.EditableExcludedPartiesLV) : void {
      def.refreshVariables(Policy.getClaimContactRolesByRole(TC_EXCLUDEDPARTY), Policy, TC_EXCLUDEDPARTY)
    }
    
    // 'def' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_165 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Policy.agent), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_200 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Policy.underwriter), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_65 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Policy.insured), null, Policy.Claim)
    }
    
    // 'value' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_112 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.policyholder = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_137 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.doingbusinessas = (__VALUE_TO_SET as entity.Company)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 36, column 38
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_176 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.agent = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TextInput (id=ProducerCode_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 170, column 38
    function defaultSetter_191 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.ProducerCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_211 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.underwriter = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on TypeKeyInput (id=Underwriting_Company_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 193, column 38
    function defaultSetter_227 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.UnderwritingCo = (__VALUE_TO_SET as typekey.UnderwritingCompanyType)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 44, column 38
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Underwriting_Group_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 200, column 38
    function defaultSetter_235 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.UnderwritingGroup = (__VALUE_TO_SET as typekey.UnderwritingGroupType)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Other_ForeignCoverage_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 216, column 38
    function defaultSetter_250 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.ForeignCoverage = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Other_OtherFinancialInterests_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 222, column 38
    function defaultSetter_258 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.FinancialInterests = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Other_Notes_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 228, column 38
    function defaultSetter_266 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.Notes = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=CancellationDate_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 50, column 38
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.CancellationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=OrigEffectiveDate_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 56, column 38
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.OrigEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 63, column 38
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.Status = (__VALUE_TO_SET as typekey.PolicyStatus)
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 71, column 88
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_75 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.insured = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 104, column 40
    function defaultSetter_95 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on DateInput (id=EffectiveDate_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 36, column 38
    function editable_11 () : java.lang.Boolean {
      return !Policy.Verified
    }
    
    // 'editable' attribute on TypeKeyInput (id=Currency_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 71, column 88
    function editable_53 () : java.lang.Boolean {
      return !Policy.Verified and Policy.CurrencyEditable
    }
    
    // 'onPick' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_110 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Policy.policyholder); var result = eval("Policy.policyholder = Policy.Claim.resolveContact(Policy.policyholder) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_135 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Policy.doingbusinessas); var result = eval("Policy.doingbusinessas = Policy.Claim.resolveContact(Policy.doingbusinessas) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_174 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Policy.agent); var result = eval("Policy.agent = Policy.Claim.resolveContact(Policy.agent) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_209 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Policy.underwriter); var result = eval("Policy.underwriter = Policy.Claim.resolveContact(Policy.underwriter) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_73 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Policy.insured); var result = eval("Policy.insured = Policy.Claim.resolveContact(Policy.insured) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'value' attribute on Reflect at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 92, column 55
    function reflectionValue_85 (TRIGGER_INDEX :  int, VALUE :  entity.Contact) : java.lang.Object {
      return VALUE.PrimaryAddressDisplayValue
    }
    
    // 'required' attribute on TypeKeyInput (id=Currency_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 71, column 88
    function required_55 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'validationExpression' attribute on ListViewInput at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 132, column 37
    function validationExpression_151 () : java.lang.Object {
      return Policy.checkCoveredPartyConstraints()
    }
    
    // 'validationExpression' attribute on ListViewInput at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 146, column 37
    function validationExpression_157 () : java.lang.Object {
      return Policy.checkExcludedPartyConstraints()
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 44, column 38
    function validationExpression_20 () : java.lang.Object {
      return Policy.EffectiveDate != null and Policy.ExpirationDate != null and Policy.ExpirationDate < Policy.EffectiveDate ? DisplayKey.get("Java.Validation.AdminCatastrophe.Date.ForbidValidReverse") : null
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_139 () : java.lang.Object {
      return Policy.Claim.RelatedCompanyArray
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_213 () : java.lang.Object {
      return Policy.Claim.RelatedPersonArray
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_77 () : java.lang.Object {
      return Policy.Claim.RelatedContacts
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 22, column 38
    function valueRoot_3 () : java.lang.Object {
      return Policy
    }
    
    // 'value' attribute on TextInput (id=Insured_Address_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 89, column 61
    function valueRoot_88 () : java.lang.Object {
      return Policy.insured
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 98, column 67
    function valueRoot_91 () : java.lang.Object {
      return Policy.PolicyAccount.AccountHolder
    }
    
    // 'value' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_111 () : entity.Contact {
      return Policy.policyholder
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 36, column 38
    function value_13 () : java.util.Date {
      return Policy.EffectiveDate
    }
    
    // 'value' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_136 () : entity.Company {
      return Policy.doingbusinessas
    }
    
    // 'value' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_175 () : entity.Contact {
      return Policy.agent
    }
    
    // 'value' attribute on TextInput (id=ProducerCode_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 170, column 38
    function value_190 () : java.lang.String {
      return Policy.ProducerCode
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 22, column 38
    function value_2 () : java.lang.String {
      return Policy.PolicyNumber
    }
    
    // 'value' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_210 () : entity.Person {
      return Policy.underwriter
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 44, column 38
    function value_22 () : java.util.Date {
      return Policy.ExpirationDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Underwriting_Company_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 193, column 38
    function value_226 () : typekey.UnderwritingCompanyType {
      return Policy.UnderwritingCo
    }
    
    // 'value' attribute on TypeKeyInput (id=Underwriting_Group_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 200, column 38
    function value_234 () : typekey.UnderwritingGroupType {
      return Policy.UnderwritingGroup
    }
    
    // 'value' attribute on BooleanRadioInput (id=Other_ForeignCoverage_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 216, column 38
    function value_249 () : java.lang.Boolean {
      return Policy.ForeignCoverage
    }
    
    // 'value' attribute on TextInput (id=Other_OtherFinancialInterests_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 222, column 38
    function value_257 () : java.lang.String {
      return Policy.FinancialInterests
    }
    
    // 'value' attribute on TextInput (id=Other_Notes_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 228, column 38
    function value_265 () : java.lang.String {
      return Policy.Notes
    }
    
    // 'value' attribute on DateInput (id=CancellationDate_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 50, column 38
    function value_31 () : java.util.Date {
      return Policy.CancellationDate
    }
    
    // 'value' attribute on DateInput (id=OrigEffectiveDate_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 56, column 38
    function value_39 () : java.util.Date {
      return Policy.OrigEffectiveDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 63, column 38
    function value_47 () : typekey.PolicyStatus {
      return Policy.Status
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 71, column 88
    function value_56 () : typekey.Currency {
      return Policy.Currency
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 29, column 38
    function value_7 () : typekey.PolicyType {
      return Policy.PolicyType
    }
    
    // 'value' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_74 () : entity.Contact {
      return Policy.insured
    }
    
    // 'value' attribute on TextInput (id=Insured_Address_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 89, column 61
    function value_87 () : java.lang.String {
      return Policy.insured.PrimaryAddressDisplayValue
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 98, column 67
    function value_90 () : java.lang.String {
      return Policy.PolicyAccount.AccountHolder.DisplayName
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 104, column 40
    function value_94 () : java.lang.String {
      return Policy.AccountNumber
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_115 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_115 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_115 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_140 ($$arg :  entity.Company[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_140 ($$arg :  gw.api.database.IQueryBeanResult<entity.Company>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_140 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_179 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_179 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_179 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_214 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_214 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_214 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_78 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_78 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_78 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_116 () : void {
      var __valueRangeArg = Policy.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_115(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_141 () : void {
      var __valueRangeArg = Policy.Claim.RelatedCompanyArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_140(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_180 () : void {
      var __valueRangeArg = Policy.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_179(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_215 () : void {
      var __valueRangeArg = Policy.Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_214(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_79 () : void {
      var __valueRangeArg = Policy.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_78(__valueRangeArg)
    }
    
    // 'valueType' attribute on ClaimContactInput (id=DBA_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 123, column 46
    function verifyValueType_149 () : void {
      var __valueTypeArg : entity.Company
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Underwriter_Name_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 186, column 38
    function verifyValueType_223 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'visible' attribute on Label at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 16, column 38
    function visible_0 () : java.lang.Boolean {
      return Policy.Verified
    }
    
    // 'visible' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_102 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Policy.policyholder), Policy.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function visible_107 () : java.lang.Boolean {
      return Policy.CommercialPolicy
    }
    
    // 'visible' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_127 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Policy.doingbusinessas), Policy.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_166 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Policy.agent), Policy.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_201 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Policy.underwriter), Policy.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on TypeKeyInput (id=Currency_Input) at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 71, column 88
    function visible_54 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode() and Policy.Verified
    }
    
    // 'visible' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_63 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_66 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Policy.insured), Policy.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    property get Policy () : Policy {
      return getRequireValue("Policy", 0) as Policy
    }
    
    property set Policy ($arg :  Policy) {
      setRequireValue("Policy", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/NewClaimPolicyGeneralPanelSet.Trav.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyCoverageListDetailExpressionsImpl extends NewClaimPolicyGeneralPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 238, column 35
    function def_onEnter_273 (def :  pcf.EditableTravelPolicyCoveragesLV) : void {
      def.onEnter(Policy)
    }
    
    // 'def' attribute on PanelRef at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 250, column 36
    function def_onEnter_276 (def :  pcf.ClaimPolicyCovTermsCV) : void {
      def.onEnter(Coverage)
    }
    
    // 'def' attribute on PanelRef at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 238, column 35
    function def_refreshVariables_274 (def :  pcf.EditableTravelPolicyCoveragesLV) : void {
      def.refreshVariables(Policy)
    }
    
    // 'def' attribute on PanelRef at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 250, column 36
    function def_refreshVariables_277 (def :  pcf.ClaimPolicyCovTermsCV) : void {
      def.refreshVariables(Coverage)
    }
    
    // 'editable' attribute on PanelRef at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 238, column 35
    function editable_271 () : java.lang.Boolean {
      return !Policy.Verified
    }
    
    // 'visible' attribute on PanelRef at NewClaimPolicyGeneralPanelSet.Trav.pcf: line 238, column 35
    function visible_272 () : java.lang.Boolean {
      return Policy.Verified
    }
    
    property get Coverage () : Coverage {
      return getCurrentSelection(1) as Coverage
    }
    
    property set Coverage ($arg :  Coverage) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}