package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/lossdetails/sidetails/SIDetails.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SIDetailsExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/lossdetails/sidetails/SIDetails.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntryExpressionsImpl extends SIDetailsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at SIDetails.pcf: line 73, column 55
    function def_onEnter_17 (def :  pcf.QuestionSetLV) : void {
      def.onEnter(QuestionSet, Claim)
    }
    
    // 'def' attribute on PanelRef at SIDetails.pcf: line 73, column 55
    function def_refreshVariables_18 (def :  pcf.QuestionSetLV) : void {
      def.refreshVariables(QuestionSet, Claim)
    }
    
    // 'label' attribute on TextInput (id=SIUQuestionSetTotalScore_Input) at SIDetails.pcf: line 86, column 48
    function label_19 () : java.lang.Object {
      return QuestionSet.Name + " Score:"
    }
    
    // 'title' attribute on TitleBar (id=QuestionSetTitle) at SIDetails.pcf: line 76, column 43
    function title_15 () : java.lang.String {
      return QuestionSet.Name
    }
    
    // 'value' attribute on TextInput (id=SIUQuestionSetTotalScore_Input) at SIDetails.pcf: line 86, column 48
    function value_20 () : java.lang.Integer {
      return util.QuestionUtils.getQuestionSetTotalScore(Claim, QuestionSet)
    }
    
    // 'visible' attribute on PanelRef at SIDetails.pcf: line 73, column 55
    function visible_16 () : java.lang.Boolean {
      return Claim.Type_TDIC != ClaimType_TDIC.TC_PROPERTY ? true : QuestionSet.QuestionSetType.equals(Claim.SIU_Exposure_TDIC)
    }
    
    property get QuestionSet () : entity.QuestionSet {
      return getIteratedValue(1) as entity.QuestionSet
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/lossdetails/sidetails/SIDetails.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SIDetailsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Claim :  Claim) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=SIDetails) at SIDetails.pcf: line 9, column 61
    function canEdit_48 () : java.lang.Boolean {
      return perm.Claim.edit(Claim)
    }
    
    // 'canVisit' attribute on Page (id=SIDetails) at SIDetails.pcf: line 9, column 61
    static function canVisit_49 (Claim :  Claim) : java.lang.Boolean {
      return perm.Claim.view(Claim) and perm.System.viewclaimbasics
    }
    
    // 'def' attribute on PanelRef at SIDetails.pcf: line 29, column 39
    function def_onEnter_2 (def :  pcf.SItriggersLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on PanelRef at SIDetails.pcf: line 29, column 39
    function def_refreshVariables_3 (def :  pcf.SItriggersLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'value' attribute on TypeKeyInput (id=exposure_Input) at SIDetails.pcf: line 59, column 75
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.SIU_Exposure_TDIC = (__VALUE_TO_SET as QuestionSetType)
    }
    
    // 'value' attribute on TypeKeyInput (id=SIinfo_SIescalateSIU_Input) at SIDetails.pcf: line 113, column 43
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.SIEscalateSIU = (__VALUE_TO_SET as typekey.YesNo)
    }
    
    // 'value' attribute on TextInput (id=SIinfo_SIEscalateSIUNote_Input) at SIDetails.pcf: line 127, column 60
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.SIEscalateSIUNote = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TypeKeyInput (id=SIinfo_SIescalateSIU_Input) at SIDetails.pcf: line 113, column 43
    function editable_29 () : java.lang.Boolean {
      return perm.System.editSensSIUdetails and Claim.SIEscalateSIU == TC_NO 
    }
    
    // 'editable' attribute on TextInput (id=SIinfo_SIEscalateSIUNote_Input) at SIDetails.pcf: line 127, column 60
    function editable_40 () : java.lang.Boolean {
      return perm.System.editSensSIUdetails
    }
    
    // 'initialValue' attribute on Variable at SIDetails.pcf: line 18, column 29
    function initialValue_0 () : QuestionSet[] {
      return Claim.getQuestionSets(util.QuestionUtils.getAppropriateQuestionSet(Claim))
    }
    
    // EditButtons at SIDetails.pcf: line 22, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on TextInput (id=SIinfo_SITotalScore_Input) at SIDetails.pcf: line 104, column 48
    function label_25 () : java.lang.Object {
      return DisplayKey.get("NVV.Claim.SubView.SIinfo.SITotalScore", ScriptParameters.SpecialInvestigation_CreateActivityForSupervisorThreshold)
    }
    
    // 'onChange' attribute on PostOnChange at SIDetails.pcf: line 61, column 86
    function onChange_7 () : void {
      util.QuestionUtils.clearSelected(QuestionSets,Claim)
    }
    
    // Page (id=SIDetails) at SIDetails.pcf: line 9, column 61
    static function parent_50 (Claim :  Claim) : pcf.api.Destination {
      return pcf.ClaimLossDetailsGroup.createDestination(Claim)
    }
    
    // 'value' attribute on TextInput (id=SIinfo_SIscore_Input) at SIDetails.pcf: line 40, column 48
    function valueRoot_5 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on PanelIterator (id=SIUQuestionSetIterator) at SIDetails.pcf: line 70, column 46
    function value_24 () : entity.QuestionSet[] {
      return QuestionSets
    }
    
    // 'value' attribute on TextInput (id=SIinfo_SITotalScore_Input) at SIDetails.pcf: line 104, column 48
    function value_26 () : java.lang.Integer {
      return util.SIUTotalScore.getSIUTotalScore(Claim)
    }
    
    // 'value' attribute on TypeKeyInput (id=SIinfo_SIescalateSIU_Input) at SIDetails.pcf: line 113, column 43
    function value_30 () : typekey.YesNo {
      return Claim.SIEscalateSIU
    }
    
    // 'value' attribute on DateInput (id=SIinfo_SIescalateSIUdate_Input) at SIDetails.pcf: line 120, column 60
    function value_36 () : java.util.Date {
      return Claim.SIEscalateSIUdate
    }
    
    // 'value' attribute on TextInput (id=SIinfo_SIscore_Input) at SIDetails.pcf: line 40, column 48
    function value_4 () : java.lang.Integer {
      return Claim.SIScore
    }
    
    // 'value' attribute on TextInput (id=SIinfo_SIEscalateSIUNote_Input) at SIDetails.pcf: line 127, column 60
    function value_42 () : java.lang.String {
      return Claim.SIEscalateSIUNote
    }
    
    // 'value' attribute on TypeKeyInput (id=exposure_Input) at SIDetails.pcf: line 59, column 75
    function value_9 () : QuestionSetType {
      return Claim.SIU_Exposure_TDIC
    }
    
    // 'valueType' attribute on TypeKeyInput (id=exposure_Input) at SIDetails.pcf: line 59, column 75
    function verifyValueType_14 () : void {
      var __valueTypeArg : QuestionSetType
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    // 'visible' attribute on DateInput (id=SIinfo_SIescalateSIUdate_Input) at SIDetails.pcf: line 120, column 60
    function visible_35 () : java.lang.Boolean {
      return  Claim.SIEscalateSIU == TC_YES 
    }
    
    // 'visible' attribute on TypeKeyInput (id=exposure_Input) at SIDetails.pcf: line 59, column 75
    function visible_8 () : java.lang.Boolean {
      return Claim.Type_TDIC == ClaimType_TDIC.TC_PROPERTY
    }
    
    property get Claim () : Claim {
      return getVariableValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setVariableValue("Claim", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.SIDetails {
      return super.CurrentLocation as pcf.SIDetails
    }
    
    property get QuestionSets () : QuestionSet[] {
      return getVariableValue("QuestionSets", 0) as QuestionSet[]
    }
    
    property set QuestionSets ($arg :  QuestionSet[]) {
      setVariableValue("QuestionSets", 0, $arg)
    }
    
    
  }
  
  
}