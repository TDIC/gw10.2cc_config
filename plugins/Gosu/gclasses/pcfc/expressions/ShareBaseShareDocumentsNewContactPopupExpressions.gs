package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsNewContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseShareDocumentsNewContactPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsNewContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseShareDocumentsNewContactPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (claim :  Claim, contactSubtype :  typekey.Contact, share :  OutboundSharing_Ext) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=ShareBaseShareDocumentsNewContactPopup) at ShareBaseShareDocumentsNewContactPopup.pcf: line 10, column 95
    function beforeCommit_8 (pickedValue :  java.lang.Object) : void {
      share.addRecipient(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseShareDocumentsNewContactPopup.pcf: line 28, column 31
    function def_onEnter_1 (def :  pcf.ShareBaseNewContactDetailScreen_Company) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseShareDocumentsNewContactPopup.pcf: line 28, column 31
    function def_onEnter_3 (def :  pcf.ShareBaseNewContactDetailScreen_Person) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseShareDocumentsNewContactPopup.pcf: line 28, column 31
    function def_onEnter_5 (def :  pcf.ShareBaseNewContactDetailScreen_Place) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseShareDocumentsNewContactPopup.pcf: line 28, column 31
    function def_refreshVariables_2 (def :  pcf.ShareBaseNewContactDetailScreen_Company) : void {
      def.refreshVariables(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseShareDocumentsNewContactPopup.pcf: line 28, column 31
    function def_refreshVariables_4 (def :  pcf.ShareBaseNewContactDetailScreen_Person) : void {
      def.refreshVariables(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseShareDocumentsNewContactPopup.pcf: line 28, column 31
    function def_refreshVariables_6 (def :  pcf.ShareBaseNewContactDetailScreen_Place) : void {
      def.refreshVariables(contact)
    }
    
    // 'initialValue' attribute on Variable at ShareBaseShareDocumentsNewContactPopup.pcf: line 25, column 30
    function initialValue_0 () : entity.Contact {
      return gw.api.contact.NewContactUtil.newContactFromSubtype(contactSubtype)
    }
    
    // 'mode' attribute on ScreenRef at ShareBaseShareDocumentsNewContactPopup.pcf: line 28, column 31
    function mode_7 () : java.lang.Object {
      return contact.Subtype
    }
    
    // 'title' attribute on Popup (id=ShareBaseShareDocumentsNewContactPopup) at ShareBaseShareDocumentsNewContactPopup.pcf: line 10, column 95
    static function title_9 (claim :  Claim, contactSubtype :  typekey.Contact, share :  OutboundSharing_Ext) : java.lang.Object {
      return gw.api.contact.NewContactUtil.getDisplayKeyForContactSubtype(contactSubtype.Code)
    }
    
    override property get CurrentLocation () : pcf.ShareBaseShareDocumentsNewContactPopup {
      return super.CurrentLocation as pcf.ShareBaseShareDocumentsNewContactPopup
    }
    
    property get claim () : Claim {
      return getVariableValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setVariableValue("claim", 0, $arg)
    }
    
    property get contact () : entity.Contact {
      return getVariableValue("contact", 0) as entity.Contact
    }
    
    property set contact ($arg :  entity.Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get contactSubtype () : typekey.Contact {
      return getVariableValue("contactSubtype", 0) as typekey.Contact
    }
    
    property set contactSubtype ($arg :  typekey.Contact) {
      setVariableValue("contactSubtype", 0, $arg)
    }
    
    property get share () : OutboundSharing_Ext {
      return getVariableValue("share", 0) as OutboundSharing_Ext
    }
    
    property set share ($arg :  OutboundSharing_Ext) {
      setVariableValue("share", 0, $arg)
    }
    
    
  }
  
  
}