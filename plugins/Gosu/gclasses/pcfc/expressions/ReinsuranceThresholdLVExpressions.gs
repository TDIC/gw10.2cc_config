package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/reinsthreshold/ReinsuranceThresholdLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ReinsuranceThresholdLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/reinsthreshold/ReinsuranceThresholdLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ReinsuranceThresholdLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at ReinsuranceThresholdLV.pcf: line 21, column 87
    function checkBoxVisible_34 () : java.lang.Boolean {
      return CurrentLocation.InEditMode ? true : false
    }
    
    // 'value' attribute on TextCell (id=ThresholdValue_Cell) at ReinsuranceThresholdLV.pcf: line 49, column 45
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      ReinsuranceThreshold.ThresholdValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TextCell (id=ReportingThreshold_Cell) at ReinsuranceThresholdLV.pcf: line 56, column 42
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      ReinsuranceThreshold.ReportingThreshold = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at ReinsuranceThresholdLV.pcf: line 62, column 51
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      ReinsuranceThreshold.StartDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCell (id=EndDate_Cell) at ReinsuranceThresholdLV.pcf: line 69, column 49
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      ReinsuranceThreshold.EndDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyCell (id=TreatyType_Cell) at ReinsuranceThresholdLV.pcf: line 29, column 53
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      ReinsuranceThreshold.TreatyType = (__VALUE_TO_SET as typekey.ReinsuranceTreatyType)
    }
    
    // 'filter' attribute on TypeKeyCell (id=PolicyType_Cell) at ReinsuranceThresholdLV.pcf: line 39, column 42
    function filter_14 (VALUE :  typekey.PolicyType, VALUES :  typekey.PolicyType[]) : java.lang.Boolean {
      if(ReinsuranceThreshold.TreatyType == TC_WC) { return VALUE == PolicyType.TC_WC7WORKERSCOMP } else if (ReinsuranceThreshold.TreatyType == TC_PROP) { return VALUE != TC_WORKERSCOMP } else { return true }
    }
    
    // 'onChange' attribute on PostOnChange at ReinsuranceThresholdLV.pcf: line 41, column 208
    function onChange_11 () : void {
      ReinsuranceThreshold.LossType = ReinsuranceThreshold.TreatyType == TC_WC  ? LossType.TC_WC7 : gw.api.policy.PolicyTypeUtil.getLossTypeForPolicyType( ReinsuranceThreshold.PolicyType )
    }
    
    // 'onChange' attribute on PostOnChange at ReinsuranceThresholdLV.pcf: line 31, column 65
    function onChange_6 () : void {
      defaultPolicyType(ReinsuranceThreshold)
    }
    
    // 'validationExpression' attribute on DateCell (id=EndDate_Cell) at ReinsuranceThresholdLV.pcf: line 69, column 49
    function validationExpression_28 () : java.lang.Object {
      return ReinsuranceThreshold.EndDate == null or ReinsuranceThreshold.StartDate == null or (ReinsuranceThreshold.StartDate.compareTo( ReinsuranceThreshold.EndDate ) <= 0) ? null : DisplayKey.get("LV.ReinsuranceThreshold.EndDate.ValidationError")
    }
    
    // 'value' attribute on TypeKeyCell (id=TreatyType_Cell) at ReinsuranceThresholdLV.pcf: line 29, column 53
    function valueRoot_9 () : java.lang.Object {
      return ReinsuranceThreshold
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyType_Cell) at ReinsuranceThresholdLV.pcf: line 39, column 42
    function value_12 () : typekey.PolicyType {
      return ReinsuranceThreshold.PolicyType
    }
    
    // 'value' attribute on TextCell (id=ThresholdValue_Cell) at ReinsuranceThresholdLV.pcf: line 49, column 45
    function value_16 () : java.math.BigDecimal {
      return ReinsuranceThreshold.ThresholdValue
    }
    
    // 'value' attribute on TextCell (id=ReportingThreshold_Cell) at ReinsuranceThresholdLV.pcf: line 56, column 42
    function value_20 () : java.lang.Integer {
      return ReinsuranceThreshold.ReportingThreshold
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at ReinsuranceThresholdLV.pcf: line 62, column 51
    function value_24 () : java.util.Date {
      return ReinsuranceThreshold.StartDate
    }
    
    // 'value' attribute on DateCell (id=EndDate_Cell) at ReinsuranceThresholdLV.pcf: line 69, column 49
    function value_29 () : java.util.Date {
      return ReinsuranceThreshold.EndDate
    }
    
    // 'value' attribute on TypeKeyCell (id=TreatyType_Cell) at ReinsuranceThresholdLV.pcf: line 29, column 53
    function value_7 () : typekey.ReinsuranceTreatyType {
      return ReinsuranceThreshold.TreatyType
    }
    
    property get ReinsuranceThreshold () : entity.ReinsuranceThreshold {
      return getIteratedValue(1) as entity.ReinsuranceThreshold
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/reinsthreshold/ReinsuranceThresholdLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ReinsuranceThresholdLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=TreatyType_Cell) at ReinsuranceThresholdLV.pcf: line 29, column 53
    function sortValue_0 (ReinsuranceThreshold :  entity.ReinsuranceThreshold) : java.lang.Object {
      return ReinsuranceThreshold.TreatyType
    }
    
    // 'value' attribute on TypeKeyCell (id=PolicyType_Cell) at ReinsuranceThresholdLV.pcf: line 39, column 42
    function sortValue_1 (ReinsuranceThreshold :  entity.ReinsuranceThreshold) : java.lang.Object {
      return ReinsuranceThreshold.PolicyType
    }
    
    // 'value' attribute on TextCell (id=ThresholdValue_Cell) at ReinsuranceThresholdLV.pcf: line 49, column 45
    function sortValue_2 (ReinsuranceThreshold :  entity.ReinsuranceThreshold) : java.lang.Object {
      return ReinsuranceThreshold.ThresholdValue
    }
    
    // 'value' attribute on TextCell (id=ReportingThreshold_Cell) at ReinsuranceThresholdLV.pcf: line 56, column 42
    function sortValue_3 (ReinsuranceThreshold :  entity.ReinsuranceThreshold) : java.lang.Object {
      return ReinsuranceThreshold.ReportingThreshold
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at ReinsuranceThresholdLV.pcf: line 62, column 51
    function sortValue_4 (ReinsuranceThreshold :  entity.ReinsuranceThreshold) : java.lang.Object {
      return ReinsuranceThreshold.StartDate
    }
    
    // 'value' attribute on DateCell (id=EndDate_Cell) at ReinsuranceThresholdLV.pcf: line 69, column 49
    function sortValue_5 (ReinsuranceThreshold :  entity.ReinsuranceThreshold) : java.lang.Object {
      return ReinsuranceThreshold.EndDate
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at ReinsuranceThresholdLV.pcf: line 21, column 87
    function toCreateAndAdd_35 () : entity.ReinsuranceThreshold {
      return new ReinsuranceThreshold()
    }
    
    // 'toRemove' attribute on RowIterator at ReinsuranceThresholdLV.pcf: line 21, column 87
    function toRemove_36 (ReinsuranceThreshold :  entity.ReinsuranceThreshold) : void {
      ReinsuranceThreshold.remove()
    }
    
    // 'value' attribute on RowIterator at ReinsuranceThresholdLV.pcf: line 21, column 87
    function value_37 () : gw.api.database.IQueryBeanResult<entity.ReinsuranceThreshold> {
      return ReinsuranceThresholdList
    }
    
    property get ReinsuranceThresholdList () : gw.api.database.IQueryBeanResult<ReinsuranceThreshold> {
      return getRequireValue("ReinsuranceThresholdList", 0) as gw.api.database.IQueryBeanResult<ReinsuranceThreshold>
    }
    
    property set ReinsuranceThresholdList ($arg :  gw.api.database.IQueryBeanResult<ReinsuranceThreshold>) {
      setRequireValue("ReinsuranceThresholdList", 0, $arg)
    }
    
    function defaultPolicyType(resThreshold:ReinsuranceThreshold){
      if(resThreshold.TreatyType == TC_WC){ 
        resThreshold.PolicyType = TC_WC7WORKERSCOMP; 
        resThreshold.LossType = TC_WC7
      } else if(resThreshold.TreatyType == ReinsuranceTreatyType.TC_PROP){
        resThreshold.PolicyType = PolicyType.TC_BUSINESSOWNERS;
        resThreshold.LossType = LossType.TC_PR
      }else if(resThreshold.TreatyType == ReinsuranceTreatyType.TC_LIAB){
        resThreshold.PolicyType = PolicyType.TC_GENERALLIABILITY;
        resThreshold.LossType = LossType.TC_GL
      }
      }
    
    
  }
  
  
}