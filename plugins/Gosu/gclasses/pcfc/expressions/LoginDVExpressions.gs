package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/login/LoginDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LoginDVExpressions {
  @javax.annotation.Generated("config/web/pcf/login/LoginDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LoginDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on LoginSubmitButton (id=submit_Input) at LoginDV.pcf: line 44, column 77
    function action_14 () : void {
      LoginForm.login()
    }
    
    // 'available' attribute on TextInput (id=username_Input) at LoginDV.pcf: line 17, column 36
    function available_0 () : java.lang.Boolean {
      return not com.tdic.util.properties.PropertyUtil.isSsoEnabled()
    }
    
    // 'value' attribute on TextInput (id=username_Input) at LoginDV.pcf: line 17, column 36
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      LoginForm.Username = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PasswordInput (id=password_Input) at LoginDV.pcf: line 24, column 37
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      LoginForm.Password = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=username_Input) at LoginDV.pcf: line 17, column 36
    function valueRoot_3 () : java.lang.Object {
      return LoginForm
    }
    
    // 'value' attribute on TextInput (id=username_Input) at LoginDV.pcf: line 17, column 36
    function value_1 () : java.lang.String {
      return LoginForm.Username
    }
    
    // 'value' attribute on PasswordInput (id=password_Input) at LoginDV.pcf: line 24, column 37
    function value_7 () : java.lang.String {
      return LoginForm.Password
    }
    
    property get LoginForm () : gw.api.util.LoginForm {
      return getRequireValue("LoginForm", 0) as gw.api.util.LoginForm
    }
    
    property set LoginForm ($arg :  gw.api.util.LoginForm) {
      setRequireValue("LoginForm", 0, $arg)
    }
    
    
  }
  
  
}