package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/medicalcasemgmt/ICDCodePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ICDCodePopupExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/medicalcasemgmt/ICDCodePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ICDCodePopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (codeParam :  String) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=SearchButton) at ICDCodePopup.pcf: line 67, column 69
    function action_1 () : void {
      codeParam = findCode; codeSearchResults = libraries.ICDCodeUtil.queryICD(findCode, findBodySystem, findDesc, find9, find10, false) 
    }
    
    // 'action' attribute on ToolbarButton (id=CancelButton) at ICDCodePopup.pcf: line 72, column 69
    function action_2 () : void {
      CurrentLocation.cancel()
    }
    
    // 'afterEnter' attribute on Popup (id=ICDCodePopup) at ICDCodePopup.pcf: line 13, column 70
    function afterEnter_38 () : void {
      codeSearchResults = libraries.ICDCodeUtil.ICDSearch( codeParam, null, false )
    }
    
    // 'value' attribute on RangeInput (id=findBodySystem_Input) at ICDCodePopup.pcf: line 101, column 50
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      findBodySystem = (__VALUE_TO_SET as typekey.ICDBodySystem)
    }
    
    // 'value' attribute on BooleanRadioInput (id=find10Ele_Input) at ICDCodePopup.pcf: line 112, column 31
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      find10 = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=find9Ele_Input) at ICDCodePopup.pcf: line 118, column 30
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      find9 = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=idcCode_Input) at ICDCodePopup.pcf: line 85, column 33
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      findCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=description_Input) at ICDCodePopup.pcf: line 92, column 33
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      findDesc = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at ICDCodePopup.pcf: line 25, column 22
    function initialValue_0 () : String {
      return codeParam
    }
    
    // 'valueRange' attribute on RangeInput (id=findBodySystem_Input) at ICDCodePopup.pcf: line 101, column 50
    function valueRange_11 () : java.lang.Object {
      return ICDBodySystem.getTypeKeys( false )
    }
    
    // 'value' attribute on BooleanRadioInput (id=find10Ele_Input) at ICDCodePopup.pcf: line 112, column 31
    function value_15 () : java.lang.Boolean {
      return find10
    }
    
    // 'value' attribute on BooleanRadioInput (id=find9Ele_Input) at ICDCodePopup.pcf: line 118, column 30
    function value_18 () : java.lang.Boolean {
      return find9
    }
    
    // 'value' attribute on TextInput (id=idcCode_Input) at ICDCodePopup.pcf: line 85, column 33
    function value_3 () : java.lang.String {
      return findCode
    }
    
    // 'value' attribute on RowIterator (id=ICDResultsIterator) at ICDCodePopup.pcf: line 135, column 80
    function value_37 () : gw.api.database.IQueryBeanResult<entity.ICDCode> {
      return codeSearchResults
    }
    
    // 'value' attribute on TextInput (id=description_Input) at ICDCodePopup.pcf: line 92, column 33
    function value_6 () : java.lang.String {
      return findDesc
    }
    
    // 'value' attribute on RangeInput (id=findBodySystem_Input) at ICDCodePopup.pcf: line 101, column 50
    function value_9 () : typekey.ICDBodySystem {
      return findBodySystem
    }
    
    // 'valueRange' attribute on RangeInput (id=findBodySystem_Input) at ICDCodePopup.pcf: line 101, column 50
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=findBodySystem_Input) at ICDCodePopup.pcf: line 101, column 50
    function verifyValueRangeIsAllowedType_12 ($$arg :  typekey.ICDBodySystem[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=findBodySystem_Input) at ICDCodePopup.pcf: line 101, column 50
    function verifyValueRange_13 () : void {
      var __valueRangeArg = ICDBodySystem.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.ICDCodePopup {
      return super.CurrentLocation as pcf.ICDCodePopup
    }
    
    property get codeParam () : String {
      return getVariableValue("codeParam", 0) as String
    }
    
    property set codeParam ($arg :  String) {
      setVariableValue("codeParam", 0, $arg)
    }
    
    property get codeSearchResults () : gw.api.database.IQueryBeanResult<ICDCode> {
      return getVariableValue("codeSearchResults", 0) as gw.api.database.IQueryBeanResult<ICDCode>
    }
    
    property set codeSearchResults ($arg :  gw.api.database.IQueryBeanResult<ICDCode>) {
      setVariableValue("codeSearchResults", 0, $arg)
    }
    
    property get find10 () : Boolean {
      return getVariableValue("find10", 0) as Boolean
    }
    
    property set find10 ($arg :  Boolean) {
      setVariableValue("find10", 0, $arg)
    }
    
    property get find9 () : Boolean {
      return getVariableValue("find9", 0) as Boolean
    }
    
    property set find9 ($arg :  Boolean) {
      setVariableValue("find9", 0, $arg)
    }
    
    property get findBodySystem () : ICDBodySystem {
      return getVariableValue("findBodySystem", 0) as ICDBodySystem
    }
    
    property set findBodySystem ($arg :  ICDBodySystem) {
      setVariableValue("findBodySystem", 0, $arg)
    }
    
    property get findCode () : String {
      return getVariableValue("findCode", 0) as String
    }
    
    property set findCode ($arg :  String) {
      setVariableValue("findCode", 0, $arg)
    }
    
    property get findDesc () : String {
      return getVariableValue("findDesc", 0) as String
    }
    
    property set findDesc ($arg :  String) {
      setVariableValue("findDesc", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/medicalcasemgmt/ICDCodePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ICDCodePopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickValue' attribute on RowIterator (id=ICDResultsIterator) at ICDCodePopup.pcf: line 135, column 80
    function pickValue_36 () : ICDCode {
      return anICDResult
    }
    
    // 'value' attribute on TextCell (id=ICDCode_Cell) at ICDCodePopup.pcf: line 141, column 43
    function valueRoot_22 () : java.lang.Object {
      return anICDResult
    }
    
    // 'value' attribute on TextCell (id=ICDCode_Cell) at ICDCodePopup.pcf: line 141, column 43
    function value_21 () : java.lang.String {
      return anICDResult.Code
    }
    
    // 'value' attribute on TypeKeyCell (id=icdEditionId_Cell) at ICDCodePopup.pcf: line 147, column 53
    function value_24 () : typekey.ICDEdition_Ext {
      return anICDResult.ICDEdition_Ext
    }
    
    // 'value' attribute on TextCell (id=CodeDesc_Cell) at ICDCodePopup.pcf: line 152, column 47
    function value_27 () : java.lang.String {
      return anICDResult.CodeDesc
    }
    
    // 'value' attribute on BooleanRadioCell (id=Chronic_Cell) at ICDCodePopup.pcf: line 157, column 46
    function value_30 () : java.lang.Boolean {
      return anICDResult.Chronic
    }
    
    // 'value' attribute on DateCell (id=ExpiryDate_Cell) at ICDCodePopup.pcf: line 167, column 49
    function value_33 () : java.util.Date {
      return anICDResult.ExpiryDate
    }
    
    property get anICDResult () : entity.ICDCode {
      return getIteratedValue(1) as entity.ICDCode
    }
    
    
  }
  
  
}