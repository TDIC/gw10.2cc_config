package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderAvailableContactsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseFolderAvailableContactsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderAvailableContactsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ShareBaseFolderAvailableContactsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=email_address_Cell) at ShareBaseFolderAvailableContactsLV.pcf: line 39, column 55
    function valueRoot_8 () : java.lang.Object {
      return (Contact as Contact)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at ShareBaseFolderAvailableContactsLV.pcf: line 34, column 39
    function value_5 () : entity.Contact {
      return Contact as Contact
    }
    
    // 'value' attribute on TextCell (id=email_address_Cell) at ShareBaseFolderAvailableContactsLV.pcf: line 39, column 55
    function value_7 () : java.lang.String {
      return (Contact as Contact).EmailAddress1
    }
    
    property get Contact () : java.lang.Object {
      return getIteratedValue(1) as java.lang.Object
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderAvailableContactsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseFolderAvailableContactsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TextCell (id=Name_Cell) at ShareBaseFolderAvailableContactsLV.pcf: line 34, column 39
    function sortValue_0 (Contact :  java.lang.Object) : java.lang.Object {
      return (Contact as Contact).PrimarySortValue
    }
    
    // 'sortBy' attribute on TextCell (id=Name_Cell) at ShareBaseFolderAvailableContactsLV.pcf: line 34, column 39
    function sortValue_1 (Contact :  java.lang.Object) : java.lang.Object {
      return (Contact as Contact).SecondarySortValue
    }
    
    // 'sortBy' attribute on TextCell (id=Name_Cell) at ShareBaseFolderAvailableContactsLV.pcf: line 34, column 39
    function sortValue_2 (Contact :  java.lang.Object) : java.lang.Object {
      return (Contact as Contact).TertiarySortValue
    }
    
    // 'sortBy' attribute on TextCell (id=Name_Cell) at ShareBaseFolderAvailableContactsLV.pcf: line 34, column 39
    function sortValue_3 (Contact :  java.lang.Object) : java.lang.Object {
      return (Contact as Contact).QuaternarySortValue
    }
    
    // 'value' attribute on TextCell (id=email_address_Cell) at ShareBaseFolderAvailableContactsLV.pcf: line 39, column 55
    function sortValue_4 (Contact :  java.lang.Object) : java.lang.Object {
      return (Contact as Contact).EmailAddress1
    }
    
    // 'value' attribute on RowIterator at ShareBaseFolderAvailableContactsLV.pcf: line 23, column 39
    function value_10 () : List<Contact> {
      return Contacts
    }
    
    // 'valueType' attribute on RowIterator at ShareBaseFolderAvailableContactsLV.pcf: line 23, column 39
    function verifyValueTypeIsAllowedType_11 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at ShareBaseFolderAvailableContactsLV.pcf: line 23, column 39
    function verifyValueTypeIsAllowedType_11 ($$arg :  gw.api.iterator.IteratorBackingData) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at ShareBaseFolderAvailableContactsLV.pcf: line 23, column 39
    function verifyValueTypeIsAllowedType_11 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at ShareBaseFolderAvailableContactsLV.pcf: line 23, column 39
    function verifyValueType_12 () : void {
      var __valueTypeArg : List<Contact>
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the valueType is not a valid type for use with an iterator
      // The valueType for an iterator must be an array or extend from List or IQueryBeanResult
      verifyValueTypeIsAllowedType_11(__valueTypeArg)
    }
    
    property get Contacts () : List<Contact> {
      return getRequireValue("Contacts", 0) as List<Contact>
    }
    
    property set Contacts ($arg :  List<Contact>) {
      setRequireValue("Contacts", 0, $arg)
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    
  }
  
  
}