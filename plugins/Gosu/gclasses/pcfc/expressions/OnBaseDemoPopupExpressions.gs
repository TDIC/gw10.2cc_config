package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/demo/OnBaseDemoPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBaseDemoPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/demo/OnBaseDemoPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseDemoPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (claim :  Claim) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=customQueryButton) at OnBaseDemoPopup.pcf: line 30, column 51
    function action_1 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUtils.constructCustomQueryURL(claim)
    }
    
    // 'action' attribute on ToolbarButton (id=onFoldersButton) at OnBaseDemoPopup.pcf: line 35, column 53
    function action_2 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUtils.constructFolderURL(claim)
    }
    
    // 'action' attribute on ToolbarButton (id=uploadButton) at OnBaseDemoPopup.pcf: line 40, column 54
    function action_3 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUtils.uploadDocURL(claim)
    }
    
    // 'action' attribute on ToolbarButton (id=uFormButton) at OnBaseDemoPopup.pcf: line 45, column 56
    function action_4 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUtils.unityFormURL(claim)
    }
    
    // 'action' attribute on ToolbarButton (id=docpacketbutton) at OnBaseDemoPopup.pcf: line 50, column 54
    function action_5 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUtils.docPacketURL(claim, "CLM - Claim Packet")
    }
    
    // 'action' attribute on ToolbarButton (id=cvubutton) at OnBaseDemoPopup.pcf: line 55, column 54
    function action_6 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUtils.combinedViewerURL(claim)
    }
    
    // 'def' attribute on PanelRef at OnBaseDemoPopup.pcf: line 23, column 55
    function def_onEnter_7 (def :  pcf.OnBaseAePopFramePanelSet) : void {
      def.onEnter(scrapeXml)
    }
    
    // 'def' attribute on PanelRef at OnBaseDemoPopup.pcf: line 23, column 55
    function def_refreshVariables_8 (def :  pcf.OnBaseAePopFramePanelSet) : void {
      def.refreshVariables(scrapeXml)
    }
    
    // 'initialValue' attribute on Variable at OnBaseDemoPopup.pcf: line 16, column 24
    function initialValue_0 () : String[] {
      return new String[]{""}
    }
    
    override property get CurrentLocation () : pcf.OnBaseDemoPopup {
      return super.CurrentLocation as pcf.OnBaseDemoPopup
    }
    
    property get claim () : Claim {
      return getVariableValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setVariableValue("claim", 0, $arg)
    }
    
    property get scrapeXml () : String[] {
      return getVariableValue("scrapeXml", 0) as String[]
    }
    
    property set scrapeXml ($arg :  String[]) {
      setVariableValue("scrapeXml", 0, $arg)
    }
    
    
  }
  
  
}