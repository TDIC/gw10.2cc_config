package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseShareDocumentsReviewScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseShareDocumentsReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseShareDocumentsReviewScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at ShareBaseShareDocumentsReviewScreen.pcf: line 41, column 25
    function def_onEnter_6 (def :  pcf.ShareBaseFolderRequestContactsLV) : void {
      def.onEnter(claim, share.LinkedRecipients, false)
    }
    
    // 'def' attribute on PanelRef (id=SelectedDocs) at ShareBaseShareDocumentsReviewScreen.pcf: line 51, column 25
    function def_onEnter_8 (def :  pcf.ShareBaseShareDocumentsSelectedDocsLV) : void {
      def.onEnter(claim, share.DocumentLinks, false)
    }
    
    // 'def' attribute on PanelRef at ShareBaseShareDocumentsReviewScreen.pcf: line 41, column 25
    function def_refreshVariables_7 (def :  pcf.ShareBaseFolderRequestContactsLV) : void {
      def.refreshVariables(claim, share.LinkedRecipients, false)
    }
    
    // 'def' attribute on PanelRef (id=SelectedDocs) at ShareBaseShareDocumentsReviewScreen.pcf: line 51, column 25
    function def_refreshVariables_9 (def :  pcf.ShareBaseShareDocumentsSelectedDocsLV) : void {
      def.refreshVariables(claim, share.DocumentLinks, false)
    }
    
    // 'value' attribute on TextInput (id=FolderName_Input) at ShareBaseShareDocumentsReviewScreen.pcf: line 24, column 38
    function valueRoot_1 () : java.lang.Object {
      return folder
    }
    
    // 'value' attribute on TextInput (id=FolderName_Input) at ShareBaseShareDocumentsReviewScreen.pcf: line 24, column 38
    function value_0 () : java.lang.String {
      return folder.FolderName
    }
    
    // 'value' attribute on DateInput (id=FolderExpiration_Input) at ShareBaseShareDocumentsReviewScreen.pcf: line 28, column 38
    function value_3 () : java.util.Date {
      return folder.Expiration
    }
    
    property get claim () : Claim {
      return getRequireValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setRequireValue("claim", 0, $arg)
    }
    
    property get folder () : ShareBase_Ext {
      return getRequireValue("folder", 0) as ShareBase_Ext
    }
    
    property set folder ($arg :  ShareBase_Ext) {
      setRequireValue("folder", 0, $arg)
    }
    
    property get share () : OutboundSharing_Ext {
      return getRequireValue("share", 0) as OutboundSharing_Ext
    }
    
    property set share ($arg :  OutboundSharing_Ext) {
      setRequireValue("share", 0, $arg)
    }
    
    
  }
  
  
}