package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/newtransaction/recovery/RecoveryDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RecoveryDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/newtransaction/recovery/RecoveryDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RecoveryDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_4 () : void {
      AddressBookPickerPopup.push(statictypeof (recovery.Payer), recovery.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_40 () : void {
      AddressBookPickerPopup.push(statictypeof (recovery.OnBehalfOf), recovery.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_42 () : void {
      if (recovery.OnBehalfOf != null) { ClaimContactDetailPopup.push(recovery.OnBehalfOf, recovery.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_44 () : void {
      ClaimContactDetailPopup.push(recovery.OnBehalfOf, recovery.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_6 () : void {
      if (recovery.Payer != null) { ClaimContactDetailPopup.push(recovery.Payer, recovery.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_8 () : void {
      ClaimContactDetailPopup.push(recovery.Payer, recovery.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_41 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (recovery.OnBehalfOf), recovery.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_45 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(recovery.OnBehalfOf, recovery.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_5 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (recovery.Payer), recovery.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_9 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(recovery.Payer, recovery.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_1 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (recovery.Payer), null, recovery.Claim)
    }
    
    // 'def' attribute on InputSetRef at RecoveryDetailDV.pcf: line 30, column 73
    function def_onEnter_21 (def :  pcf.ReserveLineInputSet) : void {
      def.onEnter(recovery, reserveLineInputSetHelper)
    }
    
    // 'def' attribute on InputSetRef at RecoveryDetailDV.pcf: line 51, column 58
    function def_onEnter_34 (def :  pcf.TransactionExchangeRateInputSet) : void {
      def.onEnter(recovery)
    }
    
    // 'def' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_37 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (recovery.OnBehalfOf), null, recovery.Claim)
    }
    
    // 'def' attribute on ListViewInput at RecoveryDetailDV.pcf: line 103, column 27
    function def_onEnter_86 (def :  pcf.EditableRecoveryLineItemsLV) : void {
      def.onEnter(recovery)
    }
    
    // 'def' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_2 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (recovery.Payer), null, recovery.Claim)
    }
    
    // 'def' attribute on InputSetRef at RecoveryDetailDV.pcf: line 30, column 73
    function def_refreshVariables_22 (def :  pcf.ReserveLineInputSet) : void {
      def.refreshVariables(recovery, reserveLineInputSetHelper)
    }
    
    // 'def' attribute on InputSetRef at RecoveryDetailDV.pcf: line 51, column 58
    function def_refreshVariables_35 (def :  pcf.TransactionExchangeRateInputSet) : void {
      def.refreshVariables(recovery)
    }
    
    // 'def' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_38 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (recovery.OnBehalfOf), null, recovery.Claim)
    }
    
    // 'def' attribute on ListViewInput at RecoveryDetailDV.pcf: line 103, column 27
    function def_refreshVariables_87 (def :  pcf.EditableRecoveryLineItemsLV) : void {
      def.refreshVariables(recovery)
    }
    
    // 'value' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      recovery.Payer = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TypeKeyInput (id=RecoveryCategory_Input) at RecoveryDetailDV.pcf: line 38, column 46
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      recovery.RecoveryCategory = (__VALUE_TO_SET as typekey.RecoveryCategory)
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at RecoveryDetailDV.pcf: line 47, column 66
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      recoveryHelper.RecoveryCurrency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      recovery.OnBehalfOf = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TextInput (id=Comments_Input) at RecoveryDetailDV.pcf: line 68, column 36
    function defaultSetter_59 (__VALUE_TO_SET :  java.lang.Object) : void {
      recovery.Comments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=RecoveryCheckNumber_Input) at RecoveryDetailDV.pcf: line 95, column 52
    function defaultSetter_79 (__VALUE_TO_SET :  java.lang.Object) : void {
      recovery.CheckNumber_Ext = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on ListViewInput at RecoveryDetailDV.pcf: line 103, column 27
    function editable_85 () : java.lang.Boolean {
      return recovery.ReserveLine != null
    }
    
    // 'filter' attribute on TypeKeyInput (id=RecoveryCategory_Input) at RecoveryDetailDV.pcf: line 38, column 46
    function filter_26 (VALUE :  typekey.RecoveryCategory, VALUES :  typekey.RecoveryCategory[]) : java.lang.Boolean {
      return (VALUE == RecoveryCategory.TC_CREDIT_EXP or VALUE == RecoveryCategory.TC_CREDIT_LOSS) ? false : true 
    }
    
    // 'onChange' attribute on ClaimContactInput (id=Payer_Input) at RecoveryDetailDV.pcf: line 27, column 64
    function onChange_7 () : void {
      recoveryHelper.autoSetRecoveryCurrency()
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_10 (PickedValue :  Contact) : void {
      var contactType = statictypeof (recovery.Payer); var result = eval("recovery.Payer = recovery.Claim.resolveContact(recovery.Payer) as " + contactType.Name + ";return null;"); recoveryHelper.autoSetRecoveryCurrency();
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Payer_Input) at RecoveryDetailDV.pcf: line 25, column 53
    function onPick_17 (PickedValue :  java.lang.Object) : void {
      recoveryHelper.autoSetRecoveryCurrency()
    }
    
    // 'onPick' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_46 (PickedValue :  Contact) : void {
      var contactType = statictypeof (recovery.OnBehalfOf); var result = eval("recovery.OnBehalfOf = recovery.Claim.resolveContact(recovery.OnBehalfOf) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_14 () : java.lang.Object {
      return recovery.Claim.RelatedContacts
    }
    
    // 'value' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRoot_13 () : java.lang.Object {
      return recovery
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at RecoveryDetailDV.pcf: line 47, column 66
    function valueRoot_31 () : java.lang.Object {
      return recoveryHelper
    }
    
    // 'value' attribute on CurrencyInput (id=OpenRecoveryReserves_Input) at RecoveryDetailDV.pcf: line 75, column 147
    function valueRoot_64 () : java.lang.Object {
      return gw.api.financials.FinancialsCalculationsForEditedTransaction.getOpenRecoveryReserves(recovery)
    }
    
    // 'value' attribute on CurrencyInput (id=Payments_Input) at RecoveryDetailDV.pcf: line 82, column 95
    function valueRoot_69 () : java.lang.Object {
      return gw.api.financials.FinancialsCalculationsForEditedTransaction.getTotalPayments(recovery)
    }
    
    // 'value' attribute on CurrencyInput (id=PastRecoveries_Input) at RecoveryDetailDV.pcf: line 89, column 97
    function valueRoot_74 () : java.lang.Object {
      return gw.api.financials.FinancialsCalculationsForEditedTransaction.getTotalRecoveries(recovery)
    }
    
    // 'value' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_11 () : entity.Contact {
      return recovery.Payer
    }
    
    // 'value' attribute on TypeKeyInput (id=RecoveryCategory_Input) at RecoveryDetailDV.pcf: line 38, column 46
    function value_23 () : typekey.RecoveryCategory {
      return recovery.RecoveryCategory
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at RecoveryDetailDV.pcf: line 47, column 66
    function value_29 () : typekey.Currency {
      return recoveryHelper.RecoveryCurrency
    }
    
    // 'value' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_47 () : entity.Contact {
      return recovery.OnBehalfOf
    }
    
    // 'value' attribute on TextInput (id=Comments_Input) at RecoveryDetailDV.pcf: line 68, column 36
    function value_58 () : java.lang.String {
      return recovery.Comments
    }
    
    // 'value' attribute on CurrencyInput (id=OpenRecoveryReserves_Input) at RecoveryDetailDV.pcf: line 75, column 147
    function value_63 () : gw.api.financials.IMoney {
      return gw.api.financials.FinancialsCalculationsForEditedTransaction.getOpenRecoveryReserves(recovery).ReservingAmount
    }
    
    // 'value' attribute on CurrencyInput (id=Payments_Input) at RecoveryDetailDV.pcf: line 82, column 95
    function value_68 () : gw.api.financials.IMoney {
      return gw.api.financials.FinancialsCalculationsForEditedTransaction.getTotalPayments(recovery).ReservingAmount
    }
    
    // 'value' attribute on CurrencyInput (id=PastRecoveries_Input) at RecoveryDetailDV.pcf: line 89, column 97
    function value_73 () : gw.api.financials.IMoney {
      return gw.api.financials.FinancialsCalculationsForEditedTransaction.getTotalRecoveries(recovery).ReservingAmount
    }
    
    // 'value' attribute on TextInput (id=RecoveryCheckNumber_Input) at RecoveryDetailDV.pcf: line 95, column 52
    function value_78 () : java.lang.String {
      return recovery.CheckNumber_Ext
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_15 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_15 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_15 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_51 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_51 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_51 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_16 () : void {
      var __valueRangeArg = recovery.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_15(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_52 () : void {
      var __valueRangeArg = recovery.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_51(__valueRangeArg)
    }
    
    // 'visible' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_0 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on TypeKeyInput (id=Currency_Input) at RecoveryDetailDV.pcf: line 47, column 66
    function visible_28 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on ClaimContactInput (id=Payer_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_3 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (recovery.Payer), recovery.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_39 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (recovery.OnBehalfOf), recovery.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=OnBehalfOf_Input) at ClaimContactWidget.xml: line 12, column 273
    function visible_43 () : java.lang.Boolean {
      return recovery.RecoveryCategory == RecoveryCategory.TC_SUBRO
    }
    
    // 'visible' attribute on CurrencyInput (id=OpenRecoveryReserves_Input) at RecoveryDetailDV.pcf: line 75, column 147
    function visible_62 () : java.lang.Boolean {
      return shouldShowCalculation() && recovery.isUseRecoveryReserves() && perm.Claim.viewrecoveryreserves(recovery.Claim)
    }
    
    // 'visible' attribute on CurrencyInput (id=Payments_Input) at RecoveryDetailDV.pcf: line 82, column 95
    function visible_67 () : java.lang.Boolean {
      return shouldShowCalculation() && perm.Claim.viewpayments(recovery.Claim)
    }
    
    // 'visible' attribute on CurrencyInput (id=PastRecoveries_Input) at RecoveryDetailDV.pcf: line 89, column 97
    function visible_72 () : java.lang.Boolean {
      return shouldShowCalculation() && perm.Claim.viewrecoveries(recovery.Claim)
    }
    
    // 'visible' attribute on TextInput (id=RecoveryCheckNumber_Input) at RecoveryDetailDV.pcf: line 95, column 52
    function visible_77 () : java.lang.Boolean {
      return !recovery.Claim.isWorkCompClaim()
    }
    
    // 'addVisible' attribute on IteratorButtons at RecoveryDetailDV.pcf: line 110, column 90
    function visible_83 () : java.lang.Boolean {
      return gw.api.financials.FinancialsUtil.isAllowMultipleLineItems()
    }
    
    property get recovery () : Recovery {
      return getRequireValue("recovery", 0) as Recovery
    }
    
    property set recovery ($arg :  Recovery) {
      setRequireValue("recovery", 0, $arg)
    }
    
    property get recoveryHelper () : gw.api.financials.RecoveryHelper {
      return getRequireValue("recoveryHelper", 0) as gw.api.financials.RecoveryHelper
    }
    
    property set recoveryHelper ($arg :  gw.api.financials.RecoveryHelper) {
      setRequireValue("recoveryHelper", 0, $arg)
    }
    
    property get reserveLineInputSetHelper () : gw.api.financials.ReserveLineInputSetHelper {
      return getRequireValue("reserveLineInputSetHelper", 0) as gw.api.financials.ReserveLineInputSetHelper
    }
    
    property set reserveLineInputSetHelper ($arg :  gw.api.financials.ReserveLineInputSetHelper) {
      setRequireValue("reserveLineInputSetHelper", 0, $arg)
    }
    
    function shouldShowCalculation() : boolean {
      var reserveLine = recovery.ReserveLine
      if (reserveLine == null) {
        return false
      } else if (reserveLine.New) {
        return recovery.CostType != null && recovery.CostCategory != null && recovery.ReservingCurrency != null
      }
      return true
    }
    
    
  }
  
  
}