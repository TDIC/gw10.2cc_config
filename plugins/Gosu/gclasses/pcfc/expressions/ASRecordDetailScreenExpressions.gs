package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/ASRecordDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ASRecordDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/ASRecordDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ASRecordDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=ASRecordDetailScreen_DeleteButton) at ASRecordDetailScreen.pcf: line 17, column 60
    function action_1 () : void {
      new tdic.cc.config.assignment.AssignmentStrategyUtil().removeAssignmentStrategy(ASRecord);AssignmentStrategy_TDIC.go()
    }
    
    // 'def' attribute on PanelRef at ASRecordDetailScreen.pcf: line 20, column 40
    function def_onEnter_2 (def :  pcf.AdminASRecordDV) : void {
      def.onEnter(ASRecord)
    }
    
    // 'def' attribute on PanelRef at ASRecordDetailScreen.pcf: line 20, column 40
    function def_refreshVariables_3 (def :  pcf.AdminASRecordDV) : void {
      def.refreshVariables(ASRecord)
    }
    
    // EditButtons at ASRecordDetailScreen.pcf: line 11, column 21
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    property get ASRecord () : entity.AssignStrategy_TDIC {
      return getRequireValue("ASRecord", 0) as entity.AssignStrategy_TDIC
    }
    
    property set ASRecord ($arg :  entity.AssignStrategy_TDIC) {
      setRequireValue("ASRecord", 0, $arg)
    }
    
    
  }
  
  
}