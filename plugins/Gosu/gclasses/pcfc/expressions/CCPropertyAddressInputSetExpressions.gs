package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses tdic.cc.config.contacts.address.TDIC_ClaimAddressOwner
@javax.annotation.Generated("config/web/pcf/addressbook/shared/CCPropertyAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CCPropertyAddressInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/addressbook/shared/CCPropertyAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CCPropertyAddressInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCPropertyAddressInputSet.pcf: line 46, column 109
    function available_21 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.CCAddressOwnerFieldId.ADDRESSTYPE)
    }
    
    // 'available' attribute on TextInput (id=Address_Description_Input) at CCPropertyAddressInputSet.pcf: line 53, column 51
    function available_32 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.CCAddressOwnerFieldId.DESCRIPTION)
    }
    
    // 'available' attribute on DateInput (id=Address_ValidUntil_Input) at CCPropertyAddressInputSet.pcf: line 62, column 26
    function available_43 () : java.lang.Boolean {
      return addressOwner.isAvailable(gw.api.address.CCAddressOwnerFieldId.VALIDUNTIL)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCPropertyAddressInputSet.pcf: line 37, column 43
    function def_onEnter_14 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCPropertyAddressInputSet.pcf: line 37, column 43
    function def_onEnter_16 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCPropertyAddressInputSet.pcf: line 37, column 43
    function def_onEnter_18 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCPropertyAddressInputSet.pcf: line 37, column 43
    function def_refreshVariables_15 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCPropertyAddressInputSet.pcf: line 37, column 43
    function def_refreshVariables_17 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddress) at CCPropertyAddressInputSet.pcf: line 37, column 43
    function def_refreshVariables_19 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'value' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCPropertyAddressInputSet.pcf: line 46, column 109
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.Address.AddressType = (__VALUE_TO_SET as typekey.AddressType)
    }
    
    // 'value' attribute on TextInput (id=Address_Description_Input) at CCPropertyAddressInputSet.pcf: line 53, column 51
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.Address.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=Address_ValidUntil_Input) at CCPropertyAddressInputSet.pcf: line 62, column 26
    function defaultSetter_47 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.Address.ValidUntil = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.Claim.LocationCode = (__VALUE_TO_SET as entity.PolicyLocation)
    }
    
    // 'value' attribute on TextInput (id=Claim_LocationCode_Input) at CCPropertyAddressInputSet.pcf: line 68, column 141
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.Claim.LossLocationCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCPropertyAddressInputSet.pcf: line 77, column 25
    function defaultSetter_62 (__VALUE_TO_SET :  java.lang.Object) : void {
      addressOwner.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'editable' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCPropertyAddressInputSet.pcf: line 46, column 109
    function editable_22 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.CCAddressOwnerFieldId.ADDRESSTYPE)
    }
    
    // 'editable' attribute on TextInput (id=Address_Description_Input) at CCPropertyAddressInputSet.pcf: line 53, column 51
    function editable_33 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.CCAddressOwnerFieldId.DESCRIPTION)
    }
    
    // 'editable' attribute on DateInput (id=Address_ValidUntil_Input) at CCPropertyAddressInputSet.pcf: line 62, column 26
    function editable_44 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.CCAddressOwnerFieldId.VALIDUNTIL)
    }
    
    // 'editable' attribute on TextInput (id=Claim_LocationCode_Input) at CCPropertyAddressInputSet.pcf: line 68, column 141
    function editable_52 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.CCAddressOwnerFieldId.LOCATIONCODE)
    }
    
    // 'editable' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCPropertyAddressInputSet.pcf: line 77, column 25
    function editable_60 () : java.lang.Boolean {
      return addressOwner.isEditable(gw.api.address.CCAddressOwnerFieldId.JURISDICTIONSTATE)
    }
    
    // 'filter' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCPropertyAddressInputSet.pcf: line 77, column 25
    function filter_64 (VALUE :  typekey.Jurisdiction, VALUES :  typekey.Jurisdiction[]) : java.lang.Boolean {
      return VALUE.hasCategory(Country.TC_US)
    }
    
    // 'filter' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function filter_7 (VALUE :  entity.PolicyLocation, VALUES :  entity.PolicyLocation[]) : java.lang.Boolean {
      return filterLossLocation(addressOwner.Claim,VALUE)
    }
    
    // 'label' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function label_2 () : java.lang.Object {
      return addressOwner.AddressNameLabel != null ? addressOwner.AddressNameLabel : DisplayKey.get("Web.Address.Default.Name")
    }
    
    // 'label' attribute on TextInput (id=Address_Description_Input) at CCPropertyAddressInputSet.pcf: line 53, column 51
    function label_34 () : java.lang.Object {
      return LocationDescriptionLabel_TDIC
    }
    
    // 'mode' attribute on InputSetRef (id=globalAddress) at CCPropertyAddressInputSet.pcf: line 37, column 43
    function mode_20 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PCFMode
    }
    
    // 'onChange' attribute on PostOnChange at CCPropertyAddressInputSet.pcf: line 31, column 32
    function onChange_0 () : void {
      addressOwner.Address = addressOwner.Claim.LocationCode.Address
    }
    
    // 'required' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCPropertyAddressInputSet.pcf: line 46, column 109
    function required_24 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.CCAddressOwnerFieldId.ADDRESSTYPE)
    }
    
    // 'required' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function required_3 () : java.lang.Boolean {
      return addressOwner.RequiredFields.contains(gw.api.address.CCAddressOwnerFieldId.ADDRESS_NAME)
    }
    
    // 'required' attribute on TextInput (id=Address_Description_Input) at CCPropertyAddressInputSet.pcf: line 53, column 51
    function required_35 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.CCAddressOwnerFieldId.DESCRIPTION)
    }
    
    // 'required' attribute on DateInput (id=Address_ValidUntil_Input) at CCPropertyAddressInputSet.pcf: line 62, column 26
    function required_45 () : java.lang.Boolean {
      return addressOwner.isRequired(gw.api.address.CCAddressOwnerFieldId.VALIDUNTIL)
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCPropertyAddressInputSet.pcf: line 77, column 25
    function valueRange_65 () : java.lang.Object {
      return addressOwner.Jurisdictions
    }
    
    // 'valueRange' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function valueRange_8 () : java.lang.Object {
      return addressOwner.Claim.Policy.PolicyLocations
    }
    
    // 'value' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCPropertyAddressInputSet.pcf: line 46, column 109
    function valueRoot_27 () : java.lang.Object {
      return addressOwner.Address
    }
    
    // 'value' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function valueRoot_6 () : java.lang.Object {
      return addressOwner.Claim
    }
    
    // 'value' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCPropertyAddressInputSet.pcf: line 77, column 25
    function valueRoot_63 () : java.lang.Object {
      return addressOwner
    }
    
    // 'value' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCPropertyAddressInputSet.pcf: line 46, column 109
    function value_25 () : typekey.AddressType {
      return addressOwner.Address.AddressType
    }
    
    // 'value' attribute on TextInput (id=Address_Description_Input) at CCPropertyAddressInputSet.pcf: line 53, column 51
    function value_36 () : java.lang.String {
      return addressOwner.Address.Description
    }
    
    // 'value' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function value_4 () : entity.PolicyLocation {
      return addressOwner.Claim.LocationCode
    }
    
    // 'value' attribute on DateInput (id=Address_ValidUntil_Input) at CCPropertyAddressInputSet.pcf: line 62, column 26
    function value_46 () : java.util.Date {
      return addressOwner.Address.ValidUntil
    }
    
    // 'value' attribute on TextInput (id=Claim_LocationCode_Input) at CCPropertyAddressInputSet.pcf: line 68, column 141
    function value_54 () : java.lang.String {
      return addressOwner.Claim.LossLocationCode
    }
    
    // 'value' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCPropertyAddressInputSet.pcf: line 77, column 25
    function value_61 () : typekey.Jurisdiction {
      return addressOwner.Jurisdiction
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCPropertyAddressInputSet.pcf: line 77, column 25
    function verifyValueRangeIsAllowedType_66 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCPropertyAddressInputSet.pcf: line 77, column 25
    function verifyValueRangeIsAllowedType_66 ($$arg :  typekey.Jurisdiction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function verifyValueRangeIsAllowedType_9 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function verifyValueRangeIsAllowedType_9 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function verifyValueRange_10 () : void {
      var __valueRangeArg = addressOwner.Claim.Policy.PolicyLocations
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_9(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_JurisdictionState_Input) at CCPropertyAddressInputSet.pcf: line 77, column 25
    function verifyValueRange_67 () : void {
      var __valueRangeArg = addressOwner.Jurisdictions
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_66(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=Address_Picker_Input) at CCPropertyAddressInputSet.pcf: line 28, column 135
    function visible_1 () : java.lang.Boolean {
      return (addressOwner.Claim.Contacts != null and addressOwner.Claim.Contacts.hasMatch(\contact -> !contact.Contact.New))
    }
    
    // 'visible' attribute on TypeKeyInput (id=Address_AddressType_Input) at CCPropertyAddressInputSet.pcf: line 46, column 109
    function visible_23 () : java.lang.Boolean {
      return not addressOwner.HiddenFields.contains(gw.api.address.CCAddressOwnerFieldId.ADDRESSTYPE)
    }
    
    // 'visible' attribute on TextInput (id=Claim_LocationCode_Input) at CCPropertyAddressInputSet.pcf: line 68, column 141
    function visible_53 () : java.lang.Boolean {
      return addressOwner.Claim != null and not addressOwner.HiddenFields.contains(gw.api.address.CCAddressOwnerFieldId.LOCATIONCODE)
    }
    
    property get addressOwner () : gw.api.address.CCAddressOwner {
      return getRequireValue("addressOwner", 0) as gw.api.address.CCAddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.CCAddressOwner) {
      setRequireValue("addressOwner", 0, $arg)
    }
    
    property get wizard () : gw.api.claim.NewClaimWizardInfo {
      return getRequireValue("wizard", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set wizard ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setRequireValue("wizard", 0, $arg)
    }
    
    
        function IsJurisdictionVisible() : boolean {
          return addressOwner.Claim != null
            and not addressOwner.HiddenFields.contains(gw.api.address.CCAddressOwnerFieldId.JURISDICTIONSTATE)
            and addressOwner.Jurisdictions.HasElements
        }
    
        /**
         * US529, robk 
         */
        property get LocationDescriptionLabel_TDIC() : String {
          if (addressOwner typeis TDIC_ClaimAddressOwner) {
            return DisplayKey.get("TDIC.LossLocation.Description")
          }
          return DisplayKey.get("Web.Address.Default.Description")
        }
    
    
    
        function filterLossLocation(aclaim : Claim , apolicyLocation : PolicyLocation) : boolean
        {
          if((aclaim.Policy.PolicyType == PolicyType.TC_BUSINESSOWNERS) & (aclaim.Type_TDIC == ClaimType_TDIC.TC_PROPERTY || aclaim.Type_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY))
          {
    
            return wizard.PolicySummary.Properties.hasMatch(\elt -> elt.Selected and apolicyLocation.DisplayName.contains(elt.Address))
    
          }
          return true
    
        }
    
    
  }
  
  
}