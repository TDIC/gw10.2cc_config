package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/keymetrics/KeyMetrics.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class KeyMetricsExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/keymetrics/KeyMetrics.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_100 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.RedValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_105 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.RedValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_115 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.RedValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.TargetValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.TargetValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.TargetValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.TargetValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_65 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.YellowValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_70 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.YellowValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_75 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.YellowValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_85 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.YellowValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_95 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultLimit.EditableValues.RedValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'sortBy' attribute on IteratorSort at KeyMetrics.pcf: line 129, column 42
    function sortBy_21 (tier :  typekey.ClaimTier) : java.lang.Object {
      return tier
    }
    
    // 'toRemove' attribute on RowIterator (id=ClaimMetricLimitIterator) at KeyMetrics.pcf: line 168, column 59
    function toRemove_217 (limit :  entity.ClaimMetricLimit) : void {
      limits.removeFromClaimMetricLimits(limit)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_32 () : java.lang.Object {
      return validateMetricInput(defaultLimit.EditableValues.TargetValue, defaultLimit)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_62 () : java.lang.Object {
      return validateMetricInput(defaultLimit.EditableValues.YellowValue, defaultLimit)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_92 () : java.lang.Object {
      return validateMetricInput(defaultLimit.EditableValues.RedValue, defaultLimit)
    }
    
    // 'value' attribute on TextCell (id=Label_Cell) at KeyMetrics.pcf: line 120, column 72
    function valueRoot_27 () : java.lang.Object {
      return defaultLimit.ClaimMetricType
    }
    
    // 'value' attribute on TextCell (id=Unit_Cell) at KeyMetrics.pcf: line 139, column 55
    function valueRoot_30 () : java.lang.Object {
      return defaultLimit
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_36 () : java.lang.Object {
      return defaultLimit.EditableValues.TargetValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at KeyMetrics.pcf: line 145, column 72
    function valueRoot_59 () : java.lang.Object {
      return defaultLimit.EditableValues
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_66 () : java.lang.Object {
      return defaultLimit.EditableValues.YellowValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_96 () : java.lang.Object {
      return defaultLimit.EditableValues.RedValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_104 () : java.math.BigDecimal {
      return defaultLimit.EditableValues.RedValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_114 () : gw.api.financials.CurrencyAmount {
      return defaultLimit.EditableValues.RedValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at KeyMetrics.pcf: line 157, column 69
    function value_118 () : gw.api.metric.EditableMetricLimitValue {
      return defaultLimit.EditableValues.RedValue
    }
    
    // 'value' attribute on RowIterator (id=ClaimMetricLimitIterator) at KeyMetrics.pcf: line 168, column 59
    function value_218 () : entity.ClaimMetricLimit[] {
      return getSortedClaimMetricLimits( category, defaultLimit )
    }
    
    // 'value' attribute on AddMenuItemIterator (id=CreateLimitMenu) at KeyMetrics.pcf: line 126, column 62
    function value_25 () : typekey.ClaimTier[] {
      return getClaimTiersWithNoLimit(defaultLimit)
    }
    
    // 'value' attribute on TextCell (id=Label_Cell) at KeyMetrics.pcf: line 120, column 72
    function value_26 () : java.lang.String {
      return defaultLimit.ClaimMetricType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Unit_Cell) at KeyMetrics.pcf: line 139, column 55
    function value_29 () : java.lang.String {
      return defaultLimit.UnitLabel
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_34 () : java.lang.Integer {
      return defaultLimit.EditableValues.TargetValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_39 () : java.math.BigDecimal {
      return defaultLimit.EditableValues.TargetValue.DecimalValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_44 () : java.math.BigDecimal {
      return defaultLimit.EditableValues.TargetValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_54 () : gw.api.financials.CurrencyAmount {
      return defaultLimit.EditableValues.TargetValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at KeyMetrics.pcf: line 145, column 72
    function value_58 () : gw.api.metric.EditableMetricLimitValue {
      return defaultLimit.EditableValues.TargetValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_64 () : java.lang.Integer {
      return defaultLimit.EditableValues.YellowValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_69 () : java.math.BigDecimal {
      return defaultLimit.EditableValues.YellowValue.DecimalValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_74 () : java.math.BigDecimal {
      return defaultLimit.EditableValues.YellowValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_84 () : gw.api.financials.CurrencyAmount {
      return defaultLimit.EditableValues.YellowValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at KeyMetrics.pcf: line 151, column 72
    function value_88 () : gw.api.metric.EditableMetricLimitValue {
      return defaultLimit.EditableValues.YellowValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_94 () : java.lang.Integer {
      return defaultLimit.EditableValues.RedValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_99 () : java.math.BigDecimal {
      return defaultLimit.EditableValues.RedValue.DecimalValue
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_103 () : java.lang.Boolean {
      return defaultLimit.EditableValues.RedValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_113 () : java.lang.Boolean {
      return defaultLimit.EditableValues.RedValue.DataType.Name == "currencyamount"
    }
    
    // 'visible' attribute on AddMenuItemIterator (id=CreateLimitMenu) at KeyMetrics.pcf: line 126, column 62
    function visible_22 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_33 () : java.lang.Boolean {
      return defaultLimit.EditableValues.TargetValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_38 () : java.lang.Boolean {
      return defaultLimit.EditableValues.TargetValue.DataType.Name == "decimalmetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_43 () : java.lang.Boolean {
      return defaultLimit.EditableValues.TargetValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_53 () : java.lang.Boolean {
      return defaultLimit.EditableValues.TargetValue.DataType.Name == "currencyamount"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_63 () : java.lang.Boolean {
      return defaultLimit.EditableValues.YellowValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_68 () : java.lang.Boolean {
      return defaultLimit.EditableValues.YellowValue.DataType.Name == "decimalmetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_73 () : java.lang.Boolean {
      return defaultLimit.EditableValues.YellowValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_83 () : java.lang.Boolean {
      return defaultLimit.EditableValues.YellowValue.DataType.Name == "currencyamount"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_93 () : java.lang.Boolean {
      return defaultLimit.EditableValues.RedValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_98 () : java.lang.Boolean {
      return defaultLimit.EditableValues.RedValue.DataType.Name == "decimalmetric"
    }
    
    property get defaultLimit () : entity.ClaimMetricLimit {
      return getIteratedValue(2) as entity.ClaimMetricLimit
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/keymetrics/KeyMetrics.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends IteratorEntry2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=CreateLimitForTier) at KeyMetrics.pcf: line 134, column 77
    function label_23 () : java.lang.Object {
      return tier.DisplayName
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=CreateLimitForTier) at KeyMetrics.pcf: line 134, column 77
    function toCreateAndAdd_24 (CheckedValues :  Object[]) : java.lang.Object {
      return defaultLimit.copyWithTier(tier)
    }
    
    property get tier () : typekey.ClaimTier {
      return getIteratedValue(3) as typekey.ClaimTier
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/keymetrics/KeyMetrics.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends IteratorEntry2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_130 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.TargetValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_135 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.TargetValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_140 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.TargetValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_150 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.TargetValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_160 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.YellowValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_165 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.YellowValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_170 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.YellowValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_180 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.YellowValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_190 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.RedValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_195 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.RedValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_200 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.RedValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_210 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.RedValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_127 () : java.lang.Object {
      return validateMetricInput(limit.EditableValues.TargetValue, limit)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_157 () : java.lang.Object {
      return validateMetricInput(limit.EditableValues.YellowValue, limit)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_187 () : java.lang.Object {
      return validateMetricInput(limit.EditableValues.RedValue, limit)
    }
    
    // 'value' attribute on TextCell (id=Unit_Cell) at KeyMetrics.pcf: line 177, column 50
    function valueRoot_125 () : java.lang.Object {
      return limit
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_131 () : java.lang.Object {
      return limit.EditableValues.TargetValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at KeyMetrics.pcf: line 183, column 67
    function valueRoot_154 () : java.lang.Object {
      return limit.EditableValues
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_161 () : java.lang.Object {
      return limit.EditableValues.YellowValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_191 () : java.lang.Object {
      return limit.EditableValues.RedValue
    }
    
    // 'value' attribute on TextCell (id=Tier_Cell) at KeyMetrics.pcf: line 174, column 119
    function value_122 () : java.lang.String {
      return DisplayKey.get("Web.Admin.KeyMetrics.Indent", limit.ClaimTier.DisplayName)
    }
    
    // 'value' attribute on TextCell (id=Unit_Cell) at KeyMetrics.pcf: line 177, column 50
    function value_124 () : java.lang.String {
      return limit.UnitLabel
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_129 () : java.lang.Integer {
      return limit.EditableValues.TargetValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_134 () : java.math.BigDecimal {
      return limit.EditableValues.TargetValue.DecimalValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_139 () : java.math.BigDecimal {
      return limit.EditableValues.TargetValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_149 () : gw.api.financials.CurrencyAmount {
      return limit.EditableValues.TargetValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at KeyMetrics.pcf: line 183, column 67
    function value_153 () : gw.api.metric.EditableMetricLimitValue {
      return limit.EditableValues.TargetValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_159 () : java.lang.Integer {
      return limit.EditableValues.YellowValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_164 () : java.math.BigDecimal {
      return limit.EditableValues.YellowValue.DecimalValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_169 () : java.math.BigDecimal {
      return limit.EditableValues.YellowValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_179 () : gw.api.financials.CurrencyAmount {
      return limit.EditableValues.YellowValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at KeyMetrics.pcf: line 189, column 67
    function value_183 () : gw.api.metric.EditableMetricLimitValue {
      return limit.EditableValues.YellowValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_189 () : java.lang.Integer {
      return limit.EditableValues.RedValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_194 () : java.math.BigDecimal {
      return limit.EditableValues.RedValue.DecimalValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_199 () : java.math.BigDecimal {
      return limit.EditableValues.RedValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_209 () : gw.api.financials.CurrencyAmount {
      return limit.EditableValues.RedValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at KeyMetrics.pcf: line 195, column 64
    function value_213 () : gw.api.metric.EditableMetricLimitValue {
      return limit.EditableValues.RedValue
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_128 () : java.lang.Boolean {
      return limit.EditableValues.TargetValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_133 () : java.lang.Boolean {
      return limit.EditableValues.TargetValue.DataType.Name == "decimalmetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_138 () : java.lang.Boolean {
      return limit.EditableValues.TargetValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_148 () : java.lang.Boolean {
      return limit.EditableValues.TargetValue.DataType.Name == "currencyamount"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_158 () : java.lang.Boolean {
      return limit.EditableValues.YellowValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_163 () : java.lang.Boolean {
      return limit.EditableValues.YellowValue.DataType.Name == "decimalmetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_168 () : java.lang.Boolean {
      return limit.EditableValues.YellowValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_178 () : java.lang.Boolean {
      return limit.EditableValues.YellowValue.DataType.Name == "currencyamount"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_188 () : java.lang.Boolean {
      return limit.EditableValues.RedValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_193 () : java.lang.Boolean {
      return limit.EditableValues.RedValue.DataType.Name == "decimalmetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_198 () : java.lang.Boolean {
      return limit.EditableValues.RedValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_208 () : java.lang.Boolean {
      return limit.EditableValues.RedValue.DataType.Name == "currencyamount"
    }
    
    property get limit () : entity.ClaimMetricLimit {
      return getIteratedValue(3) as entity.ClaimMetricLimit
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/keymetrics/KeyMetrics.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends KeyMetricsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_237 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.TargetValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_242 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.TargetValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_247 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.TargetValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_257 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.TargetValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_267 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.YellowValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_272 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.YellowValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_277 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.YellowValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_287 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.YellowValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_297 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.RedValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_302 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.RedValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_307 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.RedValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_317 (__VALUE_TO_SET :  java.lang.Object) : void {
      defaultExposureLimit.EditableValues.RedValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'sortBy' attribute on IteratorSort at KeyMetrics.pcf: line 276, column 40
    function sortBy_223 (tier :  typekey.ExposureTier) : java.lang.Object {
      return tier
    }
    
    // 'toRemove' attribute on RowIterator (id=ExposureMetricLimitIterator) at KeyMetrics.pcf: line 315, column 60
    function toRemove_419 (limit :  entity.ExposureMetricLimit) : void {
      limits.removeFromExposureMetricLimits(limit)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_234 () : java.lang.Object {
      return validateMetricInput(defaultExposureLimit.EditableValues.TargetValue, defaultExposureLimit)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_264 () : java.lang.Object {
      return validateMetricInput(defaultExposureLimit.EditableValues.YellowValue, defaultExposureLimit)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_294 () : java.lang.Object {
      return validateMetricInput(defaultExposureLimit.EditableValues.RedValue, defaultExposureLimit)
    }
    
    // 'value' attribute on TextCell (id=Label_Cell) at KeyMetrics.pcf: line 267, column 81
    function valueRoot_229 () : java.lang.Object {
      return defaultExposureLimit.ExposureMetricType
    }
    
    // 'value' attribute on TextCell (id=Unit_Cell) at KeyMetrics.pcf: line 286, column 61
    function valueRoot_232 () : java.lang.Object {
      return defaultExposureLimit
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_238 () : java.lang.Object {
      return defaultExposureLimit.EditableValues.TargetValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at KeyMetrics.pcf: line 292, column 78
    function valueRoot_261 () : java.lang.Object {
      return defaultExposureLimit.EditableValues
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_268 () : java.lang.Object {
      return defaultExposureLimit.EditableValues.YellowValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_298 () : java.lang.Object {
      return defaultExposureLimit.EditableValues.RedValue
    }
    
    // 'value' attribute on AddMenuItemIterator (id=CreateLimitMenu) at KeyMetrics.pcf: line 273, column 60
    function value_227 () : typekey.ExposureTier[] {
      return getExposureTiersWithNoLimit(defaultExposureLimit)
    }
    
    // 'value' attribute on TextCell (id=Label_Cell) at KeyMetrics.pcf: line 267, column 81
    function value_228 () : java.lang.String {
      return defaultExposureLimit.ExposureMetricType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Unit_Cell) at KeyMetrics.pcf: line 286, column 61
    function value_231 () : java.lang.String {
      return defaultExposureLimit.UnitLabel
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_236 () : java.lang.Integer {
      return defaultExposureLimit.EditableValues.TargetValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_241 () : java.math.BigDecimal {
      return defaultExposureLimit.EditableValues.TargetValue.DecimalValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_246 () : java.math.BigDecimal {
      return defaultExposureLimit.EditableValues.TargetValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_256 () : gw.api.financials.CurrencyAmount {
      return defaultExposureLimit.EditableValues.TargetValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at KeyMetrics.pcf: line 292, column 78
    function value_260 () : gw.api.metric.EditableMetricLimitValue {
      return defaultExposureLimit.EditableValues.TargetValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_266 () : java.lang.Integer {
      return defaultExposureLimit.EditableValues.YellowValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_271 () : java.math.BigDecimal {
      return defaultExposureLimit.EditableValues.YellowValue.DecimalValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_276 () : java.math.BigDecimal {
      return defaultExposureLimit.EditableValues.YellowValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_286 () : gw.api.financials.CurrencyAmount {
      return defaultExposureLimit.EditableValues.YellowValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at KeyMetrics.pcf: line 298, column 78
    function value_290 () : gw.api.metric.EditableMetricLimitValue {
      return defaultExposureLimit.EditableValues.YellowValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_296 () : java.lang.Integer {
      return defaultExposureLimit.EditableValues.RedValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_301 () : java.math.BigDecimal {
      return defaultExposureLimit.EditableValues.RedValue.DecimalValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_306 () : java.math.BigDecimal {
      return defaultExposureLimit.EditableValues.RedValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_316 () : gw.api.financials.CurrencyAmount {
      return defaultExposureLimit.EditableValues.RedValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at KeyMetrics.pcf: line 304, column 75
    function value_320 () : gw.api.metric.EditableMetricLimitValue {
      return defaultExposureLimit.EditableValues.RedValue
    }
    
    // 'value' attribute on RowIterator (id=ExposureMetricLimitIterator) at KeyMetrics.pcf: line 315, column 60
    function value_420 () : entity.ExposureMetricLimit[] {
      return getSortedExposureMetricLimits(defaultExposureLimit)
    }
    
    // 'visible' attribute on AddMenuItemIterator (id=CreateLimitMenu) at KeyMetrics.pcf: line 273, column 60
    function visible_224 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_235 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.TargetValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_240 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.TargetValue.DataType.Name == "decimalmetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_245 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.TargetValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_255 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.TargetValue.DataType.Name == "currencyamount"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_265 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.YellowValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_270 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.YellowValue.DataType.Name == "decimalmetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_275 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.YellowValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_285 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.YellowValue.DataType.Name == "currencyamount"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_295 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.RedValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_300 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.RedValue.DataType.Name == "decimalmetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_305 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.RedValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_315 () : java.lang.Boolean {
      return defaultExposureLimit.EditableValues.RedValue.DataType.Name == "currencyamount"
    }
    
    property get defaultExposureLimit () : entity.ExposureMetricLimit {
      return getIteratedValue(1) as entity.ExposureMetricLimit
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/keymetrics/KeyMetrics.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry6ExpressionsImpl extends IteratorEntry5ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=CreateLimitForTier) at KeyMetrics.pcf: line 281, column 83
    function label_225 () : java.lang.Object {
      return tier.DisplayName
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=CreateLimitForTier) at KeyMetrics.pcf: line 281, column 83
    function toCreateAndAdd_226 (CheckedValues :  Object[]) : java.lang.Object {
      return defaultExposureLimit.copyWithTier(tier)
    }
    
    property get tier () : typekey.ExposureTier {
      return getIteratedValue(2) as typekey.ExposureTier
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/keymetrics/KeyMetrics.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry7ExpressionsImpl extends IteratorEntry5ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_332 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.TargetValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_337 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.TargetValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_342 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.TargetValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_352 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.TargetValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_362 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.YellowValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_367 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.YellowValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_372 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.YellowValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_382 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.YellowValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function defaultSetter_392 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.RedValue.IntegerValue = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function defaultSetter_397 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.RedValue.DecimalValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function defaultSetter_402 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.RedValue.PercentValue = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function defaultSetter_412 (__VALUE_TO_SET :  java.lang.Object) : void {
      limit.EditableValues.RedValue.MoneyValue = (__VALUE_TO_SET as gw.api.financials.CurrencyAmount)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_329 () : java.lang.Object {
      return validateMetricInput(limit.EditableValues.TargetValue, limit)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_359 () : java.lang.Object {
      return validateMetricInput(limit.EditableValues.YellowValue, limit)
    }
    
    // 'validationExpression' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function validationExpression_389 () : java.lang.Object {
      return validateMetricInput(limit.EditableValues.RedValue, limit)
    }
    
    // 'value' attribute on TextCell (id=Unit_Cell) at KeyMetrics.pcf: line 324, column 48
    function valueRoot_327 () : java.lang.Object {
      return limit
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_333 () : java.lang.Object {
      return limit.EditableValues.TargetValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at KeyMetrics.pcf: line 330, column 65
    function valueRoot_356 () : java.lang.Object {
      return limit.EditableValues
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_363 () : java.lang.Object {
      return limit.EditableValues.YellowValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function valueRoot_393 () : java.lang.Object {
      return limit.EditableValues.RedValue
    }
    
    // 'value' attribute on TextCell (id=Tier_Cell) at KeyMetrics.pcf: line 321, column 120
    function value_324 () : java.lang.String {
      return DisplayKey.get("Web.Admin.KeyMetrics.Indent", limit.ExposureTier.DisplayName)
    }
    
    // 'value' attribute on TextCell (id=Unit_Cell) at KeyMetrics.pcf: line 324, column 48
    function value_326 () : java.lang.String {
      return limit.UnitLabel
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_331 () : java.lang.Integer {
      return limit.EditableValues.TargetValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_336 () : java.math.BigDecimal {
      return limit.EditableValues.TargetValue.DecimalValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_341 () : java.math.BigDecimal {
      return limit.EditableValues.TargetValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_351 () : gw.api.financials.CurrencyAmount {
      return limit.EditableValues.TargetValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=TargetValue_Cell) at KeyMetrics.pcf: line 330, column 65
    function value_355 () : gw.api.metric.EditableMetricLimitValue {
      return limit.EditableValues.TargetValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_361 () : java.lang.Integer {
      return limit.EditableValues.YellowValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_366 () : java.math.BigDecimal {
      return limit.EditableValues.YellowValue.DecimalValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_371 () : java.math.BigDecimal {
      return limit.EditableValues.YellowValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_381 () : gw.api.financials.CurrencyAmount {
      return limit.EditableValues.YellowValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=YellowValue_Cell) at KeyMetrics.pcf: line 336, column 65
    function value_385 () : gw.api.metric.EditableMetricLimitValue {
      return limit.EditableValues.YellowValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function value_391 () : java.lang.Integer {
      return limit.EditableValues.RedValue.IntegerValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function value_396 () : java.math.BigDecimal {
      return limit.EditableValues.RedValue.DecimalValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function value_401 () : java.math.BigDecimal {
      return limit.EditableValues.RedValue.PercentValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function value_411 () : gw.api.financials.CurrencyAmount {
      return limit.EditableValues.RedValue.MoneyValue
    }
    
    // 'value' attribute on MetricLimitValueCell (id=RedValue_Cell) at KeyMetrics.pcf: line 342, column 62
    function value_415 () : gw.api.metric.EditableMetricLimitValue {
      return limit.EditableValues.RedValue
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_330 () : java.lang.Boolean {
      return limit.EditableValues.TargetValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_335 () : java.lang.Boolean {
      return limit.EditableValues.TargetValue.DataType.Name == "decimalmetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_340 () : java.lang.Boolean {
      return limit.EditableValues.TargetValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=TargetValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_350 () : java.lang.Boolean {
      return limit.EditableValues.TargetValue.DataType.Name == "currencyamount"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_360 () : java.lang.Boolean {
      return limit.EditableValues.YellowValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_365 () : java.lang.Boolean {
      return limit.EditableValues.YellowValue.DataType.Name == "decimalmetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_370 () : java.lang.Boolean {
      return limit.EditableValues.YellowValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=YellowValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_380 () : java.lang.Boolean {
      return limit.EditableValues.YellowValue.DataType.Name == "currencyamount"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 8, column 68
    function visible_390 () : java.lang.Boolean {
      return limit.EditableValues.RedValue.DataType.Name == "integer"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 14, column 74
    function visible_395 () : java.lang.Boolean {
      return limit.EditableValues.RedValue.DataType.Name == "decimalmetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 22, column 77
    function visible_400 () : java.lang.Boolean {
      return limit.EditableValues.RedValue.DataType.Name == "percentagemetric"
    }
    
    // 'visible' attribute on MetricLimitValueCell (id=RedValue_Cell) at MetricLimitValueWidget.xml: line 35, column 75
    function visible_410 () : java.lang.Boolean {
      return limit.EditableValues.RedValue.DataType.Name == "currencyamount"
    }
    
    property get limit () : entity.ExposureMetricLimit {
      return getIteratedValue(2) as entity.ExposureMetricLimit
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/keymetrics/KeyMetrics.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends KeyMetricsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=ClaimMetricLimitCategoryHeader_Cell) at KeyMetrics.pcf: line 106, column 51
    function valueRoot_19 () : java.lang.Object {
      return category
    }
    
    // 'value' attribute on TextCell (id=ClaimMetricLimitCategoryHeader_Cell) at KeyMetrics.pcf: line 106, column 51
    function value_18 () : java.lang.String {
      return category.DisplayName
    }
    
    // 'value' attribute on RowIterator (id=ClaimMetricDefaultLimitIterator) at KeyMetrics.pcf: line 115, column 57
    function value_219 () : entity.ClaimMetricLimit[] {
      return limits.ClaimMetricLimits.where(\ l -> l.ClaimMetricCategory == category and l.DefaultLimit).sortBy(\ l -> l.Currency).sortBy(\ l -> l.ClaimMetricType)
    }
    
    property get category () : typekey.ClaimMetricCategory {
      return getIteratedValue(1) as typekey.ClaimMetricCategory
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/keymetrics/KeyMetrics.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class KeyMetricsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'available' attribute on ToolbarRangeInput (id=PolicyType_Input) at KeyMetrics.pcf: line 44, column 42
    function available_7 () : java.lang.Boolean {
      return not CurrentLocation.InEditMode
    }
    
    // 'canEdit' attribute on Page (id=KeyMetrics) at KeyMetrics.pcf: line 9, column 62
    function canEdit_424 () : java.lang.Boolean {
      return perm.ClaimMetricLimit.edit
    }
    
    // 'canVisit' attribute on Page (id=KeyMetrics) at KeyMetrics.pcf: line 9, column 62
    static function canVisit_425 () : java.lang.Boolean {
      return perm.ClaimMetricLimit.view
    }
    
    // 'def' attribute on InputSetRef at KeyMetrics.pcf: line 355, column 89
    function def_onEnter_422 (def :  pcf.LargeLossThresholdInputSet) : void {
      def.onEnter(policyType, ccThreshold, psThreshold)
    }
    
    // 'def' attribute on InputSetRef at KeyMetrics.pcf: line 355, column 89
    function def_refreshVariables_423 (def :  pcf.LargeLossThresholdInputSet) : void {
      def.refreshVariables(policyType, ccThreshold, psThreshold)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=PolicyType_Input) at KeyMetrics.pcf: line 44, column 42
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyType = (__VALUE_TO_SET as typekey.PolicyType)
    }
    
    // 'filter' attribute on ToolbarRangeInput (id=PolicyType_Input) at KeyMetrics.pcf: line 44, column 42
    function filter_10 (VALUE :  typekey.PolicyType, VALUES :  typekey.PolicyType[]) : java.lang.Object {
      return PolicyType.TF_METRICSFILTERS_TDIC.getTypeKeys()
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=YellowValueHeader_Cell) at KeyMetrics.pcf: line 84, column 40
    function iconColor_16 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_ALERT_WARNING
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=RedValueHeader_Cell) at KeyMetrics.pcf: line 93, column 40
    function iconColor_17 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_ALERT_ERROR
    }
    
    // 'initialValue' attribute on Variable at KeyMetrics.pcf: line 15, column 26
    function initialValue_0 () : PolicyType {
      return PolicyType.TC_BUSINESSOWNERS
    }
    
    // 'initialValue' attribute on Variable at KeyMetrics.pcf: line 19, column 34
    function initialValue_1 () : LargeLossThreshold {
      return getThreshold(LargeLossNotificationType.TC_CC)
    }
    
    // 'initialValue' attribute on Variable at KeyMetrics.pcf: line 23, column 34
    function initialValue_2 () : LargeLossThreshold {
      return getThreshold(LargeLossNotificationType.TC_PS)
    }
    
    // 'initialValue' attribute on Variable at KeyMetrics.pcf: line 27, column 45
    function initialValue_3 () : entity.PolicyTypeMetricLimits {
      return PolicyTypeMetricLimits.cache.limitsForPolicyType(policyType).ensureDefaultLimitsCreated()
    }
    
    // 'initialValue' attribute on Variable at KeyMetrics.pcf: line 31, column 19
    function initialValue_4 () : int {
      return 1000
    }
    
    // EditButtons at KeyMetrics.pcf: line 34, column 23
    function label_5 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'onChange' attribute on PostOnChange at KeyMetrics.pcf: line 46, column 45
    function onChange_6 () : void {
      policyTypeChanged()
    }
    
    // Page (id=KeyMetrics) at KeyMetrics.pcf: line 9, column 62
    static function parent_426 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyType_Input) at KeyMetrics.pcf: line 44, column 42
    function valueRange_11 () : java.lang.Object {
      return typekey.PolicyType.getTypeKeys( false )
    }
    
    // 'value' attribute on RowIterator (id=ClaimMetricLimitCategoryIterator) at KeyMetrics.pcf: line 100, column 59
    function value_220 () : typekey.ClaimMetricCategory[] {
      return ClaimMetricCategory.getTypeKeys(false).toTypedArray()
    }
    
    // 'value' attribute on RowIterator (id=ExposureMetricDefaultLimitIterator) at KeyMetrics.pcf: line 262, column 58
    function value_421 () : entity.ExposureMetricLimit[] {
      return limits.ExposureMetricLimits.where(\ l -> l.DefaultLimit).sortBy(\ l -> l.ExposureMetricType)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=PolicyType_Input) at KeyMetrics.pcf: line 44, column 42
    function value_8 () : typekey.PolicyType {
      return policyType
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyType_Input) at KeyMetrics.pcf: line 44, column 42
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyType_Input) at KeyMetrics.pcf: line 44, column 42
    function verifyValueRangeIsAllowedType_12 ($$arg :  typekey.PolicyType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyType_Input) at KeyMetrics.pcf: line 44, column 42
    function verifyValueRange_13 () : void {
      var __valueRangeArg = typekey.PolicyType.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.KeyMetrics {
      return super.CurrentLocation as pcf.KeyMetrics
    }
    
    property get MAX_METRIC_DAYS () : int {
      return getVariableValue("MAX_METRIC_DAYS", 0) as java.lang.Integer
    }
    
    property set MAX_METRIC_DAYS ($arg :  int) {
      setVariableValue("MAX_METRIC_DAYS", 0, $arg)
    }
    
    property get ccThreshold () : LargeLossThreshold {
      return getVariableValue("ccThreshold", 0) as LargeLossThreshold
    }
    
    property set ccThreshold ($arg :  LargeLossThreshold) {
      setVariableValue("ccThreshold", 0, $arg)
    }
    
    property get limits () : entity.PolicyTypeMetricLimits {
      return getVariableValue("limits", 0) as entity.PolicyTypeMetricLimits
    }
    
    property set limits ($arg :  entity.PolicyTypeMetricLimits) {
      setVariableValue("limits", 0, $arg)
    }
    
    property get policyType () : PolicyType {
      return getVariableValue("policyType", 0) as PolicyType
    }
    
    property set policyType ($arg :  PolicyType) {
      setVariableValue("policyType", 0, $arg)
    }
    
    property get psThreshold () : LargeLossThreshold {
      return getVariableValue("psThreshold", 0) as LargeLossThreshold
    }
    
    property set psThreshold ($arg :  LargeLossThreshold) {
      setVariableValue("psThreshold", 0, $arg)
    }
    
    function policyTypeChanged() {
      limits = PolicyTypeMetricLimits.cache.limitsForPolicyType(policyType).ensureDefaultLimitsCreated()
      initThresholds()
    }
    
    function getThreshold(type : LargeLossNotificationType) : LargeLossThreshold {
      var result = LargeLossThreshold.getThreshold( policyType, type )
      if (result == null) {
        result = new LargeLossThreshold()
        result.PolicyType = policyType
        result.NotificationType = type
      }
      return result
    }
    
    function initThresholds() {
      ccThreshold = getThreshold(LargeLossNotificationType.TC_CC)
      psThreshold = getThreshold(LargeLossNotificationType.TC_PS)
    }
    
    function getClaimTiersWithNoLimit(defaultLimit : ClaimMetricLimit) : ClaimTier[] {
      var existingTiers = defaultLimit.PolicyTypeMetricLimits.ClaimMetricLimits
              .where(\l -> l != defaultLimit and l.ClaimMetricType == defaultLimit.ClaimMetricType and l.ClaimTier != null and l.Currency == defaultLimit.Currency)
              .map(\l -> l.ClaimTier)
              .toSet()
      return ClaimTier.getTypeKeys(false)
              .where(\ t -> t.hasCategory(defaultLimit.PolicyTypeMetricLimits.PolicyType) and not existingTiers.contains(t))
              .toTypedArray()
    }
    
    function getSortedClaimMetricLimits(category : ClaimMetricCategory, defaultLimit : ClaimMetricLimit) : ClaimMetricLimit[] {
      return limits.ClaimMetricLimits.where(\ l -> l.ClaimMetricCategory == category 
                                               and l.ClaimMetricType == defaultLimit.ClaimMetricType 
                                               and not l.DefaultLimit 
                                               and l.Currency == defaultLimit.Currency)
                                     .sortBy(\ l -> l.ClaimTier)  
    }
    
    function getExposureTiersWithNoLimit(defaultLimit : ExposureMetricLimit) : ExposureTier[] {
      var existingTiers = defaultLimit.PolicyTypeMetricLimits.ExposureMetricLimits
              .where(\l -> l != defaultLimit and l.ExposureMetricType == defaultLimit.ExposureMetricType and l.ExposureTier != null and l.Currency == defaultLimit.Currency)
              .map(\l -> l.ExposureTier)
              .toSet()
      return ExposureTier.getTypeKeys(false)
              .where(\ t -> t.hasCategory(defaultLimit.PolicyTypeMetricLimits.PolicyType) and not existingTiers.contains(t))
              .toTypedArray()
    }
    
    function getSortedExposureMetricLimits(defaultLimit : ExposureMetricLimit) : ExposureMetricLimit[] {
      return limits.ExposureMetricLimits.where(\ l -> l.ExposureMetricType == defaultLimit.ExposureMetricType 
                                               and not l.DefaultLimit 
                                               and l.Currency == defaultLimit.Currency)
                                     .sortBy(\ l -> l.ExposureTier)  
    }
    
    /**
     * Ensure that the input is valid for the given metric.
     */
    function validateMetricInput(input:gw.api.metric.EditableMetricLimitValue, metricLimit:ClaimMetricLimit) : String {
      var validationResult:String = null
      switch(metricLimit.Unit) {
        case MetricUnit.TC_DAYS:
            if(input.IntegerValue > MAX_METRIC_DAYS) {
              validationResult = DisplayKey.get("Web.Admin.KeyMetrics.Input.MaxDaysExceeded", MAX_METRIC_DAYS, metricLimit.ClaimMetricType)
            }
      }
      return validationResult
    }
    
    function validateMetricInput(input:gw.api.metric.EditableMetricLimitValue, metricLimit:ExposureMetricLimit) : String {
      var validationResult:String = null
      switch(metricLimit.Unit) {
            case MetricUnit.TC_DAYS:
            if(input.IntegerValue > MAX_METRIC_DAYS) {
              validationResult = DisplayKey.get("Web.Admin.KeyMetrics.Input.MaxDaysExceeded", MAX_METRIC_DAYS, metricLimit.ExposureMetricType)
            }
      }
      return validationResult
    }
    
    
  }
  
  
}