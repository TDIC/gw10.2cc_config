package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/policy/PolicyGeneralPanelSet.Gl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyGeneralPanelSet_GlExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/policy/PolicyGeneralPanelSet.Gl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyCoverageListDetailExpressionsImpl extends PolicyGeneralPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at PolicyGeneralPanelSet.Gl.pcf: line 253, column 37
    function def_onEnter_256 (def :  pcf.EditableGeneralLiabilityPolicyCoveragesLV) : void {
      def.onEnter(Policy)
    }
    
    // 'def' attribute on PanelRef at PolicyGeneralPanelSet.Gl.pcf: line 267, column 26
    function def_onEnter_258 (def :  pcf.ClaimPolicyCovTermsCV) : void {
      def.onEnter(Coverage)
    }
    
    // 'def' attribute on PanelRef at PolicyGeneralPanelSet.Gl.pcf: line 253, column 37
    function def_refreshVariables_257 (def :  pcf.EditableGeneralLiabilityPolicyCoveragesLV) : void {
      def.refreshVariables(Policy)
    }
    
    // 'def' attribute on PanelRef at PolicyGeneralPanelSet.Gl.pcf: line 267, column 26
    function def_refreshVariables_259 (def :  pcf.ClaimPolicyCovTermsCV) : void {
      def.refreshVariables(Coverage)
    }
    
    // 'addVisible' attribute on IteratorButtons at PolicyGeneralPanelSet.Gl.pcf: line 262, column 47
    function visible_252 () : java.lang.Boolean {
      return !Policy.Verified
    }
    
    property get Coverage () : Coverage {
      return getCurrentSelection(1) as Coverage
    }
    
    property set Coverage ($arg :  Coverage) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/policy/PolicyGeneralPanelSet.Gl.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyGeneralPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_105 () : void {
      AddressBookPickerPopup.push(statictypeof (Policy.policyholder), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_107 () : void {
      if (Policy.policyholder != null) { ClaimContactDetailPopup.push(Policy.policyholder, Policy.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_110 () : void {
      ClaimContactDetailPopup.push(Policy.policyholder, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_130 () : void {
      AddressBookPickerPopup.push(statictypeof (Policy.doingbusinessas), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_132 () : void {
      if (Policy.doingbusinessas != null) { ClaimContactDetailPopup.push(Policy.doingbusinessas, Policy.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_135 () : void {
      ClaimContactDetailPopup.push(Policy.doingbusinessas, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_166 () : void {
      AddressBookPickerPopup.push(statictypeof (Policy.agent), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_168 () : void {
      if (Policy.agent != null) { ClaimContactDetailPopup.push(Policy.agent, Policy.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_170 () : void {
      ClaimContactDetailPopup.push(Policy.agent, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_194 () : void {
      AddressBookPickerPopup.push(statictypeof (Policy.underwriter), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_196 () : void {
      if (Policy.underwriter != null) { ClaimContactDetailPopup.push(Policy.underwriter, Policy.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_198 () : void {
      ClaimContactDetailPopup.push(Policy.underwriter, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_62 () : void {
      AddressBookPickerPopup.push(statictypeof (Policy.insured), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_64 () : void {
      if (Policy.insured != null) { ClaimContactDetailPopup.push(Policy.insured, Policy.Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_66 () : void {
      ClaimContactDetailPopup.push(Policy.insured, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_106 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Policy.policyholder), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_111 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Policy.policyholder, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_131 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Policy.doingbusinessas), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_136 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Policy.doingbusinessas, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_167 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Policy.agent), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_171 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Policy.agent, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_195 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Policy.underwriter), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_199 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Policy.underwriter, Policy.Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_63 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Policy.insured), Policy.Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_67 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Policy.insured, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_102 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Policy.policyholder), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_127 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Policy.doingbusinessas), null, Policy.Claim)
    }
    
    // 'def' attribute on ListViewInput at PolicyGeneralPanelSet.Gl.pcf: line 155, column 72
    function def_onEnter_154 (def :  pcf.EditableAdditionalInsuredLV) : void {
      def.onEnter(Policy.getClaimContactRolesByRole(TC_COVEREDPARTY), Policy.Claim, Policy, TC_COVEREDPARTY)
    }
    
    // 'def' attribute on ListViewInput at PolicyGeneralPanelSet.Gl.pcf: line 168, column 91
    function def_onEnter_159 (def :  pcf.EditableExcludedPartiesLV) : void {
      def.onEnter(Policy.getClaimContactRolesByRole(TC_EXCLUDEDPARTY), Policy, TC_EXCLUDEDPARTY)
    }
    
    // 'def' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_163 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Policy.agent), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_191 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Policy.underwriter), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_59 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Policy.insured), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_103 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Policy.policyholder), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_128 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Policy.doingbusinessas), null, Policy.Claim)
    }
    
    // 'def' attribute on ListViewInput at PolicyGeneralPanelSet.Gl.pcf: line 155, column 72
    function def_refreshVariables_155 (def :  pcf.EditableAdditionalInsuredLV) : void {
      def.refreshVariables(Policy.getClaimContactRolesByRole(TC_COVEREDPARTY), Policy.Claim, Policy, TC_COVEREDPARTY)
    }
    
    // 'def' attribute on ListViewInput at PolicyGeneralPanelSet.Gl.pcf: line 168, column 91
    function def_refreshVariables_160 (def :  pcf.EditableExcludedPartiesLV) : void {
      def.refreshVariables(Policy.getClaimContactRolesByRole(TC_EXCLUDEDPARTY), Policy, TC_EXCLUDEDPARTY)
    }
    
    // 'def' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_164 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Policy.agent), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_192 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Policy.underwriter), null, Policy.Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_60 (def :  pcf.ClaimNewContactPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Policy.insured), null, Policy.Claim)
    }
    
    // 'value' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_114 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.policyholder = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_139 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.doingbusinessas = (__VALUE_TO_SET as entity.Company)
    }
    
    // 'value' attribute on TypeKeyInput (id=Offerings_Input) at PolicyGeneralPanelSet.Gl.pcf: line 53, column 46
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.Offerings_TDIC = (__VALUE_TO_SET as typekey.Offering_TDIC)
    }
    
    // 'value' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_174 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.agent = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TextInput (id=ProducerCode_Input) at PolicyGeneralPanelSet.Gl.pcf: line 189, column 40
    function defaultSetter_186 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.ProducerCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_202 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.underwriter = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on TypeKeyInput (id=Underwriting_Company_Input) at PolicyGeneralPanelSet.Gl.pcf: line 208, column 56
    function defaultSetter_215 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.UnderwritingCo = (__VALUE_TO_SET as typekey.UnderwritingCompanyType)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at PolicyGeneralPanelSet.Gl.pcf: line 59, column 41
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Underwriting_Group_Input) at PolicyGeneralPanelSet.Gl.pcf: line 214, column 54
    function defaultSetter_221 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.UnderwritingGroup = (__VALUE_TO_SET as typekey.UnderwritingGroupType)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Other_ForeignCoverage_Input) at PolicyGeneralPanelSet.Gl.pcf: line 234, column 43
    function defaultSetter_236 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.ForeignCoverage = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Other_OtherFinancialInterests_Input) at PolicyGeneralPanelSet.Gl.pcf: line 239, column 46
    function defaultSetter_242 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.FinancialInterests = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Other_Notes_Input) at PolicyGeneralPanelSet.Gl.pcf: line 244, column 33
    function defaultSetter_248 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.Notes = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at PolicyGeneralPanelSet.Gl.pcf: line 65, column 42
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=CancellationDate_Input) at PolicyGeneralPanelSet.Gl.pcf: line 70, column 44
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.CancellationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=OrigEffectiveDate_Input) at PolicyGeneralPanelSet.Gl.pcf: line 75, column 45
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.OrigEffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at PolicyGeneralPanelSet.Gl.pcf: line 81, column 45
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.Status = (__VALUE_TO_SET as typekey.PolicyStatus)
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at PolicyGeneralPanelSet.Gl.pcf: line 88, column 68
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_70 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.insured = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at PolicyGeneralPanelSet.Gl.pcf: line 120, column 40
    function defaultSetter_90 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=CustomerServiceTierRange_Input) at PolicyGeneralPanelSet.Gl.pcf: line 129, column 52
    function defaultSetter_95 (__VALUE_TO_SET :  java.lang.Object) : void {
      Policy.CustomerServiceTier = (__VALUE_TO_SET as typekey.CustomerServiceTier)
    }
    
    // 'editable' attribute on DateInput (id=EffectiveDate_Input) at PolicyGeneralPanelSet.Gl.pcf: line 59, column 41
    function editable_20 () : java.lang.Boolean {
      return !Policy.Verified
    }
    
    // 'editable' attribute on TypeKeyInput (id=Currency_Input) at PolicyGeneralPanelSet.Gl.pcf: line 88, column 68
    function editable_50 () : java.lang.Boolean {
      return !Policy.Verified and Policy.CurrencyEditable
    }
    
    // 'initialValue' attribute on Variable at PolicyGeneralPanelSet.Gl.pcf: line 19, column 65
    function initialValue_0 () : java.util.List<typekey.CustomerServiceTier> {
      return CustomerServiceTier.findAvailableCustomerTiers()
    }
    
    // 'onPick' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_112 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Policy.policyholder); var result = eval("Policy.policyholder = Policy.Claim.resolveContact(Policy.policyholder) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_137 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Policy.doingbusinessas); var result = eval("Policy.doingbusinessas = Policy.Claim.resolveContact(Policy.doingbusinessas) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_172 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Policy.agent); var result = eval("Policy.agent = Policy.Claim.resolveContact(Policy.agent) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_200 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Policy.underwriter); var result = eval("Policy.underwriter = Policy.Claim.resolveContact(Policy.underwriter) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_68 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Policy.insured); var result = eval("Policy.insured = Policy.Claim.resolveContact(Policy.insured) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'value' attribute on Reflect at PolicyGeneralPanelSet.Gl.pcf: line 108, column 55
    function reflectionValue_80 (TRIGGER_INDEX :  int, VALUE :  entity.Contact) : java.lang.Object {
      return VALUE.PrimaryAddressDisplayValue
    }
    
    // 'validationExpression' attribute on ListViewInput at PolicyGeneralPanelSet.Gl.pcf: line 155, column 72
    function validationExpression_153 () : java.lang.Object {
      return Policy.checkCoveredPartyConstraints()
    }
    
    // 'validationExpression' attribute on ListViewInput at PolicyGeneralPanelSet.Gl.pcf: line 168, column 91
    function validationExpression_157 () : java.lang.Object {
      return Policy.checkExcludedPartyConstraints()
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_141 () : java.lang.Object {
      return Policy.Claim.RelatedCompanyArray
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_204 () : java.lang.Object {
      return Policy.Claim.RelatedPersonArray
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_72 () : java.lang.Object {
      return Policy.Claim.RelatedContacts
    }
    
    // 'valueRange' attribute on RangeInput (id=CustomerServiceTierRange_Input) at PolicyGeneralPanelSet.Gl.pcf: line 129, column 52
    function valueRange_97 () : java.lang.Object {
      return availableCustomerServiceTiers
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at PolicyGeneralPanelSet.Gl.pcf: line 29, column 40
    function valueRoot_2 () : java.lang.Object {
      return Policy
    }
    
    // 'value' attribute on TextInput (id=Insured_Address_Input) at PolicyGeneralPanelSet.Gl.pcf: line 105, column 61
    function valueRoot_83 () : java.lang.Object {
      return Policy.insured
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at PolicyGeneralPanelSet.Gl.pcf: line 114, column 67
    function valueRoot_86 () : java.lang.Object {
      return Policy.PolicyAccount.AccountHolder
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at PolicyGeneralPanelSet.Gl.pcf: line 29, column 40
    function value_1 () : java.lang.String {
      return Policy.PolicyNumber
    }
    
    // 'value' attribute on TextInput (id=LegacySourceSystem_TDIC_Input) at PolicyGeneralPanelSet.Gl.pcf: line 41, column 51
    function value_10 () : java.lang.String {
      return Policy.LegacyPolicySource_TDIC
    }
    
    // 'value' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_113 () : entity.Contact {
      return Policy.policyholder
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at PolicyGeneralPanelSet.Gl.pcf: line 47, column 43
    function value_13 () : typekey.PolicyType {
      return Policy.PolicyType
    }
    
    // 'value' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_138 () : entity.Company {
      return Policy.doingbusinessas
    }
    
    // 'value' attribute on TypeKeyInput (id=Offerings_Input) at PolicyGeneralPanelSet.Gl.pcf: line 53, column 46
    function value_16 () : typekey.Offering_TDIC {
      return Policy.Offerings_TDIC
    }
    
    // 'value' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_173 () : entity.Contact {
      return Policy.agent
    }
    
    // 'value' attribute on TextInput (id=ProducerCode_Input) at PolicyGeneralPanelSet.Gl.pcf: line 189, column 40
    function value_185 () : java.lang.String {
      return Policy.ProducerCode
    }
    
    // 'value' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_201 () : entity.Person {
      return Policy.underwriter
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at PolicyGeneralPanelSet.Gl.pcf: line 59, column 41
    function value_21 () : java.util.Date {
      return Policy.EffectiveDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Underwriting_Company_Input) at PolicyGeneralPanelSet.Gl.pcf: line 208, column 56
    function value_214 () : typekey.UnderwritingCompanyType {
      return Policy.UnderwritingCo
    }
    
    // 'value' attribute on TypeKeyInput (id=Underwriting_Group_Input) at PolicyGeneralPanelSet.Gl.pcf: line 214, column 54
    function value_220 () : typekey.UnderwritingGroupType {
      return Policy.UnderwritingGroup
    }
    
    // 'value' attribute on BooleanRadioInput (id=Other_VerifiedPolicy_Input) at PolicyGeneralPanelSet.Gl.pcf: line 221, column 36
    function value_225 () : java.lang.Boolean {
      return Policy.Verified
    }
    
    // 'value' attribute on BooleanRadioInput (id=ERE_Input) at PolicyGeneralPanelSet.Gl.pcf: line 225, column 36
    function value_228 () : java.lang.Boolean {
      return Policy.ERE_TDIC
    }
    
    // 'value' attribute on DateInput (id=PriorActs_TDIC_Input) at PolicyGeneralPanelSet.Gl.pcf: line 229, column 42
    function value_231 () : java.util.Date {
      return Policy.PriorActs_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=Other_ForeignCoverage_Input) at PolicyGeneralPanelSet.Gl.pcf: line 234, column 43
    function value_235 () : java.lang.Boolean {
      return Policy.ForeignCoverage
    }
    
    // 'value' attribute on TextInput (id=Other_OtherFinancialInterests_Input) at PolicyGeneralPanelSet.Gl.pcf: line 239, column 46
    function value_241 () : java.lang.String {
      return Policy.FinancialInterests
    }
    
    // 'value' attribute on TextInput (id=Other_Notes_Input) at PolicyGeneralPanelSet.Gl.pcf: line 244, column 33
    function value_247 () : java.lang.String {
      return Policy.Notes
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at PolicyGeneralPanelSet.Gl.pcf: line 65, column 42
    function value_27 () : java.util.Date {
      return Policy.ExpirationDate
    }
    
    // 'value' attribute on DateInput (id=CancellationDate_Input) at PolicyGeneralPanelSet.Gl.pcf: line 70, column 44
    function value_33 () : java.util.Date {
      return Policy.CancellationDate
    }
    
    // 'value' attribute on DateInput (id=OrigEffectiveDate_Input) at PolicyGeneralPanelSet.Gl.pcf: line 75, column 45
    function value_39 () : java.util.Date {
      return Policy.OrigEffectiveDate
    }
    
    // 'value' attribute on TextInput (id=LegacyPolicyNumber_TDIC_Input) at PolicyGeneralPanelSet.Gl.pcf: line 33, column 51
    function value_4 () : java.lang.String {
      return Policy.LegacyPolicyNumber_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at PolicyGeneralPanelSet.Gl.pcf: line 81, column 45
    function value_45 () : typekey.PolicyStatus {
      return Policy.Status
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at PolicyGeneralPanelSet.Gl.pcf: line 88, column 68
    function value_52 () : typekey.Currency {
      return Policy.Currency
    }
    
    // 'value' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_69 () : entity.Contact {
      return Policy.insured
    }
    
    // 'value' attribute on TextInput (id=LegacyPolicySuffix_TDIC_Input) at PolicyGeneralPanelSet.Gl.pcf: line 37, column 51
    function value_7 () : java.lang.String {
      return Policy.LegacyPolicySuffix_TDIC
    }
    
    // 'value' attribute on TextInput (id=Insured_Address_Input) at PolicyGeneralPanelSet.Gl.pcf: line 105, column 61
    function value_82 () : java.lang.String {
      return Policy.insured.PrimaryAddressDisplayValue
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at PolicyGeneralPanelSet.Gl.pcf: line 114, column 67
    function value_85 () : java.lang.String {
      return Policy.PolicyAccount.AccountHolder.DisplayName
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at PolicyGeneralPanelSet.Gl.pcf: line 120, column 40
    function value_89 () : java.lang.String {
      return Policy.AccountNumber
    }
    
    // 'value' attribute on RangeInput (id=CustomerServiceTierRange_Input) at PolicyGeneralPanelSet.Gl.pcf: line 129, column 52
    function value_94 () : typekey.CustomerServiceTier {
      return Policy.CustomerServiceTier
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_117 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_117 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_117 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_142 ($$arg :  entity.Company[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_142 ($$arg :  gw.api.database.IQueryBeanResult<entity.Company>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_142 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_177 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_177 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_177 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_205 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_205 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_205 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_73 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_73 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_73 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CustomerServiceTierRange_Input) at PolicyGeneralPanelSet.Gl.pcf: line 129, column 52
    function verifyValueRangeIsAllowedType_98 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CustomerServiceTierRange_Input) at PolicyGeneralPanelSet.Gl.pcf: line 129, column 52
    function verifyValueRangeIsAllowedType_98 ($$arg :  typekey.CustomerServiceTier[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_118 () : void {
      var __valueRangeArg = Policy.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_117(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_143 () : void {
      var __valueRangeArg = Policy.Claim.RelatedCompanyArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_142(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_178 () : void {
      var __valueRangeArg = Policy.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_177(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_206 () : void {
      var __valueRangeArg = Policy.Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_205(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_74 () : void {
      var __valueRangeArg = Policy.Claim.RelatedContacts
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_73(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=CustomerServiceTierRange_Input) at PolicyGeneralPanelSet.Gl.pcf: line 129, column 52
    function verifyValueRange_99 () : void {
      var __valueRangeArg = availableCustomerServiceTiers
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_98(__valueRangeArg)
    }
    
    // 'valueType' attribute on ClaimContactInput (id=DBA_Input) at PolicyGeneralPanelSet.Gl.pcf: line 147, column 46
    function verifyValueType_151 () : void {
      var __valueTypeArg : entity.Company
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Underwriter_Name_Input) at PolicyGeneralPanelSet.Gl.pcf: line 202, column 38
    function verifyValueType_212 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'visible' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_104 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Policy.policyholder), Policy.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=PolicyHolder_Input) at ClaimContactWidget.xml: line 12, column 273
    function visible_109 () : java.lang.Boolean {
      return Policy.CommercialPolicy
    }
    
    // 'visible' attribute on ClaimContactInput (id=DBA_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_129 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Policy.doingbusinessas), Policy.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ListViewInput at PolicyGeneralPanelSet.Gl.pcf: line 168, column 91
    function visible_158 () : java.lang.Boolean {
      return Claim.LossType != LossType.TC_PR and Claim.LossType != LossType.TC_GL
    }
    
    // 'visible' attribute on ClaimContactInput (id=Agent_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_165 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Policy.agent), Policy.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=Underwriter_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_193 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Policy.underwriter), Policy.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on TypeKeyInput (id=Currency_Input) at PolicyGeneralPanelSet.Gl.pcf: line 88, column 68
    function visible_51 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_58 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=Insured_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_61 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Policy.insured), Policy.Claim, null as List<SpecialistService>)" != "" && true
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get Policy () : Policy {
      return getRequireValue("Policy", 0) as Policy
    }
    
    property set Policy ($arg :  Policy) {
      setRequireValue("Policy", 0, $arg)
    }
    
    property get availableCustomerServiceTiers () : java.util.List<typekey.CustomerServiceTier> {
      return getVariableValue("availableCustomerServiceTiers", 0) as java.util.List<typekey.CustomerServiceTier>
    }
    
    property set availableCustomerServiceTiers ($arg :  java.util.List<typekey.CustomerServiceTier>) {
      setVariableValue("availableCustomerServiceTiers", 0, $arg)
    }
    
    
  }
  
  
}