package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseReqShareActionMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBaseReqShareActionMenuItemSetExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseReqShareActionMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseReqShareActionMenuItemSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=Request) at OnBaseReqShareActionMenuItemSet.pcf: line 13, column 101
    function action_0 () : void {
      ShareBaseFolderRequestWizard.push(Claim)
    }
    
    // 'action' attribute on MenuItem (id=Share) at OnBaseReqShareActionMenuItemSet.pcf: line 17, column 99
    function action_2 () : void {
      ShareBaseShareDocumentsWizard.push(Claim)
    }
    
    // 'action' attribute on MenuItem (id=Request) at OnBaseReqShareActionMenuItemSet.pcf: line 13, column 101
    function action_dest_1 () : pcf.api.Destination {
      return pcf.ShareBaseFolderRequestWizard.createDestination(Claim)
    }
    
    // 'action' attribute on MenuItem (id=Share) at OnBaseReqShareActionMenuItemSet.pcf: line 17, column 99
    function action_dest_3 () : pcf.api.Destination {
      return pcf.ShareBaseShareDocumentsWizard.createDestination(Claim)
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    
  }
  
  
}