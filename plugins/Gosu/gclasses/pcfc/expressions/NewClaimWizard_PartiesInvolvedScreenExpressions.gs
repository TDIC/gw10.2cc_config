package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.lang.reflect.IType
@javax.annotation.Generated("config/web/pcf/claim/FNOL/NewClaimWizard_PartiesInvolvedScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewClaimWizard_PartiesInvolvedScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/NewClaimWizard_PartiesInvolvedScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewClaimWizard_PartiesInvolvedScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_NewPerson) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 92, column 76
    function action_0 () : void {
      NewPartyInvolvedPopup.push(Claim, typekey.Contact.TC_PERSON, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Company) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 124, column 77
    function action_10 () : void {
      NewPartyInvolvedPopup.push(Claim, typekey.Contact.TC_COMPANY, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Adjudicator) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 131, column 94
    function action_12 () : void {
      NewPartyInvolvedPopup.push(Claim, typekey.Contact.TC_ATTORNEY, DisplayKey.get("Web.NewContactMenu.PlaintiffAttorney_TDIC"))
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Attorney) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 135, column 92
    function action_14 () : void {
      NewPartyInvolvedPopup.push(Claim, typekey.Contact.TC_ATTORNEY, DisplayKey.get("Web.NewContactMenu.DefenseAttorney_TDIC"))
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LawFirm) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 139, column 79
    function action_16 () : void {
      NewPartyInvolvedPopup.push(Claim, typekey.Contact.TC_LAWFIRM, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LegalVenue) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 143, column 85
    function action_18 () : void {
      NewPartyInvolvedPopup.push(Claim, typekey.Contact.TC_ATTORNEY, DisplayKey.get("Web.NewContactMenu.Mediator_TDIC"))
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Doctor) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 107, column 77
    function action_2 () : void {
      NewPartyInvolvedPopup.push(Claim, typekey.Contact.TC_DOCTOR, null)
    }
    
    // 'action' attribute on PickerToolbarButton (id=NewClaimWizard_PartiesInvolvedScreen_AddExistingButton) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 152, column 45
    function action_22 () : void {
      AddressBookPickerPopup.push(entity.Contact, Claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_MedicalCareOrg) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 111, column 86
    function action_4 () : void {
      NewPartyInvolvedPopup.push(Claim, typekey.Contact.TC_MEDICALCAREORG, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_CompanyVendor) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 115, column 85
    function action_6 () : void {
      NewPartyInvolvedPopup.push(Claim, typekey.Contact.TC_COMPANYVENDOR, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_PersonVendor) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 119, column 84
    function action_8 () : void {
      NewPartyInvolvedPopup.push(Claim, typekey.Contact.TC_PERSONVENDOR, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_NewPerson) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 92, column 76
    function action_dest_1 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(Claim, typekey.Contact.TC_PERSON, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Company) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 124, column 77
    function action_dest_11 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(Claim, typekey.Contact.TC_COMPANY, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Adjudicator) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 131, column 94
    function action_dest_13 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(Claim, typekey.Contact.TC_ATTORNEY, DisplayKey.get("Web.NewContactMenu.PlaintiffAttorney_TDIC"))
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Attorney) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 135, column 92
    function action_dest_15 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(Claim, typekey.Contact.TC_ATTORNEY, DisplayKey.get("Web.NewContactMenu.DefenseAttorney_TDIC"))
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LawFirm) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 139, column 79
    function action_dest_17 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(Claim, typekey.Contact.TC_LAWFIRM, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_LegalVenue) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 143, column 85
    function action_dest_19 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(Claim, typekey.Contact.TC_ATTORNEY, DisplayKey.get("Web.NewContactMenu.Mediator_TDIC"))
    }
    
    // 'action' attribute on PickerToolbarButton (id=NewClaimWizard_PartiesInvolvedScreen_AddExistingButton) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 152, column 45
    function action_dest_23 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(entity.Contact, Claim)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_Doctor) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 107, column 77
    function action_dest_3 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(Claim, typekey.Contact.TC_DOCTOR, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_MedicalCareOrg) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 111, column 86
    function action_dest_5 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(Claim, typekey.Contact.TC_MEDICALCAREORG, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_CompanyVendor) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 115, column 85
    function action_dest_7 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(Claim, typekey.Contact.TC_COMPANYVENDOR, null)
    }
    
    // 'action' attribute on MenuItem (id=ClaimContacts_PersonVendor) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 119, column 84
    function action_dest_9 () : pcf.api.Destination {
      return pcf.NewPartyInvolvedPopup.createDestination(Claim, typekey.Contact.TC_PERSONVENDOR, null)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=NewClaimWizard_PartiesInvolvedScreen_DeleteButton) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 159, column 25
    function allCheckedRowsAction_25 (CheckedValues :  entity.ClaimContact[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.claimparties.ClaimContactUtil.deleteNewClaimContacts(CurrentLocation, Claim, CheckedValues, CheckedValuesErrors)
    }
    
    // 'def' attribute on PanelRef at NewClaimWizard_PartiesInvolvedScreen.pcf: line 21, column 61
    function def_onEnter_26 (def :  pcf.NewClaimPeopleInvolvedDetailedLV) : void {
      def.onEnter(Wizard, Claim)
    }
    
    // 'def' attribute on PanelRef at NewClaimWizard_PartiesInvolvedScreen.pcf: line 21, column 61
    function def_refreshVariables_27 (def :  pcf.NewClaimPeopleInvolvedDetailedLV) : void {
      def.refreshVariables(Wizard, Claim)
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=NewClaimWizard_PartiesInvolvedScreen_AddExistingButton) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 152, column 45
    function onPick_24 (PickedValue :  Contact) : void {
      NewClaimWizard_PartyInvolvedPopup.push(Wizard, Wizard.addClaimContact(PickedValue), Wizard.isContactNew(PickedValue))
    }
    
    // 'visible' attribute on ToolbarButton (id=NewClaimWizard_PartiesInvolvedScreen_CreateNewContactButton) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 88, column 143
    function visible_20 () : java.lang.Boolean {
      return  perm.Contact.createlocal /* robk, US22: last clause added to disable CM integration; remove to recover OOTB condition */
    }
    
    // 'visible' attribute on PickerToolbarButton (id=NewClaimWizard_PartiesInvolvedScreen_AddExistingButton) at NewClaimWizard_PartiesInvolvedScreen.pcf: line 152, column 45
    function visible_21 () : java.lang.Boolean {
      return perm.Contact.viewlocal
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get Wizard () : gw.api.claim.NewClaimWizardInfo {
      return getRequireValue("Wizard", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set Wizard ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setRequireValue("Wizard", 0, $arg)
    }
    
    
    function createClaimContact(contactType : IType) {
      NewClaimWizard_PartyInvolvedPopup.push(Wizard, Wizard.createClaimContact(contactType), true);
    }
    
    
  }
  
  
}