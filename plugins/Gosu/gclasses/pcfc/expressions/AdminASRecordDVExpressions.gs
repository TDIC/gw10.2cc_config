package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/AdminASRecordDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminASRecordDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/assignstrategy_TDIC/AdminASRecordDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminASRecordDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=AssignTo_Input) at AdminASRecordDV.pcf: line 19, column 46
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      ASRecord.AssignTo = (__VALUE_TO_SET as typekey.ASAssignTo_TDIC)
    }
    
    // 'value' attribute on TextInput (id=Queue_Input) at AdminASRecordDV.pcf: line 40, column 37
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      ASRecord.QueueName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimType_Input) at AdminASRecordDV.pcf: line 26, column 44
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      ASRecord.ClaimType = (__VALUE_TO_SET as typekey.ClaimType_TDIC)
    }
    
    // 'value' attribute on TextInput (id=Group_Input) at AdminASRecordDV.pcf: line 34, column 37
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      ASRecord.GroupName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=AssignTo_Input) at AdminASRecordDV.pcf: line 19, column 46
    function valueRoot_2 () : java.lang.Object {
      return ASRecord
    }
    
    // 'value' attribute on TypeKeyInput (id=AssignTo_Input) at AdminASRecordDV.pcf: line 19, column 46
    function value_0 () : typekey.ASAssignTo_TDIC {
      return ASRecord.AssignTo
    }
    
    // 'value' attribute on TextInput (id=Queue_Input) at AdminASRecordDV.pcf: line 40, column 37
    function value_12 () : java.lang.String {
      return ASRecord.QueueName
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimType_Input) at AdminASRecordDV.pcf: line 26, column 44
    function value_4 () : typekey.ClaimType_TDIC {
      return ASRecord.ClaimType
    }
    
    // 'value' attribute on TextInput (id=Group_Input) at AdminASRecordDV.pcf: line 34, column 37
    function value_8 () : java.lang.String {
      return ASRecord.GroupName
    }
    
    property get ASRecord () : entity.AssignStrategy_TDIC {
      return getRequireValue("ASRecord", 0) as entity.AssignStrategy_TDIC
    }
    
    property set ASRecord ($arg :  entity.AssignStrategy_TDIC) {
      setRequireValue("ASRecord", 0, $arg)
    }
    
    
  }
  
  
}