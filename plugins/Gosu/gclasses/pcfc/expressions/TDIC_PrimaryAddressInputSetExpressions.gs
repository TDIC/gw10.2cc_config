package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.address.CCAddressOwner
uses tdic.cc.config.contacts.address.TDIC_DoctorContactHandleAddressOwner
uses tdic.cc.config.contacts.address.TDIC_InjuredContactHandleAddressOwner
@javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_PrimaryAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_PrimaryAddressInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/TDIC_PrimaryAddressInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_PrimaryAddressInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at TDIC_PrimaryAddressInputSet.pcf: line 22, column 56
    function def_onEnter_0 (def :  pcf.CCAddressInputSet) : void {
      def.onEnter(getAddressOwner_TDIC())
    }
    
    // 'def' attribute on InputSetRef at TDIC_PrimaryAddressInputSet.pcf: line 22, column 56
    function def_refreshVariables_1 (def :  pcf.CCAddressInputSet) : void {
      def.refreshVariables(getAddressOwner_TDIC())
    }
    
    property get contactHandle () : gw.api.contact.ContactHandle {
      return getRequireValue("contactHandle", 0) as gw.api.contact.ContactHandle
    }
    
    property set contactHandle ($arg :  gw.api.contact.ContactHandle) {
      setRequireValue("contactHandle", 0, $arg)
    }
    
    property get contactRole () : typekey.ContactRole {
      return getRequireValue("contactRole", 0) as typekey.ContactRole
    }
    
    property set contactRole ($arg :  typekey.ContactRole) {
      setRequireValue("contactRole", 0, $arg)
    }
    
    
    /**
     * US22
     * 08/26/2014 robk
     */
        function  getAddressOwner_TDIC() : CCAddressOwner {
          if (contactHandle.Contact.Subtype == typekey.Contact.TC_DOCTOR and (contactHandle.AddressOwner.Claim == null or contactHandle.AddressOwner.Claim.LossType == LossType.TC_WC7)) {
            return new TDIC_DoctorContactHandleAddressOwner(contactHandle)
          } else if ((contactRole == typekey.ContactRole.TC_INJURED or contactRole == typekey.ContactRole.TC_CLAIMANT) 
              and (contactHandle.AddressOwner.Claim == null or contactHandle.AddressOwner.Claim.LossType == LossType.TC_WC7)) {
            return new TDIC_InjuredContactHandleAddressOwner(contactHandle)
          } else {
            return contactHandle.AddressOwner
          }
        }
    
    
  }
  
  
}