package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/contacts/PersonNameInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PersonNameInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/contacts/PersonNameInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PersonNameInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PersonNameInputSet.pcf: line 23, column 54
    function def_onEnter_1 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.onEnter(new tdic.cc.config.contacts.name.TDIC_ContactNameOwner(new gw.api.name.CCContactHandlePersonNameDelegate(contactHandle)))
    }
    
    // 'def' attribute on InputSetRef at PersonNameInputSet.pcf: line 23, column 54
    function def_onEnter_3 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.onEnter(new tdic.cc.config.contacts.name.TDIC_ContactNameOwner(new gw.api.name.CCContactHandlePersonNameDelegate(contactHandle)))
    }
    
    // 'def' attribute on InputSetRef at PersonNameInputSet.pcf: line 23, column 54
    function def_refreshVariables_2 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.refreshVariables(new tdic.cc.config.contacts.name.TDIC_ContactNameOwner(new gw.api.name.CCContactHandlePersonNameDelegate(contactHandle)))
    }
    
    // 'def' attribute on InputSetRef at PersonNameInputSet.pcf: line 23, column 54
    function def_refreshVariables_4 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.refreshVariables(new tdic.cc.config.contacts.name.TDIC_ContactNameOwner(new gw.api.name.CCContactHandlePersonNameDelegate(contactHandle)))
    }
    
    // 'value' attribute on TextInput (id=FormerName_Input) at PersonNameInputSet.pcf: line 40, column 26
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.FormerName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Credentials_Input) at PersonNameInputSet.pcf: line 31, column 46
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      Person.Credential_TDIC = (__VALUE_TO_SET as typekey.Credential_TDIC)
    }
    
    // 'label' attribute on Label at PersonNameInputSet.pcf: line 20, column 48
    function label_0 () : java.lang.String {
      return Person.getSubtype().DisplayName
    }
    
    // 'mode' attribute on InputSetRef at PersonNameInputSet.pcf: line 23, column 54
    function mode_5 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'value' attribute on TypeKeyInput (id=Credentials_Input) at PersonNameInputSet.pcf: line 31, column 46
    function valueRoot_8 () : java.lang.Object {
      return Person
    }
    
    // 'value' attribute on TextInput (id=FormerName_Input) at PersonNameInputSet.pcf: line 40, column 26
    function value_11 () : java.lang.String {
      return Person.FormerName
    }
    
    // 'value' attribute on TypeKeyInput (id=Credentials_Input) at PersonNameInputSet.pcf: line 31, column 46
    function value_6 () : typekey.Credential_TDIC {
      return Person.Credential_TDIC
    }
    
    // 'visible' attribute on InputSet at PersonNameInputSet.pcf: line 25, column 109
    function visible_10 () : java.lang.Boolean {
      return Person.Subtype == TC_PERSON or Person.Subtype == TC_DOCTOR or Person.Subtype == TC_ATTORNEY
    }
    
    // 'visible' attribute on InputSet at PersonNameInputSet.pcf: line 34, column 45
    function visible_15 () : java.lang.Boolean {
      return Person.Subtype == TC_PERSON
    }
    
    property get contactHandle () : gw.api.contact.ContactHandle {
      return getRequireValue("contactHandle", 0) as gw.api.contact.ContactHandle
    }
    
    property set contactHandle ($arg :  gw.api.contact.ContactHandle) {
      setRequireValue("contactHandle", 0, $arg)
    }
    
    property get Person() : Person { return contactHandle.Contact as Person; }
    
    
  }
  
  
}