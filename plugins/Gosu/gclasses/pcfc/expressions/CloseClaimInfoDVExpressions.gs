package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/CloseClaimInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CloseClaimInfoDVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/CloseClaimInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CloseClaimInfoDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextAreaInput (id=Note_Input) at CloseClaimInfoDV.pcf: line 19, column 38
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      CloseClaimInfo.note = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=Outcome_Input) at CloseClaimInfoDV.pcf: line 27, column 52
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      CloseClaimInfo.outcome = (__VALUE_TO_SET as typekey.ClaimClosedOutcomeType)
    }
    
    // 'onChange' attribute on PostOnChange at CloseClaimInfoDV.pcf: line 29, column 160
    function onChange_5 () : void {
      CloseClaimInfo.Claim.ClosedOutcome = CloseClaimInfo.outcome ; CloseClaimInfo.Claim.validateClaimClosedOutcomeTypes(CloseClaimInfo.Claim)
    }
    
    // 'required' attribute on TextAreaInput (id=Note_Input) at CloseClaimInfoDV.pcf: line 19, column 38
    function required_0 () : java.lang.Boolean {
      return !CloseClaimInfo.Claim.isWorkCompClaim()
    }
    
    // 'valueRange' attribute on RangeInput (id=Outcome_Input) at CloseClaimInfoDV.pcf: line 27, column 52
    function valueRange_10 () : java.lang.Object {
      return ClosedClaimOutcome()
    }
    
    // 'value' attribute on TextAreaInput (id=Note_Input) at CloseClaimInfoDV.pcf: line 19, column 38
    function valueRoot_3 () : java.lang.Object {
      return CloseClaimInfo
    }
    
    // 'value' attribute on TextAreaInput (id=Note_Input) at CloseClaimInfoDV.pcf: line 19, column 38
    function value_1 () : java.lang.String {
      return CloseClaimInfo.note
    }
    
    // 'value' attribute on RangeInput (id=Outcome_Input) at CloseClaimInfoDV.pcf: line 27, column 52
    function value_7 () : typekey.ClaimClosedOutcomeType {
      return CloseClaimInfo.outcome
    }
    
    // 'valueRange' attribute on RangeInput (id=Outcome_Input) at CloseClaimInfoDV.pcf: line 27, column 52
    function verifyValueRangeIsAllowedType_11 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Outcome_Input) at CloseClaimInfoDV.pcf: line 27, column 52
    function verifyValueRangeIsAllowedType_11 ($$arg :  typekey.ClaimClosedOutcomeType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Outcome_Input) at CloseClaimInfoDV.pcf: line 27, column 52
    function verifyValueRange_12 () : void {
      var __valueRangeArg = ClosedClaimOutcome()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_11(__valueRangeArg)
    }
    
    property get CloseClaimInfo () : CloseClaimInfo {
      return getRequireValue("CloseClaimInfo", 0) as CloseClaimInfo
    }
    
    property set CloseClaimInfo ($arg :  CloseClaimInfo) {
      setRequireValue("CloseClaimInfo", 0, $arg)
    }
    
    function ClosedClaimOutcome():typekey.ClaimClosedOutcomeType[]{
      if(CloseClaimInfo.Claim.isWorkCompClaim())
         return ClaimClosedOutcomeType.TF_WCCLAIM_OUTCOMETYPES.getTypeKeys().toTypedArray()
      else if(CloseClaimInfo.Claim?.getClaimStatus(CloseClaimInfo.Claim).equalsIgnoreCase("Reopened"))
        return typekey.ClaimClosedOutcomeType.AllTypeKeys.where(\elt -> !elt.Retired).toTypedArray()
      return ClaimClosedOutcomeType.TF_CLOSEDCLAIM_OUTCOMETYPE.getTypeKeys().toTypedArray()
    }
    
    
  }
  
  
}