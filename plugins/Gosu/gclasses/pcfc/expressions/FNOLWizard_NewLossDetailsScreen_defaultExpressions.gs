package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_NewLossDetailsScreen.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FNOLWizard_NewLossDetailsScreen_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_NewLossDetailsScreen.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FNOLWizard_NewLossDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=AddVehicleButton) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 73, column 25
    function action_24 () : void {
      FNOLVehicleIncidentPopup.push(Claim, Wizard)
    }
    
    // 'action' attribute on ToolbarButton (id=AddPedestrianButton) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 80, column 25
    function action_26 () : void {
      FNOLContactPopup.push(Claim, null, TC_PEDESTRIAN, null, Wizard)
    }
    
    // 'action' attribute on ToolbarButton (id=AddPropertyDamageButton) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 86, column 120
    function action_28 () : void {
      NewFixedPropertyIncidentPopup.push(Claim)
    }
    
    // 'action' attribute on ToolbarButton (id=AddVehicleButton) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 73, column 25
    function action_dest_25 () : pcf.api.Destination {
      return pcf.FNOLVehicleIncidentPopup.createDestination(Claim, Wizard)
    }
    
    // 'action' attribute on ToolbarButton (id=AddPedestrianButton) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 80, column 25
    function action_dest_27 () : pcf.api.Destination {
      return pcf.FNOLContactPopup.createDestination(Claim, null, TC_PEDESTRIAN, null, Wizard)
    }
    
    // 'action' attribute on ToolbarButton (id=AddPropertyDamageButton) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 86, column 120
    function action_dest_29 () : pcf.api.Destination {
      return pcf.NewFixedPropertyIncidentPopup.createDestination(Claim)
    }
    
    // 'def' attribute on ListViewInput (id=PoliceReportLV) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 311, column 31
    function def_onEnter_100 (def :  pcf.MetroReportsLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on InputSetRef (id=AddressDetailInputSetRef) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 56, column 42
    function def_onEnter_19 (def :  pcf.CCAddressInputSet) : void {
      def.onEnter(Claim.AddressOwner)
    }
    
    // 'def' attribute on PanelRef at FNOLWizard_NewLossDetailsScreen.default.pcf: line 61, column 64
    function def_onEnter_22 (def :  pcf.OptionalNoteCV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on ListViewInput (id=WitnessLV) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 289, column 31
    function def_onEnter_96 (def :  pcf.EditableWitnessesLV) : void {
      def.onEnter(Claim.getClaimContactRolesByRole(ContactRole.TC_WITNESS), Claim, ContactRole.TC_WITNESS)
    }
    
    // 'def' attribute on ListViewInput (id=Claim_Officials) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 300, column 31
    function def_onEnter_98 (def :  pcf.EditableOfficialsLV) : void {
      def.onEnter(Claim)
    }
    
    // 'def' attribute on ListViewInput (id=PoliceReportLV) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 311, column 31
    function def_refreshVariables_101 (def :  pcf.MetroReportsLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on InputSetRef (id=AddressDetailInputSetRef) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 56, column 42
    function def_refreshVariables_20 (def :  pcf.CCAddressInputSet) : void {
      def.refreshVariables(Claim.AddressOwner)
    }
    
    // 'def' attribute on PanelRef at FNOLWizard_NewLossDetailsScreen.default.pcf: line 61, column 64
    function def_refreshVariables_23 (def :  pcf.OptionalNoteCV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'def' attribute on ListViewInput (id=WitnessLV) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 289, column 31
    function def_refreshVariables_97 (def :  pcf.EditableWitnessesLV) : void {
      def.refreshVariables(Claim.getClaimContactRolesByRole(ContactRole.TC_WITNESS), Claim, ContactRole.TC_WITNESS)
    }
    
    // 'def' attribute on ListViewInput (id=Claim_Officials) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 300, column 31
    function def_refreshVariables_99 (def :  pcf.EditableOfficialsLV) : void {
      def.refreshVariables(Claim)
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_Fault_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 334, column 46
    function defaultSetter_103 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.FaultRating = (__VALUE_TO_SET as typekey.FaultRating)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_Weather_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 340, column 46
    function defaultSetter_107 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Weather = (__VALUE_TO_SET as typekey.WeatherType)
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 348, column 45
    function defaultSetter_111 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Catastrophe = (__VALUE_TO_SET as entity.Catastrophe)
    }
    
    // 'value' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 356, column 52
    function defaultSetter_118 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.PermissionRequired = (__VALUE_TO_SET as typekey.ClaimSecurityType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossCause_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 46, column 42
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.LossCause = (__VALUE_TO_SET as typekey.LossCause)
    }
    
    // 'value' attribute on CheckBoxInput (id=IncidentOnly_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 51, column 41
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.IncidentReport = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 32, column 38
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard_NewLossDetailsScreen.default.pcf: line 17, column 23
    function initialValue_0 () : boolean {
      return Claim.Officials.length > 0
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard_NewLossDetailsScreen.default.pcf: line 21, column 23
    function initialValue_1 () : boolean {
      return Claim.MetroReports.length > 0
    }
    
    // 'sortBy' attribute on IteratorSort at FNOLWizard_NewLossDetailsScreen.default.pcf: line 98, column 32
    function sortBy_30 (VehicleIncident :  entity.VehicleIncident) : java.lang.Object {
      return VehicleIncident.LossParty
    }
    
    // 'validationExpression' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 39, column 35
    function validationExpression_6 () : java.lang.Object {
      return Claim.LossDate != null || Claim.LossDate < gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get("Java.Validation.Date.ForbidFuture")
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 348, column 45
    function valueRange_113 () : java.lang.Object {
      return gw.api.admin.CatastropheUtil.getCatastrophes()
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 356, column 52
    function valueRange_120 () : java.lang.Object {
      return gw.api.claim.ClaimUtil.getAvailableTypes()
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 32, column 38
    function valueRoot_4 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on TypeKeyInput (id=Notification_Fault_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 334, column 46
    function value_102 () : typekey.FaultRating {
      return Claim.FaultRating
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_Weather_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 340, column 46
    function value_106 () : typekey.WeatherType {
      return Claim.Weather
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_LossCause_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 46, column 42
    function value_11 () : typekey.LossCause {
      return Claim.LossCause
    }
    
    // 'value' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 348, column 45
    function value_110 () : entity.Catastrophe {
      return Claim.Catastrophe
    }
    
    // 'value' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 356, column 52
    function value_117 () : typekey.ClaimSecurityType {
      return Claim.PermissionRequired
    }
    
    // 'value' attribute on CheckBoxInput (id=IncidentOnly_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 51, column 41
    function value_15 () : java.lang.Boolean {
      return Claim.IncidentReport
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 32, column 38
    function value_2 () : java.lang.String {
      return Claim.Description
    }
    
    // 'value' attribute on PanelIterator (id=VehicleIncidentIterator) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 95, column 52
    function value_66 () : entity.VehicleIncident[] {
      return Claim.VehicleIncidentsOnly
    }
    
    // 'value' attribute on DateInput (id=Claim_LossDate_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 39, column 35
    function value_7 () : java.util.Date {
      return Claim.LossDate
    }
    
    // 'value' attribute on PanelIterator (id=PedestrianIterator) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 180, column 49
    function value_80 () : entity.ClaimContact[] {
      return Claim.getClaimContactsByRole( ContactRole.TC_PEDESTRIAN )
    }
    
    // 'value' attribute on PanelIterator (id=PropertyIncidentIterator) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 230, column 58
    function value_95 () : entity.FixedPropertyIncident[] {
      return Claim.FixedPropertyIncidentsOnly
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 348, column 45
    function verifyValueRangeIsAllowedType_114 ($$arg :  entity.Catastrophe[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 348, column 45
    function verifyValueRangeIsAllowedType_114 ($$arg :  gw.api.database.IQueryBeanResult<entity.Catastrophe>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 348, column 45
    function verifyValueRangeIsAllowedType_114 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 356, column 52
    function verifyValueRangeIsAllowedType_121 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 356, column 52
    function verifyValueRangeIsAllowedType_121 ($$arg :  typekey.ClaimSecurityType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Catastrophe_CatastropheNumber_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 348, column 45
    function verifyValueRange_115 () : void {
      var __valueRangeArg = gw.api.admin.CatastropheUtil.getCatastrophes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_114(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_PermissionRequired_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 356, column 52
    function verifyValueRange_122 () : void {
      var __valueRangeArg = gw.api.claim.ClaimUtil.getAvailableTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_121(__valueRangeArg)
    }
    
    // 'visible' attribute on PanelRef at FNOLWizard_NewLossDetailsScreen.default.pcf: line 61, column 64
    function visible_21 () : java.lang.Boolean {
      return Wizard.displayNotesSection_tdic(Claim)
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get Officials () : boolean {
      return getVariableValue("Officials", 0) as java.lang.Boolean
    }
    
    property set Officials ($arg :  boolean) {
      setVariableValue("Officials", 0, $arg)
    }
    
    property get PoliceReport () : boolean {
      return getVariableValue("PoliceReport", 0) as java.lang.Boolean
    }
    
    property set PoliceReport ($arg :  boolean) {
      setVariableValue("PoliceReport", 0, $arg)
    }
    
    property get Wizard () : gw.api.claim.NewClaimWizardInfo {
      return getRequireValue("Wizard", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set Wizard ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setRequireValue("Wizard", 0, $arg)
    }
    
    function removeVehicleIncident(vehicleIncidentParam : VehicleIncident) {
          for (report in Claim.MetroReports) {
            if (report.VehicleIncident == vehicleIncidentParam) {
              throw new gw.api.util.DisplayableException(DisplayKey.get("Web.NewLossDetailsScreen.CannotDeleteVehicleIncidentExceptionLabel"))
            }
          }
    
          removeInjIncidentFor(vehicleIncidentParam.driver)
          for (person in vehicleIncidentParam.passenger) {
            removeInjIncidentFor(person)
          }
    
          Claim.removeFromIncidents(vehicleIncidentParam)
          // Remove the Services Request attached to vehicleIncidentParam
          gw.api.claim.FnolServiceRequestHelper.removeIncidentServiceRequests(vehicleIncidentParam, Wizard)
        }
    
        function isNewlyCreatedVehicleIncident(vi : VehicleIncident) : boolean {
          return vi != null and vi.Vehicle.ShortDisplayName == DisplayKey.get("Java.DisplayName.NewlyCreated")
        }
    
        function removeInjIncidentFor(person : Person) {
          var injIncident = Wizard.getInjuryIncident(Claim.getClaimContact(person))
          if (injIncident != null) {
            injIncident.injured = null
            Claim.removeFromIncidents(injIncident)
          }
        }
    
        function hasExposures(pedestrian : entity.ClaimContact) : boolean {
          return Wizard.getInjuryIncident(pedestrian)?.Exposures.HasElements
        }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_NewLossDetailsScreen.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry2ExpressionsImpl extends FNOLWizard_NewLossDetailsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=EditPedestrian) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 206, column 119
    function action_68 () : void {
      FNOLContactPopup.push(Claim, Pedestrian, TC_PEDESTRIAN, null, Wizard)
    }
    
    // 'action' attribute on MenuItem (id=RemovePedestrian) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 212, column 63
    function action_71 () : void {
      Claim.removeRole( ContactRole.TC_PEDESTRIAN, Pedestrian.Contact )
    }
    
    // 'action' attribute on TextInput (id=PersonName_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 202, column 194
    function action_72 () : void {
      FNOLContactPopup.push(Claim, Pedestrian, TC_PEDESTRIAN, null, Wizard)
    }
    
    // 'action' attribute on MenuItem (id=EditPedestrian) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 206, column 119
    function action_dest_69 () : pcf.api.Destination {
      return pcf.FNOLContactPopup.createDestination(Claim, Pedestrian, TC_PEDESTRIAN, null, Wizard)
    }
    
    // 'action' attribute on TextInput (id=PersonName_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 202, column 194
    function action_dest_73 () : pcf.api.Destination {
      return pcf.FNOLContactPopup.createDestination(Claim, Pedestrian, TC_PEDESTRIAN, null, Wizard)
    }
    
    // 'icon' attribute on Label (id=PedestrianIcon) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 192, column 42
    function icon_67 () : java.lang.String {
      return gw.api.claim.IncidentIconSet.PANEL_PEDESTRIAN_ICON
    }
    
    // 'value' attribute on TextInput (id=PersonName_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 202, column 194
    function value_74 () : java.lang.String {
      return Pedestrian.DisplayName == "" ? DisplayKey.get("Web.NewLossDetailsScreen.PedestrianIterator.Pedestrian.Value") : Pedestrian.DisplayName.elide(25)
    }
    
    // 'value' attribute on TextAreaInput (id=InjuryIncidentDescription_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 218, column 83
    function value_77 () : java.lang.String {
      return Wizard.getInjuryDescription(Pedestrian)
    }
    
    // 'visible' attribute on MenuItem (id=RemovePedestrian) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 212, column 63
    function visible_70 () : java.lang.Boolean {
      return not hasExposures(Pedestrian)
    }
    
    // 'visible' attribute on TextAreaInput (id=InjuryIncidentDescription_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 218, column 83
    function visible_76 () : java.lang.Boolean {
      return Wizard.getInjuryDescription(Pedestrian).HasContent
    }
    
    property get Pedestrian () : entity.ClaimContact {
      return getIteratedValue(1) as entity.ClaimContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_NewLossDetailsScreen.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntry3ExpressionsImpl extends FNOLWizard_NewLossDetailsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=EditProperty) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 256, column 139
    function action_82 () : void {
      EditFixedPropertyIncidentPopup.push(PropertyIncident, true)
    }
    
    // 'action' attribute on MenuItem (id=RemoveProperty) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 262, column 69
    function action_85 () : void {
      Claim.removeFromIncidents( PropertyIncident ); gw.api.claim.FnolServiceRequestHelper.removeIncidentServiceRequests(PropertyIncident, Wizard)
    }
    
    // 'action' attribute on TextInput (id=PropertyName_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 252, column 210
    function action_86 () : void {
      EditFixedPropertyIncidentPopup.push(PropertyIncident, true)
    }
    
    // 'action' attribute on MenuItem (id=EditProperty) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 256, column 139
    function action_dest_83 () : pcf.api.Destination {
      return pcf.EditFixedPropertyIncidentPopup.createDestination(PropertyIncident, true)
    }
    
    // 'action' attribute on TextInput (id=PropertyName_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 252, column 210
    function action_dest_87 () : pcf.api.Destination {
      return pcf.EditFixedPropertyIncidentPopup.createDestination(PropertyIncident, true)
    }
    
    // 'icon' attribute on Label (id=PropertyIcon) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 242, column 40
    function icon_81 () : java.lang.String {
      return PropertyIncident.PanelIcon
    }
    
    // 'value' attribute on TextAreaInput (id=PropertyIncidentDescription_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 268, column 72
    function valueRoot_92 () : java.lang.Object {
      return PropertyIncident
    }
    
    // 'value' attribute on TextInput (id=PropertyName_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 252, column 210
    function value_88 () : java.lang.String {
      return PropertyIncident.DisplayName == "" ? DisplayKey.get("Web.NewLossDetailsScreen.PropertyIncidentIterator.Property.Value") : PropertyIncident.DisplayName.elide(20)
    }
    
    // 'value' attribute on TextAreaInput (id=PropertyIncidentDescription_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 268, column 72
    function value_91 () : java.lang.String {
      return PropertyIncident.Description
    }
    
    // 'visible' attribute on MenuItem (id=RemoveProperty) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 262, column 69
    function visible_84 () : java.lang.Boolean {
      return PropertyIncident.Exposures.IsEmpty
    }
    
    // 'visible' attribute on TextAreaInput (id=PropertyIncidentDescription_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 268, column 72
    function visible_90 () : java.lang.Boolean {
      return PropertyIncident.Description.HasContent
    }
    
    property get PropertyIncident () : entity.FixedPropertyIncident {
      return getIteratedValue(1) as entity.FixedPropertyIncident
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_NewLossDetailsScreen.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntryExpressionsImpl extends FNOLWizard_NewLossDetailsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=EditVehicleMenu) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 132, column 135
    function action_34 () : void {
      FNOLVehicleIncidentPopup.push(VehicleIncident, Wizard)
    }
    
    // 'action' attribute on MenuItem (id=RemoveVehicleMenu) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 138, column 68
    function action_37 () : void {
      removeVehicleIncident(VehicleIncident)
    }
    
    // 'action' attribute on TextInput (id=VehicleName_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 128, column 231
    function action_38 () : void {
      FNOLVehicleIncidentPopup.push(VehicleIncident, Wizard);
    }
    
    // 'action' attribute on MenuItem (id=EditVehicleMenu) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 132, column 135
    function action_dest_35 () : pcf.api.Destination {
      return pcf.FNOLVehicleIncidentPopup.createDestination(VehicleIncident, Wizard)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsScreen.default.pcf: line 166, column 55
    function def_onEnter_63 (def :  pcf.VehicleIncidentOccupantsLV) : void {
      def.onEnter(vehicleOccupants, VehicleIncident, Wizard)
    }
    
    // 'def' attribute on ListViewInput at FNOLWizard_NewLossDetailsScreen.default.pcf: line 166, column 55
    function def_refreshVariables_64 (def :  pcf.VehicleIncidentOccupantsLV) : void {
      def.refreshVariables(vehicleOccupants, VehicleIncident, Wizard)
    }
    
    // 'filter' attribute on TypeKeyInput (id=VehicleState_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 146, column 70
    function filter_44 (VALUE :  typekey.Jurisdiction, VALUES :  typekey.Jurisdiction[]) : java.lang.Boolean {
      return VALUE.hasCategory(JurisdictionType.TC_VEHICLE_REG)
    }
    
    // 'icon' attribute on Label (id=VehicleIcon) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 118, column 39
    function icon_33 () : java.lang.String {
      return VehicleIncident.PanelIcon
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard_NewLossDetailsScreen.default.pcf: line 102, column 62
    function initialValue_31 () : gw.api.claim.VehicleIncidentUIHelper {
      return new gw.api.claim.VehicleIncidentUIHelper(VehicleIncident)
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard_NewLossDetailsScreen.default.pcf: line 106, column 49
    function initialValue_32 () : ArrayList<Person> {
      return vehicleIncidentHelper.VehicleOccupants
    }
    
    // PanelIterator (id=VehicleIncidentIterator) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 95, column 52
    function initializeVariables_65 () : void {
        vehicleIncidentHelper = new gw.api.claim.VehicleIncidentUIHelper(VehicleIncident);
  vehicleOccupants = vehicleIncidentHelper.VehicleOccupants;

    }
    
    // 'value' attribute on TypeKeyInput (id=VehicleState_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 146, column 70
    function valueRoot_43 () : java.lang.Object {
      return VehicleIncident.Vehicle
    }
    
    // 'value' attribute on TextAreaInput (id=VehicleIncidentDescription_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 161, column 71
    function valueRoot_59 () : java.lang.Object {
      return VehicleIncident
    }
    
    // 'value' attribute on TextInput (id=VehicleName_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 128, column 231
    function value_39 () : java.lang.String {
      return isNewlyCreatedVehicleIncident( VehicleIncident ) ? DisplayKey.get("Web.NewLossDetailsScreen.VehicleIncidentIterator.UnknownVehicle.Value") : VehicleIncident.Vehicle.ShortDisplayName.elide(25)
    }
    
    // 'value' attribute on TypeKeyInput (id=VehicleState_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 146, column 70
    function value_42 () : typekey.Jurisdiction {
      return VehicleIncident.Vehicle.State
    }
    
    // 'value' attribute on TextInput (id=VehiclePlateNumber_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 151, column 83
    function value_48 () : java.lang.String {
      return VehicleIncident.Vehicle.LicensePlate
    }
    
    // 'value' attribute on TextInput (id=VehicleVIN_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 156, column 127
    function value_53 () : java.lang.String {
      return VehicleIncident.Vehicle.Vin
    }
    
    // 'value' attribute on TextAreaInput (id=VehicleIncidentDescription_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 161, column 71
    function value_58 () : java.lang.String {
      return VehicleIncident.Description
    }
    
    // 'visible' attribute on MenuItem (id=RemoveVehicleMenu) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 138, column 68
    function visible_36 () : java.lang.Boolean {
      return VehicleIncident.Exposures.IsEmpty
    }
    
    // 'visible' attribute on TypeKeyInput (id=VehicleState_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 146, column 70
    function visible_41 () : java.lang.Boolean {
      return VehicleIncident.Vehicle.State != null
    }
    
    // 'visible' attribute on TextInput (id=VehiclePlateNumber_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 151, column 83
    function visible_47 () : java.lang.Boolean {
      return VehicleIncident.Vehicle.LicensePlate.length > 0
    }
    
    // 'visible' attribute on TextInput (id=VehicleVIN_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 156, column 127
    function visible_52 () : java.lang.Boolean {
      return VehicleIncident.Vehicle.LicensePlate.length == 0 and VehicleIncident.Vehicle.Vin.length > 0
    }
    
    // 'visible' attribute on TextAreaInput (id=VehicleIncidentDescription_Input) at FNOLWizard_NewLossDetailsScreen.default.pcf: line 161, column 71
    function visible_57 () : java.lang.Boolean {
      return VehicleIncident.Description.HasContent
    }
    
    // 'visible' attribute on ListViewInput at FNOLWizard_NewLossDetailsScreen.default.pcf: line 166, column 55
    function visible_62 () : java.lang.Boolean {
      return !vehicleOccupants.Empty
    }
    
    property get VehicleIncident () : entity.VehicleIncident {
      return getIteratedValue(1) as entity.VehicleIncident
    }
    
    property get vehicleIncidentHelper () : gw.api.claim.VehicleIncidentUIHelper {
      return getVariableValue("vehicleIncidentHelper", 1) as gw.api.claim.VehicleIncidentUIHelper
    }
    
    property set vehicleIncidentHelper ($arg :  gw.api.claim.VehicleIncidentUIHelper) {
      setVariableValue("vehicleIncidentHelper", 1, $arg)
    }
    
    property get vehicleOccupants () : ArrayList<Person> {
      return getVariableValue("vehicleOccupants", 1) as ArrayList<Person>
    }
    
    property set vehicleOccupants ($arg :  ArrayList<Person>) {
      setVariableValue("vehicleOccupants", 1, $arg)
    }
    
    
  }
  
  
}