package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderRequestNewContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ShareBaseFolderRequestNewContactPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/ShareBaseFolderRequestNewContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ShareBaseFolderRequestNewContactPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (claim :  Claim, contactSubtype :  typekey.Contact, Request :  InboundRequest_Ext) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=ShareBaseFolderRequestNewContactPopup) at ShareBaseFolderRequestNewContactPopup.pcf: line 10, column 95
    function beforeCommit_8 (pickedValue :  java.lang.Object) : void {
      Request.addRecipient(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseFolderRequestNewContactPopup.pcf: line 28, column 31
    function def_onEnter_1 (def :  pcf.ShareBaseNewContactDetailScreen_Company) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseFolderRequestNewContactPopup.pcf: line 28, column 31
    function def_onEnter_3 (def :  pcf.ShareBaseNewContactDetailScreen_Person) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseFolderRequestNewContactPopup.pcf: line 28, column 31
    function def_onEnter_5 (def :  pcf.ShareBaseNewContactDetailScreen_Place) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseFolderRequestNewContactPopup.pcf: line 28, column 31
    function def_refreshVariables_2 (def :  pcf.ShareBaseNewContactDetailScreen_Company) : void {
      def.refreshVariables(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseFolderRequestNewContactPopup.pcf: line 28, column 31
    function def_refreshVariables_4 (def :  pcf.ShareBaseNewContactDetailScreen_Person) : void {
      def.refreshVariables(contact)
    }
    
    // 'def' attribute on ScreenRef at ShareBaseFolderRequestNewContactPopup.pcf: line 28, column 31
    function def_refreshVariables_6 (def :  pcf.ShareBaseNewContactDetailScreen_Place) : void {
      def.refreshVariables(contact)
    }
    
    // 'initialValue' attribute on Variable at ShareBaseFolderRequestNewContactPopup.pcf: line 25, column 30
    function initialValue_0 () : entity.Contact {
      return gw.api.contact.NewContactUtil.newContactFromSubtype(contactSubtype)
    }
    
    // 'mode' attribute on ScreenRef at ShareBaseFolderRequestNewContactPopup.pcf: line 28, column 31
    function mode_7 () : java.lang.Object {
      return contact.Subtype
    }
    
    // 'title' attribute on Popup (id=ShareBaseFolderRequestNewContactPopup) at ShareBaseFolderRequestNewContactPopup.pcf: line 10, column 95
    static function title_9 (Request :  InboundRequest_Ext, claim :  Claim, contactSubtype :  typekey.Contact) : java.lang.Object {
      return gw.api.contact.NewContactUtil.getDisplayKeyForContactSubtype(contactSubtype.Code)
    }
    
    override property get CurrentLocation () : pcf.ShareBaseFolderRequestNewContactPopup {
      return super.CurrentLocation as pcf.ShareBaseFolderRequestNewContactPopup
    }
    
    property get Request () : InboundRequest_Ext {
      return getVariableValue("Request", 0) as InboundRequest_Ext
    }
    
    property set Request ($arg :  InboundRequest_Ext) {
      setVariableValue("Request", 0, $arg)
    }
    
    property get claim () : Claim {
      return getVariableValue("claim", 0) as Claim
    }
    
    property set claim ($arg :  Claim) {
      setVariableValue("claim", 0, $arg)
    }
    
    property get contact () : entity.Contact {
      return getVariableValue("contact", 0) as entity.Contact
    }
    
    property set contact ($arg :  entity.Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get contactSubtype () : typekey.Contact {
      return getVariableValue("contactSubtype", 0) as typekey.Contact
    }
    
    property set contactSubtype ($arg :  typekey.Contact) {
      setVariableValue("contactSubtype", 0, $arg)
    }
    
    
  }
  
  
}