package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_BasicInfoScreen.WC7.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FNOLWizard_BasicInfoScreen_WC7Expressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/FNOLWizard_BasicInfoScreen.WC7.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FNOLWizard_BasicInfoScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_10 () : void {
      AddressBookPickerPopup.push(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_100 () : void {
      if (Claim.maincontact != null) { ClaimContactDetailPopup.push(Claim.maincontact, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_102 () : void {
      ClaimContactDetailPopup.push(Claim.maincontact, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_12 () : void {
      if (Claim.reporter != null) { ClaimContactDetailPopup.push(Claim.reporter, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_14 () : void {
      ClaimContactDetailPopup.push(Claim.reporter, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_143 () : void {
      AddressBookPickerPopup.push(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_145 () : void {
      if (Claim.reporter != null) { ClaimContactDetailPopup.push(Claim.reporter, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_147 () : void {
      ClaimContactDetailPopup.push(Claim.reporter, Claim)
    }
    
    // 'action' attribute on ButtonInput (id=EditContact_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 299, column 76
    function action_212 () : void {
      ClaimContactDetailPopup.push(Claim.reporter, Claim, true)
    }
    
    // 'action' attribute on TextInput (id=Insured_Name_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 308, column 49
    function action_217 () : void {
      ClaimContactDetailPopup.push(Claim.Insured, Claim, true)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_247 () : void {
      AddressBookPickerPopup.push(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_249 () : void {
      if (Claim.maincontact != null) { ClaimContactDetailPopup.push(Claim.maincontact, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_251 () : void {
      ClaimContactDetailPopup.push(Claim.maincontact, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_270 () : void {
      AddressBookPickerPopup.push(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_272 () : void {
      if (Claim.affectedparty != null) { ClaimContactDetailPopup.push(Claim.affectedparty, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_274 () : void {
      ClaimContactDetailPopup.push(Claim.affectedparty, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_308 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.claimant), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 18, column 285
    function action_310 () : void {
      if (Claim.claimant != null) { ClaimContactDetailPopup.push(Claim.claimant, Claim, false, !CurrentLocation.InEditMode) } else { NullClaimContactDetailPopup.push() }
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_312 () : void {
      ClaimContactDetailPopup.push(Claim.claimant, Claim)
    }
    
    // 'action' attribute on ButtonInput (id=Edit_Contact_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 128, column 49
    function action_75 () : void {
      ClaimContactDetailPopup.push(Claim.reporter, Claim, true)
    }
    
    // 'action' attribute on TextInput (id=InsuredName_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 137, column 43
    function action_78 () : void {
      ClaimContactDetailPopup.push(Claim.Insured, Claim, true)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_98 () : void {
      AddressBookPickerPopup.push(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_103 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.maincontact, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_11 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_144 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_148 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.reporter, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_15 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.reporter, Claim)
    }
    
    // 'action' attribute on ButtonInput (id=EditContact_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 299, column 76
    function action_dest_213 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.reporter, Claim, true)
    }
    
    // 'action' attribute on TextInput (id=Insured_Name_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 308, column 49
    function action_dest_218 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.Insured, Claim, true)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_248 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_252 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.maincontact, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_271 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(entity.Person.Type, Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_275 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.affectedparty, Claim)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_309 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.claimant), Claim, null as List<SpecialistService>)
    }
    
    // 'action' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function action_dest_313 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.claimant, Claim)
    }
    
    // 'action' attribute on ButtonInput (id=Edit_Contact_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 128, column 49
    function action_dest_76 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.reporter, Claim, true)
    }
    
    // 'action' attribute on TextInput (id=InsuredName_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 137, column 43
    function action_dest_79 () : pcf.api.Destination {
      return pcf.ClaimContactDetailPopup.createDestination(Claim.Insured, Claim, true)
    }
    
    // 'action' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 16, column 225
    function action_dest_99 () : pcf.api.Destination {
      return pcf.AddressBookPickerPopup.createDestination(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)
    }
    
    // 'def' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_140 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(Person,null,Claim,typekey.ContactRole.TC_REPORTER)
    }
    
    // 'def' attribute on InputSetRef (id=reporter_businessphone) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 270, column 76
    function def_onEnter_189 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#WorkPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.BusinessPhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reporter_homephone) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 275, column 76
    function def_onEnter_193 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#HomePhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.HomePhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reporter_mobile) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 280, column 76
    function def_onEnter_197 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, Person#CellPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.Mobile.Label"), false))
    }
    
    // 'def' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_244 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(Person,null,Claim,typekey.ContactRole.TC_MAINCONTACT)
    }
    
    // 'def' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_267 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(Person,null,Claim,typekey.ContactRole.TC_AFFECTEDPARTY_TDIC)
    }
    
    // 'def' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_305 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(Person,null,Claim,typekey.ContactRole.TC_CLAIMANT)
    }
    
    // 'def' attribute on InputSetRef (id=reporterbusinessphone) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 106, column 49
    function def_onEnter_58 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#WorkPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.BusinessPhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reporterhomephone) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 111, column 49
    function def_onEnter_62 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#HomePhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.HomePhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reportermobile) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 116, column 49
    function def_onEnter_66 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, Person#CellPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.Mobile.Label"), false))
    }
    
    // 'def' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_7 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(entity.Person.Type, null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_onEnter_95 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.onEnter(statictypeof (Claim.maincontact), null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_141 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(Person,null,Claim,typekey.ContactRole.TC_REPORTER)
    }
    
    // 'def' attribute on InputSetRef (id=reporter_businessphone) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 270, column 76
    function def_refreshVariables_190 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#WorkPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.BusinessPhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reporter_homephone) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 275, column 76
    function def_refreshVariables_194 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#HomePhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.HomePhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reporter_mobile) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 280, column 76
    function def_refreshVariables_198 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, Person#CellPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.Mobile.Label"), false))
    }
    
    // 'def' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_245 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(Person,null,Claim,typekey.ContactRole.TC_MAINCONTACT)
    }
    
    // 'def' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_268 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(Person,null,Claim,typekey.ContactRole.TC_AFFECTEDPARTY_TDIC)
    }
    
    // 'def' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_306 (def :  pcf.TDIC_ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(Person,null,Claim,typekey.ContactRole.TC_CLAIMANT)
    }
    
    // 'def' attribute on InputSetRef (id=reporterbusinessphone) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 106, column 49
    function def_refreshVariables_59 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#WorkPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.BusinessPhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reporterhomephone) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 111, column 49
    function def_refreshVariables_63 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, contact#HomePhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.HomePhone.Label"), false))
    }
    
    // 'def' attribute on InputSetRef (id=reportermobile) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 116, column 49
    function def_refreshVariables_67 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(Claim.reporter, Person#CellPhone), DisplayKey.get("Web.BasicInfoScreen.BasicInfoDetailViewPanel.Mobile.Label"), false))
    }
    
    // 'def' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_8 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(entity.Person.Type, null, Claim)
    }
    
    // 'def' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 14, column 229
    function def_refreshVariables_96 (def :  pcf.ClaimNewPersonOnlyPickerMenuItemSet) : void {
      def.refreshVariables(statictypeof (Claim.maincontact), null, Claim)
    }
    
    // 'value' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_106 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.maincontact = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimMainContactType_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 177, column 64
    function defaultSetter_119 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.MainContactType = (__VALUE_TO_SET as typekey.PersonRelationType)
    }
    
    // 'value' attribute on RangeInput (id=Claim_LocationCode_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 195, column 50
    function defaultSetter_127 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.LocationCode = (__VALUE_TO_SET as entity.PolicyLocation)
    }
    
    // 'value' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.reporter = (__VALUE_TO_SET as entity.Contact)
    }
    
    // 'value' attribute on BooleanRadioInput (id=MainContactChoice_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 335, column 45
    function defaultSetter_241 (__VALUE_TO_SET :  java.lang.Object) : void {
      maincontactSamePerson = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_278 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.affectedparty = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimReportedByType_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 68, column 54
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ReportedByType = (__VALUE_TO_SET as typekey.PersonRelationType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_AffectedPartyType_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 378, column 123
    function defaultSetter_298 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.AffectedPartType_TDIC = (__VALUE_TO_SET as typekey.PersonRelationType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_HowReported_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 47, column 52
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.HowReported = (__VALUE_TO_SET as typekey.HowReportedType)
    }
    
    // 'value' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function defaultSetter_316 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.claimant = (__VALUE_TO_SET as entity.Person)
    }
    
    // 'value' attribute on DateInput (id=NotificationReportedDate_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 78, column 43
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      Claim.ReportedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=reporterprimarytype_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 101, column 49
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      (Claim.reporter as Person).PrimaryPhone = (__VALUE_TO_SET as typekey.PrimaryPhoneType)
    }
    
    // 'value' attribute on TextInput (id=reporteremail_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 122, column 49
    function defaultSetter_70 (__VALUE_TO_SET :  java.lang.Object) : void {
      (Claim.reporter as Person).EmailAddress1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ClaimMainContactChoice_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 156, column 60
    function defaultSetter_91 (__VALUE_TO_SET :  java.lang.Object) : void {
      basicInfoUtils.MainContactSamePerson = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=Claimant_TaxNumber_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 410, column 48
    function encryptionExpression_330 (VALUE :  java.lang.String) : java.lang.String {
      return Claim.claimant.maskTaxId(VALUE)
    }
    
    // 'filter' attribute on TypeKeyInput (id=ClaimReportedByType_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 68, column 54
    function filter_30 (VALUE :  typekey.PersonRelationType, VALUES :  typekey.PersonRelationType[]) : java.lang.Boolean {
      return VALUE.hasCategory(claimPolicyType)
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard_BasicInfoScreen.WC7.pcf: line 20, column 23
    function initialValue_0 () : boolean {
      return ((Claim.maincontact == Claim.reporter and Claim.MainContactType == Claim.ReportedByType) || (Claim.maincontact == null and Claim.MainContactType == null))
    }
    
    // 'initialValue' attribute on Variable at FNOLWizard_BasicInfoScreen.WC7.pcf: line 27, column 26
    function initialValue_1 () : PolicyType {
      return Claim.Policy.PolicyType
    }
    
    // 'label' attribute on Label at FNOLWizard_BasicInfoScreen.WC7.pcf: line 327, column 212
    function label_238 () : java.lang.String {
      return (!Claim.Policy.Verified)?DisplayKey.get("TDIC.Web.BasicInfoScreen.BasicInfoDetailViewPanel.AffectedParty"):DisplayKey.get("NVV.Claim.NewClaimPeople.Claim.MainContact")
    }
    
    // 'onChange' attribute on ClaimContactInput (id=ReportedByName_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 59, column 69
    function onChange_13 () : void {
      basicInfoUtils.setReportedByToInsured()
    }
    
    // 'onChange' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 218, column 189
    function onChange_146 () : void {
      if (Claim.reporter == Claim.Insured) Claim.ReportedByType = PersonRelationType.TC_SELF else Claim.ReportedByType = null; basicInfoUtils.setAffectedParty_TDIC()
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizard_BasicInfoScreen.WC7.pcf: line 240, column 89
    function onChange_159 () : void {
      basicInfoUtils.fillMainContactFromReportedByForSamePerson()
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizard_BasicInfoScreen.WC7.pcf: line 337, column 69
    function onChange_239 () : void {
      basicInfoUtils.setAffectedParty_TDIC() 
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizard_BasicInfoScreen.WC7.pcf: line 70, column 89
    function onChange_26 () : void {
      basicInfoUtils.fillMainContactFromReportedByForSamePerson()
    }
    
    // 'onChange' attribute on PostOnChange at FNOLWizard_BasicInfoScreen.WC7.pcf: line 158, column 102
    function onChange_89 () : void {
      basicInfoUtils.fillMainContactFromReportedByForSamePersonOrResetToNull()
    }
    
    // 'onPick' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_104 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.maincontact); var result = eval("Claim.maincontact = Claim.resolveContact(Claim.maincontact) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_16 (PickedValue :  Contact) : void {
      var contactType = entity.Person.Type; var result = eval("Claim.reporter = Claim.resolveContact(Claim.reporter) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_253 (PickedValue :  Contact) : void {
      var contactType = entity.Person.Type; var result = eval("Claim.maincontact = Claim.resolveContact(Claim.maincontact) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_276 (PickedValue :  Contact) : void {
      var contactType = entity.Person.Type; var result = eval("Claim.affectedparty = Claim.resolveContact(Claim.affectedparty) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'onPick' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function onPick_314 (PickedValue :  Contact) : void {
      var contactType = statictypeof (Claim.claimant); var result = eval("Claim.claimant = Claim.resolveContact(Claim.claimant) as " + contactType.Name + ";return null;"); ;
    }
    
    // 'validationExpression' attribute on DateInput (id=NotificationReportedDate_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 78, column 43
    function validationExpression_32 () : java.lang.Object {
      return Claim.ReportedDate != null and Claim.ReportedDate > gw.api.util.DateUtil.currentDate() ? DisplayKey.get("Java.Validation.Date.ForbidFuture") : null
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_LocationCode_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 195, column 50
    function valueRange_129 () : java.lang.Object {
      return Claim.PolicyProperties
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_20 () : java.lang.Object {
      return Claim.RelatedPersonArray
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function valueRange_318 () : java.lang.Object {
      return new Person[]{}
    }
    
    // 'value' attribute on PrivacyInput (id=Claimant_TaxNumber_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 410, column 48
    function valueRoot_329 () : java.lang.Object {
      return Claim.claimant
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_HowReported_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 47, column 52
    function valueRoot_4 () : java.lang.Object {
      return Claim
    }
    
    // 'value' attribute on DateInput (id=Claim_DOB_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 86, column 49
    function valueRoot_41 () : java.lang.Object {
      return (Claim.reporter as Person)
    }
    
    // 'value' attribute on TextInput (id=InsuredAddress_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 141, column 65
    function valueRoot_84 () : java.lang.Object {
      return Claim.Insured
    }
    
    // 'value' attribute on BooleanRadioInput (id=ClaimMainContactChoice_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 156, column 60
    function valueRoot_92 () : java.lang.Object {
      return basicInfoUtils
    }
    
    // 'value' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_105 () : entity.Person {
      return Claim.maincontact
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimMainContactType_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 177, column 64
    function value_118 () : typekey.PersonRelationType {
      return Claim.MainContactType
    }
    
    // 'value' attribute on RangeInput (id=Claim_LocationCode_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 195, column 50
    function value_126 () : entity.PolicyLocation {
      return Claim.LocationCode
    }
    
    // 'value' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_17 () : entity.Contact {
      return Claim.reporter
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_HowReported_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 47, column 52
    function value_2 () : typekey.HowReportedType {
      return Claim.HowReported
    }
    
    // 'value' attribute on TextInput (id=Claim_Occupation_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 324, column 48
    function value_234 () : java.lang.String {
      return (Claim.reporter as Person).Occupation
    }
    
    // 'value' attribute on BooleanRadioInput (id=MainContactChoice_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 335, column 45
    function value_240 () : java.lang.Boolean {
      return maincontactSamePerson
    }
    
    // 'value' attribute on TypeKeyInput (id=ClaimReportedByType_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 68, column 54
    function value_27 () : typekey.PersonRelationType {
      return Claim.ReportedByType
    }
    
    // 'value' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_277 () : entity.Person {
      return Claim.affectedparty
    }
    
    // 'value' attribute on TypeKeyInput (id=Claim_AffectedPartyType_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 378, column 123
    function value_297 () : typekey.PersonRelationType {
      return Claim.AffectedPartType_TDIC
    }
    
    // 'value' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function value_315 () : entity.Person {
      return Claim.claimant
    }
    
    // 'value' attribute on PrivacyInput (id=Claimant_TaxNumber_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 410, column 48
    function value_328 () : java.lang.String {
      return Claim.claimant.TaxID
    }
    
    // 'value' attribute on DateInput (id=NotificationReportedDate_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 78, column 43
    function value_33 () : java.util.Date {
      return Claim.ReportedDate
    }
    
    // 'value' attribute on DateInput (id=Claim_DOB_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 86, column 49
    function value_40 () : java.util.Date {
      return (Claim.reporter as Person).DateOfBirth
    }
    
    // 'value' attribute on TextInput (id=ReporterAddress_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 94, column 49
    function value_46 () : java.lang.String {
      return (Claim.reporter as Person).PrimaryAddressDisplayValue
    }
    
    // 'value' attribute on TypeKeyInput (id=reporterprimarytype_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 101, column 49
    function value_51 () : typekey.PrimaryPhoneType {
      return (Claim.reporter as Person).PrimaryPhone
    }
    
    // 'value' attribute on TextInput (id=reporteremail_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 122, column 49
    function value_69 () : java.lang.String {
      return (Claim.reporter as Person).EmailAddress1
    }
    
    // 'value' attribute on TextInput (id=InsuredName_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 137, column 43
    function value_80 () : entity.Contact {
      return Claim.Insured
    }
    
    // 'value' attribute on TextInput (id=InsuredAddress_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 141, column 65
    function value_83 () : java.lang.String {
      return Claim.Insured.PrimaryAddressDisplayValue
    }
    
    // 'value' attribute on TextInput (id=InsuredWorkPhone_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 145, column 56
    function value_86 () : java.lang.String {
      return Claim.Insured.PrimaryPhoneValue
    }
    
    // 'value' attribute on BooleanRadioInput (id=ClaimMainContactChoice_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 156, column 60
    function value_90 () : java.lang.Boolean {
      return basicInfoUtils.MainContactSamePerson
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_109 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_109 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_109 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_LocationCode_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 195, column 50
    function verifyValueRangeIsAllowedType_130 ($$arg :  entity.PolicyLocation[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_LocationCode_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 195, column 50
    function verifyValueRangeIsAllowedType_130 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyLocation>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_LocationCode_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 195, column 50
    function verifyValueRangeIsAllowedType_130 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_154 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_154 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_154 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_21 ($$arg :  entity.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_21 ($$arg :  gw.api.database.IQueryBeanResult<entity.Contact>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_21 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_258 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_258 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_258 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_281 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_281 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_281 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_319 ($$arg :  entity.Person[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_319 ($$arg :  gw.api.database.IQueryBeanResult<entity.Person>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRangeIsAllowedType_319 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_110 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_109(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Claim_LocationCode_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 195, column 50
    function verifyValueRange_131 () : void {
      var __valueRangeArg = Claim.PolicyProperties
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_130(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_155 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_154(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_22 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_21(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_259 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_258(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_282 () : void {
      var __valueRangeArg = Claim.RelatedPersonArray
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_281(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function verifyValueRange_320 () : void {
      var __valueRangeArg = new Person[]{}
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_319(__valueRangeArg)
    }
    
    // 'valueType' attribute on ClaimContactInput (id=MainContactName_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 169, column 64
    function verifyValueType_116 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=MainContact_Name_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 350, column 75
    function verifyValueType_265 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 362, column 76
    function verifyValueType_288 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'valueType' attribute on ClaimContactInput (id=Claimant_Name_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 402, column 47
    function verifyValueType_326 () : void {
      var __valueTypeArg : entity.Person
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : entity.Contact = __valueTypeArg
    }
    
    // 'visible' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 12, column 273
    function visible_101 () : java.lang.Boolean {
      return !basicInfoUtils.MainContactSamePerson
    }
    
    // 'visible' attribute on DetailViewPanel at FNOLWizard_BasicInfoScreen.WC7.pcf: line 36, column 44
    function visible_124 () : java.lang.Boolean {
      return !Claim.Policy.Verified
    }
    
    // 'visible' attribute on ClaimContactInput (id=ReportedBy_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_142 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(entity.Person.Type, Claim, null as List<SpecialistService>)" != "" && false
    }
    
    // 'visible' attribute on Label at FNOLWizard_BasicInfoScreen.WC7.pcf: line 252, column 76
    function visible_175 () : java.lang.Boolean {
      return Claim.reporter != null and !Claim.Policy.Verified
    }
    
    // 'visible' attribute on TextInput (id=Claim_Occupation_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 324, column 48
    function visible_233 () : java.lang.Boolean {
      return Claim.Policy.Verified
    }
    
    // 'visible' attribute on ClaimContactInput (id=MainContact_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function visible_250 () : java.lang.Boolean {
      return !maincontactSamePerson and Claim.Policy.Verified
    }
    
    // 'visible' attribute on ClaimContactInput (id=AffectedParty_Name_Input) at ClaimContactWidget.xml: line 12, column 273
    function visible_273 () : java.lang.Boolean {
      return !maincontactSamePerson and !Claim.Policy.Verified
    }
    
    // 'visible' attribute on TypeKeyInput (id=Claim_MainContactType_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 370, column 121
    function visible_289 () : java.lang.Boolean {
      return !basicInfoUtils.MainContactSamePerson and !Claim.Policy.Verified and Claim.maincontact != null
    }
    
    // 'visible' attribute on TypeKeyInput (id=Claim_AffectedPartyType_Input) at FNOLWizard_BasicInfoScreen.WC7.pcf: line 378, column 123
    function visible_296 () : java.lang.Boolean {
      return !basicInfoUtils.MainContactSamePerson and !Claim.Policy.Verified and Claim.affectedparty != null
    }
    
    // 'visible' attribute on ClaimContactInput (id=Claimant_Name_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_307 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.claimant), Claim, null as List<SpecialistService>)" != "" && false
    }
    
    // 'visible' attribute on Label at FNOLWizard_BasicInfoScreen.WC7.pcf: line 81, column 49
    function visible_38 () : java.lang.Boolean {
      return Claim.reporter != null
    }
    
    // 'visible' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 14, column 229
    function visible_6 () : java.lang.Boolean {
      return perm.Contact.createlocal
    }
    
    // 'visible' attribute on ClaimContactInput (id=ReportedByName_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_9 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(entity.Person.Type, Claim, null as List<SpecialistService>)" != "" && true
    }
    
    // 'visible' attribute on ClaimContactInput (id=MainContactName_Input) at ClaimContactWidget.xml: line 16, column 225
    function visible_97 () : java.lang.Boolean {
      return "AddressBookPickerPopup.push(statictypeof (Claim.maincontact), Claim, null as List<SpecialistService>)" != "" && true
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get Wizard () : gw.api.claim.NewClaimWizardInfo {
      return getRequireValue("Wizard", 0) as gw.api.claim.NewClaimWizardInfo
    }
    
    property set Wizard ($arg :  gw.api.claim.NewClaimWizardInfo) {
      setRequireValue("Wizard", 0, $arg)
    }
    
    property get basicInfoUtils () : gw.pcf.fnol.BasicInfoScreenUtils {
      return getRequireValue("basicInfoUtils", 0) as gw.pcf.fnol.BasicInfoScreenUtils
    }
    
    property set basicInfoUtils ($arg :  gw.pcf.fnol.BasicInfoScreenUtils) {
      setRequireValue("basicInfoUtils", 0, $arg)
    }
    
    property get claimPolicyType () : PolicyType {
      return getVariableValue("claimPolicyType", 0) as PolicyType
    }
    
    property set claimPolicyType ($arg :  PolicyType) {
      setVariableValue("claimPolicyType", 0, $arg)
    }
    
    property get contact () : Contact {
      return getVariableValue("contact", 0) as Contact
    }
    
    property set contact ($arg :  Contact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get maincontactSamePerson () : boolean {
      return getVariableValue("maincontactSamePerson", 0) as java.lang.Boolean
    }
    
    property set maincontactSamePerson ($arg :  boolean) {
      setVariableValue("maincontactSamePerson", 0, $arg)
    }
    
    
  }
  
  
}