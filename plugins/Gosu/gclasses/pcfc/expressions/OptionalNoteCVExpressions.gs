package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/FNOL/OptionalNoteCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OptionalNoteCVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/FNOL/OptionalNoteCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OptionalNoteCVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at OptionalNoteCV.pcf: line 24, column 48
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      Note.Topic = (__VALUE_TO_SET as typekey.NoteTopicType)
    }
    
    // 'value' attribute on TextAreaInput (id=Body_Input) at OptionalNoteCV.pcf: line 31, column 42
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      Note.Body = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextAreaInput (id=Body_Input) at OptionalNoteCV.pcf: line 31, column 42
    function editable_5 () : java.lang.Boolean {
      return Note.isBodyEditable()
    }
    
    // 'initialValue' attribute on Variable at OptionalNoteCV.pcf: line 13, column 20
    function initialValue_0 () : Note {
      return Claim.addNote(null)
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at OptionalNoteCV.pcf: line 24, column 48
    function valueRoot_3 () : java.lang.Object {
      return Note
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at OptionalNoteCV.pcf: line 24, column 48
    function value_1 () : typekey.NoteTopicType {
      return Note.Topic
    }
    
    // 'value' attribute on TextAreaInput (id=Body_Input) at OptionalNoteCV.pcf: line 31, column 42
    function value_7 () : java.lang.String {
      return Note.Body
    }
    
    // 'visible' attribute on NoteBodyInput (id=NoteBody_Input) at OptionalNoteCV.pcf: line 36, column 46
    function visible_13 () : java.lang.Boolean {
      return not Note.BodyEditable
    }
    
    // 'visible' attribute on TextAreaInput (id=Body_Input) at OptionalNoteCV.pcf: line 31, column 42
    function visible_6 () : java.lang.Boolean {
      return Note.BodyEditable
    }
    
    property get Claim () : Claim {
      return getRequireValue("Claim", 0) as Claim
    }
    
    property set Claim ($arg :  Claim) {
      setRequireValue("Claim", 0, $arg)
    }
    
    property get Note () : Note {
      return getVariableValue("Note", 0) as Note
    }
    
    property set Note ($arg :  Note) {
      setVariableValue("Note", 0, $arg)
    }
    
    
  }
  
  
}