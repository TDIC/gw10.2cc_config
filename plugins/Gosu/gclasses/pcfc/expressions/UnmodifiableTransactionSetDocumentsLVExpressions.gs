package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/claim/financials/checks/UnmodifiableTransactionSetDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UnmodifiableTransactionSetDocumentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/claim/financials/checks/UnmodifiableTransactionSetDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends UnmodifiableTransactionSetDocumentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NameLinkWeb) at UnmodifiableTransactionSetDocumentsLV.pcf: line 50, column 145
    function action_14 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at UnmodifiableTransactionSetDocumentsLV.pcf: line 61, column 77
    function action_18 () : void {
      DocumentDetailsPopup.push(document, false)
    }
    
    // 'action' attribute on Link (id=NameLinkUnity) at UnmodifiableTransactionSetDocumentsLV.pcf: line 41, column 147
    function action_9 () : void {
      document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at UnmodifiableTransactionSetDocumentsLV.pcf: line 61, column 77
    function action_dest_19 () : pcf.api.Destination {
      return pcf.DocumentDetailsPopup.createDestination(document, false)
    }
    
    // 'available' attribute on Link (id=NameLinkWeb) at UnmodifiableTransactionSetDocumentsLV.pcf: line 50, column 145
    function available_12 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=ViewPropertiesLink) at UnmodifiableTransactionSetDocumentsLV.pcf: line 61, column 77
    function available_17 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'available' attribute on Link (id=NameLinkUnity) at UnmodifiableTransactionSetDocumentsLV.pcf: line 41, column 147
    function available_7 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document) 
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 28, column 32
    function icon_6 () : java.lang.String {
      return document.Icon
    }
    
    // 'label' attribute on Link (id=NameLinkUnity) at UnmodifiableTransactionSetDocumentsLV.pcf: line 41, column 147
    function label_10 () : java.lang.Object {
      return document.Name
    }
    
    // 'tooltip' attribute on Link (id=NameLinkUnity) at UnmodifiableTransactionSetDocumentsLV.pcf: line 41, column 147
    function tooltip_11 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(document)
    }
    
    // 'tooltip' attribute on Link (id=ViewPropertiesLink) at UnmodifiableTransactionSetDocumentsLV.pcf: line 61, column 77
    function tooltip_20 () : java.lang.String {
      return documentsActionsHelper.ViewDocumentPropertiesTooltip
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 68, column 45
    function valueRoot_22 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 68, column 45
    function value_21 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 74, column 58
    function value_24 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 79, column 51
    function value_27 () : typekey.DocumentStatusType {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 84, column 36
    function value_30 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 92, column 42
    function value_33 () : java.util.Date {
      return document.DateModified
    }
    
    // 'visible' attribute on Link (id=NameLinkWeb) at UnmodifiableTransactionSetDocumentsLV.pcf: line 50, column 145
    function visible_13 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on Link (id=NameLinkUnity) at UnmodifiableTransactionSetDocumentsLV.pcf: line 41, column 147
    function visible_8 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    property get document () : entity.Document {
      return getIteratedValue(1) as entity.Document
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/claim/financials/checks/UnmodifiableTransactionSetDocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UnmodifiableTransactionSetDocumentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at UnmodifiableTransactionSetDocumentsLV.pcf: line 14, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 68, column 45
    function sortValue_1 (document :  entity.Document) : java.lang.Object {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=SubType_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 74, column 58
    function sortValue_2 (document :  entity.Document) : java.lang.Object {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 79, column 51
    function sortValue_3 (document :  entity.Document) : java.lang.Object {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 84, column 36
    function sortValue_4 (document :  entity.Document) : java.lang.Object {
      return document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at UnmodifiableTransactionSetDocumentsLV.pcf: line 92, column 42
    function sortValue_5 (document :  entity.Document) : java.lang.Object {
      return document.DateModified
    }
    
    // 'value' attribute on RowIterator at UnmodifiableTransactionSetDocumentsLV.pcf: line 20, column 37
    function value_36 () : entity.Document[] {
      return DocumentList.where(\ d -> perm.Document.view(d))
    }
    
    property get DocumentList () : Document[] {
      return getRequireValue("DocumentList", 0) as Document[]
    }
    
    property set DocumentList ($arg :  Document[]) {
      setRequireValue("DocumentList", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    
  }
  
  
}