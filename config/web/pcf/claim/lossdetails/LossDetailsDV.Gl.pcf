<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../pcf.xsd">
  <DetailViewPanel
    id="LossDetailsDV"
    mode="Gl">
    <Require
      name="Claim"
      type="Claim"/>
    <InputColumn
      maxWidth="480px">
      <Label
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.LossDetails&quot;)"/>
      <TextAreaInput
        editable="true"
        id="ClaimDescription"
        label="DisplayKey.get(&quot;Web.NewLossDetailsScreen.LossDetailsAddressDV.WhatHappened.Label&quot;)"
        numRows="6"
        value="Claim.Description"/>
      <DateInput
        dateFormat="short"
        id="Claim_LossDate"
        label="DisplayKey.get(&quot;Web.FNOLWizard.LossDetails.GeneralLiability.LossDate&quot;)"
        timeFormat="short"
        validationExpression="Claim.LossDate == null || Claim.LossDate &lt; gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get(&quot;Java.Validation.Date.ForbidFuture&quot;)"
        value="Claim.LossDate"
        visible="!Claim.Policy.Verified"/>
      <TypeKeyInput
        id="Claim_LossType"
        label="DisplayKey.get(&quot;Web.FNOLWizard.LossDetails.GeneralLiability.LossType&quot;)"
        required="false"
        value="Claim.LossType"
        valueType="typekey.LossType"
        visible="!Claim.Policy.Verified"/>
      <BooleanRadioInput
        id="RiskManagementIncident"
        label="DisplayKey.get(&quot;Web.FNOLWizard.LossDetails.GeneralLiability.RiskManagementIncident_TDIC&quot;)"
        value="Claim.RiskMgmtIncident_TDIC"
        visible="!Claim.Policy.Verified"/>
      <CheckBoxInput
        id="Status_IncidentReport"
        label="DisplayKey.get(&quot;Web.FNOLWizard.LossDetails.GeneralLiability.Status.IncidentReport&quot;)"
        value="Claim.IncidentReport"
        visible="!Claim.Policy.Verified"/>
      <RangeInput
        editable="true"
        id="CallType"
        label="DisplayKey.get(&quot;Web.FNOLWizard.LossDetails.GeneralLiability.Status.CallType_TDIC&quot;)"
        required="true"
        value="Claim.CallType_TDIC"
        valueRange="Claim.getCallType(Claim)"
        valueType="String"
        visible="!Claim.Policy.Verified">
        <PostOnChange
          deferUpdate="false"/>
      </RangeInput>
      <TextInput
        editable="true"
        id="Other"
        label="DisplayKey.get(&quot;Web.FNOLWizard.LossDetails.GeneralLiability.Status.Other_TDIC&quot;)"
        required="Claim.CallType_TDIC == &quot;Other&quot;"
        value="Claim.Other_TDIC"
        visible="!Claim.Policy.Verified"/>
      <DateInput
        editable="true"
        id="DateOfNotice"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.NewClaimLossDetailsAuto.Claim.Notification.ReportedDate&quot;)"
        required="true"
        validationExpression="Claim.ReportedDate != null and Claim.ReportedDate &gt; gw.api.util.DateUtil.currentDate() ? DisplayKey.get(&quot;Java.Validation.Date.ForbidFuture&quot;) : null"
        value="Claim.ReportedDate"
        visible="!Claim.Policy.Verified"/>
      <TypeKeyInput
        editable="true"
        id="ClaimNotification_HowReported"
        label="DisplayKey.get(&quot;Web.FNOLWizard.LossDetails.GeneralLiability.Notification.HowReported&quot;)"
        value="Claim.HowReported"
        valueType="typekey.HowReportedType"
        visible="!Claim.Policy.Verified"/>
      <TextAreaInput
        editable="true"
        id="Description"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Description&quot;)"
        numRows="3"
        required="false"
        value="Claim.Description"
        visible="false"/>
      <TypeKeyInput
        editable="true"
        id="LossCause"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.LossCause&quot;)"
        required="true"
        value="Claim.LossCause"
        valueType="typekey.LossCause"
        visible="Claim.Policy.Verified"></TypeKeyInput>
      <TypeKeyInput
        editable="true"
        id="ResultType"
        label="DisplayKey.get(&quot;TDIC.Web.FNOLWizard.LossDetails.GeneralLiability.ResultType&quot;)"
        value="Claim.ResultType_TDIC"
        valueType="typekey.ResultType_TDIC"
        visible="Claim.Type_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY"/>
      <TypeKeyInput
        editable="true"
        id="Notification_Fault"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification.Fault&quot;)"
        value="Claim.FaultRating"
        valueType="typekey.FaultRating"
        visible="Claim.Policy.Verified">
        <PostOnChange
          onChange="Claim.createSubrogationActivity()"
          deferUpdate="false"/>
      </TypeKeyInput>
      <TextInput
        editable="true"
        formatType="percentagePoints"
        id="Notification_AtFaultPercentage"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification.AtFaultPercentage&quot;)"
        value="Claim.Fault"
        valueType="java.math.BigDecimal"
        visible=" Claim.FaultRating == TC_1 and Claim.Policy.Verified"/>
      <RangeInput
        editable="true"
        id="Catastrophe_CatastropheNumber"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Catastrophe.CatastropheNumber&quot;)"
        required="false"
        value="Claim.Catastrophe"
        valueRange="gw.api.admin.CatastropheUtil.getCatastrophes()"
        valueType="entity.Catastrophe"
        visible="Claim.Policy.Verified"/>
      <DateInput
        dateFormat="short"
        editable="true"
        id="LossDate"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.LossDate&quot;)"
        required="true"
        timeFormat="short"
        validationExpression="Claim.LossDate == null || Claim.LossDate &lt; gw.api.util.DateUtil.currentDate() ? null : DisplayKey.get(&quot;Java.Validation.Date.ForbidFuture&quot;)"
        value="Claim.LossDate"
        visible="Claim.Policy.Verified">
        <PostOnChange
          onChange="gw.pcf.ClaimLossDetailsUtil.changedLossDate(Claim)"/>
      </DateInput>
      <RangeInput
        editable="true"
        id="ClaimPermissionRequired"
        label="DisplayKey.get(&quot;Web.FNOLWizard.LossDetails.GeneralLiability.PermissionRequired&quot;)"
        required="false"
        value="Claim.PermissionRequired"
        valueRange="gw.api.claim.ClaimUtil.getAvailableTypes()"
        valueType="typekey.ClaimSecurityType"
        visible="Claim.Policy.Verified"/>
      <InputDivider/>
      <Label
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.LossLocation&quot;)"
        visible="Claim.Policy.Verified"/>
      <InputSetRef
        def="CCAddressInputSet(Claim.AddressOwner)"
        visible="Claim.Policy.Verified"/>
      <InputDivider
        visible="Claim.Policy.Verified"/>
    </InputColumn>
    <InputColumn>
      <ListViewInput
        def="EditableFixedPropertyIncidentsLV(Claim)"
        editable="true"
        id="Claim_Properties"
        label="DisplayKey.get(&quot;TDIC.Web.LossDetails.GeneralLiability.PropertyDamage&quot;)"
        labelAbove="true"
        visible="Claim.Policy.Verified and Claim.setPropertyVisibility()">
        <Toolbar>
          <IteratorButtons
            iterator="Claim_Properties.EditableFixedPropertyIncidentsLV"
            removeFlags="all Removeable"/>
        </Toolbar>
      </ListViewInput>
      <InputDivider
        visible="Claim.Policy.Verified"/>
      <ListViewInput
        def="EditableInjuryIncidentsLV(Claim)"
        editable="true"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetails.Claim.Injuries&quot;)"
        labelAbove="true"
        visible="Claim.Policy.Verified and Claim.setInjuryVisibility()">
        <Toolbar>
          <IteratorButtons
            iterator="EditableInjuryIncidentsLV.EditableInjuryIncidentsLV"
            removeFlags="all Removeable"/>
        </Toolbar>
      </ListViewInput>
      <InputDivider
        visible="Claim.Policy.Verified"/>
      <Label
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification&quot;)"
        visible="Claim.Policy.Verified"/>
      <BooleanRadioInput
        editable="true"
        id="Notification_FirstNoticeSuit"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification.FirstNoticeSuit&quot;)"
        value="Claim.FirstNoticeSuit"
        visible="Claim.Policy.Verified"/>
      <TypeKeyInput
        editable="true"
        id="Notification_HowReported"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification.HowReported&quot;)"
        value="Claim.HowReported"
        valueType="typekey.HowReportedType"
        visible="Claim.Policy.Verified"/>
      <ClaimContactInput
        claim="Claim"
        editable="true"
        id="ReportedBy_Picker"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification.ReportedBy.Picker&quot;)"
        newContactMenu="ClaimNewPersonOnlyPickerMenuItemSet"
        required="true"
        value="Claim.reporter"
        valueRange="Claim.RelatedPersonArray"
        visible="Claim.Policy.Verified"/>
      <TypeKeyInput
        editable="true"
        id="Notification_ReportedByType"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification.ReportedByType&quot;)"
        required="true"
        value="Claim.ReportedByType"
        valueType="typekey.PersonRelationType"
        visible="Claim.Policy.Verified">
        <Reflect
          triggerIds="ReportedBy_Picker">
          <ReflectCondition
            condition="VALUE==Claim.Insured"
            value="&quot;self&quot;"/>
          <ReflectCondition
            condition="true"
            value="&quot;&quot;"/>
        </Reflect>
      </TypeKeyInput>
      <ClaimContactInput
        claim="Claim"
        editable="true"
        id="MainContact_Picker"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification.MainContact.Picker&quot;)"
        newContactMenu="ClaimNewPersonOnlyPickerMenuItemSet"
        value="Claim.maincontact"
        valueRange="Claim.RelatedPersonArray"
        valueType="entity.Person"
        visible="Claim.Policy.Verified"/>
      <TypeKeyInput
        editable="true"
        id="Notification_MainContactType"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification.MainContactType&quot;)"
        value="Claim.MainContactType"
        valueType="typekey.PersonRelationType"
        visible="Claim.Policy.Verified">
        <Reflect
          triggerIds="MainContact_Picker">
          <ReflectCondition
            condition="VALUE==Claim.Insured"
            value="&quot;self&quot;"/>
          <ReflectCondition
            condition="true"
            value="&quot;&quot;"/>
        </Reflect>
      </TypeKeyInput>
      <DateInput
        dateFormat="short"
        editable="true"
        id="Notification_DateReportedToAgent"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification.DateReportedToAgent&quot;)"
        timeFormat="short"
        value="Claim.DateRptdToAgent"
        visible="false"/>
      <DateInput
        dateFormat="short"
        editable="true"
        id="Notification_DateReportedToInsured"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification.DateReportedToInsured&quot;)"
        value="Claim.DateRptdToInsured"
        visible="Claim.Policy.Verified"/>
      <DateInput
        dateFormat="short"
        editable="true"
        id="Notification_DateReportedofManifestation"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Notification.DateReportedofManifestation&quot;)"
        value="Claim.ManifestationDate"
        visible="Claim.Policy.Verified"/>
      <InputDivider
        visible="Claim.Policy.Verified"/>
      <ListViewInput
        def="EditableOfficialsLV(Claim)"
        editable="true"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.Officials&quot;)"
        labelAbove="true"
        visible="Claim.Policy.Verified">
        <Toolbar>
          <IteratorButtons
            iterator="EditableOfficialsLV.EditableOfficialsLV"/>
        </Toolbar>
      </ListViewInput>
    </InputColumn>
    <InputFooterSection>
      <ListViewInput
        boldLabel="true"
        def="EditableWitnessesLV(Claim.getClaimContactRolesByRole(ContactRole.TC_WITNESS), Claim, ContactRole.TC_WITNESS)"
        editable="true"
        id="WitnessLV"
        label="DisplayKey.get(&quot;Web.FNOLWizard.LossDetails.GeneralLiability.AnyWitnesses.Label&quot;)"
        labelAbove="true"
        visible="Claim.Policy.Verified">
        <Toolbar>
          <IteratorButtons
            iterator="WitnessLV.EditableWitnessesLV"/>
        </Toolbar>
      </ListViewInput>
      <ListViewInput
        def="EditableContributingFactorsLV(Claim)"
        editable="true"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsGeneralLiability.Claim.ContributingFactors&quot;)"
        labelAbove="true"
        visible="false">
        <Toolbar>
          <IteratorButtons
            iterator="EditableContributingFactorsLV.EditableContributingFactorsLV"/>
        </Toolbar>
      </ListViewInput>
      <ListViewInput
        def="MetroReportsLV(Claim)"
        editable="true"
        label="DisplayKey.get(&quot;NVV.Claim.SubView.LossDetailsAuto.Claim.MetroReports&quot;)"
        labelAbove="true"
        visible="false">
        <Toolbar>
          <IteratorButtons
            addLabel="DisplayKey.get(&quot;Button.Metro.New&quot;)"
            iterator="MetroReportsLV.MetroReportsLV"
            removeFlags="any RemovableMetroReport, no NotRemovableMetroReport"/>
        </Toolbar>
      </ListViewInput>
    </InputFooterSection>
  </DetailViewPanel>
</PCF>
