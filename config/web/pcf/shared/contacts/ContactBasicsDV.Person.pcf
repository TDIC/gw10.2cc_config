<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../pcf.xsd">
  <DetailViewPanel
    id="ContactBasicsDV"
    mode="Person|PersonVendor|Adjudicator|UserContact|Doctor|Attorney">
    <!-- NOTE: This shared section requires a gw.api.contact.ContactHandle, rather than a Contact, because it's
    possible for some actions on a contact detail screen to fundamentally change the contact. In particular, syncing
    a contact that leads to a downcast of that contact to a more specific subtype causes the original contact to
    become invalid. To protect against this, contacts on shared sections should be wrapped in a ContactHandle. If the
    contact is fundamentally changed, then only the handle needs to be updated, and all existing references will see
    the new contact.

    As a shortcut for "contactHandle.Contact", this shared section defines a read-only property which can be used to
    access the most current value of the contact.

    Note also that claimContact may be null, so fields which depend on reference it should only be available for setting if it is not. -->
    <Require
      name="contactHandle"
      type="gw.api.contact.ContactHandle"/>
    <Require
      name="showRoles"
      type="boolean"/>
    <Require
      name="linkStatus"
      type="gw.api.contact.ContactSystemLinkStatus"/>
    <Require
      name="claim"
      type="Claim"/>
    <InputHeaderSection>
      <InputSetRef
        def="ContactBasicsHeaderInputSet(contactHandle, ClaimContact, showRoles, linkStatus)"/>
    </InputHeaderSection>
    <InputColumn>
      <InputSetRef
        def="PersonNameInputSet(contactHandle)"/>
      <InputSetRef
        def="PersonContactInfoInputSet(contactHandle, ClaimContact)"
        visible="!(Person typeis PersonVendor)"/>
      <InputDivider/>
      <InputSetRef
        def="PrimaryAddressInputSet(contactHandle)"
        visible="isPrimaryAddressInfoVisible_TDIC()"/>
    </InputColumn>
    <InputColumn>
      <Label
        label="DisplayKey.get(&quot;Web.ContactDetail.AdditionalInfo&quot;)"
        visible="isAdditionalInfoVisible_TDIC()"/>
      <InputSetRef
        def="AdditionalInfoInputSet(contactHandle, ClaimContact, claim,isAdditionalInfoVisible_TDIC())"
        mode="Person.Subtype"/>
      <InputSetRef
        def="DriversLicenseInfoInputSet(contactHandle)"
        visible="ClaimContact.Claim.LossType == LossType.TC_WC7?(Person.Subtype==TC_PERSON and isDriversLicenseInfoVisible_TDIC()): false"/>
      <InputDivider/>
      <TextAreaInput
        boldLabel="true"
        editable="true"
        id="Notes"
        label="DisplayKey.get(&quot;Web.ContactDetail.Notes&quot;)"
        numRows="3"
        value="Person.Notes"
        visible="claim.LossType == LossType.TC_WC7"/>
    </InputColumn>
    <InputFooterSection>
    <!--  <InputDivider/>
      <ListViewInput
        boldLabel="true"
        def="ContactEFTLV(contactHandle.Contact)"
        label="DisplayKey.get(&quot;Web.ContactBasicsDV.BankData&quot;)"
        labelAbove="true">
        <Toolbar>
          <IteratorButtons
            iterator="ContactEFTLV.ContactEFTLV"/>
        </Toolbar>
      </ListViewInput> -->
    </InputFooterSection>
    <Code><![CDATA[uses typekey.Contact

    property get Person() : Person {
      return contactHandle.Contact as Person;
    }

    property get ClaimContact() : ClaimContact {
      return contactHandle typeis ClaimContact ? (contactHandle) : null;
    }

    /**
     * US538
     * 09/23/2014 robk
     */
    function isAdditionalInfoVisible_TDIC() : boolean {
      if (Person.Subtype == typekey.Contact.TC_DOCTOR or Person.Subtype == typekey.Contact.TC_ATTORNEY or Person.Subtype == typekey.Contact.TC_PERSON ) {
       if(claim.LossType == LossType.TC_WC7)
         return false
        return true
      }
      if (ClaimContact.Roles == null) {
        return false
      }
      return ClaimContact.Roles.firstWhere(\r -> r.Role == typekey.ContactRole.TC_REPORTER or r.Role == typekey.ContactRole.TC_MAINCONTACT) == null
    }

    /**
     * US538
     * 09/10/2014 robk
     */
    function isDriversLicenseInfoVisible_TDIC() : boolean {
      if (ClaimContact.Roles == null) {
        return false
      }
      return ClaimContact.Roles.firstWhere(\r -> r.Role == typekey.ContactRole.TC_REPORTER or r.Role == typekey.ContactRole.TC_MAINCONTACT or r.Role == typekey.ContactRole.TC_INJURED or r.Role == typekey.ContactRole.TC_CLAIMANT) == null
    }

    /**
     * US538
     * 09/23/2014 robk
     */
    function isPrimaryAddressInfoVisible_TDIC() : boolean {
      if(claim.LossType == LossType.TC_WC7 and Person.Subtype == typekey.Contact.TC_PERSON ){
        return false
      }
      if (Person.Subtype == typekey.Contact.TC_DOCTOR or Person.Subtype == typekey.Contact.TC_PERSON or Person.Subtype == typekey.Contact.TC_ATTORNEY or Person.Subtype == typekey.Contact.TC_PERSONVENDOR) {
        return true
      }
      if (ClaimContact.Roles == null) {
        return false
      }
      return ClaimContact.Roles.firstWhere(\r -> r.Role == typekey.ContactRole.TC_REPORTER or r.Role == typekey.ContactRole.TC_MAINCONTACT) == null
    }
    
    ]]></Code>
  </DetailViewPanel>
</PCF>