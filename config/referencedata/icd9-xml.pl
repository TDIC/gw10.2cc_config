#!/usr/bin/perl -w

# Print the header row of the delimited file
print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
print "<import usePeriodicFlushes=\"true\">\n";

# Loop index, used to generate public IDs
#my $i = 1;

# The code numbers that separate each of the body system ranges
# i.e., codes greater than 0 and less than 140 are code 1 
# (Infections and Parasitic Diseases)
my @bodySystemCodeBounds = (0, 140, 240, 280, 290, 320, 390, 460, 520, 580, 630, 680, 710, 760, 780, 800, 1000);

while (<>) {
    # Strip trailing newline
    chomp;
    # Each line should be of the form
    # CODE DESC
    # where CODE is a number, E<number>, or V<number>
    # and DESC is the remainder of the line
    if (/(\w+)\s+(.*)/) {
	my ($code, $desc) = ($1, $2);
	
	my $bodySystem = -1;
	# E codes are "external causes of injury", body system 18
	if ($code =~ /^E/) {
	    # If the code is only four characters long, it doesn't need
	    # a decimal delimiter.
	    if (length($code) > 4) {
		# Convert EXXXY to EXXX.Y
		$code = (substr $code, 0, 4) . '.' . (substr $code, 4);
	    }
	    $bodySystem = 18;
	} else {
	    if (length($code) > 3) {
		$code = (substr $code, 0, 3) . '.' . (substr $code, 3);
	    }
            # Determine body system, if the code is numeric
	    if ($code =~ /^(\d+)/) {
		# Parse out the int part (major number)
		my $iCode = int($1);
		# Find the first entry in the bounds that's greater than code
		for (my $bodySysIx = 0; $bodySysIx < @bodySystemCodeBounds; $bodySysIx++) {
		    if ($iCode < $bodySystemCodeBounds[$bodySysIx]) {
			$bodySystem = $bodySysIx;
			last;
		    }
		}
	    } else {
		# VXXX are body system 19
		$bodySystem = 19;
	    }
	}
	# Truncate the description to 24 characters and uppercase it
	$desc = substr(uc $desc, 0, 24);
	$desc =~ s/"/""/g;
	# Clean up the description to escape XML entities
	$desc =~ s/&/&amp;/g;
	$desc =~ s/</&lt;/g;
	$desc =~ s/>/&gt;/g;

	# Now print a row corresponding to the headers
#	print 'ICDCode,0,icd:' . ($i++) . ",$bodySystem" . ',"' . $code. '","' . $desc . '"' . "\n";
	print "  <ICDCode public-id=\"icd9:" . $code . "\">\n";
	print "    <BodySystem>$bodySystem</BodySystem>\n";
	print "    <Code>$code</Code>\n";
	print "    <CodeDesc>$desc</CodeDesc>\n";
	print "    <ICDEdition_Ext>ICD9</ICDEdition_Ext>\n";
	print "  </ICDCode>\n";
    }

}

# Close the XML file
print "</import>\n";
