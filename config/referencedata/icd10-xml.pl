#!/usr/bin/perl -w

# Print the header row of the delimited file
print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
print "<import usePeriodicFlushes=\"true\">\n";

while (<>) {
    # Strip trailing newline
    chomp;
    # Each line should be of the form
    # 'CODE','CATEGORY',"DESCRIPTION",...
    if (/^'([A-Z]\d[A-Z0-9]*)','\d+',"([^"]+)"/) {
	my ($code, $desc) = ($1, $2);
	
	my $bodySystem = -1;
	# Convert A000 -> A00.0
	if (length($code) > 3) {
	    $code = (substr $code, 0, 3) . '.' . (substr $code, 3);
	}

	# Clean up the description to escape XML entities
	$desc =~ s/&/&amp;/g;
	$desc =~ s/</&lt;/g;
	$desc =~ s/>/&gt;/g;

	for ($code) {
	    if (/[AB]/) {
		$bodySystem = 'A1';
	    } elsif (/(C|D[1-4])/) {
		$bodySystem = 'A2';
	    } elsif (/D/) {
		$bodySystem = 'A3';
	    } elsif (/E/) {
		$bodySystem = 'A4';
	    } elsif (/F/) {
		$bodySystem = 'A5';
	    } elsif (/G/) {
		$bodySystem = 'A6';
	    } elsif (/H[0-5]/) {
		$bodySystem = 'A7';
	    } elsif (/H/) {
		$bodySystem = 'A8';
	    } elsif (/I/) {
		$bodySystem = 'A9';
	    } elsif (/J/) {
		$bodySystem = 'A10';
	    } elsif (/K/) {
		$bodySystem = 'A11';
	    } elsif (/L/) {
		$bodySystem = 'A12';
	    } elsif (/M/) {
		$bodySystem = 'A13';
	    } elsif (/N/) {
		$bodySystem = 'A14';
	    } elsif (/O/) {
		$bodySystem = 'A15';
	    } elsif (/P/) {
		$bodySystem = 'A16';
	    } elsif (/Q/) {
		$bodySystem = 'A17';
	    } elsif (/R/) {
		$bodySystem = 'A18';
	    } elsif (/[ST]/) {
		$bodySystem = 'A19';
	    } elsif (/[VY]/) {
		$bodySystem = 'A20';
	    } else {
		$bodySystem = 'A21';
	    }
	}

	# Now print a row corresponding to the headers
#	print 'ICDCode,0,icd:' . ($i++) . ",$bodySystem" . ',"' . $code. '","' . $desc . '"' . "\n";
	print "  <ICDCode public-id=\"icd10:" . $code . "\">\n";
	print "    <BodySystem>$bodySystem</BodySystem>\n";
	print "    <Code>$code</Code>\n";
	print "    <CodeDesc>$desc</CodeDesc>\n";
	print "    <ICDEdition_Ext>ICD10</ICDEdition_Ext>\n";
	print "  </ICDCode>\n";
    }

}

# Close the XML file
print "</import>\n";
