TDIC utilizes a Medical Provider Network (MPN). If your employee has not been evaluated by a physician, we will send you a separate email with a list of medical providers. Once you receive this email, your employee may select a provider from the medical provider directory. Always call the provider before proceeding to the office to ensure that the address is correct and the selected office still provides treatment for occupational injuries and illnesses.

Give your employee the Medical Authorization Form and First Fill Temporary Pharmacy Card. The employee should give the Medical Authorization Form to the medical provider since it includes billing instructions. The pharmacy card can be used if any prescriptions are ordered by the medical provider.

Give your employee the Medical Provider Network Employee Notification & Guide. This guide explains how the network can be accessed online and explains the process to obtain medical treatment.

Have your employee complete the attached Workers’ Compensation Claim Form (DWC 1) and return to you. You will then complete the employer section and fax to 877.890.8410.

TDIC Workers’ Compensation benefits are administered by Sedgwick CMS. Within three business days, a Sedgwick claims examiner will contact you as part of the investigative process for the claim.

If your employee has already been evaluated by a physician, Sedgwick will work with your employee to move them into the MPN if necessary. Any medical treatment beyond an initial evaluation must be authorized by Sedgwick.



Attachments:
  •  Employer’s Report of Occupational Injury or Illness - Form 5020 (For employer records only)
  •  First Fill Temporary Pharmacy Card
  •  Medical Authorization Form
  •  Medical Provider Network Employee Notification & Guide
  •  Workers’ Compensation Claim Form (DWC 1) & Notice of Potential Eligibility