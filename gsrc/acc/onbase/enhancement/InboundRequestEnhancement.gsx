package acc.onbase.enhancement

uses acc.onbase.util.LoggerFactory
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */
enhancement InboundRequestEnhancement: InboundRequest_Ext {

  public function addRecipient(contact: Contact) {
    if (contact == null) {
      throw new IllegalArgumentException("Calling InboundRequestEnhancement.addRecipient without a contact")
    }
    addRecipients(new Contact[]{contact})
  }

  public function addRecipients(contacts: Contact[]) {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    if (_logger.DebugEnabled) {
      _logger.debug("Running method InboundRequestEnhancement.addRecipients(" + contacts + ")")
    }
    if (contacts.Count == 0) {
      throw new IllegalArgumentException("Calling InboundRequestEnhancement.addRecipients with no contacts")
    }

    foreach(contact in contacts) {
      var link = new InbdReqContactLink_Ext(){
          :InboundRequest = this,
          :Contact = contact
          }

      this.addToContacts(link)
    }
  }

  public function createClaimContact() {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    var claim = this.Claim
    if (_logger.DebugEnabled) {
      _logger.debug("Running method InboundRequestEnhancement.createClaimContact(" + claim + ")")
    }
    if (claim == null) {
      throw new IllegalArgumentException("Request not associated with a Claim")
    }
    foreach(contact in this.Contacts) {
      if (!claim.getRelatedContacts().contains(contact.Contact)) {
        claim.createClaimContact(contact.Contact)
      }
    }
  }

  public function removeRecipient(contact: Contact) {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    if (_logger.DebugEnabled) {
      _logger.debug("Running method InboundRequestEnhancement.removeReceipient(" + contact + ")")
    }
    if (contact == null) {
      throw new IllegalArgumentException("Calling InboundRequestEnhancement.removeReceipient without a contact")
    }

    var link = this.Contacts.singleWhere(\contactLink -> contactLink.Contact == contact)
    if (link != null) {
      this.removeFromContacts(link)
      link.remove()
    }
  }


  public property get LinkedRecipients(): List<Contact> {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    if (_logger.DebugEnabled) {
      _logger.debug("Running property InboundRequestEnhancement.LinkedRecipients()")
    }

    var contacts = this.Contacts.map(\contactLink -> contactLink.Contact)
    return contacts.toList()
  }

  public property get ContactsNotLinked(): List<Contact> {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    var claim = this.Claim
    if (_logger.DebugEnabled) {
      _logger.debug("Running property InboundRequestEnhancement.ContactsNotLinked(" + claim + ")")
    }
    if (claim == null) {
      throw new IllegalArgumentException("No Claim assoicated with the request")
    }
    var notLinked = new ArrayList<Contact>()

    //Only select contacts with valid email address
    var contacts = claim.getRelatedContacts().where(\contact -> contact.EmailAddress1 != null)

    var linked = this.LinkedRecipients

    // if the user isn't in the request's contacts list and it has an email address, move it to the available contacts list. Oh let's also only put it there if it's not alreasy there.
    if (!(linked.contains(User.util.CurrentUser.Contact)) && User.util.CurrentUser.Contact.EmailAddress1 != null && !(notLinked.contains(User.util.CurrentUser.Contact))) {
      notLinked.add(User.util.CurrentUser.Contact)
    }

    foreach(contact in contacts) {
      // sometimes the currently logged in user gets duplicated in the notLinked list when we don't check this here
      if (!linked.contains(contact) && !notLinked.contains(contact)) {
        notLinked.add(contact)
      }
    }
    return notLinked
  }

  public function linkShareBaseFolder(folder: ShareBase_Ext) {
    var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

    if (_logger.isDebugEnabled()) {
      _logger.debug("Running method InboundRequestEnhancement.linkShareBaseFolder(" + folder + ")")
    }

    if (folder == null) {
      throw new IllegalArgumentException("Calling InboundRequestEnhancement.linkShareBaseFolder with null folder value")
    }

    folder.Status = ShareBaseRequestStatus.TC_NOTCREATED
    this.ShareBaseFolder = folder

  }

  public function createInboundRequest() {

    if (this.Contacts.Count == 0) {
      throw new DisplayableException(DisplayKey.get("Accelerator.OnBase.ShareBase.STR_GW_MissingContacts"))
    }
    this.Owner = User.util.CurrentUser
    this.Created = gw.api.util.DateUtil.currentDate()
  }
}
