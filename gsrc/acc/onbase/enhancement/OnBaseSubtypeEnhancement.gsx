package acc.onbase.enhancement

uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.util.LoggerFactory
uses gw.api.util.TypecodeMapperUtil

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Enhancement to assist in converting between Guidewire typecodes and OnBase names.
 */
enhancement OnBaseSubtypeEnhancement: OnBaseDocumentSubtype_Ext {

  /**
   * @return OnBase alias for the typecode
   */
  public property get OnBaseName(): String {
    var alias = TypecodeMapperUtil.TypecodeMapper.getAliasByInternalCode(this.ListName, OnBaseConfigurationFactory.Instance.TypecodeNamespace, this.Code)

    if (alias == null) {
      var logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)
      logger.warn("Unmapped OnBaseDocumentSubtype_Ext: ${this.Code}")
    }

    return alias
  }

  /**
   * Look up typecode by OnBase alias
   *
   * @param alias OnBase alias
   * @return associated typecode
   */
  public static function findByOnBaseName(alias: String): OnBaseDocumentSubtype_Ext {
    var code = TypecodeMapperUtil.TypecodeMapper.getInternalCodeByAlias(OnBaseDocumentSubtype_Ext.RelativeName, OnBaseConfigurationFactory.Instance.TypecodeNamespace, alias)
    if (code == null) {
      throw new IllegalArgumentException("Unknown OnBaseDocumentSubtype: ${alias}")
    }
    return OnBaseDocumentSubtype_Ext.get(code)
  }

}
