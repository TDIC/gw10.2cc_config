package acc.onbase.configuration

uses acc.onbase.api.exception.OnBaseApiException

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 *   Exception type for IOnBaseConfiguration properties
 */

/**
 * Exception for IOnBaseConfiguration properties
 */
class OnBaseConfigurationException extends OnBaseApiException {
    /**
     * Constructor.
     *
     * @param msg The exception message.
     */
    construct(msg: String) {
        super(msg);
    }

    /**
     * Constructor.
     *
     * @param msg The exception message.
     * @param ex The underlying cause of this exception.
     */
    construct(msg: String, ex: Throwable) {
        super(msg, ex);
    }
}
