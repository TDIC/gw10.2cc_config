package acc.onbase.configuration

uses acc.onbase.util.LoggerFactory

uses java.io.File

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Implementation of IOnBaseConfiguration to get OnBase configuration settings.
 */
class OnBaseConfiguration implements IOnBaseConfiguration {
  private var _logger = LoggerFactory.getLogger(LoggerFactory.ConfigurationLoggerCategory)

  /**
   * Gets the base URL for OnBase Pop Web Server
   *
   * @return string URL of OnBase Pop Web Server
   */
  public override property get PopURL(): String {
    var spKey = "acc.onbase.PopURL"
    var url = (ScriptParameters.getParameterValue(spKey) as String)?.trim()
    if (url == null || url.Empty) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }
    return url
  }

  /**
   * Gets the base URL for the ShareBase Instance
   *
   * @return string URL of the ShareBase Instance
   */
  public override property get ShareBaseURL(): String {
    var spKey = "acc.onbase.ShareBaseURL"
    var url = (ScriptParameters.getParameterValue(spKey) as String)?.trim()
    if (url == null || url.Empty) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }
    return url
  }

  /**
   * Gets the library containing ShareBase Folders
   *
   * @return string the default ShareBase library
   */
  public override property get ShareBaseLibrary(): String {
    var spKey = "acc.onbase.ShareBaseLibrary"
    var key = (ScriptParameters.getParameterValue(spKey) as String)?.trim()
    if (key == null || key.Empty) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }
    return key
  }

  /**
   * Gets the API Authentication Token for the ShareBase Instance
   *
   * @return string the API Authentication Token for the ShareBase Instance
   */
  public override property get ShareBaseAPIToken(): String {
    return acc.onbase.api.security.SecurePropertiesManager.getTokenForShareBase()
  }

  /**
   * Gets the key for generating checksums for web client Pop.
   *
   * @return string key for checksum generation
   */
  public override property get PopChecksumKey(): String {
    var spKey = "acc.onbase.PopChecksumKey"
    var key = (ScriptParameters.getParameterValue(spKey) as String)?.trim()
    if (key == null || key.Empty) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }
    return key
  }

  /**
   * Switch to enable or disable checksum for DocPop URL
   *
   * @return true for enabled; false for disabled
   */
  public override property get EnableDocPopURLCheckSum(): Boolean {
    var spKey = "acc.onbase.EnablePopUrlChecksum"
    var enablePopUrlChecksum = ScriptParameters.getParameterValue(spKey) as Boolean

    if (enablePopUrlChecksum == null) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }

    return enablePopUrlChecksum
  }

  /**
   * Gets the key for generating checksums for the Get Doc service.
   *
   * @return string key for checksum generation
   */
  public override property get GetDocChecksumKey(): String {
    var spKey = "acc.onbase.GetDocChecksumKey"
    var key = (ScriptParameters.getParameterValue(spKey) as String)?.trim()
    if (key == null || key.Empty) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }
    return key
  }

  /**
   * Switch to enable count of linked documents
   *
   * @return true for enabled; false for disabled
   */
  public override property get EnableLinkedDocumentCount(): Boolean {
    var spKey = "acc.onbase.EnableLinkedDocumentCount"
    var enableLinkedDocumentCount = ScriptParameters.getParameterValue(spKey) as Boolean

    if (enableLinkedDocumentCount == null) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }

    return enableLinkedDocumentCount
  }

  /**
   * Gets the OnBase client type for viewing documents
   *
   * @return enum value of OnBase Client type
   */
  public override property get ClientType(): OnBaseClientType {
    var spKey = "acc.onbase.ClientType"
    var obClientType = (ScriptParameters.getParameterValue("acc.onbase.ClientType") as String)?.trim()
    switch (obClientType?.toLowerCase()) {
      case "unity":
        return OnBaseClientType.Unity
      case "web":
        return OnBaseClientType.Web
      default:
        _logger.error("Script Parameter ${spKey} not set. Defaulting to: Web")
        return OnBaseClientType.Web
    }
  }

  /**
   * Gets the web client type (HTML or ActiveX) for the OnBase web client.
   *
   * @return enum value of web client type
   */
  public override property get WebClientType(): OnBaseWebClientType {
    var spKey = "acc.onbase.WebClientType"
    var obWebClientType = (ScriptParameters.getParameterValue(spKey) as String)?.trim()

    switch (obWebClientType?.toLowerCase()) {
      case "html":
        return OnBaseWebClientType.HTML
      case "activex":
        return OnBaseWebClientType.ActiveX
      default:
        _logger.error("Script Parameter ${spKey} not set. Defaulting to: HTML")
        return OnBaseWebClientType.HTML
    }
  }

  /**
   * Gets the number of thumbnails displayed per page
   *
   * @return number of thumbnails per page
   */
  public override property get ThumbnailsPerPageCount(): Integer {
    var spKey = "acc.onbase.thumbnail.PerPageCount"
    var scriptParam = (ScriptParameters.getParameterValue(spKey) as String)?.trim()
    if (scriptParam == null || scriptParam == "") {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }
    var count: Integer
    try {
      count = Integer.parseInt(scriptParam)
      if (count > 0) {
        return count
      } else {
        logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} must be greater than 0.")
      }
    } catch (ex: NumberFormatException) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} is not a valid integer", ex)
    }
    return count
  }

  /**
   * Bounding size of thumbnails
   *
   * @return size in pixels of longest side
   */
  public override property get ThumbnailSize(): Long {
    var spKey = "acc.onbase.thumbnail.Size"
    var scriptParam = (ScriptParameters.getParameterValue(spKey) as String)?.trim()
    if (scriptParam == null || scriptParam == "") {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }
    var size: Long
    try {
      size = Long.parseLong(scriptParam)
      if (size > 0) {
        return size
      } else {
        logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} must be greater than 0.")
      }
    } catch (ex: NumberFormatException) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} is not a valid Long", ex)
    }
    return size
  }

  /**
   * Gets reference to folder for asynchronous upload to OnBase
   *
   * @return java.io.File file path to OnBase async upload folder
   */
  public override property get AsyncDocumentFolder(): File {
    var spKey = "acc.onbase.AsyncDocumentFolder"
    // Get Async document folder.
    var folderPath = (ScriptParameters.getParameterValue(spKey) as String)?.trim()
    if (folderPath == null || folderPath.Empty) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }

    var onbaseFolder = new File(folderPath)

    if (!onbaseFolder.exists()) {
      logAndThrowOnBaseConfigurationException("Async Document Folder not found for path: ${folderPath}")
    }
    return onbaseFolder
  }

  /**
   * Gets the threshold above which document uploads are performed asynchronously
   *
   * @return size in bytes of async threshold
   */
  public override property get AsyncDocumentSize(): Long {
    var spKey = "acc.onbase.AsyncDocumentSize"
    //set other parameters from plugin registry editor
    var size = (ScriptParameters.getParameterValue(spKey) as String)?.trim()
    if (size == null || size.Empty) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }

    // Get Async document size with K, KB, M, MB, G, GB suffix.
    var asyncDocumentSize = 0L
    try {
      //trim K, KB, M, MB, G, GB suffix at end of Size
      //If a value that exceeds max is specified a number format excaption is thrown
      if (size.endsWithIgnoreCase("k") || size.endsWithIgnoreCase("m") || size.endsWithIgnoreCase("g")) {
        asyncDocumentSize = Long.parseLong((size.substring(0, size.length() - 1)).trim())
      } else if (size.endsWithIgnoreCase("kb") || size.endsWithIgnoreCase("mb") || size.endsWithIgnoreCase("gb")) {
        asyncDocumentSize = Long.parseLong((size.substring(0, size.length() - 2)).trim())
      } else if (size.endsWithIgnoreCase("bytes")) {
        asyncDocumentSize = Long.parseLong((size.substring(0, size.length() - 5)).trim())
      } else {
        logAndThrowOnBaseConfigurationException("Error while parsing the ${spKey} Script Paramter: '" + size + "'. Must specify unit (bytes, Kb, Mb, Gb)")
      }

      //If a negative value is specified then throw an exception
      if (asyncDocumentSize < 0) {
        logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} must be non-negative.")
      }

      //convert to kb/mb/gb
      if (size.endsWithIgnoreCase("k") || size.endsWithIgnoreCase("kb")) {
        asyncDocumentSize = asyncDocumentSize * 1000
      } else if (size.endsWithIgnoreCase("m") || size.endsWithIgnoreCase("mb")) {
        asyncDocumentSize = asyncDocumentSize * 1000000
      } else if (size.endsWithIgnoreCase("g") || size.endsWithIgnoreCase("gb")) {
        asyncDocumentSize = asyncDocumentSize * 1000000000
      }

      // Throw Exception if multiplying the value exceeds max long. An overflow happens which changes asyncDocumentSize to negative
      if (asyncDocumentSize < 0) {
        logAndThrowOnBaseConfigurationException("Error while parsing the ${spKey} Script Paramter: '" + size + "'")
      }
    } catch (ex: NumberFormatException) {
      logAndThrowOnBaseConfigurationException("Error while parsing the ${spKey} Script Paramter: '" + size + "'", ex)
    }
    return asyncDocumentSize
  }

  /**
   * Gets the interval between retries for the OnBaseMessageTransport plugin
   *
   * @return retry interval in seconds
   */
  public override property get RetryIntervalSeconds(): Integer {
    var spKey = "acc.onbase.messaging.RetryInterval"
    var scriptParam = (ScriptParameters.getParameterValue(spKey) as String)?.trim()
    if (scriptParam == null || scriptParam == "") {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }
    var interval: Integer
    try {
      interval = Integer.parseInt(scriptParam)
      if (interval > 0) {
        return interval
      } else {
        logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} must be greater than 0.")
      }
    } catch (ex: NumberFormatException) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} is not a valid integer", ex)
    }
    return interval
  }

  /**
   * Gets the maximum number of retries for the OnBaseMessageTransport plugin
   *
   * @return max retry count
   */
  public override property get MaxRetries(): Integer {
    var spKey = "acc.onbase.messaging.MaxRetries"
    var scriptParam = (ScriptParameters.getParameterValue(spKey) as String)?.trim()
    if (scriptParam == null || scriptParam == "") {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} not set")
    }
    var max: Integer
    try {
      max = Integer.parseInt(scriptParam)
      if (max >= 0) {
        return max
      } else {
        logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} must be non-negative.")
      }
    } catch (ex: NumberFormatException) {
      logAndThrowOnBaseConfigurationException("Script Parameter ${spKey} is not a valid integer", ex)
    }
    return max
  }


  private function logAndThrowOnBaseConfigurationException(message: String) {
    _logger.error(message)
    throw new OnBaseConfigurationException(message)
  }

  private function logAndThrowOnBaseConfigurationException(message: String, ex: Throwable) {
    _logger.error(message)
    throw new OnBaseConfigurationException(message, ex)
  }

  /**
   * Gets the name of the current Guidewire center (e.g. "ClaimCenter")
   *
   * @return string name of current center
   */
  public override property get SourceCenterName(): String {
    return gw.api.system.server.ServerUtil.Product.ProductName
  }

  /**
   * Gets the namespace for OnBase typecode translation
   *
   * @return string of OnBase typecode namespace
   */
  public override property get TypecodeNamespace(): String {
    return "onbase" //Used to define typecode mapping in typecodemapping.xml
  }

  /**
   * Gets the relative file path from 'config' for secure.properties
   *
   * @return string of relative file path from 'config' for secure.properties
   */
  public override property get SecurePropertiesFile(): String {
    return "gsrc/acc/onbase/api/security/secure.properties"
  }

  /**
   * Gets the relative file path from 'config' for encryption.properties
   *
   * @return string of relative file path from 'config' for encryption.properties
   */
  public override property get EncryptionPropertiesFile(): String {
    return "gsrc/acc/onbase/api/service/encryption.properties"
  }
}