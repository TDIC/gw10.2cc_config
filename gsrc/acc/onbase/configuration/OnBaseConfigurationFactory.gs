package acc.onbase.configuration

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 *
 * Provider for OnBase configuration settings.
 * A custom implementation of IOnBaseConfiguration can be provided
 * at run-time through the Instance set method to change where and how properties are stored.
 */
class OnBaseConfigurationFactory {
  private static var _instance: IOnBaseConfiguration = new OnBaseConfiguration()
  public static property get Instance(): IOnBaseConfiguration {
    return _instance
  }
  public static property set Instance(manager: IOnBaseConfiguration){
    _instance = manager
  }
}