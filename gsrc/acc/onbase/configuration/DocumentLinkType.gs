package acc.onbase.configuration

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 *
 * Document link type enum
 */
enum DocumentLinkType {
  activityid, checkid, reserveid, ServiceRequest, claimid, exposureid, VehicleCoverage, Subrogations, Subrogation, Matter, Negotiation
}