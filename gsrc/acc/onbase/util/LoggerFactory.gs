package acc.onbase.util

uses gw.pl.logging.LoggerCategory
uses org.slf4j.ILoggerFactory
uses org.slf4j.Logger

uses java.lang.reflect.Method

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 06/08/2018 - Daniel Q. Yu
 * * Initial implementation
 * <p>
 * Factory to create new loggers.
 * <p>
 * It uses an internal class extends gw.pl.logging.LoggerCategory class so that it can call protected method createLogger.
 * <p>
 * An ILoggerFactory can be set and used to create loggers.
 * <p>
 * A random LoggerFactory can also be used, provides a static method with one string parameter returns a org.slf4j.Logger object.
 */
class LoggerFactory {
  /**
   * slf4jLoggerFactory to create loggers.
   */
  private static var _slf4jLoggerFactory : ILoggerFactory
  /**
   * LoggerFactory Method to create loggers
   */
  private static var _loggerFactoryMethod : Method
  /**
   * Services logger category.
   */
  public static final var ServicesLoggerCategory : String = "Document_OnBaseDMS"
  /**
   * Application logger category.
   */
  public static final var ApplicationLoggerCategory : String = "Document_OnBaseDMS"
  /**
   * Plugin logger category.
   */
  public static final var PluginLoggerCategory : String = "Document_OnBaseDMS"
  /**
   * Configuration logger category.
   */
  public static final var ConfigurationLoggerCategory : String = "Document_OnBaseDMS"

  /**
   * An internal class extends LoggerCategory.
   */
  private static class GW9LoggerFactory extends LoggerCategory {
    public static function getLogger(name : String) : Logger {
      return GW9LoggerFactory.createLogger(name)
    }
  }

  /**
   * Set a ILoggerFactory to create loggers.
   *
   * @param factory The ILoggerFactory object.
   */
  public static function setLoggerFactory(factory : org.slf4j.ILoggerFactory) {
    _slf4jLoggerFactory = factory
  }

  /**
   * Set a random LoggerFactory provides a static method with one string parameter returns a org.slf4j.Logger object.
   *
   * @param className  The class name for the LoggerFactory.
   * @param methodName The method name to create new loggers.
   * @return true if logger can be created.
   */
  public static function setLoggerFactory(className : String, methodName : String) : boolean {
    var logger : org.slf4j.Logger = null
    try {
      var c = Class.forName(className)
      _loggerFactoryMethod = c.getDeclaredMethod(methodName, new Class[]{String})
      logger = _loggerFactoryMethod.invoke(null, new String[]{ApplicationLoggerCategory}) as org.slf4j.Logger
    } catch (ex) {
      ex.printStackTrace()
    }

    if (logger != null) {
      return true
    } else {
      _loggerFactoryMethod = null
      return false
    }
  }

  /**
   * Create a logger with provided name.
   *
   * @param name The logger name.
   * @return a org.slf4j.Logger object.
   */
  public static function getLogger(name : String) : Logger {
    if (_slf4jLoggerFactory != null) {
      return _slf4jLoggerFactory.getLogger(name)
    }
    if (_loggerFactoryMethod != null) {
      return _loggerFactoryMethod.invoke(null, new String[]{name}) as org.slf4j.Logger
    }
    return GW9LoggerFactory.getLogger(name)
  }
}