package acc.onbase.util

uses acc.onbase.api.application.DocumentRetrieval
uses acc.onbase.api.security.SecurityManager
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.wsc.onbasegetdocwsc.getdoc.GetDoc
uses acc.onbase.wsc.onbasegetdocwsc.getdoc.elements.ThumbnailRequest
uses gw.api.database.IQueryBeanResult
uses gw.transaction.Transaction
uses gw.util.Base64Util


/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 05/12/2017 - Tori Brenneison
 * * Initial implementation.
 *
 * 08/31/2017 - Tori Brenneison
 * * Refactor to remove vars & reset function from ClaimDocuments.pcf
 *
 * 01/17/2018 - Tori Brenneison
 * Reference user preferences for showing the thumbnail view by default.
 */

/**
 * Utility class for document thumbnail viewer
 */

class ThumbnailUtil {

  private var _displayNoThumbDocs : boolean as DisplayNoThumbDocs = true
  private var _thumbnailPageNumber : Integer as ThumbnailPageNumber = 1
  private var _activeViewer : OnBaseDocListViewerType_Ext as ActiveViewerType = DefaultViewerPreference

  public property get DefaultViewerPreference() : OnBaseDocListViewerType_Ext {
    return User.util.CurrentUser.UserSettings.OnBaseDocListViewerType_Ext
  }

  public property set DefaultViewerPreference(viewerType : OnBaseDocListViewerType_Ext) {
    var user = User.util.CurrentUser
    Transaction.runWithNewBundle(\bundle -> {
      user = bundle.add(user)
      user.UserSettings.OnBaseDocListViewerType_Ext = viewerType
    })
  }

  public property get ActiveViewIsDefault() : boolean {
    return ActiveViewerType == DefaultViewerPreference
  }

  /**
   * ActiveViewIsDefault serves as the backing property for a checkbox intended to set the currently
   * active document view as the default. A setter is required in order to make the checkbox editable. Setting this
   * property to true is treated as a method call.
   * @param value true to set default view to the currently active view
   */
  public property set ActiveViewIsDefault(value : boolean) {
    if (value) {
      DefaultViewerPreference = ActiveViewerType
    }
  }

  public property get IsThumbnailViewActive() : boolean {
    return ActiveViewerType == OnBaseDocListViewerType_Ext.TC_ONBASETHUMBNAILVIEWER;
  }

  public function resetThumbnailVars() {
    ThumbnailPageNumber = 1
    DisplayNoThumbDocs = true
  }

  public function incrementPageNumber(docCount: Integer): Integer {
    var totalPages = pageCount(docCount);
    if (ThumbnailPageNumber < totalPages) {
      ThumbnailPageNumber++
    }
    return ThumbnailPageNumber
  }

  public function decrementPageNumber(): Integer {
    if (ThumbnailPageNumber != 1) {
      ThumbnailPageNumber--
    }
    return ThumbnailPageNumber
  }

  static function pageCount(docCount: Integer): Integer {

    var thumbsPerPage = OnBaseConfigurationFactory.Instance.ThumbnailsPerPageCount
    var numPages = docCount / thumbsPerPage
    var thumbsLeft = docCount % thumbsPerPage

    if (thumbsLeft > 0) {
      numPages++
    }

    return numPages
  }

  static function getStartIndex(currentPage : Integer): Integer {
    var startIndex = (currentPage - 1) * OnBaseConfigurationFactory.Instance.ThumbnailsPerPageCount
    return startIndex
  }

  static function getEndIndex(startIndex : Integer): Integer {
    var endIndex = startIndex + OnBaseConfigurationFactory.Instance.ThumbnailsPerPageCount
    return endIndex
  }

  static function getThumbnailBytes(docID: String): String {
    //hash
    var key = (OnBaseConfigurationFactory.Instance.GetDocChecksumKey as String).trim()
    var hashBytes = SecurityManager.getHashBytes(docID, key)
    var hash = Base64Util.encode(hashBytes)

    //request
    var thumbnailReq = new ThumbnailRequest()
    var thumbnailSize = OnBaseConfigurationFactory.Instance.ThumbnailSize as String
    thumbnailReq.DocID = docID
    thumbnailReq.Hash = hash
    thumbnailReq.MaxHeight = thumbnailSize
    thumbnailReq.MaxWidth = thumbnailSize

    var streamingService = new GetDoc()
    var response = streamingService.GetThumbnail(thumbnailReq)

    //return base64 image data
    var temp = response.FileContents.Bytes
    var returndata = Base64Util.encode(temp)
    return returndata
  }

  static function popWebClient(docId: String): String {
    var retrievalApp = new DocumentRetrieval()
    var popURL = retrievalApp.getDocumentWebURL(docId, OnBaseConfigurationFactory.Instance.WebClientType)
    return popURL
  }

  static function getDocSublist(currentPage : Integer, documentList: IQueryBeanResult<Document>): List<Document> {

    var docList = documentList.toList()
    var startIndex = getStartIndex(currentPage)
    var endIndex = getEndIndex(startIndex)

    if (endIndex < docList.Count) {
      var subList = docList.subList(startIndex, endIndex)
      return subList
    }

    return docList.subList(startIndex, docList.Count)
  }


}