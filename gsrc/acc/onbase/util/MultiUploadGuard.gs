package acc.onbase.util

uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
uses gw.api.util.LocationUtil
uses gw.document.DocumentCreationInfo
uses gw.pl.persistence.core.Bundle
uses gw.transaction.Transaction
uses pcf.api.Location

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */

class MultiUploadGuard {

  private final var _documentCreationInfos : Collection<DocumentCreationInfo>

  construct(documentCreationInfos : Collection<DocumentCreationInfo>) {
    _documentCreationInfos = documentCreationInfos
  }

  public function commit(location : Location) {

    var didCommit = false

    try {
      location.commit()
      didCommit = true
    }
    finally {
      afterCommit(location.Bundle, didCommit)
    }
  }

  function afterCommit(bundle : Bundle, didCommit : boolean) {

    if (didCommit) {
      return
    }

    var savedDocuments = bundle.getBeansByRootType(Document).whereTypeIs(Document).where(\doc -> doc.DocUID.HasContent || doc.PendingDocUID.HasContent)
    if (!savedDocuments.HasElements) {
      return
    }

    // Commit successful documents
    Transaction.runWithNewBundle(\newBundle -> {
      for (doc in savedDocuments) {
        doc = newBundle.add(doc)
        ensureDateSet(doc)
      }
    })

    // Display info message in the UI
    LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("Accelerator.OnBase.DocumentArchiveFailure.STR_GW_ProblemDuringUpload"))

    // Committed documents should no longer be displayed on the upload screen
    foreach (doc in savedDocuments){
      _documentCreationInfos.removeWhere(\dci -> dci.Document == doc)
      doc.remove()
    }
  }

  private function ensureDateSet(doc : Document) {
    if (doc.DateCreated == null || doc.DateModified == null) {
      var date = DateUtil.currentDate()
      doc.DateCreated = date
      doc.DateModified = date
    }
  }

}