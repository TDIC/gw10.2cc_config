package acc.onbase.util

/**
 * OnBaseDemoUtils - Code for OnBaseDemoPop.pcf
 * <p>
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */
class OnBaseDemoUtils {

  public static function constructCustomQueryURL(claim: Claim): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=ClaimCenter&Action=CC+Custom+Query&Claim+Number=" + UrlEncode(claim.ClaimNumber)
  }

  public static function constructFolderURL(claim: Claim): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=ClaimCenter&Action=CC+Foldering&Claim+Number=" + UrlEncode(claim.ClaimNumber)
  }

  public static function uploadDocURL(claim: Claim): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=ClaimCenter&Action=CC+Doc+Upload&Claim+Number=" + UrlEncode(claim.ClaimNumber)
  }

  public static function unityFormURL(claim: Claim): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=ClaimCenter&Action=CC+Unity+Form&Claim+Number=" + UrlEncode(claim.ClaimNumber)
  }

  public static function docPacketURL(claim: Claim, templateName: String): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=ClaimCenter&Action=CC+Packet&Claim+Number=" + UrlEncode(claim.ClaimNumber) + "&Template+Name=" + UrlEncode(templateName)
  }

   public static function combinedViewerURL(claim : Claim) : String {
     //searchkeyword nodes require value, OnBase keyword ID, OnBase keyword name, indexonly value, and datatype value
     var claimNumberSearchKeywordNode = AePopUtils.createCombinedViewSearchKeywordNode(claim.ClaimNumber, "107", "Claim Number", "0", "2")
     var policyNumberSearchKeywordNode = AePopUtils.createCombinedViewSearchKeywordNode(claim.Policy.PolicyNumber, "121", "Policy Number", "0", "2")
     var searchNodes = new ArrayList<String>()
      searchNodes.add(claimNumberSearchKeywordNode)
      searchNodes.add(policyNumberSearchKeywordNode)
     return AePopUtils.generateCombinedViewScrapeXml("101", searchNodes)
   }

  private static function UrlEncode(urlParam: String): String {
    return java.net.URLEncoder.encode(urlParam, java.nio.charset.StandardCharsets.UTF_8.toString())
  }

}