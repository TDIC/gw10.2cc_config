package acc.onbase.api.sharebaseconfig

uses acc.onbase.api.rest.RestApiManager

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */

enhancement ShareBaseFolderEnhancement: ShareBase_Ext {

  /**
   * Initiate API Calls for folder creation and share link
   */

  public function initiateFolderCreationAndShare() {

    var sharebaseAPI = RestApiManager.getCallShareBaseAPI()
    sharebaseAPI.createFolderfromAPI(this)
    sharebaseAPI.getShareBaseLink(this)
  }

  /**
   * Call ShareBase API to delete the folder
   */

  public function deleteFolderIfExists() {
    //check if the folder is created in ShareBase
    if (this.ShareBaseFolderUID != null) {
      var sharebaseAPI = RestApiManager.getCallShareBaseAPI()
      sharebaseAPI.deleteFolderInShareBase(this)
    }
  }
}