package acc.onbase.api.application

uses acc.onbase.configuration.DocumentLinkType
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses acc.onbase.util.LoggerFactory
uses gw.api.database.Query
uses gw.api.locale.DisplayKey

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 08/31/2016 - Daniel Q. Yu
 * * Initial implementation for BC8 doclink metadata package.
 * <p>
 * 09/27/2016 - Daniel Q. Yu
 * * Change document search for related entities from AND to OR
 * <p>
 * 10/12/2016 - Daniel Q. Yu
 * * Refactored for CC8
 */

/**
 * Document linking application.
 */
class DocumentLinking {
  /**
   * Logger
   */
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ApplicationLoggerCategory)

  /**
   * Link a document to entity.
   *
   * @param entity   The entity which documents linked to.
   * @param document The document to be linked to the entity.
   * @param linkType The document link type.
   */
  public function linkDocumentToEntity(entity: KeyableBean, document: Document, linkType: DocumentLinkType) {
    linkDocumentsToEntity(entity, new Document[]{document}, linkType)
  }

  /**
   * Link multiple documents to entity.
   *
   * @param entity    The entity which documents linked to.
   * @param documents The list of document ids to be linked to the entity.
   * @param linkType  The document link type.
   */
  public function linkDocumentsToEntity(entity: KeyableBean, documents: Object[], linkType: DocumentLinkType) {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentLinking.linkDocumentToEntity(" + entity + "," + documents + "," + linkType + ")")
    }
    var user = User.util.CurrentUser == null ? User.util.UnrestrictedUser : User.util.CurrentUser
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      foreach (doc in documents) {
        var link = new OnBaseDocumentLink_Ext()
        link.LinkType = linkType.toString()
        link.LinkedEntity = entity.PublicID
        link.Document = doc as Document
      }
    }, user)
  }

  /**
   * Unlink a document from entity.
   *
   * @param entity   The entity which document to be unlinked from.
   * @param document The document to be unlinked from the entity.
   * @param linkType The document link type.
   */
  public function unlinkDocumentFromEntity(entity: KeyableBean, document: Document, linkType: DocumentLinkType) {
    unlinkDocumentsFromEntity(entity, new Document[]{document}, linkType)
  }

  /**
   * Unlink multiple documents from entity.
   *
   * @param documents The list of documents to be unlinked from the entity.
   * @param linkType  The document link type.
   * @param entityId  The entity id which document to be unlinked from.
   */
  public function unlinkDocumentsFromEntity(entity: KeyableBean, documents: Object[], linkType: DocumentLinkType) {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentLinking.unlinkDocumentsFromEntity(" + entity + "," + documents + "," + linkType + ")")
    }
    var user = User.util.CurrentUser == null ? User.util.UnrestrictedUser : User.util.CurrentUser
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      foreach (doc in documents) {
        var query = Query.make(OnBaseDocumentLink_Ext)
        query.compare("LinkType", Equals, linkType.toString())
        query.compare("LinkedEntity", Equals, entity.PublicID)
        query.compare("Document", Equals, doc as Document)
        var result = query.select().first()
        if (result != null) {
          bundle.delete(result)
        }
      }
    }, user)
  }

  /**
   * Get documents linked to entity.
   *
   * @param entityId The entity which documents linked to.
   * @param linkType The document link type.
   * @return A list of OnBase documents which linked to the entity.
   */
  public function getDocumentsLinkedToEntity(entity: KeyableBean, linkType: DocumentLinkType): List<Document> {
    if (_logger.isDebugEnabled()) {
      _logger.debug("Running method DocumentLinking.getDocumentsLinkedToEntity(" + entity + "," + linkType + ")")
    }
    var linkedDocs = new ArrayList<Document>()
    var query = Query.make(OnBaseDocumentLink_Ext)
    query.compare("LinkType", Equals, linkType.toString())
    query.compare("LinkedEntity", Equals, entity.PublicID)
    var queryResult = query.select()
    foreach (r in queryResult) {
      linkedDocs.add(r.Document)
    }

    return linkedDocs
  }

  /**
   * Get documents not linked to an entity in OnBase.
   *
   * @param entity   The entity which documents linked to.
   * @param linkType The document link type.
   * @param beans    Additional entity beans to be passed in for process.
   * @return DocumentSearchResult object contains all documents linked to this entity.
   */
  public function getDocumentsNotLinkedToEntity(entity: KeyableBean, linkType: DocumentLinkType, beans: KeyableBean[]): List<Document> {
    if (_logger.DebugEnabled) {
      _logger.debug("Running method DocumentMetadataSource.getDocumentsNotLinkedToEntity(" + entity + ", " + linkType + ", " + beans + ")...")
    }
    var relatedDocs = new ArrayList<Document>()
    var notLinked = new ArrayList<Document>()
    foreach (bean in beans) {
      var criteria = new DocumentSearchCriteria()
      if (bean typeis Claim) {
        criteria.Claim = bean
      }

      if (criteria.Claim == null) {
        continue
      }

      criteria.IncludeObsoletes = false
      var results = criteria.performSearch()
      foreach (r in results) {
        if (!relatedDocs.contains(r)) {
          relatedDocs.add(r as Document)
        }
      }
    }
    var linked = getDocumentsLinkedToEntity(entity, linkType)
    foreach (doc in relatedDocs) {
      if (!linked.contains(doc)) {
        notLinked.add(doc)
      }
    }
    return notLinked
  }

  /**
   * Check if a document is linked to an entity.
   *
   * @param docUID   The document id to be checked.
   * @param linkType The document link type.
   * @param entity   The entity to be checked.
   * @return True if the document is linked to the entity.
   */
  public function isDocumentLinkedToEntity(document: Document, linkType: DocumentLinkType, entity: KeyableBean): boolean {
    if (_logger.isDebugEnabled()) {
      _logger.debug("Running method DocumentLinking.isDocumentLinkedToEntity(" + entity + ", " + document + "," + linkType + ")")
    }
    var results = getDocumentsLinkedToEntity(entity, linkType)
    for (doc in results) {
      if (doc.DocUID == document.DocUID) {
        return true
      }
    }
    return false
  }

  /**
   * Get the UI Label for document linking.
   *
   * @param entity   The entity which documents linked to.
   * @param linkType The document link type.
   * @return The UI Label.
   */
  public static function getLinkingDocumentUILabel(entity: KeyableBean, linkType: DocumentLinkType): String {
    if (entity == null || linkType == null) {
      throw new java.lang.NullPointerException()
    }
    if (!OnBaseConfigurationFactory.Instance.EnableLinkedDocumentCount) {
      return DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentListPopup_Label_NoCount")
    }
    var docCount = new DocumentLinking().getDocumentsLinkedToEntity(entity, linkType).Count
    if (docCount > 0) {
      return DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentListPopup_Label_ViewDocuments", docCount)
    } else {
      return DisplayKey.get("Accelerator.OnBase.STR_GW_DocumentListPopup_Label_NoDocument")
    }
  }
}
