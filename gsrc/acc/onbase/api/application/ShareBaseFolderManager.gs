package acc.onbase.api.application

uses gw.document.ContentDisposition
uses gw.document.DocumentContentsInfo
uses gw.document.DocumentsUtilBase

class ShareBaseFolderManager {

  /**
   * Build a javascript redirect to ShareBase URL
   *
   * @param url The ShareBase URL
   */
  public static function getShareBaseURL(url : String) {
    var js = "document.location.href='" + url + "';"
    var contents = "<html><head><script>" + js + "</script></head></html>"
    var dci = new DocumentContentsInfo(DocumentContentsInfo.ContentResponseType.DOCUMENT_CONTENTS, contents, "text/html")
    DocumentsUtilBase.renderDocumentContentsDirectly("sharebaseURL", dci, ContentDisposition.INLINE)
  }

}