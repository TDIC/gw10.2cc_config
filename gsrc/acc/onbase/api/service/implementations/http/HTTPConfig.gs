package acc.onbase.api.service.implementations.http

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */

/**
 * Config class used to set the HTTP BaseURL, a more specific resource path
 */

class HTTPConfig {
  private var _baseURL: String as readonly BaseURL
  private var _resourcePath: String as readonly ResourcePath

  construct(baseURL: String, resourcePath: String) {
    _baseURL = baseURL
    _resourcePath = resourcePath
  }

}