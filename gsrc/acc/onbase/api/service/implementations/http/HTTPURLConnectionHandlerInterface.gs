package acc.onbase.api.service.implementations.http

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */

/**
 * Interface for http requests to be handled - contains the specific implementations for GET POST and DELETE
 */
interface HTTPURLConnectionHandlerInterface {
  public function handleGet(config : HTTPConfig) : HTTPClientInterface

  public function handleDelete(config : HTTPConfig) : HTTPClientInterface

  public function handlePost(config : HTTPConfig) : HTTPClientInterface

}