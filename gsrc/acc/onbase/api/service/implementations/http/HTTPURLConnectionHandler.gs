package acc.onbase.api.service.implementations.http

uses java.net.HttpURLConnection
uses java.net.URL

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */

/**
 * RequestHandler for HttpURLConnection Client - contains handle methods for GET POST and DELETE
 */
class HTTPURLConnectionHandler implements HTTPURLConnectionHandlerInterface {

  private final var _connectTimeout: int
  private final var _readTimeout: int

  construct() {
    _connectTimeout = 0
    _readTimeout = 0
  }

  construct(connect: int, read: int) {
    _connectTimeout = connect
    _readTimeout = read
  }

  override function handleGet(config: HTTPConfig): HTTPClientInterface {
    var request = new URL(config.BaseURL + config.ResourcePath)
    var client = request.openConnection() as HttpURLConnection
    client.RequestMethod = "GET"
    client.setRequestProperty("Accept", "application/json")
    client.ConnectTimeout = _connectTimeout
    client.ReadTimeout = _readTimeout
    return new HTTPClient(client)
  }

  override function handlePost(config: HTTPConfig): HTTPClientInterface {
    var request = new URL(config.BaseURL + config.ResourcePath)
    var client = request.openConnection() as HttpURLConnection
    client.RequestMethod = "POST"
    client.setRequestProperty("Accept", "application/json")
    client.setRequestProperty("Content-Type", "application/json;charset=UTF-8")
    client.ConnectTimeout = _connectTimeout
    client.ReadTimeout = _readTimeout
    client.DoOutput = true
    return new HTTPClient(client)
  }

  override function handleDelete(config: HTTPConfig): HTTPClientInterface {
    var request = new URL(config.BaseURL + config.ResourcePath)
    var client = request.openConnection() as HttpURLConnection
    client.RequestMethod = "DELETE"
    client.setRequestProperty("Accept", "application/json")
    client.ConnectTimeout = _connectTimeout
    client.ReadTimeout = _readTimeout
    return new HTTPClient(client)
  }

}