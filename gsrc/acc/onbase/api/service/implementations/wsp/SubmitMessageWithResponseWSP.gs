package acc.onbase.api.service.implementations.wsp

uses acc.onbase.api.service.implementations.wsp.util.WSPUtil
uses acc.onbase.api.service.interfaces.SubmitMessageWithResponseInterface
uses acc.onbase.util.LoggerFactory
uses gw.xml.XmlElement

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */
class SubmitMessageWithResponseWSP implements SubmitMessageWithResponseInterface {
  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  /**
   * Submit a message to archive to OnBase
   *
   * @param payload string payload of message
   * @return the document handle or error message from onbase
   */
  override function submitMessageWithResponse(payload: String): String {
    _logger.debug("Start executing submitMessageWithResponse() using WSP service.")

    var service = WSPUtil.getWSPService()
    var response = service.SubmitMessageWithResponse(payload)

    _logger.debug("Finished executing submitMessageWithResponse() using WSP service with document ID: ${response}")
    return response
  }

  /**
   * Submit a message to archive to OnBase
   *
   * @param payload XML message
   * @return the document handle or error message from onbase
   */
  override function submitMessageWithResponse(payload: XmlElement): String {
    return submitMessageWithResponse(payload.asUTFString())
  }
}