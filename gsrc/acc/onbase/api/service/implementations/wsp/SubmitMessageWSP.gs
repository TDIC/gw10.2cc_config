package acc.onbase.api.service.implementations.wsp

uses acc.onbase.api.service.implementations.wsp.util.WSPUtil
uses acc.onbase.api.service.interfaces.SubmitMessageInterface
uses acc.onbase.util.LoggerFactory
uses gw.xml.XmlElement

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */
class SubmitMessageWSP implements SubmitMessageInterface {

  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  /**
   * Submit a message to OnBase
   *
   * @param payload string payload of message
   * @return submitted message id
   */
  override function submitMessage(payload: String): long {
    _logger.debug("Start executing submitMessage() using WSP service.")

    var service = WSPUtil.getWSPService()
    var response = service.SubmitMessage(payload)

    _logger.debug("Finished executing submitMessage() using WSP service with message ID: ${response}")
    return response.toLong()
  }

  /**
   * Submit a message to OnBase
   *
   * @param payload XML message
   * @return submitted message id
   */
  override function submitMessage(payload: XmlElement): long {
    return submitMessage(payload.asUTFString())
  }
}