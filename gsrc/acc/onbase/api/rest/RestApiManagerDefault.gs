package acc.onbase.api.rest

uses acc.onbase.api.sharebaseconfig.ShareBaseAPI
uses acc.onbase.api.sharebaseconfig.ShareBaseInterface

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 */
class RestApiManagerDefault implements RestApiInterface {

  public property get ShareBaseAPI(): ShareBaseInterface {
    return new ShareBaseAPI()
  }

}
