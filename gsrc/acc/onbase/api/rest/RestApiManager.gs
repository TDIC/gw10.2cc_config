package acc.onbase.api.rest

uses acc.onbase.api.sharebaseconfig.ShareBaseInterface


/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * The manager class for all API services.
 */
class RestApiManager {
  public static var _instance: RestApiInterface as Instance = new RestApiManagerDefault()

  public static function getCallShareBaseAPI(): ShareBaseInterface {
    return Instance.ShareBaseAPI
  }

}
