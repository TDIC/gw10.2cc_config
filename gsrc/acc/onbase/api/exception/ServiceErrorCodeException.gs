package acc.onbase.api.exception

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 * <p>
 * Last Changes:
 * 02/02/2015 - Clayton Sandham
 * * Initial implementation.
 */

/**
 * This is for errors returned in a status or error message from OnBase.
 */
class ServiceErrorCodeException extends ServicesTierException {
  /**
   * Constructor.
   *
   * @param msg The exception message.
   */
  construct(msg: String) {
    super(msg);
  }

  /**
   * Constructor.
   *
   * @param msg The exception message.
   * @param ex The underlying cause of this exception.
   */
  construct(msg: String, ex: Throwable) {
    super(msg, ex);
  }
}
