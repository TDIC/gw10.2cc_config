package acc.onbase.api.si

uses acc.onbase.api.si.schema.onbasemessage.OnBaseMessage
uses acc.onbase.configuration.OnBaseConfigurationFactory
uses gw.xml.XmlElement
uses gw.xml.XmlSimpleValue

/**
 * Hyland Build Version: cc-10.0.1-15-g634af75

 *
 * Simplify construction of messages intended for OnBase
 */
public class MessageBuilder {

  private var _message : OnBaseMessage as readonly Message  = new OnBaseMessage()

  private var _propertyIndex = 0
  private var _partIndex = 0

  /**
   * Create a new builder for a specific message type
   *
   * @param messageType type of message, typically an event name
   */
  construct(messageType : String) {
    _message.MessageType = messageType
    _message.Source = OnBaseConfigurationFactory.Instance.SourceCenterName
  }

  /**
   * Add a simple string property to the message
   *
   * For properties with multiple values, see addValueListAsMessagePart
   *
   * @param key property key, must be unique
   * @param value property value
   */
  public function addProperty(key : String, value : String) {
    _message.Property[_propertyIndex].Key = key
    _message.Property[_propertyIndex].Value = value

    _propertyIndex++
  }

  /**
   * Add a single XmlElement as a MessagePart
   *
   * @param key message part key, must be unique
   * @param child child XML, e.g. from a GX Model
   */
  public function addMessagePart(key : String, child : XmlElement) {
    addMessagePart(key, {child})
  }

  /**
   * Add multiple XmlElements as a MessagePart
   *
   * @param key message part key, must be unique
   * @param children child nodes, e.g. from a GX Model
   */
  public function addMessagePart(key : String, children : List<XmlElement>) {
    _message.MessagePart[_partIndex].Key = key
    foreach (child in children) {
      _message.MessagePart[_partIndex].addChild(child)
    }

    _partIndex++
  }

  /**
   * Add a list of string values as a MessagePart
   *
   * @param key message part key, must be unique
   * @param values values to add
   * @param elementName name to use for value nodes, default: Value
   */
  public function addValueListAsMessagePart(key: String, values : List<String>, elementName = 'Value') {
    var elements = values.map(\v -> {
      return new XmlElement(elementName).withSimpleValue(XmlSimpleValue.makeStringInstance(v))
    })

    addMessagePart(key, elements)
  }

}