package gw.api.claim

uses com.google.common.collect.Multimaps
uses com.google.common.collect.TreeMultimap
uses gw.api.policy.refresh.RefreshFromPolicyFetcher
uses gw.api.policy.refresh.RefreshFromPolicySummaryFetcher
uses gw.api.policy.refresh.RefreshPolicyFetcher
uses gw.pl.persistence.core.Bundle
uses gw.plugin.pcintegration.pc1000.PolicySearchPCPlugin

uses java.util.Set
uses java.util.Collection

@Export
class FnolServiceRequestHelper {

  private construct() { }

  /**
   * Clean the service requests that where added and then removed from the new claim. Also,
   * runs the finish setup for all of the claim service requests that will be committed with
   * the new claim
   */
  static function cleanAndFinishServiceRequests(wizard: NewClaimWizardInfo) {
    wizard.Claim.cleanAndFinishServiceRequests(wizard.UnusedServiceRequests, \ sr -> {
      wizard.VehicleIncidentOtherServiceRequests.remove(sr)
      wizard.PropertyOtherServiceRequests.remove(sr)
    })
  }

  /**
   * Removes the newly created services associated to the given incident 
   * when is being removed from the claim
   */
  static function removeIncidentServiceRequests(incident: Incident, wizard: NewClaimWizardInfo = null) {
    
    incident.ServiceRequests.each(\ sr -> {
      if (wizard != null) {
        wizard.VehicleIncidentOtherServiceRequests.remove(sr)
        wizard.PropertyOtherServiceRequests.remove(sr)
      }
      sr.remove()
    })
  }

  /**
   * GINTEG-1462 : Below Method will fetch the Legacy Policy Details and populates RefreshPolicyFetcher Object and returns the same.
   * @param claim
   * @return RefreshPolicyFetcher
   */
  static function createLegacyRefreshPolicyFetcher_TDIC(claim : Claim) : RefreshPolicyFetcher {
    var policySummary : PolicySummary = null
    var criteria = new PolicySearchCriteria()
    criteria.LossDate = claim.LossDate
    criteria.PolicyNumber = claim.Policy.PolicyNumber
    criteria.PolicyType = claim.Policy.PolicyType
    criteria.ClaimType_TDIC = claim.Type_TDIC

    policySummary = new PolicySearchPCPlugin().searchPolicies(criteria)?.Summaries?.first()

    if (policySummary != null) {
      policySummary.LossDate = criteria.LossDate
      policySummary.ContentExtensions.selectMatchingRiskUnits(claim.Policy)
      return new RefreshFromPolicySummaryFetcher(policySummary, true)
    }
    return null
  }
}
