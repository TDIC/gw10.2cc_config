package gw.acc.icdtransitionto10

enhancement ICDScriptParametersEnhancement: ScriptParameters {
  public static property get ICD9FileLocation_Ext(): String {
    return ScriptParameters.getParameterValue("ICD9FileLocation_Ext") as String
  }

  public static property get ICD10FileLocation_Ext(): String {
    return ScriptParameters.getParameterValue("ICD10FileLocation_Ext") as String
  }

  public static property get LoadICD9Flag_Ext(): Boolean {
    return ScriptParameters.getParameterValue("LoadICD9Flag_Ext") as Boolean
  }
}
