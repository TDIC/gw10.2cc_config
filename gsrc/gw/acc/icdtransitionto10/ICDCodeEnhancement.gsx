package gw.acc.icdtransitionto10

/**
 * An enhancement for ICDCode.
 */
enhancement ICDCodeEnhancement : entity.ICDCode {
  @Returns("A URL describing this code")
  property get ReferenceURL() : String {
    switch (this.ICDEdition_Ext) {
      case TC_ICD10:
        return "http://www.icd10data.com/Codes/${this.CodeTrimmed}"
      case TC_ICD9:
        return "http://www.icd9data.com/Search/?q=${this.Code}"
      default:
        return "http://apps.who.int/classifications/icd/en/"
    }
  }
}
