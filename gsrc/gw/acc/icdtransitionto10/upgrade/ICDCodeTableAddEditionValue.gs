package gw.acc.icdtransitionto10.upgrade

uses gw.api.database.Relop
uses gw.api.database.upgrade.after.AfterUpgradeVersionTrigger
uses gw.entity.IEntityPropertyInfo

/**
 * This version trigger adds a default value of 10 to existing entries in the
 * ICDCode table. (ClaimCenter 8.0.1+ ships with ICD-10 codes.)
 */
class ICDCodeTableAddEditionValue extends AfterUpgradeVersionTrigger {
  construct() {
    // 175 is the version number from extensions.properties
    super(175)
  }

  override property get Description() : String {
    return "Inserts value of 10 into ICDEdition_Ext Column on ICDCode Table"
  }

  override function execute() {
    var col = ICDCode#ICDEdition_Ext.PropertyInfo as IEntityPropertyInfo
    var icdtable = getTable(ICDCode).update()
    icdtable.Query.compare(ICDCode#ICDEdition_Ext, Relop.Equals, null)
    icdtable.set(col, ICDEdition_Ext.TC_ICD10)
    icdtable.execute()
  }
}