package gw.acc.adminaudit.plugin

uses gw.plugin.messaging.MessageTransport
uses gw.xml.XmlElement

/**************************************************************************************
 *  AdminAuditTransportImpl is provided as the default integration plugin for the admin auditing functionality
 ****************************************************************************************/
class AdminAuditTransportImpl implements MessageTransport {
  construct() {
  }
  private static var _LOGGER = org.slf4j.LoggerFactory.getLogger("gw.acc.adminaudit.plugin.AdminAuditTransportImpl")
  override function resume() {
    //This plugin does not perform any task in this method
  }

  override property  set DestinationID(p0: int) {
    //This plugin does not perform any task in this method
  }

  override function shutdown() {
    //This plugin does not perform any task in this method
  }

  override function suspend() {
    //This plugin does not perform any task in this method
  }

  /**************************************************************************************
   * In this default admin message handler, the audit record is persisted to the
   * ClaimCenter database.
   * If an external destination is required, create an alternate plugin with the appropriate
   * send function and register the plugin
   *************************************************************************************/
  override function send(message: Message, transformedPayload: String) {
    // persist the changes to the DB using a custom entity
    var errorMessage: String
    try {

      var x = XmlElement.parse(transformedPayload);
      var trigger = x.getAttributeValue("triggerEvent")

      if (_LOGGER.isDebugEnabled()) {
        _LOGGER.debug("Processing message: " + transformedPayload)
      }
      else {
        _LOGGER.info("Processing Audit message: " + trigger)
      }

      for (node in x.$Children) {
        if (_LOGGER.DebugEnabled)
          _LOGGER.info("Processing Node:" + node.asUTFString())

        var entryXML = gw.acc.adminaudit.gxmodel.adminaudit_ext_model.AdminAudit_Ext.parse(node.asUTFString());
        var entry = new AdminAudit_Ext(message)
        entry.TriggerEventName = trigger
        entry.ModifiedObjectName = entryXML.ModifiedObjectName
        entry.ModifiedEntityPublicID = entryXML.ModifiedEntityPublicID
        entry.ModifiedEntityName = entryXML.ModifiedEntityName
        entry.ModifiedByUserName = entryXML.ModifiedByUserName
        entry.ModifiedByUserID = entryXML.ModifiedByUserID
        entry.ModifiedDate = entryXML.ModifiedDate
        entry.ModifiedFieldName = entryXML.ModifiedFieldName
        entry.OriginalValue = entryXML.OriginalValue
        entry.NewValue = entryXML.NewValue
        entry.ModifiedEntityID = entryXML.ModifiedEntityID
        print(message)
      }
    } catch (e: java.lang.Throwable) {
      errorMessage = e.Message?:""+e.Message+"\n"
      _LOGGER.error("Error with Saving Admin Audit to Databse:" + e.getMessage(), e);
    }

    if (errorMessage == null) {
      _LOGGER.info("Saved Entries to database")
      try {
        message.reportAck()
      } catch (t: java.lang.Throwable) {
        _LOGGER.error("Error acknowledging Admin Audit message", t)
      }
    } else {
      message.reportError(typekey.ErrorCategory.TC_ADMINAUDIT_ERR_EXT)
      message.ErrorDescription = errorMessage
    }
  }
}
