package gw.plugin.pcintegration.pc1000.mapping

/**
 * GINTEG-1178 : This DTO will hold Legacy Risk Unit data from Legacy Data Mart
 */
class BOPLegacyRUMapper_TDIC {

  var buildingnumber : 	int as BUILDINGNUMBER
  var locationnumber: int as LOCATIONNUMBER
  var primarylocation : int as PRIMARYLOCATION
  var addresstype : String as ADDRESSTYPE
  var pcloc_addressline1 : String as PCLOC_ADDRESSLINE1
  var pcloc_addressline2 : String as PCLOC_ADDRESSLINE2
  var pcloc_city : String as PCLOC_CITY
  var pcloc_state : String as PCLOC_STATE
  var pcloc_postalcode : String as PCLOC_POSTALCODE
  var riskunits_description : String as RISKUNITS_DESCRIPTION
  var riskunits_runumber : int as RISKUNITS_RUNUMBER

  var coverages : List<BOPLegacyCovDataMapper_TDIC> as COVERAGES

}