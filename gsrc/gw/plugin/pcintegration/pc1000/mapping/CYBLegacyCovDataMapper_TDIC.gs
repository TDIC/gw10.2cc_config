package gw.plugin.pcintegration.pc1000.mapping

class CYBLegacyCovDataMapper_TDIC {

  var covpatterncode : String as COVPATTERNCODE
  var publicid : String as PUBLICID
  var policynumber : String as POLICYNUMBER
  var lob : String as LOB
  var source_system : String as SOURCE_SYSTEM
  var cccov_effectivedate : Date as CCCOV_EFFECTIVEDATE
  var cccov_expirationdate : Date as CCCOV_EXPIRATIONDATE
  var covTerms : List<CYBLegacyCovTermDataMap_TDIC> as COVTERMS

}