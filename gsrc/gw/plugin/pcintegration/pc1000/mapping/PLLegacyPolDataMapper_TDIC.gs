package gw.plugin.pcintegration.pc1000.mapping

/**
 *
 */

class PLLegacyPolDataMapper_TDIC {

  var addresstype : String as ADDRESSTYPE
  var addressline1 : String as ADDRESSLINE1
  var addressline2 : String as ADDRESSLINE2
  var city : String as CITY
  var state : String as STATE
  var postalcode : String as POSTALCODE
  var feinofficialid : String as FEINOFFICIALID
  var cccontact_name : String as CCCONTACT_NAME
  var cccontact_lastname : String as CCCONTACT_LASTNAME
  var cccontact_firstname : String as CCCONTACT_FIRSTNAME
  var cccontact_orgname : String as CCCONTACT_ORGNAME
  var cccontact_taxid : String as CCCONTACT_TAXID
  var cccontact_vendornumber : String as CCCONTACT_VENDORNUMBER
  var officialids_officialidinsuredandtype : String as OFFICIALIDS_OFFICIALIDINSUREDANDTYPE
  var officialids_officialidtype : String as OFFICIALIDS_OFFICIALIDTYPE
  var contacttype : String as CONTACTTYPE
  var officialids_officialidvalue : String as OFFICIALIDS_OFFICIALIDVALUE
  var primarylocation : int as PRIMARYLOCATION
  var ccpolicy_currency : String as CCPOLICY_CURRENCY
  var ccpolicy_effectivedate : Date as CCPOLICY_EFFECTIVEDATE
  var ccpolicy_expirationdate : Date as CCPOLICY_EXPIRATIONDATE
  var ccpolicy_policynumber : String as CCPOLICY_POLICYNUMBER
  var ccpolicy_policysuffix : String as CCPOLICY_POLICYSUFFIX
  var ccpolicy_policytype : String as CCPOLICY_POLICYTYPE
  var ccpolicy_producercode : String as CCPOLICY_PRODUCERCODE
  var ccpolicy_status : String as CCPOLICY_STATUS
  var ccpolicy_totalvehicles : String as CCPOLICY_TOTALVEHICLES
  var ccpolicy_underwrittingco : String as CCPOLICY_UNDERWRITTINGCO
  var ccpolicy_industrycode_tdic : String as CCPOLICY_INDUSTRYCODE_TDIC
  var riskunits_description : String as RISKUNITS_DESCRIPTION
  var riskunits_runumber : int as RISKUNITS_RUNUMBER

  public var Coverages : List<PLLegacyCovDataMapper_TDIC> as COVERAGES

}