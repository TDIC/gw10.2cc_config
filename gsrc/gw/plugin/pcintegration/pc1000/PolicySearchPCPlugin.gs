package gw.plugin.pcintegration.pc1000

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
uses gw.pl.currency.MonetaryAmount
uses gw.plugin.InitializablePlugin
uses gw.plugin.pcintegration.pc1000.mapping.BOPLegacyCovDataMapper_TDIC
uses gw.plugin.pcintegration.pc1000.mapping.BOPLegacyCovTermDataMap_TDIC
uses gw.plugin.pcintegration.pc1000.mapping.BOPLegacyPolDataMapper_TDIC
uses gw.plugin.pcintegration.pc1000.mapping.BOPLegacyRUMapper_TDIC
uses gw.plugin.pcintegration.pc1000.mapping.CYBLegacyCovDataMapper_TDIC
uses gw.plugin.pcintegration.pc1000.mapping.CYBLegacyCovTermDataMap_TDIC
uses gw.plugin.pcintegration.pc1000.mapping.CYBLegacyPolDataMapper_TDIC
uses gw.plugin.pcintegration.pc1000.mapping.PLLegacyCovDataMapper_TDIC
uses gw.plugin.pcintegration.pc1000.mapping.PLLegacyCovTermDataMap_TDIC
uses gw.plugin.pcintegration.pc1000.mapping.PLLegacyPolDataMapper_TDIC
uses gw.plugin.policy.search.IPolicySearchAdapter
uses wsi.remote.gw.webservice.pc.pc1000.ccpolicysearchintegration.CCPolicySearchIntegration
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCContact_AllAddresses
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCContact_ContactCompany
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCContact_PrimaryAddress
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCContact_PrimaryContact
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCCoverage_CovTerms
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCLocationBasedRU_Building
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCLocationBasedRU_PolicyLocation
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCPolicyLocation_Address
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCPolicyLocation_Buildings
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCPolicy_Agent
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCPolicy_Coverages
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCPolicy_CoveredParty
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCPolicy_Insured
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCPolicy_Other
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCPolicy_PolicyLocations
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCPolicy_RiskUnits
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.CCRiskUnit_Coverages
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.Envelope_CCAddress
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.Envelope_CCBuilding
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.Envelope_CCContact
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.Envelope_CCPolicy
uses wsi.remote.gw.webservice.pc.pc1000.entities.anonymous.elements.Envelope_CCPolicyLocation
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCAddress
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCBuilding
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCBuildingRU
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCClassificationCovTerm
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCCompany
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCContact
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCCovTerm
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCFinancialCovTerm
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCGeneralLiabilityRU
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCNumericCovTerm
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCPCFilteringCriteria
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCPerson
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCPolicyCoverage
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCPolicyLocation
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCPolicySummaryProperty
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCPropertyCoverage
uses wsi.remote.gw.webservice.pc.pc1000.entities.types.complex.CCPropertyRU
uses wsi.remote.tdic.webservice.bc.policybilling.tdic_policybillingapi.TDIC_PolicyBillingAPI
uses wsi.remote.tdic.webservice.bc.policybilling.tdic_policybillingapi.anonymous.elements.TDIC_PolicyBillingRequest_PolicyNumbersList
uses wsi.remote.tdic.webservice.bc.policybilling.tdic_policybillingapi.types.complex.TDIC_PolicyBillingRequest
uses java.math.BigDecimal
uses java.sql.Array
uses java.sql.Connection
uses java.sql.PreparedStatement
uses java.sql.ResultSet

/**
 * Implementation of the PolicySearchAdapter that calls into PC.
 */
@Export
class PolicySearchPCPlugin implements IPolicySearchAdapter, InitializablePlugin {

  static final var unsupportedPolicyTypes = new HashSet<String>() { PolicyType.TC_FARMOWNERS.Code,
                                                                    PolicyType.TC_PROF_LIABILITY.Code,
                                                                    PolicyType.TC_TRAVEL_PER.Code }

  private static var _businessowners = "BusinessOwners"
  private static var _commercialproperty = "CommercialProperty"
  public static final var DATAMART_URL : String = "DataMartDBURL"
  private  var LEGACY_POLICY_SEARCH_QRY1 : String = " Offering_TDIC in ("
  private var LEGACY_POLICY_SEARCH_QRY2 : String =  ") and EffectiveDate <= ? and ExpirationDate > ?"

  /**
   * Retrieves the policy indicated by policySummary from the PC instance.
   */
  override function retrievePolicyFromPolicySummary( policySummary : PolicySummary ) : PolicyRetrievalResultSet {
    if( policySummary.PolicyType != null && !isSupportedPolicyType( policySummary.PolicyType ) ) {
      throw new DisplayableException(DisplayKey.get("Java.PolicyRefresh.UnsupportedPolicyType", policySummary.PolicyType))
    }

    var pcFilter = new CCPCFilteringCriteria()
    //if you can't select risk units for the specific policySummary, add all risk units to the summary
    //else filter the risk units by selection
    if(!policySummary.CanSelectRiskUnits){
      if(policySummary.Vehicles!=null){
        policySummary.Vehicles.each( \ vehicle -> vehicle.setFieldValue("Selected",true))
      }
      if(policySummary.Properties!=null){
        policySummary.Properties.each( \ aProperty -> aProperty.setFieldValue("Selected",true))
      }
    }else{
      pcFilter.PolicySystemIDs.Entry = createRiskUnitFilter(policySummary).toList()
    }
    return retrievePolicy( policySummary.PolicyNumber, policySummary.LossDate, pcFilter, policySummary )
  }

  /**
   * Retrieves the policy again from the PC instance.
   */
  override function retrievePolicyFromPolicy( policy : Policy ) : PolicyRetrievalResultSet   {
    if( policy.PolicyType != null && !isSupportedPolicyType( policy.PolicyType ) )    {
      throw new DisplayableException( DisplayKey.get("Java.PolicyRefresh.UnsupportedPolicyType", policy.PolicyType))
    }
    return retrievePolicy( policy.PolicyNumber, policy.Claim.LossDate, new CCPCFilteringCriteria(), null)
  }

  /**
   * Search for policies on the PC instance given the search criteria.
   */
  override function searchPolicies( criteria : PolicySearchCriteria ) : PolicySearchResultSet   {
    if( criteria.PolicyType != null && !isSupportedPolicyType( criteria.PolicyType ) )    {
      throw new DisplayableException(DisplayKey.get("Java.PolicyRefresh.UnsupportedPolicyType", criteria.PolicyType))
    }

    var resultSet = new PolicySearchResultSet()
    if((criteria.PolicyNumber != null ? criteria.PolicyNumber.Numeric : true)) {
      var pcCriteria = PolicySearchConverter.INSTANCE.createPCSearchCriteria(criteria)

      // Just create an empty filter by default
      var pcFilter = new CCPCFilteringCriteria()

      var pcSummaries = PolicySearchService.searchForPolicies(pcCriteria, pcFilter)
      //GINTEG-223: If the PolicyType is 'BusinessOwners' then mapping the PolicyType to 'CommercialProperty'      
      var ccSummaries = PolicySearchConverter.INSTANCE.convertPCPolicySummary(pcSummaries)
      ccSummaries.each(\s -> {
        s.LossDate = criteria.LossDate
      })

      pcSummaries.each(\pc -> {
        var ccSummary = ccSummaries.firstWhere(\cc -> cc.PolicyNumber == pc.PolicyNumber)
        if (ccSummary != null) {
          ccSummary.AsOfDateUsedToSearch_TDIC = pc.AsOfDateUsedToSearch_TDIC
          ccSummary.RetroDate_TDIC = pc.PriorActs_TDIC
        }
      })
      //US1137 - Kesava Tavva - Filter PolicySummary list by its PaidThroughDate by retrieving it from BillingCenter.
      //GINTEG-223 : Added a condition to check 'Paid Through Date' only if the LOB is 'Worker's Compensation'
      //US1137 - Kesava Tavva - Filter PolicySummary list by its PaidThroughDate by retrieving it from BillingCenter.
      if (criteria.PolicyType == PolicyType.TC_WC7WORKERSCOMP and criteria.LossDate != null && ccSummaries.HasElements) {
        ccSummaries = filterByPaidThroughDate(ccSummaries, criteria.LossDate)
      }
      //GINTEG-223: If the PolicyType is 'BusinessOwners' then mapping the PolicyType to 'CommercialProperty'      
      resultSet.Summaries = ccSummaries
    }
    if ((criteria.PolicyNumber != null ? (not criteria.PolicyNumber.Numeric) : true) and
        (criteria.PolicyType == PolicyType.TC_BUSINESSOWNERS or criteria.PolicyType == PolicyType.TC_GENERALLIABILITY)) {
      //GINTEG-1048, GINTEG-1178 : Search for Policies from Legecy Datamart
      var _datamartDBURL = PropertyUtil.getInstance().getProperty(DATAMART_URL)
      // Get a connection from DatabaseManager using the Datamart DB URL
      var _con = DatabaseManager.getConnection(_datamartDBURL)
      var _preStatement : PreparedStatement
      if(criteria.PolicyType == PolicyType.TC_GENERALLIABILITY and
          (criteria.ClaimType_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY or criteria.ClaimType_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE
          or criteria.ClaimType_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES or (criteria.ClaimType_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY and
          criteria.CL_CoverageType_TDIC == CL_CoverageType_TDIC.TC_THIRDPARTY))) {
        _preStatement = polSearchPrepStatemntBuilderForPL_TDIC(criteria, _con)
      } else {
        _preStatement = polSearchPrepStatemntBuilder_TDIC(criteria, _con)
      }

      var _result = _preStatement.executeQuery()
      var datamartPolSummaries = new ArrayList<PolicySummary>()
      var stagingList = new ArrayList<PolicySummary>()
      while (_result.next()) {
        var datamartPolSummary = new PolicySummary()
        datamartPolSummary.AddressLine1 = _result.getString("AddressLine1")
        datamartPolSummary.City = _result.getString("City")
        datamartPolSummary.EffectiveDate = _result.getDate("EffectiveDate")
        datamartPolSummary.ExpirationDate = _result.getDate("ExpirationDate")
        datamartPolSummary.InsuredName = _result.getString("InsuredName")
        datamartPolSummary.PolicyNumber = _result.getString("PolicyNumber_TDIC")
        datamartPolSummary.AS400PolicyNumber_TDIC = _result.getString("PolicyNumber")
        datamartPolSummary.PolicyType = (_result.getString("PolicyType") == "CyberLiability_TDIC" ? PolicyType.TC_GENERALLIABILITY : typekey.PolicyType.get(_result.getString("PolicyType")))       
        datamartPolSummary.PostalCode = _result.getString("PostalCode")
        datamartPolSummary.ProducerCode = _result.getString("ProducerCode")
        datamartPolSummary.State = typekey.State.get(_result.getString("State"))
        datamartPolSummary.Status = typekey.PolicyStatus.get(_result.getString("Status"))
        datamartPolSummary.RetroDate_TDIC = _result.getDate("RetroDate")
        datamartPolSummary.EREFlag_TDIC = (_result.getInt("EREFlag") as Boolean)
        if (_result.getString("OFFERING_TDIC") != null) {
          switch (_result.getString("OFFERING_TDIC")) {
            case "BOP":
              datamartPolSummary.OFFERING_TDIC = Offering_TDIC.TC_BOP.Description
              break
            case "LRP":
              datamartPolSummary.OFFERING_TDIC = Offering_TDIC.TC_LRP.Description
              break
            case "PL-CM":
              datamartPolSummary.OFFERING_TDIC = Offering_TDIC.TC_PROFLIABCLAIMSMADE.Description
              break
            case "PL-OOC":
              datamartPolSummary.OFFERING_TDIC = Offering_TDIC.TC_PROFLIABOCCURENCE.Description
              break
            case "CYBER LIABILITY":
              datamartPolSummary.OFFERING_TDIC = Offering_TDIC.TC_CYBERLIABILITY.Description
              break
          }
        }
        datamartPolSummary.legacyPolicyInd_TDIC = true
        datamartPolSummary.ReportedDate_TDIC = criteria.ReportedDate_TDIC
        datamartPolSummary.EPLIFlag_TDIC = _result.getBoolean("EPLIFlag")
        datamartPolSummary.IdentityTheftFlag_TDIC = _result.getBoolean("IdentityTheftFlag")
        datamartPolSummary.PLFlag_TDIC = _result.getBoolean("PLFlag")
        datamartPolSummary.BLFlag_TDIC = _result.getBoolean("BLFlag")

      // GWPS-809 - Modified query to get results in the order of location number
        if(criteria.PolicyType == PolicyType.TC_BUSINESSOWNERS) {
          var _propsPreStatement = _con.prepareStatement("select * from [dbo].[BOP_PolicyRetrieve] where CCPolicy_PolicyNumber = ? and CCPolicy_EffectiveDate <= ? and CCPolicy_ExpirationDate >= ? order by CCPolicyLocation_LocationNumber")
          _propsPreStatement.setInt(1, datamartPolSummary.AS400PolicyNumber_TDIC.toInt())
          _propsPreStatement.setDate(2, criteria.LossDate.toSQLDate())
          _propsPreStatement.setDate(3, criteria.LossDate.toSQLDate())

          var _propsResult = _propsPreStatement.executeQuery()

          var propCount = 1
          while (_propsResult.next()) {
            var polSumProps = new PolicySummaryProperty()
            polSumProps.PolicySystemId = (propCount as String)
            polSumProps.AddressLine1 = _propsResult.getString("PCLoc_AddressLine1")
            polSumProps.AddressLine2 = _propsResult.getString("PCLoc_AddressLine2")
            polSumProps.City = _propsResult.getString("PCLoc_City")
            polSumProps.BuildingNumber = "1"
            polSumProps.PropertyNumber = propCount
            polSumProps.Address = _propsResult.getString("PCLoc_AddressLine1")
            polSumProps.Location = (propCount as String)
            datamartPolSummary.addToProperties(polSumProps)
            propCount++
          }
        }
        datamartPolSummaries.add(datamartPolSummary)
      }
      stagingList.addAll(datamartPolSummaries)
      // Add Logic related to PL-CM similar to what we did in PC
      if(datamartPolSummaries.Count == 1) {
        datamartPolSummaries?.each(\elt -> resultSet.addToSummaries(elt))
      } else {
        if (criteria.ClaimType_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY or criteria.ClaimType_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE or
            criteria.ClaimType_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES or (criteria.ClaimType_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY
            and criteria.CL_CoverageType_TDIC == CL_CoverageType_TDIC.TC_THIRDPARTY)) {
          datamartPolSummaries?.each(\datamartPolSumry -> {
            if(stagingList.hasMatch(\stg -> stg.PolicyNumber == datamartPolSumry.PolicyNumber)){
              resultSet.addToSummaries(stagingList.where(\stg -> stg.PolicyNumber == datamartPolSumry.PolicyNumber).sortByDescending(\sortBy -> sortBy.ExpirationDate).first())
              stagingList.removeWhere(\stg -> stg.PolicyNumber == datamartPolSumry.PolicyNumber)
            }
          })
        } else {
          datamartPolSummaries?.each(\elt -> resultSet.addToSummaries(elt))
        }
      }
    }
    return resultSet
  }

  /**
   * Retrieves the policy summary for the policy from the PC instance.
   */
  override function retrievePolicySummaryFromPolicy(policy : Policy) : PolicySummary {
    var criteria = new PolicySearchCriteria()
    criteria.LossDate = policy.Claim.LossDate
    criteria.PolicyNumber = policy.PolicyNumber

    var results = searchPolicies(criteria)
    var numResults = results.Summaries.Count
    if (numResults == 0)
      return null
    var policySummary = results.Summaries[0]
    policySummary.LossDate = criteria.LossDate
    return policySummary
  }

  // create the list of risk unit psids to pass to the policy system as a filter
  private function createRiskUnitFilter(policySummary : PolicySummary) : String[] {
    // set the policy system id filter based on policySummary selections
    var psidFilter = new ArrayList<String>()
    if(policySummary.getVehicles()!=null){
      for (var vehicle in policySummary.Vehicles) {
        if (vehicle.Selected){
          psidFilter.add(vehicle.PolicySystemId)
        }
      }
    }
    if(policySummary.getProperties()!=null){
      for (var aProperty in policySummary.Properties) {
        if (aProperty.Selected){
          psidFilter.add(aProperty.PolicySystemId)
        }
      }
    }

    return psidFilter?.toTypedArray()
  }

  // lazily initializes the policy search service

  private property get PolicySearchService() : CCPolicySearchIntegration  {
    return new CCPolicySearchIntegration()
  }

  /**
   * retrieves the policy indicated by the policynumber and lossDate
   */
  private function retrievePolicy( policyNumber : String, lossDate: Date, pcFilter : CCPCFilteringCriteria, policySummary : PolicySummary ) : PolicyRetrievalResultSet {
    if (lossDate == null) {
      throw new DisplayableException(DisplayKey.get("Java.PolicyItemHandler.LossDateRequired"))
    }
    // GINTEG-1178 : Retrive Policy Specific data from Legacy Data Mart and Populate Claim Center Specific Entities.
    if (policySummary != null and policySummary.legacyPolicyInd_TDIC) {
      var _datamartDBURL = PropertyUtil.getInstance().getProperty(DATAMART_URL)
      var _con = DatabaseManager.getConnection(_datamartDBURL)
      var legacyDataresultSet : ResultSet
      var _preStatement : PreparedStatement
      var _id = 0
      //GINTEG-1178 : Map DTO data to CC Specific Policy Retrieval Response Objects
      var dataMartEnvelope = new wsi.remote.gw.webservice.pc.pc1000.entities.Envelope()
      // GWPS-809 - Modified query to get results in the order of location number so that Risk unit number gets assigned according to the location number
      // and the coverage limits are set correctly for the location.
      var criteriaDate = (policySummary.ReportedDate_TDIC == null ? policySummary.LossDate.toSQLDate() : policySummary.ReportedDate_TDIC.toSQLDate())
      if (policySummary.PolicyType == PolicyType.TC_BUSINESSOWNERS) {
        _preStatement = _con.prepareStatement("select * from [dbo].[BOP_PolicyRetrieve] where CCPolicy_PolicyNumber = ? and CCPolicy_EffectiveDate <= ? and CCPolicy_ExpirationDate >= ? order by CCPolicyLocation_LocationNumber")
        _preStatement.setInt(1, policySummary.AS400PolicyNumber_TDIC.toInt())
        _preStatement.setDate(2, policySummary.LossDate.toSQLDate())
        _preStatement.setDate(3, policySummary.LossDate.toSQLDate())

        legacyDataresultSet = _preStatement.executeQuery()
        var mapper = new ArrayList<BOPLegacyPolDataMapper_TDIC>()
        var bopLegacyPolDataMapper_TDIC : BOPLegacyPolDataMapper_TDIC

        var riskUnitNumber = 1
        while (legacyDataresultSet.next()) {
          if (bopLegacyPolDataMapper_TDIC == null) {
            //GINTEG-1178 : Below Method will map BOP Legacy Data to DTO
            bopLegacyPolDataMapper_TDIC = mapLegacyBOPPolDataToDTO_TDIC(legacyDataresultSet, policySummary.PolicyNumber)
            mapper.add(bopLegacyPolDataMapper_TDIC)
          }
          var riskUnit = new BOPLegacyRUMapper_TDIC()
          bopLegacyPolDataMapper_TDIC.riskUnits.add(riskUnit)
          riskUnit.PCLOC_ADDRESSLINE1 = legacyDataresultSet.getString("PCLoc_AddressLine1")
          riskUnit.PCLOC_ADDRESSLINE2 = legacyDataresultSet.getString("PCLoc_AddressLine2")
          riskUnit.PCLOC_CITY = legacyDataresultSet.getString("PCLoc_City")
          riskUnit.PCLOC_STATE = legacyDataresultSet.getString("PCLoc_State")
          riskUnit.PCLOC_POSTALCODE = legacyDataresultSet.getString("PCLoc_PostalCode")
          riskUnit.BUILDINGNUMBER = legacyDataresultSet.getInt("CCBuilding_BuildingNumber")
          riskUnit.LOCATIONNUMBER = legacyDataresultSet.getInt("CCPolicyLocation_LocationNumber")
          riskUnit.PRIMARYLOCATION = legacyDataresultSet.getInt("CCPolicyLocation_PrimaryLocation")
          riskUnit.RISKUNITS_DESCRIPTION = legacyDataresultSet.getString("RiskUnits_Description")
          riskUnit.RISKUNITS_RUNUMBER = riskUnitNumber
          riskUnitNumber++
        }

        //GWPS-1852 - CP_Legacy Policy_Coverage details on the Policy Locations screen is not displayed when location numbers for policy in datamart are not starting from 1
        bopLegacyPolDataMapper_TDIC.riskUnits.each(\riskUnit -> {
          var _covPreStatement = _con.prepareStatement("select * from BOP_PolicyCoverage where POLNBR = ? and PRPNBR = ? and CCCov_EffectiveDate <= ? and CCCov_ExpirationDate >= ? Order By PRPNBR, CovPatternCode")
          _covPreStatement.setInt(1, policySummary.AS400PolicyNumber_TDIC.toInt())
          _covPreStatement.setInt(2, riskUnit.LOCATIONNUMBER)
          _covPreStatement.setDate(3, policySummary.LossDate.toSQLDate())
          _covPreStatement.setDate(4, policySummary.LossDate.toSQLDate())

          var legacyCovDataresultSet = _covPreStatement.executeQuery()
          while (legacyCovDataresultSet.next()) {
            //GINTEG-1178 : Below Method will map Legacy Coverage Data to DTO
            mapLegacyCovDataToDTO_TDIC(riskUnit, legacyCovDataresultSet, policySummary.PolicyNumber)
          }
        })

        var ccPolicy = new Envelope_CCPolicy()
        ccPolicy.Account = bopLegacyPolDataMapper_TDIC.CCPOLICY_POLICYNUMBER
        ccPolicy.Currency = bopLegacyPolDataMapper_TDIC.CCPOLICY_CURRENCY
        ccPolicy.setEffectiveDate(bopLegacyPolDataMapper_TDIC.CCPOLICY_EFFECTIVEDATE)
        ccPolicy.setExpirationDate(bopLegacyPolDataMapper_TDIC.CCPOLICY_EXPIRATIONDATE)
        ccPolicy.setOriginalEffectiveDate(bopLegacyPolDataMapper_TDIC.CCPOLICY_ORIGINALEFFECTIVEDATE)
        ccPolicy.PolicyNumber = bopLegacyPolDataMapper_TDIC.CCPOLICY_POLICYNUMBER
        ccPolicy.PolicySuffix = "1"
        ccPolicy.PolicyType = PolicyType.TC_BUSINESSOWNERS.Code
        ccPolicy.ProducerCode = bopLegacyPolDataMapper_TDIC.CCPOLICY_PRODUCERCODE
        ccPolicy.Status = "inforce"
        ccPolicy.TotalProperties = bopLegacyPolDataMapper_TDIC.CCPOLICY_TOTALPROPERTIES
        ccPolicy.TotalVehicles = 0
        ccPolicy.UnderwritingCo = bopLegacyPolDataMapper_TDIC.CCPOLICY_UNDERWRITTINGCO
        ccPolicy.IndustryCode_TDIC = bopLegacyPolDataMapper_TDIC.CCPOLICY_INDUSTRYCODE_TDIC
        var plHolderAddr = new CCAddress()
        plHolderAddr.AddressBookUID = "pc:1"
        plHolderAddr.AddressLine1 = bopLegacyPolDataMapper_TDIC.PCHOLDER_ADDRESSLINE1
        plHolderAddr.AddressLine2 = bopLegacyPolDataMapper_TDIC.PCHOLDER_ADDRESSLINE2
        plHolderAddr.AddressType = "Billing"
        plHolderAddr.City = bopLegacyPolDataMapper_TDIC.PCHOLDER_CITY
        plHolderAddr.Country = "US"
        plHolderAddr.PostalCode = bopLegacyPolDataMapper_TDIC.PCHOLDER_POSTALCODE
        plHolderAddr.State = bopLegacyPolDataMapper_TDIC.PCHOLDER_STATE
        plHolderAddr.ID = "_1"

        var polHolderContactEnv : Envelope_CCContact
        var plHolderAddrEnv = new Envelope_CCAddress(plHolderAddr)
        dataMartEnvelope.CCAddress.add(plHolderAddrEnv)

        if (bopLegacyPolDataMapper_TDIC.CONTACTTYPE == "Person") {
          var person = new CCPerson()
          person.ID = "_1"
          person.Name = bopLegacyPolDataMapper_TDIC.CCCONTACT_NAME
          person.FirstName = bopLegacyPolDataMapper_TDIC.CCCONTACT_FIRSTNAME
          person.LastName = bopLegacyPolDataMapper_TDIC.CCCONTACT_LASTNAME
          person.PolicySystemID = "default_data:1"
          person.Preferred = true
          person.TaxStatus = "unconfirmed"
          person.FEINOfficialID = bopLegacyPolDataMapper_TDIC.CCCONTACT_FEINOFFICIALID
          person.AllAddresses.add(plHolderAddrEnv)
          polHolderContactEnv = new Envelope_CCContact(person)
          var primAddrs = new CCContact_PrimaryAddress(plHolderAddrEnv)
          polHolderContactEnv.setPrimaryAddress_elem(primAddrs)
          person.setPrimaryAddress_elem(primAddrs)
        } else {
          var company = new CCCompany()
          company.ID = "_1"
          company.Name = bopLegacyPolDataMapper_TDIC.CCCONTACT_NAME
          company.PolicySystemID = "default_data:1"
          company.Preferred = true
          company.TaxStatus = "unconfirmed"
          company.FEINOfficialID = bopLegacyPolDataMapper_TDIC.CCCONTACT_FEINOFFICIALID
          company.AllAddresses.add(plHolderAddrEnv)
          polHolderContactEnv = new Envelope_CCContact(company)
          company.setContactCompany_elem(new CCContact_ContactCompany(polHolderContactEnv))
          var primAddrs = new CCContact_PrimaryAddress(plHolderAddrEnv)
          polHolderContactEnv.setPrimaryAddress_elem(primAddrs)
          company.setPrimaryAddress_elem(primAddrs)
        }

        polHolderContactEnv.Name = bopLegacyPolDataMapper_TDIC.CCCONTACT_NAME
        polHolderContactEnv.setPrimaryContact_elem(new CCContact_PrimaryContact(polHolderContactEnv))

        var allAddrs = new CCContact_AllAddresses(plHolderAddrEnv)
        polHolderContactEnv.setAllAddresses_elem({allAddrs})

        var primAddrs = new CCContact_PrimaryAddress(plHolderAddrEnv)
        polHolderContactEnv.setPrimaryAddress_elem(primAddrs)

        dataMartEnvelope.CCContact.add(polHolderContactEnv)
        ccPolicy.setInsured_elem(new CCPolicy_Insured(polHolderContactEnv))
        dataMartEnvelope.CCPolicy = ccPolicy

        var id = 1
        var ccPolicyLocations = new ArrayList<CCPolicy_PolicyLocations>()
        bopLegacyPolDataMapper_TDIC.riskUnits.each(\elt -> {
          var ccAddrs = new CCAddress()
          ccAddrs.AddressBookUID = id as String
          ccAddrs.AddressLine1 = elt.PCLOC_ADDRESSLINE1
          ccAddrs.AddressType = "business"
          ccAddrs.City = elt.PCLOC_CITY
          ccAddrs.Country = "US"
          ccAddrs.PostalCode = elt.PCLOC_POSTALCODE
          ccAddrs.State = elt.PCLOC_STATE
          ccAddrs.ID = ("_" + (id as String))

          var envCCAddrs = new Envelope_CCAddress(ccAddrs)
          dataMartEnvelope.CCAddress.add(envCCAddrs)

          var ccBldg = new CCBuilding()
          ccBldg.BuildingNumber = elt.RISKUNITS_RUNUMBER as String
          ccBldg.PolicySystemID = elt.RISKUNITS_RUNUMBER as String
          ccBldg.ID = ("_" + (id as String))

          var envBuilding1 = new Envelope_CCBuilding(ccBldg)
          dataMartEnvelope.CCBuilding.add(envBuilding1)

          var loc = new CCPolicyLocation()
          loc.ID = ("_" + (id as String))
          loc.LocationNumber = elt.LOCATIONNUMBER as String
          loc.PolicySystemID = ("_" + (id as String))
          loc.PrimaryLocation = true
          loc.ID = ("_" + (id as String))

          var ccPolLocatn = new Envelope_CCPolicyLocation(loc)
          var ccPolLocationAddr = new CCPolicyLocation_Address(envCCAddrs)
          ccPolLocatn.setAddress_elem(ccPolLocationAddr)
          ccPolLocatn.setPrimaryLocation(true)
          var allBuildings = new CCPolicyLocation_Buildings(envBuilding1)
          ccPolLocatn.setBuildings_elem({allBuildings})
          dataMartEnvelope.CCPolicyLocation.add(ccPolLocatn)

          ccPolicyLocations.add(new CCPolicy_PolicyLocations(ccPolLocatn))

          var riskUnit = new CCBuildingRU()
          riskUnit.PolicySystemID = ("_" + (id as String))
          riskUnit.RUNumber = elt.RISKUNITS_RUNUMBER
          ccPolicy.RiskUnits.add(new CCPolicy_RiskUnits(riskUnit))
          riskUnit.setBuilding_elem(new CCLocationBasedRU_Building(envBuilding1))
          riskUnit.setPolicyLocation_elem(new CCLocationBasedRU_PolicyLocation(ccPolLocatn))

          elt.COVERAGES?.each(\bopLegacyCov -> {
            var ccPropertyCov = new CCPropertyCoverage()
            var ruCov1 = new CCRiskUnit_Coverages(ccPropertyCov)
            ccPropertyCov.Currency = "usd"
            ccPropertyCov.EffectiveDate = bopLegacyCov.EFFECTIVEDATE
            ccPropertyCov.ExpirationDate = bopLegacyCov.EXPIRATIONDATE
            ccPropertyCov.PolicySystemID = "entity.BOPBuildingCov:" + ("_" + (id as String))
            ccPropertyCov.Type = bopLegacyCov.COVPATTERNCODE
            riskUnit.Coverages.add(ruCov1)
            switch(bopLegacyCov.COVPATTERNCODE){
              case "BOPBuildingOwnersLiabCov_TDIC" :
              case "BOPDentalGenLiabilityCov_TDIC" :
                bopLegacyCov?.COVTERMS?.each(\covTerm -> {
                  if (covTerm.COVTERMPATTERNCODE == "BOPBOLiabLimits_TDIC" or
                      covTerm.COVTERMPATTERNCODE =="BOPGLLimits_TDIC") {
                    var finCovTerm = new CCFinancialCovTerm()
                    finCovTerm.setFinancialAmount(new MonetaryAmount(new BigDecimal(covTerm.COVTERMSTRINGVALUE.split("/").first()), Currency.TC_USD))
                    var ccCovTerm = new CCCoverage_CovTerms(finCovTerm)
                    finCovTerm.CovTermOrder = 1
                    finCovTerm.CovTermPattern = covTerm.COVTERMPATTERNCODE
                    finCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(finCovTerm.CovTermPattern)
                    finCovTerm.PolicySystemID = ccPropertyCov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
                    ccPropertyCov.CovTerms.add(ccCovTerm)
                    ccPropertyCov.ExposureLimit = (ccPropertyCov.ExposureLimit == null ? finCovTerm.FinancialAmount : (ccPropertyCov.ExposureLimit < finCovTerm.FinancialAmount ? finCovTerm.FinancialAmount : ccPropertyCov.ExposureLimit))
                  }
                })
                break;

              case "BOPManuscriptEndorsement_TDIC":
                bopLegacyCov?.COVTERMS?.each(\covTerm -> {
                  if(covTerm.COVTERMPATTERNCODE == "BOPManuscriptType_TDIC" and covTerm.COVTERMVALUE != null) {
                    var numericCovTerm = new CCNumericCovTerm()
                    var ccCovTerm = new CCCoverage_CovTerms(numericCovTerm)
                    numericCovTerm.CovTermOrder = 1
                    numericCovTerm.CovTermPattern = covTerm.COVTERMPATTERNCODE
                    numericCovTerm.NumericValue = covTerm.COVTERMVALUE.toInt()
                    ccPropertyCov.CovTerms.add(ccCovTerm)
                  }
                  if(covTerm.COVTERMPATTERNCODE == "BOPManucriptDesc_TIDC" and covTerm.COVTERMSTRINGVALUE != null){
                    var numericCovTerm = new CCClassificationCovTerm()
                    var ccCovTerm = new CCCoverage_CovTerms(numericCovTerm)
                    numericCovTerm.CovTermOrder = 1
                    numericCovTerm.CovTermPattern = covTerm.COVTERMPATTERNCODE
                    numericCovTerm.Description = covTerm.COVTERMSTRINGVALUE
                    ccPropertyCov.CovTerms.add(ccCovTerm)
                  }
                })
                break;
              default:
                bopLegacyCov?.COVTERMS?.each(\covTerm -> {
                  if(covTerm.COVTERMVALUE != null) {
                    var finCovTerm = new CCFinancialCovTerm()
                    finCovTerm.setFinancialAmount(new MonetaryAmount(new BigDecimal(covTerm.COVTERMVALUE), Currency.TC_USD))
                    var ccCovTerm = new CCCoverage_CovTerms(finCovTerm)
                    finCovTerm.CovTermOrder = 1
                    finCovTerm.CovTermPattern = covTerm.COVTERMPATTERNCODE
                    var agModel = getModelAggregationForLegacyCovTerm_TDIC(finCovTerm.CovTermPattern)
                    if("" != agModel ){
                      finCovTerm.ModelAggregation = agModel
                    }
                    finCovTerm.PolicySystemID = ccPropertyCov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
                    ccPropertyCov.CovTerms.add(ccCovTerm)
                    ccPropertyCov.ExposureLimit = (ccPropertyCov.ExposureLimit == null ? finCovTerm.FinancialAmount : (ccPropertyCov.ExposureLimit < finCovTerm.FinancialAmount ? finCovTerm.FinancialAmount : ccPropertyCov.ExposureLimit))
                  }
                })
                break;
            }
          })
          _id++
        })
        ccPolicy.setPolicyLocations_elem(ccPolicyLocations)
        dataMartEnvelope.CCPolicy = ccPolicy
      } else if (policySummary.PolicyType == PolicyType.TC_GENERALLIABILITY and (not policySummary.OFFERING_TDIC.equalsIgnoreCase(Offering_TDIC.TC_CYBERLIABILITY.Description))) {
        _preStatement = _con.prepareStatement("select * from [dbo].[PL_PolicyRetrieve] where CCPolicy_PolicyNumber = ? and CCPolicy_EffectiveDate <= ? and CCPolicy_ExpirationDate >= ?")
        _preStatement.setInt(1, policySummary.AS400PolicyNumber_TDIC.toInt())
        _preStatement.setDate(2, policySummary.EffectiveDate.toSQLDate())
        _preStatement.setDate(3, policySummary.ExpirationDate.toSQLDate())

        legacyDataresultSet = _preStatement.executeQuery()
        var mapper = new ArrayList<PLLegacyPolDataMapper_TDIC>()
        var plLegacyPolDataMapper_TDIC : PLLegacyPolDataMapper_TDIC
        while (legacyDataresultSet.next()) {
          if (plLegacyPolDataMapper_TDIC == null) {
            //GINTEG-1178 : Below Method will map PL Legacy Data to DTO
            plLegacyPolDataMapper_TDIC = mapLegacyPLPolDataToDTO_TDIC(legacyDataresultSet, policySummary.PolicyNumber)
            mapper.add(plLegacyPolDataMapper_TDIC)
          }
        }
          var _covPreStatement = _con.prepareStatement("select * from PL_PolicyCoverage where PolicyNumber = ? and CCCov_EffectiveDate <= ? and CCCov_ExpirationDate >= ?")
          _covPreStatement.setInt(1, policySummary.AS400PolicyNumber_TDIC.toInt())
          _covPreStatement.setDate(2, policySummary.EffectiveDate.toSQLDate())
          _covPreStatement.setDate(3, policySummary.ExpirationDate.toSQLDate())
          var legacyCovDataresultSet = _covPreStatement.executeQuery()
          while (legacyCovDataresultSet.next()) {
            //GINTEG-1178 : Below Method will map Legacy Coverage Data to DTO
            mapLegacyPLCovDataToDTO_TDIC(plLegacyPolDataMapper_TDIC, legacyCovDataresultSet, policySummary.PolicyNumber)
          }
        mapPLDataFromDTOtoEnvelope_TDIC(dataMartEnvelope, plLegacyPolDataMapper_TDIC)

        // Map additional Insureds and Certfication Holder Details.
        _preStatement = _con.prepareStatement("select * from PL_PolicyCoverage_AddnlInsrds_CertfHoldrs where PolicyNumber = ?")
        _preStatement.setInt(1, policySummary.AS400PolicyNumber_TDIC.toInt())
        legacyDataresultSet = _preStatement.executeQuery()
        _id = 1
        while (legacyDataresultSet.next()) {
          mapPLAddntlInsrdDataFromDTOtoEnvelope_TDIC(dataMartEnvelope, legacyDataresultSet, "_"+(_id as String))
        }
      } else if (policySummary.PolicyType == PolicyType.TC_GENERALLIABILITY and policySummary.OFFERING_TDIC.equalsIgnoreCase(Offering_TDIC.TC_CYBERLIABILITY.Description)) {
        _preStatement = _con.prepareStatement("select * from [CYB_PolicyRetrieve] where CCPolicy_PolicyNumber = ? and CCPolicy_EffectiveDate <= ? and CCPolicy_ExpirationDate >= ?")
        _preStatement.setInt(1, policySummary.AS400PolicyNumber_TDIC.toInt())
        _preStatement.setDate(2, policySummary.EffectiveDate.toSQLDate())
        _preStatement.setDate(3, policySummary.ExpirationDate.toSQLDate())

        legacyDataresultSet = _preStatement.executeQuery()
        var cybLegacyPolDataMapper_TDIC : CYBLegacyPolDataMapper_TDIC
        while (legacyDataresultSet.next()) {
          if (cybLegacyPolDataMapper_TDIC == null) {
            //GINTEG-1178 : Below Method will map PL Legacy Data to DTO
            cybLegacyPolDataMapper_TDIC = mapLegacyCYBPolDataToDTO_TDIC(legacyDataresultSet, policySummary.PolicyNumber)
          }
        }
        var _covPreStatement = _con.prepareStatement("select * from CYB_PolicyCoverage where POLNBR = ? and CCCov_EffectiveDate <= ? and CCCov_ExpirationDate >= ?")
        _covPreStatement.setInt(1, policySummary.AS400PolicyNumber_TDIC.toInt())
        _covPreStatement.setDate(2, policySummary.EffectiveDate.toSQLDate())
        _covPreStatement.setDate(3, policySummary.ExpirationDate.toSQLDate())
        var legacyCovDataresultSet = _covPreStatement.executeQuery()
        while (legacyCovDataresultSet.next()) {
          //GINTEG-1178 : Below Method will map Legacy Coverage Data to DTO
          mapLegacyCYBCovDataToDTO_TDIC(cybLegacyPolDataMapper_TDIC, legacyCovDataresultSet, policySummary.PolicyNumber)
        }
        mapCYBDataFromDTOtoEnvelope_TDIC(dataMartEnvelope, cybLegacyPolDataMapper_TDIC)
      }

      var resultSet = new PolicyRetrievalResultSet()
      try {
        resultSet.Result = PolicySearchConverter.INSTANCE.convertPCPolicy(dataMartEnvelope.CCPolicy.$TypeInstance)
        // US22, robk: All Policy Contacts should have CM status of "Not linked to the Address Book"
        for (aPolicyContact in resultSet.Result.Contacts) {
          aPolicyContact.Contact.AddressBookUID = null
        }
      } catch (e : Throwable) {
        throw new DisplayableException(DisplayKey.get("Java.PolicyRefresh.ErrorRetrieving"), e)
      }
      if (PolicyStatus.TC_ARCHIVED == resultSet.Result.Status) {
        throw new DisplayableException(DisplayKey.get("Java.PolicyRefresh.PolicyIsArchived"))
      }
      resultSet.NotUnique = false
      return resultSet
      //return legacyDataresultSet
    } else {
      var resultSet = new PolicyRetrievalResultSet()
      try {
        var env : wsi.remote.gw.webservice.pc.pc1000.entities.Envelope
        if(policySummary.PolicyType == PolicyType.TC_WC7WORKERSCOMP) {
          env = PolicySearchService.retrievePolicy(policyNumber, lossDate, pcFilter)
        } else {
          env = PolicySearchService.retrievePolicy(policyNumber, (policySummary.AsOfDateUsedToSearch_TDIC == null ? lossDate : policySummary.AsOfDateUsedToSearch_TDIC), pcFilter)
        }
        resultSet.Result = PolicySearchConverter.INSTANCE.convertPCPolicy(env.CCPolicy.$TypeInstance)
        // US22, robk: All Policy Contacts should have CM status of "Not linked to the Address Book"
        for (aPolicyContact in resultSet.Result.Contacts) {
          aPolicyContact.Contact.AddressBookUID = null
        }
      } catch (e : Throwable) {
        throw new DisplayableException(DisplayKey.get("Java.PolicyRefresh.ErrorRetrieving"), e)
      }

      if (PolicyStatus.TC_ARCHIVED == resultSet.Result.Status) {
        throw new DisplayableException(DisplayKey.get("Java.PolicyRefresh.PolicyIsArchived"))
      }
      resultSet.NotUnique = false
      return resultSet
    }
  }

  private function isSupportedPolicyType(policyType: PolicyType): boolean {
    return !unsupportedPolicyTypes.contains(policyType.Code)
  }

  override property set Parameters(p0 : Map<Object, Object>) {

  }

  /**
   * US1137
   * 03/04/2015 Kesava Tavva
   *
   * Property to get instance of PolicyBillingService API
   */
  @Returns("TDIC_PolicyBillingAPI, Instance of TDIC_PolicyBillingAPI")
  private property get PolicyBillingService() : TDIC_PolicyBillingAPI {
    //create new ws client on each call
    return new TDIC_PolicyBillingAPI()
  }

  /**
   * US1137
   * 03/04/2015 Kesava Tavva
   * GW-445 11/09/2015 Praneeth
   * Retrieve Paid through dates, FirstInvoicePaidFull and UnpaidPolicyChangeInvoice from Billing Center for list of policies from Policy summary list.
   * Compare Loss date, Paidthroughdate, FirstInvoicePaidFull and UnpaidPolicyChangeInvoice to filter Policy summary search results.
   */
  @Param("ccSummaries","List of PolicySummary results")
  @Param("lossDate", "Claim loss date")
  @Returns("PolicySummary[], List of PolicySummary results filtered by comparing its PaidThroughDate from BillingCenter")
  @Throws(DisplayableException, "If communication error or any other SOAP problem occurs.")
  private function filterByPaidThroughDate(ccSummaries : PolicySummary[], lossDate : Date) : PolicySummary[] {
    var filteredSummaries = new ArrayList<PolicySummary>()
    try{
      var req = new TDIC_PolicyBillingRequest()
      req.PolicyNumbersList = new TDIC_PolicyBillingRequest_PolicyNumbersList()
      ccSummaries.each( \ summary -> { req.PolicyNumbersList.Entry.add(summary.PolicyNumber) })
      var response = PolicyBillingService.getPaidThroughDatesForPolicies(req)
      var ccSummary : PolicySummary
      for (result in response.PolicyBillingDTOList.Entry){
        ccSummary = ccSummaries.firstWhere( \ summary -> summary.PolicyNumber == result.PolicyNumber)
        ccSummary.PaidThruDate_TDIC = result.PaidThroughDate
        ccSummary.CancellationDate_TDIC = result.CancellationDate
        ccSummary.FirstInvoicePaidFull_TDIC = result.FirstInvoicePaidFull?YesNo.TC_YES:YesNo.TC_NO
        ccSummary.UnpaidPolicyChangeInvoice_TDIC = result.UnpaidPolicyChangeInvoice?YesNo.TC_YES:YesNo.TC_NO

        if(result.CancellationDate == null){
          if(ccSummary.LossDate <= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(true) and result.UnpaidPolicyChangeInvoice.equals(true)){
            filteredSummaries.add(ccSummary)
          }
          else if(ccSummary.LossDate <= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(true) and result.UnpaidPolicyChangeInvoice.equals(false)){
            filteredSummaries.add(ccSummary)
          }
          else if(ccSummary.LossDate <= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(false) and result.UnpaidPolicyChangeInvoice.equals(true)){
            filteredSummaries.add(ccSummary)
          }
          else if(ccSummary.LossDate <= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(false) and result.UnpaidPolicyChangeInvoice.equals(false)){
            filteredSummaries.add(ccSummary)
          }
          else if(ccSummary.LossDate >= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(false) and result.UnpaidPolicyChangeInvoice.equals(true)){
            filteredSummaries.add(ccSummary)
          }
          else if(ccSummary.LossDate >= result.PaidThroughDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(true) and result.UnpaidPolicyChangeInvoice.equals(true)){
            filteredSummaries.add(ccSummary)
          }
        }
        else if (ccSummary.LossDate < result.CancellationDate and ccSummary.EffectiveDate.compareIgnoreTime(result.EffectiveDate) ==0 and ccSummary.ExpirationDate.compareIgnoreTime(result.ExpirationDate) ==0 and result.FirstInvoicePaidFull.equals(true)){
          filteredSummaries.add(ccSummary)
        }
      }
    }catch(e : Throwable){
      throw new DisplayableException( DisplayKey.get("Java.PolicyRefresh.ErrorRetrievingPaidThroughDates", (e.Message)))
    }
    return filteredSummaries.toTypedArray()
  }

  /*
  * GINTEG-1178 : Below Method will build a query to Legacy Datamart and retrive policy Search Specific Data
   */
  function polSearchPrepStatemntBuilder_TDIC(criteria : PolicySearchCriteria , connection : Connection) : PreparedStatement {
    var prepStmnt : PreparedStatement
    var queryBuilder = new StringBuilder()
    var params = new ArrayList<String>()
    queryBuilder.append("select * from PolicySearchResponse where ")

    if (criteria.FirstName != null) {
      queryBuilder.append(" FirstName = ? and ")
      params.add(criteria.FirstName)
    }
    if (criteria.LastName != null) {
      queryBuilder.append(" LastName = ? and ")
      params.add(criteria.LastName)
    }
    if (criteria.PolicyNumber != null) {
      queryBuilder.append(" PolicyNumber = ? and ")      
      var policyNumber = criteria.PolicyNumber.split(" ").last()
      policyNumber = policyNumber.replace("-","").substring(0,7)
      params.add(policyNumber)
    }
    if (criteria.CompanyName != null) {
      queryBuilder.append(" InsuredName = ? and ")
      params.add(criteria.CompanyName)
    }
    switch (criteria.PolicyType) {
      case PolicyType.TC_BUSINESSOWNERS:
        if(criteria.ClaimType_TDIC == ClaimType_TDIC.TC_GENERALLIABILITY) {
          queryBuilder.append(LEGACY_POLICY_SEARCH_QRY1 + "?" + LEGACY_POLICY_SEARCH_QRY2)
          params.add("LRP")
        } else {
          queryBuilder.append(LEGACY_POLICY_SEARCH_QRY1 + "?,?" + LEGACY_POLICY_SEARCH_QRY2)
          params.add("BOP")
          params.add("LRP")
        }
        break
      case PolicyType.TC_GENERALLIABILITY:
        if (criteria.ClaimType_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY) {
          queryBuilder.append(LEGACY_POLICY_SEARCH_QRY1 + "?" + LEGACY_POLICY_SEARCH_QRY2)
          params.add("CYBER LIABILITY")
        } else if(criteria.ClaimType_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY or criteria.ClaimType_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE
          or criteria.ClaimType_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES) {
            queryBuilder.append(LEGACY_POLICY_SEARCH_QRY1 + "?" + ")")
            params.add("PL-CM")
        } else {
          queryBuilder.append(LEGACY_POLICY_SEARCH_QRY1 + "?,?" + LEGACY_POLICY_SEARCH_QRY2)
          params.add("PL-OOC")
          params.add("PL-CM")
        }
        break
      default:
        queryBuilder.append(LEGACY_POLICY_SEARCH_QRY1 + "?" + LEGACY_POLICY_SEARCH_QRY2)
        break

    }

    switch(criteria.ClaimType_TDIC) {
      case ClaimType_TDIC.TC_BUSINESSLIABILITY :
        queryBuilder.append(" and BLFlag=1 ")
        break
      case ClaimType_TDIC.TC_PROFESSIONALLIABILITY :
      case ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE :
        queryBuilder.append(" and PLFlag=1 ")
        break
      case ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY :
        queryBuilder.append(" and IdentityTheftFlag=1 ")
        break
    }

    prepStmnt = connection.prepareStatement(queryBuilder.toString())
    var i = 1
    params?.each(\param -> {
      prepStmnt.setString(i, param)
      i++
    })
    if(not (criteria.ClaimType_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY or criteria.ClaimType_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE
        or criteria.ClaimType_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES)){
      prepStmnt.setDate(i, criteria.LossDate.toSQLDate())
      prepStmnt.setDate(i + 1, criteria.LossDate.toSQLDate())
    }
    return prepStmnt
  }

  function polSearchPrepStatemntBuilderForPL_TDIC(criteria : PolicySearchCriteria , connection : Connection) : PreparedStatement {
    var prepStmnt : PreparedStatement
    var queryBuilder = new StringBuilder()
    var params = new ArrayList<String>()
    if (criteria.PolicyNumber != null) {
      queryBuilder.append("select top 1 * from PolicySearchResponse where ")
      queryBuilder.append(" PolicyNumber = ? and ")
      var policyNumber = criteria.PolicyNumber.split(" ").last()
      policyNumber = policyNumber.replace("-","").substring(0,7)
      params.add(policyNumber)
    } else {
      queryBuilder.append("select * from PolicySearchResponse where ")
    }

    if (criteria.FirstName != null) {
      queryBuilder.append(" FirstName = ? and ")
      params.add(criteria.FirstName)
    }
    if (criteria.LastName != null) {
      queryBuilder.append(" LastName = ? and ")
      params.add(criteria.LastName)
    }

    if (criteria.CompanyName != null) {
      queryBuilder.append(" InsuredName = ? and ")
      params.add(criteria.CompanyName)
    }
    if (criteria.ClaimType_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY) {
        queryBuilder.append(" Offering_TDIC = ? ")
          params.add("CYBER LIABILITY")
    } else if(criteria.ClaimType_TDIC == ClaimType_TDIC.TC_PROFESSIONALLIABILITY or criteria.ClaimType_TDIC == ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE
          or criteria.ClaimType_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES) {
            queryBuilder.append(" Offering_TDIC = ? ")
            params.add("PL-CM")
    } else {
        queryBuilder.append(" Offering_TDIC = ? ")
        params.add("PL-OOC")
        }

    switch(criteria.ClaimType_TDIC) {
      case ClaimType_TDIC.TC_BUSINESSLIABILITY :
        queryBuilder.append(" and BLFlag=1 ")
        break
      case ClaimType_TDIC.TC_PROFESSIONALLIABILITY :
      case ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE :
        queryBuilder.append(" and PLFlag=1 ")
        break
      case ClaimType_TDIC.TC_IDENTITYTHEFTRECOVERY :
        queryBuilder.append(" and IdentityTheftFlag=1 ")
        break
    }
    if(criteria.PolicyNumber != null){
      queryBuilder.append("order by ExpirationDate desc")
    }
    prepStmnt = connection.prepareStatement(queryBuilder.toString())
    var i = 1
    params?.each(\param -> {
      prepStmnt.setString(i, param)
      i++
    })
    return prepStmnt
  }

  /**
   * GINTEG-1178 : Below Method will map BOP Legacy Data to DTO
   * @param legacyDataresultSet
   * @return BOPLegacyPolDataMapper_TDIC
   */
  function mapLegacyBOPPolDataToDTO_TDIC(legacyDataresultSet : ResultSet, policyNumber : String) : BOPLegacyPolDataMapper_TDIC {
    var bopLegacyPolDataMapper_TDIC = new BOPLegacyPolDataMapper_TDIC()
    bopLegacyPolDataMapper_TDIC.riskUnits = new ArrayList<BOPLegacyRUMapper_TDIC>()
    bopLegacyPolDataMapper_TDIC.PCHOLDER_ADDRESSLINE1 = legacyDataresultSet.getString("PCHolder_AddressLine1")
    bopLegacyPolDataMapper_TDIC.PCHOLDER_ADDRESSLINE2 = legacyDataresultSet.getString("PCHolder_AddressLine2")
    bopLegacyPolDataMapper_TDIC.PCHOLDER_CITY = legacyDataresultSet.getString("PCHolder_City")
    bopLegacyPolDataMapper_TDIC.PCHOLDER_STATE = legacyDataresultSet.getString("PCHolder_State")
    bopLegacyPolDataMapper_TDIC.PCHOLDER_POSTALCODE = legacyDataresultSet.getString("PCHolder_PostalCode")
    bopLegacyPolDataMapper_TDIC.CCCONTACT_FEINOFFICIALID = legacyDataresultSet.getString("CCContact_FEINOfficialID")
    bopLegacyPolDataMapper_TDIC.CCCONTACT_NAME = legacyDataresultSet.getString("CCContact_Name")
    bopLegacyPolDataMapper_TDIC.CCCONTACT_TAXID = legacyDataresultSet.getString("CCContact_TaxID")
    bopLegacyPolDataMapper_TDIC.CCCONTACT_VENDORNUMBER = legacyDataresultSet.getString("CCContact_VendorNumber")
    bopLegacyPolDataMapper_TDIC.OFFICIALIDS_OFFICIALIDINSUREDANDTYPE = legacyDataresultSet.getString("OfficialIDs_OfficialIDInsuredAndType")
    bopLegacyPolDataMapper_TDIC.OFFICIALIDS_OFFICIALIDTYPE = legacyDataresultSet.getString("OfficialIDs_OfficialIDType")
    bopLegacyPolDataMapper_TDIC.CONTACTTYPE = legacyDataresultSet.getString("ContactType")
    bopLegacyPolDataMapper_TDIC.OFFICIALIDS_OFFICIALIDVALUE = legacyDataresultSet.getString("OfficialIDs_OfficialIDValue")
    bopLegacyPolDataMapper_TDIC.CCPOLICY_CURRENCY = legacyDataresultSet.getString("CCPolicy_Currency")
    bopLegacyPolDataMapper_TDIC.CCPOLICY_EFFECTIVEDATE = legacyDataresultSet.getDate("CCPolicy_EffectiveDate")
    bopLegacyPolDataMapper_TDIC.CCPOLICY_EXPIRATIONDATE = legacyDataresultSet.getDate("CCPolicy_ExpirationDate")
    bopLegacyPolDataMapper_TDIC.CCPOLICY_ORIGINALEFFECTIVEDATE = legacyDataresultSet.getDate("CCPolicy_OriginalEffectiveDate")
    bopLegacyPolDataMapper_TDIC.CCPOLICY_POLICYNUMBER = policyNumber
    bopLegacyPolDataMapper_TDIC.CCPOLICY_POLICYSUFFIX = legacyDataresultSet.getString("CCPolicy_PolicySuffix")
    bopLegacyPolDataMapper_TDIC.CCPOLICY_POLICYTYPE = PolicyType.TC_BUSINESSOWNERS.Code
    bopLegacyPolDataMapper_TDIC.CCPOLICY_PRODUCERCODE = legacyDataresultSet.getString("CCPolicy_ProducerCode")
    bopLegacyPolDataMapper_TDIC.CCPOLICY_STATUS = legacyDataresultSet.getString("CCPolicy_Status")
    bopLegacyPolDataMapper_TDIC.CCPOLICY_TOTALPROPERTIES = legacyDataresultSet.getInt("CCPolicy_TotalProperties")
    bopLegacyPolDataMapper_TDIC.CCPOLICY_TOTALVEHICLES = 0 /*legacyDataresultSet.getString("CCPolicy_TotalVehicles") == "" ? 0 :*/
    bopLegacyPolDataMapper_TDIC.CCPOLICY_UNDERWRITTINGCO = legacyDataresultSet.getString("CCPolicy_UnderwrittingCo")
    bopLegacyPolDataMapper_TDIC.CCPOLICY_INDUSTRYCODE_TDIC = legacyDataresultSet.getString("CCPolicy_IndustryCode_TDIC")
    bopLegacyPolDataMapper_TDIC.CONTACTTYPE = legacyDataresultSet.getString("ContactType")

    bopLegacyPolDataMapper_TDIC.CCCONTACT_LASTNAME = legacyDataresultSet.getString("CCContact_LastName")
    bopLegacyPolDataMapper_TDIC.CCCONTACT_FIRSTNAME = legacyDataresultSet.getString("CCContact_FirstName")
    bopLegacyPolDataMapper_TDIC.CCCONTACT_ORGNAME = legacyDataresultSet.getString("CCContact_OrgName")
    bopLegacyPolDataMapper_TDIC.LOCATIONNUMBER = legacyDataresultSet.getInt("CCPolicyLocation_LocationNumber")
    bopLegacyPolDataMapper_TDIC.PRIMARYLOCATION = legacyDataresultSet.getString("CCPolicyLocation_PrimaryLocation")

    return bopLegacyPolDataMapper_TDIC
  }

  /**
   * GINTEG-1178 : Below Method will map Legacy Coverage Data to DTO
   * @param riskUnit
   * @param legacyCovDataresultSet
   */
  function mapLegacyCovDataToDTO_TDIC(riskUnit: BOPLegacyRUMapper_TDIC, legacyCovDataresultSet : ResultSet, policyNumber : String) {
    var cov : BOPLegacyCovDataMapper_TDIC
    if (riskUnit.COVERAGES == null) {
      riskUnit.COVERAGES = new ArrayList<BOPLegacyCovDataMapper_TDIC>()
    }
    if (riskUnit.COVERAGES.hasMatch(\elt1 -> elt1.COVPATTERNCODE == legacyCovDataresultSet.getString("CovPatternCode"))) {
      cov = riskUnit.COVERAGES.firstWhere(\elt1 -> elt1.COVPATTERNCODE == legacyCovDataresultSet.getString("CovPatternCode"))
    }
    if (cov == null) {
      cov = new BOPLegacyCovDataMapper_TDIC()
      cov.COVPATTERNCODE = legacyCovDataresultSet.getString("CovPatternCode")
      cov.PUBLICID = legacyCovDataresultSet.getString("publicid")
      cov.LOCATIONID = legacyCovDataresultSet.getString("locationid")
      cov.LOB = legacyCovDataresultSet.getString("LOB")
      cov.POLNBR = policyNumber
      cov.PRPNBR = legacyCovDataresultSet.getInt("PRPNBR")
      cov.SOURCE_SYSTEM = legacyCovDataresultSet.getString("Source_System")
      riskUnit.COVERAGES.add(cov)
    }
    if (cov.COVTERMS == null and legacyCovDataresultSet.getString("CovValue") != null) {
      cov.COVTERMS = new ArrayList<BOPLegacyCovTermDataMap_TDIC>()
      mapBOPLegacyCovTermDataToDTO_TDIC(cov,legacyCovDataresultSet)
    } else if (legacyCovDataresultSet.getString("CovValue") != null) {
      mapBOPLegacyCovTermDataToDTO_TDIC(cov,legacyCovDataresultSet)
    }
  }


  /**
   * GINTEG-1178 : Below Method will map BOP Legacy Data to DTO
   * @param legacyDataresultSet
   * @return BOPLegacyPolDataMapper_TDIC
   */
  function mapLegacyPLPolDataToDTO_TDIC(legacyDataresultSet : ResultSet, policyNumber : String) : PLLegacyPolDataMapper_TDIC {
    var plLegacyPolDataMapper_TDIC = new PLLegacyPolDataMapper_TDIC()
    plLegacyPolDataMapper_TDIC.ADDRESSLINE1 = legacyDataresultSet.getString("PCHolder_AddressLine1")
    plLegacyPolDataMapper_TDIC.ADDRESSLINE2 = legacyDataresultSet.getString("PCHolder_AddressLine2")
    plLegacyPolDataMapper_TDIC.CITY = legacyDataresultSet.getString("PCHolder_City")
    plLegacyPolDataMapper_TDIC.STATE = legacyDataresultSet.getString("PCHolder_State")
    plLegacyPolDataMapper_TDIC.POSTALCODE = legacyDataresultSet.getString("PCHolder_PostalCode")
    plLegacyPolDataMapper_TDIC.FEINOFFICIALID = legacyDataresultSet.getString("CCContact_FEINOfficialID")
    plLegacyPolDataMapper_TDIC.CCCONTACT_NAME = legacyDataresultSet.getString("CCContact_Name")
    plLegacyPolDataMapper_TDIC.CCCONTACT_LASTNAME = legacyDataresultSet.getString("CCContact_LastName")
    plLegacyPolDataMapper_TDIC.CCCONTACT_FIRSTNAME = legacyDataresultSet.getString("CCContact_FirstName")
    plLegacyPolDataMapper_TDIC.CCCONTACT_ORGNAME = legacyDataresultSet.getString("CCContact_OrgName")
    plLegacyPolDataMapper_TDIC.CCCONTACT_TAXID = legacyDataresultSet.getString("CCContact_TaxID")
    plLegacyPolDataMapper_TDIC.CCCONTACT_VENDORNUMBER = legacyDataresultSet.getString("CCContact_VendorNumber")
    plLegacyPolDataMapper_TDIC.OFFICIALIDS_OFFICIALIDINSUREDANDTYPE = legacyDataresultSet.getString("OfficialIDs_OfficialIDInsuredAndType")
    plLegacyPolDataMapper_TDIC.OFFICIALIDS_OFFICIALIDTYPE = legacyDataresultSet.getString("OfficialIDs_OfficialIDType")
    plLegacyPolDataMapper_TDIC.CONTACTTYPE = legacyDataresultSet.getString("ContactType")
    plLegacyPolDataMapper_TDIC.OFFICIALIDS_OFFICIALIDVALUE = legacyDataresultSet.getString("OfficialIDs_OfficialIDValue")
    plLegacyPolDataMapper_TDIC.CCPOLICY_CURRENCY = legacyDataresultSet.getString("CCPolicy_Currency")
    plLegacyPolDataMapper_TDIC.CCPOLICY_EFFECTIVEDATE = legacyDataresultSet.getDate("CCPolicy_EffectiveDate")
    plLegacyPolDataMapper_TDIC.CCPOLICY_EXPIRATIONDATE = legacyDataresultSet.getDate("CCPolicy_ExpirationDate")
    plLegacyPolDataMapper_TDIC.PRIMARYLOCATION = legacyDataresultSet.getInt("CCPolicyLocation_PrimaryLocation")
    plLegacyPolDataMapper_TDIC.CCPOLICY_POLICYNUMBER = policyNumber
    plLegacyPolDataMapper_TDIC.CCPOLICY_POLICYSUFFIX = legacyDataresultSet.getString("CCPolicy_PolicySuffix")
    plLegacyPolDataMapper_TDIC.CCPOLICY_POLICYTYPE = PolicyType.TC_GENERALLIABILITY.Code
    plLegacyPolDataMapper_TDIC.CCPOLICY_PRODUCERCODE = legacyDataresultSet.getString("CCPolicy_ProducerCode")
    plLegacyPolDataMapper_TDIC.CCPOLICY_STATUS = legacyDataresultSet.getString("CCPolicy_Status")
    plLegacyPolDataMapper_TDIC.CCPOLICY_UNDERWRITTINGCO = legacyDataresultSet.getString("CCPolicy_UnderwrittingCo")
    plLegacyPolDataMapper_TDIC.CCPOLICY_INDUSTRYCODE_TDIC = legacyDataresultSet.getString("CCPolicy_IndustryCode_TDIC")
    plLegacyPolDataMapper_TDIC.RISKUNITS_DESCRIPTION = legacyDataresultSet.getString("RiskUnits_Description")
    plLegacyPolDataMapper_TDIC.RISKUNITS_RUNUMBER = legacyDataresultSet.getInt("RiskUnits_RUNumber")
    return plLegacyPolDataMapper_TDIC
  }

  /**
   * GINTEG-1178 : Below Method will map Legacy Coverage Data to DTO
   * @param riskUnit
   * @param legacyCovDataresultSet
   */
  function mapLegacyPLCovDataToDTO_TDIC(plLegacyPolDataMapper_TDIC : PLLegacyPolDataMapper_TDIC, legacyCovDataresultSet : ResultSet, policyNumber : String) {
    var cov : PLLegacyCovDataMapper_TDIC
    if (plLegacyPolDataMapper_TDIC.COVERAGES == null) {
      plLegacyPolDataMapper_TDIC.COVERAGES = new ArrayList<PLLegacyCovDataMapper_TDIC>()
    }
    if (plLegacyPolDataMapper_TDIC.COVERAGES.hasMatch(\elt1 -> elt1.COVPATTERNCODE == legacyCovDataresultSet.getString("CovPatternCode"))) {
      cov = plLegacyPolDataMapper_TDIC.COVERAGES.firstWhere(\elt1 -> elt1.COVPATTERNCODE == legacyCovDataresultSet.getString("CovPatternCode"))
    }
    if (cov == null) {
      cov = new PLLegacyCovDataMapper_TDIC()
      cov.COVPATTERNCODE = legacyCovDataresultSet.getString("CovPatternCode")
      cov.PUBLICID = legacyCovDataresultSet.getString("publicid")
      cov.LOB = legacyCovDataresultSet.getString("LOB")
      cov.POLICYNUMBER = policyNumber
      cov.CCCOV_EFFECTIVEDATE = legacyCovDataresultSet.getDate("CCCov_EffectiveDate")
      cov.CCCOV_EXPIRATIONDATE = legacyCovDataresultSet.getDate("CCCov_ExpirationDate")
      cov.SOURCE_SYSTEM = legacyCovDataresultSet.getString("Source_System")
      plLegacyPolDataMapper_TDIC.COVERAGES.add(cov)
    }
    if(legacyCovDataresultSet.getString("CovTermPatternCode") != null){
      if (cov.COVTERMS == null) {
        cov.COVTERMS = new ArrayList<PLLegacyCovTermDataMap_TDIC>()
      }
      mapPLLegacyCovTermDataToDTO_TDIC(cov, legacyCovDataresultSet)
    }
  }

  function mapPLLegacyCovTermDataToDTO_TDIC(cov : PLLegacyCovDataMapper_TDIC, legacyCovDataresultSet : ResultSet) {
    var covTerm = new PLLegacyCovTermDataMap_TDIC()
    var covTermVal = legacyCovDataresultSet.getString("CovValue")
    if(covTermVal != null) {
      if (covTermVal.contains("/")) {
        covTermVal = covTermVal.split("/").first()
        covTerm.COVTERMVALUE = covTermVal
      } else {
        covTerm.COVTERMVALUE = covTermVal
      }
    }
    covTerm.COVTERMDATEVALUE = legacyCovDataresultSet.getString("CovValue_Date")
    covTerm.COVTERMSTRINGVALUE = legacyCovDataresultSet.getString("CovValue_String")
    covTerm.ENDOREFFDT = legacyCovDataresultSet.getDate("EndorEffDT")
    covTerm.ENDOREXPDT = legacyCovDataresultSet.getDate("EndorExpDT")
    covTerm.NAMEOFDENTIST = legacyCovDataresultSet.getString("NameofDentist")
    covTerm.COVTERMPATTERNCODE = legacyCovDataresultSet.getString("CovTermPatternCode")
    cov.COVTERMS.add(covTerm)
  }

  /**
   * Map CYB Data from DTOs to CC Specific Entities
   * @param dataMartEnvelope
   * @param cybLegacyPolDataMapper_TDIC
   */
  function mapCYBDataFromDTOtoEnvelope_TDIC(dataMartEnvelope : wsi.remote.gw.webservice.pc.pc1000.entities.Envelope,
                                           cybLegacyPolDataMapper_TDIC : CYBLegacyPolDataMapper_TDIC) {

    var ccPolicy = new Envelope_CCPolicy()
    ccPolicy.Account = cybLegacyPolDataMapper_TDIC.CCPOLICY_POLICYNUMBER
    ccPolicy.Currency = cybLegacyPolDataMapper_TDIC.CCPOLICY_CURRENCY
    ccPolicy.setEffectiveDate(cybLegacyPolDataMapper_TDIC.CCPOLICY_EFFECTIVEDATE)
    ccPolicy.setExpirationDate(cybLegacyPolDataMapper_TDIC.CCPOLICY_EXPIRATIONDATE)
    ccPolicy.PolicyNumber = cybLegacyPolDataMapper_TDIC.CCPOLICY_POLICYNUMBER
    ccPolicy.PolicySuffix = "1"
    ccPolicy.PolicyType = "GeneralLiability"
    ccPolicy.ProducerCode = cybLegacyPolDataMapper_TDIC.CCPOLICY_PRODUCERCODE
    ccPolicy.Status = "inforce"
    ccPolicy.TotalProperties = 0
    ccPolicy.TotalVehicles = 0
    ccPolicy.UnderwritingCo = cybLegacyPolDataMapper_TDIC.CCPOLICY_UNDERWRITTINGCO
    ccPolicy.IndustryCode_TDIC = cybLegacyPolDataMapper_TDIC.CCPOLICY_INDUSTRYCODE_TDIC
    var plHolderAddr = new CCAddress()
    plHolderAddr.AddressBookUID = "pc:1"
    plHolderAddr.AddressLine1 = cybLegacyPolDataMapper_TDIC.ADDRESSLINE1
    plHolderAddr.AddressLine2 = cybLegacyPolDataMapper_TDIC.ADDRESSLINE2
    plHolderAddr.AddressType = "Billing"
    plHolderAddr.City = cybLegacyPolDataMapper_TDIC.CITY
    plHolderAddr.Country = "US"
    plHolderAddr.PostalCode = cybLegacyPolDataMapper_TDIC.POSTALCODE
    plHolderAddr.State = cybLegacyPolDataMapper_TDIC.STATE
    plHolderAddr.ID = "_1"

    var plHolderAddrEnv = new Envelope_CCAddress(plHolderAddr)
    dataMartEnvelope.CCAddress.add(plHolderAddrEnv)

    var polHolderContactEnv : Envelope_CCContact
    if (cybLegacyPolDataMapper_TDIC.CONTACTTYPE == "Person") {
      var person = new CCPerson()
      person.ID = "_1"
      person.Name = cybLegacyPolDataMapper_TDIC.CCCONTACT_NAME
      person.FirstName = cybLegacyPolDataMapper_TDIC.CCCONTACT_FIRSTNAME
      person.LastName = cybLegacyPolDataMapper_TDIC.CCCONTACT_LASTNAME
      person.PolicySystemID = "default_data:1"
      person.Preferred = true
      person.TaxStatus = "unconfirmed"
      person.FEINOfficialID = cybLegacyPolDataMapper_TDIC.FEINOFFICIALID
      person.AllAddresses.add(plHolderAddrEnv)
      polHolderContactEnv = new Envelope_CCContact(person)
      var primAddrs = new CCContact_PrimaryAddress(plHolderAddrEnv)
      polHolderContactEnv.setPrimaryAddress_elem(primAddrs)
      person.setPrimaryAddress_elem(primAddrs)
    } else {
      var company = new CCCompany()
      company.ID = "_1"
      company.Name = cybLegacyPolDataMapper_TDIC.CCCONTACT_NAME
      company.PolicySystemID = "default_data:1"
      company.Preferred = true
      company.TaxStatus = "unconfirmed"
      company.FEINOfficialID = cybLegacyPolDataMapper_TDIC.FEINOFFICIALID
      company.AllAddresses.add(plHolderAddrEnv)
      polHolderContactEnv = new Envelope_CCContact(company)
      company.setContactCompany_elem(new CCContact_ContactCompany(polHolderContactEnv))
      var primAddrs = new CCContact_PrimaryAddress(plHolderAddrEnv)
      polHolderContactEnv.setPrimaryAddress_elem(primAddrs)
      company.setPrimaryAddress_elem(primAddrs)
    }

    polHolderContactEnv.setPrimaryContact_elem(new CCContact_PrimaryContact(polHolderContactEnv))

    var allAddrs = new CCContact_AllAddresses(plHolderAddrEnv)
    polHolderContactEnv.setAllAddresses_elem({allAddrs})

    dataMartEnvelope.CCContact.add(polHolderContactEnv)

    ccPolicy.setInsured_elem(new CCPolicy_Insured(polHolderContactEnv))

    var riskUnit = new CCGeneralLiabilityRU()
    riskUnit.PolicySystemID = "_1"
    riskUnit.RUNumber = cybLegacyPolDataMapper_TDIC.RISKUNITS_RUNUMBER
    riskUnit.Description = cybLegacyPolDataMapper_TDIC.RISKUNITS_DESCRIPTION

    var loc = new CCPolicyLocation()
    loc.ID = "_1"
    loc.LocationNumber = "_1"
    loc.PolicySystemID = "_1"
    loc.PrimaryLocation = true

    var ccPolLocatn = new Envelope_CCPolicyLocation(loc)
    var ccPolLocationAddr = new CCPolicyLocation_Address(plHolderAddrEnv)
    ccPolLocatn.setAddress_elem(ccPolLocationAddr)
    ccPolLocatn.setPrimaryLocation(true)
    dataMartEnvelope.CCPolicyLocation.add(ccPolLocatn)
    riskUnit.setPolicyLocation_elem(new CCLocationBasedRU_PolicyLocation(ccPolLocatn))
    ccPolicy.RiskUnits.add(new CCPolicy_RiskUnits(riskUnit))
    dataMartEnvelope.CCPolicy = ccPolicy

    var id = 1
    cybLegacyPolDataMapper_TDIC.COVERAGES?.each(\plLegacyCov -> {
      var ccPolCov = ccPolicy.Coverages.firstWhere(\elt1 -> elt1.Type == plLegacyCov.COVPATTERNCODE)
      var cov : CCPolicyCoverage
      if(ccPolicy.Coverages.Count == 0 or ccPolCov == null){
        cov = new CCPolicyCoverage()
        ccPolCov = new CCPolicy_Coverages(cov)
        cov.Currency = "usd"
        cov.EffectiveDate = plLegacyCov.CCCOV_EFFECTIVEDATE
        cov.ExpirationDate = plLegacyCov.CCCOV_EXPIRATIONDATE
        cov.PolicySystemID = "entity.GeneralLiabilityCov:" + ("_" + (id as String))
        cov.Type = plLegacyCov.COVPATTERNCODE
        ccPolicy.Coverages.add(ccPolCov)
      } else {
        cov = ccPolCov.$TypeInstance
      }

      switch (plLegacyCov.COVPATTERNCODE) {
        case "GLCyvSupplementalERECov_TDIC":
          plLegacyCov.COVTERMS?.each(\k -> {
            if(k.COVTERMPATTERNCODE == "GLCybEREEffectiveDate_TDIC" or k.COVTERMPATTERNCODE == "GLCybEREExpirationDate_TDIC"){
              var ccCovTerm = new CCCovTerm()
              var covTerm = new CCCoverage_CovTerms(ccCovTerm)
              ccCovTerm.CovTermOrder = 1
              ccCovTerm.CovTermPattern = k.COVTERMPATTERNCODE
              ccCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(ccCovTerm.CovTermPattern)
              ccCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
              cov.CovTerms.add(covTerm)
            }
          })
          id++
          break

        default:
          plLegacyCov.COVTERMS?.each(\covTerm -> {
            if(covTerm.COVTERMVALUE != null) {
              var finCovTerm = new CCFinancialCovTerm()
              finCovTerm.setFinancialAmount(new MonetaryAmount(new BigDecimal(covTerm.COVTERMVALUE), Currency.TC_USD))
              var ccCovTerm = new CCCoverage_CovTerms(finCovTerm)
              finCovTerm.CovTermOrder = 1
              finCovTerm.CovTermPattern = covTerm.COVTERMPATTERNCODE
              finCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(finCovTerm.CovTermPattern)
              finCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
              cov.CovTerms.add(ccCovTerm)
              cov.ExposureLimit = (cov.ExposureLimit == null ? finCovTerm.FinancialAmount : (cov.ExposureLimit < finCovTerm.FinancialAmount ? finCovTerm.FinancialAmount : cov.ExposureLimit))
            }
          })
          break
      }
    })
    dataMartEnvelope.CCPolicy = ccPolicy
  }

  function mapPLAddntlInsrdDataFromDTOtoEnvelope_TDIC(dataMartEnvelope : wsi.remote.gw.webservice.pc.pc1000.entities.Envelope,
                                                      legacyDataresultSet : ResultSet, id : String) {
    var coveredParty : CCPolicy_CoveredParty
    var address = new CCAddress()
    address.AddressLine1 = legacyDataresultSet.getString("AddressLine1")
    address.AddressLine2 = legacyDataresultSet.getString("AddressLine2")
    address.AddressType = "Billing"
    address.City = legacyDataresultSet.getString("City")
    address.Country = "US"
    address.PostalCode = legacyDataresultSet.getString("Zip_Code")
    address.State = legacyDataresultSet.getString("State")
    address.ID = id
	  address.AddressBookUID = "pc:" + id

    var addressEnv = new Envelope_CCAddress(address)
    dataMartEnvelope.CCAddress.add(addressEnv)

    var contactEnv : Envelope_CCContact
    var name = legacyDataresultSet.getString("Name")
    name = (name.length() > 30 ? name.substring(0,30) : name)

    if (legacyDataresultSet.getString("ContactType") == "Person") {
      var person = new CCPerson()
      person.ID = id
      person.PolicySystemID = "default:AddntlData:" + id
      person.FirstName = name
      person.LastName = name
      person.Preferred = true
      contactEnv = new Envelope_CCContact(person)
      contactEnv.ID = id
      contactEnv.Name = name
      person.AllAddresses.add(new CCContact_AllAddresses(addressEnv))
      person.setPrimaryAddress(new CCContact_PrimaryAddress(addressEnv))
      contactEnv.AllAddresses.add(new CCContact_AllAddresses(addressEnv))
      contactEnv.setPrimaryAddress_elem(new CCContact_PrimaryAddress(addressEnv))
    } else {
      var company = new CCCompany()
      company.ID = id
      company.PolicySystemID = "default:AddntlData:" + id
      company.Name = name
      company.Preferred = true
      company.AllAddresses.add(addressEnv)

      contactEnv = new Envelope_CCContact(company)
      contactEnv.Name = name
      contactEnv.ID = id
      contactEnv.AllAddresses.add(addressEnv)

      company.setContactCompany_elem(new CCContact_ContactCompany(contactEnv))
      var primaryAddress = new CCContact_PrimaryAddress(addressEnv)
      company.setPrimaryAddress(primaryAddress)
      contactEnv.setPrimaryAddress(primaryAddress)
    }

    var allAddrs = new CCContact_AllAddresses(addressEnv)
    contactEnv.setAllAddresses_elem({allAddrs})
    var primAddrs = new CCContact_PrimaryAddress(addressEnv)
    contactEnv.setPrimaryAddress_elem(primAddrs)

    if (legacyDataresultSet.getString("Type") == "policyaddlinsured") {
      coveredParty = new CCPolicy_CoveredParty(contactEnv)
      dataMartEnvelope.CCPolicy.CoveredParty_elem.add(coveredParty)
    } else {
      var otherContact = new CCPolicy_Other(contactEnv)
      dataMartEnvelope.CCPolicy.Other_elem.add(otherContact)
    }
    dataMartEnvelope.CCContact.add(contactEnv)
  }

  /**
   * Below method will return Aggregation Model for Legacy CovTerm
   * @param covTermPattern
   * @return Aggregation Model
   */
  function getModelAggregationForLegacyCovTerm_TDIC(covTermPattern : String) : String {
    var result = Query.make(entity.LegacyCovTermsAgModl_TDIC).
        compare(LegacyCovTermsAgModl_TDIC#covTermPatternCd, Relop.Equals, covTermPattern).select()
    if(result != null and result.Count > 0){
      return (result.first().aggregationModelCd == null ? "" : result.first().aggregationModelCd)
    }
    return ""
  }

  /**
   * GINTEG-1178 : Below Method will map 'Cyber Liability'  Legacy Data to DTO
   * @param legacyDataresultSet
   * @return BOPLegacyPolDataMapper_TDIC
   */
  function mapLegacyCYBPolDataToDTO_TDIC(legacyDataresultSet : ResultSet, policyNumber : String) : CYBLegacyPolDataMapper_TDIC {
    var cybLegacyPolDataMapper_TDIC = new CYBLegacyPolDataMapper_TDIC()
    cybLegacyPolDataMapper_TDIC.ADDRESSLINE1 = legacyDataresultSet.getString("PCHolder_AddressLine1")
    cybLegacyPolDataMapper_TDIC.ADDRESSLINE2 = legacyDataresultSet.getString("PCHolder_AddressLine2")
    cybLegacyPolDataMapper_TDIC.CITY = legacyDataresultSet.getString("PCHolder_City")
    cybLegacyPolDataMapper_TDIC.STATE = legacyDataresultSet.getString("PCHolder_State")
    cybLegacyPolDataMapper_TDIC.POSTALCODE = legacyDataresultSet.getString("PCHolder_PostalCode")
    cybLegacyPolDataMapper_TDIC.FEINOFFICIALID = legacyDataresultSet.getString("CCContact_FEINOfficialID")
    cybLegacyPolDataMapper_TDIC.CCCONTACT_NAME = legacyDataresultSet.getString("CCContact_Name")
    cybLegacyPolDataMapper_TDIC.CCCONTACT_LASTNAME = legacyDataresultSet.getString("CCContact_LastName")
    cybLegacyPolDataMapper_TDIC.CCCONTACT_FIRSTNAME = legacyDataresultSet.getString("CCContact_FirstName")
    cybLegacyPolDataMapper_TDIC.CCCONTACT_ORGNAME = legacyDataresultSet.getString("CCContact_OrgName")
    cybLegacyPolDataMapper_TDIC.CCCONTACT_TAXID = legacyDataresultSet.getString("CCContact_TaxID")
    cybLegacyPolDataMapper_TDIC.CCCONTACT_VENDORNUMBER = legacyDataresultSet.getString("CCContact_VendorNumber")
    cybLegacyPolDataMapper_TDIC.OFFICIALIDS_OFFICIALIDINSUREDANDTYPE = legacyDataresultSet.getString("OfficialIDs_OfficialIDInsuredAndType")
    cybLegacyPolDataMapper_TDIC.OFFICIALIDS_OFFICIALIDTYPE = legacyDataresultSet.getString("OfficialIDs_OfficialIDType")
    cybLegacyPolDataMapper_TDIC.CONTACTTYPE = legacyDataresultSet.getString("ContactType")
    cybLegacyPolDataMapper_TDIC.OFFICIALIDS_OFFICIALIDVALUE = legacyDataresultSet.getString("OfficialIDs_OfficialIDValue")
    cybLegacyPolDataMapper_TDIC.CCPOLICY_CURRENCY = legacyDataresultSet.getString("CCPolicy_Currency")
    cybLegacyPolDataMapper_TDIC.CCPOLICY_EFFECTIVEDATE = legacyDataresultSet.getDate("CCPolicy_EffectiveDate")
    cybLegacyPolDataMapper_TDIC.CCPOLICY_EXPIRATIONDATE = legacyDataresultSet.getDate("CCPolicy_ExpirationDate")
    cybLegacyPolDataMapper_TDIC.PRIMARYLOCATION = legacyDataresultSet.getInt("CCPolicyLocation_PrimaryLocation")
    cybLegacyPolDataMapper_TDIC.CCPOLICY_POLICYNUMBER = policyNumber
    cybLegacyPolDataMapper_TDIC.CCPOLICY_POLICYSUFFIX = legacyDataresultSet.getString("CCPolicy_PolicySuffix")
    cybLegacyPolDataMapper_TDIC.CCPOLICY_POLICYTYPE = PolicyType.TC_GENERALLIABILITY.Code
    cybLegacyPolDataMapper_TDIC.CCPOLICY_PRODUCERCODE = legacyDataresultSet.getString("CCPolicy_ProducerCode")
    cybLegacyPolDataMapper_TDIC.CCPOLICY_STATUS = legacyDataresultSet.getString("CCPolicy_Status")
    cybLegacyPolDataMapper_TDIC.CCPOLICY_UNDERWRITTINGCO = legacyDataresultSet.getString("CCPolicy_UnderwrittingCo")
    cybLegacyPolDataMapper_TDIC.CCPOLICY_INDUSTRYCODE_TDIC = legacyDataresultSet.getString("CCPolicy_IndustryCode_TDIC")
    cybLegacyPolDataMapper_TDIC.RISKUNITS_DESCRIPTION = legacyDataresultSet.getString("RiskUnits_Description")
    cybLegacyPolDataMapper_TDIC.RISKUNITS_RUNUMBER = legacyDataresultSet.getInt("RiskUnits_RUNumber")
    return cybLegacyPolDataMapper_TDIC
  }

  /**
   * GINTEG-1178 : Below Method will map Legacy Coverage Data to DTO
   * @param riskUnit
   * @param legacyCovDataresultSet
   */
  function mapLegacyCYBCovDataToDTO_TDIC(cybLegacyPolDataMapper_TDIC : CYBLegacyPolDataMapper_TDIC,
                                         legacyCovDataresultSet : ResultSet, policyNumber : String) {
    var cov : CYBLegacyCovDataMapper_TDIC
    if (cybLegacyPolDataMapper_TDIC.COVERAGES == null) {
      cybLegacyPolDataMapper_TDIC.COVERAGES = new ArrayList<CYBLegacyCovDataMapper_TDIC>()
    }
    if (cybLegacyPolDataMapper_TDIC.COVERAGES.hasMatch(\elt1 -> elt1.COVPATTERNCODE == legacyCovDataresultSet.getString("CovPatternCode"))) {
      cov = cybLegacyPolDataMapper_TDIC.COVERAGES.firstWhere(\elt1 -> elt1.COVPATTERNCODE == legacyCovDataresultSet.getString("CovPatternCode"))
    }
    if (cov == null) {
      cov = new CYBLegacyCovDataMapper_TDIC()
      cov.COVPATTERNCODE = legacyCovDataresultSet.getString("CovPatternCode")
      cov.PUBLICID = legacyCovDataresultSet.getString("publicid")
      cov.LOB = legacyCovDataresultSet.getString("LOB")
      cov.POLICYNUMBER = policyNumber
      cov.CCCOV_EFFECTIVEDATE = legacyCovDataresultSet.getDate("CCCov_EffectiveDate")
      cov.CCCOV_EXPIRATIONDATE = legacyCovDataresultSet.getDate("CCCov_ExpirationDate")
      cov.SOURCE_SYSTEM = legacyCovDataresultSet.getString("Source_System")
      cybLegacyPolDataMapper_TDIC.COVERAGES.add(cov)
    }
    if(legacyCovDataresultSet.getString("CovTermPatternCode") != null){
      if (cov.COVTERMS == null) {
        cov.COVTERMS = new ArrayList<CYBLegacyCovTermDataMap_TDIC>()
      }
      mapCYBLegacyCovTermDataToDTO_TDIC(cov, legacyCovDataresultSet)
    }
  }

  function mapCYBLegacyCovTermDataToDTO_TDIC(cov : CYBLegacyCovDataMapper_TDIC, legacyCovDataresultSet : ResultSet) {
    var covTerm : CYBLegacyCovTermDataMap_TDIC
    var covTermPatternCd = legacyCovDataresultSet.getString("CovTermPatternCode")
    if(cov.COVTERMS.hasMatch(\elt1 -> elt1.COVTERMPATTERNCODE == covTermPatternCd)) {
      covTerm = cov.COVTERMS.firstWhere(\elt1 -> elt1.COVTERMPATTERNCODE == covTermPatternCd)
    } else {
      covTerm = new CYBLegacyCovTermDataMap_TDIC()
      cov.COVTERMS.add(covTerm)
    }
    var covTermVal = legacyCovDataresultSet.getString("CovValue")
    if(covTermVal != null) {
      if (covTermVal.contains("/")) {
        covTermVal = covTermVal.split("/").first()
        covTerm.COVTERMVALUE = covTermVal
      } else {
        covTerm.COVTERMVALUE = covTermVal
      }
    }
    covTerm.COVTERMDATEVALUE = legacyCovDataresultSet.getString("CovValue_Date")
    covTerm.COVTERMPATTERNCODE = legacyCovDataresultSet.getString("CovTermPatternCode")
  }

  function mapPLDataFromDTOtoEnvelope_TDIC(dataMartEnvelope : wsi.remote.gw.webservice.pc.pc1000.entities.Envelope,
                                           plLegacyPolDataMapper_TDIC : PLLegacyPolDataMapper_TDIC) {

    var ccPolicy = new Envelope_CCPolicy()
    ccPolicy.Account = plLegacyPolDataMapper_TDIC.CCPOLICY_POLICYNUMBER
    ccPolicy.Currency = plLegacyPolDataMapper_TDIC.CCPOLICY_CURRENCY
    ccPolicy.setEffectiveDate(plLegacyPolDataMapper_TDIC.CCPOLICY_EFFECTIVEDATE)
    ccPolicy.setExpirationDate(plLegacyPolDataMapper_TDIC.CCPOLICY_EXPIRATIONDATE)
    ccPolicy.PolicyNumber = plLegacyPolDataMapper_TDIC.CCPOLICY_POLICYNUMBER
    ccPolicy.PolicySuffix = "1"
    ccPolicy.PolicyType = PolicyType.TC_GENERALLIABILITY.Code
    ccPolicy.ProducerCode = plLegacyPolDataMapper_TDIC.CCPOLICY_PRODUCERCODE
    ccPolicy.Status = "inforce"
    ccPolicy.TotalProperties = 0
    ccPolicy.TotalVehicles = 0
    ccPolicy.UnderwritingCo = plLegacyPolDataMapper_TDIC.CCPOLICY_UNDERWRITTINGCO
    ccPolicy.IndustryCode_TDIC = plLegacyPolDataMapper_TDIC.CCPOLICY_INDUSTRYCODE_TDIC

    var plHolderAddr = new CCAddress()
    plHolderAddr.AddressBookUID = "pc:1"
    plHolderAddr.AddressLine1 = plLegacyPolDataMapper_TDIC.ADDRESSLINE1
    plHolderAddr.AddressLine2 = plLegacyPolDataMapper_TDIC.ADDRESSLINE2
    plHolderAddr.AddressType = "Billing"
    plHolderAddr.City = plLegacyPolDataMapper_TDIC.CITY
    plHolderAddr.Country = "US"
    plHolderAddr.PostalCode = plLegacyPolDataMapper_TDIC.POSTALCODE
    plHolderAddr.State = plLegacyPolDataMapper_TDIC.STATE
    plHolderAddr.ID = "_1"

    var plHolderAddrEnv = new Envelope_CCAddress(plHolderAddr)
    dataMartEnvelope.CCAddress.add(plHolderAddrEnv)

    var polHolderContactEnv : Envelope_CCContact

    if (plLegacyPolDataMapper_TDIC.CONTACTTYPE == "Person") {
      var person = new CCPerson()
      person.ID = "_1"
      person.Name = plLegacyPolDataMapper_TDIC.CCCONTACT_NAME
      person.FirstName = plLegacyPolDataMapper_TDIC.CCCONTACT_FIRSTNAME
      person.LastName = plLegacyPolDataMapper_TDIC.CCCONTACT_LASTNAME
      person.PolicySystemID = "default_data:1"
      person.Preferred = true
      person.TaxStatus = "unconfirmed"
      person.FEINOfficialID = plLegacyPolDataMapper_TDIC.FEINOFFICIALID
      person.AllAddresses.add(plHolderAddrEnv)
      polHolderContactEnv = new Envelope_CCContact(person)
      var primAddrs = new CCContact_PrimaryAddress(plHolderAddrEnv)
      polHolderContactEnv.setPrimaryAddress_elem(primAddrs)
      person.setPrimaryAddress_elem(primAddrs)
    } else {
      var company = new CCCompany()
      company.ID = "_1"
      company.Name = plLegacyPolDataMapper_TDIC.CCCONTACT_NAME
      company.PolicySystemID = "default_data:1"
      company.Preferred = true
      company.TaxStatus = "unconfirmed"
      company.FEINOfficialID = plLegacyPolDataMapper_TDIC.FEINOFFICIALID
      company.AllAddresses.add(plHolderAddrEnv)
      polHolderContactEnv = new Envelope_CCContact(company)
      company.setContactCompany_elem(new CCContact_ContactCompany(polHolderContactEnv))
      var primAddrs = new CCContact_PrimaryAddress(plHolderAddrEnv)
      polHolderContactEnv.setPrimaryAddress_elem(primAddrs)
      company.setPrimaryAddress_elem(primAddrs)
    }

    polHolderContactEnv.setPrimaryContact_elem(new CCContact_PrimaryContact(polHolderContactEnv))

    var allAddrs = new CCContact_AllAddresses(plHolderAddrEnv)
    polHolderContactEnv.setAllAddresses_elem({allAddrs})

    var primAddrs = new CCContact_PrimaryAddress(plHolderAddrEnv)
    polHolderContactEnv.setPrimaryAddress_elem(primAddrs)

    dataMartEnvelope.CCContact.add(polHolderContactEnv)

    ccPolicy.setInsured_elem(new CCPolicy_Insured(polHolderContactEnv))

    var riskUnit = new CCGeneralLiabilityRU()
    riskUnit.PolicySystemID = "_1"
    riskUnit.RUNumber = plLegacyPolDataMapper_TDIC.RISKUNITS_RUNUMBER
    riskUnit.Description = plLegacyPolDataMapper_TDIC.RISKUNITS_DESCRIPTION

    var loc = new CCPolicyLocation()
    loc.ID = "_1"
    loc.LocationNumber = "_1"
    loc.PolicySystemID = "_1"
    loc.PrimaryLocation = true

    var ccPolLocatn = new Envelope_CCPolicyLocation(loc)
    var ccPolLocationAddr = new CCPolicyLocation_Address(plHolderAddrEnv)
    ccPolLocatn.setAddress_elem(ccPolLocationAddr)
    ccPolLocatn.setPrimaryLocation(true)
    dataMartEnvelope.CCPolicyLocation.add(ccPolLocatn)
    ccPolicy.setPolicyLocations_elem({new CCPolicy_PolicyLocations(ccPolLocatn)})
    riskUnit.setPolicyLocation_elem(new CCLocationBasedRU_PolicyLocation(ccPolLocatn))
    ccPolicy.RiskUnits.add(new CCPolicy_RiskUnits(riskUnit))
    dataMartEnvelope.CCPolicy = ccPolicy

    var id = 1
    plLegacyPolDataMapper_TDIC.COVERAGES?.each(\plLegacyCov -> {
      var ccPolCov = ccPolicy.Coverages.firstWhere(\elt1 -> elt1.Type == plLegacyCov.COVPATTERNCODE)
      var cov : CCPolicyCoverage
      if(ccPolicy.Coverages.Count == 0 or ccPolCov == null){
        if(not (plLegacyCov.COVPATTERNCODE == "GLAggLimit_TDIC" and plLegacyPolDataMapper_TDIC.COVERAGES.
            hasMatch(\elt1 -> elt1.COVPATTERNCODE == "GLDentistProfLiabCov_TDIC" or elt1.COVPATTERNCODE == "GLDentalBusinessLiabCov_TDIC"))) {
          cov = new CCPolicyCoverage()
          ccPolCov = new CCPolicy_Coverages(cov)
          cov.Currency = "usd"
          cov.EffectiveDate = plLegacyCov.CCCOV_EFFECTIVEDATE
          cov.ExpirationDate = plLegacyCov.CCCOV_EXPIRATIONDATE
          cov.PolicySystemID = "entity.GeneralLiabilityCov:" + ("_" + (id as String))
          cov.Type = plLegacyCov.COVPATTERNCODE
          ccPolicy.Coverages.add(ccPolCov)
        }
      } else {
        cov = ccPolCov.$TypeInstance
      }

      switch (plLegacyCov.COVPATTERNCODE) {
        case "GLLocumTenensCov_TDIC":
          plLegacyCov.COVTERMS?.each(\k -> {
            var covTerm = new CCCovTerm()
            var covTerm1 = new CCCoverage_CovTerms(covTerm)
            covTerm.CovTermOrder = 1
            covTerm.CovTermPattern = k.COVTERMPATTERNCODE
            covTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(covTerm.CovTermPattern)
            covTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
            cov.CovTerms.add(covTerm1)
          })
          id++
          break

        case "GLAdditionalInsuredCov_TDIC":
          plLegacyCov.COVTERMS?.each(\k -> {
            var covTerm = new CCCovTerm()
            var covTerm1 = new CCCoverage_CovTerms(covTerm)
            covTerm.CovTermOrder = 1
            covTerm.CovTermPattern = k.COVTERMPATTERNCODE
            covTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(covTerm.CovTermPattern)
            covTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
            cov.CovTerms.add(covTerm1)
          })
          id++
          break

        case "GLDentalMedWasteCov_TDIC":
          plLegacyCov.COVTERMS?.each(\k -> {
            if(k.COVTERMPATTERNCODE == "GLDMWLimit_TDIC"){
              var finCovTerm = new CCFinancialCovTerm()
              finCovTerm.setFinancialAmount(new MonetaryAmount(new BigDecimal(k.COVTERMVALUE), Currency.TC_USD))
              var covTerm = new CCCoverage_CovTerms(finCovTerm)
              finCovTerm.CovTermOrder = 1
              finCovTerm.CovTermPattern = k.COVTERMPATTERNCODE
              finCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(covTerm.CovTermPattern)
              finCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
              cov.CovTerms.add(covTerm)
              cov.ExposureLimit = (cov.ExposureLimit == null ? finCovTerm.FinancialAmount : (cov.ExposureLimit < finCovTerm.FinancialAmount ? finCovTerm.FinancialAmount : cov.ExposureLimit))
            } else if(k.COVTERMPATTERNCODE == "GLDMWCoPay_TDIC"){
              var numericCovTerm = new CCNumericCovTerm()
              var covTerm = new CCCoverage_CovTerms(numericCovTerm)
              numericCovTerm.CovTermOrder = 1
              numericCovTerm.CovTermPattern = k.COVTERMPATTERNCODE
              numericCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(numericCovTerm.CovTermPattern)
              numericCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
              numericCovTerm.NumericValue = k.COVTERMSTRINGVALUE.toInt()
              cov.CovTerms.add(covTerm)
            }
          })
          id++
          break

        case "GLSpecialEventCov_TDIC":
          plLegacyCov.COVTERMS?.each(\k -> {
            if(k.COVTERMPATTERNCODE == "GLSpecialEventCDAPDADesc_TDIC"){
              var numericCovTerm = new CCClassificationCovTerm()
              var covTerm = new CCCoverage_CovTerms(numericCovTerm)
              numericCovTerm.CovTermOrder = 1
              numericCovTerm.CovTermPattern = k.COVTERMPATTERNCODE
              numericCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(numericCovTerm.CovTermPattern)
              numericCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
              numericCovTerm.Description = k.CDA_PDA
              cov.CovTerms.add(covTerm)
            }
          })
          id++
          break

        case "GLMultiOwnerDPECov_TDIC":
          plLegacyCov.COVTERMS?.each(\k -> {
            if(k.COVTERMPATTERNCODE == "GLCorpName_TDIC"){
              var numericCovTerm = new CCClassificationCovTerm()
              var covTerm = new CCCoverage_CovTerms(numericCovTerm)
              numericCovTerm.CovTermOrder = 1
              numericCovTerm.CovTermPattern = k.COVTERMPATTERNCODE
              numericCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(numericCovTerm.CovTermPattern)
              numericCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
              numericCovTerm.Description = k.COVTERMSTRINGVALUE
              cov.CovTerms.add(covTerm)
            }
          })
          id++
          break

        case "GLManuscriptEndor_TDIC":
          plLegacyCov.COVTERMS?.each(\k -> {
            if(k.COVTERMPATTERNCODE == "GLManuscriptDesc_TDIC"){
              var numericCovTerm = new CCClassificationCovTerm()
              var covTerm = new CCCoverage_CovTerms(numericCovTerm)
              numericCovTerm.CovTermOrder = 1
              numericCovTerm.CovTermPattern = k.COVTERMPATTERNCODE
              numericCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(numericCovTerm.CovTermPattern)
              numericCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
              numericCovTerm.Description = k.COVTERMSTRINGVALUE
              cov.CovTerms.add(covTerm)
            } else if(k.COVTERMPATTERNCODE == "GLManuscriptType_TDIC"){
              var numericCovTerm = new CCNumericCovTerm()
              var covTerm = new CCCoverage_CovTerms(numericCovTerm)
              numericCovTerm.CovTermOrder = 1
              numericCovTerm.CovTermPattern = k.COVTERMPATTERNCODE
              numericCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(numericCovTerm.CovTermPattern)
              numericCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
              numericCovTerm.NumericValue = k.COVTERMSTRINGVALUE.toInt()
              cov.CovTerms.add(covTerm)
            }
          })
          id++
          break

        case "GLDentistProfLiabCov_TDIC":
          if(cov != null) {
            plLegacyCov.COVTERMS?.each(\covTerm -> {
              if (covTerm.COVTERMVALUE != null) {
                var finCovTerm : CCFinancialCovTerm
                var ccCovTerm : CCCoverage_CovTerms
                var exposureLimit : MonetaryAmount
                if ((plLegacyPolDataMapper_TDIC.STATE == "IL") and plLegacyPolDataMapper_TDIC.COVERAGES.hasMatch(\elt1 -> elt1.COVPATTERNCODE == "GLAggLimit_TDIC") and
                    (not cov.CovTerms.hasMatch(\elt1 -> elt1.CovTermPattern == "GLAggLimitlLA_TDIC"))) {
                  finCovTerm = new CCFinancialCovTerm()
                  ccCovTerm = new CCCoverage_CovTerms(finCovTerm)
                  exposureLimit = new MonetaryAmount(new BigDecimal(plLegacyPolDataMapper_TDIC.COVERAGES.firstWhere(\elt1 -> elt1.COVPATTERNCODE == "GLAggLimit_TDIC").COVTERMS.
                      firstWhere(\elt -> elt.COVTERMPATTERNCODE == "GLAggLimitAB_TDIC").COVTERMVALUE), Currency.TC_USD)
                  finCovTerm.CovTermOrder = 1
                  finCovTerm.CovTermPattern = "GLAggLimitlLA_TDIC"
                  finCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(finCovTerm.CovTermPattern)
                  finCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
                  cov.CovTerms.add(ccCovTerm)
                  cov.ExposureLimit = exposureLimit
                  finCovTerm.setFinancialAmount(exposureLimit)
                }
                finCovTerm = new CCFinancialCovTerm()
                finCovTerm.setFinancialAmount(new MonetaryAmount(new BigDecimal(covTerm.COVTERMVALUE), Currency.TC_USD))
                ccCovTerm = new CCCoverage_CovTerms(finCovTerm)
                finCovTerm.CovTermOrder = 1
                finCovTerm.CovTermPattern = covTerm.COVTERMPATTERNCODE
                finCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(finCovTerm.CovTermPattern)
                finCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
                cov.CovTerms.add(ccCovTerm)
                cov.ExposureLimit = (cov.ExposureLimit == null ? finCovTerm.FinancialAmount : (cov.ExposureLimit < finCovTerm.FinancialAmount ? finCovTerm.FinancialAmount : cov.ExposureLimit))
              }
            })
          }
          id++
          break

        case "GLDentalBusinessLiabCov_TDIC":
          if(cov != null) {
            plLegacyCov.COVTERMS?.each(\covTerm -> {
              if (covTerm.COVTERMVALUE != null) {
                var finCovTerm : CCFinancialCovTerm
                var ccCovTerm : CCCoverage_CovTerms
                var exposureLimit : MonetaryAmount
                if ((plLegacyPolDataMapper_TDIC.STATE == "IL") and plLegacyPolDataMapper_TDIC.COVERAGES.hasMatch(\elt1 -> elt1.COVPATTERNCODE == "GLAggLimit_TDIC") and
                    (not cov.CovTerms.hasMatch(\elt1 -> elt1.CovTermPattern == "GLAggLimitlLB_TDIC"))) {
                  finCovTerm = new CCFinancialCovTerm()
                  ccCovTerm = new CCCoverage_CovTerms(finCovTerm)
                  exposureLimit = new MonetaryAmount(new BigDecimal(plLegacyPolDataMapper_TDIC.COVERAGES.firstWhere(\elt1 -> elt1.COVPATTERNCODE == "GLAggLimit_TDIC").COVTERMS.
                      firstWhere(\elt -> elt.COVTERMPATTERNCODE == "GLAggLimitAB_TDIC").COVTERMVALUE), Currency.TC_USD)
                  finCovTerm.CovTermOrder = 1
                  finCovTerm.CovTermPattern = "GLAggLimitlLB_TDIC"
                  finCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(finCovTerm.CovTermPattern)
                  finCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
                  cov.CovTerms.add(ccCovTerm)
                  cov.ExposureLimit = exposureLimit
                  finCovTerm.setFinancialAmount(exposureLimit)
                }
                finCovTerm = new CCFinancialCovTerm()
                finCovTerm.setFinancialAmount(new MonetaryAmount(new BigDecimal(covTerm.COVTERMVALUE), Currency.TC_USD))
                ccCovTerm = new CCCoverage_CovTerms(finCovTerm)
                finCovTerm.CovTermOrder = 1
                finCovTerm.CovTermPattern = covTerm.COVTERMPATTERNCODE
                finCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(finCovTerm.CovTermPattern)
                finCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
                cov.CovTerms.add(ccCovTerm)
                cov.ExposureLimit = (cov.ExposureLimit == null ? finCovTerm.FinancialAmount : (cov.ExposureLimit < finCovTerm.FinancialAmount ? finCovTerm.FinancialAmount : cov.ExposureLimit))
              }
            })
          }
          id++
          break

        default:
          if(cov != null) {
            plLegacyCov.COVTERMS?.each(\covTerm -> {
              if (covTerm.COVTERMVALUE != null) {
                var finCovTerm = new CCFinancialCovTerm()
                finCovTerm.setFinancialAmount(new MonetaryAmount(new BigDecimal(covTerm.COVTERMVALUE), Currency.TC_USD))
                var ccCovTerm = new CCCoverage_CovTerms(finCovTerm)
                finCovTerm.CovTermOrder = 1
                finCovTerm.CovTermPattern = covTerm.COVTERMPATTERNCODE
                finCovTerm.ModelAggregation = getModelAggregationForLegacyCovTerm_TDIC(finCovTerm.CovTermPattern)
                finCovTerm.PolicySystemID = cov.PolicySystemID + ".zq7hi8ve5rcjb5drru8120gt778"
                cov.CovTerms.add(ccCovTerm)
                cov.ExposureLimit = (cov.ExposureLimit == null ? finCovTerm.FinancialAmount : (cov.ExposureLimit < finCovTerm.FinancialAmount ? finCovTerm.FinancialAmount : cov.ExposureLimit))
              }
            })
          }
          id++
          break
      }
    })
    dataMartEnvelope.CCPolicy = ccPolicy
  }

  function mapBOPLegacyCovTermDataToDTO_TDIC(cov : BOPLegacyCovDataMapper_TDIC, legacyCovDataresultSet : ResultSet) {
    var covTerm : BOPLegacyCovTermDataMap_TDIC
    if(cov.COVTERMS.hasMatch(\elt1 -> elt1.COVTERMPATTERNCODE == legacyCovDataresultSet.getString("CovTermPatternCode"))){
      covTerm =cov.COVTERMS.firstWhere(\elt1 -> elt1.COVTERMPATTERNCODE == legacyCovDataresultSet.getString("CovTermPatternCode"))
    } else {
      covTerm = new BOPLegacyCovTermDataMap_TDIC()
      covTerm.COVTERMPATTERNCODE = legacyCovDataresultSet.getString("CovTermPatternCode")
    }
    var covTermVal = legacyCovDataresultSet.getString("CovValue")
    if(covTermVal != null) {
      if (covTermVal.contains("/")) {
        covTermVal = covTermVal.split("/").first()
        covTerm.COVTERMVALUE = covTermVal
      } else {
        covTerm.COVTERMVALUE = covTermVal
      }
    }
    covTerm.COVTERMDATEVALUE = legacyCovDataresultSet.getString("CovValue_Date")
    covTerm.COVTERMSTRINGVALUE = legacyCovDataresultSet.getString("CovValue_String")
    cov.COVTERMS.add(covTerm)
  }
}
