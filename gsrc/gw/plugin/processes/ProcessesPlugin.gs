package gw.plugin.processes
uses com.tdic.common.batch.MarkTClaimsForPurge
uses gw.api.financials.escalation.FinancialsEscalationMonitor
uses gw.plugin.processing.IProcessesPlugin
uses gw.processes.BatchProcess
uses gw.util.ClaimHealthCalculatorBatch
uses gw.util.PurgeMessageHistory
uses gw.util.CatastropheClaimFinderBatch
uses gw.policy.RetiredPolicyGraphDisconnectorBatch
uses gw.processes.SolrDataImportBatchProcess
uses gw.api.financials.escalation.BulkInvoiceEscalationMonitor
uses tdic.cc.common.batch.checkprinting.TDIC_CheckPrintingOutboundBatch
uses tdic.cc.common.batch.generalledger.TDIC_GeneralLedgerOutboundBatch
uses tdic.util.cache.CacheManagerBatchProcess
uses tdic.cc.integ.plugins.hpexstream.batch.TDIC_DocumentCreationBatch
uses tdic.cc.common.batch.sedgwick.TDIC_UpdateFeedBatchProcess
uses tdic.cc.integ.plugins.sedgwick.TDIC_SedgwickFNOLBatch
uses tdic.cc.common.batch.sedgwick.TDIC_CreateNewClaimBatch

@Export
class ProcessesPlugin implements IProcessesPlugin {

  construct() {
  }

  override function createBatchProcess(type : BatchProcessType, arguments : Object[]) : BatchProcess {
    switch(type) {
      case BatchProcessType.TC_BULKINVOICEESC:
          return new BulkInvoiceEscalationMonitor()
      case BatchProcessType.TC_CATASTROPHECLAIMFINDER:
          return new CatastropheClaimFinderBatch()
      case BatchProcessType.TC_CLAIMHEALTHCALC:
        return new ClaimHealthCalculatorBatch()
      case BatchProcessType.TC_FINANCIALSESC:
        //noinspection GosuDeprecatedAPIUsage
        return new FinancialsEscalationMonitor()
      case BatchProcessType.TC_PURGEMESSAGEHISTORY:
        return new PurgeMessageHistory(arguments)
      case BatchProcessType.TC_RETIREDPOLICYGRAPHDISCONNECTOR:
        return new RetiredPolicyGraphDisconnectorBatch()
      case BatchProcessType.TC_SOLRDATAIMPORT:
        return new SolrDataImportBatchProcess()
      case BatchProcessType.TC_CACHEMANAGER:
        return new CacheManagerBatchProcess()
      case BatchProcessType.TC_DOCUMENTCREATIONBATCH:
        return new TDIC_DocumentCreationBatch();
      /*
       * Update Feed From Sedgwick To CC
       * 03/30/2015 Praneethk
       */
      case BatchProcessType.TC_FEEDFROMSEDGWICK:
        return new TDIC_UpdateFeedBatchProcess()
      case BatchProcessType.TC_SEDGWICKFNOL:
        return new TDIC_SedgwickFNOLBatch ()
      case BatchProcessType.TC_MIGRATIONCLAIMS:
        return new TDIC_CreateNewClaimBatch()
      case BatchProcessType.TC_CHECKPRINTINGOUTBOUNDBATCH:
        return new TDIC_CheckPrintingOutboundBatch()
      case BatchProcessType.TC_GENERALLEDGEROUTBOUNDBATCH:
        return new TDIC_GeneralLedgerOutboundBatch()
      case BatchProcessType.TC_MARKTCLAIMSFORPURGE_EXT:
        return new MarkTClaimsForPurge()
      default:
        return null
    }
  }

}
