package gw.entity

uses gw.api.locale.DisplayKey
uses com.google.common.base.Joiner

@Export
enhancement GWClaimFinancialsHoldsEnhancement : entity.Claim {
  /**
   * 
   */
  function createNoteIfInitialReservesNotCreated(){
    var msg : String
    if(this.CoverageInQuestion == true) {
      msg = DisplayKey.get("Rules.InitialReserves.CannotCreateDueToCovInQuestion")
    }
    if(this.IncidentReport == true) {
      msg =  DisplayKey.get("Rules.InitialReserves.CannotCreateDueToIncidentOnly")
    }
    if(msg != null) {
      this.addNote( NoteTopicType.TC_COVERAGE, msg)
    }
  }
  
  /**
   * Factors to check if the coverage for this claim could be in question
   */
  function isCoverageInQuestionFactorsNotEmpty() : Boolean {
    return !getCoverageInQuestionFactors().Empty
  }
  
  /**
   * Created with IntelliJ IDEA.
   * User: SharmaaA
   * Date: 06/14/2020
   * This function is used to check whether Extended period Endorsement coverage is added into the policy and Billed field was waived.
   * if Billed field was waived,it will return false,else true.
   */
  function isExtendedPerCovExistsWithWaivedOff(): Boolean{
    var isBilled=true
    var isCovTermWithWaivedExists=this.Policy.Coverages*.CovTerms?.hasMatch(\covTerm -> covTerm.PolicySystemId.contains("zeng02aelt5j63b7rmc3gbcktd8"))
    if(isCovTermWithWaivedExists){
      //coverage term "Billed" public id "zeng02aelt5j63b7rmc3gbcktd8"
      var isCovTermValue=this.Policy.Coverages*.CovTerms?.where(\covTerm -> covTerm.PolicySystemId.contains("zeng02aelt5j63b7rmc3gbcktd8")).first().Value
      if(isCovTermValue!="true"){
        this.CoverageInQuestion=false
        isBilled=false
      }
    }
    return isBilled
  }
  
  /**
   * Get a list of factors that affect coverage
   */
  function getCoverageInQuestionFactors() : java.util.HashMap<Object, Object> {
    var validPolicyStatuses = {PolicyStatus.TC_ARCHIVED, PolicyStatus.TC_INFORCE}
    var coverageInQuestionMap = new java.util.HashMap()
    var validClaimTypes = {ClaimType_TDIC.TC_PROFESSIONALLIABILITY,ClaimType_TDIC.TC_CYBERLIABILITY,ClaimType_TDIC.TC_EMPLOYMENTPRACTICES,ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE}

    if(this.LossDate > this.Policy.ExpirationDate) {
      coverageInQuestionMap.put(DisplayKey.get("Web.Claim.CoverageInQuestionClaimIndicator.LossDate"), DisplayKey.get("Web.Claim.CoverageInQuestionClaimIndicator.Messages.AfterPolEffDates"))
    } else if( this.LossDate < this.Policy.EffectiveDate and this.Type_TDIC != ClaimType_TDIC.TC_PROFESSIONALLIABILITY
        and this.Type_TDIC != ClaimType_TDIC.TC_REGULATORYLEGALDEFENSE and this.Type_TDIC != ClaimType_TDIC.TC_EMPLOYMENTPRACTICES
        and this.Type_TDIC != ClaimType_TDIC.TC_CYBERLIABILITY) {
      coverageInQuestionMap.put(DisplayKey.get("Web.Claim.CoverageInQuestionClaimIndicator.LossDate"), DisplayKey.get("Web.Claim.CoverageInQuestionClaimIndicator.Messages.BeforePolEffDates"))
    }
    var dateInConsideration = (this.Policy.Status == PolicyStatus.TC_CANCELED) ? this.Policy.CancellationDate : this.Policy.ExpirationDate

    if(this.Policy.Status != null) {
      if(!validPolicyStatuses.contains(this.Policy.Status) and this.LossDate > this.Policy.EffectiveDate  and this.LossDate < dateInConsideration
          and this.ReportedDate > dateInConsideration and dateInConsideration.daysBetween(this.ReportedDate)+1 <= 365 and this.Type_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES){
       // coverageInQuestionMap.put(DisplayKey.get("Web.Claim.CoverageInQuestionClaimIndicator.PolicyStatus"), this.Policy.Status.toString())
      }else if(!validPolicyStatuses.contains(this.Policy.Status)){
        coverageInQuestionMap.put(DisplayKey.get("Web.Claim.CoverageInQuestionClaimIndicator.PolicyStatus"), this.Policy.Status.toString())
      }
    }

    //This section is added to set the Coverage In Question Flag
    //When Report Date is:
    //  1. > 60 days after Expiration/Cancellation Date for Non EPL Types
    //  2. > 365 days after Expiration/Cancellation Date for EPL Types

    if((validClaimTypes.contains(this.Type_TDIC) and (this.Type_TDIC == ClaimType_TDIC.TC_CYBERLIABILITY ? (this.CL_CoverageType_TDIC==CL_CoverageType_TDIC.TC_THIRDPARTY) : true)) and this.ReportedDate > dateInConsideration
        and (this.Type_TDIC == ClaimType_TDIC.TC_EMPLOYMENTPRACTICES
              ? dateInConsideration.differenceInDays(this.ReportedDate) > 365
              : dateInConsideration.differenceInDays(this.ReportedDate) > 60) and
        (this.LossDate<this.Policy.EffectiveDate ? (this.LossDate>=this.Policy.PriorActs_TDIC) : (this.LossDate>=this.Policy.EffectiveDate and this.LossDate<this.Policy.ExpirationDate))) {
      coverageInQuestionMap.put(DisplayKey.get("Web.Claim.CoverageInQuestionClaimIndicator.ReportedDate"), DisplayKey.get("Web.Claim.CoverageInQuestionClaimIndicator.Messages.AfterExpirationOrCancellationDates", dateInConsideration.differenceInDays(this.ReportedDate)))
    }
    return coverageInQuestionMap
  }

  property get CoverageInQuestionDescription() : String {
    var desc = Joiner.on(", ").withKeyValueSeparator(": ").join(this.getCoverageInQuestionFactors())
    return DisplayKey.get("Web.Claim.CoverageInQuestionClaimIndicator.Description", desc)
  }

  property get CoverageInQuestionFormattedReasons() : String{
    return Joiner.on("\n").withKeyValueSeparator(": ").join(this.getCoverageInQuestionFactors())
  }
}
