package com.tdic.util.properties.exception

uses java.lang.Exception
uses java.lang.StringBuilder
uses java.lang.Throwable

/**
 * Created by Souvik Kar on 5/13/2019.
 */
class PropertyNotFoundException extends Exception {
  /**
   * Instantiates a new property not found exception.
   *
   * @param property the property
   */
  construct(prop: String) {
    super(createMessage(prop, "Property not found"))
  }

  /**
   * Instantiates a new property not found exception.
   *
   * @param property the property
   * @param message the message
   */
  construct(prop: String, message: String) {
    super(createMessage(prop, message))
  }

  /**
   * Instantiates a new property not found exception.
   *
   * @param property the property
   * @param cause the cause
   */
  construct(prop: String, cause: Throwable) {
    super(createMessage(prop, "Property not found"), cause)
  }

  /**
   * Instantiates a new property not found exception.
   *
   * @param property the property
   * @param message the message
   * @param cause the cause
   */
  construct(prop: String, message: String, cause: Throwable) {
    super(createMessage(prop, message), cause);
  }

  private static function createMessage(prop: String, message: String): String {
    var msg = new StringBuilder(message)
    msg.append(". Property: ")
    msg.append(prop)
    return msg.toString()
  }
}