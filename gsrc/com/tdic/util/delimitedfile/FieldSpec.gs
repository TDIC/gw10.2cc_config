package com.tdic.util.delimitedfile

uses java.util.List

/**
 * Gosu object to contain the fields of a FieldSpec, which is a single row in the vendor spec Excel spreadsheet.
 */
class FieldSpec {
  public static final var COLUMN_COUNT: int = 9

  var _start: String      as Start      // column  2  count  0
  var _length: String     as Length     // column  3  count  1
  var _field: String      as Field      // column  4  count  2
  var _format: String     as Format     // column  5  count  3
  var _justify: String    as Justify    // column  6  count  4
  var _fill: String       as Fill       // column  7  count  5
  var _truncate: String   as Truncate   // column  8  count  6
  var _strip: String      as Strip      // column  9  count  7
  var _data: String       as Data       // column 10  count  8
  var _separator: String  as Separator  // column 11  count  9

  construct(aField : List<String>){
    _start      = aField.get(0)
    _length     = aField.get(1)
    _field      = aField.get(2)
    _format     = aField.get(3)
    _justify    = aField.get(4)
    _fill       = aField.get(5)
    _truncate   = aField.get(6)
    _strip      = aField.get(7)
    _data       = aField.get(8)
    _separator  = aField.get(9)
  }

  /**
   * Overriding the toString() method to show each individual field of the FieldSpec.
   */
  @Returns("A String containing each individual field of the FieldSpec")
  override function toString() : String {
    return "${Start} | ${Length} | ${Field} | ${Format} | ${Justify} | ${Fill} | ${Truncate} | ${Strip} | ${Data} | ${Separator}"
  }

}