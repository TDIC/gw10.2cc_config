package com.tdic.util.database

uses gw.pl.logging.LoggerCategory
uses java.util.Map
uses java.sql.Connection
uses java.sql.PreparedStatement
uses java.lang.StringBuilder
uses java.lang.Integer
uses gw.api.system.database.SequenceUtil
uses java.sql.ResultSet
uses java.sql.Statement
uses java.text.SimpleDateFormat
uses java.util.Date
uses java.util.SimpleTimeZone
uses java.math.BigDecimal
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory

/**
 * Abstract base class containing generic methods expediting writing WritableBeans to an external database in a uniform way
 * 
 * Methods take a map of fields,  which is the string name of the Gosu property paired with a map of properties on how to handle
 * that particular field.  Each field has a binder (block which knows how to bind the data type to an SQL statement), and the
 * sql column name for that Gosu property (See FIELD_BINDER, FIELD_SQL_NAME keys)
 * 
 * @author Guidewire
 */
abstract class IWritableBeanInfo<T> {

  /**
   * Class level logger
   */
  private static final var _logger = LoggerFactory.getLogger("TDIC_INTEGRATION")
  
  /**
   * value: FIELD_BINDER
   */
  public static final var FIELD_BINDER:String = "FIELD_BINDER"
  
  /**
   * value: FIELD_SQL_NAME
   */
  public static final var FIELD_SQL_NAME:String = "FIELD_SQL_NAME"
  
  /**
   * Abstract method which is called to get the name of the entity table from the concrete implementations
   * 
   * @return A String containing the name of the entity table
   */ 
  abstract property get TableName():String
  
  /**
   * Abstract method which is called to get the Gosu name of the ID property from the concrete implementations
   *
   * @return A String containing the Gosu name of the ID property
   */
  abstract property get IDColumnName():String

  /**
   * Abstract method which is called to get the SQL name of the ID property from the concrete implementations
   *
   * @return A String containing the SQL name of the ID property
   */
  abstract property get IDColumnSQLName():String

  /**
   * Abstract method which is called to get the SQL name of the Processed property from the concrete implementations
   *
   * @return A String containing the SQL name of the Processed property
   */
  abstract property get ProcessedColumnSQLName():String

  /**
   * Abstract method implemented by concrete implementations to load properties for the provided entity from the provided model
   * 
   * @param entity - The entity to populate
   * @param model - The GW XML model to load values from
   */  
  abstract function loadFromModel(entity:Object, model:Object)
  
  /**
   * Generic implementation of loading values from the model,  concrete classes wrap a call to this method when they implement the two parameter abstract version
   * 
   * @param entity - The entity to populate
   * @param model - The GW XML model to load values from
   * @param fields - fields map as described in the class header
   */
  protected function loadFromModel(entity:Object, model:Object, fields:Map<String,Map<String,Object>>) {
    for(entry in fields.entrySet()) {
      _logger.debug("loading ${entry.Key} from model")
      entity[entry.Key] = model[entry.Key]
      _logger.debug("value was [" + model[entry.Key] + "]")
    }
    // Load PublicID from model
    entity[IDColumnName] = model[IDColumnName]
  }

  /**
   * Abstract method implemented by concrete implementations to build a prepared statement that will insert a new entity into the database
   * 
   * @param conn - Database connection to use
   * @param entity - entity to persist
   * @return a PreparedStatement for persisting the entity
   */  
  abstract function createAndBindCreateSQL(conn:Connection,entity:T):PreparedStatement
  
  /**
   * Generic implementation, concrete classes wrap a call to this method when they implement the two parameter abstract version
   * 
   * @param conn - Database connection to use
   * @param entity - entity to persist
   * @param fields - fields map as described in the class header
   * @return a PreparedStatement for persisting the entity
   */
  protected function createAndBindCreateSQL(conn:Connection, entity:T, fields:Map<String,Map<String,Object>>):PreparedStatement {
    _logger.debug("CreateSQL for ${entity.Class.SimpleName}")
    var builder = new StringBuilder()
    builder.append("insert into ").append(TableName).append(" (")

    var secondaryBuilder = new StringBuilder()
    secondaryBuilder.append(" values(")
    
    var positions:Map<String,Integer> = {}
    var count:int = 1
    var iter = fields.entrySet().iterator()
    while(iter.hasNext()) {
      var entry = iter.next()
      builder.append(entry.Value.get(FIELD_SQL_NAME))
      secondaryBuilder.append("?")
      
      //Track where this is so we're not hand coding field positions
      positions.put(entry.Key, count as Integer)
      count++
      
      if(iter.hasNext()) {
        builder.append(",") 
        secondaryBuilder.append(",") 
      }
    }

    builder.append(",").append(IDColumnSQLName).append(",").append(ProcessedColumnSQLName).append(")")
    secondaryBuilder.append(",?").append(",0)")

    builder.append(secondaryBuilder.toString())

    _logger.debug("Generated SQL is: ${builder.toString()}")

    var stmt = conn.prepareStatement(builder.toString())
    
    for(entry in fields.entrySet()) {
      var fieldBinder = entry.Value.get(FIELD_BINDER) as block(stmt:PreparedStatement, pos:int, val:Object)
      
      fieldBinder(stmt, positions.get(entry.Key) as int, entity[entry.Key])
    }
    bindKey(stmt, fields.keySet().Count+1, entity)

    return stmt
  }
  
  /**
   * Function to bind the key of the provided entity at the proper position of the prepared statement that is being built
   *
   * @param stmt - PreparedStatement being built
   * @param position - Position to bind the provided value
   * @param entity - entity which is being persisted
   */
  protected function bindKey(stmt:PreparedStatement, position:int, entity:T) {
    // Use SequenceUtil for Public ID generation if not already set
    if (entity[IDColumnName] == null) {
      var key = SequenceUtil.next(0, (typeof entity).RelativeName)
      entity[IDColumnName] = key as String
    }
    _logger.debug("Binding key value of ${entity[IDColumnName]} to entity at position ${position}")
    stmt.setString(position, entity[IDColumnName] as String)
  }

  /**
   * Abstract method implemented by concrete implementations to build a prepared statement that will retrieve an entity from the database
   * 
   * @param conn - Database connection to use
   * @param entity - entity populated with publicid to load
   * @return a PreparedStatement to retrieve the entity
   */
  abstract function createAndBindRetrieveSQL(conn:Connection,entity:T):PreparedStatement

  /**
   * Generic implementation, concrete classes wrap a call to this method when they implement the two parameter abstract version
   * 
   * @param conn - Database connection to use
   * @param entity - entity populated with publicid to load
   * @param fields - fields map as described in the class header
   * @return a PreparedStatement to retrieve the entity
   */
  protected function createAndBindRetrieveSQL(conn:Connection, entity:T, fields:Map<String,Map<String,Object>>):PreparedStatement {
    _logger.debug("RetrieveSQL for ${entity.Class.SimpleName}")
    var builder = new StringBuilder()
    builder.append("select ")

    var iter = fields.entrySet().iterator()
    while(iter.hasNext()) {
      var entry = iter.next()
      
      builder.append(entry.Value.get(FIELD_SQL_NAME))
      if(iter.hasNext()) {
        builder.append(",")
      }
    }

    builder.append(" ").append(IDColumnSQLName)
    builder.append(" from ").append(TableName)
    builder.append(" where ").append(ProcessedColumnSQLName).append(" = ? AND ").append(IDColumnSQLName).append("=?")
    
    var stmt = conn.prepareStatement(builder.toString())
    stmt.setBoolean(1, false)
    stmt.setString(2, entity[IDColumnName] as String)

    _logger.debug("Generated SQL is: ${builder.toString()}")
    return stmt
  }
  
  /**
   * Abstract method implemented by concrete implementations to build a prepared statement that will retrieve all entities that are not marked as processed from the external database
   * 
   * @param conn - Database connection to use
   * @return a PreparedStatement to retrieve the entities
   */  
  abstract function createRetrieveAllSQL(conn:Connection):PreparedStatement
  
  /**
   * Generic implementation, concrete classes wrap a call to this method when they implement the two parameter abstract version
   * 
   * @param conn - Database connection to use
   * @param fields - fields map as described in the class header
   * @return a PreparedStatement to retrieve the entities
   */
  protected function createRetrieveAllSQL(conn:Connection, fields:Map<String,Map<String,Object>>):PreparedStatement {
    _logger.debug("RetrieveALLSQL for ${TableName}")
    var builder = new StringBuilder()
    builder.append("select ")

    var iter = fields.entrySet().iterator()
    while(iter.hasNext()) {
      var entry = iter.next()
      
      builder.append(entry.Value.get(FIELD_SQL_NAME))
      if(iter.hasNext()) {
        builder.append(",")
      }
    }

    builder.append(", ").append(IDColumnSQLName)
    builder.append(" from ").append(TableName)
    builder.append(" where ").append(ProcessedColumnSQLName).append(" = ?")
    
    //Make ResultSet scrollable to be able to check the number of rows in batch process
    var stmt = conn.prepareStatement(builder.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)
    stmt.setBoolean(1, false)
    
    _logger.debug("Generated SQL is: ${builder.toString()}")
    return stmt
  }

  /**
   * Abstract method implemented by concrete implementations to build a prepared statement that will update an entity in the database
   * 
   * @param conn - Database connection to use
   * @param entity - entity to persist
   * @return a PreparedStatement for persisting the entity
   */  
  abstract function createAndBindUpdateSQL(conn:Connection,entity:T):PreparedStatement

  /**
   * Generic implementation, concrete classes wrap a call to this method when they implement the two parameter abstract version
   * 
   * @param conn - Database connection to use
   * @param entity - entity to persist
   * @param fields - fields map as described in the class header
   * @return a PreparedStatement for persisting the entity
   */
  protected function createAndBindUpdateSQL(conn:Connection,entity:T,fields:Map<String,Map<String,Object>>):PreparedStatement {
    _logger.debug("UpdateSQL for ${entity.Class.SimpleName}")
    var builder = new StringBuilder()
    builder.append("update ").append(TableName).append(" set ")


    var iter = fields.entrySet().iterator()
    var count:int = 1
    var positions:Map<String, Integer> = {}
    
    while(iter.hasNext()) {
      var entry = iter.next()
      
      builder.append(entry.Value.get(FIELD_SQL_NAME)).append(" = ?")
      positions.put(entry.Key, count as Integer)
      count++
      
      if(iter.hasNext()) {
        builder.append(",")
      }
    }
    
    builder.append(" where ").append(IDColumnName).append("=?")
    var stmt = conn.prepareStatement(builder.toString())
    _logger.debug("Generated SQL is: ${builder.toString()}")

    for(entry in fields.entrySet()) {
      var fieldBinder = entry.Value.get(FIELD_BINDER) as block(stmt:Statement, pos:int, val:Object)
      
      fieldBinder(stmt, positions.get(entry.Key) as int, entity[entry.Key])
    }

    stmt.setString(fields.keySet().Count+1, entity[IDColumnName] as String)

    return stmt
  }

  /**
   * Abstract method implemented by concrete implementations to build a prepared statement that will mark the provided entity as processed to the external system
   * @param conn - Database connection to use
   * @param entity - entity to persist
   * @return a PreparedStatement for updating the entity as processed
   */  
  abstract function markProcessed(conn:Connection,entity:T):PreparedStatement
  
  /**
   * Generic implementation, concrete classes wrap a call to this method when they implement the two parameter abstract version
   * 
   * @param conn - Database connection to use
   * @param entity - entity to persist
   * @param fields - fields map as described in the class header
   * @return a PreparedStatement for updating the entity as processed
   */
  protected function markProcessed(conn:Connection,entity:T,fields:Map<String,Map<String,Object>>):PreparedStatement {
    _logger.debug("UpdateSQL for ${entity.Class.SimpleName}")

    var builder = new StringBuilder()
    var sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
    builder.append("update ").append(TableName).append(" set ").append(ProcessedColumnSQLName).append(" = ?")
    builder.append(" where ").append(IDColumnSQLName).append("=?")

    var stmt = conn.prepareStatement(builder.toString())

    _logger.debug("Generated SQL is: ${builder.toString()}")
    _logger.debug("key is ${entity[IDColumnName]}")

    stmt.setBoolean(1, true)
    stmt.setString(2, entity[IDColumnName] as String)

    return stmt
  }

  /**
   * Field binding block to take a prepared statement, position and value and bind a String into that position of the statement
   * 
   * @param stmt - PreparedStatement being built
   * @param pos - Position to bind the provided value
   * @param val - String value to bind to the statement
   */  
  public static var stringFieldBinder:block(stmt:PreparedStatement, pos:int, val:Object) = \ stmt, pos, val -> {
    _logger.debug("Binding ${val} at position ${pos}")
    stmt.setString(pos, val as String)
  }
  
  /**
   * Field binding block to take a prepared statement, position and value and bind a date into that position of the statement
   * 
   * @param stmt - PreparedStatement being built
   * @param pos - Position to bind the provided value
   * @param val - date value to bind to the statement
   */  
  public static var dateFieldBinder:block(stmt:PreparedStatement, pos:int, val:Object) = \ stmt, pos, val -> {
    _logger.debug("Binding ${val} at position ${pos}")
    if(val != null) {
      var d = Coercions.makeDateFrom(val)
      var sdf = new SimpleDateFormat()
      sdf.setTimeZone(new SimpleTimeZone(0, "GMT"))
      sdf.applyPattern("dd MMM yyyy HH:mm:ss z")
      stmt.setString(pos, sdf.format(d))
    }
  }
  
  /**
   * Field binding block to take a prepared statement, position and value and bind a boolean into that position of the statement
   * 
   * @param stmt - PreparedStatement being built
   * @param pos - Position to bind the provided value
   * @param val - boolean value to bind to the statement
   */  
  public static var booleanFieldBinder:block(stmt:PreparedStatement, pos:int, val:Object) = \ stmt, pos, val -> {
    _logger.debug("Binding ${val} at position ${pos}")
    stmt.setBoolean(pos, Coercions.makeBooleanFrom(val))
  }
  
  /**
   * Field binding block to take a prepared statement, position and value and bind a BigDecimal into that position of the statement
   * 
   * @param stmt - PreparedStatement being built
   * @param pos - Position to bind the provided value
   * @param val - BigDecimal value to bind to the statement
   */  
  public static var bigDecimalFieldBinder:block(stmt:PreparedStatement, pos:int, val:Object) = \ stmt, pos, val -> {
    _logger.debug("Binding ${val} at position ${pos}")
    stmt.setBigDecimal(pos, val as BigDecimal)
  }

  /**
   * Field binding block to take a prepared statement, position and value and bind a Integer into that position of the statement
   *
   * @param stmt - PreparedStatement being built
   * @param pos - Position to bind the provided value
   * @param val - Integer value to bind to the statement
   */
  public static var integerFieldBinder:block(stmt:PreparedStatement, pos:int, val:Object) = \ stmt, pos, val -> {
    _logger.debug("Binding ${val} at position ${pos}")
    stmt.setBigDecimal(pos, Coercions.makeIntFrom(val))
  }

}