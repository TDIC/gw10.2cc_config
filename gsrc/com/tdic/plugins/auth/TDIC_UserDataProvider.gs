package com.tdic.plugins.auth

uses java.util.Map

/**
 * US1132
 * 01/28/2015 Rob Kelly
 *
 * A User Data Provider.
 */
interface TDIC_UserDataProvider {

  public static final var FIRST_NAME : String = "Contact/FirstName"

  public static final var MIDDLE_NAME : String = "Contact/MiddleName"

  public static final var LAST_NAME : String = "Contact/LastName"

  public static final var WORK_PHONE: String = "Contact/WorkPhone"

  public static final var EMAIL_ADDRESS : String = "Contact/EmailAddress1"

  public static final var ADDRESS_LINE1 : String = "Contact/PrimaryAddress/AddressLine1"

  public static final var ADDRESS_LINE2 : String = "Contact/PrimaryAddress/AddressLine2"

  public static final var ADDRESS_LINE3 : String = "Contact/PrimaryAddress/AddressLine3"

  public static final var ADDRESS_CITY : String = "Contact/PrimaryAddress/City"

  public static final var ADDRESS_STATE : String = "Contact/PrimaryAddress/State"

  public static final var ADDRESS_POSTAL_CODE : String = "Contact/PrimaryAddress/PostalCode"

  public static final var ADDRESS_COUNTRY : String = "Contact/PrimaryAddress/Country"

  public static final var DEPARTMENT : String = "Department"

  public static final var JOB_TITLE : String = "JobTitle"

  public function getUserData(username : String) : Map<String, String>
}