/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package com.tdic.plugins.hpexstream.core.util

uses com.tdic.plugins.hpexstream.core.webservice.commandcenter.soapgateway.SOAPGatewayInterfaceService
uses com.tdic.plugins.hpexstream.core.webservice.commandcenteraction.soap.CCActionsInterface
uses com.tdic.util.properties.PropertyUtil
uses gw.util.concurrent.LockingLazyVar
uses org.slf4j.LoggerFactory

class TDIC_ExstreamHelper {
  private static var _theInstance = LockingLazyVar.make(\-> new TDIC_ExstreamHelper ())
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  private static var HPE_SERVICE_URL_PROP = "hpe.service.url"
  private static var HPE_OD_SERVICE_URL_PROP = "hpe.od.service.url"
  static property get Instance(): TDIC_ExstreamHelper {
    return _theInstance.get()
  }

  construct() {
    // Do nothing
  }

  /**
   * US555
   * 11/24/2014 shanem
   *
   * Gets the template Ids for the specified event Name and date
   */
  @Param("anEventName", "Event name to get associated Template Ids for")
  @Param("asOfDate", "Date the template should be in effect for")
  @Returns("List of associated template Ids")
  function getEventTemplates(anEventName: String, asOfDate: Date): HashMap<String, Integer> {
    _logger.debug("TDIC_ExstreamHelper#getEventTemplates() - entering")
    var eventTemplates = new ArrayList<ExstreamMapParam>()
    var mappingForEvent = TDIC_ExstreamPropertyUtil.getInstance().getEventTemplateMappings(anEventName) as List<ExstreamMapParam>
    for (mapping in mappingForEvent) {
      // US644, 11/24/2014, shanem: Filter out mappings that don't fall between the effective and expiration dates
      if ((mapping.EffectiveDate == null || mapping.EffectiveDate <= asOfDate)
          || (mapping.ExpirationDate == null || mapping.ExpirationDate >= asOfDate)) {
        eventTemplates.add(new ExstreamMapParam(){
            : EventName = mapping.EventName,
            : TemplateId = mapping.TemplateId,
            : EffectiveDate = mapping.EffectiveDate,
            : ExpirationDate = mapping.ExpirationDate,
            : PrintOrder = mapping.PrintOrder,
            : DocType = mapping.DocType
        })
      }
    }
    var templates = new HashMap<String, Integer>()
    _logger.debug("TDIC_ExstreamHelper#getEventTemplates() - Exiting")
    eventTemplates.each(\elt -> templates.put(elt.TemplateId, elt.PrintOrder))
    return templates
  }

  /**
   *
   * Kesava Tavva
   *
   * This function builds HP Exstream webservice isntance with externalized properties such as URL
   */
  @Returns("CCActionsInterface, Instance of HP Exstream Webservice interface service")
  public static function getDocProdService(): CCActionsInterface{
    var actionInterface: CCActionsInterface
    var serviceURL = PropertyUtil.getInstance().getProperty(HPE_SERVICE_URL_PROP)
    try{
      actionInterface = new com.tdic.plugins.hpexstream.core.webservice.commandcenteraction.soap.CCActionsInterface()
      actionInterface.Config.ServerOverrideUrl = serviceURL
    }catch(e : Exception){
      _logger.error("Unable to create HP Exstream Service instance with URL ${serviceURL}. ", e)
      throw "Unable to create HP Exstream Service instance with URL ${serviceURL}. Error Message: ${e.Message}"
    }
    return actionInterface
  }

  @Returns("CCActionsInterface, Instance of HP Exstream Webservice interface service")
  public static function getODDocProdService(): SOAPGatewayInterfaceService {
    _logger.trace("TDIC_ExstreamHelper#getODDocProdService() - entering")
    var actionInterface: SOAPGatewayInterfaceService
    var serviceURL = PropertyUtil.getInstance().getProperty(HPE_OD_SERVICE_URL_PROP)
    try{
      actionInterface = new SOAPGatewayInterfaceService()
      actionInterface.Config.ServerOverrideUrl = serviceURL//new URI(serviceURL)
    }catch(e : Exception){
      _logger.error("Unable to create HP Exstream Service instance with URL ${serviceURL}. ", e)
      throw "Unable to create HP Exstream Service instance with URL ${serviceURL}. Error Message: ${e.Message}"
    }
    _logger.debug("TDIC_ExstreamHelper#getODDocProdService() - HP Service URL: ${serviceURL}")
    _logger.trace("TDIC_ExstreamHelper#getODDocProdService() - exiting")
    return actionInterface
  }
}
