package com.tdic.plugins.hpexstream.core.util

uses com.tdic.util.properties.PropertyUtil
uses gw.api.system.server.ServerUtil
uses gw.pl.logging.LoggerCategory
uses com.tdic.plugins.hpexstream.core.bo.commandcenter.TDIC_CommandCenterActionRequest
uses com.tdic.plugins.hpexstream.core.messaging.TDIC_ExstreamTransport
uses java.lang.Exception
uses java.lang.StringBuilder
uses java.util.List
uses org.slf4j.LoggerFactory

/**
 * US669
 * 03/31/2015 Shane Murphy
 *
 * Method to handle sending of Document XML generated from events.
 */
class TDIC_DocumentEventSender {
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  /**
   * US555
   * 01/23/2015 Shane Murphy
   *
   * Extracts payload data from this staging area and sends them in a SOAP request to HP Exstream Command Center to start a composition job.
   */
  @Returns("1 if Successful")
  static function sendMessage(): int {
    return sendMessage(null)
  }

  /**
   * US555
   * 01/23/2015 Shane Murphy
   *
   */
  static function sendMessage(aPayload: String): int {
    _logger.debug("TDIC_DocumentEventSender#sendMessage() - Entering")
    var requestXML = aPayload == null ? getAggregateXML() : aPayload
    var _ret = 0
    try {
      if (requestXML != null) {
        requestXML = requestXML.remove("<?xml version=\"1.0\"?>")
        var wrappedXML = "<?xml version=\"1.0\"?><" + getProductArrayTag() + ">" + requestXML + "</" + getProductArrayTag() + ">"
        var sbBuilder : StringBuilder = new StringBuilder(wrappedXML)
        wrappedXML = windows1252ToIso8859(sbBuilder)
        var requestToCmd = createRequestObject(wrappedXML)
        _ret = sendRequest(requestToCmd)
      }
    } catch (responseException: Exception) {
      _logger.error("TDIC_DocumentEventSender#sendMessage() - Exception in Response from Command Center " + responseException.toString(), responseException)
      throw(responseException)
    }
    _logger.debug("TDIC_DocumentEventSender#sendMessage() - Exiting")
    return _ret
  }

  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * Retrieves all payloads stored in IntDB staging area for the current product's
   * batch document creation and concatenates all XML payload data together under
   * a single <XCenterArray> parent tag.
   * a single <XCenterArray> parent tag.
   */
  @Returns("Batch XML containing payloads from multiple events.")
  static function getAggregateXML(): String {
    _logger.trace("TDIC_DocumentEventSender#getAggregateXML() - Entering")
    var eventPayloads = {PropertyUtil.getInstance().getProperty(ServerUtil.getProduct().ProductName)}
    if (eventPayloads == null) {
      _logger.debug("TDIC_DocumentEventSender#getAggregateXML() - No payloads stored for batch")
      return null
    } else {
      var aggregateXmlBuilder = new StringBuilder()
      eventPayloads.each(\payload -> {
        aggregateXmlBuilder.append(payload.remove("<?xml version=\"1.0\"?>"))
      })
      var requestXML = "<" + getProductArrayTag() + ">" + aggregateXmlBuilder.toString() + "</" + getProductArrayTag() + ">"
      _logger.debug("TDIC_DocumentCreationBatch#getAggregateXML() - Request XML: ${requestXML}")
      _logger.trace("TDIC_DocumentCreationBatch#getAggregateXML() - Exiting")
      return requestXML
    }
  }

  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * Creates a request object for the batch payload.
   */
  @Param("aBatchXMLPayload", "Aggregate XML payload of all events to be sent.")
  @Returns("Request Object")
  static function createRequestObject(aBatchXMLPayload: String): TDIC_CommandCenterActionRequest {
    _logger.trace("TDIC_DocumentEventSender#createRequestObject(String) - Entering")
    var reqXMLinBase64 = gw.util.Base64Util.encode(aBatchXMLPayload.Bytes)
    var requestToCmd = new TDIC_CommandCenterActionRequest(null, getCCJobDefName(), "DRIVERFILE", reqXMLinBase64)
    _logger.debug("TDIC_DocumentEventSender#createRequestObject(String) - XML Attributes:")
    requestToCmd.getXMLModelAttributes().each(\attribute -> _logger.debug(attribute.asUTFString()))
    _logger.trace("TDIC_DocumentEventSender#createRequestObject(String) - Exiting")
    return requestToCmd
  }

  /**
   * US555
   * 02/16/2015 Shane Murphy
   *
   * Send the request object to HP Exstream and delete sent entries from staging area if successful
   */
  @Param("requestToCmd", "Request object to send")
  static function sendRequest(requestToCmd: TDIC_CommandCenterActionRequest): int {
    _logger.trace("TDIC_DocumentEventSender#sendRequest(TDIC_CommandCenterActionRequest) - Entering")
    var _ret = 0
    try {
      var actionInterface = TDIC_ExstreamHelper.getDocProdService()
      var responseFromCMD = actionInterface.action("CREATE_JOB", requestToCmd.getXMLModelAttributes())
      _logger.debug("TDIC_DocumentEventSender#sendRequest(TDIC_CommandCenterActionRequest) - Response from Command Center" + responseFromCMD.asUTFString())
      if (responseFromCMD != null and responseFromCMD.Attribute.where(\a -> a.Name == "status_code" and a.Value_Attribute == "1001").Empty == false) {
        _ret = 1
        _logger.trace("TDIC_DocumentEventSender#sendRequest(TDIC_CommandCenterActionRequest) - Successfully consumed")
      }
    } catch (var responseException: Exception) {
      _logger.error("TDIC_DocumentEventSender#sendRequest(TDIC_CommandCenterActionRequest) - Exception in Response from Command Center " + responseException.toString(), responseException)
      throw(responseException)
    }
    _logger.trace("TDIC_DocumentEventSender#sendRequest(TDIC_CommandCenterActionRequest) - Exiting")
    return _ret
  }

  /**
   *  US555
   *  10/13/2014 shanem
   *
   *  A helper method to retrieve CCJobDefName attribute of the current TDIC_ExstreamTransport plugin.
   *  Currently this attribute is configured as the plugin's parameter and during instantiation of the ExstreamTransport class its static variable <b>CCJobDefName</b> is updated.
   *
   *  <b>Note:</b> instead of using static variable (which limits number of instances of such Transport class to 1.
   *  These parameters should be retrieved via {@link gw.plugin.Plugins#get(java.lang.String pluginName)}.
   *  This is not possible as the Plugin retrieved this way cannot be cast to TDIC_ExstreamTransport class in v8.0.1.
   */
  @Returns("CCJobDefName attribute of current TDIC_ExstreamTransport plugin")
  static function getCCJobDefName(): String {
    return TDIC_ExstreamTransport.CCJobDefName
  }

  /**
   * US669
   * 04/01/2015 Shane Murphy
   *
   * Previously, requests were sent as batch requests, where each request contained
   * multiple TDIC_CCBusinessObject entities. For this, we needed well formed XML
   * to send across. This Array tage provided that structure.
   *
   * Since removing this batch process of sending requests, HP Exstream still
   * requires this tag to be passed with each request
   */
  @Returns("Wrapping Array Tag for this Product")
  static function getProductArrayTag(): String {
    switch (ServerUtil.Product.ProductCode.toUpperCase()) {
      case("PC"):
          return "PolicyArray"
      case("BC"):
          return "BillingArray"
      case("CC"):
          return "ClaimArray"
        default:
        return "Array"
    }
  }

  /**
   *  According to the article at http://en.wikipedia.org/wiki/Windows-1252 there
   *  are 27 non-standard unicode mappings used by Microsoft's Windows-1252 character encoding.
   *  This method can replaces a subset of those with characters that are can store properly in a
   *  database that uses ISO-8859-1 character encoding, for example an Oracle 10g database created
   *  using its character encoding defaults.
   * @param sbOriginal
   */
  public static function windows1252ToIso8859(sbOriginal : StringBuilder) : String {
    if (null==sbOriginal) {
      return null;
    }
    for( isb in 0..(sbOriginal.length()-1)) {
      var origCharAsInt = sbOriginal.charAt(isb) as int;
      switch (origCharAsInt) {
        case ('\u2018' as int):  sbOriginal.setCharAt(isb, '\''); break;  // left single quote
        case ('\u2019' as int):  sbOriginal.setCharAt(isb, '\''); break;  // right single quote
        case ('\u201A' as int):  sbOriginal.setCharAt(isb, '\''); break;  // lower quotation mark

        case ('\u201C' as int):  sbOriginal.setCharAt(isb, '"'); break;  // left double quote
        case ('\u201D' as int):  sbOriginal.setCharAt(isb, '"'); break;  // right double quote
        case ('\u201E' as int):  sbOriginal.setCharAt(isb, '"'); break;  // double low quotation mark

        case ('\u2039' as int):  sbOriginal.setCharAt(isb, '\''); break;  // Single Left-Pointing Quotation Mark
        case ('\u203A' as int):  sbOriginal.setCharAt(isb, '\''); break;  // Single right-Pointing Quotation Mark

        case ('\u02DC' as int):  sbOriginal.setCharAt(isb, '~'); break;  // Small Tilde

        case ('\u2013' as int):  sbOriginal.setCharAt(isb, '-'); break;  // En Dash
        case ('\u2014' as int):  sbOriginal.setCharAt(isb, '-'); break;  // EM Dash

          default: break;
      }
    }
    return sbOriginal.toString();
  }
}