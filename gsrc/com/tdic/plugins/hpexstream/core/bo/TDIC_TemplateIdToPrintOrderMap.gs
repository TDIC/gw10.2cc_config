package com.tdic.plugins.hpexstream.core.bo

class TDIC_TemplateIdToPrintOrderMap {

  private var _templateId: String as TemplateId
  private var _printOrder: int as PrintOrder

  construct(aTemplateId: String, aPrintOrder: int) {
    _templateId = aTemplateId
    _printOrder = aPrintOrder
  }

}