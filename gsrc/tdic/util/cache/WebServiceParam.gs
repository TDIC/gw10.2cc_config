package tdic.util.cache
/**
 * User: skiriaki
 * Date: 8/27/14
 */
class WebServiceParam {
  var _endPoint : String as EndPoint
  var _userName : String as UserName
  var _password : String as Password
}