package tdic.util.gunit.test

uses gw.api.system.server.Runlevel
uses gw.testharness.RunLevel
uses gw.testharness.ServerTest
uses gw.testharness.TestBase
uses tdic.util.gunit.test.stubs.MethodProtectionTest
uses tdic.util.gunit.TextTestRunner

@ServerTest
@RunLevel(Runlevel.NONE)
class MethodProtectionTestTest extends TestBase {

  function testMethodsRunInATest() {
    var runner = new TextTestRunner()

    runner.runTestsInClass(MethodProtectionTest)

    assertEquals("[testMethodThatShouldRun]", MethodProtectionTest.TestMethodsThatRan.toString())
  }

}
