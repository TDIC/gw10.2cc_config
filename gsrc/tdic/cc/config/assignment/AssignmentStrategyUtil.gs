package tdic.cc.config.assignment

uses gw.api.database.Query
uses gw.api.system.CCLoggerCategory

/**
 * GWNW 45
 * 07/17/2020 Harish Barrenka
 * Util class for all Assignment Strategy functionalities
 */
class AssignmentStrategyUtil {

  var logger = CCLoggerCategory.ASSIGNMENT

  /*
   * @param assignmentStrategy - AssignmentStrategyTDIC record to be removed.
   *
   * Please call this method if you are in read only mode. This method adds the passed object to bundle, then deletes and commits to DB.
   */
  function removeAssignmentStrategy(assignmentStrategy: AssignStrategy_TDIC){
    if(gw.transaction.Transaction.Current!=null){
      assignmentStrategy = gw.transaction.Transaction.Current.add(assignmentStrategy)
      assignmentStrategy.remove()
      gw.transaction.Transaction.Current.commit()
    }
  }

  /*
   * @param assignmentStrategies - AssignmentStrategyTDIC records to be removed.
   *
   * Please call this method if you are in read only mode. This method adds the passed object to bundle, then deletes and commits to DB.
   */
  function removeAssignmentStrategies(assignmentStrategies: AssignStrategy_TDIC[]){
    if(assignmentStrategies.Count>0 and gw.transaction.Transaction.Current!=null) {
      for(assignmentStrategy in assignmentStrategies){
        assignmentStrategy = gw.transaction.Transaction.Current.add(assignmentStrategy)
        assignmentStrategy.remove()
      }
      gw.transaction.Transaction.Current.commit()
    }
  }

  /*
   * @param claim - Claim to be assigned to user based on Strategy configured.
   * @return boolean - true if any one of the assigned strategy with AssignTo and Queue configuration successful otherwise returns false.
   */
  function assign(claim: Claim) : boolean{
    return (claim!=null ? (assignByLoggedInUser(claim) ? true : assignByQueue(claim)) : false)
  }

  /*
   * @param claim - Claim to be assigned to user based on Strategy configured.
   * @return boolean - true if there is any group configured with AssignTo LoggedInUser and if logged in user
   *                    is the member of that configured after assigning the claim to user, otherwise returns false
   */
  private function assignByLoggedInUser(claim: Claim) : boolean{
    logger.info("assignByLoggedInUser(${claim}) - start")
    var searchCriteria = new ASSearchCriteria()
    searchCriteria.AssignTo = ASAssignTo_TDIC.TC_LOGGEDINUSER
    var assignmentStrategy = retrieveStrategy(searchCriteria)
    if(assignmentStrategy!=null){
      logger.info("assignByLoggedInUser(${claim}) - Assignment Strategy exist with group name ${assignmentStrategy.GroupName}")
      var userGroups = User.util.CurrentUser.AllGroups
      if(not userGroups.Empty){
        var group = userGroups.firstWhere(\group -> group!=null and group typeis Group and group.Name==assignmentStrategy.GroupName)
        if(group!=null and group typeis Group){
          logger.debug("assignByLoggedInUser(${claim}) - Assigned to Group ${group.Name} and User ${User.util.CurrentUser}")
          return claim.assign(group, User.util.CurrentUser)
        }
      }
    }
    logger.info("assignByLoggedInUser(${claim}) - not successful")
    return false
  }

  /*
   * @param claim - Claim to be assigned to user based on Strategy configured.
   * @return boolean - true if there is any group and Queue configured with AssignTo as Queue, claim type as passed Claim Type
   *                    after assigning the claim to that queue in the group, otherwise returns false
   */
  private function assignByQueue(claim: Claim) : boolean{
    logger.info("assignByQueue(${claim}) - start")
    var searchCriteria = new ASSearchCriteria()
    searchCriteria.AssignTo = ASAssignTo_TDIC.TC_QUEUE
    searchCriteria.ClaimType = claim.Type_TDIC
    var assignmentStrategy = retrieveStrategy(searchCriteria)
    if(assignmentStrategy.GroupName!=null and assignmentStrategy.QueueName!=null){
      logger.info("assignByQueue(${claim}) - Assignment Strategy exist with group name ${assignmentStrategy.GroupName} and Queue name ${assignmentStrategy.QueueName}")
      var userGroups = User.util.CurrentUser.AllGroups
      if((not userGroups.Empty) and userGroups.hasMatch(\grpObj -> grpObj != null and grpObj typeis Group and grpObj.DisplayName == assignmentStrategy.GroupName)) {
        var group = retrieveGroup(assignmentStrategy.GroupName)
        if(group!=null){
          var queue = group.getQueue(assignmentStrategy.QueueName)
          if(queue!=null){
            logger.debug("assignByQueue(${claim}) - Assigned to Group ${group.Name} and Queue ${queue}")
            claim.CurrentAssignment.AssignedGroup = group
            claim.CurrentAssignment.AssignedQueue = queue
            return true
          }
        }
      }
    }
    logger.info("assignByQueue(${claim}) - not successful")
    return false
  }

  /*
   * @param claim - Claim object on which review assignment activity has to be created.
   */
  public function reviewAssignmentActivity(claim: Claim){
    var activityPattern = Query.make(ActivityPattern).compare(ActivityPattern#Code, Equals, "assignment_review").select().FirstResult
    if(activityPattern!=null){
      var activity = claim.createActivityFromPattern(null, activityPattern)
      activity.AssignedGroup = claim.AssignedGroup
      activity.AssignedQueue = claim.AssignedQueue
    }
  }

  /*
   * @param groupName - name to check whether group exist or not.
   * @return Group - Group object if group exist with the passed name otherwise returns null.
   */
  private function retrieveGroup(groupName : String) : Group{
    return Query.make(Group).compare(Group#Name, Equals, groupName).select().FirstResult
  }

  /*
   * @param searchCriteria - ASSearchCriteria object to filter records based on this object properites set.
   * @return AssignmentStrategry_TDIC - first record after applying cretiria passed.
   */
  private function retrieveStrategy(searchCriteria : ASSearchCriteria) : AssignStrategy_TDIC{
    return Query.make(AssignStrategy_TDIC)
            .compare(AssignStrategy_TDIC#AssignTo, Equals, searchCriteria.AssignTo)
            .compare(AssignStrategy_TDIC#ClaimType, Equals, searchCriteria.ClaimType).select().FirstResult
  }

  /*
   * @param name - name of the queue in string format to check this Queue is configured in Assignment Strategy or not.
   * @return boolean - true if the queue is not configured in Assignment Strategy otherwise false.
   */
  function queueNotExistInStrategy(name : String) : boolean{
    return Query.make(AssignStrategy_TDIC)
        .compare(AssignStrategy_TDIC#QueueName, Equals, name).select().Empty
  }
}