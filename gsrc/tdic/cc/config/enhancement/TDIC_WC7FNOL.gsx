package tdic.cc.config.enhancement

/**
 * US537
 * 08/28/2014 Rob Kelly
 *
 * Enhancement providing methods to create exposures for TDIC Worker's Comp WC7 claims based on relevant fields on the claim at FNOL.
 */
enhancement TDIC_WC7FNOL : entity.Claim {

  /**
   * Create exposures based on:
   * <ul>
   * <li>Medical or incident report flags set - create Medical Details exposure
   * <li>Time loss or death report flags set - create Time Loss exposure
   * <li>Employer liability flag set - create Employers Liability exposure
   * </ul>
   */
  function createRelevantWC7Exposures_TDIC() {
    // open WC Medical Exp if this is not an Incident Only report OR if Medical Report is specifically specified
    if (this.MedicalReport or !this.IncidentReport){
      createAndSetUpExposure(CoverageType.TC_WC7WORKERSCOMPEMPLIABINSURANCEPOLICYACOV, CoverageSubtype.TC_WC7WORKERSCOMP_MED_TDIC)
    }

    // open WC Time Loss Exp if Indemnity/TimeLossReport field is checked
    if (this.TimeLossReport or this.DeathReport){
      createAndSetUpExposure(CoverageType.TC_WC7WORKERSCOMPEMPLIABINSURANCEPOLICYACOV, CoverageSubtype.TC_WC7WORKERSCOMP_WAGES_TDIC)
    }

    // open WC Employer's Liability Exp
    if (this.EmployerLiability) {
      createAndSetUpExposure(CoverageType.TC_WC7WORKERSCOMPEMPLIABINSURANCEPOLICYACOV, CoverageSubtype.TC_WC7WORKERSCOMP_EMPLIAB_TDIC)
    }

    linkWC7ExposuresToCoverages()

    ensureNoGapsInExposureNumbers()
  }

  /**
   * Create exposure with given coverage type and coverage subtype, unless it
   * is already present. If the claim is already open then set up the exposure
   * immediately (if the claim is draft then it will be set up at the end of
   * the new claim wizard)
   */
  private function createAndSetUpExposure(coverageType : CoverageType, coverageSubtype : CoverageSubtype) {
    if (not this.hasExposureOfType(this.getNewExposureType(coverageSubtype))) {
      var exposure = this.newExposure(coverageType, coverageSubtype, true)
      if (this.State == typekey.ClaimState.TC_OPEN) {
        exposure.saveAndSetup()
      }
    }
  }

  /**
   * Set the Coverage on created Exposures.
   * Note that for TDIC all Exposures should be created against the Workers' Compensation And Employers' Liability Insurance Policy (Section 3B) Coverage.
   */
  private function linkWC7ExposuresToCoverages() {
    for (var exposure in this.Exposures) {
      for (coverage in exposure.Claim.Policy.getAllCoverages()) {
        if (coverage.Type == CoverageType.TC_WC7WORKERSCOMPEMPLIABINSURANCEPOLICYACOV) {
          exposure.Coverage = coverage
          break
        }
      }
    }
  }

  /**
   * Renumber created Exposures to ensure no gaps in ClaimOrder numbering.
   */
  private function ensureNoGapsInExposureNumbers() {
    var order = 1
    foreach (e in this.OrderedExposures) {
      e.ClaimOrder = order
      order++
    }
  }
}
