package tdic.cc.config.contacts.name

uses gw.api.name.ContactNameFields
uses gw.api.name.NameOwnerFieldId
uses java.lang.StringBuffer
uses gw.api.name.NameLocaleSettings

/**
 * US1031, DE66
 * 01/06/2015 Rob Kelly
 *
 * Formatter for person names at TDIC.
 */
class TDIC_NameFormatter {

  final var SPACE_DELIMITER = " "
  final var PERIOD_DELIMITER = "."
  final var COMMA_DELIMITER = ", "

  construct() {
    // Nothing to do
  }

  /**
   * Converts the specified ContactNameFields to a displayable string.
   */
  public function format(name : ContactNameFields) : String {
    return internalFormat(name)
  }

  /**
   * Appends the specified field value to the name prefixed with delimiter if required.
   */
  private function append(sb : StringBuffer, fieldId : NameOwnerFieldId, delimiter : String, value : String) {
    if (value.HasContent and TDIC_NameLocaleSettings.TDIC_DEFAULT_DISPLAY_NAME_FIELDS.contains(fieldId)) {
      if (sb.length() > 0) {
        sb.append(delimiter)
      }
      sb.append(value)
    }
  }

  /**
   * Returns value1 if it is non-empty; value2 otherwise.
   */
  private function firstNonEmpty(value1 : String, value2 : String) : String {
    return value1.HasContent ? value1 : value2
  }

  /**
   * Converts the specified ContactNameFields to a displayable string.
   */
  private function internalFormat(name : ContactNameFields) : String {

    if (name == null) {
      return null
    }

    var fieldId = TDIC_NameOwnerFieldId
    var sb = new StringBuffer()
    var mode = NameLocaleSettings.TextFormatMode

    if (not (name typeis TDIC_PersonNameFieldsImpl)) {
      return name.NameKanji.HasContent ? name.NameKanji : name.Name
    }
    var pName = name as TDIC_PersonNameFieldsImpl

    var lastName = firstNonEmpty(pName.LastNameKanji, pName.LastName)
    var firstName = firstNonEmpty(pName.FirstNameKanji, pName.FirstName)
    if (mode == "default") {
      // common case
      append(sb, fieldId.PREFIX, SPACE_DELIMITER, pName.Prefix.DisplayName)
      append(sb, fieldId.FIRSTNAME, SPACE_DELIMITER, firstName)
      append(sb, fieldId.MIDDLENAME, SPACE_DELIMITER, pName.MiddleName)
      append(sb, fieldId.LASTNAME, SPACE_DELIMITER, pName.Particle)
      append(sb, fieldId.LASTNAME, SPACE_DELIMITER, lastName)
      append(sb, fieldId.LASTNAME, COMMA_DELIMITER, pName.Suffix.DisplayName)
      append(sb, fieldId.CREDENTIAL, COMMA_DELIMITER, pName.Credential_TDIC.DisplayName)
    } else if (mode == "Japan") {
      append(sb, fieldId.LASTNAME, SPACE_DELIMITER, lastName)
      append(sb, fieldId.FIRSTNAME, SPACE_DELIMITER, firstName)
    }
    return sb.toString()
  }
}