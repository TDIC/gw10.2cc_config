package tdic.cc.config.validation

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException

class BulkInvoiceHelper_TDIC {

  static function validateBulkInvoiceNumber(bulkInvoiceNumber:String){
    var bulkInvoice = Query.make(BulkInvoice).compare(BulkInvoice#InvoiceNumber, Relop.Equals,bulkInvoiceNumber).select().first()
    if(bulkInvoice != null){
      throw new DisplayableException(DisplayKey.get("TDIC.Web.bulkInvoice.Validation.Error"))
    }
  }
}