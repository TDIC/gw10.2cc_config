uses java.io.FileReader
uses java.io.BufferedReader

var gwProductCode = GWProductCode;

var cleanseAdminData = tdic.cc.common.admindata.CleanseAdminData.getInstance(gwProductCode);
cleanseAdminData.cleanseAdminData();
print ("Done.");

/**
 * Determine the Guidewire product code by examining the product.properties file.
 */
property get GWProductCode() : String {
  var product : String;

  var propertyFilename = "../../../product.properties";
  var reader = new BufferedReader (new FileReader (propertyFilename));

  var line = reader.readLine();
  while (line != null) {
    if (line.startsWith("product.code=")) {
      product = line.substring(13);
      break;
    }
    line = reader.readLine();
  }

  return product;
}