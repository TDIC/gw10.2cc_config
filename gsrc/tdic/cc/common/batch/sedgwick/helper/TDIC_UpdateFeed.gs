package tdic.cc.common.batch.sedgwick.helper

uses java.util.HashMap
uses java.util.ArrayList
uses java.util.Properties
uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.locale.DisplayKey
uses gw.pl.exception.GWConfigurationException
uses gw.webservice.cc.cc1000.dto.NoteDTO
uses tdic.cc.common.batch.sedgwick.dto.FlatFileContents
uses java.lang.Exception
uses gw.api.database.Query
uses java.text.SimpleDateFormat
uses gw.webservice.cc.cc1000.claim.ClaimAPI
uses java.util.Locale
uses gw.transaction.Transaction
uses java.lang.Float
uses java.math.BigDecimal
uses gw.webservice.cc.cc1000.dto.ExposureDTO
uses gw.webservice.cc.cc1000.financials.ClaimFinancialsAPI
uses gw.webservice.cc.cc1000.dto.CheckPayeeDTO
uses gw.webservice.cc.cc1000.dto.TransactionLineItemDTO
uses gw.webservice.cc.cc1000.dto.TransactionDTO
uses gw.webservice.cc.cc1000.dto.CheckDTO
uses gw.webservice.cc.cc1000.dto.TransactionSetDTO
uses java.lang.Integer
uses gw.api.util.TypecodeMapper
uses java.lang.StringBuilder
uses gw.api.financials.CurrencyAmount
uses java.lang.Math
uses java.math.RoundingMode
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: Praneethk
 * Date: 3/1/15
 * Time: 12:52 AM
 * US674 Claim updates from Sedgwick
 * This Class is a helper class and contains main functionality of updating the feed to CC.
 */
class TDIC_UpdateFeed {

  private static var _logger = LoggerFactory.getLogger("TDIC_SEDGWICK")
  public var multiMap:HashMap<String,HashMap<String, ArrayList<HashMap<String, String>>>>
  public var multiLineMap:HashMap<String,HashMap<String, ArrayList<String>>>
  public var flatFileContents:FlatFileContents
  public var payCodesToBeSkipped : String[] = {"700","702","703","704","705","706","707","708","714","715","716","717",
      "725","727","728","729","730","731","732","733","739","740","741","742","775","777","778","779","780","781","782",
      "783","789","790","791","792","700","701","702","703","706","707","709","710","711","712","713","714","715","775",
      "776","777","778","781","782","784","785","786","787","788","789","790","791","792","704","706","714","729","779",
      "781","789"}
  var su : User
  var properties:Properties                                                           //Variable to hold properties file
  public var codeMap:HashMap<String,HashMap<String,String>>
  var mapper:TypecodeMapper
  public var _errorString:StringBuilder
  private var _emailTo : String as readonly EmailTo
  public static final var SEDGWICK_SKIP_PAYMENT_EMAIL : String = "sedgwick.paymentskip.email"

  construct(_sedgwickFileName: String){

    var headerPath = PropertyUtil.getInstance().getProperty("headerPath")
    var excelPath = PropertyUtil.getInstance().getProperty("sedgwick.updateFromSedgwickExcelPath")
    _logger.debug("TDIC_UpdateFeed#construct() - Fetching Appropriate Files")
    _logger.debug(_sedgwickFileName)
    var flatFileUtility = new TDIC_FlatFileUtilityHelper()
    flatFileContents = flatFileUtility.decode(excelPath, headerPath, _sedgwickFileName) //Decode the flat file
    multiMap=flatFileContents.multipleFlatFileContentsMap //Parsed flat file in a hash map
    multiLineMap=flatFileContents.multipleFlatFileLineMap //Unparsed lines of flat file to be used to write to error file
    su = Query.make(User).join("Credential").compare("UserName", Equals, "iu").select().AtMostOneRow
    _errorString = new StringBuilder()
    mapper = gw.api.util.TypecodeMapperUtil.getTypecodeMapper()
  }

  /*
   * A method to update the errorMap with the respective line.
   * This method is called when updating a particular line throws an exception.
   */
  public function failedUpdateMap(errorLine:String){
    _logger.debug("Updating error map from "+errorLine)
    if(_errorString.indexOf(errorLine)==-1){
       _errorString.append(errorLine)
       _errorString.append("\n")
    }
  }

  public function getFailedMapSize():int{
    return _errorString.length()
  }

  /*
   * A helper method that goes through the flatfile HashMap and calls the respective methods for each header of each claim in flat file.
   * If a particular header can't be processed, the method calls appropriate methods to update errorMap.
   * Returns the errorMap.
   */

  public function updateAll():String{
    _logger.info("Entering updateAll() -----> TDIC_Update#updateAll()")
    multiMap = changeLegacyClaimNumber(multiMap)
    for(var claimNumber in multiMap.keySet()){   //For each claimNumber in the flat file
      _logger.info("ClaimNumber" + claimNumber + "Started To Update")
      var map = multiMap.get(claimNumber)
      var lineMap = multiLineMap.get(claimNumber)

      var isFailed = false

      if(map.containsKey("CLM")){
        _logger.info("Entering to update CLM of ClaimNumber" + claimNumber)
        var claimLine =lineMap.get("CLM").get(0) //There is only one claim header per claim in a flat file
        var claimMap = map.get("CLM").get(0)
        try{
          _logger.info("Entering to assign User for ClaimNumber" + claimNumber)
          assignUser(claimMap)
          updateClaimInformation(claimMap)
          _logger.info("Exiting update CLM of ClaimNumber" + claimNumber)
        }
        catch(p:Exception){
          _logger.error("Error Found in updating CLM of ClaimNumber" + claimNumber)
          failedUpdateMap(claimLine) //Update errorMap if there is an error
          isFailed = true
          _logger.info("Writing to Error File, CLM of ClaimNumber" + claimNumber)
        }
       }

      var index = 0            //Index to access the corresponding line from lineMap
      if(map.containsKey("NOT")){
        _logger.info("Entering to update NOT of ClaimNumber" + claimNumber)
        var noteMapList = map.get("NOT")
        for(var noteMap in noteMapList){  //Go through each noteMap
          try{
            _logger.info("Entering to adding NOT for ClaimNumber" + claimNumber)
            addNote(noteMap)         //Add note
            _logger.info("Exiting adding NOT for ClaimNumber" + claimNumber)
          }
          catch(p : Exception){
            _logger.error("Error Found in updating NOT of ClaimNumber" + claimNumber)
            failedUpdateMap(lineMap.get("NOT").get(index)) //If error, get corresponding line and update errorMap
            isFailed = true
          }
          index++
        }
        _logger.info("Exiting update NOT of ClaimNumber" + claimNumber)
      } 


      index=0
      if(map.containsKey("RES")){
        _logger.info("Entering to add RES of ClaimNumber" + claimNumber)
        var resMapList = map.get("RES")
        for(var resMap in resMapList){
          try{
            _logger.info("Entering to CREATE RES for ClaimNumber" + claimNumber)
            createReserves(resMap)
            _logger.info("Exiting to CREATE RES for ClaimNumber" + claimNumber)
          }
          catch(p: Exception){
            _logger.error("Error Found in updating RES of ClaimNumber" + claimNumber)
            failedUpdateMap(lineMap.get("RES").get(index))
            isFailed = true
          }
          index++
        }
        _logger.info("Exiting to add RES of ClaimNumber" + claimNumber)
      } 


      index=0
      if(map.containsKey("PAY")){
        _logger.info("Entering to add PAY of ClaimNumber" + claimNumber)
        var payMapList = map.get("PAY")
        for(var payMap in payMapList){
          try{
            if(payCodesToBeSkipped.contains(payMap.get("TransactionView.CostCategory"))){
                _logger.info("Pay code received : " + (payMap.get("TransactionView.CostCategory")) + "lies in range 700-799. Processing is skipped ")
                _emailTo = PropertyUtil.getInstance().getProperty(SEDGWICK_SKIP_PAYMENT_EMAIL)
                if (_emailTo == null) {
                  throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"+SEDGWICK_SKIP_PAYMENT_EMAIL+"' from properties file.")
                }
                EmailUtil.sendEmail(_emailTo, DisplayKey.get("TDIC.Web.Plugin.Sedgwick.Email.SkipPayment.Subject",gw.api.system.server.ServerUtil.ServerId),
                  DisplayKey.get("TDIC.Web.Plugin.Sedgwick.Email.SkipPayment.Desc",claimNumber))
            } else{
                _logger.info("Entering to CREATE PAY for ClaimNumber" + claimNumber)
                createPayment(payMap)
                _logger.info("Exiting to CREATE PAY for ClaimNumber" + claimNumber)
            }
          }
          catch(p:Exception){
            _logger.error("Error Found in updating PAY of ClaimNumber" + claimNumber)
            failedUpdateMap(lineMap.get("PAY").get(index))
            isFailed = true
          }
          index++
        }
        _logger.info("Exiting to add PAY of ClaimNumber" + claimNumber)
      } 

      index=0
      if(map.containsKey("WD1")){
        _logger.info("Entering to add WD1 for ClaimNumber" + claimNumber)
        var wdMapList = map.get("WD1")
        for(var wdMap in wdMapList){
          try{
            _logger.info("Entering to UPDATE WD1 for ClaimNumber" + claimNumber)
            updateWD1(wdMap)
            _logger.info("Exiting to UPDATE WD1 for ClaimNumber" + claimNumber)
          }
          catch(p:Exception){
            _logger.error("Error Found in updating WD1 of ClaimNumber" + claimNumber + p.Message)
            failedUpdateMap(lineMap.get("WD1").get(index))
            isFailed = true
          }
          index++
        }
        _logger.info("Exiting to add WD1 of ClaimNumber" + claimNumber)
      }

      index=0
      if(map.containsKey("LEG")){
        _logger.info("Entering to add LEG for ClaimNumber" + claimNumber)
        var legalMap = map.get("LEG").get(0)
        try{
          _logger.info("Entering to UPDATE LEG for ClaimNumber" + claimNumber)
          updateLegal(legalMap)
          _logger.info("Exiting to UPDATE LEG for ClaimNumber" + claimNumber)
        }
        catch(p:Exception){
          _logger.error("Error Found in updating LEG of ClaimNumber" + claimNumber)
          failedUpdateMap(lineMap.get("LEG").get(index))
          isFailed = true
        }
        index++
        _logger.info("Exiting to add LEG of ClaimNumber" + claimNumber)
      }

      index=0
      if(map.containsKey("REC")){
        _logger.info("Entering to add REC for ClaimNumber" + claimNumber)
        var recoveryMap = map.get("REC").get(0)
        try{
          _logger.info("Entering to UPDATE REC for ClaimNumber" + claimNumber)
          updateRecovery(recoveryMap)
          _logger.info("Exiting to UPDATE REC for ClaimNumber" + claimNumber)
        }
        catch(p:Exception){
          _logger.error("Error Found in updating REC of ClaimNumber" + claimNumber)
          failedUpdateMap(lineMap.get("REC").get(index))
          isFailed = true
        }
        index++
        _logger.info("Exiting to add REC for ClaimNumber" + claimNumber)
      }

      if(isFailed){
        //If any lines failed, do not close the claim. Instead add the claim line to failed lines so that the claim will be closed later
        failedUpdateMap(lineMap.get("CLM").get(0))
      }
      else{
        if(map.containsKey("CLM")){
          _logger.info("Entering to Close CLM for ClaimNumber" + claimNumber)
          var claimLine =lineMap.get("CLM").get(0) //There is only one claim header per claim in a flat file
          var claimMap = map.get("CLM").get(0)
          try{
            _logger.info("Entering to CLOSE CLM for ClaimNumber" + claimNumber)
            closeClaim(claimMap)       //Claims must be closed last whereas reopening must occur first
            _logger.info("Exiting to CLOSE CLM for ClaimNumber" + claimNumber)
          }
          catch(p:Exception){
            _logger.error("Error Found in closing CLM of ClaimNumber" + claimNumber)
            failedUpdateMap(claimLine) //Update errorMap if there is an error
          }
          try {
            _logger.info("Entering to Update CloseDate CLM for ClaimNumber" + claimNumber)
            closeClaimDate(claimMap)       //Claims must be closed last whereas reopening must occur first
            _logger.info("Exiting to Update CloseDate CLM for ClaimNumber" + claimNumber)
          }
          catch(p:Exception){
            _logger.error("Error Found in closing CLM of ClaimNumber" + claimNumber)
            failedUpdateMap(claimLine) //Update errorMap if there is an error
          }
          _logger.info("Exiting to Close CLM for ClaimNumber" + claimNumber)
        }
      }
    }
   return _errorString.toString()
  }

  public function changeLegacyClaimNumber(migMultiMap:HashMap<String,HashMap<String,ArrayList<HashMap<String, String>>>>):HashMap<String,HashMap<String,ArrayList<HashMap<String, String>>>>{

    for(var legacyClaimNumber in migMultiMap.keySet()){
      var map = migMultiMap.get(legacyClaimNumber)
      if(legacyClaimNumber.startsWith("B")){
        legacyClaimNumber = legacyClaimNumber.substring(1,4) + "-" + legacyClaimNumber.substring(4,6) + "-" +legacyClaimNumber.substring(6,12)
      }
      else if(legacyClaimNumber.startsWith("0")){
        legacyClaimNumber = legacyClaimNumber
      }
      else{
        legacyClaimNumber = legacyClaimNumber.substring(0,3) + "-" + legacyClaimNumber.substring(3,5) + "-" +legacyClaimNumber.substring(5,11)
      }

      for(var header in map.keySet()){
        var innerMapList = map.get(header)

        for(var innerMap in innerMapList){
          innerMap.put("ClaimNumber",legacyClaimNumber)
        }
      }
    }
    return migMultiMap
  }
  /*
   * Method to update user on particular claim
   */
  public function assignUser(claimMap:HashMap<String,String>){

    _logger.info("Entering to assign user -----------------> assignUser()")
    var bundle = Transaction.newBundle()
    var claimNumber =claimMap.get("ClaimNumber")
    var claim = Query.make(Claim).compare(Claim#ClaimNumber, Equals, claimMap.get("ClaimNumber") ).select().first() //Get claim entity from database based on the claim number
    claim = bundle.add(claim) //Make the claim writable
    var user = Query.make(User).join("Credential").compare("UserName", Equals, claimMap.get("Claim.AssigneeAndGroupDisplayString")).select().AtMostOneRow    //Get corresponding user entity
    gw.transaction.Transaction.runWithNewBundle(\bundle1 -> {
      claim.assignUserAndDefaultGroup(user)
      _logger.info("Exiting by assigning user ----------->  assignUser()")
    }, su)
    _logger.debug(claim.AssignedUser + " assigned as user")
    bundle.commit()
  }

  /*
  * Method to update sedgwick processed claim information on particular claim
  */
  public function updateClaimInformation(claimMap:HashMap<String,String>){
    _logger.info("Entering to Update Claim Information -----------------> updateClaimInformation()")
    var bundle = Transaction.newBundle()
    var claim = Query.make(Claim).compare(Claim#ClaimNumber, Equals, claimMap.get("ClaimNumber") ).select().first()
    claim = bundle.add(claim)

    gw.transaction.Transaction.runWithNewBundle(\bundle1 -> {

      claim.claimant.FirstName = claimMap.get("Claim.claimant.FirstName")
      claim.claimant.MiddleName = claimMap.get("Claim.claimant.MiddleName")
      claim.claimant.LastName = claimMap.get("Claim.claimant.LastName")
      claim.claimant.DateOfBirth = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(claimMap.get("Claim.claimant.DateOfBirth.YearOfDate"));

      claim.claimant.Occupation = claimMap.get("Claim.claimant.Occupation")
      claim.claimant.WorkPhone = claimMap.get("Claim.claimant.WorkPhone")
      claim.claimant.WorkPhoneExtension = claimMap.get("Claim.claimant.WorkPhoneExtension")
      var taxID = claimMap.get("Claim.claimant.TaxID")
      taxID = "000-00-" + taxID.substring(5, 9)
      claim.claimant.TaxID = taxID

      claim.claimant.PrimaryAddress.AddressLine1 = claimMap.get("Claim.claimant.PrimaryAddress.AddressLine1")
      claim.claimant.PrimaryAddress.AddressLine2 = claimMap.get("Claim.claimant.PrimaryAddress.AddressLine2")
      claim.claimant.PrimaryAddress.City = claimMap.get("Claim.claimant.PrimaryAddress.City")
      claim.claimant.PrimaryAddress.State = State.get(claimMap.get("Claim.claimant.PrimaryAddress.State"))
      claim.claimant.PrimaryAddress.PostalCode = claimMap.get("Claim.claimant.PrimaryAddress.PostalCode")

      claim.LossLocation.AddressLine1 = claimMap.get("Claim.LossLocation.AddressLine1")
      claim.LossLocation.City = claimMap.get("Claim.LossLocation.City")
      claim.LossLocation.County = claimMap.get("Claim.LossLocation.County")
      claim.LossLocation.PostalCode = claimMap.get("Claim.LossLocation.PostalCode")
      claim.LossLocation.State = typekey.State.get(claimMap.get("Claim.LossLocation.State.Code"))

      try {
        var hireDate = claimMap.get("Claim.EmploymentData.HireDate.DayOfMonth") + claimMap.get("Claim.EmploymentData.HireDate.MonthOfYear") + claimMap.get("Claim.EmploymentData.HireDate.YearOfDate")
        if (hireDate != "00000000") {
          claim.EmploymentData.HireDate = new SimpleDateFormat("ddMMyyyy", Locale.ENGLISH).parse(hireDate)
        } else {
          claim.EmploymentData.HireDate = null
        }
      } catch (e : Exception) {
        //Null in flat file
      }

      try {
        var lossDate = claimMap.get("Claim.LossDate.YearOfDate") + claimMap.get("Claim.LossDate.MonthOfYear") + claimMap.get("Claim.LossDate.DayOfMonth")
            + "," + claimMap.get("Claim.LossDate.Hour") + claimMap.get("Claim.LossDate.Minute")
        if (lossDate != "000000000000") {
          claim.LossDate = new SimpleDateFormat("yyyyMMdd,hhmm", Locale.ENGLISH).parse(lossDate)
        } else {
          claim.LossDate = null
        }
      } catch (e : Exception) {
        //Null in flat file
      }

      try {
        var deathDate = claimMap.get("Claim.DeathDate.YearOfDate") + claimMap.get("Claim.DeathDate.MonthOfYear") + claimMap.get("Claim.DeathDate.DayOfMonth")
        if (deathDate != "00000000") {
          claim.DeathDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(deathDate)
        } else {
          claim.DeathDate = null
        }
      } catch (e : Exception) {
        //Null in flat file
      }

      try {
        var lastDate = claimMap.get("Claim.ClaimInjuryIncident.DateLastWorked_TDIC.YearOfDate") + claimMap.get("Claim.ClaimInjuryIncident.DateLastWorked_TDIC.MonthOfYear") + claimMap.get("Claim.ClaimInjuryIncident.DateLastWorked_TDIC.DayOfMonth")
        if (lastDate != "00000000") {
          claim.ClaimInjuryIncident.DateLastWorked_TDIC = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(lastDate)
        } else {
          claim.ClaimInjuryIncident.DateLastWorked_TDIC = null
        }
      } catch (e : Exception) {
        //Null in flat file
      }

      try {
        var retModDate = claimMap.get("Claim.ClaimInjury.ReturnToModWorkDate.YearOfDate") + claimMap.get("Claim.ClaimInjury.ReturnToModWorkDate.MonthOfYear") + claimMap.get("Claim.ClaimInjury.ReturnToModWorkDate.DayOfMonth")
        if (retModDate != "00000000") {
          claim.ClaimInjuryIncident.ReturnToModWorkDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(retModDate)
        } else {
          claim.ClaimInjuryIncident.ReturnToModWorkDate = null
        }
      } catch (e : Exception) {
        //Null in flat file
      }

      try {
        var retDate = claimMap.get("Claim.ClaimInjuryIncident.ReturnToWorkDate.YearOfDate") + claimMap.get("Claim.ClaimInjuryIncident.ReturnToWorkDate.MonthOfYear") + claimMap.get("Claim.ClaimInjuryIncident.ReturnToWorkDate.DayOfMonth")
        if (retDate != "00000000") {
          claim.ClaimInjuryIncident.ReturnToWorkDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(retDate)
        } else {
          claim.ClaimInjuryIncident.ReturnToWorkDate = null
        }
      } catch (e : Exception) {
        //Null in flat file
      }

      try {
        var closeDate = claimMap.get("Claim.CloseDate.YearOfDate") + claimMap.get("Claim.CloseDate.MonthOfYear") + claimMap.get("Claim.CloseDate.DayOfMonth")
        if (closeDate != "00000000") {
          claim.CloseDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(closeDate)
        } else {
          claim.CloseDate = null
        }
      } catch (e : Exception) {
        //Null in flat file
      }

      try {
        var reopenDate = claimMap.get("Claim.ReOpenDate.YearOfDate") + claimMap.get("Claim.ReOpenDate.MonthOfYear") + claimMap.get("Claim.ReOpenDate.DayOfMonth")
        if (reopenDate != "00000000") {
          claim.ReOpenDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(reopenDate)
        } else {
          claim.ReOpenDate = null
        }
      } catch (e : Exception) {
        //Null in flat file
      }

      try {
        var dateReported = claimMap.get("Claim.DateRptdToEmployer.YearOfDate") + claimMap.get("Claim.DateRptdToEmployer.MonthOfYear") + claimMap.get("Claim.DateRptdToEmployer.DayOfMonth")
        if (dateReported != "00000000") {
          claim.DateRptdToEmployer = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(dateReported)
        } else {
          claim.DateRptdToEmployer = null
        }
      } catch (e : Exception) {
        //Null in flat file
      }

      try {
        var reportedDate = claimMap.get("Claim.ReportedDate.YearOfDate") + claimMap.get("Claim.ReportedDate.MonthOfYear") + claimMap.get("Claim.ReportedDate.DayOfMonth")
        if (reportedDate != "00000000") {
          claim.ReportedDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(reportedDate)
        } else {
          claim.ReportedDate = null
        }
      } catch (e : Exception) {
        //Null in flat file
      }

      claim.Description = claimMap.get("Claim.LossDescription")
      claim.ClaimInjuryIncident.Description = claimMap.get("Claim.ClaimInjuryIncident.Description")

      var supervisor = Query.make(Person).compare("Name", Equals, claimMap.get("Claim.Supervisor")).select().AtMostOneRow
      claim.supervisor = supervisor
      claim.EmploymentData.WageAmount = Coercions.makeCurrencyAmountFrom(Float.parseFloat(claimMap.get("Claim.EmploymentData.WageAmount")) / 100)

      claim.claimant.PrimaryAddress.Country = typekey.Country.get(mapper.getInternalCodeByAlias("CountryCode", "tdic:sedgwick", claimMap.get("Claim.claimant.PrimaryAddress.Country").replace(" ", "")))
      if (claimMap.get("Claim.EmploymentData.HireState") != "") {
        claim.EmploymentData.HireState = typekey.State.get(claimMap.get("Claim.EmploymentData.HireState"))
      } else {
        claim.EmploymentData.HireState = null
      }
      claim.LossCause = typekey.LossCause.get(mapper.getInternalCodeByAlias("LossCause", "tdic:sedgwick", claimMap.get("Claim.LossCause")))
      claim.AccidentType = typekey.AccidentType.get(mapper.getInternalCodeByAlias("AccidentType", "tdic:sedgwick", claimMap.get("Claim.AccidentType")))
      claim.ClaimInjuryIncident.DetailedInjuryType = typekey.DetailedInjuryType.get(mapper.getInternalCodeByAlias("DetailedInjuryType", "tdic:sedgwick", "Claim.ClaimInjuryIncident.DetailedInjuryType"))
      claim.ClaimInjuryIncident.BodyParts[0].DetailedBodyPart = typekey.DetailedBodyPartType.get(mapper.getInternalCodeByAlias("WCDetailedBodyPartType", "tdic:sedgwick", claimMap.get("Claim.ClaimInjuryIncident.BodyParts.DetailedBodyPart")))
      claim.claimant.Gender = typekey.GenderType.get(mapper.getInternalCodeByAlias("GenderType", "tdic:sedgwick", claimMap.get("Claim.claimant.Gender")))
      claim.claimant.MaritalStatus = typekey.MaritalStatus.get(mapper.getInternalCodeByAlias("MaritalStatus", "tdic:sedgwick", claimMap.get("Claim.claimant.MaritalStatus")))
      claim.IncidentReport = Coercions.makeBooleanFrom(mapper.getInternalCodeByAlias("ClaimState", "tdic:sedgwick", claimMap.get("Claim.State")))
      if (claimMap.get("Claim.State") != "I") {
        claim.IncidentReport = false
      } else {
        claim.IncidentReport = true
      }
      claim.JurisdictionState = typekey.Jurisdiction.get(mapper.getInternalCodeByAlias("Jurisdiction", "tdic:sedgwick", claimMap.get("Claim.JurisdictionState")))
      claim.Compensable = typekey.CompensabilityDecision.get(mapper.getInternalCodeByAlias("CompensabilityDecision", "tdic:sedgwick", claimMap.get("Claim.Compensable")))
    }, su)

    bundle.commit()

    gw.transaction.Transaction.runWithNewBundle(\bundle1 -> {

      var claimState = mapper.getInternalCodeByAlias("ClaimState", "tdic:sedgwick", claimMap.get("Claim.State")) //Get current state of the claim
      _logger.debug(claimState + "Claim State")
      var claimAPI = new ClaimAPI()
      if (claimState.equals("open") and claim.State == typekey.ClaimState.TC_CLOSED) { //Opening a closed claim
        claimAPI.reopenClaim(claim.PublicID, typekey.ClaimReopenedReason.TC_NEWINFO, "Reopen Claim Which Was Closed") //Reopen claim
        for (var exp in claim.Exposures)
          claimAPI.reopenExposure(exp.PublicID, typekey.ExposureReopenedReason.TC_NEWINFO, "Reopen Exposures For Open Claim From Closed") //Reopen exposures

      }
    }, su)
    _logger.info("Exiting after Updating Claim Information -----------------> updateClaimInformation()")
  }

  function closeClaim(claimMap:HashMap<String,String>){
    _logger.info("Entering to close Claim Information -----------------> closeClaim()")
    var bundle = Transaction.newBundle()
    var claim = Query.make(Claim).compare(Claim#ClaimNumber, Equals, claimMap.get("ClaimNumber") ).select().first()
    claim = bundle.add(claim)

    gw.transaction.Transaction.runWithNewBundle(\bundle1 -> {
      var claimState = mapper.getInternalCodeByAlias("ClaimState","tdic:sedgwick",claimMap.get("Claim.State")) //Get current state of the claim
      var claimAPI = new ClaimAPI()
      if(claimState.equals("closed") and claim.State==typekey.ClaimState.TC_OPEN){ //If closing an open claim
        _logger.info("Entering Closing an Open Claim")
        for(var exp in claim.Exposures){
          if(!exp.Closed)
            claimAPI.closeExposure(exp.PublicID,typekey.ExposureClosedOutcomeType.TC_COMPLETED,"Closing exposures due to update from Sedgwick")  //Close each exposure
        }
        for(var mat in claim.Matters) {
          mat.close(mat.Resolution,"Closing matter due to update from Sedgwick")
          _logger.info("closing matter")
        }
        bundle.commit()
        claimAPI.closeClaim(claim.PublicID,typekey.ClaimClosedOutcomeType.TC_COMPLETED,"Closing claim due to update from Sedgwick")       //Close each claim
        resetSedgwickReserve(claimMap.get("ClaimNumber")) //Clear all reserves
        _logger.info("Exiting by closing the claim")
      }
      _logger.info("closed claim")
    }, su)
      bundle.commit()
    _logger.info("Exiting to close Claim Information -----------------> closeClaim()")
  }

  function closeClaimDate(claimMap:HashMap<String,String>){

    var bundle = Transaction.newBundle()
    var claim = Query.make(Claim).compare(Claim#ClaimNumber, Equals, claimMap.get("ClaimNumber") ).select().first()
    claim = bundle.add(claim)

    gw.transaction.Transaction.runWithNewBundle(\bundle1 -> {
    _logger.info("Entering to close Claim Date -----------------> closeClaimDate()")
    try{
     var closeDate = claimMap.get("Claim.CloseDate.YearOfDate")+claimMap.get("Claim.CloseDate.MonthOfYear")+claimMap.get("Claim.CloseDate.DayOfMonth")
     if(closeDate != "00000000"){
      claim.CloseDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(closeDate)
     }
     else{
       claim.CloseDate = null
     }
    }catch(e:Exception){
      //Null in flat file
    }
    _logger.info("Claim Closed on date :"+claim.CloseDate)

    for(var exp in claim.Exposures){
      if(exp.Closed)
        exp.CloseDate = claim.CloseDate
    }

    for(var mat in claim.Matters) {
      if(mat.Closed)
        mat.CloseDate = claim.CloseDate
    }

    var incident = claim.ensureClaimInjuryIncident()
    incident.DetailedInjuryType = typekey.DetailedInjuryType.get(mapper.getInternalCodeByAlias("DetailedInjuryType","tdic:sedgwick",claimMap.get("Claim.ClaimInjuryIncident.DetailedInjuryType")))
    },su)
    bundle.commit()
  }


  /*
  * Method to update Notes on Sedgwick Processed Claims
  */

  function addNote(noteMap:HashMap<String,String>){
    _logger.info("Entering to add Notes -----------------> addNote()")
    var notePublicID: String
    var claimAPI = new ClaimAPI()
    Transaction.runWithNewBundle(\bundle -> {
      var noteDTO = new NoteDTO()
      noteDTO.ClaimPublicID = claimAPI.findPublicIDByClaimNumber(noteMap.get("ClaimNumber"))
      noteDTO.Subject = noteMap.get("Note.Subject")
      noteDTO.Body = noteMap.get("Note.Body")
      var format = new SimpleDateFormat("yyyyMMdd,hhmmss", Locale.ENGLISH);
      var date = format.parse(noteMap.get("Note.AuthoringDate")+","+noteMap.get("Note.AuthoringTime"))
      noteDTO.AuthoringDate = date
      noteDTO.CreateUserPublicID = noteMap.get("Note.Author.DisplayName")
      noteDTO.SecurityType = NoteSecurityType.TC_PUBLIC
      notePublicID = claimAPI.createNote(noteDTO)
    }, su)

    _logger.debug("Created Note "+notePublicID)
    _logger.info("Exiting to add Notes -----------------> addNote()")
  }

  /*
  * Method to create Reserves on sedgwick processed claim.
  * Reserves are set by sedgwick as they are processing the claims
  * When Retreived through flat file TDIC Creates Reserves.
  */

  function createReserves(resMap:HashMap<String,String>){
    _logger.info("Entering to Create Reserves -----------------> createReserves()")
    var claimNumber = resMap.get("ClaimNumber")
    var claim = Query.make(Claim).compare(Claim#ClaimNumber, Equals, claimNumber ).select().first()
    if(claim==null){       //Do not process a non existing claim
      throw new Exception()
    }
    if(claim.State==typekey.ClaimState.TC_CLOSED){  //Cannot create reserve for a closed claim
      return
	}
    var medicalExposure: Exposure
    var timeLossExposure:Exposure
    for(var exp in claim.Exposures){ // Get medical and timeloss exposures
      if (exp.ExposureType.equals(ExposureType.TC_WCINJURYDAMAGE)) {
        medicalExposure = exp
      }
      if (exp.ExposureType.equals(ExposureType.TC_LOSTWAGES)) {
        timeLossExposure = exp
      }
    }

    var bundle = Transaction.newBundle()
    medicalExposure = bundle.add(medicalExposure)
    timeLossExposure = bundle.add(timeLossExposure)
    claim = bundle.add(claim)

    var type: CostType
    var category: CostCategory
    var amount: BigDecimal
    var exposure:Exposure
    var expType:ExposureType
    var netTotalIncurredCalculator = gw.api.financials.FinancialsCalculationUtil.getFinancialsCalculation(
        gw.api.financials.FinancialsCalculationUtil.getTotalIncurredNetRecoveriesExpression())

    gw.transaction.Transaction.runWithNewBundle(\bundle1 -> {

      for(var key2 in resMap.keySet()){
        if(key2.contains("TDIC")){         //The keys of resMap that contains TDIC represent reserves
          amount = Float.parseFloat(resMap.get(key2))/100
          if(amount>=0){                    //Create a reserve only if amount>0
            var claimLevel = false
            var exposureType = mapper.getInternalCodeByAlias("ExposureType","tdic:sedgwick",key2)

            if(exposureType==null){ //Claim Level
              claimLevel = true
              exposureType = null
              type = typekey.CostType.TC_EXPENSES_TDIC
            }
            else if(exposureType.equals(typekey.ExposureType.TC_WCINJURYDAMAGE.Code)){
              if(medicalExposure==null){
                medicalExposure = createExposure(claim,typekey.ExposureType.get(exposureType)) //Create a medical exposure if not exists
              }
              exposure = medicalExposure
              expType = typekey.ExposureType.get(exposureType)
              type = typekey.CostType.TC_CLAIMCOST
            }
            else if(exposureType.equals(typekey.ExposureType.TC_LOSTWAGES.Code)){
              if(timeLossExposure==null){
                timeLossExposure = createExposure(claim,typekey.ExposureType.get(exposureType)) //Create a timeLoss exposure if not exists
              }
              exposure = timeLossExposure
              expType = typekey.ExposureType.get(exposureType)
              type = typekey.CostType.TC_CLAIMCOST
            }
            category = typekey.CostCategory.get(key2)
            var sedgwickReserve = Query.make(SedgwickReserve).compare("ClaimID",Equals,claimNumber).
                compare("CostType",Equals,type).compare("CostCategory",Equals,category).select().first()  //Get reserve for claim
            var availableReserves:CurrencyAmount
            var totalNetIncurred:CurrencyAmount
            if(sedgwickReserve!=null){
              if(claimLevel){
                availableReserves = gw.api.financials.FinancialsCalculations.getAvailableReserves()
                    .withClaim(claim)
                    .withCostType(type)
                    .withCostCategory(category)
                    .Amount
                totalNetIncurred = Coercions.makeCurrencyAmountFrom(netTotalIncurredCalculator.getAmount(claim,category).Amount)
              }
              else{
                availableReserves = gw.api.financials.FinancialsCalculations.getAvailableReserves()
                    .withExposure(exposure)
                    .withCostType(type)
                    .withCostCategory(category)
                    .Amount
                totalNetIncurred = Coercions.makeCurrencyAmountFrom(netTotalIncurredCalculator.getAmount(claim,category).Amount)
              }
              _logger.debug("Available reserves "+availableReserves)
              _logger.debug("Total Incurred "+totalNetIncurred)
            }
            if(sedgwickReserve==null){       //If no such reserve exists
              var s = new SedgwickReserve()  //Create a reserve
              s.ClaimID = claimNumber.trim()
              s.CostType = type
              s.CostCategory = category
              s.Amount = Coercions.makeCurrencyAmountFrom(amount)
              _logger.debug("New sedgwick reserve created")
              bundle.commit()
              _logger.debug(category+" ->* "+amount)
              if(claimLevel){
                claim.setAvailableReserves(type,category,amount,su)
                _logger.debug("Claim Level Reserve added")
              }
              else{                            //If reserve exists
                exposure.setAvailableReserves(type,category,amount,su)  //Set reserves accordingly
                _logger.debug("Reserve added");
              }
              _logger.debug("Reserve added with money "+amount)
            }
            else if(totalNetIncurred!=Coercions.makeCurrencyAmountFrom(amount)){  //If reserve exists and its amount is less than the provided amount
              _logger.debug(category+" -> "+amount)
              var newReserveAmount = amount - totalNetIncurred.getAmount()+availableReserves.getAmount()
              _logger.debug("New Reserve Amount" + newReserveAmount)
              if(claimLevel){
                claim.setAvailableReserves(type,category,newReserveAmount,su)
                _logger.debug("Claim Level Reserve added 1")
              }
              else{
                exposure.setAvailableReserves(type,category,newReserveAmount,su)
                _logger.debug("Reserve added 1");
              }
              bundle.commit()
              sedgwickReserve = bundle.add(sedgwickReserve)
              sedgwickReserve.Amount = Coercions.makeCurrencyAmountFrom(amount)
              _logger.debug("Reserve has less available reserves "+availableReserves+" new amount "+amount)
            }
            else{
              _logger.debug("Reserve was previously added. Available reserves "+availableReserves)
            }
          }
        }
      }

    }, su)
    bundle.commit()
    _logger.info("Exiting to Create Reserves -----------------> createReserves()")
    _logger.debug("Reserve creation done");
  }

  /*
   * Method to create NewExposures.
   * If previously Exposure on a claim were not opened during FNOL Intake.
   */
  public function createExposure(claim:Claim, exposureType:ExposureType): Exposure {
    var bundle = Transaction.newBundle()
    var claimAPI = new ClaimAPI()
    gw.transaction.Transaction.runWithNewBundle(\bundle1 -> {
      var expDTO = new ExposureDTO()
      expDTO.State = typekey.ExposureState.TC_OPEN
      expDTO.ClaimPublicID = claim.PublicID
      expDTO.ExposureType = exposureType
      expDTO.PrimaryCoverage = typekey.CoverageType.TC_WC7WORKERSCOMPEMPLIABINSURANCEPOLICYACOV
      if(expDTO.ExposureType == TC_LOSTWAGES) {
        expDTO.CoverageSubType = typekey.CoverageSubtype.TC_WC7WORKERSCOMP_WAGES_TDIC
      }
      else {
        expDTO.CoverageSubType = typekey.CoverageSubtype.TC_WC7WORKERSCOMP_MED_TDIC
      }
      claimAPI.createExposure(expDTO)
    }, su)
    bundle.commit()

    for(var exp in claim.Exposures){               //Iterate through exposures and return the exposure just created
      if (exp.ExposureType.equals(exposureType)) {
        return exp
      }
    }
    return null
  }

  /*
   * Reset Reserves to Zero If claim status returned by sedgwick is "CLOSED"
   */

  public function resetSedgwickReserve(claimNumber:String){
    var bundle = Transaction.newBundle()
    var sedgwickReserveList = Query.make(SedgwickReserve).compare("ClaimID",Equals,claimNumber).select().toList()
    gw.transaction.Transaction.runWithNewBundle(\bundle1 -> {
      for(var sedgwickReserve in sedgwickReserveList){
        sedgwickReserve=bundle.add(sedgwickReserve)
        sedgwickReserve.Amount = Coercions.makeCurrencyAmountFrom(0)
      }
    },su)
    bundle.commit()
  }

  /*
   * Method to Create Payments on corresponding Claim
   * Check for avilable Reserves and as well Contacts.
   * If reserves are insuffient Throw an exception Insufficient Reseves(default by api)
   * If Payment is made on a closed claim the payment is created as supplement payment.
   * Creates a Contact before making a payment,If prior contact doesnot exist on the claim.
   */
  public function createPayment(payMap:HashMap<String,String>){
    var bundle = Transaction.newBundle()
    var claimFinancialsAPI = new ClaimFinancialsAPI()
    var claimAPI = new ClaimAPI()

    var claim = Query.make(Claim).compare(Claim#ClaimNumber, Equals, payMap.get("ClaimNumber")).select().first()
    var exposure = claim.Exposures
    var contact:Contact
    var tcontact:Contact
    Transaction.runWithNewBundle(\bundle3 -> {
      //Check for a contact
      var listcontact = Query.make(Contact).compare(Contact#Name, Equals,payMap.get("Check.FirstPayee.Payee")).select().toList()
      contact = listcontact.firstWhere(\elt -> elt.PrimaryAddress.City != null and elt.PrimaryAddress.State != null)
      if(contact == null) {
        _logger.debug("Contact Exception. Creating NEW Contact")
        contact = createContact(payMap)  //Create contact if not exists
      }

      var claimContact:ClaimContact
      try{                    //Try getting claim contact
        claimContact = Query.make(ClaimContact).compare(ClaimContact#Contact,Equals,contact).select().toList()[0]
      }
      catch(e:Exception){
        _logger.debug("Claim contact exception")
        createClaimContact(contact,claim)  //create one on error
        claimContact = Query.make(ClaimContact).compare(ClaimContact#Contact,Equals,contact).select().toList()[0]
      }

      var checkPayeeDTO = new CheckPayeeDTO()
      checkPayeeDTO.PayeeType = typekey.ContactRole.TC_CHECKPAYEE
      checkPayeeDTO.ClaimContactPublicID = claimContact.PublicID

      var tli = new TransactionLineItemDTO()
      tli.TransactionAmount = Coercions.makeCurrencyAmountFrom(Float.parseFloat(payMap.get("TransactionLineItem.TransactionAmountReservingAmountPair"))/100.00)
      tli.LineCategory = typekey.LineCategory.TC_OTHER

      var payDTO = new TransactionDTO()

      var costCategory = mapper.getInternalCodeByAlias("CostCategory","tdic:sedgwick",payMap.get("TransactionView.CostCategory"))
      payDTO.CostCategory = typekey.CostCategory.get(costCategory)
      var exposureType = mapper.getInternalCodeByAlias("ExposureType","tdic:sedgwick",costCategory)

      if(costCategory.equals("miscexpenses_TDIC")){
        exposureType = "ClaimLevel"
        payDTO.CostType = typekey.CostType.TC_EXPENSES_TDIC
      }
      else{
        payDTO.CostType = typekey.CostType.TC_CLAIMCOST
      }
      if(claim.State==typekey.ClaimState.TC_CLOSED){  //Supplement payment on closed claims
        payDTO.PaymentType = typekey.PaymentType.TC_SUPPLEMENT
      }
      else{
        payDTO.PaymentType = typekey.PaymentType.TC_PARTIAL
      }
      payDTO.NewLineItems = {tli}
      if(exposureType.equals("ClaimLevel")){
        payDTO.ExposurePublicID =  null
      }
      else if(exposureType.equals(typekey.ExposureType.TC_WCINJURYDAMAGE.Code)){
        for(var exp in exposure){
          if(exp.toString().equals("(1) Medical Details")){
            payDTO.ExposurePublicID = exp.PublicID
            break
          }
        }
      }
      else if(exposureType.equals(typekey.ExposureType.TC_LOSTWAGES.Code)){
        for(var exp in exposure){
          if(exp.toString().equals("(2) Time Loss/Indemnity")){
             payDTO.ExposurePublicID = exp.PublicID
             break
          }
        }
      }

      var checkDTO = new CheckDTO()
      checkDTO.NewPayments = {payDTO}
      checkDTO.NewPayees = {checkPayeeDTO}
      checkDTO.IssueDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(payMap.get("TransactionView.Check_IssueDate"))
      checkDTO.ScheduledSendDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(payMap.get("TransactionView.Check_IssueDate"))
      checkDTO.CheckNumber = payMap.get("TransactionView.Check_CheckNumber")
      checkDTO.NewPrimaryPayee = checkPayeeDTO
      _logger.debug(checkDTO.IssueDate+"****************" + claim.ClaimNumber+" -> "+tli.TransactionAmount)

      var transDTO = new TransactionSetDTO()
      transDTO.ClaimPublicID = claimAPI.findPublicIDByClaimNumber(payMap.get("ClaimNumber"))
      transDTO.Subtype = typekey.TransactionSet.TC_CHECKSET
      transDTO.NewChecks = {checkDTO}
      try{
        claimFinancialsAPI.createCheckSet(transDTO,true)
      }
      catch(p:NullPointerException){
        _logger.debug("NullPointer Exception")
        throw(p)
      }
      catch(p:Exception){
        _logger.debug("Insufficient Reserves for payment. Creating new reserve")
        var resMap = new HashMap<String,String>()
        resMap.put("ClaimNumber",payMap.get("ClaimNumber"))
        resMap.put(costCategory,payMap.get("TransactionLineItem.TransactionAmountReservingAmountPair"))
        createReserves(resMap)
        claimFinancialsAPI.createCheckSet(transDTO, true)
      }
    }, su)
    bundle.commit()
    _logger.debug("Check created")
  }

  /*
   * Method to Create a New Contact
   * If Contact Doesnot Exist on the Claim
   */
  public function createContact(payMap:HashMap<String,String>):Contact{
    var bundle = Transaction.newBundle()
    var contact:Contact
    Transaction.runWithNewBundle(\bundle2 -> {
      if(payMap.get("Contact.State")!="" and payMap.get("Contact.City") !=""){
        contact = new Company()
        contact.Name = payMap.get("Check.FirstPayee.Payee")
        contact.PrimaryAddress.AddressLine1 = payMap.get("Contact.Addr1")
        contact.PrimaryAddress.AddressLine2 = payMap.get("Contact.Addr2")
        contact.PrimaryAddress.City = payMap.get("Contact.City")
        contact.PrimaryAddress.State = typekey.State.get(payMap.get("Contact.State"))
        contact.PrimaryAddress.PostalCode = payMap.get("Contact.PostalCode")
      }
      contact.IsSedgwickContact_TDIC = true
    },su)
    bundle.commit()
    return contact
  }

  /*
   * Method to create a claim level contact.
   */

  public function createClaimContact(contact:Contact, claim:Claim):ClaimContact{

    var bundle = Transaction.newBundle()
    var claimContact:ClaimContact
    Transaction.runWithNewBundle(\bundle2 -> {
      claimContact = new ClaimContact()
      claimContact.Contact = contact
      claimContact.Claim = claim
      claimContact.ClaimantFlag = false

      var claimContactRole = new ClaimContactRole()
      claimContactRole.Role = ContactRole.TC_CHECKPAYEE
      claimContactRole.PartyNumber = 1
      claimContactRole.ClaimContact = claimContact
      claimContact.addToRoles(claimContactRole)
    },su)
    bundle.commit()
    return claimContact
  }

  /*
   * Method to Update Work Detail Header.
   * WD1 Will have all the information Regarding Employment Details of the injured
   */
  public function updateWD1(wdMap:HashMap<String,String>){
	_logger.info("Entering updateWD1() -----> TDIC_Update#updateAll()")
    var claimNumber = wdMap.get("ClaimNumber")
    var claim = Query.make(Claim).compare(Claim#ClaimNumber, Equals, claimNumber).select().first()
    var bundle = Transaction.newBundle()
    var claimAPI = new ClaimAPI()
    gw.transaction.Transaction.runWithNewBundle(\bundle1 -> {

      var exposure:Exposure
      //Benefits
      for(var exp in claim.Exposures){     //Get timeloss exposure
        if (exp.ExposureType.equals(ExposureType.TC_LOSTWAGES)) {
          exposure = exp
        }
      }

      if(exposure != null){
        exposure=bundle.add(exposure)

        try{
         var mmiDate = wdMap.get("Exposure.Claim.MMIdate.YearOfDate")+wdMap.get("Exposure.Claim.MMIdate.MonthOfYear")+wdMap.get("Exposure.Claim.MMIdate.DayOfMonth")
         if(mmiDate != "00000000"){
          exposure.Claim.MMIdate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(mmiDate)
         }
         else{
           exposure.Claim.MMIdate = null
         }
        }catch(e:Exception){
          //Null in flat file
        }

        exposure.Claim.EmploymentData.WageAmountPostInjury = Coercions.makeCurrencyAmountFrom(Float.parseFloat(wdMap.get("Exposure.Claim.EmploymentData.WageAmountPostInjury"))/100)
        exposure.VocationalRehab_TDIC = wdMap.get("Exposure.VocationalRehab_TDIC").equals("N")?false:true
        exposure.InjuryIncident.Impairment = Coercions.makeBigDecimalFrom(wdMap.get("Exposure.InjuryIncident.Impairment"))

        //GW-2677 - Settle Method for Unit Stat Reporting.
        var settleMapper = mapper.getInternalCodeByAlias("SettleMethod","tdic:sedgwick",wdMap.get("Exposure.Settlements.SettleMethod"))
        if(wdMap.get("Exposure.Settlements.SettleMethod") != "0"){
          if(exposure.Settlements.length==0){
            var settlement = new Settlement(exposure)
            settlement.SettleMethod = typekey.SettleMethod.get(settleMapper)
            exposure.addToSettlements(settlement)
          }
          else{
            if(exposure.Settlements[0].SettleMethod != typekey.SettleMethod.get(settleMapper)){
              exposure.Settlements[0].SettleMethod = typekey.SettleMethod.get(settleMapper)
            }
          }
        }

        var benefitList = new ArrayList<BenefitPeriod>()
        for(var i in 1..5){ //There are 5 benefitperiodWeeklyCompRate
          var benefitType = mapper.getInternalCodeByAlias("LostWagesBenefitType","tdic:sedgwick",wdMap.get("BenefitPeriod.BenefitType.Code"+i))
          var weeklyRate =  Float.parseFloat(wdMap.get("BenefitPeriod.WeeklyCompRate"+i))/100

          if(benefitType!=null && weeklyRate>0)  {
            var benefit = new BenefitPeriod(exposure)
			      var benefitBeginDate = wdMap.get("BenefitPeriod.BenefitsBeginDate.YearOfDate")+wdMap.get("BenefitPeriod.BenefitsBeginDate.MonthOfYear")+wdMap.get("BenefitPeriod.BenefitsBeginDate.DayOfMonth")
            if(benefitBeginDate != "00000000"){
              benefit.BenefitsBeginDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(benefitBeginDate)
            }
            else{
              benefit.BenefitsBeginDate = null
            }
            benefit.BenefitType = typekey.LostWagesBenefitType.get(benefitType)
            benefit.WeeklyCompRate =  Coercions.makeCurrencyAmountFrom(weeklyRate)
            benefitList.add(benefit)
          }
        }

        var benefitPeriods = new BenefitPeriod[benefitList.size()]
        if(benefitList.size()>0){                             //Convert arraylist to array
          for(var j in 0..benefitList.size()-1){
            benefitPeriods[j]=benefitList.get(j)
          }
          exposure.BenefitPeriods = benefitPeriods
          bundle.commit()
        }
      }

      //Injury
      var severity = mapper.getInternalCodeByAlias("SeverityType","tdic:sedgwick",wdMap.get("Claim.ensureClaimInjuryIncident().Severity.Code"))
      var injury = Query.make(InjuryIncident).compare(InjuryIncident#Claim, Equals, claim).select().firstWhere( \ elt -> elt.Severity == typekey.SeverityType.get(severity))
      if(injury==null){
        injury = Query.make(InjuryIncident).compare(InjuryIncident#Claim, Equals, claim).select().first()
      }
      injury = bundle.add(injury)

      var returnToModWorkValid = wdMap.get("Injury.ReturnToWorkValid")
      if(!returnToModWorkValid.equals(""))
        injury.ReturnToModWorkValid = returnToModWorkValid.equals("Y")?true:false
      try{
       var dateDisabilityBegan = wdMap.get("Injury.DateDisabilityBegan_TDIC.YearOfDate")+wdMap.get("Injury.DateDisabilityBegan_TDIC.MonthOfYear")+wdMap.get("Injury.DateDisabilityBegan_TDIC.DayOfMonth")
       if(dateDisabilityBegan != "00000000"){
        injury.DateDisabilityBegan_TDIC = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(dateDisabilityBegan)
       }
        else{
         injury.DateDisabilityBegan_TDIC = null
       }
      }catch(e:Exception){
        //Null in flat file
      }

      //Claim
      claim=bundle.add(claim)
      claim.EmploymentData.PayScheme_TDIC=typekey.PaySchemeType_TDIC.TC_WEEK
      
	    if(Integer.parseInt(wdMap.get("Claim.ensureClaimInjuryIncident().YearLastExposed_TDIC"))>0)
        claim.ensureClaimInjuryIncident().YearLastExposed_TDIC = Integer.parseInt(wdMap.get("Claim.ensureClaimInjuryIncident().YearLastExposed_TDIC"))
        claim.ensureClaimInjuryIncident().Severity = typekey.SeverityType.get(mapper.getInternalCodeByAlias("SeverityType","tdic:sedgwick",wdMap.get("Claim.ensureClaimInjuryIncident().Severity.Code")))
        claim.EmploymentData.EmploymentStatus = typekey.EmploymentStatusType.get(mapper.getInternalCodeByAlias("EmploymentStatusType","tdic:sedgwick","Claim.EmploymentData.EmploymentStatus.Code"))
    }, su)
    bundle.commit()
    _logger.info("Exiting updateWD1() -----> TDIC_Update#updateAll()")
  }

   /*
    * Method to Update Legal Header
    */

  function updateLegal(legMap:HashMap<String,String>){
	_logger.info("Entering updateLegal() -----> TDIC_Update#updateAll()")
    if(legMap.get("Matter.CaseNumber") == ""){
      return
    }
    var matter : Matter
    var attorney:Attorney

    var claimNumber = legMap.get("ClaimNumber")
    var claim = Query.make(Claim).compare(Claim#ClaimNumber, Equals, claimNumber).select().first()

    var matterQuery = Query.make(Matter).compare(Matter#Claim, Equals, claim). select().firstWhere( \ elt -> elt.CaseNumber != null and elt.CaseNumber == legMap.get("Matter.CaseNumber"))
    var bundle = Transaction.newBundle()
    gw.transaction.Transaction.runWithNewBundle(\bundle1 -> {

      if(matterQuery == null){
        matter = new Matter()
        matter.Claim = claim
        matter.Name = claim.claimant as String
        matter.MatterType = typekey.MatterType.TC_GENERAL
      }
      else{
        matter=matterQuery
      }
      //Matter
      matter=bundle.add(matter)

      matter.CaseNumber = legMap.get("Matter.CaseNumber")
      matter.DocketNumber = legMap.get("Matter.DocketNumber")

      try{
        var defenseApptDate = legMap.get("Matter.DefenseApptDate.YearOfDate")+legMap.get("Matter.DefenseApptDate.MonthOfYear")+legMap.get("Matter.DefenseApptDate.DayOfMonth")
        if(defenseApptDate != "00000000"){
          matter.DefenseApptDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(defenseApptDate)
        }
        else{
          matter.DefenseApptDate = null
        }
      }catch(e:Exception){
        //Null in flat file
      }

      try{
        var matFileDate = legMap.get("Matter.FileDate.YearOfDate")+legMap.get("Matter.FileDate.MonthOfYear")+legMap.get("Matter.FileDate.DayOfMonth")
        if(matFileDate != "00000000"){
          matter.FileDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(matFileDate)
        }
        else{
          matter.FileDate = null
        }
      }catch(e:Exception){
        //Null in flat file
      }

      try{
        var FinalSettleDate = legMap.get("Matter.FinalSettleDate.YearOfDate")+legMap.get("Matter.FinalSettleDate.MonthOfYear")+legMap.get("Matter.FinalSettleDate.DayOfMonth")
        if(FinalSettleDate != "00000000"){
          matter.FinalSettleDate = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(FinalSettleDate)
        }
        else{
          matter.FinalSettleDate = null
        }
      }catch(e:Exception){
        //Null in flat file
      }

      try{
        var serviceDate = legMap.get("Matter.ServiceDate.DayOfMonth")+legMap.get("Matter.ServiceDate.MonthOfYear")+legMap.get("Matter.ServiceDate.YearOfDate")
        if(serviceDate != "00000000"){
          matter.ServiceDate = new SimpleDateFormat("ddMMyyyy", Locale.ENGLISH).parse(serviceDate)
        }
        else{
          matter.ServiceDate = null
        }
      }catch(e:Exception){
        //Null in flat file
      }

      try{
        var responseDue = legMap.get("Matter.ResponseDue.DayOfMonth")+legMap.get("Matter.ResponseDue.MonthOfYear")+legMap.get("Matter.ResponseDue.YearOfDate")
        if(responseDue != "00000000"){
          matter.ResponseDue = new SimpleDateFormat("ddMMyyyy", Locale.ENGLISH).parse(responseDue)
        }
        else{
          matter.ResponseDue = null
        }
      }catch(e:Exception){
        //Null in flat file
      }

      try{
        var trialDate = legMap.get("Matter.TrialDate.DayOfMonth")+legMap.get("Matter.TrialDate.MonthOfYear")+legMap.get("Matter.TrialDate.YearOfDate")
        if(trialDate != "00000000"){
          matter.TrialDate = new SimpleDateFormat("ddMMyyyy", Locale.ENGLISH).parse(trialDate)
        }
        else{
          matter.TrialDate = null
        }
      }catch(e:Exception){
        //Null in flat file
      }

      //Venue is duplicating
      try{
        if(legMap.get("Matter.Venue") != ""){
          var queryVenue = Query.make(ClaimContact).join("Contact").compare("Name", Equals, legMap.get("Matter.Venue")).select().toList()[0]
          if(queryVenue == null){
            var venue = new LegalVenue()
            venue.Name = legMap.get("Matter.Venue")
            venue.IsSedgwickContact_TDIC = true
            matter.venue = venue
          }
          else{
            matter.venue.Name = queryVenue as String
            matter.venue.IsSedgwickContact_TDIC = true
          }
        }
      }catch(e:Exception){
        //Null in flat file
      }

      matter.plaintiff = claim.claimant
      matter.plaintiff.IsSedgwickContact_TDIC = true
      if(legMap.get("Matter.plaintiffatt.State.Code")!=""){
        var attorneyAddress = new Address()
        attorneyAddress.AddressLine1 = legMap.get("Matter.plaintiffatt.Address1")
        attorneyAddress.AddressLine2 = legMap.get("Matter.plaintiffatt.Address2")
        attorneyAddress.City = legMap.get("Matter.plaintiffatt.City")
        attorneyAddress.State = typekey.State.get(legMap.get("Matter.plaintiffatt.State.Code"))
        attorneyAddress.PostalCode = legMap.get("Matter.plaintiffatt.PostalCode")
        attorneyAddress.County = legMap.get("Matter.plaintiffatt.County")
        attorneyAddress.Country = TC_US
        matter.plaintiffatt.PrimaryAddress = attorneyAddress
      }

      var planattname = legMap.get("Matter.plaintiffatt.Name")
      planattname = planattname.substring(0,Math.min(30,planattname.length))
      matter.plaintiffatt.FirstName = planattname
      matter.plaintiffatt.LastName="."
      matter.plaintiffatt.WorkPhone = legMap.get("Matter.plaintiffatt.Phone")
      matter.plaintiffatt.PrimaryPhone = typekey.PrimaryPhoneType.TC_WORK
      matter.plaintiffatt.IsSedgwickContact_TDIC = true

      matter.plaintifffirm.Name = legMap.get("Matter.plaintifffirm.Name")
      matter.plaintifffirm.IsSedgwickContact_TDIC = true

      matter.defendant = claim.Insured
      matter.defendant.IsSedgwickContact_TDIC = true

      if(legMap.get("Matter.defenseattorney.State.Code")!=""){
        var defenseAttorneyAddress = new Address()
        defenseAttorneyAddress.AddressLine1 = legMap.get("Matter.defenseattorney.Address1")
        defenseAttorneyAddress.AddressLine2 = legMap.get("Matter.defenseattorney.Address2")
        defenseAttorneyAddress.City = legMap.get("Matter.defenseattorney.City")
        defenseAttorneyAddress.State = typekey.State.get(legMap.get("Matter.defenseattorney.State.Code"))
        defenseAttorneyAddress.PostalCode = legMap.get("Matter.defenseattorney.PostalCode")
        defenseAttorneyAddress.County = legMap.get("Matter.defenseattorney.County")
        defenseAttorneyAddress.Country = TC_US
        matter.defenseattorney.PrimaryAddress = defenseAttorneyAddress
      }

      var defattname = legMap.get("Matter.defenseattorney.Name")
      defattname = defattname.substring(0,Math.min(30,defattname.length))
      matter.defenseattorney.FirstName = defattname
      matter.defenseattorney.LastName="."
      matter.defenseattorney.WorkPhone = legMap.get("Matter.defenseattorney.Phone")
      matter.defenseattorney.PrimaryPhone = typekey.PrimaryPhoneType.TC_WORK
      matter.defenseattorney.EmailAddress1 = legMap.get("Matter.defendant.AttorneyLawFirm.Mail")
      matter.defenseattorney.FEINOfficialID = legMap.get("Matter.defendant.AttorneyLawFirm.FEIN")
      matter.defenseattorney.IsSedgwickContact_TDIC = true

      matter.defensefirm.Name = legMap.get("Matter.defensefirm.Name")
      matter.defensefirm.IsSedgwickContact_TDIC = true

      matter.CourtDistrict = typekey.MatterCourtDistrict.TC_CA
      matter.Resolution=typekey.ResolutionType.get(mapper.getInternalCodeByAlias("ResolutionType","tdic:sedgwick",legMap.get("Matter.Resolution.Code")))
    }, su)
    bundle.commit()
    _logger.info("Exiting updateLegal() -----> TDIC_Update#updateAll()")
  }

  /*
   * Method to Update Recovery and Recovery Reserves.
   */
  function updateRecovery(recMap:HashMap<String,String>){
    _logger.info("Entering updateRecovery() -----> TDIC_Update#updateAll()")
    var claimNumber = recMap.get("ClaimNumber")
    var claim = Query.make(Claim).compare(Claim#ClaimNumber, Equals, claimNumber).select().first()
    var bundle = Transaction.newBundle()
    var medrec = new BigDecimal("0.00").setScale(2, RoundingMode.HALF_DOWN)
    var indemrec = new BigDecimal("0.00").setScale(2, RoundingMode.HALF_DOWN)
    var otherrec = new BigDecimal("0.00").setScale(2, RoundingMode.HALF_DOWN)
    
    var medicalExposure : Exposure
    var indemExposure : Exposure
    var claimExposure : Exposure

    for(var key in recMap.keySet()){
      _logger.info(key+" "+recMap.get(key)+" "+medrec+" "+indemrec)

      if(key.startsWith("medrec")){
        var value =  Float.parseFloat(recMap.get(key))/100.0
        medrec += value
      }
      else if(key.startsWith("indemrec")){
        var value =  Float.parseFloat(recMap.get(key))/100.0
        indemrec +=  value
      }
      else if(key.equals("otherOTD_TDIC")){
         var value =  Float.parseFloat(recMap.get(key))/100.0
         otherrec +=  value
      }
    }

    for(var line in claim.ReserveLines){
      if(line.CostCategory.equals(typekey.CostCategory.TC_EXPREC_TDIC)){
        otherrec += Float.parseFloat(line.NetPaidReservingAmountExcludingSubroRecovery.Amount as String)
      }
    }

    for(var exp in claim.Exposures){

      if(exp.toString().equals("(1) Medical Details")){
        medicalExposure = exp
      }
      else if (exp.toString().equals("(2) Time Loss/Indemnity")){
        indemExposure = exp
      }
      else{
        claimExposure = exp
      }
    }

    gw.transaction.Transaction.runWithNewBundle(\bundle1 -> {
      //Daily Feed
      if(medicalExposure.TotalRecoveries.Amount!=null){    //Checking if the claim has previous recoveries
        medrec -= Float.parseFloat(medicalExposure.TotalRecoveries.Amount as String)
      }
      //Migration
      if(medrec>0){
        claim=bundle.add(claim)
        var recovery = claim.newRecoverySet()
        _logger.info("Entering updateRecovery() -----> TDIC_Update#updateAll()")
        try{
          var rec = recovery.newRecovery(medicalExposure,typekey.CostType.TC_CLAIMCOST,typekey.CostCategory.TC_MEDREC_TDIC, RecoveryCategory.TC_SUBRO, null)
          medrec = medrec.setScale(2, RoundingMode.HALF_DOWN)
          rec.addNewLineItem(Coercions.makeCurrencyAmountFrom(medrec),null,null)
          recovery.prepareForCommit()
        }
        catch(e:Exception){
           throw(e)
        }
       _logger.info("Exiting updating Medical Recovery() -----> Update Recovery")
      }

      claim=bundle.add(claim)
      //Daily
      if(indemExposure.TotalRecoveries.Amount!=null){
        indemrec -= Float.parseFloat(indemExposure.TotalRecoveries.Amount as String)
      }
      //Migration
      if(indemExposure!=null and indemrec>0) {
        _logger.info("Entering updateRecovery() -----> TDIC_Update#updateAll()")
        var recovery1 = claim.newRecoverySet()
        var rec1 = recovery1.newRecovery(indemExposure,typekey.CostType.TC_CLAIMCOST,typekey.CostCategory.TC_INDEMREC_TDIC, RecoveryCategory.TC_SUBRO, null)
        indemrec = indemrec.setScale(2, RoundingMode.HALF_DOWN)
        rec1.addNewLineItem(Coercions.makeCurrencyAmountFrom(indemrec),null,null)
        recovery1.prepareForCommit()
        _logger.info("Exiting updating Indemnity Recovery() -----> Update Recovery")
      }
      //Migration
      if(otherrec>0) {
        _logger.info("Entering updateRecovery() -----> TDIC_Update#updateAll()")
        var recovery2 = claim.newRecoverySet()
        // added 2 parameters (as null) as part of upgrade as expected by the application.
        var rec2 = recovery2.newRecovery(claimExposure,typekey.CostType.TC_EXPENSES_TDIC,typekey.CostCategory.TC_EXPREC_TDIC,RecoveryCategory.TC_SUBRO,null)
        otherrec = otherrec.setScale(2, RoundingMode.HALF_DOWN)
        rec2.addNewLineItem(Coercions.makeCurrencyAmountFrom(otherrec),null,null)
        recovery2.prepareForCommit()
        _logger.info("Exiting updating Indemnity Recovery() -----> Update Recovery")
      }
    }, su)
    bundle.commit()
    _logger.info("Exiting updateRecovery() -----> TDIC_Update#updateAll()")
  }
}