package tdic.cc.common.admindata

uses java.util.HashMap
uses org.w3c.dom.Element
uses tdic.cc.common.admindata.CleanseAdminData

uses java.util.ArrayList
uses java.util.List

class CleanseBCAdminData extends CleanseAdminData {

  /**
   * Get the list of admin data files.
   */
  override protected property get Filenames() : List<String> {
    var retval = super.Filenames;
    retval.add ("holidays.xml");
    return retval;
  }

  /**
   * Cleanse the admin data file.
   */
  override protected function cleanseAdminDataFile (filename : String, import : Element) : void {
    super.cleanseAdminDataFile (filename, import);

    if (filename == "admin.xml") {
      // The admin xml file doesn't contain all the Holiday data.  It is missing the holiday types.
      removeElements (import, {"Holiday"});

      removeAuthorityLimitProfilesWithMissingCustomerUser (import);
      removeInvisiblePaymentPlans (import);
    }
  }

  /**
   * Remove AuthorityLimitProfile entities that reference a missing custom user.
   */
  protected function removeAuthorityLimitProfilesWithMissingCustomerUser (import : Element) : void {
    var customUserElement : Element;
    var removeList = new ArrayList<Element>();
    var userPublicID : String;

    // Get Users
    var users = new HashMap<String,Element>();
    for (userElement in getChildElements (import, "User")) {
      users.put (userElement.getAttribute("public-id"), userElement);
    }

    // Process AuthorityLimitProfiles
    for (authorityLimitProfileElement in getChildElements (import, "AuthorityLimitProfile")) {
      customUserElement = getSingleElement (authorityLimitProfileElement, "CustomUser");
      userPublicID = customUserElement.getAttribute("public-id");

      if (userPublicID != "" and users.containsKey(userPublicID) == false) {
        print ("Removing AuthorityLimitProfile " + authorityLimitProfileElement.getAttribute("public-id")
                  + " because CustomUser " + userPublicID + " does not exist in admin data.");
        removeList.add (authorityLimitProfileElement);
      }
    }

    for (element in removeList) {
      import.removeChild (element);
    }
  }

  /**
   * Remove Invisible Payment Plans.
   */
  protected function removeInvisiblePaymentPlans (import : Element) : void {
    var removeList = new ArrayList<Element>();

    for (paymentPlanElement in getChildElements (import, "PaymentPlan")) {
      if (getSingleElementValue (paymentPlanElement, "UserVisible") == "false") {
        print ("Removing PaymentPlan " + getSingleElementValue (paymentPlanElement, "Name") + " - "
                  + getSingleElementValue (paymentPlanElement, "Description") + " because it is not visible.");
        removeList.add (paymentPlanElement);
      }
    }

    for (element in removeList) {
      import.removeChild (element);
    }
  }
}