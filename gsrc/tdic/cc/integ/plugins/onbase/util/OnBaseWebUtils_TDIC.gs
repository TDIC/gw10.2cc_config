package tdic.cc.integ.plugins.onbase.util

uses acc.onbase.api.security.*
uses acc.onbase.configuration.OnBaseConfigurationFactory

/**
 * Hyland Build Version: 4.2.0-5-g348135c9

 */

/*
 * OnBaseWebUtils_TDIC - Code for OnBaseDemoPop.pcf
 * This class has been copied from Policy Center Onbase accelerator to implement WebCustomQuery and it would be removed
 * in future after updating accelerator
 * Author: RambabuN
 */

class OnBaseWebUtils_TDIC {

  public static function renderPopUrl(template: String, args: String[]):String{
    var encodedArgs = UrlEncodeArray(args)
    var constructWebURL = String.format(template, encodedArgs)
    var popURL = OnBaseConfigurationFactory.Instance.PopURL
    var urlWithoutHash = popURL + constructWebURL

    if (OnBaseConfigurationFactory.Instance.EnableDocPopURLCheckSum){
      var urlWithHash = popURL + constructWebURL + "&chksum=" + SecurityManager.computeCheckSum(constructWebURL.split('\\?')[1])
      return urlWithHash
    }
    return urlWithoutHash
  }

  /**
   * Url Encoder
   */
  private static function UrlEncode(urlParam: String): String {
    return java.net.URLEncoder.encode(urlParam, java.nio.charset.StandardCharsets.UTF_8.toString())
  }

  private static function UrlEncodeArray(args: String[]): String[]{
    var newArray = new String[args.length]
    foreach(arg in args index i){
      newArray[i] = UrlEncode(arg)
    }
    return newArray
  }

}