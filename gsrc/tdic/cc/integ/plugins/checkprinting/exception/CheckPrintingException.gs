package tdic.cc.integ.plugins.checkprinting.exception

uses com.tdic.util.exception.TDICException

class CheckPrintingException extends TDICException {

  public construct() {
    super()
  }

  public construct(errorMessage : String) {
    super(errorMessage)
  }

  public construct(errorMessage : String, exception : Exception) {
    super(errorMessage, exception)
  }

}