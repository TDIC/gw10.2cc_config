package tdic.cc.integ.plugins.checkprinting.dto

uses java.math.BigDecimal
/**
 * PaymentLineItemDTO contains check payment line info felds for check printing outbound.
 * Created by RambabuN on 11/20/2019.
 */
class PaymentLineItemDTO {

  var _linePublicID : String as LinePublicID
  var _lineSquenceNumber : int as LineSquenceNumber
  var _vendorNumber : String as VendorNumber
  var _invoiceNumber : String as InvoiceNumber
  var _claimNumber : String as ClaimNumber
  var _claimantName : String as ClaimantName
  var _insuredName : String as InsuredName
  var _vendorInvoiceNumber : String as VendorInvoiceNumber
  var _lineAmount : BigDecimal as LineAmount
  var _voucherNumber : String as VoucherNumber

}