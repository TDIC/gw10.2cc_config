/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package tdic.cc.integ.plugins.hpexstream.util

uses com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateIds
uses com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateidToDocPubIdMap
uses com.tdic.plugins.hpexstream.core.util.TDIC_ExstreamHelper
uses com.tdic.plugins.hpexstream.core.util.TDIC_Helper
uses com.tdic.plugins.hpexstream.core.xml.TDIC_ExstreamXMLCreatorFactory
uses gw.api.util.DateUtil
uses gw.plugin.Plugins
uses gw.plugin.document.IDocumentTemplateSource
uses gw.util.concurrent.LockingLazyVar

uses java.lang.IllegalArgumentException
uses java.util.ArrayList
uses java.util.Date

uses gw.pl.persistence.core.Bundle
uses gw.api.system.database.SequenceUtil
uses org.slf4j.LoggerFactory

class TDIC_CCExstreamHelper extends TDIC_ExstreamHelper implements TDIC_Helper {
  private static var _theInstance = LockingLazyVar.make(\-> new TDIC_CCExstreamHelper ())
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  private static var _portalStatusYes =  "Yes"
  private static var _portalStatusNo =  "No"
  private static var _authorSystemGenerated = "System Generated"
  static property get Instance(): TDIC_CCExstreamHelper {
    return _theInstance.get()
  }

  /**
   * This function
   * 1) Sets the appropriate document status and doc delivery status
   * 2) Instantiates Document Approval Workflow
   * when the user tries to Finalize a live document
   *
   * @param pLiveDocument  Live document being finalized
   * @return void
   */
  function finalizeLiveDocument(pLiveDocument: Document): void {
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {

      // Update document status and doc delivery status
      var writeableDoc = bundle.add(pLiveDocument)
      writeableDoc.Status = DocumentStatusType.TC_APPROVING
      writeableDoc.DeliveryStatus_TDIC = DocDlvryStatusType_TDIC.TC_SENDING

      // Save updated document metadata
      try {
        var documentMetadataSource = Plugins.get(gw.plugin.document.IDocumentMetadataSource)
        documentMetadataSource.saveDocument(writeableDoc)
      }catch (e : Exception) {
        // ignore
        _logger.warn("BaseLocalDocumentMetadataSource.storeDocument: " + e.Message, e)
      }

      // Instantiate workflow
      var workflow = new entity.DocumentApprovalWF_TDIC()
      workflow.Document = writeableDoc
      workflow.startAsynchronously()
    })
  }

  /**
   * US555
   * 11/18/2014 shanem
   *
   * Returns a document name for a given PolicyPeriod
   */
  @Param("aPolicyNumber", "PolicyNumber of the document's associated policy")
  @Param("aTemplateId", "Template ID used to create document")
  @Returns("Template Desccriptor")
  function getDocumentName(aClaimNumber: String, aTemplateId: String): String {
    if (aClaimNumber == null || aTemplateId == null){
      throw new IllegalArgumentException("Policy Number or Template Id Is NULL")
    }
    return aTemplateId + "_" + aClaimNumber + "_" + DateUtil.currentDate()
  }

  function createDocumentStub(aTemplateId: String, aPrintOrder: int, aClaim: Claim, eventName: String): void {
    createDocumentStub(aTemplateId, aPrintOrder, aClaim, eventName, "System Generated")
  }

  /**
   * US644
   * 11/25/2014 shanem
   *
   * Creates a document for a given Claim
   */
  @Param("aTemplateId", "Template to create stub for")
  @Param("aPolicyPeriod", "PolicyPeriod to associate document stub with")
  function createDocumentStub(aTemplateId: String, aPrintOrder: int, aClaim: Claim, eventName: String, user: String): void {
    _logger.trace("TDIC_CCExstreamHelper#createDocumentStub() - Entering.")
    var doc: Document
    var dts = gw.plugin.Plugins.get(IDocumentTemplateSource)
    var templateDescriptor = dts.getDocumentTemplate(aTemplateId, null)
    if (templateDescriptor != null) {
      doc = gw.transaction.Transaction.getCurrent().add(new Document())
      doc.PendingDocUID = SequenceUtil.next(1, "ExstreamDocUID") as String
      doc.Name = getDocumentName(aClaim.ClaimNumber, aTemplateId)
      doc.Status = DocumentStatusType.TC_APPROVED
      doc.Type = typekey.DocumentType.get(templateDescriptor.TemplateType)
      doc.Subtype = typekey.OnBaseDocumentSubtype_Ext.get(templateDescriptor.getMetadataPropertyValue("subtype") as String)
      doc.Author = user
      doc.Claim = aClaim
      doc.Description = templateDescriptor.Description
      doc.MimeType = templateDescriptor.MimeType
      doc.DeliveryStatus_TDIC = DocDlvryStatusType_TDIC.TC_SENDING
      doc.TemplateId_TDIC = aTemplateId
      doc.Event_TDIC = eventName
      doc.DateCreated = new Date()
      doc.DateModified = new Date()
      doc.PrintOrder_TDIC = aPrintOrder
      doc.PortalStatus_TDIC = user.equalsIgnoreCase(_authorSystemGenerated) ? _portalStatusYes : _portalStatusNo
      doc.PrintID_TDIC = doc.PendingDocUID
      aClaim.addToDocuments(doc)
    } else {
      _logger.warn("TDIC_CCExstreamHelper#createDocumentStub() -No Template Descriptor found for Template Id " + aTemplateId)
    }
    _logger.trace("TDIC_CCExstreamHelper#createDocumentStub() - Created document" + doc.Name)
    _logger.trace("TDIC_CCExstreamHelper#createDocumentStub() - Exiting.")
  }

  /**
   * US644
   * 11/25/2014 shanem
   *
   * Creates a document stub for all template Id's
   */
  @Param("aTemplateIdList", "Templates to create stubs for")
  @Param("aPolicyPeriod", "PolicyPeriod for documents to be associated with")
  function createDocumentStubs(eventName: String, aClaim: Claim): void {
    _logger.trace("TDIC_PCExstreamHelper#createDocumentStubs() - Entering.")
    var helper = new TDIC_ExstreamHelper()
    var templatesForEvent = helper.getEventTemplates(eventName, aClaim.AssignmentDate)
    if (!aClaim.SuppressFNOLDocsEmail_TDIC && eventName == "FNOL"){
      templatesForEvent.put("FNOLEmail", 99)
    }
    templatesForEvent.eachKeyAndValue(\k, val -> createDocumentStub(k, val, aClaim, eventName))
    _logger.debug("TDIC_ClaimEnhancement#sendIssuanceRequest() - Created " + templatesForEvent.Count + " document stubs.")
    _logger.trace("TDIC_PCExstreamHelper#createDocumentStubs() - Exiting.")
  }

  /**
   * US644
   * 11/26/2014 shanem
   *
   * Creates a message for documents to be sent asynchronously
   */
  @Param("aMessageContext", "The data type is: MessageContext")
  @Param("aDoc", "Document to create message for.")
  function createDocumentMessage(aMessageContext: MessageContext, aDoc: Document): void {
    _logger.trace("TDIC_PCExstreamHelper()#createDocumentMessage() - Entering.")
    var claim = aDoc.Claim
    var parameters = new java.util.HashMap(){"Claim" -> claim}
    var templateId = {new TDIC_TemplateidToDocPubIdMap(aDoc.TemplateId_TDIC, aDoc)}
    var templateMap = new TDIC_TemplateIds(new String[] {aDoc.TemplateId_TDIC}, templateId?.toTypedArray())
    var payload = TDIC_ExstreamXMLCreatorFactory
        .getExstreamXMLCreator(parameters)
        .generateTransactionXML(templateMap, aMessageContext.EventName, true, null, null)
    var message = aMessageContext.createMessage(payload)
    message.MessageRoot = aDoc
    _logger.trace("TDIC_PCExstreamHelper()#createDocumentMessage() - Message " + message.PublicID + " created.")
    _logger.trace("TDIC_PCExstreamHelper()#createDocumentMessage() - Exiting.")
  }

  /**
   * US644
   * 11/26/2014 shanem
   *
   * Creates a message for documents to be sent asynchronously
   */
  @Param("aMessageContext", "The data type is: MessageContext")
  @Param("aBatchIdMap", "HashMap of templateIds and their corresponding document stub public Id")
  @Param("aDoc", "Document to create message for.")
  function createBatchMessage(aMessageContext: MessageContext, aBatchIdMap: TDIC_TemplateIds, documents: ArrayList<Document>): void {
    if (aBatchIdMap != null){
      var aClaim = aMessageContext.Root
      if(aMessageContext.Root typeis Claim){
        aClaim = aMessageContext.Root as Claim
      }
      else if(aMessageContext.Root typeis Matter){
        aClaim = (aMessageContext.Root as Matter).Claim
      }
      var parameters = new java.util.HashMap(){"Claim" -> aClaim}
      var payload = TDIC_ExstreamXMLCreatorFactory
          .getExstreamXMLCreator(parameters)
          .generateTransactionXML(aBatchIdMap, aMessageContext.EventName, true, null, null)
      var message = aMessageContext.createMessage(payload)
      message.MessageRoot = documents.first()
    }
  }

  override function createStub(aTemplateId: String, aPrintOrder: int, anEntity: Object, eventName: String, user: String) {
    createDocumentStub(aTemplateId, aPrintOrder, (anEntity as Claim), eventName, user)
  }

  override function skipDocCreation(aTemplateId: String, anEntity: Object, aBundle: Bundle) {
    if (anEntity typeis Claim) {
      var claim = aBundle.add(anEntity)
      claim.addEvent(aTemplateId)
    }
  }
}
