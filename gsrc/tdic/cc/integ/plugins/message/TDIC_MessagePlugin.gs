package tdic.cc.integ.plugins.message

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.plugin.messaging.MessageTransport
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 5/7/15
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class TDIC_MessagePlugin implements MessageTransport {

  private var _logger = LoggerFactory.getLogger("TDIC_INTEGRATION")
  protected var _destId : int = 0

  //Commented as part of upgrade.
 /* override function setDestinationID(aDestId: int): void {
    _destId = aDestId
  }*/

  override function suspend() {
    var _emailRecipient = PropertyUtil.getInstance().getProperty("InfraIssueNotificationEmail")
    if(_emailRecipient == null){
      _logger.error("TDIC_WCpolsTransport#suspend() - Cannot retrieve notification email addresses with the key 'InfraIssueNotificationEmail' from integration database.")
    }
    else
      EmailUtil.sendEmail(_emailRecipient, "Destination suspended in :" + gw.api.system.server.ServerUtil.Product.ProductName + " at " + gw.api.system.server.ServerUtil.ServerId, "Destination with ID: " + _destId + " is suspended.")
  }

}