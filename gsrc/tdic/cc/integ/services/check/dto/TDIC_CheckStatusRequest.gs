package tdic.cc.integ.services.check.dto

uses gw.xml.ws.annotation.WsiExportable

/**
 *This class is used to capture list of CheckStatus records from updateCheckStatus webservice Request.
 * Created by RambabuN on 11/22/2019.
 */
@WsiExportable("http://tdic.com/tdic/cc/integ/services/check/dto/TDIC_CheckStatusRequest")
final class TDIC_CheckStatusRequest {

  var _checkStatusDTOList : List<TDIC_CheckStatusDTO> as CheckStatusDTOList

  override function toString() : String {
    var checkStatusRequest = new StringBuffer("Request: ")
    for(record in CheckStatusDTOList) {
      checkStatusRequest.append("Record: " + record.toString() + "; ")
    }
    return checkStatusRequest.toString()
  }
}