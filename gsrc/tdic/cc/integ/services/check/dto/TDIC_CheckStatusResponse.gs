package tdic.cc.integ.services.check.dto

uses gw.xml.ws.annotation.WsiExportable

/**
 *This class is used to capture check status response information for updateCheckStatus Webservice.
 * Created by RambabuN on 11/22/2019.
 */
@WsiExportable("http://tdic.com/tdic/cc/integ/services/check/dto/TDIC_CheckStatusResponse")
final class TDIC_CheckStatusResponse {

  var _checkStatusResponseDTOList : List<TDIC_CheckStatusResponseDTO> as CheckStatusResponseDTOList

  override function toString() : String {
    var response = new StringBuffer("Response: ")
    for(record in CheckStatusResponseDTOList) {
      response.append(record.toString())
    }
    return response.toString()
  }

}