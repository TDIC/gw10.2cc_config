package tdic.cc.integ.services.check.dto

uses gw.xml.ws.annotation.WsiExportable
uses tdic.cc.integ.services.util.dto.TDIC_ErrorMessageDTO

/**
 *This class is used to capture list of CheckStatus records,Error messages and comments for updateCheckStatus
 * webservice Response.
 * Created by RambabuN on 11/22/2019.
 */
@WsiExportable("http://tdic.com/tdic/cc/integ/services/check/dto/TDIC_CheckStatusResponseDTO")
final class TDIC_CheckStatusResponseDTO {
  var _checkStatusDTO : TDIC_CheckStatusDTO as CheckStatusDTO
  var _comments : String as Comments
  var _errors : List<TDIC_ErrorMessageDTO> as Errors

  override function toString() : String {
    var response = new StringBuffer("Record: ${CheckStatusDTO.toString()} | ${Comments} | Errors: ")
    for(error in Errors) {
      response.append(error.toString()+"| ")
    }
    return response.toString()
  }
}