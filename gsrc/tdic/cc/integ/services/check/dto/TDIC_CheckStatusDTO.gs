package tdic.cc.integ.services.check.dto

uses gw.xml.ws.annotation.WsiExportable

uses java.math.BigDecimal

/**
 *This class is used to capture check status record information for updateCheckStatus Webservice request object.
 * Created by RambabuN on 11/22/2019.
 */
@WsiExportable("http://tdic.com/tdic/cc/integ/services/check/dto/TDIC_CheckStatusDTO")
final class TDIC_CheckStatusDTO {
  var _transactionRecordID : String as TransactionRecordID //Required
  var _checkDate : Date as CheckIssueDate
  var _checkNumber : String as CheckNumber //Required
  var _checkPayee : String as CheckPayee
  var _vendorID : String as VendorID
  var _checkStatus : String as CheckStatus
  var _checkStatusDate : Date as CheckStatusDate
  var _checkAmount : BigDecimal as CheckAmount

  construct() {
  }

  override function toString() : String {
    return new StringBuilder()
        .append("${TransactionRecordID} | ")
        .append("${CheckIssueDate} | ")
        .append("${CheckNumber} | ")
        .append("${CheckPayee} | ")
        .append("${VendorID} | ")
        .append("${CheckStatus} | ")
        .append("${CheckStatusDate} | ")
        .append("${CheckAmount}").toString()
  }
}